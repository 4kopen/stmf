#!/bin/bash

#build_stmf.sh

# this is standalone top build file for APPS_STMF_ext. This file
# calls build_module.sh to compile. it takes single argument
# <mock-name> as input if rootfs is on mock else it will use
# "/".
# @author      : Pavan Goyal
# @Last Upadte : 5th Dec 2014


MOCK_ROOT=""

if [ "$1" != "" ]
then
 MOCK_ROOT=/var/lib/mock/st-redhat6.4-x86_64-$1/root/
 proot -R $MOCK_ROOT -w $PWD -b $PWD -0 ./build_module.sh
else
 echo "****** No Mock Specified! Running in default ******"
 ./build_module.sh
fi
