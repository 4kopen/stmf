/* Gstreamer ST Video-Decoder Plugin
 *
 * Copyright (C) 2008 David Schleef <ds@schleef.org>
 * Copyright (C) 2011 Mark Nauwelaerts <mark.nauwelaerts@collabora.co.uk>.
 * Copyright (C) 2011 Nokia Corporation.
 *   Contact: Stefan Kost <stefan.kost@nokia.com>
 * Copyright (C) 2012 Collabora Ltd.
 *   Author : Edward Hervey <edward@collabora.com>
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:element-stvideo
 *
 * Implementation of the gstreamer stvideo decoder element
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! stvideo ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <string.h>
#include <linux/dvb/video.h>
#include <linux/dvb/stm_ioctls.h>
#include <linux/videodev2.h>
#include <linux/dvb/dvb_v4l2_export.h>
#include "gststvideo.h"
#include "pes.h"
#include "v4l2_utils.h"
#include "gst_stm_mctl_wrap.h"
#include "gststencode.h"
#include <gst/video/video.h>
#include <sys/mman.h>
#include <stdlib.h>
#include "gstmpegdefs.h"
GST_DEBUG_CATEGORY_STATIC (gst_stvideo_debug);
#define GST_CAT_DEFAULT gst_stvideo_debug

#define DEFAULT_DISABLE_AVSYNC                  FALSE
#define DEFAULT_DISABLE_INJ                     FALSE
#define GST_STVIDEO_H264_HEADER_SIZE            4
#define GST_STVIDEO_H264_PLAYER2_HEADER_SIZE    10
#define GST_STVIDEO_H264_PLAYER2_DATA_SIZE      12
#define GST_STVIDEO_H265_HEADER_SIZE            3
#define GST_STVIDEO_TM_MASTER_MAX_FRAME_REPEAT  500
#define IS_GST_STVIDEO_TM_MASTER_SPEED(X)       ((X > 1.0) || (X < 0))
#define GST_STVIDEO_FF_MAX_UNDERFLOW            3
#define GST_STVIDEO_REVERSE_BUF_CMP_SIZE        128
#define DEFAULT_LIVE_LATENCY                    150     /* ms */
#define GST_STVIDEO_FRAME_RATE_MULTIPLIER       1000

#define LOG_CAPS(obj, caps) GST_DEBUG_OBJECT (obj, "%" GST_PTR_FORMAT, caps)
#define DEFAULT_CURRENTPTS      		(1)
#define MAX_VALUE_CURRENTPTS     	(0x1FFFFFFFF)
#define MIN_VALUE_CURRENTPTS 		(0)

#define DEFAULT_CONTENTFRAMERATE 	(0)
#define MAX_VALUE_CONTENTFRAMERATE 	(60)
#define MIN_VALUE_CONTENTFRAMERATE 	(0)

/* the capabilities of the inputs and outputs.
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/fake-pes;"
        "video/x-h263;"
        "video/x-h264;"
        "video/x-h264-pes;"
        "video/x-h265;"
        "video/x-h265-pes;"
        "image/jpeg,"
        "  framerate = (fraction) [ 0, MAX ];"
        "video/mpeg,"
        "  mpegversion = (int) {1, 2, 4},"
        "  systemstream = (boolean) false;"
        "video/mpeg-pes,"
        "  mpegversion = (int) {1, 2, 4},"
        "  systemstream = (boolean) false;"
        "video/x-xvid;"
        "video/x-3ivx;"
        "video/x-divx,"
        "  divxversion = (int) {3, 4, 5};"
        "video/x-theora;"
        "video/x-vp3;"
        "video/x-flash-video;"
        "video/x-vp6;"
        "video/x-vp6-alpha;"
        "video/x-vp6-flash;"
        "video/x-vp8;"
        "video/x-vp9;"
        "video/x-pn-realvideo;"
        "video/x-wmv,"
        "  wmvversion = (int) {1, 2, 3};"
        "video/x-wmv,"
        "  wmvversion = (int) 3,"
        "  format = (string) WVC1;"
        "video/x-wmv-pes,"
        "  wmvversion = (int) 3,"
        "  format = (string) WVC1;"
        "video/x-avs;" "video/x-avs-pes;" "video/x-avsp;" "video/x-avsp-pes;")
    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-fake-yuv;"
        GST_VIDEO_CAPS_MAKE ("BGRA") ";"
        GST_VIDEO_CAPS_MAKE ("BGRx") ";"
        GST_VIDEO_CAPS_MAKE ("RGB") ";" GST_VIDEO_CAPS_MAKE ("UYVY") ";"
        GST_VIDEO_CAPS_MAKE ("YUYV") ";")
    );

#define gst_stvideo_parent_class parent_class
G_DEFINE_TYPE (Gststvideo, gst_stvideo, GST_TYPE_ELEMENT);
#define MIN_DEV_ID          0
/* total video device available are 25 on target video0 .. video25 */
#define MAX_DEV_ID          24
#define DEFAULT_DEV_ID      MAX_DEV_ID

#define MIN_LATENCY         0
#define MAX_LATENCY         2550
#define DEFAULT_LATENCY     0

#define DEFAULT_INTERLACE FALSE

/* DVB memory profiles are defined in stm_dvb_options.h file in STLinuxTV */
/* 0 (HD), 1 (4K2K), 2 (SD), 3 (720p),  4 (UHD 3840*2160) */
/* 6 (HD 10 bits), 7 (4K2K 10 bits), 8 (720p 10 bits),
 * 9 (UHD 3840*2160 10 bits)
 * 10 (Auto)
 * */
#define MIN_MEM_PROFILE      DVB_OPTION_CTRL_VALUE_HD_PROFILE   /* 0 */
#define MAX_MEM_PROFILE      DVB_OPTION_CTRL_VALUE_AUTO_PROFILE /* 10 */
#define DEFAULT_MEM_PROFILE  DVB_OPTION_CTRL_VALUE_HD_PROFILE   /* 0 */

/* Video Decoder Decimation */
/* (0=off, 1=H2V2(half), 2=H4V2(quarter), 3=H2V4, 4=H2V8,
 * 5=H4V4, 6=H4V8, 7=H8V2, 8=H8V4, 9=H8V8)
 * */
#define MIN_DECODER_DECIMATION      DVB_OPTION_VALUE_DECIMATE_DECODER_OUTPUT_DISABLED   /* 0 */
#define MAX_DECODER_DECIMATION      DVB_OPTION_VALUE_DECIMATE_DECODER_OUTPUT_H8V8       /* 9 */
#define DEFAULT_DECODER_DECIMATION  DVB_OPTION_VALUE_DECIMATE_DECODER_OUTPUT_DISABLED   /* 0 */

#define DEFAULT_BPP          16
#define DEFAULT_PIXEL_FORMAT V4L2_PIX_FMT_UYVY

enum
{
  PROP_0,
  PROP_DISABLE_INJECT,
  PROP_DEV_ID,
  PROP_CURRENTPTS,
  PROP_CONTENTFRAMERATE,
  PROP_LATENCY,
  PROP_DISABLE_AVSYNC,
  PROP_MEM_PROFILE,
  PROP_STEPFRAME,
  PROP_INTERLACE,
  PROP_DECODER_DECIMATION,
  PROP_LAST
};

/* A structure for storing events in List-queued_events_list */
typedef struct
{
  GstClockTime ts;              /* Time (in ms) at which Event is to be sent downstream */
  GstEvent *event;              /* Serialized Event to be sent downstream */
} queued_event;

static gboolean gst_stvideo_send_queued_events_before (Gststvideo * viddec,
    GstClockTime ts);
static void gst_stvideo_queue_event (Gststvideo * viddec, GstEvent * event,
    GstClockTime ts);
static queued_event *gst_stvideo_make_queued_event (Gststvideo * viddec,
    GstEvent * event, GstClockTime ts);
static void gst_stvideo_drop_queued_events (Gststvideo * viddec);
static void gst_stvideo_free_queued_event (queued_event * qe);

static void gst_stvideo_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_stvideo_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static gboolean gst_stvideo_sink_acceptcaps (GstCaps * caps);
static gboolean gst_stvideo_set_caps (Gststvideo * viddec, GstCaps * caps);
static GstFlowReturn gst_stvideo_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buf);
static void gst_stvideo_reset (Gststvideo * viddec);
static gboolean gst_stvideo_open_device (Gststvideo * viddec);
static gboolean gst_stvideo_close_device (Gststvideo * viddec);
static gboolean gst_stvideo_term_device (Gststvideo * viddec);
static void gst_stvideo_setup_device (Gststvideo * viddec);
static gboolean gst_stvideo_ioctl (Gststvideo * viddec, guint ioctl_code,
    void *parameter);
static gboolean gst_stvideo_write (Gststvideo * viddec, guint8 * data,
    guint data_length);
static void gst_stvideo_update_time (Gststvideo * viddec);
static gboolean gst_stvideo_apply_set_speed (Gststvideo * viddec);
static gboolean gst_stvideo_tm_control_update (Gststvideo * viddec,
    gdouble rate, GstClockTime seek_time);
static void gst_stvideo_tm_control_check_event (Gststvideo * viddec);
static gchar *gst_stvideo_ioctl_name (gint ioctl_code);

static gboolean gst_stvideo_write_packet (Gststvideo * viddec, GstBuffer * buf);
static gboolean gst_stvideo_write_h264_hevc_packet (Gststvideo * viddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts);
static gboolean gst_stvideo_write_mpeg4p2_packet (Gststvideo * viddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts);
static gboolean gst_stvideo_write_vp_packet (Gststvideo * viddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts);
static gboolean gst_stvideo_write_video_packet (Gststvideo * viddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts);

static gboolean gst_stvideo_injection_control (Gststvideo * viddec,
    GstBuffer * buf);
static gboolean gst_stvideo_push_dummy_packet (Gststvideo * viddec);
static gboolean gst_stvideo_set_speed (Gststvideo * viddec, gdouble rate,
    gboolean set_policies);
static void gst_stvideo_set_live_play_policies (Gststvideo * viddec);
static void gst_stvideo_set_file_play_policies (Gststvideo * viddec);
static void gst_stvideo_manage_se_trickmode_policies (Gststvideo * viddec,
    gdouble rate, GstStVideoTrickPolicyType policy);
static void gst_stvideo_handle_size_changed_event (Gststvideo * viddec,
    void *event);
static void gst_stvideo_handle_framerate_changed_event (Gststvideo * viddec,
    void *event);
static void gst_stvideo_handle_trickmode_change_event (Gststvideo * viddec,
    void *event);

static void gst_stvideo_video_loop (Gststvideo * viddec);
static gboolean gst_stvideo_capture_setup_device (Gststvideo * viddec);
static gboolean gst_stvideo_capture_queue_buffer (Gststvideo * viddec);
static gboolean gst_stvideo_capture_dequeue_buffer (Gststvideo * viddec);
static gboolean gst_stvideo_capture_alloc_buffer (Gststvideo * viddec, int fd);
static gboolean gst_stvideo_capture_free_buffer (Gststvideo * viddec, int fd);
static gboolean gst_stvideo_capture_stream_on (Gststvideo * viddec, int fd);
static gboolean gst_stvideo_capture_stream_off (Gststvideo * viddec, int fd);

static gboolean gst_stvideo_manage_dev_id (Gststvideo * viddec);
static void gst_stvideo_start_monitor_task (Gststvideo * viddec);
static void gst_stvideo_stop_monitor_task (Gststvideo * viddec);
static void gst_stvideo_monitor_loop (GstPad * pad);
static gboolean gst_stvideo_src_pad_query (GstPad * pad, GstObject * parent,
    GstQuery * query);
static gboolean gst_stvideo_src_pad_event (GstPad * pad, GstObject * parent,
    GstEvent * event);
static gboolean gst_stvideo_sink_pad_query (GstPad * pad, GstObject * parent,
    GstQuery * query);
static gboolean gst_stvideo_manage_caps (Gststvideo * viddec);
static gboolean gst_stvideo_capture_set_format (Gststvideo * viddec);
static void gst_stvideo_update_src_pad_caps (Gststvideo * viddec);
static GstBufferPool *gst_stvideo_get_pool (Gststvideo * viddec);

static guint8 h264_start_code[GST_STVIDEO_H264_HEADER_SIZE] =
    { 0x00, 0x00, 0x00, 0x01 };
static guint8 h264_player2_header[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE] =
    { 0x00, 0x00, 0x00, 0x01, 0x18, 0x01, 0xff, 0x00, 0x02, 0xff };

static guint8 h265_start_code[GST_STVIDEO_H265_HEADER_SIZE] =
    { 0x00, 0x00, 0x01 };

/* Send all queued events received before timestamp ts downstream */
static gboolean
gst_stvideo_send_queued_events_before (Gststvideo * viddec, GstClockTime ts)
{
  GstPad *pad = viddec->srcpad;
  gboolean ret = TRUE;

  g_mutex_lock (&viddec->event_lock);

  if (ts == GST_CLOCK_TIME_NONE) {
    GST_DEBUG_OBJECT (viddec, "Drain Queued Events downstream");
  }
  while (viddec->queued_events_list) {
    queued_event *ev = viddec->queued_events_list->data;
    gboolean ret_loop = TRUE;

    if ((ev->ts != GST_CLOCK_TIME_NONE) && (ts != GST_CLOCK_TIME_NONE)
        && (ev->ts > ts))
      break;

    GST_DEBUG_OBJECT (viddec,
        "Sending event %s with Queue Event Time %lld ms at Dequeue Time %lld ms",
        GST_EVENT_TYPE_NAME (ev->event), ev->ts, ts);

    /* For trickmode segment needs to be updated before pushing down */
    if (GST_EVENT_TYPE (ev->event) == GST_EVENT_SEGMENT
        && viddec->tunneled == FALSE) {
      const GstSegment *in_segment;
      GstSegment out_segment;

      memset (&out_segment, 0, sizeof (out_segment));
      gst_event_parse_segment (ev->event, &in_segment);

      gst_segment_copy_into (in_segment, &viddec->segment);

      out_segment.rate = 1.0;
      out_segment.applied_rate = in_segment->rate;
      out_segment.start = 0;
      if (in_segment->stop == GST_CLOCK_TIME_NONE) {
        out_segment.stop = in_segment->stop;
      } else {
        out_segment.stop =
            (in_segment->stop - in_segment->start) / ABS (in_segment->rate);
      }
      out_segment.time = in_segment->position;
      out_segment.duration = out_segment.stop - out_segment.start;
      out_segment.format = in_segment->format;
      out_segment.position = in_segment->position;

      gst_event_unref (ev->event);
      ev->event = gst_event_new_segment ((const GstSegment *) &out_segment);
    }
    /* Push Serialized event downstream */
    ret_loop = gst_pad_push_event (pad, gst_event_ref (ev->event));
    gst_stvideo_free_queued_event (viddec->queued_events_list->data);
    viddec->queued_events_list =
        g_list_delete_link (viddec->queued_events_list,
        viddec->queued_events_list);
    if (!ret_loop) {
      GST_WARNING_OBJECT (viddec, "Pushing event downstream failed");
      ret = ret_loop;
    }
  }

  g_mutex_unlock (&viddec->event_lock);

  return ret;
}

/* Queue an event in the List-queued_events_list */
static void
gst_stvideo_queue_event (Gststvideo * viddec, GstEvent * event, GstClockTime ts)
{
  g_mutex_lock (&viddec->event_lock);
  GST_DEBUG_OBJECT (viddec, "Queueing event %s with TimeStamp %lld ms",
      GST_EVENT_TYPE_NAME (event), ts);
  viddec->queued_events_list =
      g_list_append (viddec->queued_events_list,
      gst_stvideo_make_queued_event (viddec, event, ts));
  g_mutex_unlock (&viddec->event_lock);
}

/* Make an object of queued_event which can be inserted in the List-queued_events_list */
static queued_event *
gst_stvideo_make_queued_event (Gststvideo * viddec, GstEvent * event,
    GstClockTime ts)
{
  queued_event *qe = g_slice_new (queued_event);
  GST_LOG_OBJECT (viddec, "Event %s Timestamp %lld ms",
      GST_EVENT_TYPE_NAME (event), ts);
  qe->ts = ts;
  qe->event = gst_event_ref (event);
  return qe;
}

/* Flush all queued events and free the list */
static void
gst_stvideo_drop_queued_events (Gststvideo * viddec)
{
  g_mutex_lock (&viddec->event_lock);
  GST_DEBUG_OBJECT (viddec, "Drop all Queued Serialized Events");
  g_list_free_full (viddec->queued_events_list,
      (GDestroyNotify) gst_stvideo_free_queued_event);
  viddec->queued_events_list = NULL;
  g_mutex_unlock (&viddec->event_lock);
}

/* Free queued event */
static void
gst_stvideo_free_queued_event (queued_event * qe)
{
  gst_event_unref (qe->event);
  g_slice_free (queued_event, qe);
}

/* GObject vmethod implementations */
static void
gst_stvideo_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  Gststvideo *viddec = GST_STVIDEO (object);

  GST_DEBUG_OBJECT (viddec, "property to be set is %d", prop_id);
  GST_OBJECT_LOCK (viddec);
  switch (prop_id) {
    case PROP_DISABLE_AVSYNC:
      viddec->disable_avsync = g_value_get_boolean (value);
      break;
    case PROP_DISABLE_INJECT:
      viddec->property_disable_dev = g_value_get_boolean (value);
      break;
    case PROP_DEV_ID:
      viddec->dev_id_prop = g_value_get_int (value);
      break;
    case PROP_LATENCY:
      viddec->latency = g_value_get_uint (value);
      break;
    case PROP_STEPFRAME:
      if (viddec->state == VIDEO_FREEZED) {
        if (gst_stvideo_ioctl (viddec, VIDEO_STEP, NULL) == FALSE) {
          GST_ERROR_OBJECT (viddec, "video ioctl step failed");
        }
      } else {
        GST_WARNING_OBJECT (viddec,
            "Need to be in pause state to issue this command");
      }
      break;
    case PROP_MEM_PROFILE:
      if (viddec->device_config == TRUE) {
        viddec->mem_profile = g_value_get_uint (value);
      } else {
        GST_WARNING_OBJECT (viddec,
            "device configured: can not set memory profile");
      }
      break;
    case PROP_INTERLACE:
      if (viddec->device_config == TRUE) {
        viddec->interlace = g_value_get_boolean (value);
      } else {
        GST_WARNING_OBJECT (viddec,
            "device configured: can not set interlaced mode");
      }
      break;
    case PROP_DECODER_DECIMATION:
      if (viddec->device_config == TRUE) {
        viddec->decimation = g_value_get_uint (value);
      } else {
        GST_WARNING_OBJECT (viddec,
            "device configured: can not set video decoder decimation");
      }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

  GST_OBJECT_UNLOCK (viddec);
  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return;
}

static void
gst_stvideo_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  Gststvideo *viddec = GST_STVIDEO (object);

  GST_DEBUG_OBJECT (viddec, "property to be get is %d", prop_id);
  GST_OBJECT_LOCK (viddec);
  switch (prop_id) {
    case PROP_DISABLE_AVSYNC:
      g_value_set_boolean (value, viddec->disable_avsync);
      break;
    case PROP_DISABLE_INJECT:
      g_value_set_boolean (value, viddec->property_disable_dev);
      break;
    case PROP_DEV_ID:
      /* Return the actual value of dev_id that is currently set, rather than dev_id_prop set by App */
      g_value_set_int (value, viddec->dev_id);
      break;
    case PROP_CURRENTPTS:
    {
      /*
         We are using the ioctl call to get the PTS value.The parameter
         to get the PTS value is "VIDEO_GET_PTS".IOctl from file
         /kernel/sdk2-linux-3.x-arm/include/linux/dvb/video.h
       */
      guint64 currentPTS = DEFAULT_CURRENTPTS;

      if (!gst_stvideo_ioctl (viddec, VIDEO_GET_PTS, &currentPTS)) {
        GST_DEBUG_OBJECT (viddec, "failed to get PTS");
      } else {
        g_value_set_uint64 (value, currentPTS);
      }
    }
      break;
    case PROP_CONTENTFRAMERATE:
    {
      /*
         We are using the ioctl call to get the PTS value.The parameter
         to get the framerate value is "VIDEO_GET_FRAME_RATE".IOctl from file
         /kernel/sdk2-linux-3.x-arm/include/linux/dvb/video.h
       */

      gint framerate = DEFAULT_CONTENTFRAMERATE;

      if (!gst_stvideo_ioctl (viddec, VIDEO_GET_FRAME_RATE, &framerate)) {
        GST_DEBUG_OBJECT (viddec, "Failed to get frame rate");
        break;
      } else {
        g_value_set_int (value, framerate);
      }
    }
      break;
      break;
    case PROP_LATENCY:
      g_value_set_uint (value, viddec->latency);
      break;
    case PROP_MEM_PROFILE:
      g_value_set_uint (value, viddec->mem_profile);
      break;
    case PROP_INTERLACE:
      g_value_set_boolean (value, viddec->interlace);
      break;
    case PROP_DECODER_DECIMATION:
      g_value_set_uint (value, viddec->decimation);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
  GST_OBJECT_UNLOCK (viddec);
  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return;
}

/* GstElement vmethod implementations */

/* this function checks if we can accept these caps */
static gboolean
gst_stvideo_sink_acceptcaps (GstCaps * caps)
{
  gboolean ret = TRUE;
  /* We do the usual intersection, which is technically wrong, but
     for JPEG, where we really want to limit our allowed input to MJPG,
     we also check whether we have a framerate field, which indicates
     MPEG. */
  GstStructure *structure = gst_caps_get_structure (caps, 0);
  const char *media_type = gst_structure_get_name (structure);
  if (!strcmp (media_type, "image/jpeg")) {
    if (!gst_structure_has_field (structure, "framerate")) {
      GST_DEBUG ("JPEG is not MJPEG, declining");
      ret = FALSE;
    }
  }

  return ret;
}

/* this function handles the link with other elements */
static gboolean
gst_stvideo_set_caps (Gststvideo * viddec, GstCaps * caps)
{
  const GstStructure *structure;
  const gchar *mimetype;
  gboolean ret = FALSE;
  GstQuery *query;
  GstCaps *srccaps;

  structure = gst_caps_get_structure (caps, 0);
  mimetype = gst_structure_get_name (structure);

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  LOG_CAPS (viddec, caps);

  if (!strcmp (mimetype, "video/fake-pes")) {
    /* playback is disabled, terminate audio device */
    gst_stvideo_term_device (viddec);
    /* we still want pipeline to be PLAYING so we need to push at least a dummy buffer */
    viddec->tunneled = TRUE;
    if ((GST_PAD_TASK (viddec->srcpad)) &&
        (gst_task_get_state (GST_PAD_TASK (viddec->srcpad)) ==
            GST_TASK_STARTED)) {
      g_mutex_lock (&viddec->dummy_buff.mutex);
      viddec->video_task_state = GST_STVIDEO_TASK_STARTED;
      g_mutex_unlock (&viddec->dummy_buff.mutex);
    } else {
      srccaps = gst_caps_new_simple ("video/x-fake-yuv",
          "width", G_TYPE_INT, viddec->properties.width,
          "height", G_TYPE_INT, viddec->properties.height,
          "framerate", GST_TYPE_FRACTION, viddec->properties.fps_num,
          viddec->properties.fps_den,
          "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1, NULL);

      gst_pad_push_event (viddec->srcpad, gst_event_new_caps (srccaps));
      gst_caps_unref (srccaps);
    }
    /* nothing more to do, we can exit */
    return TRUE;
  }

  /* if not fake-pes caps, we apply property value */
  viddec->disable_dev = viddec->property_disable_dev;

  if (!strcmp (mimetype, "video/mpeg") || !strcmp (mimetype, "video/mpeg-pes")) {
    gint mpegversion = 0;

    if (!strcmp (mimetype, "video/mpeg-pes")) {
      viddec->is_pes = TRUE;
    }

    if (gst_structure_get_int (structure, "mpegversion", &mpegversion)) {
      switch (mpegversion) {
        case 1:
          GST_DEBUG_OBJECT (viddec, "codec MPEG1");
          viddec->codec.type = VIDEO_ENCODING_MPEG1;
          break;
        case 2:
          GST_DEBUG_OBJECT (viddec, "codec MPEG2");
          viddec->codec.type = VIDEO_ENCODING_MPEG2;
          break;
        case 4:
        {
          const GValue *codec_data_value;

          GST_DEBUG_OBJECT (viddec, "codec MPEG4");
          viddec->codec.type = VIDEO_ENCODING_MPEG4P2;
          /* REVIEW/FIXME : Just keep a reference to the buffer instead of making a copy of it */
          if ((codec_data_value =
                  gst_structure_get_value (structure, "codec_data"))) {
            GstBuffer *codec_data_buffer;
            GstMapInfo info_read, info_write;
            codec_data_buffer = gst_value_get_buffer (codec_data_value);
            gst_buffer_map (codec_data_buffer, &info_read, GST_MAP_READ);
            viddec->codec_data.buf = gst_buffer_new_and_alloc (info_read.size);
            gst_buffer_map (viddec->codec_data.buf, &info_write, GST_MAP_WRITE);
            if (viddec->codec_data.buf != NULL) {
              memcpy (info_write.data, info_read.data, info_read.size);
            }
            gst_buffer_unmap (codec_data_buffer, &info_read);
            gst_buffer_unmap (viddec->codec_data.buf, &info_write);
          }
        }
          break;
      }
    }
  } else if (!strcmp (mimetype, "video/x-h265")
      || !strcmp (mimetype, "video/x-h265-pes")) {
    const GValue *codec_data_value;

    GST_DEBUG_OBJECT (viddec, "codec HEVC");
    viddec->codec.type = VIDEO_ENCODING_HEVC;

    if (!strcmp (mimetype, "video/x-h265-pes")) {
      viddec->is_pes = TRUE;
    }

    if ((codec_data_value = gst_structure_get_value (structure, "codec_data"))) {
      GstBuffer *codec_data_buffer;
      guint8 *codec_data;
      guint offset = 0, param_sets_length = 0, num_param_sets = 0;
      guint codec_data_size;
      gint i = 0;
      gint j = 0;
      gint initial_header_len = 0;
      guint nb_nal;
      GstMapInfo info_read, info_write;

      codec_data_buffer = gst_value_get_buffer (codec_data_value);
      gst_buffer_map (codec_data_buffer, &info_read, GST_MAP_READ);
      codec_data = info_read.data;
      codec_data_size = info_read.size;

      GST_DEBUG_OBJECT (viddec, "additional codec data (%d bytes)",
          codec_data_size);

      /* Parse the hvcC data, according to ISO/IEC 14496-15, minimum size of
       * HEVCDecoderConfigurationRecord (i.e., the contents of a hvcC box) is 23 bytes.
       * Also, ConfigurationVersion is specified to be 1 but can be 0 for streams
       * which were created before the spec was finalized.
       */
      if (codec_data_size >= 23 && (codec_data[0] == 0 || codec_data[0] == 1)) {

        offset += 21;
        viddec->codec_data.h264.nal_header_size =
            (codec_data[offset] & 0x03) + 1;
        GST_DEBUG_OBJECT (viddec, "nal length size %u",
            viddec->codec_data.h264.nal_header_size);
        offset++;
        num_param_sets = codec_data[offset];
        offset++;

        for (i = 0; i < num_param_sets; i++) {
          nb_nal = (codec_data[offset + 1] << 8) + codec_data[offset + 2];
          offset += 3;

          for (j = 0; j < nb_nal; j++) {
            param_sets_length =
                (codec_data[offset] << 8) + codec_data[offset + 1];

            if (viddec->codec_data.buf == NULL) {
              viddec->codec_data.buf =
                  gst_buffer_new_and_alloc (param_sets_length +
                  sizeof (h265_start_code));
              if (viddec->codec_data.buf != NULL) {
                gst_buffer_map (viddec->codec_data.buf, &info_write,
                    GST_MAP_WRITE);
                memcpy (info_write.data + initial_header_len, h265_start_code,
                    sizeof (h265_start_code));
                initial_header_len += sizeof (h265_start_code);
                memcpy (info_write.data + initial_header_len,
                    &codec_data[offset + 2], param_sets_length);
                initial_header_len += param_sets_length;
                gst_buffer_unmap (viddec->codec_data.buf, &info_write);
              }
            } else {
              GstBuffer *buf = NULL;

              buf = gst_buffer_new_and_alloc (sizeof (h265_start_code) +
                  param_sets_length);

              if (buf != NULL) {
                gst_buffer_map (buf, &info_write, GST_MAP_WRITE);
                memcpy (info_write.data, h265_start_code,
                    sizeof (h265_start_code));
                memcpy (info_write.data + sizeof (h265_start_code),
                    &codec_data[offset + 2], param_sets_length);
                gst_buffer_unmap (buf, &info_write);
                viddec->codec_data.buf =
                    gst_buffer_append (viddec->codec_data.buf, buf);
              }
            }
            offset += param_sets_length + 2;
          }
        }

        if (viddec->codec_data.buf == NULL) {
          viddec->codec_data.buf =
              gst_buffer_new_and_alloc (sizeof (h265_start_code));

          if (viddec->codec_data.buf != NULL) {
            gst_buffer_map (viddec->codec_data.buf, &info_write, GST_MAP_WRITE);
            memcpy (info_write.data, h265_start_code, sizeof (h265_start_code));
            gst_buffer_unmap (viddec->codec_data.buf, &info_write);
          }
        }
      } else if ((codec_data[0] == 0x00) && (codec_data[1] == 0x00)
          && (codec_data[2] == 0x00) && (codec_data[3] == 0x01)) {
        GST_DEBUG_OBJECT (viddec, "h265 start code not needed");
      } else {
        viddec->codec.type = VIDEO_ENCODING_NONE;
        GST_ERROR_OBJECT (viddec, "Invalid hvcC header");
      }
      gst_buffer_unmap (codec_data_buffer, &info_read);
    }
  } else if (!strcmp (mimetype, "video/x-h264")
      || !strcmp (mimetype, "video/x-h264-pes")) {
    const GValue *codec_data_value;

    GST_DEBUG_OBJECT (viddec, "codec H264");
    viddec->codec.type = VIDEO_ENCODING_H264;

    if (!strcmp (mimetype, "video/x-h264-pes")) {
      viddec->is_pes = TRUE;
    }


    if ((codec_data_value = gst_structure_get_value (structure, "codec_data"))) {
      GstBuffer *codec_data_buffer;
      guint8 *codec_data;
      guint offset = 0, param_sets_length = 0, num_param_sets = 0;
      guint codec_data_size;
      gint profile;
      gint i = 0;
      GstMapInfo info_read, info_write;
      codec_data_buffer = gst_value_get_buffer (codec_data_value);
      gst_buffer_map (codec_data_buffer, &info_read, GST_MAP_READ);
      codec_data = info_read.data;
      codec_data_size = info_read.size;
      GST_DEBUG_OBJECT (viddec, "additional codec data (%d bytes)",
          codec_data_size);

      /* Parse the avcC data */
      if ((codec_data_size >= 8) && (codec_data[0] == 0x01)) {
        /* avcC profile */
        profile = (codec_data[1] << 16) | (codec_data[2] << 8) | codec_data[3];
        GST_DEBUG_OBJECT (viddec, "avcC profile %06x\n", profile);

        /* Read NAL length field */
        viddec->codec_data.h264.nal_header_size = (codec_data[4] & 0x03) + 1;
        GST_DEBUG_OBJECT (viddec, "NAL length will be coded on %u bytes\n",
            viddec->codec_data.h264.nal_header_size);

        /* Compute SPS and PPS size */
        /* Set offset to 5. Indicates sps byte location in avcC header. */
        offset = 5;

        /* Get number of SPS in avcC header */
        num_param_sets = codec_data[offset] & 0x1f;
        GST_DEBUG_OBJECT (viddec, "Number of Sequence Parameter Sets %d ",
            num_param_sets);

        /* Number of SPS is 1-byte long, increment offset counter by 1 byte */
        offset++;
        for (i = 0; i < num_param_sets; i++) {
          param_sets_length =
              ((codec_data[offset] << 8) | codec_data[offset + 1]);

          if (viddec->codec_data.buf == NULL) {
            viddec->codec_data.buf =
                gst_buffer_new_and_alloc (param_sets_length +
                sizeof (h264_start_code));
            if (viddec->codec_data.buf != NULL) {
              gst_buffer_map (viddec->codec_data.buf, &info_write,
                  GST_MAP_WRITE);
              memcpy (info_write.data, h264_start_code,
                  sizeof (h264_start_code));
              memcpy (info_write.data + sizeof (h264_start_code),
                  &codec_data[offset + 2], param_sets_length);
              gst_buffer_unmap (viddec->codec_data.buf, &info_write);
            }
          } else {
            GstBuffer *buf = NULL;
            buf = gst_buffer_new_and_alloc (sizeof (h264_start_code));
            if (buf != NULL) {
              gst_buffer_map (buf, &info_write, GST_MAP_WRITE);
              memcpy (info_write.data, h264_start_code,
                  sizeof (h264_start_code));
              gst_buffer_unmap (buf, &info_write);
              viddec->codec_data.buf =
                  gst_buffer_append (viddec->codec_data.buf, buf);
              buf = gst_buffer_new_and_alloc (param_sets_length);
              gst_buffer_map (buf, &info_write, GST_MAP_WRITE);
              memcpy (info_write.data, &codec_data[offset + 2],
                  param_sets_length);
              gst_buffer_unmap (buf, &info_write);
              viddec->codec_data.buf =
                  gst_buffer_append (viddec->codec_data.buf, buf);
            }
          }

          /* NAL length is 2-byte long, increment the offset by NAL length plus 2. */
          offset += param_sets_length + 2;
        }

        /* Get the number of PPS in avcC header */
        num_param_sets = codec_data[offset];
        GST_DEBUG_OBJECT (viddec, "Number of Picture Parameter Sets %d",
            num_param_sets);
        /* Number of PPS is 1-byte long, increment offset counter  by 1 byte */
        offset++;
        for (i = 0; i < num_param_sets; i++) {
          param_sets_length =
              (codec_data[offset] << 8 | codec_data[offset + 1]);
          if (viddec->codec_data.buf == NULL) {
            viddec->codec_data.buf =
                gst_buffer_new_and_alloc (param_sets_length +
                sizeof (h264_start_code));
            if (viddec->codec_data.buf != NULL) {
              gst_buffer_map (viddec->codec_data.buf, &info_write,
                  GST_MAP_WRITE);
              memcpy (info_write.data, h264_start_code,
                  sizeof (h264_start_code));
              memcpy (info_write.data + sizeof (h264_start_code),
                  &codec_data[offset + 2], param_sets_length);
              gst_buffer_unmap (viddec->codec_data.buf, &info_write);
            }
          } else {
            GstBuffer *buf = NULL;
            buf = gst_buffer_new_and_alloc (sizeof (h264_start_code));
            gst_buffer_map (buf, &info_write, GST_MAP_WRITE);
            memcpy (info_write.data, h264_start_code, sizeof (h264_start_code));
            gst_buffer_unmap (buf, &info_write);
            viddec->codec_data.buf =
                gst_buffer_append (viddec->codec_data.buf, buf);
            buf = gst_buffer_new_and_alloc (param_sets_length);
            gst_buffer_map (buf, &info_write, GST_MAP_WRITE);
            memcpy (info_write.data, &codec_data[offset + 2],
                param_sets_length);
            gst_buffer_unmap (buf, &info_write);
            viddec->codec_data.buf =
                gst_buffer_append (viddec->codec_data.buf, buf);
          }

          /* NAL length is 2-byte long, increment the offset by NAL length plus 2. */
          offset += param_sets_length + 2;
        }
      } else if ((codec_data[0] == 0x00) && (codec_data[1] == 0x00)
          && (codec_data[2] == 0x00) && (codec_data[3] == 0x01)) {
        GST_DEBUG_OBJECT (viddec, "h264 start code not needed");
      } else {
        viddec->codec.type = VIDEO_ENCODING_NONE;
        GST_ERROR_OBJECT (viddec, "Invalid avcC header");
      }
      gst_buffer_unmap (codec_data_buffer, &info_read);
    }
  } else if (!strcmp (mimetype, "image/jpeg")) {
    GST_DEBUG_OBJECT (viddec, "codec MJPEG");
    viddec->codec.type = VIDEO_ENCODING_MJPEG;
  } else if (!strcmp (mimetype, "video/x-h263")) {
    GST_DEBUG_OBJECT (viddec, "codec H263");
    viddec->codec.type = VIDEO_ENCODING_H263;
  } else if (!strcmp (mimetype, "video/x-xvid")) {
    GST_DEBUG_OBJECT (viddec, "codec MPEG4");
    viddec->codec.type = VIDEO_ENCODING_MPEG4P2;
  } else if (!strcmp (mimetype, "video/x-3ivx")) {
    GST_DEBUG_OBJECT (viddec, "codec MPEG4");
    viddec->codec.type = VIDEO_ENCODING_MPEG4P2;
  } else if (!strcmp (mimetype, "video/x-pn-realvideo")) {
    GST_DEBUG_OBJECT (viddec, "codec RealVideo");
    viddec->codec.type = VIDEO_ENCODING_RMV;
  } else if (!strcmp (mimetype, "video/x-divx")) {
    gint divxversion = 0;

    viddec->codec.type = VIDEO_ENCODING_MPEG4P2;

    if (gst_structure_get_int (structure, "divxversion", &divxversion)) {
      switch (divxversion) {
        case 3:
          GST_DEBUG_OBJECT (viddec, "codec DIVX3");
          viddec->codec.subtype = VIDEO_ENCODING_DIVX3;
          break;
        case 4:
          GST_DEBUG_OBJECT (viddec, "codec DIVX4");
          viddec->codec.subtype = VIDEO_ENCODING_DIVX4;
          break;
        case 5:
          GST_DEBUG_OBJECT (viddec, "codec DIVX5");
          viddec->codec.subtype = VIDEO_ENCODING_DIVX5;
          break;
      }
    }
  } else if (!strcmp (mimetype, "video/x-theora")) {
    GST_DEBUG_OBJECT (viddec, "codec THEORA");
    viddec->codec.type = VIDEO_ENCODING_THEORA;
  } else if (!strcmp (mimetype, "video/x-flash-video")) {
    GST_DEBUG_OBJECT (viddec, "codec FLV1");
    viddec->codec.type = VIDEO_ENCODING_FLV1;
  } else if (!strcmp (mimetype, "video/x-vp3")) {
    GST_DEBUG_OBJECT (viddec, "codec VP3");
    viddec->codec.type = VIDEO_ENCODING_VP3;
  } else if (!strcmp (mimetype, "video/x-vp6")) {
    GST_DEBUG_OBJECT (viddec, "codec VP6");
    viddec->codec.type = VIDEO_ENCODING_VP6;
    viddec->codec.subtype = VIDEO_ENCODING_VP6;
  } else if (!strcmp (mimetype, "video/x-vp6-alpha")) {
    GST_DEBUG_OBJECT (viddec, "codec VP6 alpha");
    viddec->codec.type = VIDEO_ENCODING_VP6;
    viddec->codec.subtype = 5;
  } else if (!strcmp (mimetype, "video/x-vp6-flash")) {
    GST_DEBUG_OBJECT (viddec, "codec VP6 flash");
    viddec->codec.type = VIDEO_ENCODING_VP6;
    viddec->codec.subtype = 4;
  } else if (!strcmp (mimetype, "video/x-vp8")) {
    GST_DEBUG_OBJECT (viddec, "codec VP8");
    viddec->codec.type = VIDEO_ENCODING_VP8;
  } else if (!strcmp (mimetype, "video/x-vp9")) {
    GST_DEBUG_OBJECT (viddec, "codec VP9");
    viddec->codec.type = VIDEO_ENCODING_VP9;
  } else if (!strcmp (mimetype, "video/x-avs")) {
    GST_DEBUG_OBJECT (viddec, "codec avs");
    viddec->codec.type = VIDEO_ENCODING_AVS;
  } else if (!strcmp (mimetype, "video/x-avs-pes")) {
    GST_DEBUG_OBJECT (viddec, "codec avs");
    viddec->codec.type = VIDEO_ENCODING_AVS;
    viddec->is_pes = TRUE;
  } else if (!strcmp (mimetype, "video/x-avsp")) {
    GST_DEBUG_OBJECT (viddec, "codec avsp");
    viddec->codec.type = VIDEO_ENCODING_AVSP;
  } else if (!strcmp (mimetype, "video/x-avsp-pes")) {
    GST_DEBUG_OBJECT (viddec, "codec avsp");
    viddec->codec.type = VIDEO_ENCODING_AVSP;
    viddec->is_pes = TRUE;
  } else if (!strcmp (mimetype, "video/x-wmv")
      || !strcmp (mimetype, "video/x-wmv-pes")) {
    gint wmvversion = 0;
    const GValue *codec_data_value;

    GST_DEBUG_OBJECT (viddec, "codec WMV");

    if (!strcmp (mimetype, "video/x-wmv-pes")) {
      viddec->is_pes = TRUE;
    }

    if (gst_structure_get_int (structure, "wmvversion", &wmvversion)) {
      switch (wmvversion) {
        case 1:
        case 2:
          viddec->codec.type = VIDEO_ENCODING_WMV;
          break;
        case 3:
        {
          gchar *format = NULL;

          /* WMV unless the fourcc exists and says otherwise */
          viddec->codec.type = VIDEO_ENCODING_WMV;
          format = (gchar *) gst_structure_get_string (structure, "format");
          if (format) {
            if (!strcmp (format, "WVC1")) {
              GST_DEBUG_OBJECT (viddec, "codec VC1");
              viddec->codec.type = VIDEO_ENCODING_VC1;
            }
          }
        }
          break;
        default:
          GST_ERROR_OBJECT (viddec, "codec not supported");
          break;
      }
    }

    if ((codec_data_value = gst_structure_get_value (structure, "codec_data"))) {
      GstBuffer *codec_data_buffer;
      GstMapInfo info_write, info_read;
/* REVIEW/FIXME: Just keep a copy to the buffer instead of copying it */
      codec_data_buffer = gst_value_get_buffer (codec_data_value);
      gst_buffer_map (codec_data_buffer, &info_read, GST_MAP_READ);
      viddec->codec_data.buf = gst_buffer_new_and_alloc (info_read.size);
      if (viddec->codec_data.buf != NULL) {
        gst_buffer_map (viddec->codec_data.buf, &info_write, GST_MAP_WRITE);
        memcpy (info_write.data, info_read.data, info_read.size);
        gst_buffer_unmap (viddec->codec_data.buf, &info_write);
      }
      gst_buffer_unmap (codec_data_buffer, &info_read);
    }
  } else {
    GST_ERROR_OBJECT (viddec, "codec %s not supported", mimetype);
  }

  if (viddec->codec.type != VIDEO_ENCODING_NONE) {
    /* Get playback properties */
    if (gst_structure_get_int (structure, "width", &viddec->properties.width)) {
      GST_INFO_OBJECT (viddec, "width %d", viddec->properties.width);
      viddec->capture.width = viddec->properties.width;
    }
    if (gst_structure_get_int (structure, "height", &viddec->properties.height)) {
      GST_INFO_OBJECT (viddec, "height %d", viddec->properties.height);
      viddec->capture.height = viddec->properties.height;
    }

    GST_INFO_OBJECT (viddec, "Decimation - %d", viddec->decimation);
    switch (viddec->decimation) {
      case 1:
        viddec->capture.width = viddec->capture.width / 2;
        viddec->capture.height = viddec->capture.height / 2;
        break;
      case 2:
        viddec->capture.width = viddec->capture.width / 4;
        viddec->capture.height = viddec->capture.height / 2;
        break;
      case 3:
        viddec->capture.width = viddec->capture.width / 2;
        viddec->capture.height = viddec->capture.height / 4;
        break;
      case 4:
        viddec->capture.width = viddec->capture.width / 2;
        viddec->capture.height = viddec->capture.height / 8;
        break;
      case 5:
        viddec->capture.width = viddec->capture.width / 4;
        viddec->capture.height = viddec->capture.height / 4;
        break;
      case 6:
        viddec->capture.width = viddec->capture.width / 4;
        viddec->capture.height = viddec->capture.height / 8;
        break;
      case 7:
        viddec->capture.width = viddec->capture.width / 8;
        viddec->capture.height = viddec->capture.height / 2;
        break;
      case 8:
        viddec->capture.width = viddec->capture.width / 8;
        viddec->capture.height = viddec->capture.height / 4;
        break;
      case 9:
        viddec->capture.width = viddec->capture.width / 8;
        viddec->capture.height = viddec->capture.height / 8;
        break;
      default:
        break;
    }

    if (viddec->capture.width % 16 != 0) {
      viddec->capture.width = (viddec->capture.width / 16 + 1) * 16;
    }
    GST_DEBUG_OBJECT (viddec, "SetCaps - After Decimation (%d, %d)",
        viddec->capture.width, viddec->capture.height);

    if (gst_structure_get_fraction (structure, "framerate",
            &viddec->properties.fps_num, &viddec->properties.fps_den)) {
      GST_INFO_OBJECT (viddec, "framerate num=%d den=%d",
          viddec->properties.fps_num, viddec->properties.fps_den);
    }
    query = gst_query_new_latency ();
    if (query != NULL) {
      GstClockTime min_latency, max_latency;
      if (gst_pad_peer_query (viddec->sinkpad, query)) {
        gst_query_parse_latency (query, &viddec->live, &min_latency,
            &max_latency);
        GST_DEBUG_OBJECT (viddec,
            "Peer latency: live %s, min %" GST_TIME_FORMAT " max %"
            GST_TIME_FORMAT, viddec->live ? "TRUE" : "FALSE",
            GST_TIME_ARGS (min_latency), GST_TIME_ARGS (max_latency));
      }
      gst_query_unref (query);
    }

    GST_DEBUG_OBJECT (viddec, "video encoding %x", viddec->codec.type);

    if ((viddec->properties.fps_num == 0) || (viddec->properties.fps_den == 0)) {
      viddec->properties.fps_num = 0;
      viddec->properties.fps_den = 1;
    }

    if ((viddec->properties.par_num == 0) || (viddec->properties.par_den == 0)) {
      viddec->properties.par_num = 1;
      viddec->properties.par_den = 1;
    }

    const GValue *pixel_aspect_ratio;
    if ((pixel_aspect_ratio =
            gst_structure_get_value (structure, "pixel-aspect-ratio"))) {
      viddec->properties.par_num =
          gst_value_get_fraction_numerator (pixel_aspect_ratio);
      viddec->properties.par_den =
          gst_value_get_fraction_denominator (pixel_aspect_ratio);
    }
  }

  viddec->downstream_frame_pushed = FALSE;

  /* Set start code */
  switch (viddec->codec.type) {
    case VIDEO_ENCODING_WMV:
    case VIDEO_ENCODING_VC1:
    case VIDEO_ENCODING_RMV:
      viddec->codec.start_code = VC1_VIDEO_PES_START_CODE;
      break;
    case VIDEO_ENCODING_H264:
      viddec->codec.start_code = H264_VIDEO_PES_START_CODE;
      break;
    case VIDEO_ENCODING_H263:
    case VIDEO_ENCODING_FLV1:
    case VIDEO_ENCODING_VP6:
    case VIDEO_ENCODING_VP3:
    case VIDEO_ENCODING_VP8:
    case VIDEO_ENCODING_VP9:
      viddec->codec.start_code = H263_VIDEO_PES_START_CODE;
      break;
    case VIDEO_ENCODING_AVS:
    case VIDEO_ENCODING_AVSP:
    case VIDEO_ENCODING_HEVC:
    default:
      viddec->codec.start_code = MPEG_VIDEO_PES_START_CODE;
      break;
  }

  ret = gst_stvideo_manage_caps (viddec);
  viddec->device_config = TRUE;

  return ret;
}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_stvideo_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
  Gststvideo *viddec = GST_STVIDEO (parent);
  GstFlowReturn ret = GST_FLOW_OK;
  video_discontinuity_t video_discontinuity = 0;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);

  /* The manage_id_function waits till the downstream element is connected and opens the
     device corresponding to the downstream element. Also decides if stvideo needs to be in tunnelled
     or non-tunnelled
   */
  if ((viddec->device_config) && (gst_buffer_get_size (buf) > 0)) {
    if (!gst_stvideo_manage_dev_id (viddec)) {
      GST_ERROR_OBJECT (viddec, "error in device id management");
      if (viddec->srcresult != GST_FLOW_OK) {
        GST_ERROR_OBJECT (viddec, "error in data flow %s ",
            gst_flow_get_name (viddec->srcresult));
        ret = viddec->srcresult;
        goto done;
      }
    }
    viddec->device_config = FALSE;
    viddec->channel_change = FALSE;
  }

  if (viddec->channel_change && !viddec->device_config) {
    viddec->channel_change = FALSE;
    /* We have to reset the encoding type, if not SD <-> HD could freeze the display */
    if (!gst_stvideo_ioctl (viddec, VIDEO_SET_ENCODING,
            (void *) viddec->codec.type)) {
      GST_ERROR_OBJECT (viddec, "failed to set encoding");
    }
  }

  if (viddec->device_config == FALSE) {
    /* make it TRUE only if device is configured successfully */
    viddec->first_buffer_received = TRUE;
  }

  /* The resuming condition is for the case when capture task has been paused if srcresult is not GST_FLOW_OK.
   * The capture task needs to be started again if the upstream element recovers and sends the buffers to
   * stvideo. In that case, stvideo may not go through the state transition & capture task won't be started.
   */

  if (GST_PAD_TASK (viddec->srcpad)
      && (gst_task_get_state (GST_PAD_TASK (viddec->srcpad)) ==
          GST_TASK_PAUSED) && !viddec->exit_loop) {
    if (gst_pad_start_task (viddec->srcpad,
            (GstTaskFunction) gst_stvideo_video_loop, viddec, NULL) != TRUE) {
      GST_ERROR_OBJECT (viddec, "cannot start capture task");
    }
  }

  if ((viddec->tunneled == TRUE) && (viddec->new_segment_received == TRUE)) {
    /* Send for tunneled, the Pending New Segment events */
    GST_LOG_OBJECT (viddec, "Pushing NEWSEGMENT events downstream");
    if (FALSE == gst_stvideo_send_queued_events_before (viddec,
            GST_CLOCK_TIME_NONE)) {
      GST_WARNING_OBJECT (viddec,
          "Pushing NEWSEGMENT events downstream failed");
    }
    viddec->new_segment_received = FALSE;
  }

  if (viddec->fd == -1) {
    GST_LOG_OBJECT (viddec, "dropping video data");
    if (viddec->disable_dev == TRUE) {
      g_mutex_lock (&viddec->dummy_buff.mutex);
      GST_STVIDEO_VIDEO_LOOP_SIGNAL (viddec);
      g_mutex_unlock (&viddec->dummy_buff.mutex);
    }
    GST_DEBUG_OBJECT (viddec,
        "video device is not opened, data is being dropped");
    goto done;
  }

  /* push a dummy buffer to basesink */
  if ((viddec->tunneled) &&
      (viddec->properties.width != 0) && (viddec->properties.height != 0)) {
    g_mutex_lock (&viddec->dummy_buff.mutex);
    /* signal the waiting task only if required */
    if (viddec->downstream_frame_pushed == FALSE) {
      GST_LOG_OBJECT (viddec, "signal the video loop task");
      GST_STVIDEO_VIDEO_LOOP_SIGNAL (viddec);
    }
    g_mutex_unlock (&viddec->dummy_buff.mutex);
  }

  /* pause the video frame push task */
  if (viddec->srcresult != GST_FLOW_OK) {
    ret = viddec->srcresult;
    if (viddec->tunneled == FALSE)
      gst_pad_pause_task (viddec->srcpad);
    GST_ERROR_OBJECT (viddec, "error in data flow %s ",
        gst_flow_get_name (viddec->srcresult));
    goto done;
  }

  if (viddec->disable_dev == TRUE) {
    GST_LOG_OBJECT (viddec, "video decoder is disabled, exit chain");
    goto done;
  }

  if (viddec->discontinuity) {
    GST_DEBUG_OBJECT (viddec, "flush caused discontinuity in data flow");
    if (!viddec->tm_control.upstream_rap_index_support) {
      GST_BUFFER_FLAG_SET (buf, GST_BUFFER_FLAG_DISCONT);
    }
    viddec->discontinuity = FALSE;
  }

  g_mutex_lock (&viddec->write_lock);

  /* non ts containers */
  if (viddec->is_pes == FALSE) {
    if (GST_BUFFER_IS_DISCONT (buf) && (viddec->live == FALSE)) {
      if (viddec->segment.rate < 0.0)
        viddec->first_injection = TRUE;
      video_discontinuity = VIDEO_DISCONTINUITY_CONTINUOUS_REVERSE;
      if (gst_stvideo_ioctl (viddec, VIDEO_DISCONTINUITY,
              (void *) video_discontinuity) == FALSE)
        GST_WARNING_OBJECT (viddec, "discontinuity injection failed");
    }
    if (gst_stvideo_injection_control (viddec, buf) == FALSE) {
      GST_ERROR_OBJECT (viddec, "injection failed");
      ret = GST_FLOW_ERROR;
      goto exit;
    }
  } else {
    /* ts container */
    GstMapInfo info_read;
    gst_buffer_map (buf, &info_read, GST_MAP_READ);
    /* FIXME: discontinuity rework (BZ25398) */
    if (viddec->tm_control.upstream_rap_index_support) {
      /* upstream recorder, write last valid frame then discontinuity */
      if (gst_stvideo_write (viddec, info_read.data, info_read.size) == FALSE) {
        gst_buffer_unmap (buf, &info_read);
        GST_ERROR_OBJECT (viddec, "Failed to write ts packet");
        ret = GST_FLOW_FLUSHING;
        goto exit;
      }
      if (GST_BUFFER_IS_DISCONT (buf) && (viddec->live == FALSE)) {
        gst_stvideo_ioctl (viddec, VIDEO_DISCONTINUITY,
            (void *) video_discontinuity);
      }
    } else {
      /* discontinuity set with valid new position (no recorder) */
      /* sttsdemux ts reverse file playback (feature not supported) */
      if (GST_BUFFER_IS_DISCONT (buf) && (viddec->live == FALSE)) {
        video_discontinuity =
            VIDEO_DISCONTINUITY_SURPLUS_DATA |
            VIDEO_DISCONTINUITY_CONTINUOUS_REVERSE;
        gst_stvideo_ioctl (viddec, VIDEO_DISCONTINUITY,
            (void *) video_discontinuity);
      }
      if (gst_stvideo_write (viddec, info_read.data, info_read.size) == FALSE) {
        gst_buffer_unmap (buf, &info_read);
        GST_ERROR_OBJECT (viddec, "Failed to write ts packet");
        ret = GST_FLOW_FLUSHING;
        goto exit;
      }
    }
    gst_buffer_unmap (buf, &info_read);
  }

  if ((viddec->state == VIDEO_FREEZED)
      && (((viddec->flushing_seek == TRUE)
              && (viddec->tm_control.rate_change == FALSE))
          || (viddec->display_first_frame_early == TRUE))) {
    viddec->display_first_frame_early = FALSE;
    gst_stvideo_ioctl (viddec, VIDEO_STEP, NULL);
  }

  /* reset sticky flag on first buffer received after each flushing seek */
  viddec->flushing_seek = FALSE;

  /* Update the last buffer timestamp */
  if (GST_BUFFER_TIMESTAMP_IS_VALID (buf) &&
      ((viddec->last_buffer_ts <
              GST_TIME_AS_MSECONDS (GST_BUFFER_TIMESTAMP (buf)))
          || (viddec->last_buffer_ts == GST_CLOCK_TIME_NONE))) {
    GST_LOG_OBJECT (viddec, "Setting Last TimeStamp to %lld ms",
        GST_TIME_AS_MSECONDS (GST_BUFFER_TIMESTAMP (buf)));
    viddec->last_buffer_ts = GST_TIME_AS_MSECONDS (GST_BUFFER_TIMESTAMP (buf));
  }

exit:
  g_mutex_unlock (&viddec->write_lock);
done:
  gst_buffer_unref (buf);

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);

  return ret;
}

/* change_state function
 * this function does the actual processing
 */
static GstStateChangeReturn
gst_stvideo_change_state (GstElement * element, GstStateChange transition)
{
  Gststvideo *viddec = GST_STVIDEO (element);
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;

  GST_DEBUG_OBJECT (viddec, "> %d -> %d",
      GST_STATE_TRANSITION_CURRENT (transition),
      GST_STATE_TRANSITION_NEXT (transition));

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      g_mutex_init (&viddec->dummy_buff.mutex);
      g_cond_init (&viddec->dummy_buff.cond);
      g_mutex_init (&viddec->write_lock);
      g_mutex_init (&viddec->event_lock);

      viddec->tm_control.reverse_cmp_buffer =
          g_malloc0 (GST_STVIDEO_REVERSE_BUF_CMP_SIZE);

      viddec->tm_control.reverse_tmp_buffer =
          g_malloc0 (GST_STVIDEO_REVERSE_BUF_CMP_SIZE);

      /* Open device */
      if (viddec->v4l2_fd < 0) {
        viddec->v4l2_fd =
            v4l2_open_by_name (V4L2_CAPTURE_DRIVER_NAME, V4L2_CAPTURE_CARD_NAME,
            O_RDWR);

        if (viddec->v4l2_fd < 0) {
          GST_ERROR_OBJECT (viddec,
              "Couldn't open v4l2 device %s - %s", V4L2_CAPTURE_DRIVER_NAME,
              strerror (errno));
          return GST_STATE_CHANGE_FAILURE;
        }
      }

      gst_stvideo_start_monitor_task (viddec);
      viddec->disable_dev = FALSE;
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      gst_segment_init (&viddec->segment, GST_FORMAT_TIME);
      viddec->exit_loop = FALSE;
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      if (viddec->fd >= 0) {
        GST_OBJECT_LOCK (viddec);
        if (viddec->tunneled == TRUE) {
          if (viddec->segment.rate < 0.0) {
            gint speed = viddec->segment.rate * DVB_SPEED_NORMAL_PLAY;
            ret = gst_stvideo_ioctl (viddec, VIDEO_SET_SPEED, (void *) speed);
          } else {
            ret = gst_stvideo_ioctl (viddec, VIDEO_CONTINUE, NULL);
          }
          if (ret)
            viddec->state = VIDEO_PLAYING;
        }
        GST_OBJECT_UNLOCK (viddec);

        if ((viddec->eos == TRUE) && (viddec->eos_set == FALSE)) {
          /* received EOS event in paused state. set video
             eos discontinuity here
           */
          viddec->eos_set = TRUE;
          gst_stvideo_ioctl (viddec, VIDEO_DISCONTINUITY,
              (void *) VIDEO_DISCONTINUITY_EOS);
        }
      }
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      viddec->exit_loop = TRUE;
      if (viddec->tunneled == TRUE) {
        /* Before quit, application should set EOS discontinuity
           on video stream to indicate end of decode so that if
           other applications like transcode are running, they
           can get notified.
           As per bugzilla 53722, SE does not accept discontinuity
           in paused state so we need to set stream in play state
           before setting discontinuity. its a workaround to get
           rid of issue in SE that it does not accept discontinuity
           in paused state.
           once SE rectifies it, VIDEO_CLEAR_BUFFER, VIDEO_PLAY at
           this stage should not be required. */
        GST_FIXME_OBJECT (viddec, "VIDEO_CLEAR_BUFFER ioctl is not required.");
        GST_FIXME_OBJECT (viddec, "VIDEO_PLAY ioctl is not required.");
        /* clear the pes buffer so that we dont see data play after
           pause during quit */
        gst_stvideo_ioctl (viddec, VIDEO_CLEAR_BUFFER, NULL);
        if (gst_stvideo_ioctl (viddec, VIDEO_PLAY, NULL) == TRUE) {
          gst_stvideo_ioctl (viddec, VIDEO_DISCONTINUITY,
              (void *) VIDEO_DISCONTINUITY_EOS);
        }
      }
      if (gst_stvideo_ioctl (viddec, VIDEO_STOP, NULL) == FALSE) {
        GST_ERROR_OBJECT (viddec, "failed to stop video device");
        return GST_STATE_CHANGE_FAILURE;
      } else {
        GST_DEBUG_OBJECT (viddec, "VIDEO_STOP");
        viddec->state = VIDEO_STOPPED;
      }
      if (viddec->tunneled == FALSE) {
        GstTask *task = NULL;
        /* release the video loop task struck in DQBUF ioctl */
        /* in background record, no data will be pushed from
           stts_demux so this task will never get created since
           we create this task in chain on valid data reception
         */
        task = GST_PAD_TASK (viddec->srcpad);
        if (task != NULL) {
          gst_task_pause (task);
        }
        gst_stvideo_ioctl (viddec, VIDEO_DISCONTINUITY,
            (void *) VIDEO_DISCONTINUITY_EOS);
        if (task != NULL) {
          GST_PAD_STREAM_LOCK (viddec->srcpad);
          GST_PAD_STREAM_UNLOCK (viddec->srcpad);
        }
      } else {
        /* signal the waiting task, so as to stop */
        g_mutex_lock (&viddec->dummy_buff.mutex);
        viddec->video_task_state = GST_STVIDEO_TASK_PAUSED;
        GST_STVIDEO_VIDEO_LOOP_SIGNAL (viddec);
        g_mutex_unlock (&viddec->dummy_buff.mutex);
        if (GST_PAD_TASK (viddec->srcpad)
            && (gst_task_get_state (GST_PAD_TASK (viddec->srcpad)) ==
                GST_TASK_STARTED)) {
          gst_pad_pause_task (viddec->srcpad);
        }
      }
      viddec->srcresult = GST_FLOW_OK;
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      if (viddec->tunneled == TRUE) {
        if (viddec->state == VIDEO_PLAYING) {
          gboolean ret = TRUE;
          if (viddec->segment.rate < 0.0)
            ret =
                gst_stvideo_ioctl (viddec, VIDEO_SET_SPEED,
                (void *) DVB_SPEED_REVERSE_STOPPED);
          else
            ret = gst_stvideo_ioctl (viddec, VIDEO_FREEZE, NULL);
          if (ret) {
            GST_DEBUG_OBJECT (viddec, "VIDEO_FREEZE");
            viddec->state = VIDEO_FREEZED;
          } else {
            return GST_STATE_CHANGE_FAILURE;
          }
        }
        viddec->downstream_frame_pushed = FALSE;

        /* push a dummy buffer to basesink */
        g_mutex_lock (&viddec->dummy_buff.mutex);
        GST_STVIDEO_VIDEO_LOOP_SIGNAL (viddec);
        g_mutex_unlock (&viddec->dummy_buff.mutex);

        if (viddec->live && viddec->is_pes) {
          viddec->segment.start =
              MPEGTIME_TO_GSTTIME ((gint64) viddec->currentPTS);
          viddec->segment.time = 0;
        }
      }
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      if (viddec->codec_data.buf != NULL) {
        gst_buffer_unref (viddec->codec_data.buf);
        viddec->codec_data.buf = NULL;
      }
      if (viddec->tunneled == TRUE) {
        /* signal the waiting task, so as to stop */
        g_mutex_lock (&viddec->dummy_buff.mutex);
        viddec->video_task_state = GST_STVIDEO_TASK_STOPPED;
        GST_STVIDEO_VIDEO_LOOP_SIGNAL (viddec);
        g_mutex_unlock (&viddec->dummy_buff.mutex);
      } else {
        if (viddec->capture.is_stream_on_done) {
          if (gst_stvideo_capture_stream_off (viddec, viddec->v4l2_fd)) {
            /* free the Capture Buffer */
            gst_stvideo_capture_free_buffer (viddec, viddec->v4l2_fd);
            viddec->capture.is_stream_on_done = FALSE;
          }
        }
      }
      if (GST_PAD_TASK (viddec->srcpad)
          && (gst_task_get_state (GST_PAD_TASK (viddec->srcpad)) !=
              GST_TASK_STOPPED)) {
        gst_pad_stop_task (viddec->srcpad);
      }
      if (viddec->capture.pool) {
        gst_object_unref (viddec->capture.pool);
        viddec->capture.pool = NULL;
      }
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      gst_stvideo_stop_monitor_task (viddec);
      if (viddec->tm_control.reverse_cmp_buffer) {
        g_free (viddec->tm_control.reverse_cmp_buffer);
        viddec->tm_control.reverse_cmp_buffer = NULL;
      }
      if (viddec->tm_control.reverse_tmp_buffer) {
        g_free (viddec->tm_control.reverse_tmp_buffer);
        viddec->tm_control.reverse_tmp_buffer = NULL;
      }
      /* Close V4L2 device */
      if (viddec->v4l2_fd != -1) {
        close (viddec->v4l2_fd);
        viddec->v4l2_fd = -1;
      }
      gst_stvideo_term_device (viddec);

      g_mutex_clear (&viddec->dummy_buff.mutex);
      g_cond_clear (&viddec->dummy_buff.cond);
      g_mutex_clear (&viddec->write_lock);
      g_mutex_clear (&viddec->event_lock);
      break;
    default:
      break;
  }

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return ret;
}

/*
 * Function name : gst_stvideo_sink_query
 *  Input        :
 *    pad - viddec sink pad
 *    query - query from upstream
 *
 *  Description  : stvideo will reply to requested query.
 *
 * Return        : TRUE if successful else FALSE.
 */
static gboolean
gst_stvideo_sink_pad_query (GstPad * pad, GstObject * parent, GstQuery * query)
{
  Gststvideo *viddec = GST_STVIDEO (parent);
  GstStructure *structure = NULL;
  gboolean res = TRUE;

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CUSTOM:
      structure = (GstStructure *) gst_query_get_structure (query);
      if (structure) {
        if (!strncmp ("current-pts", gst_structure_get_name (structure), 11)) {
          gst_stvideo_ioctl (viddec, VIDEO_GET_PTS,
              (void *) &viddec->currentPTS);
          gst_structure_set (structure, "current_pts", G_TYPE_UINT64,
              viddec->currentPTS, NULL);
          res = TRUE;
        } else {
          res = gst_pad_query_default (pad, parent, query);
        }
      }
      break;
    default:
      res = gst_pad_query_default (pad, parent, query);
      break;
  }

  return res;
}

static gboolean
gst_stvideo_src_pad_query (GstPad * pad, GstObject * parent, GstQuery * query)
{
  Gststvideo *viddec = GST_STVIDEO (parent);
  GstStructure *structure = NULL;
  gboolean res = FALSE;
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);

  GST_LOG_OBJECT (viddec, "%s - (%s)", __FUNCTION__,
      gst_query_type_get_name (GST_QUERY_TYPE (query)));

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CUSTOM:
      /* This is custom query from staudiosink
       * it is required because the position query does not reach here from sink */
      structure = (GstStructure *) gst_query_get_structure (query);
      if (structure) {
        if (!strncmp ("stream-pos", gst_structure_get_name (structure), 11)) {
          gst_structure_set (structure, "stream_pos", G_TYPE_INT64,
              viddec->current_time, NULL);
          res = TRUE;
        } else {
          res = gst_pad_query_default (pad, parent, query);
        }
      }
      break;
    case GST_QUERY_ACCEPT_CAPS:
    {
      GstCaps *caps;
      gst_query_parse_caps (query, &caps);
      res = gst_stvideo_sink_acceptcaps (caps);
      gst_query_set_accept_caps_result (query, res);
      res = TRUE;
      goto done;
    }
      break;

    case GST_QUERY_POSITION:
    {
      GstFormat format;

      gst_query_parse_position (query, &format, NULL);
      if (format == GST_FORMAT_TIME) {
        gst_query_set_position (query, format, viddec->current_time);
        res = TRUE;
      } else {
        res = gst_pad_query_default (pad, parent, query);
      }
    }
      break;
    default:
      /* send to pads which link to this pad to query */
      res = gst_pad_query_default (pad, parent, query);
      break;
  }

  if (res == FALSE) {
    GST_WARNING_OBJECT (viddec, "query %d failed", GST_QUERY_TYPE (query));
  }
  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
done:
  return res;
}

static gboolean
gst_stvideo_src_pad_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
  Gststvideo *viddec = GST_STVIDEO (parent);
  GstFormat format;
  GstSeekFlags flags;
  GstSeekType cur_type, stop_type;
  gint64 cur, stop;
  gdouble rate;
  gboolean ret = TRUE;
  gboolean rate_change = FALSE;

  GST_DEBUG_OBJECT (viddec, "%s - (%s)", __FUNCTION__,
      gst_event_type_get_name (GST_EVENT_TYPE (event)));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_SEEK:
      gst_event_parse_seek (event, &rate, &format, &flags, &cur_type, &cur,
          &stop_type, &stop);

      /* assuming that seek will not change playback direction */
      viddec->direction_change = FALSE;

      /* on speed change get the play info */
      if (rate != viddec->tm_control.set_rate) {
        rate_change = TRUE;
        if (flags & GST_SEEK_FLAG_SKIP) {
          /* skip flag - set only rate seek */
          viddec->skipflag = TRUE;
        } else {
          viddec->skipflag = FALSE;
        }
      }

      viddec->tm_control.rate_change = rate_change;

      /* set direction_change flag to actual */
      if (((rate > 0) && (viddec->tm_control.set_rate < 0))
          || ((rate < 0) && (viddec->tm_control.set_rate > 0))) {
        viddec->direction_change = TRUE;
      }

      viddec->rate_pending = TRUE;
      /* Update to the new rate */
      viddec->tm_control.set_rate = rate;
      viddec->tm_control.reverse_support = TRUE;
      ret = gst_pad_event_default (pad, parent, event);
      break;
    case GST_EVENT_CUSTOM_UPSTREAM:
    {
      const GstStructure *structure;
      structure = gst_event_get_structure (event);
      if (gst_structure_has_name (structure, "re-configure")) {
        g_mutex_lock (&viddec->write_lock);
        gst_stvideo_close_device (viddec);
        g_mutex_unlock (&viddec->write_lock);
        viddec->device_config = TRUE;
        viddec->dev_id = DEFAULT_DEV_ID;
        gst_event_unref (event);
        ret = TRUE;
      } else if (gst_structure_has_name (structure, "request-buffer")) {
        g_mutex_lock (&viddec->dummy_buff.mutex);
        viddec->downstream_frame_pushed = FALSE;
        GST_STVIDEO_VIDEO_LOOP_SIGNAL (viddec);
        g_mutex_unlock (&viddec->dummy_buff.mutex);
        gst_event_unref (event);
        ret = TRUE;
      } else {
        if (gst_structure_has_name (structure, "catch-live")) {
          viddec->skipflag = FALSE;
          viddec->catchlive = TRUE;
        }
        ret = gst_pad_event_default (pad, parent, event);
      }
    }
      break;
    default:
      /* Push event upstream if needed */
      ret = gst_pad_event_default (pad, parent, event);
      break;
  }

  if (ret == FALSE) {
    GST_LOG_OBJECT (viddec, "event %s failed",
        gst_event_type_get_name (GST_EVENT_TYPE (event)));
  }
  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return ret;
}

static gboolean
gst_stvideo_sink_pad_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
  Gststvideo *viddec = GST_STVIDEO (parent);
  const GstStructure *structure;
  GstFormat format;
  guint64 start;
  guint64 stop;
  guint64 position;
  gint64 time;
  gdouble rate;
  gboolean ret = TRUE;
  const GstSegment *segment;
  GstCaps *caps;
  gboolean force_pass_event = FALSE;
  video_play_interval_t VideoPlayInterval;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);

  GST_DEBUG_OBJECT (viddec, "%s - (%s)", __FUNCTION__,
      gst_event_type_get_name (GST_EVENT_TYPE (event)));


  /* Drop flush start/stop events arriving due to demuxers initiated seeks */
  /* e.g. ogg demuxer initiated seeks in streaming usecases like http */
  /* FIXME: its a work around, can be removed if demuxers are rectified */
  if (viddec->first_buffer_received == FALSE) {
    switch (GST_EVENT_TYPE (event)) {
        /* flush events during pipeline creation are not expected */
      case GST_EVENT_FLUSH_START:
        GST_DEBUG_OBJECT (viddec, "drop flush start event");
        gst_event_unref (event);
        goto done;

      case GST_EVENT_FLUSH_STOP:
        GST_DEBUG_OBJECT (viddec, "drop flush stop event");
        gst_event_unref (event);
        goto done;

      default:
        break;
        /* do nothing, we will process events as usual */
    }
  }

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_EOS:
      /* Signal video loop to avoid hang when EOS is received */
      /* demuxer sends EOS when we jump to 0 in reverse trickmode */
      if (viddec->flushing_seek) {
        g_mutex_lock (&viddec->dummy_buff.mutex);
        GST_STVIDEO_VIDEO_LOOP_SIGNAL (viddec);
        g_mutex_unlock (&viddec->dummy_buff.mutex);
      }

      viddec->eos = TRUE;

      if (viddec->disable_dev) {
        ret = gst_pad_event_default (pad, parent, event);
      } else {
        /* SE accepts doscontinuity only in playing state */
        if (viddec->state == VIDEO_PLAYING) {
          viddec->eos_set = TRUE;
          gst_stvideo_ioctl (viddec, VIDEO_DISCONTINUITY,
              (void *) VIDEO_DISCONTINUITY_EOS);
        }
        gst_event_unref (event);
      }

      goto done;
      break;

    case GST_EVENT_SEGMENT:
      gst_event_parse_segment (event, &segment);
      /* REVIEW/PERF: Why use extra local values instead of just using the
       * segment fields directly ? */
      rate = segment->rate;
      format = segment->format;
      start = segment->start;
      stop = segment->stop;
      position = segment->position;
      time = segment->time;

      /* If new rate is different from current rate */
      if (gst_stvideo_set_speed (viddec, rate, TRUE) == FALSE) {
        GST_ERROR_OBJECT (viddec, "failed to set rate %f", rate);
        ret = FALSE;
      }

      if (format == GST_FORMAT_TIME) {
        GST_DEBUG_OBJECT (viddec, "New segment rate %f "
            "start %" GST_TIME_FORMAT " stop %" GST_TIME_FORMAT
            " position %" GST_TIME_FORMAT, rate,
            GST_TIME_ARGS (start), GST_TIME_ARGS (stop),
            GST_TIME_ARGS (position));
      } else {
        GST_DEBUG_OBJECT (viddec, "New segment format %d"
            "rate %f start %" G_GUINT64_FORMAT " stop %" G_GUINT64_FORMAT
            " position %" G_GUINT64_FORMAT,
            format, rate, start, stop, position);
        format = GST_FORMAT_TIME;
        start = 0;
        stop = GST_CLOCK_TIME_NONE;
        position = 0;
      }

      /* apply it here if data is flushed, otherwise in update time */
      viddec->update_segment = TRUE;
      if ((viddec->tunneled && (viddec->flushing_seek || !viddec->ptswrap))
          || !viddec->first_buffer_received) {
        viddec->update_segment = FALSE;
        if (!viddec->catchlive) {
          gst_segment_copy_into (segment, &viddec->segment);
          viddec->segment.start = start;
          viddec->segment.format = format;
          viddec->segment.stop = stop;
          viddec->segment.position = position;
          viddec->segment.time = time;
          if (!GST_CLOCK_TIME_IS_VALID (viddec->segment.time)) {
            viddec->segment.time = viddec->current_time;
          }
        }
        viddec->catchlive = FALSE;
      }
      viddec->segment.rate = rate;

      if (GST_CLOCK_TIME_IS_VALID (stop)) {
        stop = GSTTIME_TO_MPEGTIME (stop);
      } else {
        stop = DVB_TIME_NOT_BOUNDED;
      }

      /* set the segment play interval on SE */
      memset (&VideoPlayInterval, 0, sizeof (VideoPlayInterval));
      /* use currentPTS instead of segment start in case of speed change */
      /* do not use currentPTS in case of forward catch live & direct catch live */
      if ((viddec->tm_control.set_rate > 0.0)
          && (viddec->tm_control.rate_change == TRUE)
          && (viddec->flushing_seek == TRUE)
          && (viddec->ptswrap == FALSE)) {
        viddec->currentPTS = 0;
        gst_stvideo_update_time (viddec);
        VideoPlayInterval.start = viddec->currentPTS;
        VideoPlayInterval.end = (gint64) stop;
      } else {
        if (viddec->tm_control.set_rate > 0.0) {
          /* Forward trickmode play interval setting */
          VideoPlayInterval.start = GSTTIME_TO_MPEGTIME (start);
          VideoPlayInterval.end = (gint64) stop;
        } else {
          /* FIXME: use 'stop' to set bounds of VideoPlayInterval */
          /* should be corrected after TF modification done */
          VideoPlayInterval.start = DVB_TIME_NOT_BOUNDED;
          VideoPlayInterval.end = DVB_TIME_NOT_BOUNDED;
        }
      }
      /* DVB_TIME_NOT_BOUNDED            0xfedcba9876543210ULL */
      GST_DEBUG_OBJECT (viddec,
          "set play interval on streaming engine start=%llu, end=%llu",
          VideoPlayInterval.start, VideoPlayInterval.end);

      if (gst_stvideo_ioctl (viddec, VIDEO_SET_PLAY_INTERVAL,
              (void *) &VideoPlayInterval) == FALSE) {
        GST_WARNING_OBJECT (viddec, "set play interval failed: %s\n",
            strerror (errno));
      }
      viddec->ptswrap = FALSE;

      if ((viddec->first_new_segment == TRUE)) {
        GST_DEBUG_OBJECT (viddec, "pushing downstream first NewSegment event");
        ret = gst_pad_event_default (pad, parent, event);
        viddec->first_new_segment = FALSE;
        viddec->new_segment_received = FALSE;
        goto done;
      } else {
        /* Queue the NewSegment Event if Pad Caps are not yet set */
        if (gst_pad_get_current_caps (viddec->srcpad) == NULL) {
          GST_DEBUG_OBJECT (viddec,
              "Queueing NewSegment event with TimeStamp %lld ms",
              viddec->last_buffer_ts);
          gst_stvideo_queue_event (viddec, event, viddec->last_buffer_ts);
          gst_event_unref (event);
          viddec->new_segment_received = TRUE;
          goto done;
        }
      }
      break;

    case GST_EVENT_FLUSH_START:
      ret = gst_pad_event_default (pad, parent, event);
      /* Pause the capture/srcpad task if video decoder element is non-tunneled */
      if (viddec->tunneled == FALSE) {
        gst_pad_pause_task (viddec->srcpad);
      }

      /* during migration to 1.0 from 0.10, GST_FLOW_WRONG_STATE should be
         replaced by GST_FLOW_FLUSHING
       */
      viddec->srcresult = GST_FLOW_FLUSHING;
      if (!gst_stvideo_ioctl (viddec, VIDEO_CLEAR_BUFFER, NULL)) {
        GST_WARNING_OBJECT (viddec, "failed to clear PES buffer");
      }

      goto done;
      break;

    case GST_EVENT_FLUSH_STOP:

      viddec->srcresult = GST_FLOW_OK;
      viddec->downstream_frame_pushed = FALSE;
      viddec->first_injection = TRUE;
      viddec->discontinuity = TRUE;
      viddec->eos = FALSE;
      viddec->eos_set = FALSE;
      viddec->flushing_seek = TRUE;     /* reset only in chain function */
      /* Flush the Queued Events and invalidate the last buffer time */
      viddec->last_buffer_ts = GST_CLOCK_TIME_NONE;
      gst_stvideo_drop_queued_events (viddec);
      force_pass_event = TRUE;

      break;
    case GST_EVENT_CAPS:
      gst_event_parse_caps (event, &caps);
      if ((viddec->prev_caps != NULL)
          && (gst_caps_is_equal ((const GstCaps *) viddec->prev_caps,
                  (const GstCaps *) caps))) {
        GST_DEBUG_OBJECT (viddec, "caps are already set");
        return TRUE;
      }

      viddec->first_injection = TRUE;

      ret = gst_stvideo_set_caps (viddec, caps);
      if (ret) {
        if (viddec->prev_caps) {
          gst_caps_unref (viddec->prev_caps);
          viddec->prev_caps = NULL;
        }
        viddec->prev_caps = gst_caps_ref (caps);
      } else {
        GST_ERROR_OBJECT (viddec, "failed to set caps!");
      }

      gst_event_unref (event);
      goto done;
    case GST_EVENT_CUSTOM_DOWNSTREAM:
    {
      structure = gst_event_get_structure (event);
      if (gst_structure_has_name (structure, "pts-wrap")) {
        GST_DEBUG_OBJECT (viddec, "received pts wrap event");
        viddec->ptswrap = TRUE;
        gst_event_unref (event);
        goto done;
      }
    }
      break;

    case GST_EVENT_CUSTOM_DOWNSTREAM_OOB:
    {
      structure = gst_event_get_structure (event);
      if (gst_structure_has_name (structure, "pcr-data")) {
        dvb_clock_data_point_t clock_data;
        const GValue *source_time;
        const GValue *system_time;

        source_time = gst_structure_get_value (structure, "source-time");
        system_time = gst_structure_get_value (structure, "system-time");
        clock_data.source_time = g_value_get_uint64 (source_time);
        clock_data.system_time = g_value_get_uint64 (system_time);
        clock_data.flags = viddec->clock_data_flags | DVB_CLOCK_FORMAT_PTS;
        if ((viddec->state == VIDEO_PLAYING)
            && (viddec->tm_control.rate == 1.0)) {
          GST_DEBUG_OBJECT (viddec, "VIDEO_SET_CLOCK_DATA_POINT (%llu, %llu)",
              clock_data.source_time, clock_data.system_time);
          if (gst_stvideo_ioctl (viddec, VIDEO_SET_CLOCK_DATA_POINT,
                  (void *) &clock_data) == FALSE) {
            GST_WARNING_OBJECT (viddec,
                "unable to set clock data point (%llu, %llu)",
                clock_data.source_time, clock_data.system_time);
          }
          viddec->clock_data_flags = 0;
        } else {
          GST_DEBUG_OBJECT (viddec, "dropping PCR data (%llu, %llu)",
              clock_data.source_time, clock_data.system_time);
          /* Inform upstream element, that pcr-data event has not been handled */
          ret = FALSE;
        }
        gst_event_unref (event);
        goto done;
      } else if (gst_structure_has_name (structure, "index-support")) {
        gboolean timeshift_playback = FALSE;
        /* timeshift mode - upstream recorder */
        gst_structure_get_boolean (structure, "rap-index",
            &viddec->tm_control.upstream_rap_index_support);
        gst_structure_get_boolean (structure, "timeshift-playback",
            &timeshift_playback);
        if (timeshift_playback == TRUE) {
          viddec->live = FALSE;
          gst_stvideo_set_file_play_policies (viddec);
        } else {
          viddec->live = TRUE;
          gst_stvideo_set_live_play_policies (viddec);
        }
      }
      /* spts caching on rdk server */
      else if (gst_structure_has_name (structure, "cache")) {
        gboolean cached = FALSE;
        gst_structure_get_boolean (structure, "cache", &cached);
        if (FALSE == cached) {
          gst_stvideo_ioctl (viddec, VIDEO_SET_ENCODING,
              (void *) viddec->codec.type);
          gst_stvideo_set_file_play_policies (viddec);
          GST_INFO_OBJECT (viddec,
              "cached disabled, VIDEO_SET_ENCODING & non live file play policies set");
        } else {
          GST_WARNING_OBJECT (viddec,
              "cached enabled, no SE policy modified (previous policy retained)");
        }
      } else if (gst_structure_has_name (structure, "hwdemux-connection")) {
        gst_structure_get_boolean (structure, "channel_change",
            &viddec->channel_change);
      }
    }
      break;
    case GST_EVENT_STREAM_START:
      ret = gst_pad_event_default (pad, parent, event);
      goto done;
      break;
    default:
      break;
  }

  /* Pass event downstream if video decoder element is tunneled or event is not-serialized
     or Src Pad Caps are not yet set or force_pass_event flag is set. Else Queue the Serialized Event */
  if ((force_pass_event == FALSE)
      && (gst_pad_get_current_caps (viddec->srcpad) != NULL)
      && (FALSE == viddec->tunneled) && GST_EVENT_IS_SERIALIZED (event)) {
    GST_DEBUG_OBJECT (viddec,
        "Queueing Serialized event %s with TimeStamp %lld ms",
        GST_EVENT_TYPE_NAME (event), viddec->last_buffer_ts);
    gst_stvideo_queue_event (viddec, event, viddec->last_buffer_ts);
    gst_event_unref (event);
  } else {
    /* Push event downstream */
    ret = gst_pad_event_default (pad, parent, event);
  }

done:
  if (ret == FALSE) {
    GST_LOG_OBJECT (viddec, "event %s failed",
        gst_event_type_get_name (GST_EVENT_TYPE (event)));
  }
  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return ret;
}

/* initialize the stvideo's class */
static void
gst_stvideo_class_init (GststvideoClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  gobject_class->set_property = gst_stvideo_set_property;
  gobject_class->get_property = gst_stvideo_get_property;

  g_object_class_install_property (gobject_class, PROP_DISABLE_AVSYNC,
      g_param_spec_boolean ("dis-avsync", "disable av sync",
          "Whether to enable/disable av sync",
          DEFAULT_DISABLE_AVSYNC, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_DISABLE_INJECT,
      g_param_spec_boolean ("dis-inj", "disable video inject",
          "Whether to enable/disable video injection",
          DEFAULT_DISABLE_INJ, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_DEV_ID,
      g_param_spec_int ("dev-id", "device id for video",
          "device id for video", MIN_DEV_ID, MAX_DEV_ID, DEFAULT_DEV_ID,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  g_object_class_install_property (gobject_class, PROP_CURRENTPTS,
      g_param_spec_uint64 ("currentPTS", "PTS value",
          "get the current PTS value",
          MIN_VALUE_CURRENTPTS, MAX_VALUE_CURRENTPTS,
          DEFAULT_CURRENTPTS, G_PARAM_READABLE));

  g_object_class_install_property (gobject_class, PROP_CONTENTFRAMERATE,
      g_param_spec_int ("contentframerate", "contentframerate",
          "Frame rate of the video displayed",
          MIN_VALUE_CONTENTFRAMERATE, MAX_VALUE_CONTENTFRAMERATE,
          DEFAULT_CONTENTFRAMERATE, G_PARAM_READABLE));

  g_object_class_install_property (gobject_class, PROP_LATENCY,
      g_param_spec_uint ("latency", "latency time (milli seconds)",
          "latency for video", MIN_LATENCY, MAX_LATENCY, DEFAULT_LATENCY,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  g_object_class_install_property (gobject_class, PROP_STEPFRAME,
      g_param_spec_boolean ("stepframe", "step functionality",
          "step functionality whenevr ioctl is called", FALSE,
          G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  g_object_class_install_property (gobject_class, PROP_MEM_PROFILE,
      g_param_spec_uint ("mem-profile", "memory profile",
          "memory profile used for video decoding in SE", MIN_MEM_PROFILE,
          MAX_MEM_PROFILE, DEFAULT_MEM_PROFILE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_INTERLACE,
      g_param_spec_boolean ("interlace", "interlace",
          "interlace processing used for video decoding in SE",
          DEFAULT_INTERLACE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_DECODER_DECIMATION,
      g_param_spec_uint ("decoder-decimation", "video decoder decimation",
          "deimation used for video decoding in SE",
          MIN_DECODER_DECIMATION, MAX_DECODER_DECIMATION,
          DEFAULT_DECODER_DECIMATION,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  gstelement_class->change_state = gst_stvideo_change_state;
  gst_element_class_set_static_metadata (gstelement_class,
      "ST Video Decoders", "Codec/Decoder/Video/Parser",
      "GStreamer Video Decoders Element for ST", "http://www.st.com");
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&src_factory));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&sink_factory));
  GST_LOG (" < %s", __FUNCTION__);
  return;
}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
static void
gst_stvideo_init (Gststvideo * viddec)
{
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  gst_stvideo_reset (viddec);
  gst_segment_init (&viddec->segment, GST_FORMAT_TIME);
  viddec->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
  gst_pad_set_chain_function (viddec->sinkpad,
      GST_DEBUG_FUNCPTR (gst_stvideo_chain));
  gst_pad_set_event_function (viddec->sinkpad,
      GST_DEBUG_FUNCPTR (gst_stvideo_sink_pad_event));
  gst_pad_set_query_function (viddec->sinkpad,
      GST_DEBUG_FUNCPTR (gst_stvideo_sink_pad_query));
  viddec->srcpad = gst_pad_new_from_static_template (&src_factory, "src");
  gst_pad_set_event_function (viddec->srcpad,
      GST_DEBUG_FUNCPTR (gst_stvideo_src_pad_event));
  gst_pad_set_query_function (viddec->srcpad,
      GST_DEBUG_FUNCPTR (gst_stvideo_src_pad_query));

  gst_pad_use_fixed_caps (viddec->srcpad);
  gst_element_add_pad (GST_ELEMENT (viddec), viddec->sinkpad);
  gst_element_add_pad (GST_ELEMENT (viddec), viddec->srcpad);


  viddec->dev_id = DEFAULT_DEV_ID;
  viddec->dev_id_prop = DEFAULT_DEV_ID;
  viddec->device_config = TRUE;

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);

  return;
}

static void
gst_stvideo_reset (Gststvideo * viddec)
{
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  viddec->fd = -1;
  viddec->v4l2_fd = -1;
  viddec->prev_codec = -1;
  viddec->codec.type = VIDEO_ENCODING_NONE;
  viddec->codec.subtype = VIDEO_ENCODING_NONE;
  viddec->codec.start_code = MPEG_VIDEO_PES_START_CODE;
  viddec->state = VIDEO_STOPPED;
  viddec->downstream_frame_pushed = FALSE;
  viddec->first_injection = TRUE;
  viddec->disable_avsync = FALSE;
  viddec->properties.width = 0;
  viddec->properties.height = 0;
  viddec->properties.fps_num = 0;
  viddec->properties.fps_den = 1;
  viddec->properties.par_num = 1;
  viddec->properties.par_den = 1;
  viddec->is_pes = FALSE;
  viddec->live = FALSE;
  viddec->rate_pending = FALSE;
  viddec->prev_video_pts = INVALID_PTS_VALUE;
  viddec->currentPTS = 0;
  viddec->prevPTS = 0;
  viddec->current_time = 0;
  viddec->latency = 0;
  viddec->mem_profile = G_MAXUINT;
  viddec->tm_control.time_skip = GST_CLOCK_TIME_NONE;
  viddec->tm_control.time_base = GST_CLOCK_TIME_NONE;
  viddec->tm_control.rate = 1.0;
  viddec->tm_control.set_rate = viddec->tm_control.rate;
  viddec->tm_control.rate_change = FALSE;
  viddec->tm_control.enable = FALSE;
  viddec->tm_control.reverse_support = FALSE;
  viddec->tm_control.bos = FALSE;
  viddec->tm_control.upstream_rap_index_support = FALSE;
  viddec->skipflag = FALSE;
  viddec->decimation = -1;
  memset (&viddec->codec_data, 0, sizeof (viddec->codec_data));

  viddec->tm_control.reverse_cur_num = 0;

  viddec->tunneled = FALSE;
  viddec->last_buffer_ts = GST_CLOCK_TIME_NONE;
  viddec->queued_events_list = NULL;

  viddec->capture.buffer = NULL;
  viddec->capture.bpp = DEFAULT_BPP;
  viddec->capture.pixel_format = DEFAULT_PIXEL_FORMAT;
  viddec->capture.pool = NULL;
  viddec->capture.task_start_delayed = FALSE;
  viddec->capture.task_start_wait_for_size = FALSE;
  viddec->capture.task_start_wait_for_framerate = FALSE;
  viddec->capture.mem_type = GST_STVIDEO_INVALID;
  viddec->capture.buf_start_address = NULL;
  viddec->capture.is_stream_on_done = FALSE;
  viddec->capture.width = 0;
  viddec->capture.height = 0;

  viddec->srcresult = GST_FLOW_OK;
  viddec->discontinuity = FALSE;
  viddec->prev_caps = NULL;
  viddec->eos = FALSE;
  viddec->eos_set = FALSE;
  viddec->new_segment_received = FALSE;
  viddec->first_new_segment = TRUE;
  viddec->first_buffer_received = FALSE;
  viddec->video_task_state = GST_STVIDEO_TASK_IDLE;
  viddec->flushing_seek = FALSE;
  viddec->display_first_frame_early = TRUE;
  viddec->direction_change = FALSE;
  viddec->property_disable_dev = FALSE;
  viddec->contentframerate = DEFAULT_CONTENTFRAMERATE;
  viddec->interlace = DEFAULT_INTERLACE;
  viddec->prev_start = DVB_TIME_NOT_BOUNDED;
  viddec->update_segment = FALSE;
  viddec->ptswrap = FALSE;
  viddec->channel_change = FALSE;
  viddec->catchlive = FALSE;
}

static void
gst_stvideo_resize_pip_window (Gststvideo * viddec)
{
  int fd_overlay;
  int fd_output;
  struct v4l2_format output_win;
  struct v4l2_cropcap cropcap;
  struct v4l2_control control;
  char device_name[16];
  gchar plane_name[16];
  guint plane_width, plane_height;
  gint ret = -1;
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  /* Open device for overlay, io window */
  fd_overlay =
      v4l2_open_by_name (V4L2_OVERLAY_DRIVER_NAME, V4L2_OVERLAY_CARD_NAME,
      O_RDWR);
  if (fd_overlay < 0) {
    GST_ERROR_OBJECT (viddec, "failed to open device %s for overlay - %s",
        V4L2_OVERLAY_DRIVER_NAME, strerror (errno));
    return;
  }


  /* Open device for outputs, to get current resolution */
  fd_output =
      v4l2_open_by_name (V4L2_OUTPUT_DRIVER_NAME, V4L2_OUTPUT_CARD_NAME,
      O_RDWR);
  if (fd_output < 0) {
    GST_ERROR_OBJECT (viddec, "failed to open device %s for overlay - %s",
        V4L2_OUTPUT_DRIVER_NAME, strerror (errno));
    close (fd_overlay);
    return;
  }

  g_snprintf (device_name, sizeof (device_name), "dvb0.video%d", 1);
  ret = v4l2_set_input_by_name (fd_overlay, device_name);
  if (ret < 0) {
    GST_ERROR_OBJECT (viddec, "failed to set %s to input %s ", device_name,
        strerror (errno));
    goto done;
  }

  /* Set manual mode for resizing PIP window (input window remains in auto mode) */
  control.id = V4L2_CID_STM_INPUT_WINDOW_MODE;
  control.value = VCSPLANE_AUTO_MODE;
  if (ioctl (fd_overlay, VIDIOC_S_CTRL, &control) != 0) {
    GST_ERROR_OBJECT (viddec,
        "VIDIOC_S_CTRL(V4L2_CID_STM_INPUT_WINDOW_MODE) failed (%s).\n",
        strerror (errno));
    goto done;
  }

  control.id = V4L2_CID_STM_OUTPUT_WINDOW_MODE;
  control.value = VCSPLANE_MANUAL_MODE;
  if (ioctl (fd_overlay, VIDIOC_S_CTRL, &control) != 0) {
    GST_ERROR_OBJECT (viddec,
        "VIDIOC_S_CTRL(V4L2_CID_STM_OUTPUT_WINDOW_MODE) failed (%s).\n",
        strerror (errno));
    goto done;
  }

  /* Check if set mode was successfull */
  control.id = V4L2_CID_STM_INPUT_WINDOW_MODE;
  if (ioctl (fd_overlay, VIDIOC_G_CTRL, &control) != 0) {
    GST_ERROR_OBJECT (viddec,
        "VIDIOC_G_CTRL(V4L2_CID_STM_INPUT_WINDOW_MODE) failed (%s).\n",
        strerror (errno));
    goto done;
  }
  if (control.value != VCSPLANE_AUTO_MODE) {
    GST_ERROR_OBJECT (viddec,
        "Input Window mode has not been set properly %s!!\n", strerror (errno));
    goto done;
  }

  control.id = V4L2_CID_STM_OUTPUT_WINDOW_MODE;
  if (ioctl (fd_overlay, VIDIOC_G_CTRL, &control) != 0) {
    GST_ERROR_OBJECT (viddec,
        "VIDIOC_G_CTRL(V4L2_CID_STM_OUTPUT_WINDOW_MODE) failed (%s).\n",
        strerror (errno));
    goto done;
  }
  if (control.value != VCSPLANE_MANUAL_MODE) {
    GST_ERROR_OBJECT (viddec,
        "Output Window mode has not been set properly !!\n");
    goto done;
  }

  /* Use Output to retrieve current resolution and resize/position accordingly */
  g_snprintf (plane_name, sizeof (plane_name), "Main-PIP");

  ret = v4l2_set_output_by_name (fd_output, plane_name);
  if (ret < 0) {
    GST_ERROR_OBJECT (viddec, "failed to set output : %s \n", strerror (errno));
    goto done;
  }

  /* Get current output resolution to compute PIP window size/position */
  memset (&cropcap, 0, sizeof (cropcap));
  cropcap.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

  if (ioctl (fd_output, VIDIOC_CROPCAP, &cropcap) != 0) {
    GST_ERROR_OBJECT (viddec, "VIDIOC_CROPCAP failed (%s).\n",
        strerror (errno));
    goto done;
  }

  plane_width = cropcap.bounds.width;
  plane_height = cropcap.bounds.height;

  output_win.type = V4L2_BUF_TYPE_VIDEO_OVERLAY;
  output_win.fmt.win.w.left = (3 * plane_width / 4) - (plane_width / 30);
  output_win.fmt.win.w.top = plane_height / 30;
  output_win.fmt.win.w.width = plane_width / 4;
  output_win.fmt.win.w.height = plane_height / 4;

  /* Finally set PIP window size/position */
  if (ioctl (fd_overlay, VIDIOC_S_FMT, &output_win) != 0) {
    GST_ERROR_OBJECT (viddec, "VIDIOC_S_FMT failed (%s).\n", strerror (errno));
    goto done;
  }

done:
  close (fd_overlay);
  close (fd_output);
  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return;
}

static gboolean
gst_stvideo_open_device (Gststvideo * viddec)
{
  gchar *fn;
  unsigned int sync_id;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);

  if ((viddec->fd == -1) && (viddec->disable_dev == FALSE)) {
    fn = g_strdup_printf ("/dev/dvb/adapter0/video%d", viddec->dev_id);
    viddec->fd = open (fn, O_RDWR);
    if (viddec->fd < 0) {
      GST_ERROR_OBJECT (viddec, "failed to open video device %s - %s %i", fn,
          strerror (errno), errno);
      g_free (fn);
      return FALSE;
    }
    GST_INFO_OBJECT (viddec, "device %s has been opened", fn);
    g_free (fn);

    /* Set synchronization group: synchronize this video device to audio device of same ID */
    sync_id = VIDEO_SYNC_GROUP_AUDIO | viddec->dev_id;
    if (ioctl (viddec->fd, VIDEO_SET_SYNC_GROUP, (void *) sync_id) != 0) {
      GST_ERROR_OBJECT (viddec, "VIDEO_SET_SYNC_GROUP(id=%d) failed",
          viddec->fd);
    }

    /* Pause the decoder */
    if ((viddec->live == FALSE)
        && (gst_stvideo_ioctl (viddec, VIDEO_FREEZE, NULL) == TRUE)) {
      GST_DEBUG_OBJECT (viddec, "VIDEO_FREEZE");
      viddec->state = VIDEO_FREEZED;
    }
    viddec->tm_control.time_skip = GST_CLOCK_TIME_NONE;
    if (gst_stvideo_apply_set_speed (viddec) == TRUE) {
      GST_WARNING_OBJECT (viddec, "failed to start trickmode control loop");
    }
  }

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return TRUE;
}

static gboolean
gst_stvideo_close_device (Gststvideo * viddec)
{
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);

  /* Stop Video device */
  if (viddec->state != VIDEO_STOPPED) {
    if (gst_stvideo_ioctl (viddec, VIDEO_STOP, NULL) == TRUE) {
      viddec->state = VIDEO_STOPPED;
    } else {
      GST_WARNING_OBJECT (viddec, "failed to stop video device");
    }
  }

  /* Close Video device */
  if (viddec->fd != -1) {
    close (viddec->fd);
    viddec->fd = -1;
  }

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return TRUE;
}

static gboolean
gst_stvideo_term_device (Gststvideo * viddec)
{
  struct video_command video_cmd;

  GST_LOG_OBJECT (viddec, "%s:%d", __FUNCTION__, __LINE__);

  GST_INFO_OBJECT (viddec, "Disable avsync");
  memset (&video_cmd, 0, sizeof (struct video_command));
  video_cmd.cmd = VIDEO_CMD_SET_OPTION;
  video_cmd.option.option = DVB_OPTION_AV_SYNC;
  video_cmd.option.value = DVB_OPTION_VALUE_DISABLE;
  gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd);

  gst_stvideo_close_device (viddec);

  if (viddec->codec_data.buf) {
    gst_buffer_unref (viddec->codec_data.buf);
    viddec->codec_data.buf = NULL;
  }

  gst_stvideo_drop_queued_events (viddec);
  gst_stvideo_reset (viddec);
  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  /* after term, we can consider device to be disabled */
  viddec->disable_dev = TRUE;
  /* also don't configure, when the device is disabled */
  viddec->device_config = FALSE;

  return TRUE;
}

static void
gst_stvideo_setup_device (Gststvideo * viddec)
{
  GstQuery *query;
  GstStructure *structure;
  struct video_command video_cmd;
  gboolean ret = TRUE;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);

  if (!gst_stvideo_ioctl (viddec, VIDEO_SELECT_SOURCE,
          (void *) VIDEO_SOURCE_MEMORY)) {
    GST_WARNING_OBJECT (viddec, "failed to select video source as memory");
  }

  /* Set live policies */
  if (viddec->live == TRUE) {
    gst_stvideo_set_live_play_policies (viddec);
    /* We need to set a playback latency to compensate latency brought by gstreamer pipeline */
    /* The time for PES packets to come from source element to here has been measured to be max 150ms */
    /* So we apply this latency, unless it has been forced by user */
    if (viddec->latency < DEFAULT_LIVE_LATENCY) {
      viddec->latency = DEFAULT_LIVE_LATENCY;
      GST_INFO_OBJECT (viddec,
          "Forcing playback latency to %dms in case of live!!",
          DEFAULT_LIVE_LATENCY);
    }
  }

  if (viddec->latency != 0) {
    memset (&video_cmd, 0, sizeof (struct video_command));
    video_cmd.cmd = VIDEO_CMD_SET_OPTION;
    video_cmd.option.option = DVB_OPTION_CTRL_PLAYBACK_LATENCY;
    video_cmd.option.value = viddec->latency;
    if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
      GST_WARNING_OBJECT (viddec, "failed to set playback latency");
    }
    GST_DEBUG_OBJECT (viddec, "<DVB_OPTION_CTRL_PLAYBACK_LATENCY : %d>",
        viddec->latency);
  }

  /* Check if upstream supports smooth reverse */
  structure = gst_structure_new ("reverse-support",
      "smooth-reverse", G_TYPE_BOOLEAN, FALSE, NULL);
  query = gst_query_new_custom (GST_QUERY_CUSTOM, structure);
  if (query) {
    if (gst_pad_peer_query (viddec->sinkpad, query) == TRUE) {
      gst_structure_get_boolean (structure, "reverse",
          &viddec->tm_control.reverse_support);
    }
    gst_query_unref (query);
  }

  /* Check if upstream supports index */
  structure = gst_structure_new ("index-support",
      "index", G_TYPE_BOOLEAN, FALSE, NULL);
  query = gst_query_new_custom (GST_QUERY_CUSTOM, structure);
  if (query) {
    if (gst_pad_peer_query (viddec->sinkpad, query) == TRUE) {
      gst_structure_get_boolean (structure, "rap-index",
          &viddec->tm_control.upstream_rap_index_support);
    }
    gst_query_unref (query);
  }

  /* Reset variables */
  viddec->tm_control.rate = 1.0;
  viddec->clock_data_flags = DVB_CLOCK_INITIALIZE;

  /* avsync policy setup */
  memset (&video_cmd, 0, sizeof (struct video_command));
  video_cmd.cmd = VIDEO_CMD_SET_OPTION;
  if (viddec->tunneled == FALSE) {
    /* non tunneled pipeline - avsync disabled */
    video_cmd.option.option = DVB_OPTION_AV_SYNC;
    video_cmd.option.value = DVB_OPTION_VALUE_DISABLE;
    gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd);
    GST_DEBUG_OBJECT (viddec,
        "NON TUNNELED PIPELINE <DVB_OPTION_AV_SYNC : DVB_OPTION_VALUE_DISABLE>");
  } else {
    /* tunneled pipeline */
    if (viddec->disable_avsync == TRUE) {
      /* application sent action to disable avsync - tunneled pipeline */
      video_cmd.option.option = DVB_OPTION_AV_SYNC;
      video_cmd.option.value = DVB_OPTION_VALUE_DISABLE;
      gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd);
      GST_DEBUG_OBJECT (viddec,
          "TUNNELED PIPELINE (application disables avsync) <DVB_OPTION_AV_SYNC : DVB_OPTION_VALUE_DISABLE>");
    } else {
      /* by default avsync has to be enabled for tunneled pipeline */
      video_cmd.option.option = DVB_OPTION_AV_SYNC;
      video_cmd.option.value = DVB_OPTION_VALUE_ENABLE;
      gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd);
      GST_DEBUG_OBJECT (viddec,
          "TUNNELED PIPELINE <DVB_OPTION_AV_SYNC : DVB_OPTION_VALUE_ENABLE>");
    }
  }

  /* set user specified memory profile on SE */
  if (viddec->mem_profile != G_MAXUINT) {
    memset (&video_cmd, 0, sizeof (struct video_command));
    video_cmd.cmd = VIDEO_CMD_SET_OPTION;
    video_cmd.option.option = DVB_OPTION_CTRL_VIDEO_MEMORY_PROFILE;
    video_cmd.option.value = viddec->mem_profile;
    ret = gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd);
    if (ret == FALSE) {
      GST_ERROR_OBJECT (viddec,
          "<DVB_OPTION_MEM_PROFILE: could not set memory profile %u>",
          viddec->mem_profile);
    } else {
      GST_DEBUG_OBJECT (viddec,
          "<DVB_OPTION_MEM_PROFILE : DVB_OPTION_CTRL_VIDEO_MEMORY_PROFILE %u>",
          viddec->mem_profile);
    }
  }

  /* set/reset interlaced for H264 */
  if (viddec->codec.type == VIDEO_ENCODING_H264) {
    memset (&video_cmd, 0, sizeof (struct video_command));
    video_cmd.cmd = VIDEO_CMD_SET_OPTION;
    video_cmd.option.option =
        DVB_OPTION_H264_TREAT_TOP_BOTTOM_PICTURE_STRUCT_AS_INTERLACED;
    video_cmd.option.value = viddec->interlace;
    ret = gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd);
    if (ret == FALSE) {
      GST_ERROR_OBJECT (viddec,
          "<DVB_OPTION_INTERLACED: could not set interlaced mode %u>",
          viddec->interlace);
    } else {
      GST_DEBUG_OBJECT (viddec,
          "<DVB_OPTION_INTERLACED : DVB_OPTION_H264_TREAT_TOP_BOTTOM_PICTURE_STRUCT_AS_INTERLACED %u>",
          viddec->interlace);
    }
  }

  /* set video decoder decimation on SE */
  if (viddec->decimation != -1) {
    memset (&video_cmd, 0, sizeof (struct video_command));
    video_cmd.cmd = VIDEO_CMD_SET_OPTION;
    video_cmd.option.option = DVB_OPTION_DECIMATE_DECODER_OUTPUT;
    video_cmd.option.value = viddec->decimation;
    ret = gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd);
    if (ret == FALSE) {
      GST_ERROR_OBJECT (viddec,
          "<DVB_OPTION_DECIMATE: could not set video decoder decimation %u>",
          viddec->decimation);
    } else {
      GST_DEBUG_OBJECT (viddec,
          "<DVB_OPTION_DECIMATE : DVB_OPTION_DECIMATE_OUTPUT %u>",
          viddec->decimation);
    }
  }

  /* stream switch only if codec type has changed */
  if (viddec->codec.type != viddec->prev_codec) {
    if (gst_stvideo_ioctl (viddec, VIDEO_SET_ENCODING,
            (void *) viddec->codec.type) == TRUE) {
      gst_stvideo_ioctl (viddec, VIDEO_PLAY, NULL);
      /* store for use during caps change */
      viddec->prev_codec = viddec->codec.type;
      if (GST_STATE (viddec) == GST_STATE_PAUSED && (viddec->tunneled == TRUE)) {
        /* Pause the decoder */
        if (gst_stvideo_ioctl (viddec, VIDEO_FREEZE, NULL) == TRUE) {
          GST_DEBUG_OBJECT (viddec, "VIDEO_FREEZE");
          viddec->state = VIDEO_FREEZED;
        }
      }
      else {
        viddec->state = VIDEO_PLAYING;
      }
    } else {
      GST_WARNING_OBJECT (viddec, "failed to set encoding to %d",
          viddec->codec.type);
    }
  }

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return;
}

static gboolean
gst_stvideo_ioctl (Gststvideo * viddec, guint ioctl_code, void *parameter)
{
  gint ret = FALSE;

  if (viddec->disable_dev == TRUE) {
    return TRUE;
  }

  if (viddec->fd > 0) {
    if (ioctl_code != VIDEO_SET_CLOCK_DATA_POINT
        && ioctl_code != VIDEO_GET_PLAY_INFO && ioctl_code != VIDEO_GET_EVENT
        && ioctl_code != VIDEO_DISCONTINUITY) {
      GST_DEBUG_OBJECT (viddec, "ioctl %s         : parameter %d",
          gst_stvideo_ioctl_name (ioctl_code), (gint) parameter);
    } else {
      GST_TRACE_OBJECT (viddec, "ioctl %s         : parameter %d",
          gst_stvideo_ioctl_name (ioctl_code), (gint) parameter);
    }
    if (ioctl (viddec->fd, ioctl_code, parameter) == 0) {
      ret = TRUE;
    } else {
      GST_ERROR_OBJECT (viddec, "%s failed - %s",
          gst_stvideo_ioctl_name (ioctl_code), strerror (errno));
    }
  } else {
    GST_ERROR_OBJECT (viddec, "device not open - %s",
        gst_stvideo_ioctl_name (ioctl_code));
    /* In change_state function, we need to be able to quit even if device was not opened */
    ret = TRUE;
  }

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return ret;
}

static gboolean
gst_stvideo_write (Gststvideo * viddec, guint8 * data, guint data_length)
{
  gint written = 0;

  if ((viddec->disable_dev == TRUE) || (viddec->exit_loop == TRUE)
      || (viddec->fd <= 0) || (!data) || (!data_length))
    return TRUE;

  GST_TRACE_OBJECT (viddec, "write(%d) from 0x%08x", data_length, (guint) data);

  written = write (viddec->fd, data, data_length);
  if (written < 0) {
    GST_ERROR_OBJECT (viddec, "write failed - %s %i", strerror (errno), errno);
    GST_ELEMENT_ERROR (viddec, RESOURCE, WRITE, ("Video write failed %s %i",
            strerror (errno), errno), GST_ERROR_SYSTEM);
    return FALSE;
  }

  if (written != data_length)
    GST_WARNING_OBJECT (viddec, "written %d != expected write %u", written,
        data_length);

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return TRUE;
}

static void
gst_stvideo_update_time (Gststvideo * viddec)
{
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  /* Get video playback time */
  if (viddec->state != VIDEO_STOPPED) {
    video_play_info_t vid_play_info;
    memset (&vid_play_info, 0, sizeof (vid_play_info));
    vid_play_info.pts = INVALID_PTS_VALUE;
    if (gst_stvideo_ioctl (viddec, VIDEO_GET_PLAY_INFO,
            (void *) &vid_play_info) == TRUE) {
      viddec->tm_control.frame_count = vid_play_info.frame_count;
      if (IS_PTS_VALUE_VALID (vid_play_info.pts)) {
        gint64 current_pts = 0;
        viddec->currentPTS = vid_play_info.pts;
        current_pts = MPEGTIME_TO_GSTTIME ((gint64) viddec->currentPTS);
        if (!IS_PTS_VALUE_VALID (viddec->prev_video_pts)) {
          viddec->prev_video_pts = vid_play_info.pts;
        }
        /* TS streams may have pts wrap around */
        if ((current_pts < viddec->prevPTS)
            && (viddec->update_segment == TRUE)) {
          viddec->segment.start = current_pts;
          viddec->segment.time = viddec->current_time;
          viddec->update_segment = FALSE;
        }
        viddec->current_time =
            viddec->segment.time + current_pts - viddec->segment.start;
        viddec->prevPTS = current_pts;
        viddec->prev_video_pts = vid_play_info.pts;
      }
    }
  }
}

static gboolean
gst_stvideo_apply_set_speed (Gststvideo * viddec)
{
  struct video_status status;
  gdouble rate = viddec->tm_control.rate;

  /* Resume to previous speed */
  if (IS_GST_STVIDEO_TM_MASTER_SPEED (viddec->tm_control.rate)) {
    gst_stvideo_tm_control_update (viddec, viddec->tm_control.set_rate,
        viddec->tm_control.time_skip);
  }

  /* Force a set speed */
  viddec->tm_control.rate = 0;
  if (gst_stvideo_set_speed (viddec, rate, TRUE) == FALSE) {
    GST_ERROR_OBJECT (viddec, "failed to set rate %f", rate);
    return FALSE;
  }

  if (gst_stvideo_ioctl (viddec, VIDEO_GET_STATUS, (void *) &status) == TRUE) {
    if (viddec->disable_dev == TRUE) {
      viddec->state = VIDEO_PLAYING;
    } else {
      viddec->state = status.play_state;
    }
  } else {
    GST_ERROR_OBJECT (viddec, "failed to get video status");
    return FALSE;
  }

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return TRUE;
}

static gboolean
gst_stvideo_tm_control_update (Gststvideo * viddec, gdouble rate,
    GstClockTime seek_time)
{
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  viddec->tm_control.time_skip = seek_time;
  viddec->tm_control.time_ref = gst_clock_get_time (GST_ELEMENT_CLOCK (viddec));
  viddec->tm_control.prev_frame_count = 0;
  viddec->tm_control.frame_count = 0;
  viddec->tm_control.frame_repeat = 0;
  viddec->tm_control.frame_dec_late = 0;
  viddec->tm_control.time_base = viddec->current_time;
  viddec->tm_control.bos = FALSE;
  GST_DEBUG_OBJECT (viddec, "time base %" GST_TIME_FORMAT,
      GST_TIME_ARGS (viddec->tm_control.time_base));
  if ((rate < 0) && (viddec->tm_control.reverse_support == FALSE)) {
    viddec->tm_control.enable = TRUE;
  } else {
    viddec->tm_control.enable = FALSE;
  }
  viddec->tm_control.reverse_cur_num = 0;
  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return TRUE;
}

static void
gst_stvideo_tm_control_check_event (Gststvideo * viddec)
{
  struct video_event vid_event;
  struct pollfd poll_fd[1];
  gboolean decoder_paused = FALSE;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  poll_fd[0].fd = viddec->fd;
  poll_fd[0].events = POLLPRI;

  if (poll (poll_fd, 1, 10) == -1) {
    GST_ERROR_OBJECT (viddec, "poll error");
  }
  if (poll_fd[0].revents & POLLPRI) {
    if (gst_stvideo_ioctl (viddec, VIDEO_GET_EVENT,
            (void *) &vid_event) == TRUE) {
      switch (vid_event.type) {
        case VIDEO_EVENT_SIZE_CHANGED:
          GST_DEBUG_OBJECT (viddec, "VIDEO_EVENT_SIZE_CHANGED");
          gst_stvideo_handle_size_changed_event (viddec, (void *) &vid_event);
          break;
        case VIDEO_EVENT_FRAME_RATE_CHANGED:
          /* freeze the decoder in order to avoid data overwrite in SE
             in capture mode */
          if ((viddec->state == VIDEO_PLAYING) && (!viddec->tunneled)) {
            if (gst_stvideo_ioctl (viddec, VIDEO_FREEZE, NULL)) {
              viddec->state = VIDEO_FREEZED;
              decoder_paused = TRUE;
            }
          }
          gst_stvideo_handle_framerate_changed_event (viddec,
              (void *) &vid_event);
          /* Resume the decoder */
          if ((viddec->state == VIDEO_FREEZED) && (!viddec->tunneled)
              && (decoder_paused)) {
            if (gst_stvideo_ioctl (viddec, VIDEO_PLAY, NULL)) {
              viddec->state = VIDEO_PLAYING;
            }
          }
          break;
        case VIDEO_EVENT_FRAME_DECODED_LATE:
          GST_DEBUG_OBJECT (viddec, "VIDEO_EVENT_FRAME_DECODED_LATE");
          /* Start to monitor underflow when we are in fast-forward */
          if ((viddec->tm_control.set_rate == viddec->tm_control.rate)
              && (viddec->tm_control.set_rate > 1.0)
              && (viddec->tm_control.enable == FALSE)) {
            viddec->tm_control.frame_dec_late++;
            if ((viddec->tm_control.frame_dec_late >
                    GST_STVIDEO_FF_MAX_UNDERFLOW)
                && (viddec->tm_control.reverse_support == FALSE)) {
              /* Turn on the tm control */
              viddec->tm_control.enable = TRUE;
            }
          }
          break;
        case VIDEO_EVENT_TRICK_MODE_CHANGE:
          GST_DEBUG_OBJECT (viddec, "VIDEO_EVENT_TRICK_MODE_CHANGE");
          gst_stvideo_handle_trickmode_change_event (viddec,
              (void *) &vid_event);
          break;
        case VIDEO_EVENT_END_OF_STREAM:
          GST_DEBUG_OBJECT (viddec, "VIDEO_EVENT_END_OF_STREAM");
          if ((viddec->eos_set == TRUE) && (viddec->tunneled)) {
            GST_INFO_OBJECT (viddec, "End of Stream event pushed");
            gst_pad_push_event (viddec->srcpad, gst_event_new_eos ());
          }
          break;
        default:
          break;
      }
    } else {
      GST_WARNING_OBJECT (viddec, "failed to get event");
    }
  }
  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return;
}

static void
gst_stvideo_start_monitor_task (Gststvideo * viddec)
{
  /* Create and initialize the video monitor task */
  g_rec_mutex_init (&viddec->vm_task_lock);
  viddec->video_monitor_task =
      gst_task_new ((GstTaskFunction) gst_stvideo_monitor_loop,
      viddec->srcpad, NULL);
  gst_object_set_name (GST_OBJECT_CAST (viddec->video_monitor_task),
      "SMF-Vid Monitor");
  gst_task_set_lock (viddec->video_monitor_task, &viddec->vm_task_lock);
  gst_task_start (viddec->video_monitor_task);
}

static void
gst_stvideo_stop_monitor_task (Gststvideo * viddec)
{
  gst_task_stop (viddec->video_monitor_task);
  g_rec_mutex_lock (&viddec->vm_task_lock);
  g_rec_mutex_unlock (&viddec->vm_task_lock);
  gst_task_join (viddec->video_monitor_task);
  gst_object_unref (viddec->video_monitor_task);
  g_rec_mutex_clear (&viddec->vm_task_lock);
  viddec->video_monitor_task = NULL;
}

/*
  * Function name : gst_stvideo_monitor_loop
  *  Input        :
  *    pad - viddec src pad
  *
  *  Description  : This task is scheduled every 10ms to update video time
  *                 and to check any event from streaming engine
  *
  * Return        : void
*/
static void
gst_stvideo_monitor_loop (GstPad * pad)
{
  Gststvideo *viddec = GST_STVIDEO (gst_pad_get_parent (pad));

  if (viddec->fd != -1) {
    gst_stvideo_update_time (viddec);
    gst_stvideo_tm_control_check_event (viddec);
  } else {
    usleep (10 * 1000);
  }

  gst_object_unref (viddec);
}

static gchar *
gst_stvideo_ioctl_name (gint ioctl_code)
{
  switch (ioctl_code) {
    case VIDEO_STOP:
      return "VIDEO_STOP";
    case VIDEO_PLAY:
      return "VIDEO_PLAY";
    case VIDEO_FREEZE:
      return "VIDEO_FREEZE";
    case VIDEO_CONTINUE:
      return "VIDEO_CONTINUE";
    case VIDEO_SELECT_SOURCE:
      return "VIDEO_SELECT_SOURCE";
    case VIDEO_SET_BLANK:
      return "VIDEO_SET_BLANK";
    case VIDEO_GET_STATUS:
      return "VIDEO_GET_STATUS";
    case VIDEO_GET_EVENT:
      return "VIDEO_GET_EVENT";
    case VIDEO_GET_SIZE:
      return "VIDEO_GET_SIZE";
    case VIDEO_SET_DISPLAY_FORMAT:
      return "VIDEO_SET_DISPLAY_FORMAT";
    case VIDEO_STILLPICTURE:
      return "VIDEO_STILLPICTURE";
    case VIDEO_FAST_FORWARD:
      return "VIDEO_FAST_FORWARD";
    case VIDEO_SLOWMOTION:
      return "VIDEO_SLOWMOTION";
    case VIDEO_GET_CAPABILITIES:
      return "VIDEO_GET_CAPABILITIES";
    case VIDEO_SET_ID:
      return "VIDEO_SET_ID";
    case VIDEO_CLEAR_BUFFER:
      return "VIDEO_CLEAR_BUFFER";
    case VIDEO_SET_STREAMTYPE:
      return "VIDEO_SET_STREAMTYPE";
    case VIDEO_SET_FORMAT:
      return "VIDEO_SET_FORMAT";
    case VIDEO_SET_SYSTEM:
      return "VIDEO_SET_SYSTEM";
    case VIDEO_SET_HIGHLIGHT:
      return "VIDEO_SET_HIGHLIGHT";
    case VIDEO_SET_SPU:
      return "VIDEO_SET_SPU";
    case VIDEO_SET_SPU_PALETTE:
      return "VIDEO_SET_SPU_PALETTE";
    case VIDEO_GET_NAVI:
      return "VIDEO_GET_NAVI";
    case VIDEO_SET_ATTRIBUTES:
      return "VIDEO_SET_ATTRIBUTES";
    case VIDEO_GET_FRAME_RATE:
      return "VIDEO_GET_FRAME_RATE";
    case VIDEO_SET_ENCODING:
      return "VIDEO_SET_ENCODING";
    case VIDEO_FLUSH:
      return "VIDEO_FLUSH";
    case VIDEO_SET_SPEED:
      return "VIDEO_SET_SPEED";
    case VIDEO_DISCONTINUITY:
      return "VIDEO_DISCONTINUITY";
    case VIDEO_STEP:
      return "VIDEO_STEP";
    case VIDEO_SET_PLAY_INTERVAL:
      return "VIDEO_SET_PLAY_INTERVAL";
    case VIDEO_SET_SYNC_GROUP:
      return "VIDEO_SET_SYNC_GROUP";
    case VIDEO_GET_PLAY_TIME:
      return "VIDEO_GET_PLAY_TIME";
    case VIDEO_GET_PLAY_INFO:
      return "VIDEO_GET_PLAY_INFO";
    case VIDEO_GET_PTS:
      return "VIDEO_GET_PTS";
    case VIDEO_COMMAND:
      return "VIDEO_COMMAND";
    case VIDEO_SET_CLOCK_DATA_POINT:
      return "VIDEO_SET_CLOCK_DATA_POINT";
    default:
      return "Invalid ioctl";
  }
}

static gboolean
gst_stvideo_write_packet (Gststvideo * viddec, GstBuffer * buf)
{
  guint8 *data;
  guint8 pes_header[PES_MAX_HEADER_SIZE];
  guint data_length;
  guint64 pts;
  GstMapInfo info_read;
  gboolean ret = TRUE;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);

  /* Reset variables */
  pts = 0ull;
  gst_buffer_map (buf, &info_read, GST_MAP_READ);
  data = info_read.data;
  data_length = info_read.size;
  memset (pes_header, 0, PES_MAX_HEADER_SIZE);

  /* Get video timestamp */
  if (GST_BUFFER_TIMESTAMP_IS_VALID (buf)) {
    pts = gst_util_uint64_scale (GST_BUFFER_TIMESTAMP (buf), 9, 100000);
    /* modulo pts with max value (33 bits). it will take care
       of wrap around also.
     */
    pts %= (MAX_PTS_VALUE);
  } else {
    if (viddec->first_injection == FALSE) {
      pts = INVALID_PTS_VALUE;
    }
  }

  if ((viddec->tm_control.reverse_injection == TRUE)
      || (viddec->tm_control.set_rate > 0)) {
    /* Injection to video codec */
    switch (viddec->codec.type) {
      case VIDEO_ENCODING_MPEG4P2:
        ret =
            gst_stvideo_write_mpeg4p2_packet (viddec, pes_header,
            PES_MAX_HEADER_SIZE, data, data_length, pts);
        break;
      case VIDEO_ENCODING_VP6:
      case VIDEO_ENCODING_VP8:
      case VIDEO_ENCODING_VP9:
        ret =
            gst_stvideo_write_vp_packet (viddec, pes_header,
            PES_MAX_HEADER_SIZE, data, data_length, pts);
        break;
      case VIDEO_ENCODING_HEVC:
      case VIDEO_ENCODING_H264:
        if (viddec->codec_data.buf != NULL) {
          ret =
              gst_stvideo_write_h264_hevc_packet (viddec, pes_header,
              PES_MAX_HEADER_SIZE, data, data_length, pts);
          break;
        }
      default:
        ret =
            gst_stvideo_write_video_packet (viddec, pes_header,
            PES_MAX_HEADER_SIZE, data, data_length, pts);
        break;
    }
  }
  gst_buffer_unmap (buf, &info_read);
  return ret;
}

static gboolean
gst_stvideo_write_h264_hevc_packet (Gststvideo * viddec, guint8 * pes_header,
    guint pes_header_length, guint8 * pes_data, guint pes_data_length,
    guint64 pts)
{
  guint remaining_bytes = 0;
  guint offset = 0;
  guint nal_length = 0;
  guint header_length;
  GstMapInfo info_read;
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  if (viddec->first_injection == TRUE) {
    if (viddec->codec_data.buf != NULL) {
      gst_buffer_map (viddec->codec_data.buf, &info_read, GST_MAP_READ);
      header_length =
          InsertPesHeader (pes_header, info_read.size,
          viddec->codec.start_code, INVALID_PTS_VALUE, 0);
      if (gst_stvideo_write (viddec, pes_header, header_length) == FALSE) {
        gst_buffer_unmap (viddec->codec_data.buf, &info_read);
        GST_ERROR_OBJECT (viddec, "failed to write data on video device");
        return FALSE;
      }
      if (gst_stvideo_write (viddec, info_read.data, info_read.size) == FALSE) {
        gst_buffer_unmap (viddec->codec_data.buf, &info_read);
        GST_ERROR_OBJECT (viddec, "failed to write data on video device");
        return FALSE;
      }
      gst_buffer_unmap (viddec->codec_data.buf, &info_read);
    }
    /* This is to insert a special nal for pixel aspect ratio to player2 */
    if ((viddec->properties.par_num > 1)
        && (viddec->properties.par_den > 1)) {
      guint8 *special_nal =
          g_malloc0 (GST_STVIDEO_H264_PLAYER2_HEADER_SIZE +
          GST_STVIDEO_H264_PLAYER2_DATA_SIZE);
      guint i = 0;

      /* h264 player2 header */
      /* start code - 0x00 0x00 0x00 0x01 */
      /* nal type - 0x18                  */
      /* parameter version - 0x01         */
      /* marker - 0xff                    */
      /* field present mask - 0x00 0x02   */
      /* marker - 0xff                    */
      memcpy (special_nal, h264_player2_header,
          GST_STVIDEO_H264_PLAYER2_HEADER_SIZE);
      /* Numerator */
      special_nal[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE + (i++)] =
          (guint8) ((viddec->properties.par_num >> 24) & 0xff);
      special_nal[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE + (i++)] =
          (guint8) ((viddec->properties.par_num >> 16) & 0xff);
      special_nal[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE + (i++)] = 0xff; /* marker */
      special_nal[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE + (i++)] =
          (guint8) ((viddec->properties.par_num >> 8) & 0xff);
      special_nal[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE + (i++)] =
          (guint8) (viddec->properties.par_num & 0xff);
      special_nal[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE + (i++)] = 0xff; /* marker */
      /* Denominator */
      special_nal[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE + (i++)] =
          (guint8) ((viddec->properties.par_den >> 24) & 0xff);
      special_nal[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE + (i++)] =
          (guint8) ((viddec->properties.par_den >> 16) & 0xff);
      special_nal[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE + (i++)] = 0xff; /* marker */
      special_nal[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE + (i++)] =
          (guint8) ((viddec->properties.par_den >> 8) & 0xff);
      special_nal[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE + (i++)] =
          (guint8) (viddec->properties.par_den & 0xff);
      special_nal[GST_STVIDEO_H264_PLAYER2_HEADER_SIZE + (i++)] = 0xff; /* marker */

      header_length =
          InsertPesHeader (pes_header,
          GST_STVIDEO_H264_PLAYER2_HEADER_SIZE +
          GST_STVIDEO_H264_PLAYER2_DATA_SIZE, viddec->codec.start_code,
          INVALID_PTS_VALUE, 0);
      if (gst_stvideo_write (viddec, pes_header, header_length) == FALSE) {
        g_free (special_nal);
        GST_ERROR_OBJECT (viddec, "failed to write data on video device");
        return FALSE;
      }
      if (gst_stvideo_write (viddec, special_nal,
              GST_STVIDEO_H264_PLAYER2_HEADER_SIZE +
              GST_STVIDEO_H264_PLAYER2_DATA_SIZE) == FALSE) {
        g_free (special_nal);
        GST_ERROR_OBJECT (viddec, "failed to write data on video device");
        return FALSE;
      }
      g_free (special_nal);
    }
    viddec->first_injection = FALSE;
  }

  remaining_bytes = pes_data_length;

  while ((remaining_bytes > 0) && (pes_data != NULL)
      && (offset < pes_data_length)) {
    if (viddec->codec_data.h264.nal_header_size == 1) {
      nal_length = pes_data[offset];
    } else if (viddec->codec_data.h264.nal_header_size == 2) {
      if (remaining_bytes <= 1) {
        /* Not enough data to calculate nal_length */
        GST_WARNING_OBJECT (viddec, "remaining bytes %u are <= 1",
            remaining_bytes);
        break;
      } else {
        nal_length = (pes_data[offset] << 8) | pes_data[offset + 1];
      }
    } else if (viddec->codec_data.h264.nal_header_size == 3) {
      if (remaining_bytes <= 2) {
        /* Not enough data to calculate nal_length */
        GST_WARNING_OBJECT (viddec, "remaining bytes %u are <= 2",
            remaining_bytes);
        break;
      } else {
        nal_length =
            (pes_data[offset] << 16) | (pes_data[offset +
                1] << 8) | pes_data[offset + 2];
      }
    } else {
      if (remaining_bytes <= 3) {
        /* Not enough data to calculate nal_length */
        GST_WARNING_OBJECT (viddec, "remaining bytes %u are <= 3",
            remaining_bytes);
        break;
      } else {
        nal_length =
            (pes_data[offset] << 24) | (pes_data[offset +
                1] << 16) | (pes_data[offset + 2] << 8) | pes_data[offset + 3];
      }
    }

    if (nal_length > 0) {
      if (pes_data_length <
          (offset + viddec->codec_data.h264.nal_header_size + nal_length)) {
        /* Not required data to write */
        GST_WARNING_OBJECT (viddec,
            "data availavle is %u, nal_length is %u, data required is %u",
            remaining_bytes, nal_length,
            nal_length + viddec->codec_data.h264.nal_header_size);
        break;
      }
      memset (pes_header, 0, PES_MAX_HEADER_SIZE);
      header_length =
          InsertPesHeader (pes_header, (nal_length + 4),
          viddec->codec.start_code, pts, 0);
      if (gst_stvideo_write (viddec, pes_header, header_length) == FALSE) {
        return FALSE;
      }

      if (viddec->codec_data.h264.nal_header_size == 4) {
        memcpy ((pes_data + offset), h264_start_code, sizeof (h264_start_code));
        if (gst_stvideo_write (viddec,
                pes_data + (pes_data_length - remaining_bytes),
                (nal_length + 4)) == FALSE) {
          return FALSE;
        }
      } else {
        guint8 *temp_data = NULL;

        temp_data = g_malloc0 (nal_length + sizeof (h264_start_code));
        if (temp_data != NULL) {
          memcpy (temp_data, h264_start_code, sizeof (h264_start_code));
          memcpy ((temp_data + sizeof (h264_start_code)),
              pes_data + offset + viddec->codec_data.h264.nal_header_size,
              nal_length);
          if (gst_stvideo_write (viddec, temp_data,
                  (nal_length + sizeof (h264_start_code))) == FALSE) {
            g_free (temp_data);
            return FALSE;
          }
          g_free (temp_data);
        }
      }
    }
    pts = INVALID_PTS_VALUE;
    offset += (viddec->codec_data.h264.nal_header_size + nal_length);
    if (pes_data_length >= offset) {
      remaining_bytes = pes_data_length - offset;
    } else {
      remaining_bytes = 0;
    }
  }

  return TRUE;
}

static gboolean
gst_stvideo_write_mpeg4p2_packet (Gststvideo * viddec, guint8 * pes_header,
    guint pes_header_length, guint8 * pes_data, guint pes_data_length,
    guint64 pts)
{
  guint fake_sc = 0;
  guint header_length;
  GstMapInfo info_read;
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  if (viddec->first_injection == TRUE) {
    if (viddec->codec_data.buf != NULL) {
      gst_buffer_map (viddec->codec_data.buf, &info_read, GST_MAP_READ);
      header_length =
          InsertPesHeader (pes_header, info_read.size,
          viddec->codec.start_code, INVALID_PTS_VALUE, 0);
      if (gst_stvideo_write (viddec, pes_header, header_length) == FALSE) {
        gst_buffer_unmap (viddec->codec_data.buf, &info_read);
        GST_ERROR_OBJECT (viddec, "failed to write data on video device");
        return FALSE;
      }
      if (gst_stvideo_write (viddec, info_read.data, info_read.size) == FALSE) {
        gst_buffer_unmap (viddec->codec_data.buf, &info_read);
        GST_ERROR_OBJECT (viddec, "failed to write data on video device");
        return FALSE;
      }
      gst_buffer_unmap (viddec->codec_data.buf, &info_read);
    }
    viddec->first_injection = FALSE;
  }

  if (viddec->codec.subtype == VIDEO_ENCODING_DIVX3) {
    fake_sc = (3 << 8) | PES_VERSION_FAKE_START_CODE;
  } else if (viddec->codec.subtype == VIDEO_ENCODING_DIVX4) {
    fake_sc = (4 << 8) | PES_VERSION_FAKE_START_CODE;
  } else if (viddec->codec.subtype == VIDEO_ENCODING_DIVX5) {
    fake_sc = (5 << 8) | PES_VERSION_FAKE_START_CODE;
  }

  header_length =
      InsertPesHeader (pes_header, pes_data_length, viddec->codec.start_code,
      pts, fake_sc);

  if (gst_stvideo_write (viddec, pes_header, header_length) == FALSE) {
    GST_ERROR_OBJECT (viddec, "failed to write data on video device");
    return FALSE;
  }

  if ((viddec->codec.subtype == VIDEO_ENCODING_DIVX3)
      || (viddec->codec.subtype == VIDEO_ENCODING_DIVX4)
      || (viddec->codec.subtype == VIDEO_ENCODING_DIVX5)) {
    guint8 fake_header[PES_MAX_HEADER_SIZE];
    guint fake_header_length;
    guint micro_seconds_per_frame = 0;

    /* Generate fake header for frame parser */
    /* VOS */
    BitPacker_t ld = {
      fake_header, 0, 32
    };
    memset (fake_header, 0, sizeof (fake_header));
    PutBits (&ld, 0x1b0, 32);   /* startcode */
    PutBits (&ld, 0, 8);        /* profile = reserved */
    PutBits (&ld, 0x1b2, 32);   /* startcode (user data) */
    PutBits (&ld, 0x53545443, 32);      /* STTC - an embedded ST timecode from an avi file */
    if (viddec->properties.fps_num != 0) {
      micro_seconds_per_frame =
          (1000000 * (guint64) viddec->properties.fps_den) /
          (guint64) viddec->properties.fps_num;
    }
    PutBits (&ld, micro_seconds_per_frame, 32);
    if ((viddec->codec.subtype == VIDEO_ENCODING_DIVX3)
        && (pes_data != NULL)) {
      guint frame_type;

      /* Construct VOL and VOP for this type of file. If the frame is an IFrame then
         insert a VOL+VOP, otherwise just a VOP. */
      frame_type = pes_data[0];
      frame_type = (frame_type >> 6) & 0x3;

      if (frame_type == 0) {
        guint vid_fixed_time_increment_resolution,
            vid_fixed_time_increment_resolution_length;
        guint vid_fixed_vop_time_increment, vid_fixed_vop_time_increment_length;

        AlignBits (&ld);

        /* Fake VOL & VOP */
        /* VOL */
        PutBits (&ld, 0x120, 32);       /* startcode                        */
        PutBits (&ld, 0, 1);    /* random_accessible_vol            */
        PutBits (&ld, 1, 8);    /* type_indication (Simple Object)  */
        PutBits (&ld, 0, 1);    /* is_object_layer_identification   */
        PutBits (&ld, 1, 4);    /* aspect_ratio_info (SQUARE)       */
        PutBits (&ld, 0, 1);    /* vol_control_parameters           */
        PutBits (&ld, 0, 2);    /* shape (RECTANGULAR)              */
        PutBits (&ld, 1, 1);    /* MARKER BIT                       */

        /* These are recognised frame rates... 23.976fps has been seen with some
           variations, so I'll add this by hand until we get a more overall picture
           and then work out the correct formula */
        switch (micro_seconds_per_frame) {
            /* 23.976fps */
          case 41708:
            vid_fixed_time_increment_resolution = 24000;
            vid_fixed_time_increment_resolution_length = 16;
            vid_fixed_vop_time_increment = 1001;
            vid_fixed_vop_time_increment_length = 15;
            break;
            /* 29.976fps */
          case 33367:
            vid_fixed_time_increment_resolution = 30000;
            vid_fixed_time_increment_resolution_length = 16;
            vid_fixed_vop_time_increment = 1001;
            vid_fixed_vop_time_increment_length = 15;
            break;
            /* Assume valid in formula */
          default:
            vid_fixed_time_increment_resolution =
                1000000000 / micro_seconds_per_frame;
            vid_fixed_time_increment_resolution_length = 16;
            vid_fixed_vop_time_increment = 1000;
            vid_fixed_vop_time_increment_length = 15;
            break;
        }

        PutBits (&ld, vid_fixed_time_increment_resolution,
            vid_fixed_time_increment_resolution_length);
        PutBits (&ld, 1, 1);    /* MARKER BIT                       */
        PutBits (&ld, 1, 1);    /* fixed_vop_rate                   */
        PutBits (&ld, vid_fixed_vop_time_increment,
            vid_fixed_vop_time_increment_length);
        PutBits (&ld, 1, 1);    /* MARKER BIT                       */
        PutBits (&ld, viddec->properties.width, 13);
        PutBits (&ld, 1, 1);    /* MARKER BIT                       */
        PutBits (&ld, viddec->properties.height, 13);
        PutBits (&ld, 1, 1);    /* MARKER BIT                       */
        PutBits (&ld, 0, 1);    /* interlaced                       */
        PutBits (&ld, 1, 1);    /* obmc_disabled                    */
        PutBits (&ld, 0, 1);    /* sprite_usage (SPRITE_NOT_USED)   */
        PutBits (&ld, 0, 1);    /* not_8_bit                        */
        PutBits (&ld, 0, 1);    /* quant_type                       */
        PutBits (&ld, 1, 1);    /* complexity_estimation_disable    */
        PutBits (&ld, 1, 1);    /* resync_marker_disable            */
        PutBits (&ld, 0, 1);    /* data_partitioning                */
        PutBits (&ld, 0, 1);    /* scalability                      */

        AlignBits (&ld);

        /* VOP */
        PutBits (&ld, 0x1b6, 32);       /* startcode                        */
      } else if (frame_type == 1) {
        AlignBits (&ld);

        /* Fake VOP */
        PutBits (&ld, 0x1b6, 32);       /* startcode                        */
      } else {
        GST_ERROR_OBJECT (viddec, "DIVX3 wrong frame type %d", frame_type);
      }

      viddec->codec_data.MP4P2.tick_elapsed += viddec->properties.fps_den;
    }

    FlushBits (&ld);
    fake_header_length = (ld.ptr - (fake_header));
    if (gst_stvideo_write (viddec, fake_header, fake_header_length) == FALSE) {
      GST_ERROR_OBJECT (viddec, "failed to write data on video device");
      return FALSE;
    }
  }

  if (gst_stvideo_write (viddec, pes_data, pes_data_length) == FALSE) {
    GST_ERROR_OBJECT (viddec, "failed to write data on video device");
    return FALSE;
  }

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  return TRUE;
}

static gboolean
gst_stvideo_write_vp_packet (Gststvideo * viddec, guint8 * pes_header,
    guint pes_header_length, guint8 * pes_data, guint pes_data_length,
    guint64 pts)
{
  guint fake_sc = 0;
  guint pes_length;
  guint private_header_length;
  guint header_length;
  guint metadata[5];
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  if (viddec->first_injection == TRUE) {
    metadata[0] = viddec->codec.subtype;
    metadata[1] = viddec->properties.width;
    metadata[2] = viddec->properties.height;
    /* Duration */
    metadata[3] = 0;
    if (viddec->properties.fps_den != 0) {
      /* Framerate */
      metadata[4] =
          (1000000 * (guint64) viddec->properties.fps_num) /
          (guint64) viddec->properties.fps_den;
    }

    header_length =
        InsertPesHeader (pes_header, sizeof (metadata),
        viddec->codec.start_code, INVALID_PTS_VALUE, 0);
    memcpy ((pes_header + header_length), &metadata, sizeof (metadata));
    if (gst_stvideo_write (viddec, pes_header,
            header_length + sizeof (metadata)) == FALSE) {
      GST_ERROR_OBJECT (viddec, "failed to write data on video device");
      return FALSE;
    }
    viddec->first_injection = FALSE;
  }

  header_length =
      InsertPesHeader (pes_header, pes_data_length, H263_VIDEO_PES_START_CODE,
      pts, fake_sc);

  if (viddec->codec.type == VIDEO_ENCODING_VP6) {
    private_header_length =
        InsertVideoPrivateDataHeader (&pes_header[header_length],
        (pes_data_length + 1), viddec->codec.type);
  } else {
    private_header_length =
        InsertVideoPrivateDataHeader (&pes_header[header_length],
        pes_data_length, viddec->codec.type);
  }

  /* Update pes length */
  pes_length =
      pes_header[PES_LENGTH_BYTE_0] + (pes_header[PES_LENGTH_BYTE_1] << 8) +
      private_header_length;
  pes_header[PES_LENGTH_BYTE_0] = pes_length & 0xff;
  pes_header[PES_LENGTH_BYTE_1] = (pes_length >> 8) & 0xff;
  pes_header[PES_HEADER_DATA_LENGTH_BYTE] += private_header_length;
  pes_header[PES_FLAGS_BYTE] |= PES_EXTENSION_DATA_PRESENT;
  header_length += private_header_length;

  if (gst_stvideo_write (viddec, pes_header, header_length) == FALSE) {
    GST_ERROR_OBJECT (viddec, "failed to write data on video device");
    return FALSE;
  }

  if (viddec->codec.type == VIDEO_ENCODING_VP6) {
    guint8 *temp_data = NULL;

    temp_data = g_malloc0 (pes_data_length + 1);
    if (temp_data != NULL) {
      temp_data[0] = 0;
      memcpy ((temp_data + 1), pes_data, pes_data_length);
      if (gst_stvideo_write (viddec, temp_data, (pes_data_length + 1)) == FALSE) {
        g_free (temp_data);
        return FALSE;
      }
      g_free (temp_data);
    }
  } else {
    if (gst_stvideo_write (viddec, pes_data, pes_data_length) == FALSE) {
      GST_ERROR_OBJECT (viddec, "failed to write data on video device");
      return FALSE;
    }
  }

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return TRUE;
}

static gboolean
gst_stvideo_write_video_packet (Gststvideo * viddec, guint8 * pes_header,
    guint pes_header_length, guint8 * pes_data, guint pes_data_length,
    guint64 pts)
{
  guint fake_sc = 0;
  guint header_length;
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  /* Set start code */


  if (viddec->first_injection == TRUE) {
    viddec->first_injection = FALSE;
    /* WMV codec */
    if (((viddec->codec.type == VIDEO_ENCODING_WMV)
            || (viddec->codec.type == VIDEO_ENCODING_VC1))
        && (viddec->codec_data.buf != NULL)) {
      guint8 *pes_ptr;
      guint8 pes_packet[PES_MIN_HEADER_SIZE + 128];
      guint8 sequence_layer_sc[4] = {
        0x00, 0x00, 0x01, 0x80
      };
      guint8 metadata[] = {
        0x00, 0x00, 0x00, 0xc5, 0x04, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x00, 0x00, /* Struct C set for for advanced profile */
        0x00, 0x00, 0x00, 0x00, /* Struct A */
        0x00, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x00, /* Struct B */
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00
      };
      guint metadata_length;
      guint64 frame_rate = 0ull;
      GstMapInfo info_read;
      gst_buffer_map (viddec->codec_data.buf, &info_read, GST_MAP_READ);
      pes_ptr = &pes_packet[PES_MIN_HEADER_SIZE];

      if (viddec->codec.type == VIDEO_ENCODING_VC1) {
        memcpy (pes_ptr, sequence_layer_sc, sizeof (sequence_layer_sc));
        pes_ptr += sizeof (sequence_layer_sc);
      }

      memcpy (pes_ptr, metadata, sizeof (metadata));
      /* Struct C start */
      pes_ptr += 8;

      if (viddec->codec.type == VIDEO_ENCODING_WMV) {
        /* The codec private data is the same as structure C in annex J.
           The complete pes packet is constructed in accordance with Annex L. */
        memcpy (pes_ptr, info_read.data, 4);
      }
      pes_ptr += 4;

      /* Metadata Header Struct A */
      *pes_ptr++ = (viddec->properties.height >> 0) & 0xff;
      *pes_ptr++ = (viddec->properties.height >> 8) & 0xff;
      *pes_ptr++ = (viddec->properties.height >> 16) & 0xff;
      *pes_ptr++ = (viddec->properties.height >> 24);
      *pes_ptr++ = (viddec->properties.width >> 0) & 0xff;
      *pes_ptr++ = (viddec->properties.width >> 8) & 0xff;
      *pes_ptr++ = (viddec->properties.width >> 16) & 0xff;
      *pes_ptr++ = (viddec->properties.width >> 24);

      /* Metadata Header Struct B */
      /* Skip flag word and Struct B first 8 bytes */
      pes_ptr += 12;
      if (viddec->properties.fps_num != 0) {
        frame_rate =
            (10000000 * (guint64) viddec->properties.fps_den) /
            (guint64) viddec->properties.fps_num;
      }
      *pes_ptr++ = (frame_rate >> 0) & 0xff;
      *pes_ptr++ = (frame_rate >> 8) & 0xff;
      *pes_ptr++ = (frame_rate >> 16) & 0xff;
      *pes_ptr++ = (frame_rate >> 24);

      metadata_length = pes_ptr - &pes_packet[PES_MIN_HEADER_SIZE];

      header_length =
          InsertPesHeader (pes_packet, metadata_length,
          viddec->codec.start_code, INVALID_PTS_VALUE, 0);
      if (gst_stvideo_write (viddec, pes_packet,
              header_length + metadata_length) == FALSE) {
        gst_buffer_unmap (viddec->codec_data.buf, &info_read);
        return FALSE;
      }

      if (viddec->codec.type == VIDEO_ENCODING_VC1) {
        /* For VC1 the codec private data is a standard vc1 sequence header so we just copy it to the output */
        memcpy (&pes_packet[PES_MIN_HEADER_SIZE],
            info_read.data, info_read.size);

        header_length =
            InsertPesHeader (pes_packet,
            info_read.size, viddec->codec.start_code, INVALID_PTS_VALUE, 0);
        if (gst_stvideo_write (viddec, pes_packet,
                header_length + info_read.size) == FALSE) {
          GST_ERROR_OBJECT (viddec, "failed to write data on video device");
          gst_buffer_unmap (viddec->codec_data.buf, &info_read);
          return FALSE;
        }
      }
      gst_buffer_unmap (viddec->codec_data.buf, &info_read);
    } else if (viddec->codec.type == VIDEO_ENCODING_MJPEG) {
      guint8 sequence_layer_sc[2] = {
        0xff, 0xef
      };
      gint8 vendor_id[] = "STMicroelectronics";
      guint metadata[4];
      guint metadata_length;
      guint8 pes_packet[PES_MIN_HEADER_SIZE + 128];

      metadata[0] = (((guint64) viddec->properties.fps_num & 0x00FF) << 24) |
          (((guint64) viddec->properties.fps_num & 0xFF00) << 8) |
          (((guint64) viddec->properties.fps_num >> 8) & 0xFF00) |
          (((guint64) viddec->properties.fps_num >> 24) & 0x00FF);
      metadata[1] = (((guint64) viddec->properties.fps_den & 0x00FF) << 24) |
          (((guint64) viddec->properties.fps_den & 0xFF00) << 8) |
          (((guint64) viddec->properties.fps_den >> 8) & 0xFF00) |
          (((guint64) viddec->properties.fps_den >> 24) & 0x00FF);

      metadata_length = PES_MIN_HEADER_SIZE;
      memcpy (&pes_packet[metadata_length], sequence_layer_sc,
          sizeof (sequence_layer_sc));
      metadata_length += sizeof (sequence_layer_sc);
      memcpy (&pes_packet[metadata_length], vendor_id, sizeof (vendor_id));
      metadata_length += sizeof (vendor_id);
      memcpy (&pes_packet[metadata_length], metadata, sizeof (metadata));
      metadata_length += sizeof (metadata);

      header_length =
          InsertPesHeader (pes_packet, metadata_length,
          viddec->codec.start_code, INVALID_PTS_VALUE, 0);
      if (gst_stvideo_write (viddec, pes_packet,
              header_length + metadata_length) == FALSE) {
        GST_ERROR_OBJECT (viddec, "failed to write data on video device");
        return FALSE;
      }
    }
  }

  header_length =
      InsertPesHeader (pes_header, pes_data_length, viddec->codec.start_code,
      pts, fake_sc);
  if ((viddec->codec.type == VIDEO_ENCODING_THEORA)
      || (viddec->codec.type == VIDEO_ENCODING_FLV1)
      || (viddec->codec.type == VIDEO_ENCODING_VP6)
      || (viddec->codec.type == VIDEO_ENCODING_VP8)
      || (viddec->codec.type == VIDEO_ENCODING_VP9)
      || (viddec->codec.type == VIDEO_ENCODING_WMV)) {
    guint pes_length;
    guint private_header_length;

    private_header_length =
        InsertVideoPrivateDataHeader (&pes_header[header_length],
        pes_data_length, viddec->codec.type);

    /* Update pes length */
    pes_length =
        pes_header[PES_LENGTH_BYTE_0] + (pes_header[PES_LENGTH_BYTE_1] << 8) +
        private_header_length;
    pes_header[PES_LENGTH_BYTE_0] = pes_length & 0xff;
    pes_header[PES_LENGTH_BYTE_1] = (pes_length >> 8) & 0xff;
    pes_header[PES_HEADER_DATA_LENGTH_BYTE] += private_header_length;
    pes_header[PES_FLAGS_BYTE] |= PES_EXTENSION_DATA_PRESENT;
    header_length += private_header_length;
  } else if ((viddec->codec.type == VIDEO_ENCODING_VC1)
      && (viddec->codec_data.buf != NULL)) {
    guint8 vc1_frame_sc[] = {
      0x00, 0x00, 0x01, 0x0d
    };
    static gboolean frame_header_present = FALSE;

    if (!frame_header_present && (pes_data_length > 3)
        && (memcmp (pes_data, vc1_frame_sc, 4) == 0)) {
      frame_header_present = TRUE;
    }

    if (!frame_header_present) {
      memcpy (&pes_header[header_length], vc1_frame_sc, sizeof (vc1_frame_sc));
      header_length += sizeof (vc1_frame_sc);
    }
  }

  if (gst_stvideo_write (viddec, pes_header, header_length) == FALSE) {
    GST_ERROR_OBJECT (viddec, "failed to write data on video device");
    return FALSE;
  }
  if (gst_stvideo_write (viddec, pes_data, pes_data_length) == FALSE) {
    GST_ERROR_OBJECT (viddec, "failed to write data on video device");
    return FALSE;
  }

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  return TRUE;
}

static gboolean
gst_stvideo_injection_control (Gststvideo * viddec, GstBuffer * buf)
{
  gboolean ret = TRUE;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);

  if (viddec->segment.rate < 0.0)
    viddec->tm_control.reverse_injection = TRUE;
  else
    viddec->tm_control.reverse_injection = FALSE;

  if (gst_stvideo_write_packet (viddec, buf) == FALSE) {
    GST_ERROR_OBJECT (viddec, "failed to write data on video device");
    ret = FALSE;
  }

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  return ret;
}

static gboolean
gst_stvideo_push_dummy_packet (Gststvideo * viddec)
{
  GstBuffer *buffer;
  GstCaps *srccaps = NULL;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);

  if (viddec->downstream_frame_pushed == FALSE) {
    buffer = gst_buffer_new ();
    if (buffer != NULL) {
      /* Setting fake caps for dummy buffer */
      /* NOTE: framerate is mandatory for video_sink base class to go to PLAYING state */
      GST_BUFFER_TIMESTAMP (buffer) = GST_CLOCK_TIME_NONE;
      GST_BUFFER_DURATION (buffer) = GST_CLOCK_TIME_NONE;

      srccaps = gst_pad_get_current_caps (viddec->srcpad);
      if (srccaps == NULL) {
        srccaps = gst_caps_new_empty_simple ("video/x-fake-yuv");
      } else {
        srccaps = gst_caps_ref (srccaps);
        srccaps = gst_caps_make_writable (srccaps);
      }
      /* update width/height */
      gst_caps_set_simple (srccaps, "width", G_TYPE_INT,
          viddec->properties.width, "height", G_TYPE_INT,
          viddec->properties.height, NULL);
      /* update pixel aspect ratio */
      gst_caps_set_simple (srccaps, "pixel-aspect-ratio",
          GST_TYPE_FRACTION, viddec->properties.par_num,
          viddec->properties.par_den, NULL);
      /* update frame rate */
      gst_caps_set_simple (srccaps, "framerate", GST_TYPE_FRACTION,
          viddec->properties.fps_num, viddec->properties.fps_den, NULL);
      /* update video fd */
      gst_caps_set_simple (srccaps, "video-fd", G_TYPE_INT, viddec->fd, NULL);

      gst_pad_push_event (viddec->srcpad, gst_event_new_caps (srccaps));
      gst_caps_unref (srccaps);
      /* push new segment event */
      gst_pad_push_event (viddec->srcpad,
          gst_event_new_segment (&viddec->segment));
      /* Push dummy buffer to base sink */
      viddec->srcresult = gst_pad_push (viddec->srcpad, buffer);
      if (viddec->srcresult == GST_FLOW_OK) {
        viddec->downstream_frame_pushed = TRUE;
      } else {
        GST_ERROR_OBJECT (viddec,
            "Failed to push dummy buffer downstream -  %s",
            gst_flow_get_name (viddec->srcresult));
      }
    }
  }

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  return TRUE;
}

static gboolean
gst_stvideo_set_speed (Gststvideo * viddec, gdouble rate, gboolean set_policies)
{
  gint speed;
  video_play_state_t prev_state;
  gboolean ret = TRUE;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  if ((viddec->live == TRUE) && (rate != 1.0) && (rate != 0)) {
    GST_ERROR_OBJECT (viddec, "this rate %f can not be set", rate);
    return FALSE;
  }

  if ((rate == viddec->tm_control.rate) && (viddec->direction_change == FALSE)) {
    viddec->tm_control.rate = rate;
    return TRUE;
  }

  /* we need to check the direction of data injection to clear eos state.
     it's irrespective of values of rate. At change of direction of data flow
     eos state should be cleared so that based on new data flow direction
     eos can take place
   */
  if (((rate > 0) && (viddec->tm_control.set_rate < 0)) || ((rate < 0)
          && (viddec->tm_control.set_rate > 0))) {
    viddec->eos = FALSE;
    viddec->eos_set = FALSE;
  }

  if (set_policies == TRUE) {
    /* set default trick mode policies */
    gst_stvideo_manage_se_trickmode_policies (viddec, rate,
        GST_STVIDEO_DEFAULT);
  }

  speed = rate * DVB_SPEED_NORMAL_PLAY;
  prev_state = viddec->state;
  if ((viddec->state == VIDEO_FREEZED) && (rate < 0.0)) {
    ret =
        gst_stvideo_ioctl (viddec, VIDEO_SET_SPEED,
        (void *) DVB_SPEED_REVERSE_STOPPED);
  } else {
    ret = gst_stvideo_ioctl (viddec, VIDEO_SET_SPEED, (void *) speed);
    if (ret && speed != 0)
      viddec->state = VIDEO_PLAYING;
  }

  if (ret == TRUE) {
    GST_DEBUG_OBJECT (viddec, "new speed %d", speed);
    viddec->tm_control.rate = rate;

    /* Fast-backward */
    if (rate < 0.0) {
      /* FIXME - may not be needed because when rate is set,
       * flushing seek will already set discontinuity flag */
      viddec->discontinuity = TRUE;
    }

    /* SE starts playing after setting speed. */
    /* therefore restore the video freeze state if required */
    /* FIXME: it is workaround to avoid SE bad behaviour during set speed */
    GST_OBJECT_LOCK (viddec);
    if (prev_state == VIDEO_FREEZED && rate > 0.0) {
      if (gst_stvideo_ioctl (viddec, VIDEO_FREEZE, NULL) == TRUE) {
        viddec->state = VIDEO_FREEZED;
      } else {
        GST_DEBUG_OBJECT (viddec, "unable to restore VIDEO_FREEZE state");
        GST_OBJECT_UNLOCK (viddec);
        return FALSE;
      }
    }
    GST_OBJECT_UNLOCK (viddec);
  } else {
    GST_ERROR_OBJECT (viddec, "failed to set speed %d", speed);
    return FALSE;
  }

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return TRUE;
}

static void
gst_stvideo_set_live_play_policies (Gststvideo * viddec)
{
  struct video_command video_cmd;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);

  memset (&video_cmd, 0, sizeof (struct video_command));
  video_cmd.cmd = VIDEO_CMD_SET_OPTION;
  video_cmd.option.option = DVB_OPTION_DISCARD_LATE_FRAMES;
  video_cmd.option.value = DVB_OPTION_VALUE_DISCARD_LATE_FRAMES_NEVER;
  if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
    GST_WARNING_OBJECT (viddec,
        "Failed to set DVB_OPTION_VALUE_DISCARD_LATE_FRAMES_NEVER");
  } else {
    GST_DEBUG_OBJECT (viddec,
        "<DVB_OPTION_DISCARD_LATE_FRAMES : DVB_OPTION_VALUE_DISCARD_LATE_FRAMES_NEVER>");
  }
  video_cmd.option.option = DVB_OPTION_VIDEO_START_IMMEDIATE;
  video_cmd.option.value = DVB_OPTION_VALUE_DISABLE;
  if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
    GST_WARNING_OBJECT (viddec,
        "Failed to set DVB_OPTION_VIDEO_START_IMMEDIATE disable");
  } else {
    GST_DEBUG_OBJECT (viddec,
        "<DVB_OPTION_VIDEO_START_IMMEDIATE : DVB_OPTION_VALUE_DISABLE>");
  }
  video_cmd.option.option = DVB_OPTION_MASTER_CLOCK;
  video_cmd.option.value = DVB_OPTION_VALUE_SYSTEM_CLOCK_MASTER;
  if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
    GST_WARNING_OBJECT (viddec,
        "Failed to set DVB_OPTION_VALUE_SYSTEM_CLOCK_MASTER");
  } else {
    GST_DEBUG_OBJECT (viddec,
        "<DVB_OPTION_MASTER_CLOCK : DVB_OPTION_VALUE_SYSTEM_CLOCK_MASTER>");
  }
  video_cmd.option.option = DVB_OPTION_LIVE_PLAY;
  video_cmd.option.value = DVB_OPTION_VALUE_ENABLE;
  if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
    GST_WARNING_OBJECT (viddec, "Failed to set DVB_OPTION_LIVE_PLAY");
  } else {
    GST_DEBUG_OBJECT (viddec,
        "<DVB_OPTION_LIVE_PLAY : DVB_OPTION_VALUE_ENABLE>");
  }
  video_cmd.option.option = DVB_OPTION_REBASE_ON_DATA_DELIVERY_LATE;
  video_cmd.option.value = DVB_OPTION_VALUE_DISABLE;
  if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
    GST_WARNING_OBJECT (viddec,
        "Failed to disable DVB_OPTION_REBASE_ON_DATA_DELIVERY_LATE");
  } else {
    GST_DEBUG_OBJECT (viddec,
        "<DVB_OPTION_REBASE_ON_DATA_DELIVERY_LATE : DVB_OPTION_VALUE_DISABLE>");
  }
  video_cmd.option.option = DVB_OPTION_REBASE_ON_FRAME_DECODE_LATE;
  video_cmd.option.value = DVB_OPTION_VALUE_DISABLE;
  if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
    GST_WARNING_OBJECT (viddec,
        "Failed to disable DVB_OPTION_REBASE_ON_FRAME_DECODE_LATE");
  } else {
    GST_DEBUG_OBJECT (viddec,
        "<DVB_OPTION_REBASE_ON_FRAME_DECODE_LATE : DVB_OPTION_VALUE_DISABLE>");
  }

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  return;
}

static void
gst_stvideo_set_file_play_policies (Gststvideo * viddec)
{
  struct video_command video_cmd;
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  memset (&video_cmd, 0, sizeof (struct video_command));
  video_cmd.cmd = VIDEO_CMD_SET_OPTION;
  video_cmd.option.option = DVB_OPTION_DISCARD_LATE_FRAMES;
  video_cmd.option.value =
      DVB_OPTION_VALUE_DISCARD_LATE_FRAMES_AFTER_SYNCHRONIZE;
  if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
    GST_WARNING_OBJECT (viddec,
        "Failed to set DVB_OPTION_VALUE_DISCARD_LATE_FRAMES_AFTER_SYNCHRONIZE");
  } else {
    GST_DEBUG_OBJECT (viddec,
        "<DVB_OPTION_DISCARD_LATE_FRAMES : DVB_OPTION_VALUE_DISCARD_LATE_FRAMES_AFTER_SYNCHRONIZE>");
  }

  video_cmd.option.option = DVB_OPTION_VIDEO_START_IMMEDIATE;
  video_cmd.option.value = DVB_OPTION_VALUE_ENABLE;
  if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
    GST_WARNING_OBJECT (viddec,
        "Failed to disable DVB_OPTION_VIDEO_START_IMMEDIATE");
  } else {
    GST_DEBUG_OBJECT (viddec,
        "<DVB_OPTION_VIDEO_START_IMMEDIATE : DVB_OPTION_VALUE_ENABLE>");
  }
  video_cmd.option.option = DVB_OPTION_MASTER_CLOCK;
  video_cmd.option.value = DVB_OPTION_VALUE_VIDEO_CLOCK_MASTER;
  if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
    GST_WARNING_OBJECT (viddec,
        "Failed to set DVB_OPTION_VALUE_VIDEO_CLOCK_MASTER");
  } else {
    GST_DEBUG_OBJECT (viddec,
        "<DVB_OPTION_MASTER_CLOCK : DVB_OPTION_VALUE_VIDEO_CLOCK_MASTER>");
  }
  video_cmd.option.option = DVB_OPTION_LIVE_PLAY;
  video_cmd.option.value = DVB_OPTION_VALUE_DISABLE;
  if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
    GST_WARNING_OBJECT (viddec, "Failed to disable DVB_OPTION_LIVE_PLAY");
  } else {
    GST_DEBUG_OBJECT (viddec,
        "<DVB_OPTION_LIVE_PLAY : DVB_OPTION_VALUE_DISABLE>");
  }
  video_cmd.option.option = DVB_OPTION_REBASE_ON_DATA_DELIVERY_LATE;
  video_cmd.option.value = DVB_OPTION_VALUE_ENABLE;
  if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
    GST_WARNING_OBJECT (viddec,
        "Failed to enable DVB_OPTION_REBASE_ON_DATA_DELIVERY_LATE");
  } else {
    GST_DEBUG_OBJECT (viddec,
        "<DVB_OPTION_REBASE_ON_DATA_DELIVERY_LATE : DVB_OPTION_VALUE_ENABLE>");
  }
  video_cmd.option.option = DVB_OPTION_REBASE_ON_FRAME_DECODE_LATE;
  video_cmd.option.value = DVB_OPTION_VALUE_ENABLE;
  if (!gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd)) {
    GST_WARNING_OBJECT (viddec,
        "Failed to enable DVB_OPTION_REBASE_ON_FRAME_DECODE_LATE");
  } else {
    GST_DEBUG_OBJECT (viddec,
        "<DVB_OPTION_REBASE_ON_FRAME_DECODE_LATE : DVB_OPTION_VALUE_ENABLE>");
  }

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  return;
}

static void
gst_stvideo_manage_se_trickmode_policies (Gststvideo * viddec, gdouble rate,
    GstStVideoTrickPolicyType policy)
{
  struct video_command video_cmd;

  GST_LOG_OBJECT (viddec, "%s:%d\n", __FUNCTION__, __LINE__);

  memset (&video_cmd, 0, sizeof (struct video_command));
  video_cmd.cmd = VIDEO_CMD_SET_OPTION;
  switch (policy) {
    case GST_STVIDEO_DEFAULT:
      if (viddec->skipflag) {
        GST_DEBUG_OBJECT (viddec,
            "TRICK MODE POLICY - GST_STVIDEO_DEFAULT (KEY FRAMES)");
        video_cmd.option.option = DVB_OPTION_TRICK_MODE_DOMAIN;
        video_cmd.option.value = DVB_OPTION_VALUE_TRICK_MODE_DECODE_KEY_FRAMES;
        gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd);
      } else if ((rate > 0) && (rate <= 1.0)) {
        GST_DEBUG_OBJECT (viddec,
            "TRICK MODE POLICY - GST_STVIDEO_DEFAULT (ALL FRAMES-DECODE ALL)");
        video_cmd.option.option = DVB_OPTION_TRICK_MODE_DOMAIN;
        video_cmd.option.value = DVB_OPTION_VALUE_TRICK_MODE_DECODE_ALL;
        gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd);
      } else {
        GST_DEBUG_OBJECT (viddec,
            "TRICK MODE POLICY - GST_STVIDEO_DEFAULT (ALL FRAMES-AUTO MODE)");
        video_cmd.option.option = DVB_OPTION_TRICK_MODE_DOMAIN;
        video_cmd.option.value = DVB_OPTION_VALUE_TRICK_MODE_AUTO;
        gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd);
      }
      break;
    case GST_STVIDEO_KF_SE:
      GST_DEBUG_OBJECT (viddec, "TRICK MODE POLICY - SE EVENT (KEY FRAMES)");
      video_cmd.option.option = DVB_OPTION_TRICK_MODE_DOMAIN;
      video_cmd.option.value = DVB_OPTION_VALUE_TRICK_MODE_DECODE_KEY_FRAMES;
      gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd);
      GST_DEBUG_OBJECT (viddec,
          "<DVB_OPTION_TRICK_MODE_DOMAIN : DVB_OPTION_VALUE_TRICK_MODE_DECODE_KEY_FRAMES>");
      break;
    case GST_STVIDEO_ALL_SE:
      GST_DEBUG_OBJECT (viddec,
          "TRICK MODE POLICY - SE EVENT (ALL FRAMES-AUTO MODE)");
      video_cmd.option.option = DVB_OPTION_TRICK_MODE_DOMAIN;
      video_cmd.option.value = DVB_OPTION_VALUE_TRICK_MODE_AUTO;
      gst_stvideo_ioctl (viddec, VIDEO_COMMAND, (void *) &video_cmd);
      break;
    default:
      break;
  }
}

/*
  * Function name : gst_stvideo_get_pool
  *  Input        :
  *    viddec - stvideo instance
  *
  *  Description  : Queries the downstream element
  *                 for a pool.
  *
  * Return        : pool
*/
static GstBufferPool *
gst_stvideo_get_pool (Gststvideo * viddec)
{
  GstQuery *query = NULL;
  GstBufferPool *pool = NULL;
  guint size, min, max;

  /* Query for the pool for keeping the capture data */
  query =
      gst_query_new_allocation (gst_pad_get_current_caps (viddec->srcpad),
      TRUE);
  if (query) {
    if (gst_pad_peer_query (viddec->srcpad, query)) {
      if (gst_query_get_n_allocation_pools (query) > 0) {
        gst_query_parse_nth_allocation_pool (query, 0, &pool, &size, &min,
            &max);
        if (pool != NULL) {
          gst_buffer_pool_set_active (pool, TRUE);
          gst_object_ref (pool);
        } else {
          GST_WARNING_OBJECT (viddec, "could not get pool from downstream!");
        }
      } else {
        GST_WARNING_OBJECT (viddec, "could not get pool from downstream!");
      }
    }
    gst_query_unref (query);
  }
  return pool;
}

/*
  * Function name : gst_stvideo_update_src_pad_caps
  *  Input        :
  *    viddec - stvideo instance
  *
  *  Description  : Sets updated caps (width, height,
  *                 PAR and framerate) on srcpad.
  *
  * Return        : void
*/
static void
gst_stvideo_update_src_pad_caps (Gststvideo * viddec)
{
  GstCaps *srccaps = NULL;
  /* get the current caps on the srcpad */
  srccaps = gst_pad_get_current_caps (viddec->srcpad);
  srccaps = gst_caps_make_writable (srccaps);
  /* update width/height */
  gst_caps_set_simple (srccaps, "width", G_TYPE_INT,
      viddec->capture.width, "height", G_TYPE_INT,
      viddec->capture.height, NULL);
  /* update pixel aspect ratio */
  gst_caps_set_simple (srccaps, "pixel-aspect-ratio",
      GST_TYPE_FRACTION, viddec->properties.par_num,
      viddec->properties.par_den, NULL);
  /* update frame rate */
  gst_caps_set_simple (srccaps, "framerate", GST_TYPE_FRACTION,
      viddec->properties.fps_num, viddec->properties.fps_den, NULL);
  /* set the updated caps on the srcpad */
  gst_pad_set_caps (viddec->srcpad, srccaps);
  gst_caps_unref (srccaps);
}

/*
  * Function name : gst_stvideo_capture_set_format
  *  Input        :
  *    viddec - stvideo instance
  *
  *  Description  : Sets new format (changed width/height)
  *                 for capture.
  *
  * Return        : TRUE if able to set format else FALSE
*/
static gboolean
gst_stvideo_capture_set_format (Gststvideo * viddec)
{
  if (viddec->capture.is_stream_on_done) {
    /* free the captured buffer if any */
    gst_stvideo_capture_free_buffer (viddec, viddec->v4l2_fd);

    /* stream off the ongoing stream before changing Format */
    if (gst_stvideo_capture_stream_off (viddec, viddec->v4l2_fd) == FALSE) {
      GST_WARNING_OBJECT (viddec, "Capture Stream Off failed");
      return FALSE;
    }
    viddec->capture.is_stream_on_done = FALSE;
  }

  struct v4l2_format fmt;
  /* Configure capture ouput */
  memset (&fmt, 0, sizeof (struct v4l2_format));
  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  fmt.fmt.pix.width = viddec->capture.width;
  fmt.fmt.pix.height = viddec->capture.height;
  fmt.fmt.pix.field = V4L2_FIELD_ANY;

  fmt.fmt.pix.pixelformat = viddec->capture.pixel_format;

  /* Configure colorspace based on pixel format */
  switch (fmt.fmt.pix.pixelformat) {
    case V4L2_PIX_FMT_BGR24:
    case V4L2_PIX_FMT_BGR32:
      fmt.fmt.pix.colorspace = V4L2_COLORSPACE_SRGB;
      break;
    case V4L2_PIX_FMT_YUYV:
    case V4L2_PIX_FMT_UYVY:
      fmt.fmt.pix.colorspace = V4L2_COLORSPACE_REC709;
      break;
    default:
      GST_ERROR_OBJECT (viddec,
          "Unexpected pixelformat, setting colorspace V4L2_COLORSPACE_REC709\n");
      fmt.fmt.pix.colorspace = V4L2_COLORSPACE_REC709;
      break;
  }

  if (ioctl (viddec->v4l2_fd, VIDIOC_S_FMT, &fmt) < 0) {
    GST_ERROR_OBJECT (viddec, "VIDIOC_S_FMT failed %s", strerror (errno));
    return FALSE;
  }

  return TRUE;
}

static void
gst_stvideo_handle_size_changed_event (Gststvideo * viddec, void *event)
{
  struct video_event *vid_event = event;
  struct v4l2_cropcap cropcap;
  gboolean par_changed = FALSE;
  gboolean resolution_changed = FALSE;
  gboolean decoder_paused = FALSE;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  if ((!viddec->tunneled) && (GST_PAD_TASK (viddec->srcpad))
      && (gst_task_get_state (GST_PAD_TASK (viddec->srcpad)) !=
          GST_TASK_STOPPED)) {
    /* Set the crop */
    memset (&cropcap, 0, sizeof (cropcap));
    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    /* Get the pixel aspect ratio information */
    if (ioctl (viddec->v4l2_fd, VIDIOC_CROPCAP, &cropcap) != 0) {
      GST_ERROR_OBJECT (viddec, "erorr occured fd is %d %s\n", viddec->v4l2_fd,
          strerror (errno));
      return;
    }
    if ((cropcap.pixelaspect.numerator != viddec->properties.par_num)
        || (cropcap.pixelaspect.denominator != viddec->properties.par_den)) {
      viddec->properties.par_num = cropcap.pixelaspect.numerator;
      viddec->properties.par_den = cropcap.pixelaspect.denominator;
      par_changed = TRUE;
    }
  }
  /* Normally we should be able to get width and height params from demuxers,
     but for some demuxers (sttsdemux), there is no width and height params.
     So we have to retrieve width and height params from SE and
     then send to downstream element (such as sttextoverlay).
     Sttextoverlay needs video width and height
     to determine the subtitle area
   */

  if (((vid_event->u.size.w != viddec->properties.width)
          && (vid_event->u.size.w != viddec->capture.width))
      || ((vid_event->u.size.h != viddec->properties.height)
          && (vid_event->u.size.h != viddec->capture.height))) {
    viddec->properties.width = vid_event->u.size.w;
    viddec->properties.height = vid_event->u.size.h;
    viddec->capture.width = viddec->properties.width;
    viddec->capture.height = viddec->properties.height;

    switch (viddec->decimation) {
      case 1:
        GST_DEBUG_OBJECT (viddec, "Decimation 1");
        viddec->capture.width = viddec->capture.width / 2;
        viddec->capture.height = viddec->capture.height / 2;
        break;
      case 2:
        GST_DEBUG_OBJECT (viddec, "Decimation 2");
        viddec->capture.width = viddec->capture.width / 4;
        viddec->capture.height = viddec->capture.height / 2;
        break;
      case 3:
        GST_DEBUG_OBJECT (viddec, "Decimation 3");
        viddec->capture.width = viddec->capture.width / 2;
        viddec->capture.height = viddec->capture.height / 4;
        break;
      case 4:
        GST_DEBUG_OBJECT (viddec, "Decimation 4");
        viddec->capture.width = viddec->capture.width / 2;
        viddec->capture.height = viddec->capture.height / 8;
        break;
      case 5:
        GST_DEBUG_OBJECT (viddec, "Decimation 5");
        viddec->capture.width = viddec->capture.width / 4;
        viddec->capture.height = viddec->capture.height / 4;
        break;
      case 6:
        GST_DEBUG_OBJECT (viddec, "Decimation 6");
        viddec->capture.width = viddec->capture.width / 4;
        viddec->capture.height = viddec->capture.height / 8;
        break;
      case 7:
        GST_DEBUG_OBJECT (viddec, "Decimation 7");
        viddec->capture.width = viddec->capture.width / 8;
        viddec->capture.height = viddec->capture.height / 2;
        break;
      case 8:
        GST_DEBUG_OBJECT (viddec, "Decimation 8");
        viddec->capture.width = viddec->capture.width / 8;
        viddec->capture.height = viddec->capture.height / 4;
        break;
      case 9:
        GST_DEBUG_OBJECT (viddec, "Decimation 9");
        viddec->capture.width = viddec->capture.width / 8;
        viddec->capture.height = viddec->capture.height / 8;
        break;
      default:
        GST_DEBUG_OBJECT (viddec, "Decimation 0");
        break;
    }

    if (viddec->capture.width % 16 != 0) {
      viddec->capture.width = (viddec->capture.width / 16 + 1) * 16;
    }
    GST_DEBUG_OBJECT (viddec, "After Decimation (%d, %d)",
        viddec->capture.width, viddec->capture.height);

    resolution_changed = TRUE;
    viddec->capture.task_start_wait_for_size = FALSE;
  }
  /* send dummy packet to downstream element to notify the width and height change */
  if ((gst_pad_is_linked (viddec->srcpad)) && (viddec->tunneled)) {
    GST_DEBUG_OBJECT (viddec, "video size changed (%d, %d)",
        viddec->properties.width, viddec->properties.height);
    viddec->downstream_frame_pushed = FALSE;
    g_mutex_lock (&viddec->dummy_buff.mutex);
    GST_STVIDEO_VIDEO_LOOP_SIGNAL (viddec);
    g_mutex_unlock (&viddec->dummy_buff.mutex);
  } else if (!viddec->tunneled) {
    if (viddec->capture.task_start_delayed) {
      if ((!viddec->capture.task_start_wait_for_size)
          && (!viddec->capture.task_start_wait_for_framerate)) {
        /* freeze the decoder in order to avoid data overwrite
           in SE in capture mode */
        if (viddec->state == VIDEO_PLAYING) {
          if (gst_stvideo_ioctl (viddec, VIDEO_FREEZE, NULL)) {
            viddec->state = VIDEO_FREEZED;
            decoder_paused = TRUE;
          }
        }
        /* update the src pad caps */
        gst_stvideo_update_src_pad_caps (viddec);
        if (gst_stvideo_capture_setup_device (viddec)) {
          if (gst_pad_start_task (viddec->srcpad,
                  (GstTaskFunction) gst_stvideo_video_loop,
                  viddec, NULL) != TRUE) {
            GST_ERROR_OBJECT (viddec, "cannot start capture task");
          }
        }
        /* Resume the decoder */
        if ((viddec->state == VIDEO_FREEZED) && (decoder_paused)) {
          if (gst_stvideo_ioctl (viddec, VIDEO_PLAY, NULL)) {
            viddec->state = VIDEO_PLAYING;
          }
        }
        viddec->capture.task_start_delayed = FALSE;
      }
    } else if (par_changed || resolution_changed) {
      /* FIXME: Dynamic resolution change has to be checked
         and analyzed for the case when it occurs in
         paused pipeline.
       */
      /* pause the streaming thread */
      GstTask *task = GST_PAD_TASK (viddec->srcpad);
      gst_task_pause (task);
      /* release the video loop task struck in DQBUF ioctl */
      gst_stvideo_ioctl (viddec, VIDEO_DISCONTINUITY,
          (void *) VIDEO_DISCONTINUITY_EOS);
      GST_PAD_STREAM_LOCK (viddec->srcpad);
      GST_PAD_STREAM_UNLOCK (viddec->srcpad);
      /* freeze the decoder in order to avoid data overwrite
         in SE in capture mode */
      if (viddec->state == VIDEO_PLAYING) {
        if (gst_stvideo_ioctl (viddec, VIDEO_FREEZE, NULL)) {
          viddec->state = VIDEO_FREEZED;
          decoder_paused = TRUE;
        }
      }
      /* set the changed format on v4l2 for capture */
      if (resolution_changed) {
        if (!gst_stvideo_capture_set_format (viddec)) {
          GST_ERROR_OBJECT (viddec, "failed to set format");
          return;
        }

        /* Make sure all serialized data is consumed */
        /* call drain query to make sure all serialized data
           is consumed
         */
        GstQuery *query = gst_query_new_drain ();
        if (query) {
          gst_pad_peer_query (viddec->srcpad, query);
          gst_query_unref (query);
        }
        /* deactivate the buffer pool */
        if (viddec->capture.pool) {
          gst_buffer_pool_set_active (viddec->capture.pool, FALSE);
        }
        /* unref existing pool */
        if (viddec->capture.pool) {
          gst_object_unref (viddec->capture.pool);
          viddec->capture.pool = NULL;
        }
      }
      /* update the src pad caps */
      gst_stvideo_update_src_pad_caps (viddec);

      if (resolution_changed) {
        /* Get new pool */
        viddec->capture.pool = gst_stvideo_get_pool (viddec);
        /* activate the buffer pool */
        if (viddec->capture.pool) {
          gst_buffer_pool_set_active (viddec->capture.pool, TRUE);
        }
      }
      /* Resume the task */
      gst_pad_start_task (viddec->srcpad,
          (GstTaskFunction) gst_stvideo_video_loop, viddec, NULL);
      /* Resume the decoder */
      if ((viddec->state == VIDEO_FREEZED) && (decoder_paused)) {
        if (gst_stvideo_ioctl (viddec, VIDEO_PLAY, NULL)) {
          viddec->state = VIDEO_PLAYING;
        }
      }
    }
  }
}

static void
gst_stvideo_handle_framerate_changed_event (Gststvideo * viddec, void *event)
{
  struct video_event *vid_event = event;

  /* Normally we should be able to get framerate from demuxers,
     but for some demuxers (sttsdemux), there is no framerate.
     So we have to retrieve framerate from SE and
     then send to downstream element.
   */
  if ((((gint) vid_event->u.frame_rate / GST_STVIDEO_FRAME_RATE_MULTIPLIER) !=
          (gint) (viddec->properties.fps_num / viddec->properties.fps_den))
      && (viddec->capture.task_start_wait_for_framerate)) {
    /* Handle framerate change event if framerate is not already set by demuxer */
    GST_DEBUG_OBJECT (viddec, "VIDEO_EVENT_FRAMERATE_CHANGED");
    viddec->properties.fps_num =
        (gint) vid_event->u.frame_rate / GST_STVIDEO_FRAME_RATE_MULTIPLIER;
    viddec->properties.fps_den = 1;

    /* update the src pad caps */
    gst_stvideo_update_src_pad_caps (viddec);

    viddec->capture.task_start_wait_for_framerate = FALSE;
    GST_DEBUG_OBJECT (viddec, "video framerate changed (%d, %d)",
        viddec->properties.fps_num, viddec->properties.fps_den);

    /* send dummy packet to downstream element to notify the framerate change */
    if ((gst_pad_is_linked (viddec->srcpad)) && (viddec->tunneled)) {
      viddec->downstream_frame_pushed = FALSE;
      g_mutex_lock (&viddec->dummy_buff.mutex);
      GST_STVIDEO_VIDEO_LOOP_SIGNAL (viddec);
      g_mutex_unlock (&viddec->dummy_buff.mutex);
    } else if ((!viddec->tunneled) && (viddec->capture.task_start_delayed)
        && (!viddec->capture.task_start_wait_for_size)
        && (!viddec->capture.task_start_wait_for_framerate)) {
      if (gst_stvideo_capture_setup_device (viddec)) {
        gst_pad_start_task (viddec->srcpad,
            (GstTaskFunction) gst_stvideo_video_loop, viddec, NULL);
      }
      viddec->capture.task_start_delayed = FALSE;
    }
  }
}

static void
gst_stvideo_handle_trickmode_change_event (Gststvideo * viddec, void *event)
{
  struct video_event *vid_event = event;
  GstStructure *structure;
  GstEvent *upstream_event;

  if (viddec->tm_control.upstream_rap_index_support == FALSE) {
    return;
  }

  switch (vid_event->u.frame_rate) {
    case DVB_OPTION_VALUE_TRICK_MODE_AUTO:
      GST_DEBUG_OBJECT (viddec, "DVB_OPTION_VALUE_TRICK_MODE_AUTO");
      break;
    case DVB_OPTION_VALUE_TRICK_MODE_DECODE_ALL:
      GST_DEBUG_OBJECT (viddec, "DVB_OPTION_VALUE_TRICK_MODE_DECODE_ALL");
      break;
    case DVB_OPTION_VALUE_TRICK_MODE_DECODE_ALL_DEGRADE_NON_REFERENCE_FRAMES:
      GST_DEBUG_OBJECT (viddec,
          "DVB_OPTION_VALUE_TRICK_MODE_DECODE_ALL_DEGRADE_NON_REFERENCE_FRAMES");
      break;
    case DVB_OPTION_VALUE_TRICK_MODE_START_DISCARDING_NON_REFERENCE_FRAMES:
      GST_DEBUG_OBJECT (viddec,
          "DVB_OPTION_VALUE_TRICK_MODE_START_DISCARDING_NON_REFERENCE_FRAMES");
      break;
    case DVB_OPTION_VALUE_TRICK_MODE_DECODE_REFERENCE_FRAMES_DEGRADE_NON_KEY_FRAMES:
      GST_DEBUG_OBJECT (viddec,
          "DVB_OPTION_VALUE_TRICK_MODE_DECODE_REFERENCE_FRAMES_DEGRADE_NON_KEY_FRAMES");
      break;
    case DVB_OPTION_VALUE_TRICK_MODE_DECODE_KEY_FRAMES:
      GST_DEBUG_OBJECT (viddec,
          "DVB_OPTION_VALUE_TRICK_MODE_DECODE_KEY_FRAMES");
      break;
    case DVB_OPTION_VALUE_TRICK_MODE_DISCONTINUOUS_KEY_FRAMES:
      GST_DEBUG_OBJECT (viddec,
          "DVB_OPTION_VALUE_TRICK_MODE_DISCONTINUOUS_KEY_FRAMES");
      break;
    default:
      GST_DEBUG_OBJECT (viddec, "trick mode policy - unknown event");
      break;
  }

  if (vid_event->u.frame_rate >= DVB_OPTION_VALUE_TRICK_MODE_DECODE_KEY_FRAMES) {
    /* SE event - inject only key frames and setup key frame policy */
    gst_stvideo_manage_se_trickmode_policies (viddec,
        viddec->tm_control.rate, GST_STVIDEO_KF_SE);
    structure = gst_structure_new_empty ("rap-injection");
    if (structure) {
      gst_structure_set (structure, "rap-enable", G_TYPE_BOOLEAN, TRUE, NULL);
      upstream_event =
          gst_event_new_custom (GST_EVENT_CUSTOM_UPSTREAM, structure);
      gst_pad_push_event (viddec->sinkpad, upstream_event);
    }
    /* Flush the buffer so that we can get to injected keyframe,
       if not SE will take time to consume the GOP */
    if (viddec->tm_control.rate > 0) {
      gst_stvideo_ioctl (viddec, VIDEO_CLEAR_BUFFER, NULL);
    }
  } else if (vid_event->u.frame_rate == DVB_OPTION_VALUE_TRICK_MODE_DECODE_ALL) {
    /* SE event - inject all frames, setup all frames policy, ignored if application set key frames policy */
    if (viddec->skipflag == FALSE) {
      gst_stvideo_manage_se_trickmode_policies (viddec, viddec->tm_control.rate,
          GST_STVIDEO_ALL_SE);
      if (viddec->tm_control.rate < 0) {
        gst_stvideo_set_speed (viddec, viddec->tm_control.rate, FALSE);
      }
      structure = gst_structure_new_empty ("rap-injection");
      if (structure) {
        gst_structure_set (structure, "rap-enable", G_TYPE_BOOLEAN, FALSE,
            NULL);
        event = gst_event_new_custom (GST_EVENT_CUSTOM_UPSTREAM, structure);
        gst_pad_push_event (viddec->sinkpad, event);
      }
    }
  }
}

static gboolean
gst_stvideo_capture_setup_device (Gststvideo * viddec)
{
  struct v4l2_input v4l2input;
  gchar *linuxdvb_input;
  gint index = 0;
  gboolean ret = TRUE;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  /* Choose the LinuxDVB decoder input to use according to the dev-id property        */
  linuxdvb_input = g_strdup_printf ("dvb0.video%01d", viddec->dev_id);
  GST_INFO_OBJECT (viddec, "DVB Input = %s\n", linuxdvb_input);

  /* Configure capture input */
  memset (&v4l2input, 0, sizeof (struct v4l2_input));
  for (v4l2input.index = 0;; v4l2input.index++) {
    if (ioctl (viddec->v4l2_fd, VIDIOC_ENUMINPUT, &v4l2input) < 0)
      break;

    if (strcmp ((char *) v4l2input.name, linuxdvb_input) == 0) {
      index = v4l2input.index;
      break;
    }
  }

  if (ioctl (viddec->v4l2_fd, VIDIOC_S_INPUT, &index) < 0) {
    GST_ERROR_OBJECT (viddec, "Couldn't configure capture input to %s\n",
        linuxdvb_input);
    ret = FALSE;
    goto exit;
  }

  GST_INFO_OBJECT (viddec, "Selected LinuxDVB outputs %d (%s)\n", index,
      linuxdvb_input);

  if (!gst_stvideo_capture_set_format (viddec)) {
    GST_ERROR_OBJECT (viddec, "failed to set format");
    ret = FALSE;
    goto exit;
  }

  /* Getthe pool for keeping the capture data */
  viddec->capture.pool = gst_stvideo_get_pool (viddec);

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);

exit:
  g_free (linuxdvb_input);
  return ret;
}

static gboolean
gst_stvideo_capture_queue_buffer (Gststvideo * viddec)
{
  GstFlowReturn ret = GST_FLOW_OK;
  struct v4l2_buffer buf;
  struct v4l2_video_uncompressed_metadata video_meta;
  gboolean buffer_allocated = FALSE;
  GstAllocationParams params = {
    0
  };

  GST_LOG_OBJECT (viddec, "%s:%d\n", __FUNCTION__, __LINE__);
  /* REVIEW/CRITICAL: return value of _acquire_buffer is not propagated upstream */

  if (viddec->capture.pool != NULL) {
    ret =
        gst_buffer_pool_acquire_buffer (viddec->capture.pool,
        &viddec->capture.buffer, NULL);
  } else {
    gint buf_size =
        viddec->capture.width * viddec->capture.height *
        viddec->capture.bpp / 8;
    viddec->capture.buffer = gst_buffer_new_allocate (NULL, buf_size, &params);
  }

  if (ret == GST_FLOW_OK) {
    if (viddec->capture.pool) {
      GstMapInfo info_read;
      if (!viddec->capture.is_stream_on_done) {
        GST_DEBUG_OBJECT (viddec,
            "Configuring Capture Buffer and Stream On for USERPTR ");
        viddec->capture.mem_type = GST_STVIDEO_USER_PTR;

        if (gst_stvideo_capture_alloc_buffer (viddec, viddec->v4l2_fd) == FALSE) {
          GST_ERROR_OBJECT (viddec, "Capture Alloc Buffer failed");
          viddec->srcresult = GST_FLOW_ERROR;
          goto exit_error;
        }
        buffer_allocated = TRUE;

        if (gst_stvideo_capture_stream_on (viddec, viddec->v4l2_fd) == FALSE) {
          GST_ERROR_OBJECT (viddec, "Capture Stream On failed");
          viddec->srcresult = GST_FLOW_ERROR;
          goto exit_error;
        }
        viddec->capture.is_stream_on_done = TRUE;
      }

      gst_buffer_map (viddec->capture.buffer, &info_read, GST_MAP_READ);
      memset (&buf, 0, sizeof (struct v4l2_buffer));
      buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      buf.memory = V4L2_MEMORY_USERPTR;
      buf.m.userptr = (unsigned long) info_read.data;
      buf.field = V4L2_FIELD_NONE;
      buf.length =
          viddec->capture.width * viddec->capture.height *
          viddec->capture.bpp / 8;
      buf.reserved = (int) &video_meta;
      gst_buffer_unmap (viddec->capture.buffer, &info_read);
      if (ioctl (viddec->v4l2_fd, VIDIOC_QBUF, &buf) < 0) {
        GST_ERROR_OBJECT (viddec, "Couldn't queue buffer - %s",
            strerror (errno));
        goto exit_error;
      }
    } else {
      if (viddec->capture.buffer) {
        if (!viddec->capture.is_stream_on_done) {
          GST_DEBUG_OBJECT (viddec,
              "Configuring Capture Buffer and Stream On for MMAP ");
          viddec->capture.mem_type = GST_STVIDEO_MMAP;

          if (gst_stvideo_capture_alloc_buffer (viddec,
                  viddec->v4l2_fd) == FALSE) {
            GST_ERROR_OBJECT (viddec, "Capture Alloc Buffer failed");
            viddec->srcresult = GST_FLOW_ERROR;
            goto exit_error;
          }
          buffer_allocated = TRUE;

          if (gst_stvideo_capture_stream_on (viddec, viddec->v4l2_fd) == FALSE) {
            GST_ERROR_OBJECT (viddec, "Capture Stream On failed");
            viddec->srcresult = GST_FLOW_ERROR;
            goto exit_error;
          }
          viddec->capture.is_stream_on_done = TRUE;
        }

        viddec->capture.v4l2_capture_buf.flags = 0;
        viddec->capture.v4l2_capture_buf.index = 0;
        viddec->capture.v4l2_capture_buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        viddec->capture.v4l2_capture_buf.memory = V4L2_MEMORY_MMAP;
        if (ioctl (viddec->v4l2_fd, VIDIOC_QBUF,
                &viddec->capture.v4l2_capture_buf) < 0) {
          GST_ERROR_OBJECT (viddec, "Couldn't queue buffer - %s",
              strerror (errno));
          goto exit_error;
        }
      } else {
        /* Need to allocate a GstBuffer and push downstream in this case */
        GST_ERROR_OBJECT (viddec,
            "Invalid Case, should not have reached here. \n");
        goto exit_error;
      }
    }
  } else {
    GST_ERROR_OBJECT (viddec, "Failed to allocate buffer, flow error - %s",
        gst_flow_get_name (ret));
    viddec->srcresult = ret;
    goto exit_error;
  }
  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  return TRUE;

exit_error:
  if (viddec->capture.buffer) {
    gst_buffer_unref (viddec->capture.buffer);
    viddec->capture.buffer = NULL;
  }

  if (buffer_allocated == TRUE) {
    gst_stvideo_capture_free_buffer (viddec, viddec->v4l2_fd);
  }

  return FALSE;
}

static gboolean
gst_stvideo_capture_dequeue_buffer (Gststvideo * viddec)
{
  struct v4l2_buffer buf;
  GstClockTime ev_ts;
  GstClockTime start, stop;
  guint64 cstart, cstop;
  GstSegment *segment;
  GstClockTime duration;

  if (viddec->capture.mem_type == GST_STVIDEO_INVALID) {
    GST_ERROR_OBJECT (viddec, "Not valid memory type");
    return FALSE;
  }

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);
  memset (&buf, 0, sizeof (struct v4l2_buffer));
  if (viddec->capture.mem_type == GST_STVIDEO_USER_PTR) {
    buf.memory = V4L2_MEMORY_USERPTR;
  } else {
    buf.memory = V4L2_MEMORY_MMAP;
  }
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl (viddec->v4l2_fd, VIDIOC_DQBUF, &buf) < 0) {
    /* unref capture buffer since we are not going to push it. */
    gst_buffer_unref (viddec->capture.buffer);
    GST_ERROR_OBJECT (viddec, "Couldn't de-queue user buffer - %s",
        strerror (errno));
    return FALSE;
  }

  if (buf.bytesused == 0) {
    gst_buffer_unref (viddec->capture.buffer);
    if (viddec->eos == TRUE) {
      GST_DEBUG_OBJECT (viddec, "EOS MARKER received");
      /* Send all queued serialized events downstream */
      /* FIXME: This drain of events downstream on EOS is a workaround for Bug 33744 */
      if (FALSE == gst_stvideo_send_queued_events_before (viddec,
              GST_CLOCK_TIME_NONE)) {
        GST_WARNING_OBJECT (viddec, "Sending Event failed at EOS Marker");
      }
      /*Pause the capture task */
      gst_pad_pause_task (viddec->srcpad);
      gst_pad_push_event (viddec->srcpad, gst_event_new_eos ());
    }
    return TRUE;
  }
  /* FIXME: Buffer Size Updation based on bytesused commented until fix of Bug 36733 */
  /* GST_BUFFER_SIZE (viddec->capture.buffer) = buf.bytesused; */

  /* Set the buffer timestamp to the original pts grabbed from streaming engine */
  GST_BUFFER_TIMESTAMP (viddec->capture.buffer) =
      GST_TIMEVAL_TO_TIME (buf.timestamp);

  GST_LOG_OBJECT (viddec, "Capture time : %lld ms (%ld sec %ld usec)\n",
      GST_TIME_AS_MSECONDS (GST_BUFFER_TIMESTAMP (viddec->capture.buffer)),
      buf.timestamp.tv_sec, buf.timestamp.tv_usec);

  /* Check if we have any queued events that's to be sent before the current dequeued buffer timestamp,
     and send them if so. */
  ev_ts = GST_TIME_AS_MSECONDS (GST_BUFFER_TIMESTAMP (viddec->capture.buffer));
  if (FALSE == gst_stvideo_send_queued_events_before (viddec, ev_ts)) {
    GST_WARNING_OBJECT (viddec, "Sending Event failed at %lld ms", ev_ts);
  }

  /* Check for clipping */
  start = GST_BUFFER_TIMESTAMP (viddec->capture.buffer);
  duration =
      gst_util_uint64_scale (GST_SECOND, viddec->properties.fps_den,
      viddec->properties.fps_num);
  stop = GST_CLOCK_TIME_NONE;

  if (GST_CLOCK_TIME_IS_VALID (start) && GST_CLOCK_TIME_IS_VALID (duration)) {
    stop = start + duration;
  }

  if (stop == GST_CLOCK_TIME_NONE) {
    stop = start;
  }

  segment = &viddec->segment;
  if (gst_segment_clip (segment, GST_FORMAT_TIME, start, stop, &cstart, &cstop)) {

    GST_BUFFER_TIMESTAMP (viddec->capture.buffer) = cstart;

    if ((stop != GST_CLOCK_TIME_NONE) && (stop != start)) {
      GST_BUFFER_DURATION (viddec->capture.buffer) = cstop - cstart;
    }

    GST_LOG_OBJECT (viddec,
        "accepting buffer inside segment: %" GST_TIME_FORMAT " %"
        GST_TIME_FORMAT " seg %" GST_TIME_FORMAT " to %" GST_TIME_FORMAT
        " time %" GST_TIME_FORMAT,
        GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (viddec->capture.buffer)),
        GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (viddec->capture.buffer) +
            GST_BUFFER_DURATION (viddec->capture.buffer)),
        GST_TIME_ARGS (segment->start), GST_TIME_ARGS (segment->stop),
        GST_TIME_ARGS (segment->time));
  } else {
    GST_LOG_OBJECT (viddec,
        "dropping buffer outside segment: %" GST_TIME_FORMAT
        " %" GST_TIME_FORMAT
        " seg %" GST_TIME_FORMAT " to %" GST_TIME_FORMAT
        " time %" GST_TIME_FORMAT,
        GST_TIME_ARGS (start), GST_TIME_ARGS (stop),
        GST_TIME_ARGS (segment->start),
        GST_TIME_ARGS (segment->stop), GST_TIME_ARGS (segment->time));
    gst_buffer_unref (viddec->capture.buffer);
    goto done;
  }

  {
    GstClockTime timestamp;
    timestamp = GST_BUFFER_TIMESTAMP (viddec->capture.buffer);
    timestamp =
        gst_segment_to_running_time (segment, GST_FORMAT_TIME, timestamp);
    GST_BUFFER_TIMESTAMP (viddec->capture.buffer) = timestamp;
  }

  if (viddec->capture.mem_type == GST_STVIDEO_MMAP) {
    GstMapInfo info_write;
    /* Copy the Capture data from v4l2 buffer to Capture GstBuffer from downstream */
    /* FIXME: Use V4l2 Capture Buffer length instead of buf.bytesused until fix of Bug 36733 */
    gst_buffer_map (viddec->capture.buffer, &info_write, GST_MAP_WRITE);
    memcpy (info_write.data, viddec->capture.buf_start_address,
        viddec->capture.v4l2_capture_buf.length);
    gst_buffer_unmap (viddec->capture.buffer, &info_write);
  }

  /* Push the buffer to downstream element */
  viddec->srcresult = gst_pad_push (viddec->srcpad, viddec->capture.buffer);
  if (viddec->srcresult != GST_FLOW_OK) {
    GST_ERROR_OBJECT (viddec, "Failed to push buffer downstream -  %s",
        gst_flow_get_name (viddec->srcresult));
    return FALSE;
  }
  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);

done:
  return TRUE;
}

static gboolean
gst_stvideo_capture_alloc_buffer (Gststvideo * viddec, int fd)
{
  struct v4l2_requestbuffers reqbuf;

  GST_LOG_OBJECT (viddec, "%s:%d\n", __FUNCTION__, __LINE__);

  if (viddec->capture.mem_type == GST_STVIDEO_INVALID) {
    GST_ERROR_OBJECT (viddec, "Not valid memory type");
    return FALSE;
  }

  /* Capture Buffer formats : VIDIOC_REQBUFS */
  memset (&reqbuf, 0, sizeof (struct v4l2_requestbuffers));
  reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  reqbuf.count = 1;

  if (viddec->capture.mem_type == GST_STVIDEO_USER_PTR) {
    reqbuf.memory = V4L2_MEMORY_USERPTR;
  } else {
    reqbuf.memory = V4L2_MEMORY_MMAP;
  }
  if (ioctl (viddec->v4l2_fd, VIDIOC_REQBUFS, &reqbuf) < 0) {
    GST_ERROR_OBJECT (viddec, "VIDIOC_REQBUFS failed %s", strerror (errno));
    return FALSE;
  }

  /* mmap the allocated buffer in case of V4L2_MEMORY_MMAP */
  if (viddec->capture.mem_type == GST_STVIDEO_MMAP) {
    memset (&viddec->capture.v4l2_capture_buf, 0, sizeof (struct v4l2_buffer));
    viddec->capture.v4l2_capture_buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    viddec->capture.v4l2_capture_buf.index = 0;
    viddec->capture.v4l2_capture_buf.memory = V4L2_MEMORY_MMAP;
    if (ioctl (viddec->v4l2_fd, VIDIOC_QUERYBUF,
            &(viddec->capture.v4l2_capture_buf)) < 0) {
      GST_ERROR_OBJECT (viddec, "VIDIOC_QUERYBUF failed %s", strerror (errno));
      return FALSE;
    }
    viddec->capture.buf_start_address =
        (guchar *) mmap (NULL, viddec->capture.v4l2_capture_buf.length,
        PROT_READ | PROT_WRITE, MAP_SHARED, viddec->v4l2_fd,
        viddec->capture.v4l2_capture_buf.m.offset);

    if (viddec->capture.buf_start_address == ((void *) -1)) {
      GST_ERROR_OBJECT (viddec,
          "mmap for Capture Buffer failed v4l2 fd %d length %d offset %d",
          viddec->v4l2_fd, viddec->capture.v4l2_capture_buf.length,
          viddec->capture.v4l2_capture_buf.m.offset);
      return FALSE;
    }
    GST_DEBUG_OBJECT (viddec,
        "mmap for Capture Buffer passed v4l2 fd %d length %d offset %d",
        viddec->v4l2_fd, viddec->capture.v4l2_capture_buf.length,
        viddec->capture.v4l2_capture_buf.m.offset);
  }
  return TRUE;
}

static gboolean
gst_stvideo_capture_free_buffer (Gststvideo * viddec, int fd)
{
  struct v4l2_requestbuffers reqbuf;

  GST_LOG_OBJECT (viddec, "%s:%d\n", __FUNCTION__, __LINE__);

  if (viddec->capture.mem_type != GST_STVIDEO_INVALID) {

    /* unmap the Capture Buffer */
    if ((viddec->capture.mem_type == GST_STVIDEO_MMAP)
        && (viddec->capture.buf_start_address != NULL)) {
      GST_DEBUG_OBJECT (viddec, "unmap Capture Buffer");
      munmap (viddec->capture.buf_start_address,
          viddec->capture.v4l2_capture_buf.length);
    }

    /* Free the v4l2 buffer:  VIDIOC_REQBUFS */
    memset (&reqbuf, 0, sizeof (struct v4l2_requestbuffers));
    reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    reqbuf.count = 0;

    if (viddec->capture.mem_type == GST_STVIDEO_USER_PTR) {
      reqbuf.memory = V4L2_MEMORY_USERPTR;
    } else {
      reqbuf.memory = V4L2_MEMORY_MMAP;
    }
    if (ioctl (fd, VIDIOC_REQBUFS, &reqbuf) < 0) {
      GST_ERROR_OBJECT (viddec, "VIDIOC_REQBUFS failed %s", strerror (errno));
      return FALSE;
    }
  }
  viddec->capture.mem_type = GST_STVIDEO_INVALID;
  viddec->capture.buf_start_address = NULL;

  return TRUE;
}

static gboolean
gst_stvideo_capture_stream_on (Gststvideo * viddec, int fd)
{
  struct v4l2_buffer buf;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);

  memset (&buf, 0, sizeof (struct v4l2_buffer));
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl (fd, VIDIOC_STREAMON, &buf.type) < 0) {
    GST_ELEMENT_ERROR (viddec, RESOURCE, FAILED,
        ("Failed to start streaming on device %d", viddec->dev_id),
        ("VIDIOC_STREAMON failed : %s", strerror (errno)));
    return FALSE;
  }

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return TRUE;
}

static gboolean
gst_stvideo_capture_stream_off (Gststvideo * viddec, int fd)
{
  struct v4l2_buffer buf;

  GST_LOG_OBJECT (viddec, " > %s", __FUNCTION__);

  memset (&buf, 0, sizeof (struct v4l2_buffer));
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl (fd, VIDIOC_STREAMOFF, &buf.type) < 0) {
    GST_ELEMENT_ERROR (viddec, RESOURCE, FAILED,
        ("Failed to start streaming off device %d", viddec->dev_id),
        ("VIDIOC_STREAMOFF failed : %s", strerror (errno)));
    return FALSE;
  }

  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return TRUE;
}

/*
  * Function name : gst_stvideo_video_loop
  *  Input        :
  *   viddec - video decoder control information
  *
  *  Description  : This task is used in two usecases
  *   capture: it reads the buffer from streaming engine and push buffers downstream
  *   playback: it pushes dummy buffer to downstream ( basesink requires a buffer to preroll )
  *
  * Return        :  none
*/

static void
gst_stvideo_video_loop (Gststvideo * viddec)
{
  if (viddec->exit_loop) {
    GST_DEBUG_OBJECT (viddec, "exit_loop true, going to pause");
    goto error1;
  }

  /* push a dummy buffer to basesink */
  if (viddec->tunneled == TRUE) {
    g_mutex_lock (&viddec->dummy_buff.mutex);
    /* wait only if there is no request to push dummy buffer */
    if ((viddec->downstream_frame_pushed == TRUE)
        && (viddec->video_task_state == GST_STVIDEO_TASK_STARTED)) {
      GST_DEBUG_OBJECT (viddec, "waiting for signal to push dummy buffer");
      GST_STVIDEO_VIDEO_LOOP_WAIT (viddec);
    }
    g_mutex_unlock (&viddec->dummy_buff.mutex);
    gst_stvideo_push_dummy_packet (viddec);

    if (viddec->srcresult != GST_FLOW_OK)
      goto error1;

    return;
  }

  /* Push the empty buffer in the queue */
  if (!gst_stvideo_capture_queue_buffer (viddec)) {
    GST_ERROR_OBJECT (viddec, "Could not queue buffer");
    goto error;
  }

  if (!gst_stvideo_capture_dequeue_buffer (viddec)) {
    GST_ERROR_OBJECT (viddec, "Could not dequeue buffer");
    goto error;
  }

  return;
error:
  usleep (10 * 1000);
  GST_LOG_OBJECT (viddec, " < %s", __FUNCTION__);
  return;

error1:
  gst_pad_pause_task (viddec->srcpad);
  return;
}

static gboolean
gst_stvideo_manage_caps (Gststvideo * viddec)
{
  GstCaps *srccaps;
  GstCaps *allowed_caps;
  GstStructure *srcstructure;
  gboolean ret = FALSE;

  /* Do allowed caps to get the caps of the downstream element for negotiation  */
  allowed_caps = gst_pad_get_allowed_caps (viddec->srcpad);
  if (allowed_caps && !gst_caps_is_empty (allowed_caps)) {
    srccaps = gst_caps_copy_nth (allowed_caps, 0);
    gst_caps_unref (allowed_caps);

    srcstructure = gst_caps_get_structure (srccaps, 0);
    if (gst_structure_has_name (srcstructure, "video/x-fake-yuv")) {
      gst_caps_unref (srccaps);
      srccaps = gst_caps_new_simple ("video/x-fake-yuv",
          "width", G_TYPE_INT, viddec->properties.width,
          "height", G_TYPE_INT, viddec->properties.height,
          "framerate", GST_TYPE_FRACTION, viddec->properties.fps_num,
          viddec->properties.fps_den,
          "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1, NULL);
      GST_DEBUG_OBJECT (viddec, "Format Fake YUV\n");
    } else {
      gst_structure_set (srcstructure, "width", G_TYPE_INT,
          viddec->capture.width, "height", G_TYPE_INT,
          viddec->capture.height, "framerate", GST_TYPE_FRACTION,
          viddec->properties.fps_num, viddec->properties.fps_den,
          "pixel-aspect-ratio", GST_TYPE_FRACTION,
          viddec->properties.par_num, viddec->properties.par_den, NULL);

      if (gst_structure_has_name (srcstructure, "video/x-raw")) {
        if (!strcmp (gst_structure_get_string (srcstructure, "format"), "UYVY")) {
          viddec->capture.pixel_format = V4L2_PIX_FMT_UYVY;
          viddec->capture.bpp = 16;
        } else {
          if (gst_structure_get_int (srcstructure, "bpp", &viddec->capture.bpp)) {
            if (viddec->capture.bpp == 32) {
              GST_INFO_OBJECT (viddec, "Format BGR32 Bpp %d\n",
                  viddec->capture.bpp);
              viddec->capture.pixel_format = V4L2_PIX_FMT_BGR32;
            } else {
              GST_INFO_OBJECT (viddec, "Format BGR24 Bpp %d\n",
                  viddec->capture.bpp);
              viddec->capture.pixel_format = V4L2_PIX_FMT_BGR24;
              viddec->capture.bpp = 24;
            }
          } else {
            /* stvideo supports RGB, BGRx, BGRA for x-rgb */
            const char *format =
                gst_structure_get_string (srcstructure, "format");
            if (format != NULL && !strcmp (format, "RGB")) {
              GST_INFO_OBJECT (viddec, "Format BGR24 Bpp %d\n",
                  viddec->capture.bpp);
              viddec->capture.pixel_format = V4L2_PIX_FMT_BGR24;
              viddec->capture.bpp = 24;
            } else {
              GST_INFO_OBJECT (viddec, "Format BGR32 Bpp %d\n",
                  viddec->capture.bpp);
              viddec->capture.pixel_format = V4L2_PIX_FMT_BGR32;
              viddec->capture.bpp = 32;
            }
          }
        }
      }
    }
  } else {
    if (allowed_caps) {
      gst_caps_unref (allowed_caps);
    }
    GST_INFO_OBJECT (viddec, "Format Fake YUV\n");

    srccaps = gst_caps_new_simple ("video/x-fake-yuv",
        "width", G_TYPE_INT, viddec->properties.width,
        "height", G_TYPE_INT, viddec->properties.height,
        "framerate", GST_TYPE_FRACTION, viddec->properties.fps_num,
        viddec->properties.fps_den,
        "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1, NULL);
  }
  /* Fixate caps on src pad */
  gst_pad_use_fixed_caps (viddec->srcpad);
  ret = gst_pad_set_caps (viddec->srcpad, srccaps);
  gst_caps_unref (srccaps);

  return ret;
}

static gboolean
gst_stvideo_manage_dev_id (Gststvideo * viddec)
{
  gchar *video_entity;
  GstCaps *srccaps;
  GstQuery *query;
  GstStructure *structure;

  GST_LOG_OBJECT (viddec, "%s:%d\n", __FUNCTION__, __LINE__);

  structure = gst_structure_new ("video-entity",
      "video-entity", G_TYPE_STRING, NULL, NULL);
  query = gst_query_new_custom (GST_QUERY_CUSTOM, structure);
  if (query) {
    if (gst_pad_peer_query (viddec->srcpad, query) == TRUE) {
      structure = (GstStructure *) gst_query_get_structure (query);
      video_entity =
          g_strdup_printf ("%s", gst_structure_get_string (structure,
              "video-entity"));
    } else {
      video_entity = g_strdup_printf ("%s", "no-video-plane");
    }
    gst_query_unref (query);
  } else {
    video_entity = g_strdup_printf ("%s", "no-video-plane");
  }

  GST_DEBUG_OBJECT (viddec,
      "video entity requested by the downstream element %s ", video_entity);

  gchar *input_link, *output_link;
  gchar *device_name;
  gboolean success = FALSE;
  guint i;

  GST_DEBUG_OBJECT (viddec,
      "staudio instance dev-id %d Property dev-id %d ", viddec->dev_id,
      viddec->dev_id_prop);

  if (viddec->dev_id_prop == DEFAULT_DEV_ID) {
    if (!strcmp (video_entity, "no-video-plane")) {

      for (i = 0; i <= MAX_DEV_ID; i++) {
        device_name = g_strdup_printf ("dvb0.video%d", i);

        /* Use media-controller library to get appropriate video device */
        output_link = (gchar *) gst_stm_mctl_get_entity_sink (device_name);
        g_free (device_name);
        if (output_link == NULL) {
          GST_ERROR_OBJECT (viddec,
              "Failed to get appropriate video entity from media-ctl");
          g_free (video_entity);
          return FALSE;
        }

        /* STM Grab Overlay is the entity returned by the media controller
           if the video decoder is not linked  to MAIN/PIP/AUX Vids  */
        if (!strcmp (output_link, "STM Grab Overlay")) {
          viddec->dev_id = i;
          if (gst_stvideo_open_device (viddec)) {
            success = TRUE;
            g_free (output_link);
            break;
          }
        }
        g_free (output_link);
      }

      if (!success) {
        GST_ELEMENT_ERROR (viddec, RESOURCE, OPEN_READ,
            ("Failed to find video device for capturing, all devices are BUSY"),
            GST_ERROR_SYSTEM);
        g_free (video_entity);
        return FALSE;
      }
      srccaps = gst_pad_get_current_caps (viddec->srcpad);
      /* If the downstream element is fakesink, configure in tunneled for capturing using stvidcapture element */
      viddec->tunneled =
          gst_structure_has_name (gst_caps_get_structure (srccaps, 0),
          "video/x-fake-yuv");
      gst_caps_unref (srccaps);
    } else {
      /* Use media-controller library to get appropriate video-device */
      input_link = (gchar *) gst_stm_mctl_get_entity_source (video_entity);
      if (input_link == NULL) {
        GST_ERROR_OBJECT (viddec,
            "Failed to get appropriate video entity from media-ctl");
        g_free (video_entity);
        return FALSE;
      }

      /* check if we have valid input_link */
      if (strncmp (input_link, "dvb0.video", strlen ("dvb0.video"))) {
        /* input_link is not valid */
        g_free (input_link);
        GST_ERROR_OBJECT (viddec,
            "Failed to get appropriate input link from media-ctl");
        g_free (video_entity);
        return FALSE;
      }

      viddec->dev_id = atoi (input_link + strlen ("dvb0.video"));
      g_free (input_link);
      if (gst_stvideo_open_device (viddec) == FALSE) {
        GST_ELEMENT_ERROR (viddec, RESOURCE, OPEN_READ,
            ("Failed to open stvideo device %d", viddec->dev_id),
            GST_ERROR_SYSTEM);
        g_free (video_entity);
        return FALSE;
      }
      viddec->tunneled = TRUE;
    }
    g_free (video_entity);
  } else {
    g_free (video_entity);
    GST_DEBUG_OBJECT (viddec, "Using dev-id set by App %d",
        viddec->dev_id_prop);
    viddec->dev_id = viddec->dev_id_prop;
    device_name = g_strdup_printf ("dvb0.video%d", viddec->dev_id);

    /* Use media-controller to query for tunnelled or non-tunnelled mode */
    output_link = (gchar *) gst_stm_mctl_get_entity_sink (device_name);
    g_free (device_name);
    if (output_link == NULL) {
      GST_ERROR_OBJECT (viddec,
          "Failed to get appropriate video entity from media-ctl");
      return FALSE;
    }
    if (!strcmp (output_link, "STM Grab Overlay")) {
      srccaps = gst_pad_get_current_caps (viddec->srcpad);
      /* If the downstream element is fakesink, configure in tunneled for capturing using stvidcapture element */
      viddec->tunneled =
          gst_structure_has_name (gst_caps_get_structure (srccaps, 0),
          "video/x-fake-yuv");
      gst_caps_unref (srccaps);
    } else {
      viddec->tunneled = TRUE;
    }
    g_free (output_link);
    if (gst_stvideo_open_device (viddec) == FALSE) {
      GST_ELEMENT_ERROR (viddec, RESOURCE, OPEN_READ,
          ("Failed to open stvideo device %d", viddec->dev_id),
          GST_ERROR_SYSTEM);
      return FALSE;
    }
  }

  GST_DEBUG_OBJECT (viddec, "Device id  = %d , tunneled = %s", viddec->dev_id,
      viddec->tunneled ? "TRUE" : "FALSE");

  gst_stvideo_setup_device (viddec);
  if (viddec->tunneled == FALSE) {
    if ((viddec->properties.width == 0) || (viddec->properties.height == 0)) {
      viddec->capture.task_start_wait_for_size = TRUE;
    }
    if ((viddec->properties.fps_num == 0)
        || (viddec->properties.fps_den == 0)) {
      viddec->capture.task_start_wait_for_framerate = TRUE;
    }
    if ((!viddec->capture.task_start_wait_for_size)
        && (!viddec->capture.task_start_wait_for_framerate)) {
      if (gst_stvideo_capture_setup_device (viddec)) {
        if (gst_pad_start_task (viddec->srcpad,
                (GstTaskFunction) gst_stvideo_video_loop, viddec,
                NULL) != TRUE) {
          GST_ERROR_OBJECT (viddec,
              "cannot start stvideo loop task for capture");
          return FALSE;
        }
      } else {
        GST_ERROR_OBJECT (viddec, "Failed to setup stvideo capture device %d",
            viddec->dev_id);
      }
    } else {
      viddec->capture.task_start_delayed = TRUE;
    }
  } else {
    if (gst_pad_start_task (viddec->srcpad,
            (GstTaskFunction) gst_stvideo_video_loop, viddec, NULL) != TRUE) {
      GST_ERROR_OBJECT (viddec, "cannot start stvideo loop task for playback");
      return FALSE;
    }
    g_mutex_lock (&viddec->dummy_buff.mutex);
    viddec->video_task_state = GST_STVIDEO_TASK_STARTED;
    g_mutex_unlock (&viddec->dummy_buff.mutex);
  }

  return TRUE;
}

/* entry point to initialize the element
 * register the element factories and other features
 */
gboolean
stvideo_init (GstPlugin * plugin)
{
  /* debug category for viddec log messages */
  GST_DEBUG_CATEGORY_INIT (gst_stvideo_debug, "stvideo", 0, "ST video decoder");
  return gst_element_register (plugin, "stvideo", GST_RANK_PRIMARY + 10,
      GST_TYPE_STVIDEO);
}
