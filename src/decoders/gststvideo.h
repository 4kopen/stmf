/* Gstreamer ST Video-Decoder Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STVIDEO_H__
#define __GST_STVIDEO_H__

#include <gst/gst.h>
#include <linux/dvb/video.h>
#include <linux/videodev2.h>

G_BEGIN_DECLS
/* #defines don't like whitespacey bits */
#define GST_TYPE_STVIDEO (gst_stvideo_get_type())
#define GST_STVIDEO(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_STVIDEO,Gststvideo))
#define GST_STVIDEO_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_STVIDEO,GststvideoClass))
#define GST_IS_STVIDEO(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_STVIDEO))
#define GST_IS_STVIDEO_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_STVIDEO))
#define GST_STVIDEO_VIDEO_LOOP_GET_COND(viddec)  (&((Gststvideo *)viddec)->dummy_buff.cond)
#define GST_STVIDEO_VIDEO_LOOP_WAIT(viddec)      (g_cond_wait (GST_STVIDEO_VIDEO_LOOP_GET_COND (viddec), &viddec->dummy_buff.mutex))
#define GST_STVIDEO_VIDEO_LOOP_SIGNAL(viddec) (g_cond_signal (GST_STVIDEO_VIDEO_LOOP_GET_COND (viddec)))
typedef struct _Gststvideo Gststvideo;
typedef struct _GststvideoClass GststvideoClass;

typedef enum
{
  GST_STVIDEO_TASK_IDLE = 0,
  GST_STVIDEO_TASK_STARTED,
  GST_STVIDEO_TASK_PAUSED,
  GST_STVIDEO_TASK_STOPPED
} GstStVideoTaskState;

typedef enum
{
  GST_STVIDEO_INVALID,
  GST_STVIDEO_MMAP,
  GST_STVIDEO_USER_PTR
} GstStVideoCaptureMemType;

typedef enum
{
  GST_STVIDEO_DEFAULT = 0,      /* default policy based on skip flag */
  GST_STVIDEO_KF_SE,            /* SE event driven Key Frames policy */
  GST_STVIDEO_ALL_SE            /* SE event driven based All frames policy */
} GstStVideoTrickPolicyType;

typedef struct
{
  GCond cond;
  GMutex mutex;
} GstStVidCond;

struct _Gststvideo
{
  GstElement element;

  GstPad *sinkpad;
  GstPad *srcpad;

  GstSegment segment;
  gboolean new_segment_received;        /* Used to keep track whenever NEWSEGMENT event is received */
  gboolean first_new_segment;   /* first new segment event */
  gboolean first_buffer_received;       /* first video buffer of stream received */
  gboolean flushing_seek;       /* a sticky flag to check that stvideo received a flushing seek */
  gboolean direction_change;    /* if TRUE playback direction has changed */
  gboolean update_segment;      /* segment to be updated */
  guint dev_id;
  guint dev_id_prop;            /* The dev-id set as a property */

  gint fd;                      /* Video codec file descriptor      */
  gint v4l2_fd;
  video_play_state_t state;     /* Video codec current state        */
  gboolean downstream_frame_pushed;
  gboolean first_injection;     /* First injection into video codec */
  gboolean is_pes;              /* Data directly from demux         */
  gboolean live;
  gboolean rate_pending;
  gboolean disable_avsync;
  gboolean disable_dev;         /* Used to manage device state machine internally */
  gboolean property_disable_dev;        /* Used to store what user has requested */
  gboolean display_first_frame_early;   /* display first video frame early (even when in paused state) */
  gboolean ptswrap;             /* TS demux sends ptswrap event in case of ptswrap */
  gint64 current_time;
  guint64 prev_video_pts;
  guint clock_data_flags;
  guint64 currentPTS;           /* Current PTS value */
  gint64 prevPTS;               /* represents gst time of prev frame */
  gint contentframerate;        /* Current framerate value */
  guint64 prev_start;           /* Previous start value of play interval */

  guint latency;
  guint mem_profile;            /* used to set memory profile property */
  gboolean tunneled;
  GstFlowReturn srcresult;
  gboolean discontinuity;
  GstCaps *prev_caps;
  gboolean start_of_stream;
  gboolean eos;
  gboolean eos_set;             /* TRUE if eos discontinuity has been set on SE */
  gboolean device_config;
  GList *queued_events_list;    /* List of Queued Serialized Events */
  GstClockTime last_buffer_ts;  /* Timestamp (in ms) of last buffer received in chain function */
  GMutex write_lock;
  gboolean exit_loop;
  gboolean interlace;
  gboolean skipflag;            /* if set, key frame else all frames trickmode policy */
  GMutex event_lock;            /* lock for event queue/dequeue */
  gint decimation;              /* used to set video decoder decimation */
  gboolean channel_change;      /* channel change or not */
  gint prev_codec;              /* Main codec type */
  /* Codec */
  struct
  {
    gint type;                  /* Main codec type */
    gint subtype;               /* Sub codec type  */
    guint8 start_code;          /* Start code for codec type */
  } codec;

  /* Video properties */
  struct
  {
    gint width;                 /* Video width                          */
    gint height;                /* Video height                         */
    gint fps_num;               /* Video frame per second numerator     */
    gint fps_den;               /* Video frame per second denominator   */
    gint par_num;               /* Video pixel aspect ratio numerator   */
    gint par_den;               /* Video pixel aspect ratio denominator */
  } properties;

  /* Video codec data */
  struct
  {
    GstBuffer *buf;
    struct
    {
      guint nal_header_size;
    } h264;
    struct
    {
      guint tick_elapsed;
    } MP4P2;
  } codec_data;

  /* Trickmode control */
  struct
  {
    GstClockTime time_ref;      /* Time when trickmode injection started                */
    GstClockTime time_base;     /* Time base for trickmode injection                    */
    GstClockTime time_skip;     /* Time to skip during trickmode injection              */
    guint64 prev_frame_count;   /* Number of frame display before trickmode injection   */
    gint64 frame_count;         /* Number of frame display before trickmode injection   */
    gint64 frame_repeat;        /* Max number of repeated frames before next injection  */
    guint64 frame_dec_late;     /* Video frame decoded late from video event            */
    gboolean enable;            /* enable trickmode playback position control from application */
    gdouble rate;               /* Trickmode current rate                               */
    gdouble set_rate;
    gboolean rate_change;       /* received a seek event for rate change */
    gboolean bos;               /* beginning of stream, used during reverse trickmode */
    gboolean upstream_rap_index_support;

    gboolean reverse_support;
    gboolean reverse_injection;
    guint64 reverse_cur_num;
    gchar *reverse_tmp_buffer;
    gchar *reverse_cmp_buffer;
  } tm_control;

  /* Capture */
  struct
  {
    gint bpp;
    guint pixel_format;
    gint width;                 /* 16 bit aligned width for capture mode */
    gint height;

    GstBuffer *buffer;
    GstBufferPool *pool;
    gboolean task_start_delayed;        /* Start of capture task delayed due to wrong input params */
    gboolean task_start_wait_for_size;  /* Start of capture task delayed due to wrong width & height params */
    gboolean task_start_wait_for_framerate;     /* Start of capture task delayed due to wrong framerate */
    GstStVideoCaptureMemType mem_type;  /* Used to store the type of Capture Buffer: V4L2_MEMORY_MMAP or V4L2_MEMORY_USERPTR   */
    gboolean is_stream_on_done; /* Used to check whether stream on is done  */
    struct v4l2_buffer v4l2_capture_buf;        /* Used to store Capture v4l2 buffer when REQBUFS is done for V4L2_MEMORY_MMAP */
    void *buf_start_address;    /* Used to store mapped capture buffer start address when REQBUFS is done for V4L2_MEMORY_MMAP */
  } capture;

  GstTask *video_monitor_task;  /* Video Monitor task */
  GRecMutex vm_task_lock;

  GstTask *video_task;          /* video loop task */
  GstStVideoTaskState video_task_state; /* video loop task state */
  /* condition used in video loop in tunneled playback */
  GstStVidCond dummy_buff;
  gboolean catchlive;           /* if catch live is in progress or not */
};

struct _GststvideoClass
{
  GstElementClass parent_class;
};

GType gst_stvideo_get_type (void);

gboolean stvideo_init (GstPlugin * plugin);

G_END_DECLS
#endif /* __GST_STVIDEO_H__ */
