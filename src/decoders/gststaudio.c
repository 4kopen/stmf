/* Gstreamer ST Audio-Decoder Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:element-staudio
 *
 * Implementation of the gstreamer staudio decoder element
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! staudio ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#include <gst/gst.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/poll.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <linux/dvb/stm_ioctls.h>
#include "gststaudio.h"
#include "pes.h"
#include <linux/videodev2.h>
#include "linux/dvb/dvb_v4l2_export.h"
#include "linux/stm/stmedia_export.h"
#include "v4l2_utils.h"
#include "gststencode.h"
#include <math.h>
#include <sys/mman.h>
#include <gst/audio/audio.h>
#include "gststbufferpool.h"
#include "gst_stm_mctl_wrap.h"
#include <stdlib.h>


GST_DEBUG_CATEGORY_STATIC (gst_staudio_debug);
#define GST_CAT_DEFAULT gst_staudio_debug

#define GST_STAUDIO_AAC_HEADER_LENGTH           7
#define GST_STAUDIO_AAC_SAMPLE_RATE_INDEX       13
#define GST_STAUDIO_AAC_CHANNELS_INDEX          8

#define GST_STAUDIO_MP3_INJECTION_SIZE          (32 * 1024)

/* the capabilities of the inputs and outputs.
 * describe the real formats here.
 */

static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/fake-pes;"
        "audio/mpeg,"
        "  mpegversion = (int) {1},"
        "  layer = (int) {1, 2, 3};"
        "audio/mpeg,"
        "  mpegversion = (int) {2,4};"
        "audio/mpeg-pes,"
        "  mpegversion = (int) {1},"
        "  layer = (int) {1, 2, 3};"
        "audio/mpeg-pes,"
        "  mpegversion = (int) {2,4};"
        "audio/ac3;"
        "audio/x-ac3;"
        "audio/x-ac3-pes;"
        "audio/x-private1-ac3;"
        "audio/x-eac3;"
        "audio/x-sac3;"
        "audio/x-dd;"
        "audio/x-dts;"
        "audio/x-dts-pes;"
        "audio/x-private1-dts;"
        "audio/x-flac;"
        "audio/x-mlp;"
        "audio/x-mlp-pes;"
        "audio/x-raw,"
        "rate = (int) {8000, 11025, 12000,16000, 22050, 24000, 44100,48000, 64000, 88200, 96000,176400, 192000},"
        "channels = (int) [ 1, 8 ];"
        "audio/x-private1-lpcm;"
        "audio/x-private1-lpcm-pes;"
        "audio/x-private-ts-lpcm;"
        "audio/x-private-ts-lpcm-pes;"
        "audio/x-pn-realaudio;"
        "audio/x-vorbis;"
        "audio/x-wma,"
        "  wmaversion = (int) {2,3,4};"
        "audio/x-dra;" "audio/x-dra-pes;" "audio/x-adpcm;")
    );

/* REVIEW/CRITICAL: non-fake caps are wrong. Use GST_AUDIO_CAPS_MAKE (S32LE) */
static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-fake-int-stm,"
        "dev-id = (int) [0, MAX];" GST_AUDIO_CAPS_MAKE ("S32LE"))
    );

#define gst_staudio_parent_class parent_class
G_DEFINE_TYPE (Gststaudio, gst_staudio, GST_TYPE_ELEMENT);

#define DEFAULT_DISABLE_AVSYNC  FALSE
#define DEFAULT_DISABLE_INJ     FALSE
#define MIN_DEV_ID              0
#define MAX_DEV_ID              MAX_NUM_DEV -1
#define DEFAULT_DEV_ID          MAX_DEV_ID

#define MIN_LATENCY         0
#define MAX_LATENCY         2550
#define DEFAULT_LATENCY     0

#define DEFAULT_LIVE_LATENCY                    150     /* ms */

#define DEFAULT_MUTE FALSE


/* Audio service type are defined in stm_audio.h in adaptation layer */
/* PRIMARY = 0, SECONDARY = 1, MAIN = 2, AUDIO_DESCRIPTION = 3,
    MAIN_AND_AUDIO_DESCRIPTION = 4, CLEAN_AUDIO = 5
  */
#define DEFAULT_SERVICE_TYPE AUDIO_SERVICE_PRIMARY
#define MIN_VALUE_SERVICE_TYPE DEFAULT_SERVICE_TYPE
#define MAX_VALUE_SERVICE_TYPE AUDIO_SERVICE_CLEAN_AUDIO

/* Audio application type are defined in stm_audio.h in adaptation layer */
/* ISO = 0, DVD = 1, DVB = 2, MS10 = 3, MS11 = 4, MS12 = 5 */
#define DEFAULT_APPLICATION_TYPE AUDIO_APPLICATION_ISO
#define MIN_VALUE_APPLICATION_TYPE DEFAULT_APPLICATION_TYPE
#define MAX_VALUE_APPLICATION_TYPE AUDIO_APPLICATION_MS12

/* audio device supports downmix at decoder stream level */
/* number of channels to be supported by audio are 1,2,3,6,8 */
#define DEFAULT_CHANNEL_NUM 0
#define MIN_VALUE_CHANNEL_NUM DEFAULT_CHANNEL_NUM
#define MAX_VALUE_CHANNEL_NUM 20

#define CLOCK_BASE 9LL
#define MPEGTIME_TO_GSTTIME(time) (gst_util_uint64_scale ((time), \
            GST_MSECOND/10, CLOCK_BASE))

#define GSTTIME_TO_MPEGTIME(time) (gst_util_uint64_scale ((time), \
            CLOCK_BASE, GST_MSECOND/10))

#define DEFAULT_CHANNEL_MASK 0xff
#define DEFAULT_BITS_PER_SAMPLE 32
#define DEFAULT_SAMPLE_RATE 48000
#define DEFAULT_CHANNELS 8
#define MAX_SAMPLES_NUMBER 2048
#define DEFAULT_SIZE (MAX_SAMPLES_NUMBER*DEFAULT_CHANNELS*DEFAULT_BITS_PER_SAMPLE/8)

enum
{
  PROP_0,
  PROP_DISABLE_INJECT,
  PROP_DEV_ID,
  PROP_DISABLE_AVSYNC,
  PROP_CHANNEL_CONFIG,
  PROP_LATENCY,
  PROP_MUTE,
  PROP_APPLICATION_TYPE,
  PROP_SERVICE_TYPE,
  PROP_CHANNEL_NUM,
  PROP_LAST
};

/* A structure for storing events in List-queued_events_list */
typedef struct
{
  GstClockTime ts;              /* Time (in ms) at which Event is to be sent downstream */
  GstEvent *event;              /* Serialized Event to be sent downstream */
} queued_event;

typedef struct
{
  gshort format_tag;
  gushort channels;
  gulong samples_per_sec;
  gulong avg_bytes_per_sec;
  gushort block_align;
  gushort bits_per_sample;
/* Note: there may be additional fields here, depending upon wFormatTag. */
} wav_format_chunk;

/* Define the structure of the private data for the adpcm PES packets.
 * This is the structure that is expected by the Streaming Engine.*/
struct adpcmprivatedata_s
{
  unsigned char streamid;
  unsigned int frequency;
  unsigned char nb_of_coefficients;
  unsigned char nb_of_channels;
  unsigned short nb_of_samples_per_block;
  unsigned char last_packet;
} __attribute__ ((packed));

typedef struct adpcmprivatedata_s adpcmprivatedata_t;

/* vorbis workaround for seek. See bugzilla 21834 */
struct vorbisprivatedata_s
{
  gboolean saved;
  guint8 *page1;
  gint len1;
  guint8 *page2;
  gint len2;
  guint8 *page3;
  gint len3;
};

typedef struct vorbisprivatedata_s vorbisprivatedata_t;

static gboolean gst_staudio_send_queued_events_before (Gststaudio * auddec,
    GstClockTime ts);
static void gst_staudio_queue_event (Gststaudio * auddec, GstEvent * event,
    GstClockTime ts);
static queued_event *gst_staudio_make_queued_event (Gststaudio * auddec,
    GstEvent * event, GstClockTime ts);
static void gst_staudio_drop_queued_events (Gststaudio * auddec);
static void gst_staudio_free_queued_event (queued_event * qe);

static void gst_staudio_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_staudio_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static gboolean gst_staudio_set_caps (Gststaudio * auddec, GstCaps * caps);
static GstFlowReturn gst_staudio_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buf);
static gboolean gst_staudio_sink_pad_query (GstPad * pad, GstObject * parent,
    GstQuery * query);
static gboolean gst_staudio_src_pad_query (GstPad * pad, GstObject * parent,
    GstQuery * query);
static void gst_staudio_reset (Gststaudio * auddec);
static gboolean gst_staudio_open_device (Gststaudio * auddec);
static gboolean gst_staudio_close_device (Gststaudio * auddec, gboolean stop);
static gboolean gst_staudio_term_device (Gststaudio * auddec);
static void gst_staudio_setup_device (Gststaudio * auddec);
static gboolean gst_staudio_ioctl (Gststaudio * auddec, guint ioctl_code,
    void *parameter);
static gboolean gst_staudio_write (Gststaudio * auddec, guint8 * data,
    guint data_length);
static gchar *gst_staudio_ioctl_name (gint ioctl_code);

static gboolean gst_staudio_queue_packet (Gststaudio * auddec, GstBuffer * buf);
static gboolean gst_staudio_write_packet (Gststaudio * auddec, GstBuffer * buf);
static gboolean gst_staudio_write_aac_packet (Gststaudio * auddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts);
static gboolean gst_staudio_write_ac3_packet (Gststaudio * auddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts);
static gboolean gst_staudio_write_mp3_packet (Gststaudio * auddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts);
static gboolean gst_staudio_write_pcm_packet (Gststaudio * auddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts);
static gboolean gst_staudio_write_wma_packet (Gststaudio * auddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts);
static gboolean gst_staudio_write_adpcm_packet (Gststaudio * auddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts);
static gboolean gst_staudio_write_vorbis_packet (Gststaudio * auddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts);
static gboolean gst_staudio_write_audio_packet (Gststaudio * auddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts);
static void gst_staudio_update_time (Gststaudio * auddec);
static gboolean gst_staudio_push_dummy_packet (Gststaudio * auddec);
static guint64 gst_staudio_get_pts (Gststaudio * auddec);
static void gst_staudio_save_vorbis_packet (Gststaudio * auddec,
    GstBuffer * buf);
static void gst_staudio_free_vorbis_packet (Gststaudio * auddec);
static void gst_staudio_start_monitor_task (Gststaudio * auddec);
static void gst_staudio_stop_monitor_task (Gststaudio * auddec);
static void gst_staudio_monitor_loop (Gststaudio * auddec);

static void gst_staudio_audio_loop (Gststaudio * auddec);
static gboolean gst_staudio_capture_setup_device (Gststaudio * auddec);
static gboolean gst_staudio_capture_queue_buffer (Gststaudio * auddec);
static gboolean gst_staudio_capture_dequeue_buffer (Gststaudio * auddec);
static gboolean gst_staudio_capture_alloc_buffer (Gststaudio * auddec, int fd);
static gboolean gst_staudio_capture_free_buffer (Gststaudio * auddec, int fd);
static gboolean gst_staudio_capture_stream_on (Gststaudio * auddec, int fd);
static gboolean gst_staudio_capture_stream_off (Gststaudio * auddec, int fd);
static void gst_staudio_check_eos_event (Gststaudio * auddec);
static void gst_staudio_set_live_play_policies (Gststaudio * auddec);
static gboolean gst_staudio_manage_dev_id (Gststaudio * auddec);
static gboolean gst_staudio_set_channel_positions (Gststaudio * auddec,
    struct v4l2_buffer *buf);
static gboolean gst_staudio_manage_caps (Gststaudio * auddec);

/* Send all queued events received before timestamp ts downstream */
static gboolean
gst_staudio_send_queued_events_before (Gststaudio * auddec, GstClockTime ts)
{
  GstPad *pad = auddec->srcpad;
  gboolean ret = TRUE;

  g_mutex_lock (&auddec->event_lock);

  if (ts == GST_CLOCK_TIME_NONE) {
    GST_DEBUG_OBJECT (auddec, "Drain Queued Events downstream");
  }
  while (auddec->queued_events_list) {
    queued_event *ev = auddec->queued_events_list->data;
    gboolean ret_loop = TRUE;

    if ((ev->ts != GST_CLOCK_TIME_NONE) && (ts != GST_CLOCK_TIME_NONE)
        && (ev->ts > ts)) {
      break;
    }

    GST_DEBUG_OBJECT (auddec,
        "Sending event %s with Queue Event Time %lld ms at Dequeue Time %lld ms",
        GST_EVENT_TYPE_NAME (ev->event), ev->ts, ts);

    /* For trickmode segment needs to be updated before pushing down */
    if (GST_EVENT_TYPE (ev->event) == GST_EVENT_SEGMENT
        && auddec->tunneled == FALSE) {
      const GstSegment *in_segment;
      GstSegment out_segment;

      memset (&out_segment, 0, sizeof (out_segment));
      gst_event_parse_segment (ev->event, &in_segment);

      gst_segment_copy_into (in_segment, &auddec->segment);

      out_segment.rate = 1.0;
      out_segment.applied_rate = in_segment->rate;
      out_segment.start = 0;
      if (in_segment->stop == GST_CLOCK_TIME_NONE) {
        out_segment.stop = in_segment->stop;
      } else {
        out_segment.stop =
            (in_segment->stop - in_segment->start) / ABS (in_segment->rate);
      }
      out_segment.time = in_segment->position;
      out_segment.position = in_segment->position;
      out_segment.duration = out_segment.stop - out_segment.start;
      out_segment.format = in_segment->format;

      gst_event_unref (ev->event);
      ev->event = gst_event_new_segment ((const GstSegment *) &out_segment);
    }

    /* Push Serialized event downstream */
    ret_loop = gst_pad_push_event (pad, gst_event_ref (ev->event));
    gst_staudio_free_queued_event (auddec->queued_events_list->data);
    auddec->queued_events_list =
        g_list_delete_link (auddec->queued_events_list,
        auddec->queued_events_list);
    if (!ret_loop) {
      GST_WARNING_OBJECT (auddec, "Pushing event downstream failed");
      ret = ret_loop;
    }
  }

  g_mutex_unlock (&auddec->event_lock);

  return ret;
}

/* Queue an event in the List-queued_events_list */
static void
gst_staudio_queue_event (Gststaudio * auddec, GstEvent * event, GstClockTime ts)
{
  g_mutex_lock (&auddec->event_lock);
  GST_DEBUG_OBJECT (auddec, "Queueing event %s with TimeStamp %lld ms",
      GST_EVENT_TYPE_NAME (event), ts);
  auddec->queued_events_list =
      g_list_append (auddec->queued_events_list,
      gst_staudio_make_queued_event (auddec, event, ts));
  g_mutex_unlock (&auddec->event_lock);
}

/* Make an object of queued_event which can be inserted in the List-queued_events_list */
static queued_event *
gst_staudio_make_queued_event (Gststaudio * auddec, GstEvent * event,
    GstClockTime ts)
{
  queued_event *qe = g_slice_new (queued_event);
  GST_LOG_OBJECT (auddec, "Event %s Timestamp %lld ms",
      GST_EVENT_TYPE_NAME (event), ts);
  qe->ts = ts;
  qe->event = gst_event_ref (event);
  return qe;
}

/* Flush all queued events and free the list */
static void
gst_staudio_drop_queued_events (Gststaudio * auddec)
{
  g_mutex_lock (&auddec->event_lock);
  GST_DEBUG_OBJECT (auddec, "Drop all Queued Serialized Events");
  g_list_free_full (auddec->queued_events_list,
      (GDestroyNotify) gst_staudio_free_queued_event);
  auddec->queued_events_list = NULL;
  g_mutex_unlock (&auddec->event_lock);
}

/* Free queued event */
static void
gst_staudio_free_queued_event (queued_event * qe)
{
  gst_event_unref (qe->event);
  g_slice_free (queued_event, qe);
}

/*
 * gst_staudio_get_active_element:
 *
 * this function returns current_active auddec
 *
 * current_auddec : staudio instance which is currently active
 *
 * return value: (Gststaudio *) active[dev_id].audio
 *
 */
static Gststaudio *
gst_staudio_get_active_element (Gststaudio * current_auddec)
{
  GststaudioClass *klass;
  klass = GST_STAUDIO_CLASS (G_OBJECT_GET_CLASS (current_auddec));

  return klass->active[current_auddec->dev_id].audio;
}

/*
 * gst_staudio_set_active_element:
 *
 * this function sets active[dev_id].audio to new_active auddec
 *
 * previous_auddec : staudio instance which was previously active
 *
 * new_auddec : staudio instance which is active now
 *
 * return_value : void
 *
 */
static void
gst_staudio_set_active_element (Gststaudio * previous_auddec,
    Gststaudio * new_auddec)
{
  GststaudioClass *klass;
  klass = GST_STAUDIO_CLASS (G_OBJECT_GET_CLASS (previous_auddec));

  /* acquire the lock to make sure active[dev_id].audio */
  /* is not overwritten by different staudio instance */
  g_mutex_lock (&klass->active[previous_auddec->dev_id].lock);
  klass->active[previous_auddec->dev_id].audio = new_auddec;

  if (new_auddec) {
    new_auddec->mute =
        klass->active[new_auddec->dev_id].mute | new_auddec->mute;
  }

  klass->active[previous_auddec->dev_id].mute = previous_auddec->mute;

  g_mutex_unlock (&klass->active[previous_auddec->dev_id].lock);

  /* post msg to application to inform about active audio element */
  if (new_auddec != NULL) {
    gst_element_post_message (GST_ELEMENT_CAST (new_auddec),
        gst_message_new_element (GST_OBJECT (new_auddec),
            gst_structure_new_empty ("audio-active")));
  }

  return;
}

/* GObject vmethod implementations */
static void
gst_staudio_set_property (GObject * object, guint prop_id, const GValue * value,
    GParamSpec * pspec)
{
  Gststaudio *auddec = GST_STAUDIO (object);

  GST_LOG_OBJECT (auddec, "%s:%d", __FUNCTION__, __LINE__);

  GST_OBJECT_LOCK (auddec);
  switch (prop_id) {
    case PROP_DISABLE_AVSYNC:
      auddec->disable_avsync = g_value_get_boolean (value);
      break;
    case PROP_DISABLE_INJECT:
      auddec->property_disable_dev = g_value_get_boolean (value);
      break;
    case PROP_DEV_ID:
      auddec->dev_id_prop = g_value_get_int (value);
      break;
    case PROP_CHANNEL_CONFIG:
      auddec->channel_config = g_value_get_int (value);
      if (auddec->fd > 0) {
        gst_staudio_ioctl (auddec, AUDIO_CHANNEL_SELECT,
            (void *) auddec->channel_config);
      }
      break;
    case PROP_LATENCY:
      auddec->latency = g_value_get_uint (value);
      break;
    case PROP_MUTE:
    {
      gboolean mute = FALSE;
      mute = g_value_get_boolean (value);

      if (auddec->fd <= 0) {
        /* device is not opened, may be an inactive element */
        /* property can't be set, show warning msg and break */
        GST_WARNING_OBJECT (auddec,
            "device is not opened so cant be %s. will be set when opened",
            (mute ? "muted" : "unmuted"));
        /* property to be set later when device is opened */
        g_mutex_lock (&auddec->write_lock);
        auddec->mute = mute;
        g_mutex_unlock (&auddec->write_lock);
        break;
      }

      /* set mute setting on staudio  */
      if (gst_staudio_ioctl (auddec, AUDIO_SET_MUTE, (void *) mute)) {
        /* audio mute setting is successful so save current mute status */
        auddec->mute = mute;
        GST_INFO_OBJECT (auddec, "Audio is %s ", (mute ? "mute" : "unmute"));
      } else {
        GST_WARNING_OBJECT (auddec, "Failed to %s audio ",
            (mute ? "mute" : "unmute"));
      }
    }
      break;
    case PROP_APPLICATION_TYPE:
      if (auddec->device_config == TRUE) {
        auddec->application_type = g_value_get_int (value);
      } else {
        GST_WARNING_OBJECT (auddec,
            "device configured: can not set application type");
      }
      break;
    case PROP_SERVICE_TYPE:
      if (auddec->device_config == TRUE) {
        auddec->service_type = g_value_get_int (value);
      } else {
        GST_WARNING_OBJECT (auddec,
            "device configured: can not set service type");
      }
      break;
    case PROP_CHANNEL_NUM:
    {
      guint channel_num;
      channel_num = g_value_get_uint (value);
      /* audio supports downmix only for 1, 2, 3, 6, 8, 20 channels */
      if ((channel_num == 1) | (channel_num == 2) | (channel_num ==
              3) | (channel_num == 6) | (channel_num == 8) | (channel_num ==
              20)) {
        auddec->channel_num = channel_num;
      } else {
        GST_WARNING_OBJECT (auddec,
            "channel num %d, for downmix, is not supported", channel_num);
        break;
      }
      if (auddec->fd <= 0) {
        /* device is not opened, may be an inactive element */
        /* property can't be set, show warning msg and break */
        GST_WARNING_OBJECT (auddec,
            "device is not opened so cant set channel num %d",
            auddec->channel_num);
        break;
      }
      /* set number of channel/s to downmix on audio stream */
      if (!gst_staudio_ioctl (auddec, AUDIO_STREAM_DOWNMIX,
              (void *) auddec->channel_num)) {
        GST_WARNING_OBJECT (auddec, "failed to set channel num %d for downmix",
            auddec->channel_num);
      } else {
        GST_INFO_OBJECT (auddec, "downmix set to channel num %d",
            auddec->channel_num);
      }
    }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
  GST_OBJECT_UNLOCK (auddec);
}

static gboolean
gst_staudio_sink_pad_query (GstPad * pad, GstObject * parent, GstQuery * query)
{
  Gststaudio *auddec = GST_STAUDIO (parent);
  gboolean res = FALSE;

  GST_DEBUG_OBJECT (auddec, "%s - (%s)", __FUNCTION__,
      gst_query_type_get_name (GST_QUERY_TYPE (query)));

  switch (GST_QUERY_TYPE (query)) {
    default:
      res = gst_pad_query_default (pad, parent, query);
      break;
  }

  return res;
}

static gboolean
gst_staudio_src_pad_query (GstPad * pad, GstObject * parent, GstQuery * query)
{
  Gststaudio *auddec = GST_STAUDIO (parent);
  GstStructure *structure = NULL;
  gboolean res = FALSE;

  GST_LOG_OBJECT (auddec, "%s - (%s)", __FUNCTION__,
      gst_query_type_get_name (GST_QUERY_TYPE (query)));

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CUSTOM:
      /* This is custom query from staudiosink
       * it is required because the position query does not reach here from sink */
      structure = (GstStructure *) gst_query_get_structure (query);
      if (structure) {
        if (!strncmp ("stream-pos", gst_structure_get_name (structure), 11)) {
          if (auddec->rate > 0.0) {
            gst_structure_set (structure, "stream_pos", G_TYPE_INT64,
                auddec->current_time, NULL);
            res = TRUE;
          } else {
            /* SE drops audio in case of reverse trickmode,
               pts position does not update
             */
            res = FALSE;
          }
        } else {
          res = gst_pad_query_default (pad, parent, query);
        }
      }
      break;
    case GST_QUERY_POSITION:
    {
      GstFormat format;

      gst_query_parse_position (query, &format, NULL);
      if (format == GST_FORMAT_TIME) {
        gst_staudio_update_time (auddec);
        gst_query_set_position (query, format, auddec->current_time);
        res = TRUE;
      } else {
        res = gst_pad_query_default (pad, parent, query);
      }
      break;
    }

    default:
      res = gst_pad_query_default (pad, parent, query);
      break;
  }

  return res;
}

static void
gst_staudio_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  Gststaudio *auddec = GST_STAUDIO (object);

  GST_OBJECT_LOCK (auddec);
  switch (prop_id) {
    case PROP_DISABLE_AVSYNC:
      g_value_set_boolean (value, auddec->disable_avsync);
      break;
    case PROP_DISABLE_INJECT:
      g_value_set_boolean (value, auddec->property_disable_dev);
      break;
    case PROP_DEV_ID:
      g_value_set_int (value, auddec->dev_id);
      break;
    case PROP_CHANNEL_CONFIG:
      g_value_set_int (value, auddec->channel_config);
      break;
    case PROP_LATENCY:
      g_value_set_uint (value, auddec->latency);
      break;
    case PROP_MUTE:
      g_value_set_boolean (value, auddec->mute);
      break;
    case PROP_APPLICATION_TYPE:
      g_value_set_int (value, auddec->application_type);
      break;
    case PROP_SERVICE_TYPE:
      g_value_set_int (value, auddec->service_type);
      break;
    case PROP_CHANNEL_NUM:
      g_value_set_uint (value, auddec->channel_num);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
  GST_OBJECT_UNLOCK (auddec);
}

/* GstElement vmethod implementations */

/* this function handles the link with other elements */
static gboolean
gst_staudio_set_caps (Gststaudio * auddec, GstCaps * caps)
{
  const gchar *mimetype;
  const GstStructure *structure;
  gboolean ret = FALSE;
  GstQuery *query;

  structure = gst_caps_get_structure (caps, 0);
  mimetype = gst_structure_get_name (structure);

  GST_DEBUG_OBJECT (auddec, "param caps: %" GST_PTR_FORMAT, caps);

  if (!strcmp (mimetype, "audio/fake-pes")) {
    /* playback is disabled, terminate audio device */
    gst_staudio_term_device (auddec);
    /* we still want pipeline to be PLAYING so we need to push at least a dummy buffer */
    auddec->tunneled = TRUE;
    if ((GST_PAD_TASK (auddec->srcpad)) &&
        (gst_task_get_state (GST_PAD_TASK (auddec->srcpad)) ==
            GST_TASK_STARTED)) {
      g_mutex_lock (&auddec->dummy_buff.mutex);
      auddec->audio_task_state = GST_STAUDIO_TASK_STARTED;
      g_mutex_unlock (&auddec->dummy_buff.mutex);
    } else {
      gst_staudio_manage_caps (auddec);
    }
    return TRUE;
  }

  /* if not fake-pes caps, we apply property value */
  auddec->disable_dev = auddec->property_disable_dev;

  if (gst_structure_get_int (structure, "depth", &auddec->properties.depth)) {
    GST_DEBUG_OBJECT (auddec, "Bits per sample: %d", auddec->properties.depth);
    /* Due to bug #16975 in the streaming engine,
       audio grab configuration always returns fixed configuration.
       Please keep this until that bug is closed.
     */
#if 0
    auddec->capture.depth = auddec->properties.depth;
#endif
  }
  if (gst_structure_get_int (structure, "rate",
          &auddec->properties.sampling_freq)) {
    GST_DEBUG_OBJECT (auddec, "Samples per second: %d",
        auddec->properties.sampling_freq);
    auddec->capture.sample_rate = auddec->properties.sampling_freq;
  }
  if (gst_structure_get_int (structure, "channels",
          &auddec->properties.num_chans)) {
    GST_DEBUG_OBJECT (auddec, "Channels number: %d",
        auddec->properties.num_chans);
#if 0
    auddec->capture.channels = auddec->properties.num_chans;
#endif
  }
  if (gst_structure_get_int (structure, "bitrate", &auddec->properties.bitrate)) {
    GST_DEBUG_OBJECT (auddec, "bitrate %d", auddec->properties.bitrate);
  }

  if (!strcmp (mimetype, "audio/mpeg") || !strcmp (mimetype, "audio/mpeg-pes")) {
    gint mpegversion = 0;
    gint layer = 0;

    if (!strcmp (mimetype, "audio/mpeg-pes")) {
      auddec->is_pes = TRUE;
    }

    if (gst_structure_get_int (structure, "mpegversion", &mpegversion)) {
      switch (mpegversion) {
        case 1:
          if (gst_structure_get_int (structure, "layer", &layer)) {
            GST_DEBUG_OBJECT (auddec, "layer %d", layer);
            switch (layer) {
              case 1:
                GST_DEBUG_OBJECT (auddec, "codec MPEG1");
                auddec->codec = AUDIO_ENCODING_MPEG1;
                break;
              case 2:
                GST_DEBUG_OBJECT (auddec, "codec MPEG2");
                auddec->codec = AUDIO_ENCODING_MPEG2;
                break;
              case 3:
                GST_DEBUG_OBJECT (auddec, "codec MP3");
                auddec->codec = AUDIO_ENCODING_MP3;
                break;
            }
          } else {
            GST_DEBUG_OBJECT (auddec, "codec MPEG1 Layer 2");
            auddec->codec = AUDIO_ENCODING_MPEG2;
          }
          break;
        case 2:
        case 4:
        {
          const GValue *codec_data_value;

          GST_DEBUG_OBJECT (auddec, "codec AAC");
          auddec->codec = AUDIO_ENCODING_AAC;

          /* REVIEW/PERFORMANCE: Why not just save a reference to the GstBuffer
           * instead of creating a copy of exactly the same content ? */
          if ((codec_data_value =
                  gst_structure_get_value (structure, "codec_data"))) {
            GstBuffer *codec_data_buffer;
            GstMapInfo info_read, info_write;

            codec_data_buffer = gst_value_get_buffer (codec_data_value);
            gst_buffer_map (codec_data_buffer, &info_read, GST_MAP_READ);

            GST_DEBUG_OBJECT (auddec, "additional codec data (%d bytes)",
                info_read.size);

            auddec->codec_data = gst_buffer_new_and_alloc (info_read.size);
            if (auddec->codec_data != NULL) {
              gst_buffer_map (auddec->codec_data, &info_write, GST_MAP_WRITE);
              memcpy (info_write.data, info_read.data, info_read.size);
              gst_buffer_unmap (auddec->codec_data, &info_write);
            }
            gst_buffer_unmap (codec_data_buffer, &info_read);
          }
          break;
        }
        default:
          break;
      }
    }
  } else if (!strcmp (mimetype, "audio/x-ac3")
      || !strcmp (mimetype, "audio/ac3")
      || !strcmp (mimetype, "audio/x-eac3")
      || !strcmp (mimetype, "audio/x-sac3")
      || !strcmp (mimetype, "audio/x-dd")
      || !strcmp (mimetype, "audio/x-ac3-pes")) {
    GST_DEBUG_OBJECT (auddec, "codec AC3");
    if (!strcmp (mimetype, "audio/x-ac3-pes")) {
      auddec->is_pes = TRUE;
    }
    auddec->codec = AUDIO_ENCODING_AC3;
  } else if (!strcmp (mimetype, "audio/x-private1-ac3")) {
    GST_DEBUG_OBJECT (auddec, "codec AC3");
    auddec->codec = AUDIO_ENCODING_AC3;
    auddec->properties.dvd_mode = TRUE;
  } else if (!strcmp (mimetype, "audio/x-dts")
      || !strcmp (mimetype, "audio/x-dts-pes")) {
    GST_DEBUG_OBJECT (auddec, "codec DTS");
    if (!strcmp (mimetype, "audio/x-dts-pes")) {
      auddec->is_pes = TRUE;
    }
    auddec->codec = AUDIO_ENCODING_DTS;
  } else if (!strcmp (mimetype, "audio/x-private1-dts")) {
    GST_DEBUG_OBJECT (auddec, "codec DTS");
    auddec->codec = AUDIO_ENCODING_DTS;
    auddec->properties.dvd_mode = TRUE;
  } else if (!strcmp (mimetype, "audio/x-raw")) {
    GST_DEBUG_OBJECT (auddec, "codec PCM");
    auddec->codec = AUDIO_ENCODING_PCM;
  } else if (!strcmp (mimetype, "audio/x-private1-lpcm")
      || !strcmp (mimetype, "audio/x-private1-lpcm-pes")) {
    GST_DEBUG_OBJECT (auddec, "codec DVD LPCM");
    if (!strcmp (mimetype, "audio/x-private1-lpcm-pes")) {
      auddec->is_pes = TRUE;
    }
    auddec->codec = AUDIO_ENCODING_PCM;
    auddec->properties.dvd_mode = TRUE;
    auddec->properties.endianness = G_BIG_ENDIAN;
  } else if (!strcmp (mimetype, "audio/x-private-ts-lpcm")
      || !strcmp (mimetype, "audio/x-private-ts-lpcm-pes")) {
    GST_DEBUG_OBJECT (auddec, "codec LPCMB");
    if (!strcmp (mimetype, "audio/x-private-ts-lpcm-pes")) {
      auddec->is_pes = TRUE;
    }
    auddec->codec = AUDIO_ENCODING_LPCMB;
  } else if (!strcmp (mimetype, "audio/x-flac")) {
    GST_DEBUG_OBJECT (auddec, "codec FLAC");
    auddec->codec = AUDIO_ENCODING_FLAC;
  } else if (!strcmp (mimetype, "audio/x-pn-realaudio")) {
    GST_DEBUG_OBJECT (auddec, "codec RMA");
    auddec->codec = AUDIO_ENCODING_RMA;
  } else if (!strcmp (mimetype, "audio/x-vorbis")) {
    GST_DEBUG_OBJECT (auddec, "codec VORBIS");
    auddec->priv_data = (void *) g_malloc0 (sizeof (vorbisprivatedata_t));
    if (auddec->priv_data != NULL) {
      vorbisprivatedata_t *p = (vorbisprivatedata_t *) auddec->priv_data;
      p->saved = FALSE;
      p->page1 = NULL;
      p->page2 = NULL;
      p->page3 = NULL;
    }
    auddec->codec = AUDIO_ENCODING_VORBIS;
  } else if (!strcmp (mimetype, "audio/x-wma")) {
    /* FIXME : We should'nt have to recreate an ASF structure but this is what Player2 is expecting !!!! */
    /* ASF STREAM PROPERTIES OBJECT */
    guint8 object_id[16] =
        { 0x91, 0x07, 0xDC, 0xB7, 0xB7, 0xA9, 0xCF, 0x11, 0x8E, 0xE6, 0x00,
      0xC0, 0x0C, 0x20, 0x53, 0x65
    };
    /* ASF AUDIO MEDIA */
    guint8 stream_type[16] =
        { 0x40, 0x9E, 0x69, 0xF8, 0x4D, 0x5B, 0xCF, 0x11, 0xA8, 0xFD, 0x00,
      0x80, 0x5F, 0x5C, 0x44, 0x2B
    };
    /* ASF AUDIO SPREAD */
    guint8 err_correction_type[16] =
        { 0x50, 0xCD, 0xC3, 0xBF, 0x8F, 0x61, 0xCF, 0x11, 0x8B, 0xB2, 0x00,
      0xAA, 0x00, 0xB4, 0xE2, 0x20
    };

    const GValue *codec_data_value;
    GstBuffer *codec_data_buffer = NULL;
    guint type_specific_data_length = 18;
    guint err_correction_data_length = 0;
    guint16 flags = 1;
    guint16 codec_id = 0;
    guint16 block_alignment = 0;
    guint16 codec_specific_data_size = 0;
    guint16 num_of_chans = (guint16) (auddec->properties.num_chans);
    guint16 bits_per_sample = (guint16) (auddec->properties.depth);
    guint samples_per_second = (guint) (auddec->properties.sampling_freq);
    guint bytes_per_second = (guint) (auddec->properties.bitrate / 8);
    guint reserved = 0;
    guint offset = 0;
    gint wmaversion = 0;
    gint temp_block_alignment;
    guint64 time_offset = 0;
    guint64 object_size = 96;
    GstMapInfo info_write, info_read;

    auddec->codec = AUDIO_ENCODING_WMA;

    if (gst_structure_get_int (structure, "wmaversion", &wmaversion)) {
      switch (wmaversion) {
        case 1:
          GST_DEBUG_OBJECT (auddec, "WMA 1");
          codec_id = 0x0160;
          break;
        case 2:
          GST_DEBUG_OBJECT (auddec, "WMA 2");
          codec_id = 0x0161;
          break;
        case 3:
          GST_DEBUG_OBJECT (auddec, "WMA 9 pro");
          codec_id = 0x0162;
          break;
        case 4:
          GST_DEBUG_OBJECT (auddec, "WMA Lossless");
          codec_id = 0x0163;
          break;
        default:
          codec_id = 0;
          break;
      }
    }

    if (gst_structure_get_int (structure, "block_align", &temp_block_alignment)) {
      block_alignment = (guint16) temp_block_alignment;
    }

    if ((codec_data_value = gst_structure_get_value (structure, "codec_data"))) {
      codec_data_buffer = gst_value_get_buffer (codec_data_value);
      codec_specific_data_size = gst_buffer_get_size (codec_data_buffer);
      type_specific_data_length += codec_specific_data_size;
    }

    object_size += codec_specific_data_size;
    auddec->codec_data = gst_buffer_new_and_alloc (object_size);

    if (auddec->codec_data != NULL) {
      gst_buffer_map (auddec->codec_data, &info_write, GST_MAP_WRITE);
      memcpy (info_write.data, object_id, sizeof (object_id));
      offset += sizeof (object_id);
      memcpy (info_write.data + offset, &object_size, sizeof (object_size));
      offset += sizeof (object_size);
      memcpy (info_write.data + offset, stream_type, sizeof (stream_type));
      offset += sizeof (stream_type);
      memcpy (info_write.data + offset,
          err_correction_type, sizeof (err_correction_type));
      offset += sizeof (err_correction_type);
      memcpy (info_write.data + offset, &time_offset, sizeof (time_offset));
      offset += sizeof (time_offset);
      memcpy (info_write.data + offset,
          &type_specific_data_length, sizeof (type_specific_data_length));
      offset += sizeof (type_specific_data_length);
      memcpy (info_write.data + offset,
          &err_correction_data_length, sizeof (err_correction_data_length));
      offset += sizeof (err_correction_data_length);
      memcpy (info_write.data + offset, &flags, sizeof (flags));
      offset += sizeof (flags);
      memcpy (info_write.data + offset, &reserved, sizeof (reserved));
      offset += sizeof (reserved);
      memcpy (info_write.data + offset, &codec_id, sizeof (codec_id));
      offset += sizeof (codec_id);
      memcpy (info_write.data + offset, &num_of_chans, sizeof (num_of_chans));
      offset += sizeof (num_of_chans);
      memcpy (info_write.data + offset,
          &samples_per_second, sizeof (samples_per_second));
      offset += sizeof (samples_per_second);
      memcpy (info_write.data + offset,
          &bytes_per_second, sizeof (bytes_per_second));
      offset += sizeof (bytes_per_second);
      memcpy (info_write.data + offset, &block_alignment,
          sizeof (block_alignment));
      offset += sizeof (block_alignment);
      memcpy (info_write.data + offset, &bits_per_sample,
          sizeof (bits_per_sample));
      offset += sizeof (bits_per_sample);
      memcpy (info_write.data + offset,
          &codec_specific_data_size, sizeof (codec_specific_data_size));
      offset += sizeof (codec_specific_data_size);
      if (codec_data_value) {
        gst_buffer_map (codec_data_buffer, &info_read, GST_MAP_READ);
        memcpy (info_write.data + offset,
            info_read.data, codec_specific_data_size);
        gst_buffer_unmap (codec_data_buffer, &info_read);
      }
      gst_buffer_unmap (auddec->codec_data, &info_write);
    }
  } else if (!strcmp (mimetype, "audio/x-mlp")
      || !strcmp (mimetype, "audio/x-mlp-pes")) {
    GST_DEBUG_OBJECT (auddec, "codec MLP");
    if (!strcmp (mimetype, "audio/x-mlp-pes")) {
      auddec->is_pes = TRUE;
    }
    auddec->codec = AUDIO_ENCODING_MLP;
  } else if (!strcmp (mimetype, "audio/x-dra")
      || !strcmp (mimetype, "audio/x-dra-pes")) {
    GST_DEBUG_OBJECT (auddec, "codec DRA");
    if (!strcmp (mimetype, "audio/x-dra-pes")) {
      auddec->is_pes = TRUE;
    }
    auddec->codec = AUDIO_ENCODING_DRA;
  } else if (!strcmp (mimetype, "audio/x-adpcm")) {
    G_CONST_RETURN gchar *layout;
    const GValue *codec_data_value;
    GstBuffer *codec_data_buffer = NULL;
    GstMapInfo info_read, info_write;

    GST_DEBUG_OBJECT (auddec, "codec ADPCM");
    layout = gst_structure_get_string (structure, "layout");
    if ((layout != NULL) && (strcmp (layout, "microsoft") == 0)) {
      GST_DEBUG_OBJECT (auddec, "microsoft ADPCM");
      auddec->codec = AUDIO_ENCODING_MS_ADPCM;
    } else if ((layout != NULL) && (strcmp (layout, "dvi") == 0)) {
      GST_DEBUG_OBJECT (auddec, "DVI ADPCM");
      auddec->codec = AUDIO_ENCODING_IMA_ADPCM;
    } else {
      GST_WARNING_OBJECT (auddec, "NOT SUPPORTED LAYOUT ADPCM");
    }

    switch (auddec->codec) {
      case AUDIO_ENCODING_MS_ADPCM:
      case AUDIO_ENCODING_IMA_ADPCM:
        /* REVIEW/PERFORMANCE: Why not just save a reference to the GstBuffer
         * instead of creating a copy of exactly the same content ? */
        codec_data_value = gst_structure_get_value (structure, "codec_data");
        if (codec_data_value != NULL) {
          codec_data_buffer = gst_value_get_buffer (codec_data_value);
          gst_buffer_map (codec_data_buffer, &info_read, GST_MAP_READ);
          GST_DEBUG_OBJECT (auddec, "additional codec data (%d bytes)",
              info_read.size);
          auddec->codec_data = gst_buffer_new_and_alloc (info_read.size);
          if (auddec->codec_data != NULL) {
            gst_buffer_map (auddec->codec_data, &info_write, GST_MAP_WRITE);
            memcpy (info_write.data, info_read.data, info_read.size);
            gst_buffer_unmap (auddec->codec_data, &info_write);
          }
          gst_buffer_unmap (codec_data_buffer, &info_read);
        }
        break;
      default:
        break;
    }
  } else {
    return FALSE;
  }

  query = gst_query_new_latency ();
  if (query != NULL) {
    GstClockTime min_latency, max_latency;
    if (gst_pad_peer_query (auddec->sinkpad, query)) {
      gst_query_parse_latency (query, &auddec->live, &min_latency,
          &max_latency);
      GST_DEBUG_OBJECT (auddec,
          "Peer latency: live %s, min %" GST_TIME_FORMAT " max %"
          GST_TIME_FORMAT, auddec->live ? "TRUE" : "FALSE",
          GST_TIME_ARGS (min_latency), GST_TIME_ARGS (max_latency));
    }
    gst_query_unref (query);
  }

  /* For now we have to return TRUE to start the audio playback, some demux requires 2 audio device */
  ret = gst_staudio_manage_caps (auddec);
  auddec->device_config = TRUE;

  return ret;
}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_staudio_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
  Gststaudio *auddec = GST_STAUDIO (parent);

  GST_LOG_OBJECT (auddec, "%s:%d", __FUNCTION__, __LINE__);

  if (auddec->first_buffer_received == FALSE) {
    auddec->first_buffer_received = TRUE;
  }

  /* The manage_id_function waits till the downstream element
     is connected and opens th  device corresponding to the
     downstream element. Also decides if staudio needs to be
     in tunnelled or non-tunnelled */
  if ((auddec->device_config) && (gst_buffer_get_size (buf) > 0)) {
    if (!gst_staudio_manage_dev_id (auddec)) {
      GST_ERROR_OBJECT (auddec, "error in device id management");
      if (auddec->srcresult != GST_FLOW_OK) {
        GST_ERROR_OBJECT (auddec, "error in data flow %s ",
            gst_flow_get_name (auddec->srcresult));
      }
      goto exit;
    }
    auddec->device_config = FALSE;
    auddec->channel_change = FALSE;
  }

  if (auddec->channel_change && !auddec->device_config) {
    auddec->channel_change = FALSE;
    gst_staudio_ioctl (auddec, AUDIO_SET_AV_SYNC, (void *) auddec->set_avsync);
    gst_staudio_ioctl (auddec, AUDIO_SET_ENCODING, (void *) auddec->codec);
  }

  /* reset sticky flag on first buffer received after each flushing seek */
  auddec->flushing_seek = FALSE;

  /* The resuming condition is for the case when capture task has been paused if srcresult is not GST_FLOW_OK.
   * The capture task needs to be started again if the upstream element recovers and sends the buffers to
   * staudio. In that case, staudio may not go through the state transition & capture task won't be started.
   */

  if (GST_PAD_TASK (auddec->srcpad)
      && (gst_task_get_state (GST_PAD_TASK (auddec->srcpad)) ==
          GST_TASK_PAUSED) && !auddec->exit_loop) {
    if (gst_pad_start_task (auddec->srcpad,
            (GstTaskFunction) gst_staudio_audio_loop, auddec, NULL) != TRUE) {
      GST_ERROR_OBJECT (auddec, "cannot start capture task");
    }
  }

  if ((auddec->tunneled == TRUE) && (auddec->new_segment_received == TRUE)) {
    /* Send for tunneled, the Pending New Segment events */
    GST_LOG_OBJECT (auddec, "Pushing NEWSEGMENT events downstream");
    if (FALSE == gst_staudio_send_queued_events_before (auddec,
            GST_CLOCK_TIME_NONE)) {
      GST_WARNING_OBJECT (auddec,
          "Pushing NEWSEGMENT events downstream failed");
    }
    auddec->new_segment_received = FALSE;
  }

  if (auddec->tunneled) {
    g_mutex_lock (&auddec->dummy_buff.mutex);
    /* signal the waiting task only if required */
    if (auddec->first_frame_pushed == FALSE) {
      GST_LOG_OBJECT (auddec, "signal the audio loop task");
      GST_STAUDIO_AUDIO_LOOP_SIGNAL (auddec);
    }
    g_mutex_unlock (&auddec->dummy_buff.mutex);
  }

  /* pause the audio frame push task */
  if (auddec->srcresult != GST_FLOW_OK) {
    if (auddec->tunneled == FALSE)
      gst_pad_pause_task (auddec->srcpad);
    GST_ERROR_OBJECT (auddec, "error in data flow %s ",
        gst_flow_get_name (auddec->srcresult));
    goto exit;
  }

  if (auddec->disable_dev == TRUE) {
    GST_LOG_OBJECT (auddec, "audio decoder is disabled, exit chain");
    goto exit;
  }

  if (auddec->discontinuity) {
    GST_DEBUG_OBJECT (auddec, "flush caused discontinuity in data flow");
    GST_BUFFER_FLAG_SET (buf, GST_BUFFER_FLAG_DISCONT);
    auddec->discontinuity = FALSE;
  }

  /* save 1st 3 pages here for vorbis */
  if (auddec->codec == AUDIO_ENCODING_VORBIS) {
    gst_staudio_save_vorbis_packet (auddec, buf);
  }

  if (auddec->fd == -1) {
    gst_staudio_queue_packet (auddec, buf);
    return GST_FLOW_OK;
  }

  if (!gst_staudio_write_packet (auddec, buf)) {
    GST_DEBUG_OBJECT (auddec, "failed to write buffer");
  }

  /* Update the last buffer timestamp */
  if (GST_BUFFER_TIMESTAMP_IS_VALID (buf) &&
      ((auddec->last_buffer_ts <
              GST_TIME_AS_MSECONDS (GST_BUFFER_TIMESTAMP (buf)))
          || (auddec->last_buffer_ts == GST_CLOCK_TIME_NONE))) {
    GST_LOG_OBJECT (auddec, "Setting Last TimeStamp to %lld ms ",
        GST_TIME_AS_MSECONDS (GST_BUFFER_TIMESTAMP (buf)));
    auddec->last_buffer_ts = GST_TIME_AS_MSECONDS (GST_BUFFER_TIMESTAMP (buf));
  }

exit:
  gst_buffer_unref (buf);
  return auddec->srcresult;
}

/* change_state function
 * this function does the actual processing
 */
static GstStateChangeReturn
gst_staudio_change_state (GstElement * element, GstStateChange transition)
{
  Gststaudio *auddec = GST_STAUDIO (element);
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;

  GST_DEBUG_OBJECT (auddec, "%d -> %d",
      GST_STATE_TRANSITION_CURRENT (transition),
      GST_STATE_TRANSITION_NEXT (transition));

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      g_mutex_init (&auddec->dummy_buff.mutex);
      g_cond_init (&auddec->dummy_buff.cond);
      g_mutex_init (&auddec->write_lock);
      g_mutex_init (&auddec->event_lock);

      /* Open audio capture device */
      if (auddec->v4l2_fd < 0) {
        auddec->v4l2_fd =
            v4l2_open_by_name (V4L2_CAPTURE_DRIVER_NAME, V4L2_CAPTURE_CARD_NAME,
            O_RDWR);

        if (auddec->v4l2_fd < 0) {
          GST_ERROR_OBJECT (auddec,
              "Couldn't open v4l2 device %s %s\n", V4L2_CAPTURE_DRIVER_NAME,
              strerror (errno));
          return GST_STATE_CHANGE_FAILURE;
        }
      }
      auddec->adapter = gst_adapter_new ();
      gst_staudio_start_monitor_task (auddec);
      auddec->disable_dev = FALSE;
      auddec->property_disable_dev = FALSE;
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      gst_segment_init (&auddec->segment, GST_FORMAT_TIME);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
    {
      struct audio_status status;

      if (gst_staudio_ioctl (auddec, AUDIO_GET_STATUS,
              (void *) &status) == TRUE) {
        if ((auddec->disable_dev == TRUE) || (auddec->fd < 0)) {
          auddec->state = AUDIO_PLAYING;
        } else {
          auddec->state = status.play_state;
        }

        GST_OBJECT_LOCK (auddec);
        if (auddec->tunneled == TRUE) {
          gboolean ret = TRUE;
          if (auddec->segment.rate < 0.0) {
            gint speed = auddec->segment.rate * DVB_SPEED_NORMAL_PLAY;
            ret = gst_staudio_ioctl (auddec, AUDIO_SET_SPEED, (void *) speed);
          } else {
            ret = gst_staudio_ioctl (auddec, AUDIO_CONTINUE, NULL);
          }
          if (ret)
            auddec->state = AUDIO_PLAYING;
        }
        GST_OBJECT_UNLOCK (auddec);

        if ((auddec->eos == TRUE) && (auddec->eos_set == FALSE)) {
          /* EOS discontinuity was received in paused state,
             so set eos discontinuity now
           */
          auddec->eos_set = TRUE;
          gst_staudio_ioctl (auddec, AUDIO_DISCONTINUITY,
              (void *) AUDIO_DISCONTINUITY_EOS);
        }
      }
    }
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      auddec->exit_loop = TRUE;
      if (auddec->tunneled == TRUE) {
        /* Before quit, application should set EOS discontinuity
           on audio stream to indicate end of decode so that if
           other applications like transcode are running, they
           can get notified.
           As per bugzilla 53722, SE does not accept discontinuity
           in paused state so we need to set stream in play state
           before setting discontinuity. its a workaround to get
           rid of issue in SE that it does not accept discontinuity
           in paused state.
           once SE rectifies it, AUDIO_CLEAR_BUFFER, AUDIO_PLAY at
           this stage should not be required. */
        GST_FIXME_OBJECT (auddec, "AUDIO_CLEAR_BUFFER ioctl is not required.");
        GST_FIXME_OBJECT (auddec, "AUDIO_PLAY ioctl is not required.");
        /* clear the pes buffer so that we dont hear data play after
           pause during quit */
        gst_staudio_ioctl (auddec, AUDIO_CLEAR_BUFFER, NULL);
        if (gst_staudio_ioctl (auddec, AUDIO_PLAY, NULL) == TRUE) {
          gst_staudio_ioctl (auddec, AUDIO_DISCONTINUITY,
              (void *) AUDIO_DISCONTINUITY_EOS);
        }
      }
      if (gst_staudio_ioctl (auddec, AUDIO_STOP, NULL) == FALSE) {
        GST_DEBUG_OBJECT (auddec, "AUDIO_STOP failed");
        return GST_STATE_CHANGE_FAILURE;
      } else {
        auddec->state = AUDIO_STOPPED;
      }
      if (auddec->tunneled == FALSE) {
        GstTask *task = NULL;
        /* release the audio loop task struck in DQBUF ioctl */
        /* in background record, no data will be pushed from
           stts_demux so this task will never get created since
           we create this task in chain on valid data reception
         */
        task = GST_PAD_TASK (auddec->srcpad);
        if (task != NULL) {
          gst_task_pause (task);
        }
        gst_staudio_ioctl (auddec, AUDIO_DISCONTINUITY,
            (void *) AUDIO_DISCONTINUITY_EOS);

        if (task != NULL) {
          GST_PAD_STREAM_LOCK (auddec->srcpad);
          GST_PAD_STREAM_UNLOCK (auddec->srcpad);
        }
      } else {
        /* signal the waiting task, so as to stop */
        g_mutex_lock (&auddec->dummy_buff.mutex);
        auddec->audio_task_state = GST_STAUDIO_TASK_PAUSED;
        GST_STAUDIO_AUDIO_LOOP_SIGNAL (auddec);
        g_mutex_unlock (&auddec->dummy_buff.mutex);
        if ((GST_PAD_TASK (auddec->srcpad))
            && (gst_task_get_state (GST_PAD_TASK (auddec->srcpad)) ==
                GST_TASK_STARTED)) {
          gst_pad_pause_task (auddec->srcpad);
        }
      }
      auddec->srcresult = GST_FLOW_OK;
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
    {
      struct audio_status status;
      if (auddec->tunneled == TRUE) {
        if (gst_staudio_ioctl (auddec, AUDIO_GET_STATUS,
                (void *) &status) == TRUE) {
          if ((auddec->disable_dev == TRUE) || (auddec->fd < 0)) {
            auddec->state = AUDIO_PAUSED;
          } else {
            auddec->state = status.play_state;
          }
          if (auddec->state != AUDIO_PAUSED) {
            gboolean ret = TRUE;
            if (auddec->segment.rate < 0.0)
              ret =
                  gst_staudio_ioctl (auddec, AUDIO_SET_SPEED,
                  (void *) DVB_SPEED_REVERSE_STOPPED);
            else
              ret = gst_staudio_ioctl (auddec, AUDIO_PAUSE, NULL);

            if (ret) {
              GST_DEBUG_OBJECT (auddec, "AUDIO_PAUSED");
              auddec->state = AUDIO_PAUSED;
            } else {
              return GST_STATE_CHANGE_FAILURE;
            }
          }
        }


          auddec->first_frame_pushed = FALSE;
          /* push a dummy buffer to basesink */
          GST_DEBUG_OBJECT (auddec, "Will push a dummy buf ");
          g_mutex_lock (&auddec->dummy_buff.mutex);
          GST_STAUDIO_AUDIO_LOOP_SIGNAL (auddec);
          g_mutex_unlock (&auddec->dummy_buff.mutex);

        if (auddec->live && auddec->is_pes) {
          auddec->segment.start = MPEGTIME_TO_GSTTIME (auddec->currentPTS);
          auddec->segment.time = 0;
        }
      }
    }
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      if (auddec->priv_data != NULL) {
        gst_staudio_free_vorbis_packet (auddec);
      }
      gst_adapter_clear (auddec->adapter);
      if (auddec->tunneled == TRUE) {
        /* signal the waiting task, so as to stop */
        g_mutex_lock (&auddec->dummy_buff.mutex);
        auddec->audio_task_state = GST_STAUDIO_TASK_STOPPED;
        GST_STAUDIO_AUDIO_LOOP_SIGNAL (auddec);
        g_mutex_unlock (&auddec->dummy_buff.mutex);
      } else {
        if (auddec->capture.is_stream_on_done) {
          if (gst_staudio_capture_stream_off (auddec, auddec->v4l2_fd)) {
            /* free the Capture Buffer */
            gst_staudio_capture_free_buffer (auddec, auddec->v4l2_fd);
            auddec->capture.is_stream_on_done = FALSE;
          }
        }
      }
      if ((GST_PAD_TASK (auddec->srcpad))
          && (gst_task_get_state (GST_PAD_TASK (auddec->srcpad)) !=
              GST_TASK_STOPPED)) {
        gst_pad_stop_task (auddec->srcpad);
      }
      if (auddec->capture.pool) {
        gst_object_unref (auddec->capture.pool);
        auddec->capture.pool = NULL;
      }
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      gst_object_unref (auddec->adapter);
      /* Close V4L2 device */
      if (auddec->v4l2_fd != -1) {
        close (auddec->v4l2_fd);
        auddec->v4l2_fd = -1;
      }
      gst_staudio_term_device (auddec);
      gst_staudio_stop_monitor_task (auddec);

      g_mutex_clear (&auddec->dummy_buff.mutex);
      g_cond_clear (&auddec->dummy_buff.cond);
      g_mutex_clear (&auddec->write_lock);
      g_mutex_clear (&auddec->event_lock);
      break;
    default:
      break;
  }

  return ret;
}

static gboolean
gst_staudio_src_pad_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
  Gststaudio *auddec = GST_STAUDIO (parent);
  gboolean ret = TRUE;

  GST_DEBUG_OBJECT (auddec, "%s - (%s)", __FUNCTION__,
      gst_event_type_get_name (GST_EVENT_TYPE (event)));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CUSTOM_UPSTREAM:
    {
      const GstStructure *structure;
      Gststaudio *current_active = NULL;
      GststaudioClass *klass;
      klass = GST_STAUDIO_CLASS (G_OBJECT_GET_CLASS (auddec));
      structure = gst_event_get_structure (event);
      gchar *str_element = gst_element_get_name (auddec);

      if (gst_structure_has_name (structure, "track-switch")) {
        gboolean active;

        if (gst_structure_get_boolean (structure, "intial-active",
                &active) == TRUE) {
          GST_INFO_OBJECT (auddec, "%s received intial-active", str_element);

          /* if initial-active comes on pad other than the currently active pad, */
          /* we need to send close device on current active pad, and then */
          /* open device on other one, on which event has come. */
          g_mutex_lock (&klass->active[auddec->dev_id].lock);
          current_active = gst_staudio_get_active_element (auddec);
          g_mutex_unlock (&klass->active[auddec->dev_id].lock);
          if (current_active != NULL && current_active != auddec) {
            GST_INFO_OBJECT (auddec,
                "intial-active: current_active is not NULL and current_active != auddec ");

            gst_staudio_close_device (current_active, FALSE);

            /* send open command only after close command for current active pad */
            /* has completed successfully, to avoid any race condition */
            g_mutex_lock (&klass->active[auddec->dev_id].lock);
            current_active = gst_staudio_get_active_element (auddec);
            g_mutex_unlock (&klass->active[auddec->dev_id].lock);

            if (current_active != NULL) {
              GST_DEBUG_OBJECT (auddec, "current auddec not deactivated yet");
              ret = FALSE;
              break;
            }
            /* added sleep here to make sure, previous close command is completed successfully */
            /* before sending open, to avoid device busy error */
            usleep (10 * 1000);

            /* For configuring the dev-id on next chain function */
            /* re-negotiation is needed by manage_dev_id() to get dev-id to open */
            /* So open device to be done in manage_dev_id itself */
            auddec->device_config = TRUE;
          } else {
            /* For current_active = NULL or current_active = auddec */
            GST_INFO_OBJECT (auddec,
                "intial-active: current_active is NULL or current_active = auddec ");
          }
          GST_OBJECT_LOCK (auddec);
          auddec->is_active = TRUE;
          GST_OBJECT_UNLOCK (auddec);

        } else if (gst_structure_get_boolean (structure, "active",
                &active) == TRUE) {
          if (active == FALSE) {
            GST_INFO_OBJECT (auddec, "%s de-activated", str_element);
            gst_staudio_close_device (auddec, FALSE);
            GST_OBJECT_LOCK (auddec);
            auddec->is_active = FALSE;
            GST_OBJECT_UNLOCK (auddec);
          } else {
            GST_INFO_OBJECT (auddec, "%s activated", str_element);
            /* send open command only after close command for current active pad */
            /* has completed successfully, to avoid any race condition */
            g_mutex_lock (&klass->active[auddec->dev_id].lock);
            current_active = gst_staudio_get_active_element (auddec);
            g_mutex_unlock (&klass->active[auddec->dev_id].lock);
            if (current_active == NULL) {
              /* added sleep here to make sure, previous close command is complated successfully */
              /* before sending open, to avoid device busy error */
              usleep (10 * 1000);
              /* Not opening device, allowing to renegotiate dev-id,
                 though not necessary to renegotiate in this case */
              GST_INFO_OBJECT (auddec,
                  "active upstream activate: current_active is NULL");

            } else {
              GST_INFO_OBJECT (auddec,
                  "active upstream  activate: current_active is not NULL");
              /* dev-id might have changed, so close the auddec device and allow to open in manage_dev_id() */
              gst_staudio_close_device (auddec, FALSE);
              usleep (10 * 1000);
            }

            /* For reconfiguring the dev-id on next chain function */
            /* re-negotiation is needed by manage_dev_id() as dev-id might have changed */
            /* open device here */
            auddec->device_config = TRUE;

            GST_OBJECT_LOCK (auddec);
            auddec->is_active = TRUE;
            GST_OBJECT_UNLOCK (auddec);
          }
        }
        /* send to upstream maybe demux need to handle also */
        /* for example, stasf and stavi need to switch the stream */
        gst_pad_event_default (pad, parent, event);
        /* the event already handled in staudio side so return current ret */
      } else {
        ret = gst_pad_event_default (pad, parent, event);
      }
      g_free (str_element);
      break;
    }

    default:
      ret = gst_pad_event_default (pad, parent, event);
      break;
  }

  return ret;
}

static gboolean
gst_staudio_sink_pad_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
  Gststaudio *auddec = GST_STAUDIO (parent);
  const GstStructure *structure;
  GstFormat format;
  gint speed = 0;
  guint64 start;
  guint64 stop;
  guint64 position;
  gint64 time;
  gdouble rate;
  gboolean ret = TRUE;
  const GstSegment *segment;
  GstCaps *caps;
  gboolean force_pass_event = FALSE;

  GST_DEBUG_OBJECT (auddec, "%s - (%s)", __FUNCTION__,
      gst_event_type_get_name (GST_EVENT_TYPE (event)));


  /* Drop flush start/stop events arriving due to demuxers initiated seeks */
  /* e.g. ogg demuxer initiated seeks in streaming usecases like http */
  /* its a work around, can be removed if demuxers are rectified */
  if (auddec->first_buffer_received == FALSE) {
    switch (GST_EVENT_TYPE (event)) {
        /* flush events during pipeline creation are not expected */
      case GST_EVENT_FLUSH_START:
        GST_DEBUG_OBJECT (auddec, "drop flush start event");
        gst_event_unref (event);
        goto done;

      case GST_EVENT_FLUSH_STOP:
        GST_DEBUG_OBJECT (auddec, "drop flush stop event");
        gst_event_unref (event);
        goto done;

      default:
        break;
        /* do nothing, we will process events as usual */
    }
  }

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_EOS:
    {
      GststaudioClass *klass;
      Gststaudio *current_active = NULL;
      klass = GST_STAUDIO_CLASS (G_OBJECT_GET_CLASS (auddec));
      force_pass_event = TRUE;

      /* acquire the lock to make sure active[dev_id].audio */
      /* is not overwritten by different staudio instance */
      g_mutex_lock (&klass->active[auddec->dev_id].lock);
      current_active = gst_staudio_get_active_element (auddec);
      g_mutex_unlock (&klass->active[auddec->dev_id].lock);

      /* Signal audio loop to avoid hang when EOS is received */
      /* demuxer sends EOS when we jump to 0 in reverse trickmode */
      if (auddec->flushing_seek) {
        g_mutex_lock (&auddec->dummy_buff.mutex);
        GST_STAUDIO_AUDIO_LOOP_SIGNAL (auddec);
        g_mutex_unlock (&auddec->dummy_buff.mutex);
      }

      /* send EOS event for inactive element */
      if (auddec != current_active) {
        break;
      }

      if (auddec->state == AUDIO_PLAYING) {
        /* set EOS discontinuity on audio in playing state */
        auddec->eos_set = TRUE;
        gst_staudio_ioctl (auddec, AUDIO_DISCONTINUITY,
            (void *) AUDIO_DISCONTINUITY_EOS);
      }
      auddec->eos = TRUE;
      gst_event_unref (event);
      goto done;
    }
      break;

    case GST_EVENT_SEGMENT:
      /* Save a copy of NEWSEGMENT event */
      auddec->new_segment_received = TRUE;
      gst_event_parse_segment (event, &segment);
      rate = segment->rate;
      format = segment->format;
      start = segment->start;
      stop = segment->stop;
      position = segment->position;
      time = segment->time;

      /* if direction of data injection is changed, clear eos state */
      if (((rate > 0) && (auddec->rate < 0)) || ((rate < 0)
              && (auddec->rate > 0))) {
        auddec->eos = FALSE;
        auddec->eos_set = FALSE;
      }

      if (rate != auddec->rate && rate != 0.0) {
        gboolean ret = TRUE;
        speed = rate * DVB_SPEED_NORMAL_PLAY;
        if (auddec->state == AUDIO_PAUSED && rate < 0.0)
          ret =
              gst_staudio_ioctl (auddec, AUDIO_SET_SPEED,
              (void *) DVB_SPEED_REVERSE_STOPPED);
        else
          ret = gst_staudio_ioctl (auddec, AUDIO_SET_SPEED, (void *) speed);

        if (ret == FALSE) {
          GST_ERROR_OBJECT (auddec, "AUDIO_SET_SPEED failed");
          gst_event_unref (event);
          return FALSE;
        }
      }
      /* SE starts playing after setting speed. */
      /* therefore restore the audio pause state if required */
      /* FIXME: it is workaround to avoid SE bad behaviour during set speed */
      GST_OBJECT_LOCK (auddec);
      if (auddec->state == AUDIO_PAUSED && rate > 0.0) {
        if (gst_staudio_ioctl (auddec, AUDIO_PAUSE, NULL) == FALSE) {
          GST_ERROR_OBJECT (auddec, "unable to restore AUDIO_PAUSED state");
          gst_event_unref (event);
          GST_OBJECT_UNLOCK (auddec);
          return FALSE;
        }
      }
      GST_OBJECT_UNLOCK (auddec);

      if (format == GST_FORMAT_TIME) {
        GST_DEBUG_OBJECT (auddec, "New segment rate %f "
            "start %" GST_TIME_FORMAT " stop %" GST_TIME_FORMAT
            " position %" GST_TIME_FORMAT, rate,
            GST_TIME_ARGS (start), GST_TIME_ARGS (stop),
            GST_TIME_ARGS (position));
      } else {
        GST_DEBUG_OBJECT (auddec, "New segment format %d "
            "rate %f start %" G_GUINT64_FORMAT " stop %" G_GUINT64_FORMAT
            " position %" G_GUINT64_FORMAT,
            format, rate, start, stop, position);
        format = GST_FORMAT_TIME;
        start = 0;
        stop = GST_CLOCK_TIME_NONE;
        position = 0;
      }

      auddec->rate = rate;
      auddec->update_segment = TRUE;
      if (auddec->tunneled == TRUE || (auddec->first_new_segment
              || !auddec->ptswrap)) {
        auddec->update_segment = FALSE;
        gst_segment_copy_into (segment, &auddec->segment);
        auddec->segment.start = start;
        auddec->segment.format = format;
        auddec->segment.stop = stop;
        auddec->segment.position = position;
        auddec->segment.time = time;
        auddec->segment.rate = rate;
      }
      auddec->ptswrap = FALSE;

      /* set the segment play interval on SE */
      if (rate > 0.0) {
        audio_play_interval_t AudioPlayInterval;
        memset (&AudioPlayInterval, 0, sizeof (AudioPlayInterval));
        AudioPlayInterval.start = GSTTIME_TO_MPEGTIME (start);
        if (GST_CLOCK_TIME_IS_VALID (stop)) {
          AudioPlayInterval.end = (gint64) GSTTIME_TO_MPEGTIME (stop);
        } else {
          AudioPlayInterval.end = DVB_TIME_NOT_BOUNDED;
        }
        /* DVB_TIME_NOT_BOUNDED            0xfedcba9876543210ULL */
        GST_DEBUG_OBJECT (auddec,
            "set play interval on streaming engine start=%llu, end=%llu",
            AudioPlayInterval.start, AudioPlayInterval.end);

        if (gst_staudio_ioctl (auddec, AUDIO_SET_PLAY_INTERVAL,
                (void *) &AudioPlayInterval) == FALSE) {
          GST_WARNING_OBJECT (auddec, "set play interval failed: %s\n",
              strerror (errno));
        }
      } else {
        audio_play_interval_t AudioPlayInterval;
        AudioPlayInterval.start = DVB_TIME_NOT_BOUNDED;
        AudioPlayInterval.end = DVB_TIME_NOT_BOUNDED;
        /* DVB_TIME_NOT_BOUNDED            0xfedcba9876543210ULL */
        GST_DEBUG_OBJECT (auddec,
            "set play interval on streaming engine start=%llu, end=%llu",
            AudioPlayInterval.start, AudioPlayInterval.end);

        if (gst_staudio_ioctl (auddec, AUDIO_SET_PLAY_INTERVAL,
                (void *) &AudioPlayInterval) == FALSE) {
          GST_WARNING_OBJECT (auddec, "set play interval failed: %s\n",
              strerror (errno));
        }
      }

      if ((auddec->first_new_segment == TRUE)) {
        GST_DEBUG_OBJECT (auddec, "pushing downstream first NewSegment event");
        ret = gst_pad_event_default (pad, parent, event);
        auddec->first_new_segment = FALSE;
        auddec->new_segment_received = FALSE;
        goto done;
      } else {
        /* Queue the NewSegment Event if Pad Caps are not yet set */
        if (gst_pad_get_current_caps (auddec->srcpad) == NULL) {
          GST_DEBUG_OBJECT (auddec,
              "Queueing NewSegment event with TimeStamp %lld ms",
              auddec->last_buffer_ts);
          gst_staudio_queue_event (auddec, event, auddec->last_buffer_ts);
          gst_event_unref (event);
          auddec->new_segment_received = TRUE;
          goto done;
        }
      }
      break;

    case GST_EVENT_FLUSH_START:
      ret = gst_pad_event_default (pad, parent, event);
      /* Pause the capture/srcpad task if audio decoder element is non-tunneled */
      if (auddec->tunneled == FALSE) {
        gst_pad_pause_task (auddec->srcpad);
      }

      /* during migration to 1.0 from 0.10, GST_FLOW_WRONG_STATE should be
         replaced by GST_FLOW_FLUSHING
       */
      auddec->srcresult = GST_FLOW_FLUSHING;
      auddec->flush = TRUE;
      /* flush PES buffers into SE */
      gst_staudio_ioctl (auddec, AUDIO_CLEAR_BUFFER, NULL);
      goto done;
      break;

    case GST_EVENT_FLUSH_STOP:
      auddec->srcresult = GST_FLOW_OK;
      auddec->flush = FALSE;
      gst_adapter_clear (auddec->adapter);
      auddec->first_frame_pushed = FALSE;
      auddec->first_injection = TRUE;
      auddec->discontinuity = TRUE;
      auddec->eos = FALSE;
      auddec->eos_set = FALSE;
      auddec->flushing_seek = TRUE;     /* reset only in chain function */
      /* Flush the Queued Events and invalidate the last buffer time */
      auddec->last_buffer_ts = GST_CLOCK_TIME_NONE;
      gst_segment_init (&auddec->segment, GST_FORMAT_TIME);
      gst_staudio_drop_queued_events (auddec);
      force_pass_event = TRUE;
      break;
    case GST_EVENT_CAPS:
      gst_event_parse_caps (event, &caps);
      if ((auddec->prev_caps != NULL)
          && (gst_caps_is_equal ((const GstCaps *) caps,
                  (const GstCaps *) auddec->prev_caps))) {
        GST_DEBUG_OBJECT (auddec, "caps are already set");
        goto exit;
      }
      ret = gst_staudio_set_caps (auddec, caps);
      if (ret) {
        if (auddec->prev_caps) {
          gst_caps_unref (auddec->prev_caps);
        }
        auddec->prev_caps = gst_caps_ref (caps);
      } else {
        GST_ERROR_OBJECT (auddec, "failed to set caps!");
      }
      goto exit;

    case GST_EVENT_CUSTOM_DOWNSTREAM:
    {
      structure = gst_event_get_structure (event);
      if (gst_structure_has_name (structure, "pts-wrap")) {
        GST_DEBUG_OBJECT (auddec, "received pts wrap event");
        auddec->ptswrap = TRUE;
        gst_event_unref (event);
        goto done;
      }
    }
      break;

    case GST_EVENT_CUSTOM_DOWNSTREAM_OOB:
    {
      structure = gst_event_get_structure (event);
      if (gst_structure_has_name (structure, "pcr-data")) {
        GST_DEBUG_OBJECT (auddec, "pcr data received");
        dvb_clock_data_point_t clock_data;
        const GValue *source_time;
        const GValue *system_time;

        source_time = gst_structure_get_value (structure, "source-time");
        system_time = gst_structure_get_value (structure, "system-time");
        clock_data.source_time = g_value_get_uint64 (source_time);
        clock_data.system_time = g_value_get_uint64 (system_time);
        clock_data.flags = auddec->clock_data_flags | DVB_CLOCK_FORMAT_PTS;
        /* We want to send clock data point to SE asap, so just wait for device to be opened */
        if (auddec->fd != -1) {
          GST_DEBUG_OBJECT (auddec, "AUDIO_SET_CLOCK_DATA_POINT (%llu, %llu)",
              clock_data.source_time, clock_data.system_time);
          if (gst_staudio_ioctl (auddec, AUDIO_SET_CLOCK_DATA_POINT,
                  (void *) &clock_data) == FALSE) {
            GST_WARNING_OBJECT (auddec, "unable to set clock data point data");
          }
          auddec->clock_data_flags = 0;
        } else {
          GST_DEBUG_OBJECT (auddec, "dropping PCR data (%llu, %llu)",
              clock_data.source_time, clock_data.system_time);
          /* Inform upstream element, that pcr-data event has not been handled */
          ret = FALSE;
        }
        goto exit;
      }
      /* Disable av-sync on audio pad before playing the audio only channel */
      if (gst_structure_has_name (structure, "av-sync")) {
        gboolean set_avsync = FALSE;
        gst_structure_get_boolean (structure, "sync-disable", &set_avsync);
        /* Enable av-sync only if not disabled by set property permanently */
        if (!((auddec->disable_avsync == TRUE) && (set_avsync == TRUE))) {
          gst_staudio_ioctl (auddec, AUDIO_SET_AV_SYNC, (void *) set_avsync);
          GST_DEBUG_OBJECT (auddec, "AV_SYNC done");
        }
        auddec->set_avsync = set_avsync;
        goto exit;
      }
      if (gst_structure_has_name (structure, "index-support")) {
        gboolean timeshift_playback = FALSE;
        /* timeshift mode - upstream recorder */
        gst_structure_get_boolean (structure, "timeshift-playback",
            &timeshift_playback);
        if (timeshift_playback == TRUE) {
          auddec->live = FALSE;
        } else {
          auddec->live = TRUE;
        }
      } else if (gst_structure_has_name (structure, "hwdemux-connection")) {
        gst_structure_get_boolean (structure, "channel_change",
            &auddec->channel_change);
      }
    }
      break;
    case GST_EVENT_TAG:
      ret = gst_pad_event_default (pad, parent, event);
      goto done;
      break;

    case GST_EVENT_STREAM_START:
      ret = gst_pad_event_default (pad, parent, event);
      goto done;

    default:
      break;
  }
  /* Pass event downstream if audio decoder element is tunneled or event is not-serialized
     or Src Pad Caps are not yet set or force_pass_event flag is set. Else Queue the Serialized Event */
  if ((force_pass_event == FALSE)
      && (gst_pad_get_current_caps (auddec->srcpad) != NULL)
      && (FALSE == auddec->tunneled) && GST_EVENT_IS_SERIALIZED (event)) {
    GST_DEBUG_OBJECT (auddec,
        "Queueing serialized event %s with TimeStamp %lld ms",
        GST_EVENT_TYPE_NAME (event), auddec->last_buffer_ts);
    gst_staudio_queue_event (auddec, event, auddec->last_buffer_ts);
    gst_event_unref (event);
  } else {
    /* Push event downstream */
    ret = gst_pad_event_default (pad, parent, event);
  }
  goto done;

exit:
  gst_event_unref (event);

done:
  return ret;
}

/* initialize the staudio's class */
static void
gst_staudio_class_init (GststaudioClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  /* initialize structure active */
  /* active[dev_id].audio to point the active staudio element which open the audio device & if NULL, means no active element */
  /* active[dev_id].lock mutex to avoid unnecessary overwrites for active[dev_id].audio */
  int i = 0;
  for (; i < MAX_NUM_DEV; i++) {
    klass->active[i].audio = NULL;
    klass->active[i].mute = DEFAULT_MUTE;
    g_mutex_init (&klass->active[i].lock);
  }
  klass->first_audio_pts = INVALID_PTS_VALUE;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  gobject_class->set_property = gst_staudio_set_property;
  gobject_class->get_property = gst_staudio_get_property;

  g_object_class_install_property (gobject_class, PROP_DISABLE_AVSYNC,
      g_param_spec_boolean ("dis-avsync", "disable av sync",
          "Whether to enable/disable av sync",
          DEFAULT_DISABLE_AVSYNC, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_DISABLE_INJECT,
      g_param_spec_boolean ("dis-inj", "disable audio inject",
          "Whether to enable/disable audio injection",
          DEFAULT_DISABLE_INJ, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_DEV_ID,
      g_param_spec_int ("dev-id", "device id for audio", "device id for audio",
          MIN_DEV_ID, MAX_DEV_ID, DEFAULT_DEV_ID,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  g_object_class_install_property (gobject_class, PROP_CHANNEL_CONFIG,
      g_param_spec_int ("ch-config", "channel config for audio",
          "channel config for audio", AUDIO_STEREO, AUDIO_STEREO_SWAPPED,
          AUDIO_STEREO,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  g_object_class_install_property (gobject_class, PROP_LATENCY,
      g_param_spec_uint ("latency", "latency time (milli seconds)",
          "latency for audio", MIN_LATENCY, MAX_LATENCY, DEFAULT_LATENCY,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  g_object_class_install_property (gobject_class, PROP_MUTE,
      g_param_spec_boolean ("mute", "mute audio",
          "Whether to mute/unmute audio",
          DEFAULT_MUTE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_APPLICATION_TYPE,
      g_param_spec_int ("application-type", "application type",
          "application type to be set in SE like  ISO, DVD, DVB, MS10, MS11, MS12",
          MIN_VALUE_APPLICATION_TYPE, MAX_VALUE_APPLICATION_TYPE,
          DEFAULT_APPLICATION_TYPE, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_SERVICE_TYPE,
      g_param_spec_int ("service-type", "service type",
          "service type to be set in SE like primary, sec, main, AD, main and AD, clean audio",
          MIN_VALUE_SERVICE_TYPE, MAX_VALUE_SERVICE_TYPE,
          DEFAULT_SERVICE_TYPE, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_CHANNEL_NUM,
      g_param_spec_uint ("channel-num", "channel num",
          "Number of channels to downmix",
          MIN_VALUE_CHANNEL_NUM, MAX_VALUE_CHANNEL_NUM,
          DEFAULT_CHANNEL_NUM, G_PARAM_READWRITE));

  gstelement_class->change_state = gst_staudio_change_state;

  gst_element_class_set_static_metadata (gstelement_class,
      "ST Audio Decoders",
      "Codec/Decoder/Audio/Parser",
      "GStreamer Audio Decoders Element for ST", "http://www.st.com");

  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&src_factory));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&sink_factory));

}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
static void
gst_staudio_init (Gststaudio * auddec)
{
  gst_staudio_reset (auddec);

  gst_segment_init (&auddec->segment, GST_FORMAT_TIME);

  auddec->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
  gst_pad_set_chain_function (auddec->sinkpad,
      GST_DEBUG_FUNCPTR (gst_staudio_chain));
  gst_pad_set_event_function (auddec->sinkpad,
      GST_DEBUG_FUNCPTR (gst_staudio_sink_pad_event));
  gst_pad_set_query_function (auddec->sinkpad,
      GST_DEBUG_FUNCPTR (gst_staudio_sink_pad_query));

  auddec->srcpad = gst_pad_new_from_static_template (&src_factory, "src");
  gst_pad_set_event_function (auddec->srcpad,
      GST_DEBUG_FUNCPTR (gst_staudio_src_pad_event));
  gst_pad_set_query_function (auddec->srcpad,
      GST_DEBUG_FUNCPTR (gst_staudio_src_pad_query));
  gst_pad_use_fixed_caps (auddec->srcpad);

  gst_element_add_pad (GST_ELEMENT (auddec), auddec->sinkpad);
  gst_element_add_pad (GST_ELEMENT (auddec), auddec->srcpad);

  auddec->dev_id = DEFAULT_DEV_ID;
  auddec->dev_id_prop = DEFAULT_DEV_ID;
  auddec->is_active = FALSE;
  auddec->device_config = TRUE;
}

static GstStaticCaps dra_caps = GST_STATIC_CAPS ("audio/x-dra");
#define DRA_CAPS (gst_static_caps_get (&dra_caps))

static void
gst_dra_typefind (GstTypeFind * tf, gpointer unused)
{
  const guint8 *data = gst_type_find_peek (tf, 0, 4);

  /* We are looking for DRA identifier in ES streams: the first 4 bytes should be 0x7FFF */
  if (data) {
    if ((guint16) ((data[0] << 8) | data[1]) == 0x7FFF) {
      gst_type_find_suggest (tf, GST_TYPE_FIND_MAXIMUM, DRA_CAPS);
    }
  }
}

/* entry point to initialize the element
 * register the element factories and other features
 */
gboolean
staudio_init (GstPlugin * plugin)
{
  static gchar *exts = "dra,es";

  /* debug category for auddec log messages */
  GST_DEBUG_CATEGORY_INIT (gst_staudio_debug, "staudio", 0, "ST audio decoder");

  if (!gst_element_register (plugin, "staudio", GST_RANK_PRIMARY + 10,
          GST_TYPE_STAUDIO)) {
    return FALSE;
  }

  /* Register typefinder for DRA codec */
  if (!gst_type_find_register (plugin, "gst_dra_typefind", GST_RANK_PRIMARY,
          gst_dra_typefind, exts, DRA_CAPS, NULL, NULL)) {
    g_print
        ("Gstreamer: Failed to register DRA typefinder in staudio plugin\n");
    return FALSE;
  }

  return TRUE;
}

static void
gst_staudio_reset (Gststaudio * auddec)
{
  GST_LOG_OBJECT (auddec, "%s:%d", __FUNCTION__, __LINE__);
  auddec->fd = -1;
  auddec->v4l2_fd = -1;
  auddec->prev_codec = -1;
  auddec->codec = AUDIO_ENCODING_NONE;
  auddec->state = AUDIO_STOPPED;
  auddec->is_pes = FALSE;
  auddec->first_frame_pushed = FALSE;
  auddec->first_injection = TRUE;
  auddec->disable_avsync = FALSE;
  auddec->properties.depth = 16;
  auddec->properties.sampling_freq = 48000;
  auddec->properties.num_chans = 2;
  auddec->properties.bitrate = -1;
  auddec->properties.endianness = G_LITTLE_ENDIAN;
  auddec->properties.dvd_mode = FALSE;
  auddec->codec_data = NULL;
  auddec->current_time = 0;
  auddec->prev_audio_pts = INVALID_PTS_VALUE;
  auddec->currentPTS = 0;
  auddec->rate = 1.0;
  auddec->priv_data = NULL;
  auddec->flush = FALSE;
  auddec->exit_loop = FALSE;
  auddec->flushing_seek = FALSE;

  auddec->tunneled = FALSE;
  auddec->queued_events_list = NULL;
  auddec->last_buffer_ts = GST_CLOCK_TIME_NONE;

  auddec->capture.buffer = NULL;
  auddec->capture.depth = DEFAULT_BITS_PER_SAMPLE;
  auddec->capture.sample_rate = DEFAULT_SAMPLE_RATE;
  auddec->capture.channels = DEFAULT_CHANNELS;
  auddec->capture.mem_type = GST_STAUDIO_INVALID;
  auddec->capture.buf_start_address = NULL;
  auddec->capture.is_stream_on_done = FALSE;
  auddec->capture.pool = NULL;
  auddec->channel_config = AUDIO_STEREO;
  auddec->latency = 0;
  auddec->srcresult = GST_FLOW_OK;
  auddec->discontinuity = FALSE;
  auddec->eos = FALSE;
  auddec->eos_set = FALSE;
  auddec->new_segment_received = FALSE;
  auddec->first_new_segment = TRUE;
  auddec->first_buffer_received = FALSE;
  auddec->capture.first_buffer = FALSE;
  auddec->audio_task_state = GST_STAUDIO_TASK_IDLE;
  auddec->mute = DEFAULT_MUTE;
  auddec->application_type = -1;
  auddec->service_type = -1;
  auddec->channel_num = DEFAULT_CHANNEL_NUM;
  auddec->set_avsync = TRUE;
  auddec->channel_change = FALSE;
  auddec->ptswrap = FALSE;
  auddec->update_segment = FALSE;
}

static gboolean
gst_staudio_open_device (Gststaudio * auddec)
{
  gchar *fn;
  Gststaudio *current_active = NULL;
  GststaudioClass *klass;
  klass = GST_STAUDIO_CLASS (G_OBJECT_GET_CLASS (auddec));

  GST_LOG_OBJECT (auddec, "%s:%d", __FUNCTION__, __LINE__);

  /* To ensure dev_id is valid */
  if (auddec->dev_id > MAX_DEV_ID) {
    GST_DEBUG_OBJECT (auddec, "wrong device id %d, should be <= %d",
        auddec->dev_id, MAX_DEV_ID);
    return FALSE;
  }

  if ((auddec->fd == -1) && (auddec->disable_dev == FALSE)) {
    fn = g_strdup_printf ("/dev/dvb/adapter0/audio%d", auddec->dev_id);

    /* acquire the lock to make sure active[dev_id].audio */
    /* is not overwritten by different staudio instance */
    g_mutex_lock (&klass->active[auddec->dev_id].lock);
    current_active = gst_staudio_get_active_element (auddec);
    g_mutex_unlock (&klass->active[auddec->dev_id].lock);

    /* open a new device only after all other audio devices are closed */
    /* here we assume only one audio device can be opened at a time */
    if ((current_active == NULL)) {
      /* take the lock before sending the open command, to avoid unncessary writes */
      /* acquire the lock to be sure the chain write has returned */
      g_mutex_lock (&auddec->write_lock);
      auddec->fd = open (fn, O_RDWR);

      if (auddec->fd >= 0) {
        gchar *str_element = gst_element_get_name (auddec);
        GST_DEBUG_OBJECT (auddec, "setting %s as active element\n",
            str_element);
        g_free (str_element);
        gst_staudio_set_active_element (auddec, auddec);
      }
      g_mutex_unlock (&auddec->write_lock);
    }

    if (auddec->fd < 0) {
      GST_ERROR_OBJECT (auddec, "failed to open audio device %s - %s %i", fn,
          strerror (errno), errno);
      g_free (fn);

      return FALSE;
    }
    GST_INFO_OBJECT (auddec, "device %s has been opened", fn);
    g_free (fn);
  }

  return TRUE;
}

static gboolean
gst_staudio_close_device (Gststaudio * auddec, gboolean stop)
{
  GST_LOG_OBJECT (auddec, "%s:%d", __FUNCTION__, __LINE__);

  /* Stop Audio device */
  if (auddec->state != AUDIO_STOPPED) {
    if (auddec->fd != -1) {
      gst_staudio_ioctl (auddec, AUDIO_STOP, NULL);
    }

    if (stop == TRUE) {
      auddec->state = AUDIO_STOPPED;
    }
  }

  /* Acquire the lock to be sure the chain write has returned */
  g_mutex_lock (&auddec->write_lock);

  /* Close Audio device */
  if (auddec->fd != -1) {
    gst_staudio_set_active_element (auddec, (Gststaudio *) NULL);
    close (auddec->fd);
    auddec->fd = -1;
  }

  g_mutex_unlock (&auddec->write_lock);

  return TRUE;
}

static gboolean
gst_staudio_term_device (Gststaudio * auddec)
{
  GST_LOG_OBJECT (auddec, "%s:%d", __FUNCTION__, __LINE__);

  gst_staudio_close_device (auddec, TRUE);

  if (auddec->codec_data) {
    gst_buffer_unref (auddec->codec_data);
    auddec->codec_data = NULL;
  }

  if (auddec->priv_data != NULL) {
    if (auddec->codec == AUDIO_ENCODING_VORBIS) {
      /* we need to keep 1st 3 pages for vorbis during playback in case of multiple audio track selection */
      /* and only free when device is terminated. see https://bugzilla.stlinux.com/show_bug.cgi?id=15234#c63 */
      gst_staudio_free_vorbis_packet (auddec);
    }
    g_free (auddec->priv_data);
    auddec->priv_data = NULL;
  }

  gst_staudio_drop_queued_events (auddec);
  gst_staudio_reset (auddec);

  /* We now consider device is disabled */
  auddec->disable_dev = TRUE;

  return TRUE;
}

static void
gst_staudio_setup_device (Gststaudio * auddec)
{
  struct audio_command audio_cmd;

  GST_LOG_OBJECT (auddec, "%s:%d", __FUNCTION__, __LINE__);

  if (auddec->fd < 0) {
    GST_ERROR_OBJECT (auddec, "audio device not ready to be setup");
    return;
  }

  gst_staudio_ioctl (auddec, AUDIO_SELECT_SOURCE, (void *) AUDIO_SOURCE_MEMORY);
  gst_staudio_ioctl (auddec, AUDIO_CHANNEL_SELECT,
      (void *) auddec->channel_config);
  GST_DEBUG_OBJECT (auddec, "channel_config = %d", auddec->channel_config);

  auddec->clock_data_flags = DVB_CLOCK_INITIALIZE;

  /* Set playback latency in case of live playback */
  if (auddec->live == TRUE) {
    gst_staudio_set_live_play_policies (auddec);
    /* We need to set a playback latency to compensate latency brought by gstreamer pipeline */
    /* The time for PES packets to come from source element to here has been measured to be max 150ms */
    /* So we apply this latency, unless it has been forced by user */
    if (auddec->latency < DEFAULT_LIVE_LATENCY) {
      auddec->latency = DEFAULT_LIVE_LATENCY;
      GST_INFO_OBJECT (auddec,
          "Forcing playback latency to %dms in case of live!!",
          DEFAULT_LIVE_LATENCY);
    }
  }

  if (auddec->latency != 0) {
    memset (&audio_cmd, 0, sizeof (struct audio_command));
    audio_cmd.cmd = AUDIO_CMD_SET_OPTION;
    audio_cmd.u.option.option = DVB_OPTION_CTRL_PLAYBACK_LATENCY;
    audio_cmd.u.option.value = auddec->latency;
    gst_staudio_ioctl (auddec, AUDIO_COMMAND, (void *) &audio_cmd);
    GST_DEBUG_OBJECT (auddec, "<DVB_OPTION_CTRL_PLAYBACK_LATENCY : %d>",
        auddec->latency);
  }

  /* avsync policy setup */
  if (auddec->tunneled == FALSE) {
    /* non tunneled pipeline - avsync disabled */
    gst_staudio_ioctl (auddec, AUDIO_SET_AV_SYNC, (void *) FALSE);
    GST_DEBUG_OBJECT (auddec, "AV_SYNC disable");
  } else {
    /* tunneled pipeline */
    if (auddec->disable_avsync == TRUE) {
      /* application sent action to disable avsync - tunneled pipeline */
      gst_staudio_ioctl (auddec, AUDIO_SET_AV_SYNC, (void *) FALSE);
      GST_DEBUG_OBJECT (auddec, "AV_SYNC disable");
    } else if (auddec->set_avsync) {
      /* by default avsync has to be enabled for tunneled pipeline */
      gst_staudio_ioctl (auddec, AUDIO_SET_AV_SYNC, (void *) TRUE);
      GST_DEBUG_OBJECT (auddec, "AV_SYNC enable");
    }
  }

  /* set application type if requested by user */
  if (auddec->application_type != -1) {
    if (gst_staudio_ioctl (auddec, AUDIO_SET_APPLICATION_TYPE,
            (void *) auddec->application_type)) {
      GST_INFO_OBJECT (auddec, "application type set to %d",
          auddec->application_type);
    }
  }

  /* set service type if requested by user */
  if (auddec->service_type != -1) {
    if (gst_staudio_ioctl (auddec, AUDIO_SET_SERVICE_TYPE,
            (void *) auddec->service_type)) {
      GST_INFO_OBJECT (auddec, "service type set to %d", auddec->service_type);
    }
  }

  if (auddec->channel_num != 0) {
    /* set number of channel/s to downmix on audio stream */
    if (!gst_staudio_ioctl (auddec, AUDIO_STREAM_DOWNMIX,
            (void *) auddec->channel_num)) {
      GST_WARNING_OBJECT (auddec, "failed to set channel num %d for downmix",
          auddec->channel_num);
    } else {
      GST_INFO_OBJECT (auddec, "downmix set to channel num %d",
          auddec->channel_num);
    }
  }

  if (auddec->mute) {
    /* set mute setting on staudio  */
    if (gst_staudio_ioctl (auddec, AUDIO_SET_MUTE, (void *) auddec->mute)) {
      GST_INFO_OBJECT (auddec, "Audio is %s ",
          (auddec->mute ? "mute" : "unmute"));
    } else {
      GST_WARNING_OBJECT (auddec, "Failed to %s audio ",
          (auddec->mute ? "mute" : "unmute"));
    }
  }

  /* stream switch only if codec type has changed */
  if (auddec->codec != auddec->prev_codec) {
    if (gst_staudio_ioctl (auddec, AUDIO_SET_ENCODING,
            (void *) auddec->codec) == TRUE) {
      /* store for use during caps change */
      auddec->prev_codec = auddec->codec;
      /* [MC] This needs to be done to have PES datatpath starting */
      gst_staudio_ioctl (auddec, AUDIO_PLAY, NULL);
      auddec->state = AUDIO_PLAY;
      if ((GST_STATE (auddec) == GST_STATE_PAUSED)
          && (auddec->tunneled == TRUE)) {
        /* Pause the decoder */
        if (gst_staudio_ioctl (auddec, AUDIO_PAUSE, NULL) == TRUE) {
          GST_DEBUG_OBJECT (auddec, "AUDIO_PAUSE");
          auddec->state = AUDIO_PAUSED;
        }
      }
    }
  }

  return;
}

static gboolean
gst_staudio_ioctl (Gststaudio * auddec, guint ioctl_code, void *parameter)
{
  gint ret = FALSE;

  if (auddec->disable_dev == TRUE) {
    return TRUE;
  }

  if (auddec->fd > 0) {
    if (ioctl_code != AUDIO_GET_EVENT && ioctl_code != AUDIO_GET_PLAY_INFO) {
      GST_DEBUG_OBJECT (auddec, "ioctl %s         : parameter %d",
          gst_staudio_ioctl_name (ioctl_code), (gint) parameter);
    } else {
      GST_LOG_OBJECT (auddec, "ioctl %s         : parameter %d",
          gst_staudio_ioctl_name (ioctl_code), (gint) parameter);
    }
    if (ioctl (auddec->fd, ioctl_code, parameter) == 0) {
      ret = TRUE;
    } else {
      if (ioctl_code != AUDIO_GET_EVENT)
        GST_ERROR_OBJECT (auddec, "%s failed - %s",
            gst_staudio_ioctl_name (ioctl_code), strerror (errno));
      else
        /* AUDIO_GET_EVENT always returning Error */
        GST_LOG_OBJECT (auddec, "%s failed - %s",
            gst_staudio_ioctl_name (ioctl_code), strerror (errno));
    }
  } else {
//    GST_ERROR_OBJECT (auddec, "device not open - %s",
//        gst_staudio_ioctl_name (ioctl_code));
    /* Warning ! for now we have to return true to play streams with more than one audio track */
    ret = TRUE;
  }

  return ret;
}

static gboolean
gst_staudio_write (Gststaudio * auddec, guint8 * data, guint data_length)
{
  gboolean ret = FALSE;

  if ((auddec->disable_dev == TRUE) || (auddec->exit_loop == TRUE)) {
    GST_WARNING_OBJECT (auddec, "write will not occur  - dev is %d in state %d",
        auddec->disable_dev, auddec->state);
    return TRUE;
  }

  if ((auddec->fd > 0) && (data != NULL) && (data_length != 0)) {
    GST_TRACE_OBJECT (auddec, "write(%d) from 0x%08x", data_length,
        (guint) data);
    /* still let it return true because staudio element still can consume data without audio decoded */
    /* the failure of write can happen when still writing device but AUDIO_IOCTL_STOP called */
    if (write (auddec->fd, data, data_length) == data_length) {
      ret = TRUE;
    } else {
      GST_WARNING_OBJECT (auddec,
          "write failed - %s %i when audio is in state %d", strerror (errno),
          errno, auddec->state);
      if ((errno == ENODEV) || (errno == EPERM)) {
        GST_ELEMENT_ERROR (auddec, RESOURCE, WRITE, ("Audio write failed %s %i",
                strerror (errno), errno), GST_ERROR_SYSTEM);
      }
    }
  }

  return ret;
}

static gchar *
gst_staudio_ioctl_name (gint ioctl_code)
{
  switch (ioctl_code) {
    case AUDIO_STOP:
      return "AUDIO_STOP";
    case AUDIO_PLAY:
      return "AUDIO_PLAY";
    case AUDIO_PAUSE:
      return "AUDIO_PAUSE";
    case AUDIO_CONTINUE:
      return "AUDIO_CONTINUE";
    case AUDIO_SELECT_SOURCE:
      return "AUDIO_SELECT_SOURCE";
    case AUDIO_SET_MUTE:
      return "AUDIO_SET_MUTE";
    case AUDIO_SET_AV_SYNC:
      return "AUDIO_SET_AV_SYNC";
    case AUDIO_SET_BYPASS_MODE:
      return "AUDIO_SET_BYPASS_MODE";
    case AUDIO_CHANNEL_SELECT:
      return "AUDIO_CHANNEL_SELECT";
    case AUDIO_GET_STATUS:
      return "AUDIO_GET_STATUS";
    case AUDIO_GET_CAPABILITIES:
      return "AUDIO_GET_CAPABILITIES";
    case AUDIO_CLEAR_BUFFER:
      return "AUDIO_CLEAR_BUFFER";
    case AUDIO_SET_ID:
      return "AUDIO_SET_ID";
    case AUDIO_SET_MIXER:
      return "AUDIO_SET_MIXER";
    case AUDIO_SET_STREAMTYPE:
      return "AUDIO_SET_STREAMTYPE";
    case AUDIO_SET_EXT_ID:
      return "AUDIO_SET_EXT_ID";
    case AUDIO_SET_ATTRIBUTES:
      return "AUDIO_SET_ATTRIBUTES";
    case AUDIO_SET_KARAOKE:
      return "AUDIO_SET_KARAOKE";
    case AUDIO_SET_ENCODING:
      return "AUDIO_SET_ENCODING";
    case AUDIO_FLUSH:
      return "AUDIO_FLUSH";
    case AUDIO_SET_SPDIF_SOURCE:
      return "AUDIO_SET_SPDIF_SOURCE";
    case AUDIO_SET_SPEED:
      return "AUDIO_SET_SPEED";
    case AUDIO_DISCONTINUITY:
      return "AUDIO_DISCONTINUITY";
    case AUDIO_SET_PLAY_INTERVAL:
      return "AUDIO_SET_PLAY_INTERVAL";
    case AUDIO_SET_SYNC_GROUP:
      return "AUDIO_SET_SYNC_GROUP";
    case AUDIO_GET_PLAY_TIME:
      return "AUDIO_GET_PLAY_TIME";
    case AUDIO_GET_PLAY_INFO:
      return "AUDIO_GET_PLAY_INFO";
    case AUDIO_SET_CLOCK_DATA_POINT:
      return "AUDIO_SET_CLOCK_DATA_POINT";
    case AUDIO_COMMAND:
      return "AUDIO_COMMAND";
    case AUDIO_GET_EVENT:
      return "AUDIO_GET_EVENT";
    default:
      return "Invalid ioctl";
  }
}

static gboolean
gst_staudio_queue_packet (Gststaudio * auddec, GstBuffer * buf)
{
  GST_DEBUG_OBJECT (auddec, "gst_staudio_queue_packet() - IN");
  /* Not sure how to handle buffer without timestamp, now drop it as simple
     Maybe later we can put in one list to save and check timestamp later when buffer
     with valid time stamp comes
   */
  if (GST_BUFFER_TIMESTAMP_IS_VALID (buf) == FALSE) {
    GST_DEBUG_OBJECT (auddec, "buffer without timestamp, drop the packet");
    goto done;
  }

  if (gst_buffer_get_size (buf) == 0) {
    /* buffer without data, nothing to queue */
    goto done;
  }

  guint count_invalid_pts = 0;  //Used to check the number of times invalid pts is received
  GststaudioClass *klass;
  klass = GST_STAUDIO_CLASS (G_OBJECT_GET_CLASS (auddec));

  /* the sync between active and in-active staudio element as flow control */
  while (auddec->exit_loop == FALSE) {
    /* acquire the lock to make sure active[dev_id].lock */
    /* is not overwritten by different staudio instance */
    g_mutex_lock (&klass->active[auddec->dev_id].lock);
    Gststaudio *aud = gst_staudio_get_active_element (auddec);
    g_mutex_unlock (&klass->active[auddec->dev_id].lock);

    guint64 pts;
    GstClockTime t1, t2, timediff;

    /* if audio is disabled, no need to queue just drop it. */
    if (auddec->disable_dev == TRUE) {
      GST_LOG_OBJECT (auddec, "audio is disabled, drop the packet");
      break;
    }

    if (auddec->flush) {
      GST_LOG_OBJECT (auddec, "flushing, drop the packet");
      break;
    }

    /* The reason why in trick mode we drop the packet is that the SE will not
       able to give valid PTS always unless switch back to 1.0x again. So here
       not able to compare the PTS with coming buffer time stamp and have to drop
       to avoid block chain too many time. Thanks after switch back to 1.0x, there
       will be one SEEK happen in application side and new sync data will come.
     */
    if ((auddec->rate != 1.0) || (aud != NULL && aud->rate != 1.0)) {
      GST_LOG_OBJECT (auddec, "in trick mode, so drop the packet");
      break;
    }

    GST_OBJECT_LOCK (auddec);
    if (aud == NULL && auddec->is_active == FALSE) {    /* no active staudio element now */
      GST_OBJECT_UNLOCK (auddec);
      usleep (10 * 1000);
      continue;
    }


    if (auddec == aud || auddec->is_active == TRUE) {   /* switch from in-active to active */
      GST_OBJECT_UNLOCK (auddec);
      GST_INFO_OBJECT (auddec, "switching from inactive to active state");
      break;
    }
    GST_OBJECT_UNLOCK (auddec);

    /* other active staudio element available */
    pts = gst_staudio_get_pts (aud);
    if (IS_PTS_VALUE_VALID (pts) == FALSE) {
      /* invalid PTS and can not compare */
      usleep (10 * 1000);
      if (auddec->state == AUDIO_PLAYING) {
        count_invalid_pts++;
      }
      if (count_invalid_pts >= 10) {
        count_invalid_pts = 0;
        GST_WARNING_OBJECT (aud, "Invalid PTS");
        break;
      }
      continue;
    }

    /* we have valid pts now */
    t1 = GST_BUFFER_TIMESTAMP (buf);
    t2 = MPEGTIME_TO_GSTTIME (pts);
    if (GST_CLOCK_DIFF (t1, t2) >= 0) {
      /* buffer is later then drop it as fake decoder */
      GST_LOG_OBJECT (auddec, "buffer is later, drop as fake decoder");
      break;
    } else {
      timediff = (GSTTIME_TO_MPEGTIME (t1) - GSTTIME_TO_MPEGTIME (t2)) / 90;
      if (timediff > HUGE_TIME_DIFF_MS) {
        GST_ERROR_OBJECT (auddec,
            "huge time difference detected=%llu, drop the packet", timediff);
        break;
      }
      /* buffer is advance and not in huge range, wait for next check */
      GST_LOG_OBJECT (auddec, "buffer is advance, wait for next check");
      usleep (10 * 1000);
      continue;
    }
  }

done:
  gst_buffer_unref (buf);

  GST_DEBUG_OBJECT (auddec, "gst_staudio_queue_packet() - OUT");

  return TRUE;
}

static gboolean
gst_staudio_write_packet (Gststaudio * auddec, GstBuffer * buf)
{
  guint8 *data;
  guint8 pes_header[PES_MAX_HEADER_SIZE];
  guint data_length;
  guint64 pts = 0ull;
  GstMapInfo info_read;
  gboolean ret = TRUE;

  g_mutex_lock (&auddec->write_lock);

  gst_buffer_map (buf, &info_read, GST_MAP_READ);
  if (auddec->is_pes == FALSE) {
    /* Reset variables */
    memset (pes_header, 0, PES_MAX_HEADER_SIZE);
    data = info_read.data;
    data_length = info_read.size;

    if (GST_BUFFER_TIMESTAMP_IS_VALID (buf)) {
      pts = gst_util_uint64_scale (GST_BUFFER_TIMESTAMP (buf), 9, 100000);
      /* modulo pts with max value (33 bits). it will take care
         of wrap around also.
       */
      pts %= (MAX_PTS_VALUE);
    } else {
      if (auddec->first_injection == FALSE) {
        pts = INVALID_PTS_VALUE;
      }
    }

    /* Create PES packet as per codec received and write packet to SE */
    switch (auddec->codec) {
      case AUDIO_ENCODING_AAC:
        ret =
            gst_staudio_write_aac_packet (auddec, pes_header,
            PES_MAX_HEADER_SIZE, data, data_length, pts);
        break;
      case AUDIO_ENCODING_MP3:
        gst_buffer_ref (buf);
        ret =
            gst_staudio_write_mp3_packet (auddec, pes_header,
            PES_MAX_HEADER_SIZE, (guint8 *) buf, data_length, pts);
        break;
      case AUDIO_ENCODING_PCM:
        ret =
            gst_staudio_write_pcm_packet (auddec, pes_header,
            PES_MAX_HEADER_SIZE, data, data_length, pts);
        break;
      case AUDIO_ENCODING_WMA:
        ret =
            gst_staudio_write_wma_packet (auddec, pes_header,
            PES_MAX_HEADER_SIZE, data, data_length, pts);
        break;
      case AUDIO_ENCODING_MS_ADPCM:
      case AUDIO_ENCODING_IMA_ADPCM:
        ret =
            gst_staudio_write_adpcm_packet (auddec, pes_header,
            PES_MAX_HEADER_SIZE, data, data_length, pts);
        break;
      case AUDIO_ENCODING_VORBIS:
        ret =
            gst_staudio_write_vorbis_packet (auddec, pes_header,
            PES_MAX_HEADER_SIZE, data, data_length, pts);
        break;
      case AUDIO_ENCODING_AC3:
        if (auddec->properties.dvd_mode == TRUE) {
          if (data_length > 2) {
            ret =
                gst_staudio_write_ac3_packet (auddec, pes_header,
                PES_MAX_HEADER_SIZE, data, data_length, pts);
          }
          break;
        }
      default:
        ret =
            gst_staudio_write_audio_packet (auddec, pes_header,
            PES_MAX_HEADER_SIZE, data, data_length, pts);
        break;
    }
  } else {
    if (gst_staudio_write (auddec, info_read.data, info_read.size) == FALSE) {
      ret = FALSE;
      goto error;;
    }
    /* Check for reverse discontinuity from upstream */
    if ((auddec->rate < 0) && (GST_BUFFER_IS_DISCONT (buf))) {
      audio_discontinuity_t audio_discontinuity;

      if (GST_BUFFER_FLAG_IS_SET (buf, GST_BUFFER_FLAG_DELTA_UNIT)) {
        audio_discontinuity = AUDIO_DISCONTINUITY_SURPLUS_DATA;
      }
      ret =
          gst_staudio_ioctl (auddec, AUDIO_DISCONTINUITY,
          (void *) audio_discontinuity);
    }
  }

error:
  g_mutex_unlock (&auddec->write_lock);
  gst_buffer_unmap (buf, &info_read);
  return ret;
}

static gboolean
gst_staudio_write_aac_packet (Gststaudio * auddec, guint8 * pes_header,
    guint pes_header_length, guint8 * pes_data, guint pes_data_length,
    guint64 pts)
{
  guint8 aac_header[GST_STAUDIO_AAC_HEADER_LENGTH] =
      { 0xff, 0xf1, 0x00, 0x00, 0x00, 0x1f, 0xfc };
  gint profile = 1;
  gint sample_index = 0x0F;
  gint channel_count = 2;
  guint aac_data_length = pes_data_length;
  guint8 *aac_data;
  guint header_length, extra_header_length = 0;
  GstMapInfo info_read;

  if (auddec->codec_data) {
    gst_buffer_map (auddec->codec_data, &info_read, GST_MAP_READ);
    channel_count = (info_read.data[1] & 0x7f) >> 3;
    if (channel_count == 7) {
      channel_count = 8;
    }

    sample_index =
        ((info_read.data[0] & 0x7) << 1) | ((info_read.data[1] & 0x80) >> 7);

    extra_header_length = GST_STAUDIO_AAC_HEADER_LENGTH;
    aac_data_length += extra_header_length;
    aac_header[2] =
        ((profile & 0x03) << 6) | (sample_index << 2) | ((channel_count >> 2) &
        0x01);
    aac_header[3] = (channel_count & 0x03) << 6;
    aac_header[3] |= (aac_data_length >> 11) & 0x3;
    aac_header[4] = (aac_data_length >> 3) & 0xff;
    aac_header[5] |= (aac_data_length << 5) & 0xe0;
    gst_buffer_unmap (auddec->codec_data, &info_read);
  }

  header_length =
      InsertPesHeader (pes_header, aac_data_length, MPEG_AUDIO_PES_START_CODE,
      pts, 0);
  aac_data = (guint8 *) g_malloc0 (aac_data_length * sizeof (guint8));
  if (aac_data != NULL) {
    memcpy (aac_data, aac_header, extra_header_length);
    memcpy (aac_data + extra_header_length, pes_data, pes_data_length);
    if (gst_staudio_write (auddec, pes_header, header_length) == FALSE) {
      g_free (aac_data);
      return FALSE;
    }
    if (gst_staudio_write (auddec, aac_data, aac_data_length) == FALSE) {
      g_free (aac_data);
      return FALSE;
    }
    g_free (aac_data);
  }

  if (auddec->first_injection == TRUE) {
    auddec->first_injection = FALSE;
  }

  return TRUE;
}

static gboolean
gst_staudio_write_ac3_packet (Gststaudio * auddec, guint8 * pes_header,
    guint pes_header_length, guint8 * pes_data, guint pes_data_length,
    guint64 pts)
{
  guint header_length;

  header_length =
      InsertPesHeader (pes_header, pes_data_length - 2,
      PRIVATE_STREAM_1_PES_START_CODE, pts, 0);
  if (gst_staudio_write (auddec, pes_header, header_length) == FALSE) {
    return FALSE;
  }
  if (gst_staudio_write (auddec, (pes_data + 2), pes_data_length - 2) == FALSE) {
    return FALSE;
  }

  if (auddec->first_injection == TRUE) {
    auddec->first_injection = FALSE;
  }

  return TRUE;
}

static gboolean
gst_staudio_write_mp3_packet (Gststaudio * auddec, guint8 * pes_header,
    guint pes_header_length, guint8 * pes_data, guint pes_data_length,
    guint64 pts)
{
  guint8 *data;
  guint header_length;
  guint data_length;

  gst_adapter_push (auddec->adapter, (GstBuffer *) pes_data);
  while (gst_adapter_available (auddec->adapter) != 0) {
    if (gst_adapter_available (auddec->adapter) >=
        GST_STAUDIO_MP3_INJECTION_SIZE) {
      data_length = GST_STAUDIO_MP3_INJECTION_SIZE;
    } else {
      data_length = gst_adapter_available (auddec->adapter);
    }
    data = (guint8 *) gst_adapter_map (auddec->adapter, data_length);

    memset (pes_header, 0x00, PES_MAX_HEADER_SIZE);
    header_length =
        InsertPesHeader (pes_header, data_length, MPEG_AUDIO_PES_START_CODE,
        pts, 0);
    if (gst_staudio_write (auddec, pes_header, header_length) == FALSE) {
      gst_adapter_flush (auddec->adapter, header_length);
      return FALSE;
    }
    if (gst_staudio_write (auddec, data, data_length) == FALSE) {
      gst_adapter_flush (auddec->adapter, data_length);
      return FALSE;
    }

    /* Valid pts has been set once */
    pts = INVALID_PTS_VALUE;
    gst_adapter_flush (auddec->adapter, data_length);
  }

  if (auddec->first_injection == TRUE) {
    auddec->first_injection = FALSE;
  }

  return TRUE;
}

static gboolean
gst_staudio_write_pcm_packet (Gststaudio * auddec, guint8 * pes_header,
    guint pes_header_length, guint8 * pes_data, guint pes_data_length,
    guint64 pts)
{
  guint8 *buf;
  guint8 *pes_ptr = &pes_header[PES_MIN_HEADER_SIZE];
  char wav_format_header[] = { 0x66, 0x6d, 0x74, 0x20,  /* "fmt "    */
    0x16, 0x00, 0x00, 0x00
  };                            /* data size */
  guint header_length;
  wav_format_chunk wav_format;

  if (auddec->first_injection == TRUE) {
    if (auddec->properties.dvd_mode == TRUE) {
      guint8 *header_data;
      guint header_size;
      guint32 header;

      header_size = pes_data_length;
      header_data = pes_data;

      if (header_size < 5) {
        return FALSE;
      }

      header =
          ((header_data[2] & 0xC0) << 16) | (header_data[3] << 8) |
          header_data[4];

      /* These two bits tell us the bit depth */
      switch (header & 0xC000) {
        case 0x8000:
          auddec->properties.depth = 24;
          break;
        case 0x4000:
          auddec->properties.depth = 20;
          break;
        default:
          auddec->properties.depth = 16;
          break;
      }

      switch (header & 0x3000) {
        case 0x0000:
          auddec->properties.sampling_freq = 48000;
          break;
        case 0x1000:
          auddec->properties.sampling_freq = 96000;
          break;
        case 0x2000:
          auddec->properties.sampling_freq = 44100;
          break;
        case 0x3000:
          auddec->properties.sampling_freq = 32000;
          break;
        default:
          break;
      }

      /* And, of course, the number of channels (up to 8) */
      auddec->properties.num_chans = ((header >> 8) & 0x7) + 1;
    }

    if (auddec->properties.endianness == G_BIG_ENDIAN)
      strncpy (wav_format_header, " tmf", 4);

    memcpy (pes_ptr, &wav_format_header, sizeof (wav_format_header));
    wav_format.format_tag = 1;
    wav_format.channels = auddec->properties.num_chans;
    wav_format.samples_per_sec = auddec->properties.sampling_freq;
    wav_format.block_align =
        auddec->properties.depth / 8 * auddec->properties.num_chans;
    wav_format.bits_per_sample = auddec->properties.depth;
    wav_format.avg_bytes_per_sec =
        auddec->properties.sampling_freq * wav_format.block_align;
    GST_INFO_OBJECT (auddec, "format tag %d", wav_format.format_tag);
    GST_INFO_OBJECT (auddec, "channels %d", wav_format.channels);
    GST_INFO_OBJECT (auddec, "samples per second %lu",
        wav_format.samples_per_sec);
    GST_INFO_OBJECT (auddec, "average bytes per second %lu",
        wav_format.avg_bytes_per_sec);
    GST_INFO_OBJECT (auddec, "block align %d", wav_format.block_align);
    GST_INFO_OBJECT (auddec, "bits per sample %d", wav_format.bits_per_sample);
    memcpy (pes_ptr + sizeof (wav_format_header), (guchar *) & wav_format,
        sizeof (wav_format));
    header_length =
        InsertPesHeader (pes_header,
        sizeof (wav_format_header) + sizeof (wav_format),
        PRIVATE_STREAM_1_PES_START_CODE, INVALID_PTS_VALUE, 0);
    if (!gst_staudio_write (auddec, pes_header,
            header_length + sizeof (wav_format_header) + sizeof (wav_format))) {
      GST_ERROR_OBJECT (auddec, "failed to write pes header");
      return FALSE;
    }
  }

  if (auddec->properties.dvd_mode == TRUE) {
    pes_data = pes_data + 5;
    pes_data_length = pes_data_length - 5;
  }

  header_length =
      InsertPesHeader (pes_header, pes_data_length,
      PRIVATE_STREAM_1_PES_START_CODE, pts, 0);
  /* Combine two strings into one, bugzilla 13000 */
  buf = (guint8 *) g_malloc0 (pes_header_length + pes_data_length);
  if (buf != NULL) {
    memcpy (buf, pes_header, header_length);
    memcpy (buf + header_length, pes_data, pes_data_length);
    if (gst_staudio_write (auddec, buf,
            header_length + pes_data_length) == FALSE) {
      g_free (buf);
      return FALSE;
    }
    g_free (buf);
  }

  if (auddec->first_injection == TRUE) {
    auddec->first_injection = FALSE;
  }

  return TRUE;
}

static gboolean
gst_staudio_write_wma_packet (Gststaudio * auddec, guint8 * pes_header,
    guint pes_header_length, guint8 * pes_data, guint pes_data_length,
    guint64 pts)
{
  guint header_length;
  GstMapInfo info_read;
  gboolean ret = FALSE;

  if (auddec->codec_data != NULL) {
    gst_buffer_map (auddec->codec_data, &info_read, GST_MAP_READ);
  }

  if ((auddec->first_injection == TRUE) && (auddec->codec_data != NULL)) {
    header_length =
        InsertPesHeader (pes_header, info_read.size,
        MPEG_AUDIO_PES_START_CODE, INVALID_PTS_VALUE, 0);
    if (gst_staudio_write (auddec, pes_header, header_length) == FALSE) {
      goto exit;
    }
    if (gst_staudio_write (auddec, info_read.data, info_read.size) == FALSE) {
      goto exit;
    }
  }
  header_length =
      InsertPesHeader (pes_header, pes_data_length, MPEG_AUDIO_PES_START_CODE,
      pts, 0);
  if (gst_staudio_write (auddec, pes_header, header_length) == FALSE) {
    goto exit;
  }
  if (gst_staudio_write (auddec, pes_data, pes_data_length) == FALSE) {
    goto exit;
  }

  if (auddec->first_injection == TRUE) {
    auddec->first_injection = FALSE;
  }

  ret = TRUE;

exit:
  if (auddec->codec_data != NULL) {
    gst_buffer_unmap (auddec->codec_data, &info_read);
  }
  return ret;
}

gboolean
gst_staudio_write_adpcm_packet (Gststaudio * auddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts)
{
  guint header_length;
  adpcmprivatedata_t privatedata;
  GstMapInfo info_read;
  gboolean ret = FALSE;

  gst_buffer_map (auddec->codec_data, &info_read, GST_MAP_READ);

  memset (&privatedata, 0, sizeof (privatedata));
  privatedata.streamid = 0xA5;
  privatedata.frequency = auddec->properties.sampling_freq;
  privatedata.nb_of_channels = auddec->properties.num_chans;

  if (auddec->codec == AUDIO_ENCODING_MS_ADPCM) {
    if (info_read.size >= 4) {
      privatedata.nb_of_samples_per_block =
          (info_read.data[1] << 8) | (info_read.data[0]);
      privatedata.nb_of_coefficients =
          (info_read.data[3] << 8) | (info_read.data[2]);
    }
  }

  if (auddec->codec == AUDIO_ENCODING_IMA_ADPCM) {
    if (info_read.size >= 2) {
      privatedata.nb_of_samples_per_block =
          (info_read.data[1] << 8) | (info_read.data[0]);
      privatedata.nb_of_coefficients = 2;
    }
  }
  GST_DEBUG_OBJECT (auddec, "number of samples per block %d",
      privatedata.nb_of_samples_per_block);
  GST_DEBUG_OBJECT (auddec, "number of coefficients      %d",
      privatedata.nb_of_coefficients);

  /* write pes header and private adpcm data and payload data */
  header_length =
      InsertPesHeader (pes_header,
      sizeof (adpcmprivatedata_t) + pes_data_length,
      PRIVATE_STREAM_1_PES_START_CODE, pts, 0);
  if (gst_staudio_write (auddec, pes_header, header_length) == FALSE) {
    goto exit;
  }
  if (gst_staudio_write (auddec, (guint8 *) & privatedata,
          sizeof (adpcmprivatedata_t)) == FALSE) {
    goto exit;
  }
  if (gst_staudio_write (auddec, pes_data, pes_data_length) == FALSE) {
    goto exit;
  }
  ret = TRUE;

exit:
  gst_buffer_unmap (auddec->codec_data, &info_read);
  return ret;
}

static gboolean
gst_staudio_write_vorbis_packet (Gststaudio * auddec,
    guint8 * pes_header, guint pes_header_length, guint8 * pes_data,
    guint pes_data_length, guint64 pts)
{
  guint8 start_code = PRIVATE_STREAM_1_PES_START_CODE;
  guint header_length;
  guint pes_length;
  guint private_header_length;

  if (auddec->first_injection == TRUE) {
    /* push 3 saved pages to stream engine. See bugzilla 21834 */
    if (auddec->priv_data != NULL) {
      vorbisprivatedata_t *p = (vorbisprivatedata_t *) auddec->priv_data;

      if ((p->page1 != NULL) && (p->page2 != NULL) && (p->page3 != NULL)) {
        if (gst_staudio_write (auddec, p->page1, p->len1) == FALSE) {
          return FALSE;
        }
        if (gst_staudio_write (auddec, p->page2, p->len2) == FALSE) {
          return FALSE;
        }
        if (gst_staudio_write (auddec, p->page3, p->len3) == FALSE) {
          return FALSE;
        }
      }
    }
    auddec->first_injection = FALSE;
  }

  header_length =
      InsertPesHeader (pes_header, pes_data_length, start_code, pts, 0);

  private_header_length =
      InsertAudioPrivateDataHeader (&pes_header[header_length],
      pes_data_length, auddec->codec);

  /* Update pes length */
  pes_length =
      pes_header[PES_LENGTH_BYTE_0] + (pes_header[PES_LENGTH_BYTE_1] << 8) +
      private_header_length;
  pes_header[PES_LENGTH_BYTE_0] = pes_length & 0xff;
  pes_header[PES_LENGTH_BYTE_1] = (pes_length >> 8) & 0xff;
  pes_header[PES_HEADER_DATA_LENGTH_BYTE] += private_header_length;
  pes_header[PES_FLAGS_BYTE] |= PES_EXTENSION_DATA_PRESENT;
  header_length += private_header_length;

  if (gst_staudio_write (auddec, pes_header, header_length) == FALSE) {
    return FALSE;
  }
  if (gst_staudio_write (auddec, pes_data, pes_data_length) == FALSE) {
    return FALSE;
  }

  /* save 3 pages data to match stream engine requirement. See bugzilla 21834 */
  if (auddec->priv_data != NULL) {
    vorbisprivatedata_t *p = (vorbisprivatedata_t *) auddec->priv_data;

    if (p->page1 == NULL) {
      p->page1 = g_malloc0 (header_length + pes_data_length);
      memcpy (p->page1, pes_header, header_length);
      memcpy ((p->page1 + header_length), pes_data, pes_data_length);
      p->len1 = header_length + pes_data_length;
    } else if (p->page2 == NULL) {
      p->page2 = g_malloc0 (header_length + pes_data_length);
      memcpy (p->page2, pes_header, header_length);
      memcpy ((p->page2 + header_length), pes_data, pes_data_length);
      p->len2 = header_length + pes_data_length;
    } else if (p->page3 == NULL) {
      p->page3 = g_malloc0 (header_length + pes_data_length);
      memcpy (p->page3, pes_header, header_length);
      memcpy ((p->page3 + header_length), pes_data, pes_data_length);
      p->len3 = header_length + pes_data_length;
    }
  }

  return TRUE;
}

static gboolean
gst_staudio_write_audio_packet (Gststaudio * auddec, guint8 * pes_header,
    guint pes_header_length, guint8 * pes_data, guint pes_data_length,
    guint64 pts)
{
  guint8 start_code;
  guint header_length;
  gint fake_sc = 0;

  /* Set start code */
  switch (auddec->codec) {
    case AUDIO_ENCODING_MPEG1:
    case AUDIO_ENCODING_MPEG2:
    case AUDIO_ENCODING_WMA:
      start_code = MPEG_AUDIO_PES_START_CODE;
      break;
    case AUDIO_ENCODING_DTS:
      start_code = PRIVATE_STREAM_1_PES_START_CODE;
      if (auddec->properties.dvd_mode == TRUE) {
        fake_sc = 1;
      }
      break;
    case AUDIO_ENCODING_AC3:
    default:
      start_code = PRIVATE_STREAM_1_PES_START_CODE;
      break;
  }

  header_length =
      InsertPesHeader (pes_header, pes_data_length, start_code, pts, fake_sc);

  if (gst_staudio_write (auddec, pes_header, header_length) == FALSE) {
    return FALSE;
  }
  if (gst_staudio_write (auddec, pes_data, pes_data_length) == FALSE) {
    return FALSE;
  }

  if (auddec->first_injection == TRUE) {
    auddec->first_injection = FALSE;
  }

  return TRUE;
}

static void
gst_staudio_update_time (Gststaudio * auddec)
{
  /* Get audio playback time */
  if (auddec->state != AUDIO_STOPPED) {
    audio_play_info_t aud_play_info;
    memset (&aud_play_info, 0, sizeof (aud_play_info));
    aud_play_info.pts = INVALID_PTS_VALUE;
    if (gst_staudio_ioctl (auddec, AUDIO_GET_PLAY_INFO,
            (void *) &aud_play_info) == TRUE) {
      if (IS_PTS_VALUE_VALID (aud_play_info.pts)) {
        gint64 current_pts = 0;
        auddec->currentPTS = aud_play_info.pts;
        current_pts = MPEGTIME_TO_GSTTIME ((gint64) auddec->currentPTS);

        /* TS streams may have pts wrap around */
        if ((current_pts < auddec->prev_audio_pts)
            && (auddec->update_segment == TRUE)) {
          auddec->segment.start = current_pts;
          auddec->segment.time = auddec->current_time;
          auddec->update_segment = FALSE;
        }
        auddec->current_time =
            auddec->segment.time + current_pts - auddec->segment.start;
        auddec->prev_audio_pts = current_pts;
      }
    }
  }
}

static gboolean
gst_staudio_push_dummy_packet (Gststaudio * auddec)
{
  GstBuffer *buffer;
  GstCaps *srccaps = NULL;

  if (auddec->first_frame_pushed == FALSE) {
    buffer = gst_buffer_new ();
    if (buffer != NULL) {
      GST_BUFFER_TIMESTAMP (buffer) = GST_CLOCK_TIME_NONE;
      GST_BUFFER_DURATION (buffer) = GST_CLOCK_TIME_NONE;

      srccaps = gst_pad_get_current_caps (auddec->srcpad);
      if (srccaps == NULL) {
        srccaps =
            gst_caps_new_simple ("audio/x-fake-int-stm", "dev-id",
            G_TYPE_INT, auddec->dev_id_prop, "rate", G_TYPE_INT,
            auddec->properties.sampling_freq, "channels", G_TYPE_INT,
            auddec->properties.num_chans, NULL);
      }

      GST_DEBUG_OBJECT (auddec, "setting srccaps to %" GST_PTR_FORMAT, srccaps);

      gst_pad_push_event (auddec->srcpad, gst_event_new_caps (srccaps));
      auddec->srcresult = gst_pad_push (auddec->srcpad, buffer);
      if (auddec->srcresult == GST_FLOW_OK) {
        auddec->first_frame_pushed = TRUE;
      } else {
        GST_ERROR_OBJECT (auddec, "failed to push dummy buffer downstream %s",
            gst_flow_get_name (auddec->srcresult));
      }
      gst_caps_unref (srccaps);
    }
  }

  return TRUE;
}

static guint64
gst_staudio_get_pts (Gststaudio * auddec)
{
  audio_play_info_t audio_play_info;
  guint64 pts = INVALID_PTS_VALUE;

  if (auddec == NULL) {
    goto exit;
  }

  if (auddec->fd < 0) {
    goto exit;
  }

  memset (&audio_play_info, 0, sizeof (audio_play_info_t));

  if (gst_staudio_ioctl (auddec, AUDIO_GET_PLAY_INFO,
          (void *) &audio_play_info) == TRUE) {
    if (IS_PTS_VALUE_VALID (audio_play_info.pts)) {
      pts = audio_play_info.pts;
    }
  }

exit:
  return pts;
}

static void
gst_staudio_save_vorbis_packet (Gststaudio * auddec, GstBuffer * buf)
{
  guint8 pes_header[PES_MAX_HEADER_SIZE];
  guint8 *pes_data;
  guint pes_data_length;
  guint64 pts;

  guint8 start_code = PRIVATE_STREAM_1_PES_START_CODE;
  guint header_length;
  guint pes_length;
  guint private_header_length;
  GstMapInfo info_read;

  vorbisprivatedata_t *p = (vorbisprivatedata_t *) auddec->priv_data;

  if ((p == NULL) || (p->saved == TRUE)) {
    return;
  }

  gst_buffer_map (buf, &info_read, GST_MAP_READ);
  pes_data = info_read.data;
  pes_data_length = info_read.size;

  if (GST_BUFFER_TIMESTAMP_IS_VALID (buf)) {
    pts = GSTTIME_TO_MPEGTIME (GST_BUFFER_TIMESTAMP (buf));
  } else {
    pts = INVALID_PTS_VALUE;
  }

  header_length =
      InsertPesHeader (pes_header, pes_data_length, start_code, pts, 0);
  private_header_length =
      InsertAudioPrivateDataHeader (&pes_header[header_length],
      pes_data_length, auddec->codec);

  /* Update pes length */
  pes_length =
      pes_header[PES_LENGTH_BYTE_0] + (pes_header[PES_LENGTH_BYTE_1] << 8) +
      private_header_length;
  pes_header[PES_LENGTH_BYTE_0] = pes_length & 0xff;
  pes_header[PES_LENGTH_BYTE_1] = (pes_length >> 8) & 0xff;
  pes_header[PES_HEADER_DATA_LENGTH_BYTE] += private_header_length;
  pes_header[PES_FLAGS_BYTE] |= PES_EXTENSION_DATA_PRESENT;
  header_length += private_header_length;

  /* save 3 pages data to match stream engine requirement. See bugzilla 21834 */
  if (p->page1 == NULL) {
    p->page1 = g_malloc0 (header_length + pes_data_length);
    memcpy (p->page1, pes_header, header_length);
    memcpy ((p->page1 + header_length), pes_data, pes_data_length);
    p->len1 = header_length + pes_data_length;
  } else if (p->page2 == NULL) {
    p->page2 = g_malloc0 (header_length + pes_data_length);
    memcpy (p->page2, pes_header, header_length);
    memcpy ((p->page2 + header_length), pes_data, pes_data_length);
    p->len2 = header_length + pes_data_length;
  } else if (p->page3 == NULL) {
    p->page3 = g_malloc0 (header_length + pes_data_length);
    memcpy (p->page3, pes_header, header_length);
    memcpy ((p->page3 + header_length), pes_data, pes_data_length);
    p->len3 = header_length + pes_data_length;
    p->saved = TRUE;
  }
  gst_buffer_unmap (buf, &info_read);
  return;
}

static void
gst_staudio_free_vorbis_packet (Gststaudio * auddec)
{
  vorbisprivatedata_t *p = (vorbisprivatedata_t *) auddec->priv_data;

  if (p->page1 != NULL) {
    g_free (p->page1);
    p->page1 = NULL;
  }
  if (p->page2 != NULL) {
    g_free (p->page2);
    p->page2 = NULL;
  }
  if (p->page3 != NULL) {
    g_free (p->page3);
    p->page3 = NULL;
  }
  p->saved = FALSE;

  return;
}

static void
gst_staudio_check_eos_event (Gststaudio * auddec)
{
  struct audio_event aud_event;
  struct pollfd poll_fd[1];

  poll_fd[0].fd = auddec->fd;
  poll_fd[0].events = POLLPRI;
  if (poll (poll_fd, 1, 1) == -1) {
    GST_ERROR_OBJECT (auddec, "poll error");
  }

  if (poll_fd[0].revents & POLLPRI) {
    if (gst_staudio_ioctl (auddec, AUDIO_GET_EVENT,
            (void *) &aud_event) == TRUE) {
      switch (aud_event.type) {
        case AUDIO_EVENT_END_OF_STREAM:
          GST_DEBUG_OBJECT (auddec, "AUDIO_EVENT_END_OF_STREAM");
          if ((auddec->eos_set == TRUE) && (auddec->tunneled)) {
            GST_INFO_OBJECT (auddec, "End of Stream event pushed");
            gst_pad_push_event (auddec->srcpad, gst_event_new_eos ());
          }
          break;
        default:
          break;
      }
    }
  }
}

/*
* gst_staudio_set_live_play_policies:
*
* this function sets policies for live playback
*
* auddec : staudio instance
*
* return value: NULL
*/
static void
gst_staudio_set_live_play_policies (Gststaudio * auddec)
{
  struct audio_command audio_cmd;

  GST_LOG_OBJECT (auddec, "%s:%d\n", __FUNCTION__, __LINE__);

  memset (&audio_cmd, 0, sizeof (struct audio_command));
  audio_cmd.cmd = AUDIO_CMD_SET_OPTION;
  audio_cmd.u.option.option = DVB_OPTION_DISCARD_LATE_FRAMES;
  audio_cmd.u.option.value = DVB_OPTION_VALUE_DISCARD_LATE_FRAMES_NEVER;
  gst_staudio_ioctl (auddec, AUDIO_COMMAND, (void *) &audio_cmd);
  GST_DEBUG_OBJECT (auddec,
      "<DVB_OPTION_DISCARD_LATE_FRAMES : DVB_OPTION_VALUE_DISCARD_LATE_FRAMES_NEVER>");
  audio_cmd.u.option.option = DVB_OPTION_MASTER_CLOCK;
  audio_cmd.u.option.value = DVB_OPTION_VALUE_SYSTEM_CLOCK_MASTER;
  gst_staudio_ioctl (auddec, AUDIO_COMMAND, (void *) &audio_cmd);
  GST_DEBUG_OBJECT (auddec,
      "<DVB_OPTION_MASTER_CLOCK : DVB_OPTION_VALUE_SYSTEM_CLOCK_MASTER>");
  audio_cmd.u.option.option = DVB_OPTION_LIVE_PLAY;
  audio_cmd.u.option.value = DVB_OPTION_VALUE_ENABLE;
  gst_staudio_ioctl (auddec, AUDIO_COMMAND, (void *) &audio_cmd);
  GST_DEBUG_OBJECT (auddec, "<DVB_OPTION_LIVE_PLAY : DVB_OPTION_VALUE_ENABLE>");
  GST_DEBUG_OBJECT (auddec,
      "<DVB_OPTION_REBASE_ON_DATA_DELIVERY_LATE : DVB_OPTION_VALUE_DISABLE>");
}

static void
gst_staudio_start_monitor_task (Gststaudio * auddec)
{
  /* Create and initialize the auddec task
     This task will be use as multi purpose monitoring */
  g_rec_mutex_init (&auddec->task_lock);
  auddec->task =
      gst_task_new ((GstTaskFunction) gst_staudio_monitor_loop, auddec, NULL);
  gst_object_set_name (GST_OBJECT_CAST (auddec->task), "SMF-Aud Monitor");
  gst_task_set_lock (auddec->task, &auddec->task_lock);
  gst_task_start (auddec->task);
}

static void
gst_staudio_stop_monitor_task (Gststaudio * auddec)
{
  gst_task_stop (auddec->task);
  g_rec_mutex_lock (&auddec->task_lock);
  g_rec_mutex_unlock (&auddec->task_lock);
  gst_task_join (auddec->task);
  gst_object_unref (auddec->task);
  g_rec_mutex_clear (&auddec->task_lock);
  auddec->task = NULL;
}

static void
gst_staudio_monitor_loop (Gststaudio * auddec)
{
  if (auddec->fd != -1) {
    gst_staudio_check_eos_event (auddec);
  }

  gst_staudio_update_time (auddec);

  usleep (20 * 1000);
}

static gboolean
gst_staudio_capture_setup_device (Gststaudio * auddec)
{
  struct v4l2_input v4l2input;
  gchar *linuxdvb_input;
  gint index = 0;
  GstQuery *query;
  GstBufferPool *pool;
  guint size, min, max;
  gboolean ret = TRUE;

  GST_LOG_OBJECT (auddec, "%s:%d\n", __FUNCTION__, __LINE__);

  /* Choose the LinuxDVB decoder input to use according to the dev-id property        */
  linuxdvb_input = g_strdup_printf ("dvb0.audio%01d", auddec->dev_id);
  GST_INFO_OBJECT (auddec, "DVB Input = %s\n", linuxdvb_input);

  /* Configure capture input */
  memset (&v4l2input, 0, sizeof (struct v4l2_input));
  for (v4l2input.index = 0;; v4l2input.index++) {
    if (ioctl (auddec->v4l2_fd, VIDIOC_ENUMAUDIO, &v4l2input) < 0)
      break;

    if (strcmp ((char *) v4l2input.name, linuxdvb_input) == 0) {
      index = v4l2input.index;
      break;
    }
  }

  if (ioctl (auddec->v4l2_fd, VIDIOC_S_AUDIO, &index) < 0) {
    GST_ERROR_OBJECT (auddec, "Couldn't configure capture input to %s\n",
        linuxdvb_input);
    ret = FALSE;
    goto exit;
  }

  GST_INFO_OBJECT (auddec, "Selected LinuxDVB outputs %d (%s)\n", index,
      linuxdvb_input);

  /* Query for the pool for keeping the capture data */
  query =
      gst_query_new_allocation (gst_pad_get_current_caps (auddec->srcpad),
      TRUE);
  if (query) {
    if (gst_pad_peer_query (auddec->srcpad, query)) {
      if (gst_query_get_n_allocation_pools (query) > 0) {
        gst_query_parse_nth_allocation_pool (query, 0, &pool, &size, &min,
            &max);
        if (pool != NULL) {
          auddec->capture.pool = pool;
          gst_buffer_pool_set_active (pool, TRUE);
          gst_object_ref (pool);
        } else {
          GST_WARNING_OBJECT (auddec, "Failed to get pool from downstream!");
        }
      } else {
        GST_WARNING_OBJECT (auddec, "Failed to get pool from downstream!");
      }
    }
    gst_query_unref (query);
  }

exit:
  g_free (linuxdvb_input);
  return ret;
}

static gboolean
gst_staudio_capture_queue_buffer (Gststaudio * auddec)
{
  GstFlowReturn ret = GST_FLOW_OK;
  struct v4l2_buffer buf;
  struct v4l2_audio_uncompressed_metadata audio_meta;
  gboolean buffer_allocated = FALSE;
  GstAllocationParams params = { 0 };

  GST_LOG_OBJECT (auddec, "%s:%d\n", __FUNCTION__, __LINE__);

  /* REVIEW/CRITICAL : Return value from _acquire_buffer is not propagated
   * upstream */
  if (auddec->capture.pool != NULL) {
    ret =
        gst_buffer_pool_acquire_buffer (auddec->capture.pool,
        &auddec->capture.buffer, NULL);
  } else {
    auddec->capture.buffer =
        gst_buffer_new_allocate (NULL, DEFAULT_SIZE, &params);
  }

  if (ret == GST_FLOW_OK) {
    if ((auddec->capture.buffer) && (auddec->capture.pool)) {
      GstMapInfo info_read;

      if (!auddec->capture.is_stream_on_done) {
        GST_DEBUG_OBJECT (auddec,
            "Configuring Capture Buffer and Stream On for USERPTR ");
        auddec->capture.mem_type = GST_STAUDIO_USER_PTR;

        if (gst_staudio_capture_alloc_buffer (auddec, auddec->v4l2_fd) == FALSE) {
          GST_ERROR_OBJECT (auddec, "Capture Alloc Buffer failed");
          auddec->srcresult = GST_FLOW_ERROR;
          goto exit_error;
        }
        buffer_allocated = TRUE;

        if (gst_staudio_capture_stream_on (auddec, auddec->v4l2_fd) == FALSE) {
          GST_ERROR_OBJECT (auddec, "Capture Stream On failed");
          auddec->srcresult = GST_FLOW_ERROR;
          goto exit_error;
        }
        auddec->capture.is_stream_on_done = TRUE;
      }

      gst_buffer_map (auddec->capture.buffer, &info_read, GST_MAP_WRITE);
      memset (&buf, 0, sizeof (struct v4l2_buffer));
      buf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;
      buf.memory = V4L2_MEMORY_USERPTR;
      buf.m.userptr = (unsigned long) info_read.data;
      buf.field = V4L2_FIELD_NONE;
      buf.length = DEFAULT_SIZE;
      buf.reserved = (int) &audio_meta;
      gst_buffer_unmap (auddec->capture.buffer, &info_read);

      if (ioctl (auddec->v4l2_fd, VIDIOC_QBUF, &buf) < 0) {
        GST_ERROR_OBJECT (auddec, "Couldn't queue buffer %s", strerror (errno));
        goto exit_error;
      }
    } else {
      if (auddec->capture.buffer) {
        if (!auddec->capture.is_stream_on_done) {
          GST_DEBUG_OBJECT (auddec,
              "Configuring Capture Buffer and Stream On for MMAP ");
          auddec->capture.mem_type = GST_STAUDIO_MMAP;

          if (gst_staudio_capture_alloc_buffer (auddec,
                  auddec->v4l2_fd) == FALSE) {
            GST_ERROR_OBJECT (auddec, "Capture Alloc Buffer failed");
            auddec->srcresult = GST_FLOW_ERROR;
            goto exit_error;
          }
          buffer_allocated = TRUE;

          if (gst_staudio_capture_stream_on (auddec, auddec->v4l2_fd) == FALSE) {
            GST_ERROR_OBJECT (auddec, "Capture Stream On failed");
            auddec->srcresult = GST_FLOW_ERROR;
            goto exit_error;
          }
          auddec->capture.is_stream_on_done = TRUE;
        }

        auddec->capture.v4l2_capture_buf.flags = 0;
        auddec->capture.v4l2_capture_buf.index = 0;
        auddec->capture.v4l2_capture_buf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;
        auddec->capture.v4l2_capture_buf.memory = V4L2_MEMORY_MMAP;
        if (ioctl (auddec->v4l2_fd, VIDIOC_QBUF,
                &auddec->capture.v4l2_capture_buf) < 0) {
          GST_ERROR_OBJECT (auddec, "Couldn't queue buffer %s",
              strerror (errno));
          goto exit_error;
        }
      } else {
        /* Need to allocate a GstBuffer and push downstream in this case */
        GST_ERROR_OBJECT (auddec,
            "Invalid Case, should not have reached here. \n");
        goto exit_error;
      }
    }
  } else {
    GST_ERROR_OBJECT (auddec, "Failed to allocate buffer, flow error - %s",
        gst_flow_get_name (ret));
    auddec->srcresult = ret;
    goto exit_error;
  }

  return TRUE;

exit_error:
  if (auddec->capture.buffer) {
    gst_buffer_unref (auddec->capture.buffer);
    auddec->capture.buffer = NULL;
  }

  if (buffer_allocated == TRUE) {
    gst_staudio_capture_free_buffer (auddec, auddec->v4l2_fd);
  }
  return FALSE;
}

static gboolean
gst_staudio_capture_dequeue_buffer (Gststaudio * auddec)
{
  struct v4l2_buffer buf;
  GstClockTime ev_ts;
  GstClockTime start, stop;
  guint64 cstart, cstop;
  GstSegment *segment;
  GstClockTime duration;
  GstMapInfo info_write;

  GST_LOG_OBJECT (auddec, "%s:%d\n", __FUNCTION__, __LINE__);

  if (auddec->capture.mem_type == GST_STAUDIO_INVALID) {
    GST_ERROR_OBJECT (auddec, "Not valid memory type");
    return FALSE;
  }

  memset (&buf, 0, sizeof (struct v4l2_buffer));
  buf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;

  if (auddec->capture.mem_type == GST_STAUDIO_USER_PTR) {
    buf.memory = V4L2_MEMORY_USERPTR;
  } else {
    buf.memory = V4L2_MEMORY_MMAP;
  }

  if (ioctl (auddec->v4l2_fd, VIDIOC_DQBUF, &buf) < 0) {
    /* unref capture buffer since we are not going to push it. */
    gst_buffer_unref (auddec->capture.buffer);

    GST_ERROR_OBJECT (auddec, "Couldn't de-queue user buffer - %s",
        strerror (errno));
    return FALSE;
  }

  if (buf.bytesused == 0) {
    gst_buffer_unref (auddec->capture.buffer);
    GST_DEBUG_OBJECT (auddec, "EOS MARKER received");
    /* Send all queued serialized events downstream */
    /* FIXME: This drain of events downstream on EOS is a workaround for Bug 33744 */
    if (FALSE == gst_staudio_send_queued_events_before (auddec,
            GST_CLOCK_TIME_NONE)) {
      GST_WARNING_OBJECT (auddec, "Sending Event failed at EOS Marker");
    }
    /* Pause the capture task */
    gst_pad_pause_task (auddec->srcpad);
    if (auddec->eos == TRUE) {
      gst_pad_push_event (auddec->srcpad, gst_event_new_eos ());
    }
    return TRUE;
  }

  if (auddec->capture.first_buffer == TRUE) {
    if (gst_staudio_set_channel_positions (auddec, &buf)) {
      gst_pad_push_event (auddec->srcpad,
          gst_event_new_caps (gst_pad_get_current_caps (auddec->srcpad)));
    } else {
      GST_DEBUG_OBJECT (auddec, "Caps will not be updated");
    }
    auddec->capture.first_buffer = FALSE;
  }

  /* Update the buffer size with the returned size */
  gst_buffer_set_size (auddec->capture.buffer, buf.bytesused);

  /* Set the buffer timestamp to the original pts grabbed from streaming engine */
  GST_BUFFER_TIMESTAMP (auddec->capture.buffer) =
      GST_TIMEVAL_TO_TIME (buf.timestamp);
  GST_BUFFER_DURATION (auddec->capture.buffer) = GST_CLOCK_TIME_NONE;

  GST_LOG_OBJECT (auddec, "Capture time : %lld ms (%ld sec %ld usec)\n",
      GST_TIME_AS_MSECONDS (GST_BUFFER_TIMESTAMP (auddec->capture.buffer)),
      buf.timestamp.tv_sec, buf.timestamp.tv_usec);

  /* Check if we have any queued events that's to be sent before the current dequeued buffer timestamp,
     and send them if so. */
  ev_ts = GST_TIME_AS_MSECONDS (GST_BUFFER_TIMESTAMP (auddec->capture.buffer));
  if (FALSE == gst_staudio_send_queued_events_before (auddec, ev_ts)) {
    GST_WARNING_OBJECT (auddec, "Sending Event failed at %lld ms", ev_ts);
  }

  /* Check for clipping */
  start = GST_BUFFER_TIMESTAMP (auddec->capture.buffer);
  duration = GST_BUFFER_DURATION (auddec->capture.buffer);
  stop = GST_CLOCK_TIME_NONE;

  if (GST_CLOCK_TIME_IS_VALID (start) && GST_CLOCK_TIME_IS_VALID (duration)) {
    stop = start + duration;
  }

  if (stop == GST_CLOCK_TIME_NONE) {
    stop = start;
  }

  segment = &auddec->segment;
  if (gst_segment_clip (segment, GST_FORMAT_TIME, start, stop, &cstart, &cstop)) {

    GST_BUFFER_TIMESTAMP (auddec->capture.buffer) = cstart;

    if ((stop != GST_CLOCK_TIME_NONE) && (stop != start)) {
      GST_BUFFER_DURATION (auddec->capture.buffer) = cstop - cstart;
    }
    GST_LOG_OBJECT (auddec,
        "accepting buffer inside segment: %" GST_TIME_FORMAT " %"
        GST_TIME_FORMAT " seg %" GST_TIME_FORMAT " to %" GST_TIME_FORMAT
        " time %" GST_TIME_FORMAT,
        GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (auddec->capture.buffer)),
        GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (auddec->capture.buffer) +
            GST_BUFFER_DURATION (auddec->capture.buffer)),
        GST_TIME_ARGS (segment->start), GST_TIME_ARGS (segment->stop),
        GST_TIME_ARGS (segment->time));
  } else {
    GST_LOG_OBJECT (auddec,
        "dropping buffer outside segment: %" GST_TIME_FORMAT
        " %" GST_TIME_FORMAT
        " seg %" GST_TIME_FORMAT " to %" GST_TIME_FORMAT
        " time %" GST_TIME_FORMAT,
        GST_TIME_ARGS (start), GST_TIME_ARGS (stop),
        GST_TIME_ARGS (segment->start),
        GST_TIME_ARGS (segment->stop), GST_TIME_ARGS (segment->time));
    gst_buffer_unref (auddec->capture.buffer);
    goto done;
  }

  {
    GstClockTime timestamp;
    timestamp = GST_BUFFER_TIMESTAMP (auddec->capture.buffer);
    timestamp =
        gst_segment_to_running_time (segment, GST_FORMAT_TIME, timestamp);
    GST_BUFFER_TIMESTAMP (auddec->capture.buffer) = timestamp;
  }

  if (auddec->capture.mem_type == GST_STAUDIO_MMAP) {
    /* Copy the Capture data from v4l2 buffer to Capture GstBuffer from downstream */
    gst_buffer_map (auddec->capture.buffer, &info_write, GST_MAP_WRITE);
    memcpy (info_write.data, auddec->capture.buf_start_address, buf.bytesused);
    gst_buffer_unmap (auddec->capture.buffer, &info_write);
  }

  /* Push the buffer to downstream element */
  auddec->srcresult = gst_pad_push (auddec->srcpad, auddec->capture.buffer);
  if (auddec->srcresult != GST_FLOW_OK) {
    GST_ERROR_OBJECT (auddec, "Failed to push buffer downstream - %s",
        gst_flow_get_name (auddec->srcresult));
    return FALSE;
  }

done:
  return TRUE;
}

/*
 * This function fetches the channel configuration from buffer metadata.
 * The channel-positions are then set on the srcpad caps.
 */
static gboolean
gst_staudio_set_channel_positions (Gststaudio * auddec, struct v4l2_buffer *buf)
{
  gint i, channels_meta = 0;
  struct v4l2_audio_uncompressed_metadata *audio_meta = NULL;
  GstCaps *newcaps = NULL;
  GstAudioChannelPosition *pos = NULL;
  guint64 channel_mask = 0;

  audio_meta = (struct v4l2_audio_uncompressed_metadata *) buf->reserved;
  if (audio_meta) {
    channels_meta = audio_meta->channel_count;
  } else {
    goto nothing_to_do;
  }

  if (audio_meta->channel_count <= 0)
    goto nothing_to_do;

  for (i = 0; i < audio_meta->channel_count; i++) {
    if (audio_meta->channel[i] == STM_V4L2_AUDIO_CHAN_STUFFING)
      channels_meta--;
  }

  pos = g_new (GstAudioChannelPosition, channels_meta);
  if (!pos) {
    GST_DEBUG_OBJECT (auddec, "Failed to allocate channel position array");
    goto nothing_to_do;
  }

  for (i = 0; i < channels_meta; i++)
    channel_mask |= G_GUINT64_CONSTANT (1) << pos[i];

  newcaps = gst_caps_copy (gst_pad_get_current_caps (auddec->srcpad));
  gst_caps_set_simple (newcaps, "channel-mask", GST_TYPE_BITMASK, channel_mask,
      NULL);
  gst_pad_set_caps (auddec->srcpad, newcaps);
  GST_DEBUG_OBJECT (auddec, "New Caps = %" GST_PTR_FORMAT,
      gst_pad_get_current_caps (auddec->srcpad));
  gst_caps_unref (newcaps);
  g_free (pos);
  return TRUE;


nothing_to_do:
  return FALSE;
}

static gboolean
gst_staudio_capture_alloc_buffer (Gststaudio * auddec, int fd)
{
  struct v4l2_requestbuffers reqbuf;
  struct v4l2_format fmt;
  struct v4l2_audio_format *audio_fmt;

  GST_LOG_OBJECT (auddec, "%s:%d\n", __FUNCTION__, __LINE__);

  if (auddec->capture.mem_type == GST_STAUDIO_INVALID) {
    GST_ERROR_OBJECT (auddec, "Not valid memory type");
    return FALSE;
  }
  /* Configure capture ouput */
  memset (&fmt, 0, sizeof (struct v4l2_format));
  fmt.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;
  /* FIXME: Remove the following check after fix of Bug: 36569
   * Also move this IOCTL: VIDIOC_S_FMT call to gst_staudio_capture_setup_device */
  if (auddec->capture.mem_type == GST_STAUDIO_MMAP) {
    audio_fmt = (struct v4l2_audio_format *) fmt.fmt.raw_data;
    audio_fmt->BitsPerSample = auddec->capture.depth;
    audio_fmt->Channelcount = auddec->capture.channels;
    audio_fmt->SampleRateHz = auddec->capture.sample_rate;
    GST_DEBUG_OBJECT (auddec,
        "Setting Format BitsPerSample %d Channelcount %d SampleRateHz %d",
        audio_fmt->BitsPerSample, audio_fmt->Channelcount,
        audio_fmt->SampleRateHz);
  }

  if (ioctl (auddec->v4l2_fd, VIDIOC_S_FMT, &fmt) < 0) {
    GST_ERROR_OBJECT (auddec, "VIDIOC_S_FMT failed - %s", strerror (errno));
    return FALSE;
  }

  /* Capture Buffer formats : VIDIOC_REQBUFS */
  memset (&reqbuf, 0, sizeof (struct v4l2_requestbuffers));
  reqbuf.count = 1;
  reqbuf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;
  if (auddec->capture.mem_type == GST_STAUDIO_USER_PTR) {
    reqbuf.memory = V4L2_MEMORY_USERPTR;
  } else {
    reqbuf.memory = V4L2_MEMORY_MMAP;
  }

  if (ioctl (auddec->v4l2_fd, VIDIOC_REQBUFS, &reqbuf) < 0) {
    GST_ERROR_OBJECT (auddec, "VIDIOC_REQBUFS %s", strerror (errno));
    return FALSE;
  }

  /* mmap the allocated buffer in case of V4L2_MEMORY_MMAP */
  if (auddec->capture.mem_type == GST_STAUDIO_MMAP) {
    memset (&auddec->capture.v4l2_capture_buf, 0, sizeof (struct v4l2_buffer));
    auddec->capture.v4l2_capture_buf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    auddec->capture.v4l2_capture_buf.index = 0;
    auddec->capture.v4l2_capture_buf.memory = V4L2_MEMORY_MMAP;
    if (ioctl (auddec->v4l2_fd, VIDIOC_QUERYBUF,
            &(auddec->capture.v4l2_capture_buf)) < 0) {
      GST_ERROR_OBJECT (auddec, "VIDIOC_QUERYBUF failed - %s",
          strerror (errno));
      return FALSE;
    }
    auddec->capture.buf_start_address =
        (guchar *) mmap (NULL, auddec->capture.v4l2_capture_buf.length,
        PROT_READ | PROT_WRITE, MAP_SHARED, auddec->v4l2_fd,
        auddec->capture.v4l2_capture_buf.m.offset);
    if (auddec->capture.buf_start_address == ((void *) -1)) {
      GST_ERROR_OBJECT (auddec,
          "mmap for Capture Buffer failed v4l2 fd %d length %d offset %d",
          auddec->v4l2_fd, auddec->capture.v4l2_capture_buf.length,
          auddec->capture.v4l2_capture_buf.m.offset);
      return FALSE;
    }
    GST_DEBUG_OBJECT (auddec,
        "mmap for Capture Buffer passed v4l2 fd %d length %d offset %d",
        auddec->v4l2_fd, auddec->capture.v4l2_capture_buf.length,
        auddec->capture.v4l2_capture_buf.m.offset);
  }

  return TRUE;
}

static gboolean
gst_staudio_capture_free_buffer (Gststaudio * auddec, int fd)
{
  struct v4l2_requestbuffers reqbuf;

  GST_LOG_OBJECT (auddec, "%s:%d\n", __FUNCTION__, __LINE__);

  if (auddec->capture.mem_type != GST_STAUDIO_INVALID) {

    /* unmap the Capture Buffer */
    if ((auddec->capture.mem_type == GST_STAUDIO_MMAP)
        && (auddec->capture.buf_start_address != NULL)) {
      GST_DEBUG_OBJECT (auddec, "unmap Capture Buffer");
      munmap (auddec->capture.buf_start_address,
          auddec->capture.v4l2_capture_buf.length);
    }

    /* Free the v4l2 buffer:  VIDIOC_REQBUFS */
    memset (&reqbuf, 0, sizeof (struct v4l2_requestbuffers));
    reqbuf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;
    reqbuf.count = 0;

    if (auddec->capture.mem_type == GST_STAUDIO_USER_PTR) {
      reqbuf.memory = V4L2_MEMORY_USERPTR;
    } else {
      reqbuf.memory = V4L2_MEMORY_MMAP;
    }
    if (ioctl (fd, VIDIOC_REQBUFS, &reqbuf) < 0) {
      GST_ERROR_OBJECT (auddec, "VIDIOC_REQBUFS failed %s", strerror (errno));
      return FALSE;
    }
  }
  auddec->capture.mem_type = GST_STAUDIO_INVALID;
  auddec->capture.buf_start_address = NULL;

  return TRUE;
}


static gboolean
gst_staudio_capture_stream_on (Gststaudio * auddec, int fd)
{
  struct v4l2_buffer buf;

  GST_LOG_OBJECT (auddec, "%s:%d\n", __FUNCTION__, __LINE__);

  memset (&buf, 0, sizeof (struct v4l2_buffer));
  buf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;

  if (ioctl (fd, VIDIOC_STREAMON, &buf.type) < 0) {
    GST_ELEMENT_ERROR (auddec, RESOURCE, FAILED,
        ("Failed to start streaming on device %d", auddec->dev_id),
        ("VIDIOC_STREAMON failed : %s", strerror (errno)));
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_staudio_capture_stream_off (Gststaudio * auddec, int fd)
{
  struct v4l2_buffer buf;

  GST_LOG_OBJECT (auddec, "%s:%d\n", __FUNCTION__, __LINE__);

  memset (&buf, 0, sizeof (struct v4l2_buffer));
  buf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;

  if (ioctl (fd, VIDIOC_STREAMOFF, &buf.type) < 0) {
    GST_ELEMENT_ERROR (auddec, RESOURCE, FAILED,
        ("Failed to stop streaming on device %d", auddec->dev_id),
        ("VIDIOC_STREAMOFF failed : %s", strerror (errno)));
    return FALSE;
  }

  return TRUE;
}

/* ---------------------------------------------------

    due to a bug in the ST V4L capture and the fact we can't
    unlock proprely the dqueue buffer with  a stream off or close()
    the capture is initialized in NOBLOCKING and we have to manage
    the exit of the thread.

    the callabck push a buffer and try to capture it a push it in the bad
    the idle exits for each farme to give a chance to quit the thread cleanly

 --------------------------------------------------*/
 /*
  * Function name : gst_staudio_audio_loop
  *  Input        :
  *   auddec - audio decoder control information
  *
  *  Description  : This task is used in two usecases
  *   capture: it reads the buffer from streaming engine and push buffers downstream
  *   playback: it pushes dummy buffer to downstream ( basesink requires a buffer to preroll )
  *
  * Return        :  none
  */

static void
gst_staudio_audio_loop (Gststaudio * auddec)
{
  if (auddec->exit_loop) {
    GST_DEBUG_OBJECT (auddec, "exit_loop true, going to pause");
    goto error1;
  }

  /* push a dummy buffer to basesink */
  if (auddec->tunneled == TRUE) {
    g_mutex_lock (&auddec->dummy_buff.mutex);
    /* wait only if there is no request to push dummy buffer */
    if ((auddec->first_frame_pushed == TRUE)
        && (auddec->audio_task_state == GST_STAUDIO_TASK_STARTED)) {
      GST_DEBUG_OBJECT (auddec, "waiting for signal to push dummy buffer");
      GST_STAUDIO_AUDIO_LOOP_WAIT (auddec);
    }
    g_mutex_unlock (&auddec->dummy_buff.mutex);
    gst_staudio_push_dummy_packet (auddec);

    if (auddec->srcresult != GST_FLOW_OK)
      goto error1;

    return;
  }

  /* Push the empty buffer in the queue */
  if (!gst_staudio_capture_queue_buffer (auddec)) {
    GST_ERROR_OBJECT (auddec, "Could not queue buffer");
    goto error;
  }

  if (!gst_staudio_capture_dequeue_buffer (auddec)) {
    GST_ERROR_OBJECT (auddec, "Could not dequeue buffer");
    goto error;
  }

  return;

error:
  usleep (10 * 1000);
  return;

error1:
  gst_pad_pause_task (auddec->srcpad);
  return;
}

static gboolean
gst_staudio_manage_caps (Gststaudio * auddec)
{
  GstCaps *srccaps = NULL;
  GstCaps *allowed_caps = NULL;
  GstStructure *srcstructure = NULL;
  gboolean ret = FALSE;

  /* Do allowed caps to get the caps of the downstream element for negotiation  */
  allowed_caps = gst_pad_get_allowed_caps (auddec->srcpad);
  if (allowed_caps && !gst_caps_is_empty (allowed_caps)) {
    srccaps = gst_caps_copy_nth (allowed_caps, 0);
    gst_caps_unref (allowed_caps);

    srcstructure = gst_caps_get_structure (srccaps, 0);
    if (gst_structure_has_name (srcstructure, "audio/x-fake-int-stm")) {
      if (auddec->dev_id_prop == DEFAULT_DEV_ID) {
        if (gst_structure_get_int (srcstructure, "dev-id",
                &auddec->dev_id_prop))
          GST_DEBUG_OBJECT (auddec, "dev-id: %d", auddec->dev_id_prop);
      }
      gst_caps_unref (srccaps);
      srccaps = gst_caps_new_simple ("audio/x-fake-int-stm",
          "dev-id", G_TYPE_INT, auddec->dev_id_prop,
          "rate", G_TYPE_INT, auddec->properties.sampling_freq,
          "channels", G_TYPE_INT, auddec->properties.num_chans, NULL);
      auddec->tunneled = TRUE;
      GST_DEBUG_OBJECT (auddec, "%s", "audio/x-fake-int-stm");
    } else {
      gst_caps_unref (srccaps);
      srccaps = gst_caps_new_simple ("audio/x-raw",
          "depth", G_TYPE_INT, auddec->capture.depth,
          "width", G_TYPE_INT, 32,
          "channels", G_TYPE_INT, auddec->capture.channels,
          "endianness", G_TYPE_INT, G_LITTLE_ENDIAN,
          "signed", G_TYPE_BOOLEAN, TRUE,
          "rate", G_TYPE_INT, auddec->capture.sample_rate,
          "format", G_TYPE_STRING, "S32LE",
          "layout", G_TYPE_STRING, "interleaved",
          "channel-mask", GST_TYPE_BITMASK, (guint64) DEFAULT_CHANNEL_MASK,
          NULL);

      auddec->capture.first_buffer = TRUE;
      auddec->tunneled = FALSE;
      GST_DEBUG_OBJECT (auddec, "%s", "audio/x-raw");
    }
  } else {
    if (auddec->dev_id_prop == DEFAULT_DEV_ID)
      auddec->dev_id_prop = 0;
    srccaps = gst_caps_new_simple ("audio/x-fake-int-stm",
        "dev-id", G_TYPE_INT, auddec->dev_id_prop,
        "rate", G_TYPE_INT, auddec->properties.sampling_freq,
        "channels", G_TYPE_INT, auddec->properties.num_chans, NULL);
    GST_DEBUG_OBJECT (auddec, "%s", "audio/x-fake-int-stm");
    if (allowed_caps) {
      gst_caps_unref (allowed_caps);
    }
    auddec->tunneled = TRUE;
  }
  /* Fixate caps on src pad */
  gst_pad_use_fixed_caps (auddec->srcpad);
  ret = gst_pad_set_caps (auddec->srcpad, srccaps);
  gst_caps_unref (srccaps);

  return TRUE;
}


/*
 function: gst_staudio_manage_dev_id
 auddec: staudio instance
 description: this function opens audio device based on
              device id received in manage_caps or set
              by application.
 return value: TRUE if device opened or FALSE
*/
static gboolean
gst_staudio_manage_dev_id (Gststaudio * auddec)
{
  GststaudioClass *klass;
  gchar *output_link;
  gchar *device_name;
  gboolean success = FALSE;
  guint i;
  Gststaudio *current_active = NULL;
  klass = GST_STAUDIO_CLASS (G_OBJECT_GET_CLASS (auddec));

  GST_DEBUG_OBJECT (auddec,
      "staudio instance dev-id %d Property dev-id %d ", auddec->dev_id,
      auddec->dev_id_prop);

  if (auddec->dev_id_prop == DEFAULT_DEV_ID) {
    GST_DEBUG_OBJECT (auddec, "Negotiating dev-id");
    if (!auddec->tunneled) {
      for (i = 0; i <= MAX_DEV_ID; i++) {
        device_name = g_strdup_printf ("dvb0.audio%d", i);

        /* Use media-controller library to get appropriate audio-device */
        output_link = gst_stm_mctl_get_entity_sink (device_name);
        g_free (device_name);
        if (output_link == NULL) {
          GST_ERROR_OBJECT (auddec,
              "Failed to get appropriate audio entity from media-ctl");
          return FALSE;
        }

        /* STM Grab Overlay is the entity returned by the media controller
           if the audio decoder is not linked to mixer 0 or mixer 1 */
        if (!strcmp (output_link, "STM Grab Overlay")) {
          auddec->dev_id = i;
          if (gst_staudio_open_device (auddec)) {
            success = TRUE;
            g_free (output_link);
            break;
          }
        }
        g_free (output_link);
      }
      if (!success) {
        GST_ERROR_OBJECT (auddec,
            "Failed to find audio device for capturing , all devices are BUSY");
        return FALSE;
      }
    }
  } else {
    GST_DEBUG_OBJECT (auddec, "Using dev-id set by App %d",
        auddec->dev_id_prop);
    auddec->dev_id = auddec->dev_id_prop;
    device_name = g_strdup_printf ("dvb0.audio%d", auddec->dev_id);

    /* Use media-controller to query for tunnelled or non-tunnelled mode */
    output_link = gst_stm_mctl_get_entity_sink (device_name);
    g_free (device_name);
    if (output_link == NULL) {
      GST_ERROR_OBJECT (auddec,
          "Failed to get appropriate audio mixer from media-ctl");
      return FALSE;
    }
    g_free (output_link);
    GST_OBJECT_LOCK (auddec);
    if (auddec->is_active == TRUE) {
      GST_OBJECT_UNLOCK (auddec);
      g_mutex_lock (&klass->active[auddec->dev_id].lock);
      current_active = gst_staudio_get_active_element (auddec);
      g_mutex_unlock (&klass->active[auddec->dev_id].lock);
      if (current_active != NULL && current_active != auddec) {
        GST_DEBUG_OBJECT (auddec,
            "Forcing Close of current active device for dev-id %d fd %d",
            current_active->dev_id, current_active->fd);
        gst_staudio_close_device (current_active, FALSE);
        usleep (10 * 1000);
      }
    } else {
      GST_OBJECT_UNLOCK (auddec);
    }
    g_mutex_lock (&klass->active[auddec->dev_id].lock);
    current_active = gst_staudio_get_active_element (auddec);
    g_mutex_unlock (&klass->active[auddec->dev_id].lock);
    if (current_active == NULL) {
      if (!gst_staudio_open_device (auddec)) {
        GST_ERROR_OBJECT (auddec, "Failed to open audio dev-id %d",
            auddec->dev_id);
        return FALSE;
      } else {
        GST_DEBUG_OBJECT (auddec, "Opened audio device fd %d dev id%d",
            auddec->fd, auddec->dev_id);
      }
    }
  }

  GST_DEBUG_OBJECT (auddec, "Device id  = %d , tunneled = %s",
      auddec->dev_id, auddec->tunneled ? "TRUE" : "FALSE");

  gst_staudio_setup_device (auddec);
  auddec->first_injection = TRUE;

  if (auddec->tunneled == FALSE) {
    if (gst_staudio_capture_setup_device (auddec)) {
      if (gst_pad_start_task (auddec->srcpad,
              (GstTaskFunction) gst_staudio_audio_loop, auddec, NULL) != TRUE) {
        GST_ERROR_OBJECT (auddec, "cannot start capture task");
        return FALSE;
      }
    }
  } else {
    if (gst_pad_start_task (auddec->srcpad,
            (GstTaskFunction) gst_staudio_audio_loop, auddec, NULL) != TRUE) {
      GST_ERROR_OBJECT (auddec, "cannot start staudio loop task for playback");
      return FALSE;
    }
    auddec->audio_task_state = GST_STAUDIO_TASK_STARTED;
    auddec->first_frame_pushed = FALSE;
  }

  return TRUE;
}
