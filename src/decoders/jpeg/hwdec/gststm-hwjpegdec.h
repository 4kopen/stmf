/* Gstreamer ST HW JPEG Decoder Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STM_HWJPEGDEC_H__
#define __GST_STM_HWJPEGDEC_H__


#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <gst/video/video.h>
#include "gststvidcapture.h"
#include <sys/mman.h>
#include <linux/videodev2.h>

G_BEGIN_DECLS
#define GST_TYPE_HWJPEG_DEC \
  (gst_hwjpeg_dec_get_type())
#define GST_HWJPEG_DEC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_HWJPEG_DEC,GstHWJpegDec))
#define GST_HWJPEG_DEC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_HWJPEG_DEC,GstHWJpegDecClass))
#define GST_IS_HWJPEG_DEC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_HWJPEG_DEC))
#define GST_IS_HWJPEG_DEC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_HWJPEG_DEC))
#define DEFAULT_PLANE_WIDTH             1920
#define DEFAULT_PLANE_HEIGHT            1080
#define MIN_WIDTH                       1
#define MAX_WIDTH                       65535
#define MIN_HEIGHT                      1
#define MAX_HEIGHT                      65535
typedef struct _GstHWJpegDec GstHWJpegDec;
typedef struct _GstHWJpegDecClass GstHWJpegDecClass;

/* Can't use GstBaseTransform, because GstBaseTransform
 * doesn't handle the N buffers in, 1 buffer out case,
 * but only the 1-in 1-out case */
struct _GstHWJpegDec
{
  GstElement element;

  /* pads */
  GstPad *sinkpad;
  GstPad *srcpad;

  GstSegment segment;
  gint videofd;                 /* handle to opened JPEG device */
  guint offset;                 /* offset to write in input buffer for decode */
  gint64 image_size;            /* size of compressed image */
  struct v4l2_buffer inputbuf;  /* input buf */
  struct v4l2_buffer outputbuf; /* output buf */
  void *input_addr;             /* mmaped address to write compressed image data */
  void *output_addr;            /* output address */
  guint width;                  /* image width */
  guint height;                 /* image height */
  guint fourcc;                 /* fourcc/pixel format */
  gboolean multipart_container  /* Multipart jpeg file */
};

struct _GstHWJpegDecClass
{
  GstElementClass parent_class;
};

GType gst_hwjpeg_dec_get_type (void);
gboolean sthwjpegdec_init (GstPlugin * plugin);

G_END_DECLS
#endif /* __GST_STM_HWJPEGDEC_H__ */
