/* Gstreamer ST HW JPEG Decoder Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:element-sthwjpegdec
 *
 * Description: sthwjpegdec element is used for invoking the ST hardware jpeg decoder v4l2 interface ioctl
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 filesrc location="Image.jpg" ! sthwjpegdec ! imagefreeze ! videoconvert ! videoscale ! stdisplaysink
 * ]| Decode single jpeg file
 * |[
 * gst-launch-1.0 filesrc location="Video.mjpeg" ! multipartdemux ! queue2 ! sthwjpegdec ! videoconvert ! videoscale ! stdisplaysink
 * ]| Decode multipart jpeg file format
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <string.h>

#include "gststm-hwjpegdec.h"
#include "v4l2_utils.h"

/* Number of input/output buffers allocated for decode */
#define NUM_INPUT_OUTPUT_BUF 1

static GstStaticPadTemplate gst_hwjpeg_dec_src_pad_template =
    GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("I420") "; "
        GST_VIDEO_CAPS_MAKE ("NV12") "; "
        GST_VIDEO_CAPS_MAKE ("NV16") "; "
        GST_VIDEO_CAPS_MAKE ("NV24") "; "
        GST_VIDEO_CAPS_MAKE ("YV12") "; " GST_VIDEO_CAPS_MAKE ("YU12"))
    );

/* FIXME: sof-marker is for IJG libjpeg 8, should be different for 6.2 */
static GstStaticPadTemplate gst_hwjpeg_dec_sink_pad_template =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("image/jpeg")
    );

GST_DEBUG_CATEGORY_STATIC (hwjpeg_dec_debug);
#define GST_CAT_DEFAULT hwjpeg_dec_debug
GST_DEBUG_CATEGORY_STATIC (GST_CAT_PERFORMANCE);

#define gst_hwjpeg_dec_parent_class parent_class
G_DEFINE_TYPE (GstHWJpegDec, gst_hwjpeg_dec, GST_TYPE_ELEMENT);

struct fmt
{
  unsigned bppsize;
  unsigned int fourcc;
  struct
  {
    unsigned num;
    unsigned denom;
  } sampling;
  unsigned chromasampling;
  char *desc;
  char *shortdesc;
};


static struct fmt fmttable[] = {
  [0] = {
        .bppsize = 0,
        .fourcc = 0,
        .sampling = {0, 0},
        .desc = 0,
      .shortdesc = 0},
  [1] = {
        .bppsize = 12,
        .fourcc = V4L2_PIX_FMT_NV12,
        .sampling = {1, 2},
        .desc = "Non-contiguous two planes YCbCr 4:2:0",
      .shortdesc = "420"},
  [2] = {
        .bppsize = 16,
        .fourcc = V4L2_PIX_FMT_NV16,
        .sampling = {1, 1},
        .desc = "Non-contiguous two planes YCbCr 4:2:2",
      .shortdesc = "422"},
  [3] = {
        .bppsize = 24,
        .fourcc = V4L2_PIX_FMT_NV24,
        .sampling = {2, 1},
        .desc = "Non-contiguous two planes YCbCr 4:4:4",
      .shortdesc = "444"}
};

#define NUM_FORMATS sizeof(fmttable) / sizeof(struct fmt)

static gboolean gst_hwjpeg_dec_query (GstElement * element, GstQuery * query);
static gboolean gst_hwjpeg_dec_sink_acceptcaps (GstCaps * caps);
static void gst_hwjpeg_dec_class_init (GstHWJpegDecClass * klass);
static void gst_hwjpeg_dec_init (GstHWJpegDec * hwjpegdec);

static GstFlowReturn gst_hwjpeg_dec_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buffer);

static GstCaps *gst_hwjpeg_dec_getcaps (GstHWJpegDec * hwjpegdec);
static gboolean gst_hwjpeg_dec_sink_event (GstPad * pad, GstObject * parent,
    GstEvent * event);
static GstStateChangeReturn gst_hwjpeg_dec_change_state (GstElement * element,
    GstStateChange transition);
static void gst_hwjpeg_send_msg (GstHWJpegDec * dec);
static gboolean gst_hwjpeg_dec_dealloc (GstHWJpegDec * dec, void **input_addr,
    gint src_length, void **output_addr, gint dst_length);
static gboolean gst_hwjpeg_dec_alloc_input_buf (GstHWJpegDec * dec,
    guint jpeg_src_size);
static void gst_hwjpeg_dec_dealloc_v4l2_buf (GstHWJpegDec * dec, gint type);
static gboolean gst_hwjpeg_dec_decode_image (GstHWJpegDec * dec);
static gboolean gst_hwjpeg_dec_alloc_output_buf (GstHWJpegDec * dec,
    struct v4l2_format *output_format);
static GstFlowReturn gst_hwjpeg_dec_push_buf (GstHWJpegDec * dec);
static gboolean gst_hwjpeg_dec_alloc_v4l2_buf (GstHWJpegDec * dec,
    struct v4l2_buffer *buf, gint type);
static void gst_hwjpeg_dec_reset_params (GstHWJpegDec * dec);

static void
gst_hwjpeg_send_msg (GstHWJpegDec * dec)
{
  /* hwjpegdec is not capable of decode, inform application */
  gst_element_post_message (GST_ELEMENT_CAST (dec),
      gst_message_new_element (GST_OBJECT (dec),
          gst_structure_new_empty ("disable-hwjpegdec")));
}


static void
gst_hwjpeg_dec_class_init (GstHWJpegDecClass * klass)
{
  GstElementClass *gstelement_class;

  gstelement_class = (GstElementClass *) klass;

  parent_class = g_type_class_peek_parent (klass);

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_hwjpeg_dec_change_state);
  gstelement_class->query = gst_hwjpeg_dec_query;

  GST_DEBUG_CATEGORY_INIT (hwjpeg_dec_debug, "sthwjpegdec", 0, "JPEG decoder");
  GST_DEBUG_CATEGORY_GET (GST_CAT_PERFORMANCE, "GST_PERFORMANCE");

  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&gst_hwjpeg_dec_src_pad_template));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&gst_hwjpeg_dec_sink_pad_template));
  gst_element_class_set_static_metadata (gstelement_class,
      "Hardware JPEG image decoder", "Codec/Decoder/Jpeg",
      "ST hardware image decoder for JPEG format", "http://www.st.com");
}

/* this function checks if we can accept these caps */
static gboolean
gst_hwjpeg_dec_sink_acceptcaps (GstCaps * caps)
{
  gboolean ret = TRUE;
  gint videofd;

  videofd = v4l2_open_by_name (V4L2_HWJPEG_DRIVER_NAME,
      V4L2_HWJPEG_CARD_NAME, O_RDWR);

  if (videofd < 0) {
    GST_LOG ("couldn't open <%s %s>!\n", V4L2_HWJPEG_DRIVER_NAME,
        V4L2_HWJPEG_CARD_NAME);
    ret = FALSE;
  } else {
    ret = TRUE;
    close (videofd);
  }
  return ret;
}

static gboolean
gst_hwjpeg_dec_query (GstElement * element, GstQuery * query)
{
  GstHWJpegDec *dec = (GstHWJpegDec *) element;
  gboolean res = FALSE;

  GST_DEBUG_OBJECT (dec, "%s - (%s)", __FUNCTION__,
      gst_query_type_get_name (GST_QUERY_TYPE (query)));

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_ACCEPT_CAPS:
    {
      GstCaps *caps;
      gst_query_parse_caps (query, &caps);
      res = gst_hwjpeg_dec_sink_acceptcaps (caps);
      gst_query_set_accept_caps_result (query, res);
      res = TRUE;
      goto done;
    }
      break;
    case GST_QUERY_CAPS:
    {
      {
        GstCaps *caps;
        gst_query_parse_caps (query, &caps);
        caps = gst_hwjpeg_dec_getcaps (dec);
        gst_query_set_caps_result (query, caps);
        res = TRUE;
        goto done;
      }
      break;
    }
    default:
      /* send to pads which link to this pad to query */
      res = gst_pad_peer_query (dec->sinkpad, query);
      break;
  }

  if (res == FALSE) {
    GST_WARNING_OBJECT (dec, "query %d failed", GST_QUERY_TYPE (query));
  }
  GST_LOG_OBJECT (dec, " < %s", __FUNCTION__);
done:
  return res;
}


static void
gst_hwjpeg_dec_init (GstHWJpegDec * dec)
{
  GST_DEBUG_OBJECT (dec, "initializing");

  /* create the sink and src pads */
  dec->sinkpad =
      gst_pad_new_from_static_template (&gst_hwjpeg_dec_sink_pad_template,
      "sink");
  gst_element_add_pad (GST_ELEMENT (dec), dec->sinkpad);

  gst_pad_set_chain_function (dec->sinkpad,
      GST_DEBUG_FUNCPTR (gst_hwjpeg_dec_chain));
  gst_pad_set_event_function (dec->sinkpad,
      GST_DEBUG_FUNCPTR (gst_hwjpeg_dec_sink_event));

  dec->srcpad =
      gst_pad_new_from_static_template (&gst_hwjpeg_dec_src_pad_template,
      "src");
  gst_pad_use_fixed_caps (dec->srcpad);
  gst_element_add_pad (GST_ELEMENT (dec), dec->srcpad);

  gst_segment_init (&dec->segment, GST_FORMAT_UNDEFINED);

  /* by default is not a multipart container */
  dec->multipart_container = FALSE;
}

static GstCaps *
gst_hwjpeg_dec_getcaps (GstHWJpegDec * dec)
{
  GstCaps *caps = NULL;
  GstPad *peer = NULL;

  if (gst_pad_get_current_caps (dec->sinkpad)) {
    return gst_caps_ref (gst_pad_get_current_caps (dec->sinkpad));
  }

  peer = gst_pad_get_peer (dec->srcpad);

  if (peer) {
    GstCaps *peer_caps;
    GstCaps *templ_caps;
    GstStructure *s;
    guint i, n;

    peer_caps = gst_pad_get_current_caps (peer);

    /* Translate peercaps to image/jpeg */
    if (peer_caps) {
      peer_caps = gst_caps_make_writable (peer_caps);
      n = gst_caps_get_size (peer_caps);
      for (i = 0; i < n; i++) {
        s = gst_caps_get_structure (peer_caps, i);

        gst_structure_set_name (s, "image/jpeg");
      }

      templ_caps = gst_pad_get_pad_template_caps (dec->sinkpad);
      caps = gst_caps_intersect_full (peer_caps, templ_caps,
          GST_CAPS_INTERSECT_FIRST);
      gst_caps_unref ((GstCaps *) templ_caps);
      gst_caps_unref (peer_caps);
    }
    gst_object_unref (peer);
  } else {
    caps = gst_caps_copy (gst_pad_get_pad_template_caps (dec->sinkpad));
  }

  return caps;
}

static struct fmt *
getfmt (unsigned int fourcc)
{
  int i;
  for (i = 0; i < NUM_FORMATS; i++) {
    if (fmttable[i].fourcc == fourcc) {
      return &fmttable[i];
    }
  }
  return NULL;
}


static GstFlowReturn
gst_hwjpeg_dec_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
  GstHWJpegDec *dec;
  GstMapInfo info_read;
  gint64 size;

  dec = GST_HWJPEG_DEC (parent);

  gst_buffer_map (buf, &info_read, GST_MAP_READ);

  if (dec->multipart_container) {
    gst_hwjpeg_dec_reset_params (dec);
    size = info_read.size;
  } else {
    size = dec->image_size;
  }

  if (!dec->input_addr) {
    if (!gst_hwjpeg_dec_alloc_input_buf (dec, size))
      goto error;
    dec->image_size = size;
  }

  if (dec->offset + info_read.size > dec->image_size) {
    GST_ERROR_OBJECT (dec,
        "received input size %d exceeding queried image size %lld",
        info_read.size, dec->image_size);
    return GST_FLOW_ERROR;
  }

  memcpy (dec->input_addr + dec->offset, info_read.data, info_read.size);
  dec->offset += info_read.size;
  gst_buffer_unmap (buf, &info_read);
  gst_buffer_unref (buf);

  if (dec->multipart_container) {
    /* decode Image */
    if (!gst_hwjpeg_dec_decode_image (dec))
      goto error;
    /* push decoded data to display */
    return gst_hwjpeg_dec_push_buf (dec);
  }

  return GST_FLOW_OK;

error:
  /* dealloc reqbuf */
  gst_hwjpeg_dec_dealloc (dec, &dec->input_addr, dec->inputbuf.length,
      &dec->output_addr, dec->outputbuf.length);
  /* inform app about failure */
  gst_hwjpeg_send_msg (dec);

  return GST_FLOW_ERROR;
}

static void
gst_hwjpeg_dec_dealloc_v4l2_buf (GstHWJpegDec * dec, gint type)
{
  struct v4l2_requestbuffers reqbufs;

  /* request buffer */
  memset (&reqbufs, 0, sizeof (reqbufs));
  reqbufs.count = 0;
  reqbufs.type = type;
  reqbufs.memory = V4L2_MEMORY_MMAP;

  if (ioctl (dec->videofd, VIDIOC_REQBUFS, &reqbufs) < 0) {
    GST_WARNING_OBJECT (dec, "Failed to dealloc reqbuf");
  }
}

static gboolean
gst_hwjpeg_dec_alloc_v4l2_buf (GstHWJpegDec * dec, struct v4l2_buffer *buf,
    gint type)
{
  struct v4l2_requestbuffers reqbufs;

  /* request buffer */
  memset (&reqbufs, 0, sizeof (reqbufs));
  reqbufs.count = NUM_INPUT_OUTPUT_BUF;
  reqbufs.type = type;
  reqbufs.memory = V4L2_MEMORY_MMAP;

  if (ioctl (dec->videofd, VIDIOC_REQBUFS, &reqbufs) < 0)
    goto error_reqbuf;

  if (reqbufs.count < NUM_INPUT_OUTPUT_BUF)
    goto error_req_buf_cnt;

  memset (buf, 0, sizeof (struct v4l2_buffer));
  buf->index = 0;
  buf->type = type;
  buf->memory = V4L2_MEMORY_MMAP;

  if (ioctl (dec->videofd, VIDIOC_QUERYBUF, buf) < 0)
    goto error_querybuf;

  if (ioctl (dec->videofd, VIDIOC_STREAMON, &type) < 0)
    goto error_streamon;

  return TRUE;

error_reqbuf:
  GST_ERROR_OBJECT (dec, "Failed VIDIOC_REQBUFS - %s", strerror (errno));
  return FALSE;
error_req_buf_cnt:
  GST_ERROR_OBJECT (dec, "allocated buffers %d < required %d buffers",
      reqbufs.count, NUM_INPUT_OUTPUT_BUF);
  return FALSE;
error_querybuf:
  GST_ERROR_OBJECT (dec, "Failed VIDIOC_QUERYBUF - %s", strerror (errno));
  return FALSE;
error_streamon:
  GST_ERROR_OBJECT (dec, "Failed VIDIOC_STREAMON - %s", strerror (errno));
  return FALSE;
}

static gboolean
gst_hwjpeg_dec_alloc_input_buf (GstHWJpegDec * dec, guint jpeg_src_size)
{
  struct v4l2_format format;

  if (dec->videofd < 0)
    goto error_no_dev;

  /* set format on the JPEG decoder */
  memset (&format, 0, sizeof (format));
  format.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  format.fmt.pix.pixelformat = V4L2_PIX_FMT_JPEG;
  format.fmt.pix.sizeimage = jpeg_src_size;

  if (ioctl (dec->videofd, VIDIOC_S_FMT, &format) < 0)
    goto error_set_fmt;

  if (!gst_hwjpeg_dec_alloc_v4l2_buf (dec, &dec->inputbuf,
          V4L2_BUF_TYPE_VIDEO_OUTPUT))
    goto error_alloc_v4l2_buf;

  dec->input_addr = mmap (NULL, dec->inputbuf.length, PROT_READ | PROT_WRITE,
      MAP_SHARED, dec->videofd, dec->inputbuf.m.offset);
  if ((dec->input_addr == MAP_FAILED) || (dec->input_addr == NULL))
    goto error_mmap;

  return TRUE;

error_no_dev:
  GST_ERROR_OBJECT (dec, "device not opened yet");
  return FALSE;
error_set_fmt:
  GST_ERROR_OBJECT (dec, "Failed VIDIOC_S_FMT - %s", strerror (errno));
  return FALSE;
error_alloc_v4l2_buf:
  GST_ERROR_OBJECT (dec, "failed to allocate v4l2 buf");
  return FALSE;
error_mmap:
  GST_ERROR_OBJECT (dec, "mmap buffer failed\n");
  return FALSE;
}

static gboolean
gst_hwjpeg_dec_alloc_output_buf (GstHWJpegDec * dec,
    struct v4l2_format *output_format)
{
  guint32 image_width = 0, image_height = 0;
  struct v4l2_format format;
  struct v4l2_format outputformat;
  gint width = -1, height = -1;

  memset (&format, 0, sizeof (format));
  format.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

  if (ioctl (dec->videofd, VIDIOC_G_FMT, &format) < 0)
    goto error_get_fmt;

  image_width = format.fmt.pix.width;
  image_height = format.fmt.pix.height;

  /*We make sure that the plane dimensions passed for trying
     format are defined in a proportionate range
   */
  width = DEFAULT_PLANE_WIDTH;
  height = DEFAULT_PLANE_HEIGHT;

  format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  format.fmt.pix.width = width;
  format.fmt.pix.height = height;

  outputformat = format;

  if (ioctl (dec->videofd, VIDIOC_TRY_FMT, &outputformat) < 0)
    goto error_try_fmt;

  GST_INFO_OBJECT (dec,
      "VIDIOC_TRY_FMT (dst): driver suggested output buffer "
      "dimensions: %dx%d, imagesize: %d\n", outputformat.fmt.pix.width,
      outputformat.fmt.pix.height, outputformat.fmt.pix.sizeimage);

  height = outputformat.fmt.pix.height;
  width = (image_width * height) / image_height;

  format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  format.fmt.pix.width = width;
  format.fmt.pix.height = height;

  outputformat = format;

  if (ioctl (dec->videofd, VIDIOC_TRY_FMT, &outputformat) < 0)
    goto error_try_fmt;

  /* We cannot handle at ffmpeg level bytesperline different from width
   * for NVxx formats, move to software decode.
   */
  if (outputformat.fmt.pix.bytesperline != outputformat.fmt.pix.width)
    goto error_width;

  /* NOTE: format.fmt.pix.sizeimage must be set before a VIDIOC_S_FMT */
  format.fmt.pix.sizeimage = outputformat.fmt.pix.sizeimage;
  /* Set the pixelformat suggested by VIDIOC_TRY_FMT (dst) */
  format.fmt.pix.pixelformat = outputformat.fmt.pix.pixelformat;

  /* NOTE: VIDIOC_S_FMT takes the original format (desired dimensions), not outputformat! */
  if (ioctl (dec->videofd, VIDIOC_S_FMT, &format) < 0)
    goto error_set_fmt;

  if (!gst_hwjpeg_dec_alloc_v4l2_buf (dec, &dec->outputbuf,
          V4L2_BUF_TYPE_VIDEO_CAPTURE))
    goto error_alloc_v4l2_buf;

  dec->output_addr = mmap (NULL, dec->outputbuf.length, PROT_READ,
      MAP_SHARED, dec->videofd, dec->outputbuf.m.offset);

  if ((dec->output_addr == MAP_FAILED) || (dec->output_addr == NULL))
    goto error_mmap;

  *output_format = outputformat;
  dec->image_size = outputformat.fmt.pix.sizeimage;
  dec->width = outputformat.fmt.pix.width;
  dec->height = outputformat.fmt.pix.height;

  return TRUE;

error_get_fmt:
  GST_ERROR_OBJECT (dec, "failed in VIDIOC_G_FMT - %s", strerror (errno));
  return FALSE;
error_try_fmt:
  GST_ERROR_OBJECT (dec, "failed in VIDIOC_TRY_FMT - %s", strerror (errno));
  return FALSE;
error_width:
  GST_ERROR_OBJECT (dec, "bytesperline %d != %d width",
      outputformat.fmt.pix.bytesperline, outputformat.fmt.pix.width);
  return FALSE;
error_set_fmt:
  GST_ERROR_OBJECT (dec, "failed in VIDIOC_S_FMT - %s", strerror (errno));
  return FALSE;
error_alloc_v4l2_buf:
  GST_ERROR_OBJECT (dec, "failed to allocate v4l2 buf");
  return FALSE;
error_mmap:
  GST_ERROR_OBJECT (dec, "failed in mmap");
  return FALSE;
}

static gboolean
gst_hwjpeg_dec_decode_image (GstHWJpegDec * dec)
{
  struct v4l2_format outputformat;
  struct v4l2_format format;
  struct fmt *outputfmt;

  /* qbuf input buffer */
  if (ioctl (dec->videofd, VIDIOC_QBUF, &dec->inputbuf) < 0)
    goto error_qbuf;

  /* allocate output buffer */
  if (!gst_hwjpeg_dec_alloc_output_buf (dec, &outputformat))
    goto error_alloc_output_buf;

  /* qbuf output buffer */
  if (ioctl (dec->videofd, VIDIOC_QBUF, &dec->outputbuf) < 0)
    goto error_qbuf;

  /* block until decode finishes */
  if (ioctl (dec->videofd, VIDIOC_DQBUF, &dec->outputbuf) < 0)
    goto error_dqbuf;

  memset (&format, 0, sizeof (format));
  format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl (dec->videofd, VIDIOC_G_FMT, &format) < 0)
    goto error_get_fmt;

  outputfmt = getfmt (format.fmt.pix.pixelformat);
  if (!outputfmt)
    goto error_output_fmt;

  GST_INFO_OBJECT (dec, "output picture is %dx%d (pitch: %d) %s\n",
      format.fmt.pix.width,
      format.fmt.pix.height, format.fmt.pix.bytesperline, outputfmt->desc);

  dec->fourcc = outputfmt->fourcc;

  return TRUE;

error_qbuf:
  GST_ERROR_OBJECT (dec, "failed in VIDIOC_QBUF - %s", strerror (errno));
  return FALSE;
error_alloc_output_buf:
  GST_ERROR_OBJECT (dec, "failed to allocate output v4l2 buf");
  return FALSE;
error_dqbuf:
  GST_ERROR_OBJECT (dec, "failed in VIDIOC_DQBUF - %s", strerror (errno));
  return FALSE;
error_get_fmt:
  GST_ERROR_OBJECT (dec, "failed in VIDIOC_G_FMT - %s", strerror (errno));
  return FALSE;
error_output_fmt:
  GST_ERROR_OBJECT (dec, "failed to get output fmt");
  return FALSE;
}

static GstFlowReturn
gst_hwjpeg_dec_push_buf (GstHWJpegDec * dec)
{
  gchar format_string[256] = { '\0' };
  GstBuffer *outbuf = NULL;
  GstStructure *s = NULL;
  GstCaps *c = NULL;
  GstMapInfo info_write;

  /* create format string */
  switch (dec->fourcc) {
    case V4L2_PIX_FMT_NV12:
      g_snprintf (format_string, sizeof (format_string), "NV12");
      break;
    case V4L2_PIX_FMT_NV16:
      g_snprintf (format_string, sizeof (format_string), "NV16");
      break;
    case V4L2_PIX_FMT_NV24:
      g_snprintf (format_string, sizeof (format_string), "NV24");
      break;
    default:
      /* unsupported format */
      goto error_format;
      break;
  }

  /* create caps structute */
  s = gst_structure_new ("video/x-raw",
      "width", G_TYPE_INT, dec->width,
      "height", G_TYPE_INT, dec->height,
      "framerate", GST_TYPE_FRACTION, 25, 1,
      "format", G_TYPE_STRING, format_string,
      "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1, NULL);

  /* create caps */
  c = gst_caps_new_full (s, NULL);
  outbuf = gst_buffer_new_and_alloc (dec->image_size);
  if (outbuf == NULL)
    goto error_outbuf_alloc;

  GST_BUFFER_TIMESTAMP (outbuf) = GST_CLOCK_TIME_NONE;
  GST_BUFFER_DURATION (outbuf) = GST_CLOCK_TIME_NONE;

  gst_buffer_map (outbuf, &info_write, GST_MAP_WRITE);
  memcpy (info_write.data, dec->output_addr, info_write.size);
  gst_buffer_unmap (outbuf, &info_write);

  /* Since image decode is done and decoded image is copied
     into user buffer so before pushing buffer downstream,
     dealloc the memory which was requested for image decode
     since stdisplaysink will also try to get buffers from kernel
     and if we dont free here, then stdisplaysink may fail to get
     free memory. */
  if (!gst_hwjpeg_dec_dealloc (dec, &dec->input_addr, dec->inputbuf.length,
          &dec->output_addr, dec->outputbuf.length)) {
    GST_WARNING_OBJECT (dec,
        "Failed to de-allocate memory, try to render data");
    /* inform application also as warning, in case
       if application wants to take a decision on it
     */
    GST_ELEMENT_WARNING (dec, RESOURCE, CLOSE, ("memory could not freed"),
        GST_ERROR_SYSTEM);
  }

  /* push new caps */
  gst_pad_push_event (dec->srcpad, gst_event_new_caps (c));
  /* free caps */
  gst_caps_unref (c);

  /* push segment event */
  gst_pad_push_event (dec->srcpad, gst_event_new_segment (&dec->segment));
  /* push data to display */
  return gst_pad_push (dec->srcpad, outbuf);

error_format:
  GST_ERROR_OBJECT (dec, "format is not supported");
  return GST_FLOW_ERROR;
error_outbuf_alloc:
  GST_ERROR_OBJECT (dec, "failed to allocate output buf");
  gst_caps_unref (c);
  return GST_FLOW_ERROR;
}

void
gst_hwjpeg_dec_tag_list (const GstTagList * list, const gchar * tag,
    gpointer user_data)
{
  GstHWJpegDec *dec = GST_HWJPEG_DEC (user_data);
  int count, i;

  count = gst_tag_list_get_tag_size (list, tag);
  for (i = 0; !dec->multipart_container && i < count; i++) {
    if (!strcmp (tag, "container-format")) {
      char *str;
      if (gst_tag_list_get_string (list, tag, &str)) {
        if (!strcmp (str, "Multipart")) {
          dec->multipart_container = TRUE;
          /* buffer image size will be set in decode chain */
          dec->image_size = 0;
        }
        g_free (str);
      }
    }
  }
}

static gboolean
gst_hwjpeg_dec_sink_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
  GstFlowReturn ret = GST_FLOW_OK;
  GstHWJpegDec *dec = GST_HWJPEG_DEC (parent);

  GST_INFO_OBJECT (dec, "event : %s", GST_EVENT_TYPE_NAME (event));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_SEGMENT:
    {
      const GstSegment *segment;
      gst_event_parse_segment (event, &segment);
      gst_segment_copy_into (segment, &dec->segment);
      gst_event_unref (event);

      return TRUE;
    }

    case GST_EVENT_EOS:
    {
      if (!dec->multipart_container) {
        /* decode Image */
        if (!gst_hwjpeg_dec_decode_image (dec))
          goto error;

        /* push decoded data to display */
        ret = gst_hwjpeg_dec_push_buf (dec);
      }
      break;
    }

    case GST_EVENT_TAG:
    {
      GstTagList *list;
      gst_event_parse_tag (event, &list);
      gst_tag_list_foreach (list, gst_hwjpeg_dec_tag_list, parent);
      break;
    }

    case GST_EVENT_STREAM_START:
    {
      if (dec->image_size <= 0) {
        GstPad *peer;

        /* Get the end position of the stream */
        peer = gst_pad_get_peer (dec->sinkpad);
        if (peer) {
          gst_pad_query_duration (peer, GST_FORMAT_BYTES, &dec->image_size);
          gst_object_unref (peer);
        }
      }
      break;
    }

    default:
      break;
  }

  if (ret == GST_FLOW_OK) {
    return gst_pad_event_default (pad, parent, event);
  } else {
    return FALSE;
  }

error:
  if (!gst_hwjpeg_dec_dealloc (dec, &dec->input_addr, dec->inputbuf.length,
          &dec->output_addr, dec->outputbuf.length)) {
    /* could not dealloc memory so close the device
       to free it. */
    if (dec->videofd > -1) {
      close (dec->videofd);
      dec->videofd = -1;
    }
  }
  gst_hwjpeg_send_msg (dec);

  return FALSE;
}

/*
 function: gst_hwjpeg_dec_dealloc
 input params:
           dec - sthwjpegdec context
           input_addr - mapped address of src buf
           src_length - size of src buf
           output_addr - mapped address of dst buf
           dst_length - size of dst buf
 description : will deallocate memory resources which were
               allocated for source and destination buffers
               for image decode.
 return : TRUE if successful else FALSE.
 */
static gboolean
gst_hwjpeg_dec_dealloc (GstHWJpegDec * dec, void **input_addr, gint src_length,
    void **dst_vaddr, gint dst_length)
{
  gint type;

  if (dec->videofd < 0) {
    /* video device not opened */
    GST_ERROR_OBJECT (dec, "device not opened yet");
    return FALSE;
  }

  if (*input_addr) {
    /* unmap source buffer memory */
    munmap (*input_addr, src_length);
    *input_addr = NULL;

    type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    if (ioctl (dec->videofd, VIDIOC_STREAMOFF, &type) < 0) {
      GST_ERROR_OBJECT (dec, "Failed VIDIOC_STREAMOFF");
      return FALSE;
    }

    gst_hwjpeg_dec_dealloc_v4l2_buf (dec, type);
  }

  if (*dst_vaddr) {
    /* unmap destination buffer memory */
    munmap (*dst_vaddr, dst_length);
    *dst_vaddr = NULL;

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl (dec->videofd, VIDIOC_STREAMOFF, &type) < 0) {
      GST_ERROR_OBJECT (dec, "Failed VIDIOC_STREAMOFF");
      return FALSE;
    }

    gst_hwjpeg_dec_dealloc_v4l2_buf (dec, type);
  }

  return TRUE;
}

/*
 function: gst_hwjpeg_dec_reset_params
 input params:
           dec - sthwjpegdec context
 description : reset hwjpegdec params
 return : void
 */
static void
gst_hwjpeg_dec_reset_params (GstHWJpegDec * dec)
{
  dec->input_addr = NULL;
  dec->offset = 0;
  dec->image_size = 0;
  dec->width = 0;
  dec->height = 0;
  dec->fourcc = 0;
}

static GstStateChangeReturn
gst_hwjpeg_dec_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;;
  GstHWJpegDec *dec = (GstHWJpegDec *) element;

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      /* setup adapter for jpeg image source data */
      dec->videofd = v4l2_open_by_name (V4L2_HWJPEG_DRIVER_NAME,
          V4L2_HWJPEG_CARD_NAME, O_RDWR);
      if (dec->videofd < 0) {
        GST_ERROR_OBJECT (dec, "couldn't open <%s %s>!\n",
            V4L2_HWJPEG_DRIVER_NAME, V4L2_HWJPEG_CARD_NAME);
        return GST_STATE_CHANGE_FAILURE;
      }
      gst_hwjpeg_dec_reset_params (dec);
      break;
    default:
      break;
  }


  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
  if (ret != GST_STATE_CHANGE_SUCCESS) {
    return ret;
  }

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      gst_hwjpeg_dec_reset_params (dec);
      /* free if any memory */
      gst_hwjpeg_dec_dealloc (dec, &dec->input_addr, dec->inputbuf.length,
          &dec->output_addr, dec->outputbuf.length);
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      if (dec->videofd > -1) {
        close (dec->videofd);
        dec->videofd = -1;
      }
      break;
    default:
      break;
  }

  return ret;
}

gboolean
sthwjpegdec_init (GstPlugin * plugin)
{

  return gst_element_register (plugin, "sthwjpegdec",
      (GST_RANK_PRIMARY + 9), GST_TYPE_HWJPEG_DEC);
}
