/* Gstreamer ST Audio-Decoder Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STAUDIO_H__
#define __GST_STAUDIO_H__

#include <gst/gst.h>
#include <gst/base/gstadapter.h>
#include <linux/dvb/audio.h>
#include <linux/videodev2.h>

G_BEGIN_DECLS
/* #defines don't like whitespacey bits */
#define GST_TYPE_STAUDIO (gst_staudio_get_type())
#define GST_STAUDIO(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_STAUDIO,Gststaudio))
#define GST_STAUDIO_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_STAUDIO,GststaudioClass))
#define GST_IS_STAUDIO(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_STAUDIO))
#define GST_IS_STAUDIO_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_STAUDIO))
#define GST_STAUDIO_AUDIO_LOOP_GET_COND(auddec)  (&((Gststaudio *)auddec)->dummy_buff.cond)
#define GST_STAUDIO_AUDIO_LOOP_WAIT(auddec)      (g_cond_wait (GST_STAUDIO_AUDIO_LOOP_GET_COND (auddec), &auddec->dummy_buff.mutex))
#define GST_STAUDIO_AUDIO_LOOP_SIGNAL(auddec) (g_cond_signal (GST_STAUDIO_AUDIO_LOOP_GET_COND (auddec)))
/* total audio device available are 25 on target audio0 .. audio25 */
#define MAX_NUM_DEV 25
/* time in mseconds */
#define HUGE_TIME_DIFF_MS (30 * 1000)
typedef struct _Gststaudio Gststaudio;
typedef struct _GststaudioClass GststaudioClass;

typedef enum
{
  GST_STAUDIO_TASK_IDLE = 0,
  GST_STAUDIO_TASK_STARTED,
  GST_STAUDIO_TASK_PAUSED,
  GST_STAUDIO_TASK_STOPPED
} GstStAudioTaskState;

typedef enum
{
  GST_STAUDIO_INVALID,
  GST_STAUDIO_MMAP,
  GST_STAUDIO_USER_PTR
} GstStAudioCaptureMemType;

typedef struct
{
  GCond cond;
  GMutex mutex;
} GstStAudCond;

struct _Gststaudio
{
  GstElement element;

  GstPad *sinkpad;
  GstPad *srcpad;

  GstSegment segment;
  gboolean new_segment_received;        /* Used to keep track whenever NEWSEGMENT event is received */
  gboolean first_new_segment;   /* first new segment event */
  gboolean first_buffer_received;       /* first audio buffer of stream received */

  gint dev_id;                  /* The dev-id opened for staudio instance */
  /* Set the value of dev_id set by the application in dev_id_prop, rather than dev_id, so that if re-negotiation of dev-id is needed
   * because of track change, the dev-id value can be negotiated by manage_dev_id, if dev_id_prop is not set by app */
  gint dev_id_prop;             /* The dev-id set as a property */
  gint fd;                      /* Audio codec file descriptor      */
  gint v4l2_fd;
  gint prev_codec;              /* previous codec type */
  gint codec;                   /* Audio codec                      */
  audio_play_state_t state;     /* Audio codec current state        */
  gboolean first_frame_pushed;
  gboolean first_injection;     /* First injection into audio codec */
  gboolean is_pes;
  gboolean disable_avsync;
  gboolean disable_dev;         /* Used to manage device state machine internally */
  gboolean property_disable_dev;        /* Used to store what user has requested */
  GstBuffer *codec_data;
  GstAdapter *adapter;
  gdouble rate;                 /* Trickmode current rate */
  gint64 current_time;
  guint64 currentPTS;           /* Current PTS value */
  guint64 prev_audio_pts;
  guint clock_data_flags;
  gboolean flush;
  gboolean tunneled;
  GstFlowReturn srcresult;
  guint channel_config;         /* Channel configuration : stereo, mono left,
                                   mono right, mono, stereo swapped */
  GstCaps *prev_caps;
  gboolean live;
  guint latency;
  gboolean discontinuity;
  gboolean eos;
  gboolean eos_set;             /* TRUE if eos discontinuity has been set on SE */
  gboolean is_active;           /* Used to indicate if staudio instance is active or not, as per InputSelector custom upstream event */
  gboolean flushing_seek;       /* A sticky flag to check that staudio received a flushing seek */

  gboolean device_config;
  GList *queued_events_list;    /* List of Queued Serialized Events */
  GstClockTime last_buffer_ts;  /* Timestamp (in ms) of last buffer received in chain function */
  gboolean mute;                /* Audio is mute or not */
  gint application_type;
  gint service_type;
  guint channel_num;
  GMutex event_lock;            /* lock for event queue/dequeue */
  gboolean set_avsync;          /* current av sync value */
  gboolean channel_change;      /* channel change or not */
  gboolean ptswrap;             /* pts is wrapped or not */
  gboolean update_segment;      /* segment to be updated or not */

  /* Audio properties */
  struct
  {
    gint depth;                 /* Audio bits per sample    */
    gint sampling_freq;         /* Audio sampling frequency */
    gint num_chans;             /* Audio number of channels */
    gint bitrate;               /* Audio bitrate            */
    gint endianness;            /* Audio endianess          */
    gboolean dvd_mode;          /* Audio dvd format         */
  } properties;

  /* Capture */
  struct
  {
    gint depth;
    gint sample_rate;
    gint channels;
    gboolean first_buffer;      /* Used to keep track of the first buffer (non-tunneled case) */

    GstBuffer *buffer;
    GstBufferPool *pool;
    GstStAudioCaptureMemType mem_type;  /* Used to store the type of Capture Buffer: V4L2_MEMORY_MMAP or V4L2_MEMORY_USERPTR    */
    gboolean is_stream_on_done; /* Used to check whether stream on is done */
    struct v4l2_buffer v4l2_capture_buf;        /* Used to store Capture v4l2 buffer when REQBUFS is done for V4L2_MEMORY_MMAP  */
    void *buf_start_address;    /* Used to store mapped capture buffer start address when Capture REQBUFS is done for V4L2_MEMORY_MMAP */
  } capture;

  void *priv_data;
  GMutex write_lock;            /* mutex lock to protect write */
  gboolean exit_loop;           /* flag to exit the queue packet loop */

  GstTask *task;
  GRecMutex task_lock;

  GstTask *audio_task;          /* audio task */
  GstStAudioTaskState audio_task_state; /* audio loop task state */
  /* condition used in audio loop in tunneled playback */
  GstStAudCond dummy_buff;
};

struct _GststaudioClass
{
  GstElementClass parent_class;

  /* structure to contain active audio and lock per device id */
  struct
  {
    /* variable to point the active staudio element which opens the audio device */
    Gststaudio *audio;
    GMutex lock;                /* mutex lock to protect audio variable */
    gboolean mute;              /* audio on this dev id is mute/unmute */
  } active[MAX_NUM_DEV];
  guint64 first_audio_pts;      /* first frame pts, updated by first active element */
};

GType gst_staudio_get_type (void);

gboolean staudio_init (GstPlugin * plugin);

G_END_DECLS
#endif /* __GST_STAUDIO_H__ */
