/* Gstreamer Audio-Capture
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:element-staudcapture
 *
 * FIXME:Describe staudcapture here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch filesrc location=<file> ! avidemux name=d d. ! queue ! stvideo ! staudcapture ! tee name=t t. ! ximagesink sync=0 async=0 t. ! stvideosink w=400 h= 400 x=700 y=300 async=0 sync=0
 * gst-launch filesrc location=<file> ! avidemux name=d d. ! queue ! stvideo ! staudcapture ! ximagesink sync=0 async=0
 * ]|
 * </refsect2>
 */

#include <gst/gst.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>

#include <linux/videodev2.h>
#include "linux/dvb/dvb_v4l2_export.h"
#include "linux/stm/stmedia_export.h"
#include "v4l2_utils.h"
#include "gststaudcapture.h"
#include "gststencode.h"
#include <linux/dvb/dvb_v4l2_export.h>


GST_DEBUG_CATEGORY_STATIC (gst_staudcapture_debug);
#define GST_CAT_DEFAULT gst_staudcapture_debug

/* audcapt_elem signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_DEV_ID,
  PROP_LAST
};

#define MIN_DEV_ID          0
#define MAX_DEV_ID          7
#define DEFAULT_DEV_ID      3

#define DEFAULT_BITS_PER_SAMPLE 32
#define DEFAULT_SAMPLE_RATE 48000
#define DEFAULT_CHANNELS 8
#define MAX_SAMPLES_NUMBER 2048
#define DEFAULT_SIZE (MAX_SAMPLES_NUMBER*DEFAULT_CHANNELS*DEFAULT_BITS_PER_SAMPLE/8)


/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("ANY")
    );

/* REVIEW/CRITICAL : Use format for audio caps (S32LE in this case) */
static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-raw, "
        "depth = (int) 32, "
        "width = (int) 32, "
        "channels = (int) 8, "
        "endianness = (int) LITTLE_ENDIAN, "
        "signed = (boolean) true, " "rate = (int) [ 1, MAX ]; ")
    );

#define gst_staudcapture_parent_class parent_class
G_DEFINE_TYPE (Gststaudcapture, gst_staudcapture, GST_TYPE_ELEMENT);

static void gst_staudcapture_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_staudcapture_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static void gst_staudcapture_finalize (GObject * object);

static GstStateChangeReturn gst_staudcapture_change_state (GstElement * element,
    GstStateChange transition);
static gboolean gst_staudcapture_set_caps (Gststaudcapture * audcapt_elem,
    GstCaps * caps);
static GstFlowReturn gst_staudcapture_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buf);
static gboolean gst_staudcapture_sink_pad_event (GstPad * pad,
    GstObject * parent, GstEvent * event);

static void gst_staudcapture_thread (Gststaudcapture * audcapt_elem);
static gboolean gst_staudcapture_setup_device (Gststaudcapture * audcapt_elem);
static gboolean gst_staudcapture_queue_buffer (Gststaudcapture * audcapt_elem);
static gboolean gst_staudcapture_dequeue_buffer (Gststaudcapture *
    audcapt_elem);
static gboolean gst_staudcapture_stream_on (Gststaudcapture * audcapt_elem,
    int fd);
static gboolean gst_staudcapture_stream_off (Gststaudcapture * audcapt_elem,
    int fd);

/* initialize the staudcapture's class */
static void
gst_staudcapture_class_init (GststaudcaptureClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *element_class;

  gobject_class = (GObjectClass *) klass;

  gobject_class->set_property = gst_staudcapture_set_property;
  gobject_class->get_property = gst_staudcapture_get_property;
  gobject_class->finalize = gst_staudcapture_finalize;
  element_class = GST_ELEMENT_CLASS (klass);

  gst_element_class_set_static_metadata (element_class,
      "ST Audio capture",
      "Generic", "GStreamer Audio Capture Element for ST", "http://www.st.com");

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_factory));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_factory));

  element_class->change_state = gst_staudcapture_change_state;

  g_object_class_install_property (gobject_class, PROP_DEV_ID,
      g_param_spec_int ("dev-id", "device id for video", "device id for video",
          MIN_DEV_ID, MAX_DEV_ID, DEFAULT_DEV_ID,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));
}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
static void
gst_staudcapture_init (Gststaudcapture * audcapt_elem)
{
  GST_DEBUG_OBJECT (audcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  gst_segment_init (&audcapt_elem->segment, GST_FORMAT_TIME);

  audcapt_elem->v4l2_fd = -1;
  audcapt_elem->dev_id = DEFAULT_DEV_ID;
  audcapt_elem->audiosinkbuffer = NULL;
  audcapt_elem->numframe = 0;
  audcapt_elem->sample_rate = DEFAULT_SAMPLE_RATE;
  audcapt_elem->channels = DEFAULT_CHANNELS;
  audcapt_elem->depth = DEFAULT_BITS_PER_SAMPLE;
  audcapt_elem->srcresult = GST_FLOW_OK;

  audcapt_elem->sinkpad =
      gst_pad_new_from_static_template (&sink_factory, "sink");
  gst_pad_set_chain_function (audcapt_elem->sinkpad,
      GST_DEBUG_FUNCPTR (gst_staudcapture_chain));
  gst_pad_set_event_function (audcapt_elem->sinkpad,
      GST_DEBUG_FUNCPTR (gst_staudcapture_sink_pad_event));

  audcapt_elem->srcpad = gst_pad_new_from_static_template (&src_factory, "src");

  gst_element_add_pad (GST_ELEMENT (audcapt_elem), audcapt_elem->sinkpad);
  gst_pad_set_active (audcapt_elem->srcpad, TRUE);
  gst_element_add_pad (GST_ELEMENT (audcapt_elem), audcapt_elem->srcpad);

  /* Create the capture thread */
  g_rec_mutex_init (&audcapt_elem->task_lock);
  audcapt_elem->task =
      gst_task_new ((GstTaskFunction) gst_staudcapture_thread, audcapt_elem,
      NULL);
  gst_object_set_name (GST_OBJECT_CAST (audcapt_elem->task), "SMF-AudCapture");
  gst_task_set_lock (audcapt_elem->task, &audcapt_elem->task_lock);
}

static void
gst_staudcapture_finalize (GObject * object)
{
  Gststaudcapture *audcapt_elem = GST_STAUDCAPTURE (object);

  gst_task_stop (audcapt_elem->task);
  /* Wait for the task to stop */
  gst_task_join (audcapt_elem->task);
  gst_object_unref (audcapt_elem->task);
  audcapt_elem->task = NULL;
  g_rec_mutex_clear (&audcapt_elem->task_lock);

  G_OBJECT_CLASS (parent_class)->finalize (object);

  return;
}

static void
gst_staudcapture_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  Gststaudcapture *audcapt_elem = GST_STAUDCAPTURE (object);

  switch (prop_id) {
    case PROP_DEV_ID:
      audcapt_elem->dev_id = g_value_get_int (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_staudcapture_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  Gststaudcapture *audcapt_elem = GST_STAUDCAPTURE (object);

  switch (prop_id) {
    case PROP_DEV_ID:
      g_value_set_int (value, audcapt_elem->dev_id);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

/* GstElement vmethod implementations */

static GstStateChangeReturn
gst_staudcapture_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
  Gststaudcapture *audcapt_elem = (Gststaudcapture *) element;

  GST_DEBUG_OBJECT (audcapt_elem, "%d -> %d",
      GST_STATE_TRANSITION_CURRENT (transition),
      GST_STATE_TRANSITION_NEXT (transition));

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      /* Open device */
      if (audcapt_elem->v4l2_fd < 0) {
        audcapt_elem->v4l2_fd =
            v4l2_open_by_name (V4L2_CAPTURE_DRIVER_NAME, V4L2_CAPTURE_CARD_NAME,
            O_RDWR);

        if (audcapt_elem->v4l2_fd < 0) {
          GST_ERROR_OBJECT (audcapt_elem,
              "Couldn't open v4l2 device %s - %s", V4L2_CAPTURE_DRIVER_NAME,
              strerror (errno));
          return GST_STATE_CHANGE_FAILURE;
        }
      }
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      gst_task_pause (audcapt_elem->task);
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      gst_staudcapture_stream_off (audcapt_elem, audcapt_elem->v4l2_fd);
      /* Close device */
      if (audcapt_elem->v4l2_fd != -1) {
        close (audcapt_elem->v4l2_fd);
        audcapt_elem->v4l2_fd = -1;
      }
      break;

    default:
      break;
  }

  return ret;
}


/* this function handles the link with other elements */
static gboolean
gst_staudcapture_set_caps (Gststaudcapture * audcapt_elem, GstCaps * caps)
{
  gboolean ret = FALSE;
  GstCaps *srccaps;
  GstStructure *srcstructure;

  /* Due to bug #16975 in the streaming engine,
     audio grab configuration always returns fixed configuration.
     Please keep this until that bug is closed.
   */
#if 0
  const GstStructure *structure;
  structure = gst_caps_get_structure (caps, 0);

  if (gst_structure_get_uint (structure, "rate", &audcapt_elem->sample_rate)) {
    GST_DEBUG_OBJECT (audcapt_elem, "Samples per second: %d",
        audcapt_elem->sample_rate);
  }

  if (gst_structure_get_uint (structure, "channels", &audcapt_elem->channels)) {
    GST_DEBUG_OBJECT (audcapt_elem, "Channels number: %d",
        audcapt_elem->channels);
  }

  if (gst_structure_get_uint (structure, "depth", &audcapt_elem->depth)) {
    GST_DEBUG_OBJECT (audcapt_elem, "Bits per sample: %d", audcapt_elem->depth);
  }
#endif

  /* REVIEW/CRITICAL : Use format field for cap (instead of depth, width,...) */
  if (gst_pad_get_current_caps (audcapt_elem->srcpad) == NULL) {
    srcstructure = gst_structure_new ("audio/x-raw",
        "rate", G_TYPE_INT, audcapt_elem->sample_rate,
        "channels", G_TYPE_INT, audcapt_elem->channels,
        "depth", G_TYPE_INT, audcapt_elem->depth,
        "width", G_TYPE_INT, 32,
        "endianness", G_TYPE_INT, G_LITTLE_ENDIAN,
        "signed", G_TYPE_BOOLEAN, TRUE,
        "format", G_TYPE_STRING, "S32LE",
        "layout", G_TYPE_STRING, "interleaved",
        "channel-mask", GST_TYPE_BITMASK, (guint64) 0, NULL);

    srccaps = gst_caps_new_full (srcstructure, NULL);

    if (gst_staudcapture_setup_device (audcapt_elem)) {
      if (gst_staudcapture_stream_on (audcapt_elem, audcapt_elem->v4l2_fd)) {
        gst_task_start (audcapt_elem->task);
        ret = TRUE;
      }
    } else {
      GST_ELEMENT_ERROR (audcapt_elem, RESOURCE, SETTINGS,
          ("Failed to setup staudcapture device %d", audcapt_elem->dev_id),
          GST_ERROR_SYSTEM);
      ret = FALSE;
    }
    gst_pad_use_fixed_caps (audcapt_elem->srcpad);
    gst_pad_set_caps (audcapt_elem->srcpad, srccaps);
    gst_caps_unref (srccaps);
  }

  return ret;
}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_staudcapture_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
  Gststaudcapture *audcapt_elem = GST_STAUDCAPTURE (parent);

  GST_LOG_OBJECT (audcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  if (audcapt_elem->srcresult != GST_FLOW_OK) {
    GST_ERROR_OBJECT (audcapt_elem, "error in data flow %s ",
        gst_flow_get_name (audcapt_elem->srcresult));
  }

  gst_buffer_unref (buf);

  /* just push out the incoming buffer without touching it */
  return audcapt_elem->srcresult;
}


static gboolean
gst_staudcapture_sink_pad_event (GstPad * pad, GstObject * parent,
    GstEvent * event)
{
  Gststaudcapture *audcapt_elem = GST_STAUDCAPTURE (parent);
  gboolean ret = TRUE;
  GstCaps *caps;
  const GstSegment *in_segment;

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_EOS:
      GST_DEBUG_OBJECT (audcapt_elem,
          "EOS event received = entering the end of capture loop");

      gst_event_unref (event);
      goto done;
      break;
    case GST_EVENT_CAPS:
      gst_event_parse_caps (event, &caps);
      if ((audcapt_elem->prev_caps != NULL)
          && (gst_caps_is_equal ((const GstCaps *) audcapt_elem->prev_caps,
                  (const GstCaps *) caps))) {
        GST_DEBUG_OBJECT (audcapt_elem, "caps are already set");
        return TRUE;
      }
      ret = gst_staudcapture_set_caps (audcapt_elem, caps);
      if (ret) {
        if (audcapt_elem->prev_caps) {
          gst_caps_unref (audcapt_elem->prev_caps);
        }
        audcapt_elem->prev_caps = gst_caps_ref (caps);
      } else {
        GST_ERROR_OBJECT (audcapt_elem, "failed to set caps!");
      }
      gst_event_unref (event);
      break;
    case GST_EVENT_SEGMENT:
      gst_event_parse_segment (event, &in_segment);
      if (in_segment->format == GST_FORMAT_TIME) {
        GST_DEBUG_OBJECT (audcapt_elem, "New segment rate %f "
            "start %" GST_TIME_FORMAT " stop %" GST_TIME_FORMAT
            " position %" GST_TIME_FORMAT, in_segment->rate,
            GST_TIME_ARGS (in_segment->start), GST_TIME_ARGS (in_segment->stop),
            GST_TIME_ARGS (in_segment->position));

        audcapt_elem->segment.rate = in_segment->rate;
        audcapt_elem->segment.applied_rate = in_segment->applied_rate;
        audcapt_elem->segment.format = in_segment->format;
        audcapt_elem->segment.start = in_segment->start;
        audcapt_elem->segment.stop = in_segment->stop;
        audcapt_elem->segment.position = in_segment->position;
      } else {
        GST_DEBUG_OBJECT (audcapt_elem, "New segment rate %f "
            "start %" GST_TIME_FORMAT " stop %" GST_TIME_FORMAT
            " position %" GST_TIME_FORMAT, in_segment->rate,
            GST_TIME_ARGS (in_segment->start), GST_TIME_ARGS (in_segment->stop),
            GST_TIME_ARGS (in_segment->position));
        audcapt_elem->segment.rate = 1;
        audcapt_elem->segment.format = GST_FORMAT_TIME;
        audcapt_elem->segment.start = 0;
        audcapt_elem->segment.stop = GST_CLOCK_TIME_NONE;
        audcapt_elem->segment.time = 0;
        gst_pad_push_event (audcapt_elem->srcpad,
            gst_event_new_segment ((const GstSegment *)
                &audcapt_elem->segment));
        gst_event_unref (event);
        goto done;
      }
      break;
    default:
      break;
  }
  /* Push event downstream */
  ret = gst_pad_event_default (pad, parent, event);
done:
  return ret;
}

static gboolean
gst_staudcapture_setup_device (Gststaudcapture * audcapt_elem)
{
  struct v4l2_requestbuffers reqbuf;
  struct v4l2_format fmt;
#if 0
  struct v4l2_audio_format *audio_fmt;
#endif
  struct v4l2_input v4l2input;
  gchar *linuxdvb_input;
  GstQuery *query;
  GstBufferPool *pool;
  guint size, min, max;
  gint index = 0;

  GST_LOG_OBJECT (audcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  /* Choose the LinuxDVB decoder input to use according to the dev-id property        */
  linuxdvb_input = g_strdup_printf ("dvb0.audio%01d", audcapt_elem->dev_id);
  GST_INFO_OBJECT (audcapt_elem, "DVB Input = %s", linuxdvb_input);

  /* Configure capture input */
  memset (&v4l2input, 0, sizeof (struct v4l2_input));
  for (v4l2input.index = 0;; v4l2input.index++) {
    if (ioctl (audcapt_elem->v4l2_fd, VIDIOC_ENUMAUDIO, &v4l2input) < 0)
      break;

    if (strcmp ((char *) v4l2input.name, linuxdvb_input) == 0) {
      index = v4l2input.index;
      break;
    }
  }

  if (ioctl (audcapt_elem->v4l2_fd, VIDIOC_S_AUDIO, &index) < 0) {
    GST_ERROR_OBJECT (audcapt_elem, "Couldn't configure capture input to %s",
        linuxdvb_input);
    return FALSE;
  }

  GST_INFO_OBJECT (audcapt_elem, "Selected LinuxDVB outputs %d (%s)", index,
      linuxdvb_input);
  g_free (linuxdvb_input);

  /* Configure capture ouput */
  memset (&fmt, 0, sizeof (struct v4l2_format));
#if 0
  audio_fmt = (struct v4l2_audio_format *) fmt.fmt.raw_data;
  audio_fmt->BitsPerSample = audcapt_elem->depth;
  audio_fmt->Channelcount = audcapt_elem->channels;
  audio_fmt->SampleRateHz = audcapt_elem->sample_rate;
#endif
  fmt.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;

  if (ioctl (audcapt_elem->v4l2_fd, VIDIOC_S_FMT, &fmt) < 0) {
    GST_ERROR_OBJECT (audcapt_elem, "VIDIOC_S_FMT failed %s", strerror (errno));
    return FALSE;
  }

  /* Capture Buffer formats : VIDIOC_REQBUFS */
  memset (&reqbuf, 0, sizeof (struct v4l2_requestbuffers));
  reqbuf.memory = V4L2_MEMORY_USERPTR;
  reqbuf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;
  reqbuf.count = 1;

  if (ioctl (audcapt_elem->v4l2_fd, VIDIOC_REQBUFS, &reqbuf) < 0) {
    GST_ERROR_OBJECT (audcapt_elem, "VIDIOC_REQBUFS %s", strerror (errno));
    return FALSE;
  }

  /* Query for the pool for keeping the capture data */
  query =
      gst_query_new_allocation (gst_pad_get_current_caps (audcapt_elem->srcpad),
      TRUE);
  if (query) {
    if (gst_pad_peer_query (audcapt_elem->srcpad, query)) {
      gst_query_parse_nth_allocation_pool (query, 0, &pool, &size, &min, &max);
      if (pool != NULL)
        audcapt_elem->pool = pool;
    }
    gst_query_unref (query);
  }

  return TRUE;
}

static gboolean
gst_staudcapture_queue_buffer (Gststaudcapture * audcapt_elem)
{
  GstFlowReturn ret = GST_FLOW_OK;
  struct v4l2_buffer buf;
  struct v4l2_audio_uncompressed_metadata audio_meta;
  GstEncodeBufferMeta *meta;
  GST_LOG_OBJECT (audcapt_elem, "%s:%d", __FUNCTION__, __LINE__);


  /* REVIEW/CRITICAL: Return value from _acquire_buffer is not propagated
   * upstream. */
  ret =
      gst_buffer_pool_acquire_buffer (audcapt_elem->pool,
      &audcapt_elem->audiosinkbuffer, NULL);

  if (ret == GST_FLOW_OK) {
    meta = (GstEncodeBufferMeta *)
        gst_buffer_get_meta (audcapt_elem->audiosinkbuffer,
        GST_ENCODEBUFFER_META_API_TYPE);
    if (audcapt_elem->audiosinkbuffer && (!strcmp (meta->name, "EncodeBuffer"))) {
      GstMapInfo info_read;
      gst_buffer_map (audcapt_elem->audiosinkbuffer, &info_read, GST_MAP_READ);
      memset (&buf, 0, sizeof (struct v4l2_buffer));
      buf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;
      buf.memory = V4L2_MEMORY_USERPTR;
      buf.m.userptr = (unsigned long) info_read.data;
      buf.field = V4L2_FIELD_NONE;
      buf.length = DEFAULT_SIZE;
      buf.reserved = (int) &audio_meta;

      gst_buffer_unmap (audcapt_elem->audiosinkbuffer, &info_read);
      if (ioctl (audcapt_elem->v4l2_fd, VIDIOC_QBUF, &buf) < 0) {
        GST_ERROR_OBJECT (audcapt_elem, "Couldn't queue buffer - %s",
            strerror (errno));
        gst_buffer_unref (audcapt_elem->audiosinkbuffer);
        return FALSE;
      }
    } else {
      GST_ERROR_OBJECT (audcapt_elem, "No buffer allocated");

      return FALSE;
    }
  } else {
    GST_ERROR_OBJECT (audcapt_elem,
        "Failed to allocate buffer, flow error - %s", gst_flow_get_name (ret));

    return FALSE;
  }


  return TRUE;
}

static gboolean
gst_staudcapture_dequeue_buffer (Gststaudcapture * audcapt_elem)
{
  struct v4l2_buffer buf;

  GST_LOG_OBJECT (audcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  memset (&buf, 0, sizeof (struct v4l2_buffer));
  buf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;
  buf.memory = V4L2_MEMORY_USERPTR;

  if (ioctl (audcapt_elem->v4l2_fd, VIDIOC_DQBUF, &buf) < 0) {
    gst_buffer_unref (audcapt_elem->audiosinkbuffer);
    GST_ERROR_OBJECT (audcapt_elem, "Couldn't de-queue user buffer - %s",
        strerror (errno));
    return FALSE;
  }

  if (buf.bytesused == 0) {
    gst_buffer_unref (audcapt_elem->audiosinkbuffer);
    GST_DEBUG_OBJECT (audcapt_elem, "EOS MARKER received");
    gst_task_stop (audcapt_elem->task);
    g_rec_mutex_lock (&audcapt_elem->task_lock);
    g_rec_mutex_unlock (&audcapt_elem->task_lock);
    gst_pad_push_event (audcapt_elem->srcpad, gst_event_new_eos ());
    return TRUE;
  }

  /* Update the buffer size with the returned size */
  gst_buffer_set_size (audcapt_elem->audiosinkbuffer, buf.bytesused);

  /* Set the buffer timestamp to the original pts grabbed from streaming engine */
  GST_BUFFER_TIMESTAMP (audcapt_elem->audiosinkbuffer) =
      GST_TIMEVAL_TO_TIME (buf.timestamp);
  GST_BUFFER_DURATION (audcapt_elem->audiosinkbuffer) = GST_CLOCK_TIME_NONE;

  audcapt_elem->numframe++;

  GST_LOG_OBJECT (audcapt_elem, "Capture time : %lld ms (%ld sec %ld usec)",
      GST_TIME_AS_MSECONDS (GST_BUFFER_TIMESTAMP
          (audcapt_elem->audiosinkbuffer)), buf.timestamp.tv_sec,
      buf.timestamp.tv_usec);

  gst_pad_push_event (audcapt_elem->srcpad,
      gst_event_new_caps (gst_pad_get_current_caps (audcapt_elem->srcpad)));

  /* Push the buffer to downstream element */
  audcapt_elem->srcresult =
      gst_pad_push (audcapt_elem->srcpad, audcapt_elem->audiosinkbuffer);
  if (audcapt_elem->srcresult != GST_FLOW_OK) {
    GST_ERROR_OBJECT (audcapt_elem, "Failed to push buffer downstream - %s",
        gst_flow_get_name (audcapt_elem->srcresult));
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_staudcapture_stream_on (Gststaudcapture * audcapt_elem, int fd)
{
  struct v4l2_buffer buf;

  GST_LOG_OBJECT (audcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  memset (&buf, 0, sizeof (struct v4l2_buffer));
  buf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;

  if (ioctl (fd, VIDIOC_STREAMON, &buf.type) < 0) {
    GST_ELEMENT_ERROR (audcapt_elem, RESOURCE, FAILED,
        ("Failed to start streaming on device %d", audcapt_elem->dev_id),
        ("VIDIOC_STREAMON failed : %s", strerror (errno)));
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_staudcapture_stream_off (Gststaudcapture * audcapt_elem, int fd)
{
  struct v4l2_buffer buf;

  GST_LOG_OBJECT (audcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  memset (&buf, 0, sizeof (struct v4l2_buffer));
  buf.type = V4L2_BUF_TYPE_AUDIO_CAPTURE;

  if (ioctl (fd, VIDIOC_STREAMOFF, &buf.type) < 0) {
    GST_ELEMENT_ERROR (audcapt_elem, RESOURCE, FAILED,
        ("Failed to stop streaming on device %d", audcapt_elem->dev_id),
        ("VIDIOC_STREAMOFF failed : %s", strerror (errno)));
    return FALSE;
  }

  return TRUE;
}

/* ---------------------------------------------------

    due to a bug in the ST V4L capture and the fact we can't
    unlock proprely the dqueue buffer with  a stream off or close()
    the capture is initialized in NOBLOCKING and we have to manage
    the exit of the thread.

    the callabck push a buffer and try to capture it a push it in the bad
    the idle exits for each farme to give a chance to quit the thread cleanly

 --------------------------------------------------*/
static void
gst_staudcapture_thread (Gststaudcapture * audcapt_elem)
{
  /* Push the empty buffer in the queue */
  if (!gst_staudcapture_queue_buffer (audcapt_elem)) {
    GST_ERROR_OBJECT (audcapt_elem, "Could not queue buffer");
    goto error;
  }

  if (!gst_staudcapture_dequeue_buffer (audcapt_elem)) {
    GST_ERROR_OBJECT (audcapt_elem, "Could not dequeue buffer");
    goto error;
  }

  return;

error:
  usleep (1000);

  return;
}

/* entry point to initialize the element */
gboolean
staudcapture_init (GstPlugin * plugin)
{
  GST_DEBUG_CATEGORY_INIT (gst_staudcapture_debug, "staudcapture", 0,
      "ST capture");

  return gst_element_register (plugin, "staudcapture", GST_RANK_PRIMARY + 10,
      GST_TYPE_STAUDCAPTURE);
}
