/* Gstreamer Video-Capture
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:element-stvidcapture
 *
 * FIXME:Describe stvidcapture here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch filesrc location=<file> ! asfdemux ! dev-id=1 ! stvidcapture dev-id=1 height=720 width=1280 ! dfbvideosink
 * ]|
 * </refsect2>
 */

#include <gst/gst.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>

#include <linux/videodev2.h>
#include <gst/video/video.h>
#include "v4l2_utils.h"
#include "gststvidcapture.h"
#include "gststencode.h"
#include <linux/dvb/dvb_v4l2_export.h>


GST_DEBUG_CATEGORY_STATIC (gst_stvidcapture_debug);
#define GST_CAT_DEFAULT gst_stvidcapture_debug

/* vidcapt_elem signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_DEV_ID,
  PROP_LAST
};

enum
{
  NO_MRK = 0,
  SYTEM_TIME_MRK = 1,
  FRAME_INFO_MRK = 2
};

#define MIN_DEV_ID          0
#define MAX_DEV_ID          7
#define DEFAULT_DEV_ID      3

#define DEFAULT_WIDTH       1280
#define DEFAULT_HEIGHT       720

#define DEFAULT_BPP          16
#define DEFAULT_PIXEL_FORMAT V4L2_PIX_FMT_UYVY

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("ANY")
    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-fake-yuv;"
        GST_VIDEO_CAPS_MAKE ("BGRA") ";"
        GST_VIDEO_CAPS_MAKE ("BGRx") ";"
        GST_VIDEO_CAPS_MAKE ("RGB") ";" GST_VIDEO_CAPS_MAKE ("UYVY") ";"
        GST_VIDEO_CAPS_MAKE ("YUYV") ";")
    );

#define gst_stvidcapture_parent_class parent_class
G_DEFINE_TYPE (Gststvidcapture, gst_stvidcapture, GST_TYPE_ELEMENT);

static void gst_stvidcapture_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_stvidcapture_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static void gst_stvidcapture_finalize (GObject * object);

static GstStateChangeReturn gst_stvidcapture_change_state (GstElement * element,
    GstStateChange transition);
static gboolean gst_stvidcapture_set_caps (Gststvidcapture * vidcapt_elem,
    GstCaps * caps);
static GstFlowReturn gst_stvidcapture_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buf);
static gboolean gst_stvidcapture_sink_pad_event (GstPad * pad,
    GstObject * parent, GstEvent * event);

static void gst_stvidcapture_thread (Gststvidcapture * vidcapt_elem);
static gboolean gst_stvidcapture_setup_device (Gststvidcapture * vidcapt_elem);
static gboolean gst_stvidcapture_queue_buffer (Gststvidcapture * vidcapt_elem);
static gboolean gst_stvidcapture_dequeue_buffer (Gststvidcapture *
    vidcapt_elem);
static gboolean gst_stvidcapture_stream_on (Gststvidcapture * vidcapt_elem,
    int fd);
static gboolean gst_stvidcapture_stream_off (Gststvidcapture * vidcapt_elem,
    int fd);

/* initialize the stvidcapture's class */
static void
gst_stvidcapture_class_init (GststvidcaptureClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *element_class;

  gobject_class = (GObjectClass *) klass;
  element_class = GST_ELEMENT_CLASS (klass);

  gobject_class->set_property = gst_stvidcapture_set_property;
  gobject_class->get_property = gst_stvidcapture_get_property;
  gobject_class->finalize = gst_stvidcapture_finalize;

  gst_element_class_set_static_metadata (element_class,
      "ST Video capture",
      "Generic", "GStreamer Video Capture Element for ST", "http://www.st.com");

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_factory));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_factory));

  element_class->change_state = gst_stvidcapture_change_state;

  g_object_class_install_property (gobject_class, PROP_DEV_ID,
      g_param_spec_int ("dev-id", "device id for video", "device id for video",
          MIN_DEV_ID, MAX_DEV_ID, DEFAULT_DEV_ID,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));
}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
static void
gst_stvidcapture_init (Gststvidcapture * vidcapt_elem)
{
  GST_LOG_OBJECT (vidcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  gst_segment_init (&vidcapt_elem->segment, GST_FORMAT_TIME);

  vidcapt_elem->v4l2_fd = -1;
  vidcapt_elem->dev_id = DEFAULT_DEV_ID;
  vidcapt_elem->videosinkbuffer = NULL;
  vidcapt_elem->width = DEFAULT_WIDTH;
  vidcapt_elem->height = DEFAULT_HEIGHT;
  vidcapt_elem->bpp = DEFAULT_BPP;
  vidcapt_elem->pixel_format = DEFAULT_PIXEL_FORMAT;
  vidcapt_elem->numframe = 0;
  vidcapt_elem->pool = NULL;
  vidcapt_elem->srcresult = GST_FLOW_OK;

  vidcapt_elem->sinkpad =
      gst_pad_new_from_static_template (&sink_factory, "sink");
  gst_pad_set_chain_function (vidcapt_elem->sinkpad,
      GST_DEBUG_FUNCPTR (gst_stvidcapture_chain));
  gst_pad_set_event_function (vidcapt_elem->sinkpad,
      GST_DEBUG_FUNCPTR (gst_stvidcapture_sink_pad_event));

  vidcapt_elem->srcpad = gst_pad_new_from_static_template (&src_factory, "src");

  gst_element_add_pad (GST_ELEMENT (vidcapt_elem), vidcapt_elem->sinkpad);
  gst_pad_set_active (vidcapt_elem->srcpad, TRUE);
  gst_element_add_pad (GST_ELEMENT (vidcapt_elem), vidcapt_elem->srcpad);

  /* Create the capture thread */
  g_rec_mutex_init (&vidcapt_elem->task_lock);
  vidcapt_elem->task =
      gst_task_new ((GstTaskFunction) gst_stvidcapture_thread, vidcapt_elem,
      NULL);
  gst_object_set_name (GST_OBJECT_CAST (vidcapt_elem->task), "SMF-VidCapture");
  gst_task_set_lock (vidcapt_elem->task, &vidcapt_elem->task_lock);
}

static void
gst_stvidcapture_finalize (GObject * object)
{
  Gststvidcapture *vidcapt_elem = GST_STVIDCAPTURE (object);

  gst_task_stop (vidcapt_elem->task);
  /* Wait for the task to stop */
  gst_task_join (vidcapt_elem->task);
  gst_object_unref (vidcapt_elem->task);
  vidcapt_elem->task = NULL;
  g_rec_mutex_clear (&vidcapt_elem->task_lock);

  G_OBJECT_CLASS (parent_class)->finalize (object);

  return;
}

static void
gst_stvidcapture_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  Gststvidcapture *vidcapt_elem = GST_STVIDCAPTURE (object);

  switch (prop_id) {
    case PROP_DEV_ID:
      vidcapt_elem->dev_id = g_value_get_int (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_stvidcapture_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  Gststvidcapture *vidcapt_elem = GST_STVIDCAPTURE (object);

  switch (prop_id) {
    case PROP_DEV_ID:
      g_value_set_int (value, vidcapt_elem->dev_id);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

/* GstElement vmethod implementations */

static GstStateChangeReturn
gst_stvidcapture_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
  Gststvidcapture *vidcapt_elem = (Gststvidcapture *) element;

  GST_DEBUG_OBJECT (vidcapt_elem, "%d -> %d",
      GST_STATE_TRANSITION_CURRENT (transition),
      GST_STATE_TRANSITION_NEXT (transition));

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      /* Open device */
      if (vidcapt_elem->v4l2_fd < 0) {
        vidcapt_elem->v4l2_fd =
            v4l2_open_by_name (V4L2_CAPTURE_DRIVER_NAME, V4L2_CAPTURE_CARD_NAME,
            O_RDWR);

        if (vidcapt_elem->v4l2_fd < 0) {
          GST_ERROR_OBJECT (vidcapt_elem,
              "Couldn't open v4l2 device %s %s", V4L2_CAPTURE_DRIVER_NAME,
              strerror (errno));
          return GST_STATE_CHANGE_FAILURE;
        }
      }
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      gst_task_pause (vidcapt_elem->task);
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      gst_stvidcapture_stream_off (vidcapt_elem, vidcapt_elem->v4l2_fd);
      /* Close device */
      if (vidcapt_elem->v4l2_fd != -1) {
        close (vidcapt_elem->v4l2_fd);
        vidcapt_elem->v4l2_fd = -1;
      }
      break;

    default:
      break;
  }

  return ret;
}


/* this function handles the link with other elements */
static gboolean
gst_stvidcapture_set_caps (Gststvidcapture * vidcapt_elem, GstCaps * caps)
{
  const GstStructure *structure;
  gboolean ret = FALSE;
  GstStructure *srcstructure;
  GstCaps *allowed_caps;
  GstCaps *srccaps;
  gint fps_num;
  gint fps_den;

  if (gst_pad_get_current_caps (vidcapt_elem->srcpad) == NULL) {
    structure = gst_caps_get_structure (caps, 0);

    if (gst_structure_get_fraction (structure, "framerate", &fps_num, &fps_den)) {
      GST_INFO_OBJECT (vidcapt_elem, "Framerate num=%d den=%d", fps_num,
          fps_den);
    }
    /* REVIEW/FIXME : All the caps below are wrong (need to be updated to 1.0 caps) */
    allowed_caps = gst_pad_get_allowed_caps (vidcapt_elem->srcpad);
    if (allowed_caps && !gst_caps_is_empty (allowed_caps)) {
      srccaps = gst_caps_copy_nth (allowed_caps, 0);
      gst_caps_unref (allowed_caps);

      srcstructure = gst_caps_get_structure (srccaps, 0);
      gst_structure_set (srcstructure, "width", G_TYPE_INT, vidcapt_elem->width,
          "height", G_TYPE_INT, vidcapt_elem->height,
          "framerate", GST_TYPE_FRACTION, fps_num, fps_den, NULL);

      if (gst_structure_has_name (srcstructure, "video/x-raw")) {
        if (gst_structure_get_int (srcstructure, "bpp", &vidcapt_elem->bpp)) {
          if (vidcapt_elem->bpp == 32) {
            GST_INFO_OBJECT (vidcapt_elem, "Format BGR32 Bpp %d",
                vidcapt_elem->bpp);
            vidcapt_elem->pixel_format = V4L2_PIX_FMT_BGR32;
          } else {
            GST_INFO_OBJECT (vidcapt_elem, "Format BGR24 Bpp %d",
                vidcapt_elem->bpp);
            vidcapt_elem->pixel_format = V4L2_PIX_FMT_BGR24;
          }
        }
      }
    } else {
      GST_DEBUG_OBJECT (vidcapt_elem, "Format UYVY");
      vidcapt_elem->pixel_format = V4L2_PIX_FMT_UYVY;
      vidcapt_elem->bpp = 16;
      srcstructure = gst_structure_new ("video/x-raw",
          "framerate", GST_TYPE_FRACTION, fps_num, fps_den,
          "width", G_TYPE_INT, vidcapt_elem->width,
          "height", G_TYPE_INT, vidcapt_elem->height,
          "bpp", G_TYPE_INT, vidcapt_elem->bpp,
          "format", G_TYPE_STRING, "UYVY", NULL);
      srccaps = gst_caps_new_full (srcstructure, NULL);
    }

    if (gst_stvidcapture_setup_device (vidcapt_elem)) {
      if (gst_stvidcapture_stream_on (vidcapt_elem, vidcapt_elem->v4l2_fd)) {
        gst_task_start (vidcapt_elem->task);
        ret = TRUE;
      }
    } else {
      GST_ELEMENT_ERROR (vidcapt_elem, RESOURCE, SETTINGS,
          ("Failed to setup stvidcapture device %d", vidcapt_elem->dev_id),
          GST_ERROR_SYSTEM);
      ret = FALSE;
    }
    gst_pad_use_fixed_caps (vidcapt_elem->srcpad);
    gst_pad_set_caps (vidcapt_elem->srcpad, srccaps);
    gst_caps_unref (srccaps);
  }

  return ret;
}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_stvidcapture_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
  Gststvidcapture *vidcapt_elem = GST_STVIDCAPTURE (parent);

  GST_LOG_OBJECT (vidcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  if (vidcapt_elem->srcresult != GST_FLOW_OK) {
    GST_ERROR_OBJECT (vidcapt_elem, "error in data flow %s ",
        gst_flow_get_name (vidcapt_elem->srcresult));
  }

  gst_buffer_unref (buf);

  /* just push out the incoming buffer without touching it */
  return vidcapt_elem->srcresult;
}


static gboolean
gst_stvidcapture_sink_pad_event (GstPad * pad, GstObject * parent,
    GstEvent * event)
{
  gboolean ret = TRUE;
  GstCaps *caps;
  const GstSegment *in_segment;
  Gststvidcapture *vidcapt_elem = GST_STVIDCAPTURE (parent);

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_EOS:
      gst_event_unref (event);
      goto done;
      break;
    case GST_EVENT_CAPS:
      gst_event_parse_caps (event, &caps);
      if ((vidcapt_elem->prev_caps != NULL)
          && (gst_caps_is_equal ((const GstCaps *) vidcapt_elem->prev_caps,
                  (const GstCaps *) caps))) {
        GST_DEBUG_OBJECT (vidcapt_elem, "caps are already set");
        return TRUE;
      }
      ret = gst_stvidcapture_set_caps (vidcapt_elem, caps);
      if (ret) {
        if (vidcapt_elem->prev_caps) {
          gst_caps_unref (vidcapt_elem->prev_caps);
        }
        vidcapt_elem->prev_caps = gst_caps_ref (caps);
      } else {
        GST_ERROR_OBJECT (vidcapt_elem, "failed to set caps!");
      }
      gst_event_unref (event);
      break;
    case GST_EVENT_SEGMENT:
      gst_event_parse_segment (event, &in_segment);
      if (in_segment->format == GST_FORMAT_TIME) {
        GST_DEBUG_OBJECT (vidcapt_elem, "New segment rate %f "
            "start %" GST_TIME_FORMAT " stop %" GST_TIME_FORMAT
            " position %" GST_TIME_FORMAT, in_segment->rate,
            GST_TIME_ARGS (in_segment->start), GST_TIME_ARGS (in_segment->stop),
            GST_TIME_ARGS (in_segment->position));

        vidcapt_elem->segment.rate = in_segment->rate;
        vidcapt_elem->segment.applied_rate = in_segment->applied_rate;
        vidcapt_elem->segment.format = in_segment->format;
        vidcapt_elem->segment.start = in_segment->start;
        vidcapt_elem->segment.stop = in_segment->stop;
        vidcapt_elem->segment.position = in_segment->position;
      } else {
        GST_DEBUG_OBJECT (vidcapt_elem, "New segment rate %f "
            "start %" GST_TIME_FORMAT " stop %" GST_TIME_FORMAT
            " position %" GST_TIME_FORMAT, in_segment->rate,
            GST_TIME_ARGS (in_segment->start), GST_TIME_ARGS (in_segment->stop),
            GST_TIME_ARGS (in_segment->position));
        vidcapt_elem->segment.rate = 1;
        vidcapt_elem->segment.format = GST_FORMAT_TIME;
        vidcapt_elem->segment.start = 0;
        vidcapt_elem->segment.stop = GST_CLOCK_TIME_NONE;
        vidcapt_elem->segment.time = 0;
        gst_pad_push_event (vidcapt_elem->srcpad,
            gst_event_new_segment ((const GstSegment *)
                &vidcapt_elem->segment));
        gst_event_unref (event);
        goto done;
      }
      break;
    default:
      break;
  }
  /* Push event downstream */
  ret = gst_pad_event_default (pad, parent, event);
done:
  return ret;
}

static gboolean
gst_stvidcapture_setup_device (Gststvidcapture * vidcapt_elem)
{
  struct v4l2_requestbuffers reqbuf;
  struct v4l2_format fmt;
  gchar *linuxdvb_input;
  GstQuery *query;
  GstBufferPool *pool;
  guint size, min, max;
  gint ret = -1;

  GST_LOG_OBJECT (vidcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  /* Choose the LinuxDVB decoder input to use according to the dev-id property        */
  linuxdvb_input = g_strdup_printf ("dvb0.video%01d", vidcapt_elem->dev_id);
  GST_INFO_OBJECT (vidcapt_elem, "DVB Input = %s", linuxdvb_input);

  ret = v4l2_set_input_by_name (vidcapt_elem->v4l2_fd, linuxdvb_input);
  if (ret < 0) {
    GST_ERROR_OBJECT (vidcapt_elem, "Couldn't configure capture input to %s",
        linuxdvb_input);
    g_free (linuxdvb_input);
    return FALSE;
  }
  g_free (linuxdvb_input);

  /* Configure capture ouput */
  memset (&fmt, 0, sizeof (struct v4l2_format));
  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  fmt.fmt.pix.width = vidcapt_elem->width;
  fmt.fmt.pix.height = vidcapt_elem->height;
  fmt.fmt.pix.pixelformat = vidcapt_elem->pixel_format;
  fmt.fmt.pix.colorspace = V4L2_COLORSPACE_REC709;
  fmt.fmt.pix.field = V4L2_FIELD_ANY;

  if (ioctl (vidcapt_elem->v4l2_fd, VIDIOC_S_FMT, &fmt) < 0) {
    GST_ERROR_OBJECT (vidcapt_elem, "VIDIOC_S_FMT failed %s", strerror (errno));
    return FALSE;
  }

  /* Capture Buffer formats : VIDIOC_REQBUFS */
  memset (&reqbuf, 0, sizeof (struct v4l2_requestbuffers));
  reqbuf.memory = V4L2_MEMORY_USERPTR;
  reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  reqbuf.count = 1;

  if (ioctl (vidcapt_elem->v4l2_fd, VIDIOC_REQBUFS, &reqbuf) < 0) {
    GST_ERROR_OBJECT (vidcapt_elem, "VIDIOC_REQBUFS %s", strerror (errno));
    return FALSE;
  }

  /* Query for the pool for keeping the capture data */
  query =
      gst_query_new_allocation (gst_pad_get_current_caps (vidcapt_elem->srcpad),
      TRUE);
  if (query) {
    if (gst_pad_peer_query (vidcapt_elem->srcpad, query)) {
      gst_query_parse_nth_allocation_pool (query, 0, &pool, &size, &min, &max);
      if (pool != NULL) {
        vidcapt_elem->pool = pool;
      }
    }
    gst_query_unref (query);
  }

  return TRUE;
}

static gboolean
gst_stvidcapture_queue_buffer (Gststvidcapture * vidcapt_elem)
{
  GstFlowReturn ret = GST_FLOW_OK;
  struct v4l2_buffer buf;
  struct v4l2_video_uncompressed_metadata video_meta;

  GstEncodeBufferMeta *meta;
  GST_LOG_OBJECT (vidcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  ret =
      gst_buffer_pool_acquire_buffer (vidcapt_elem->pool,
      &vidcapt_elem->videosinkbuffer, NULL);

  if (ret == GST_FLOW_OK) {
    meta = (GstEncodeBufferMeta *)
        gst_buffer_get_meta (vidcapt_elem->videosinkbuffer,
        GST_ENCODEBUFFER_META_API_TYPE);
    if (vidcapt_elem->videosinkbuffer && (!strcmp (meta->name, "EncodeBuffer"))) {
      GstMapInfo info_read;
      gst_buffer_map (vidcapt_elem->videosinkbuffer, &info_read, GST_MAP_READ);
      memset (&buf, 0, sizeof (struct v4l2_buffer));
      buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      buf.memory = V4L2_MEMORY_USERPTR;
      buf.m.userptr = (unsigned long) info_read.data;
      buf.field = V4L2_FIELD_NONE;
      buf.length =
          vidcapt_elem->width * vidcapt_elem->height * vidcapt_elem->bpp / 8;
      buf.reserved = (int) &video_meta;
      gst_buffer_unmap (vidcapt_elem->videosinkbuffer, &info_read);
      if (ioctl (vidcapt_elem->v4l2_fd, VIDIOC_QBUF, &buf) < 0) {
        GST_ERROR_OBJECT (vidcapt_elem, "Couldn't queue buffer %s",
            strerror (errno));
        gst_buffer_unref (vidcapt_elem->videosinkbuffer);
        return FALSE;
      }
    } else {
      if (vidcapt_elem->videosinkbuffer) {
        /* Need contiguous memory, so unref the gstreamer standard buffer) */
        gst_buffer_unref (vidcapt_elem->videosinkbuffer);
      }
      g_print
          ("=========> FIXME allocate a buffer here, wait for MMAP support in dvb_adaptation\n");
      return FALSE;
    }
  } else {
    GST_ERROR_OBJECT (vidcapt_elem,
        "Failed to allocate buffer, flow error - %s", gst_flow_get_name (ret));
    return FALSE;
  }
  return TRUE;
}

static gboolean
gst_stvidcapture_dequeue_buffer (Gststvidcapture * vidcapt_elem)
{
  struct v4l2_buffer buf;

  GST_LOG_OBJECT (vidcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  memset (&buf, 0, sizeof (struct v4l2_buffer));
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_USERPTR;

  if (ioctl (vidcapt_elem->v4l2_fd, VIDIOC_DQBUF, &buf) < 0) {
    gst_buffer_unref (vidcapt_elem->videosinkbuffer);
    GST_ERROR_OBJECT (vidcapt_elem, "Couldn't de-queue user buffer - %s",
        strerror (errno));
    return FALSE;
  }

  if (buf.bytesused == 0) {
    GST_DEBUG_OBJECT (vidcapt_elem, "EOS Marker received.");
    gst_buffer_unref (vidcapt_elem->videosinkbuffer);
    gst_task_stop (vidcapt_elem->task);
    g_rec_mutex_lock (&vidcapt_elem->task_lock);
    g_rec_mutex_unlock (&vidcapt_elem->task_lock);
    gst_pad_push_event (vidcapt_elem->srcpad, gst_event_new_eos ());
    return TRUE;
  }

  /* Set the buffer timestamp to the original pts grabbed from streaming engine */
  GST_BUFFER_TIMESTAMP (vidcapt_elem->videosinkbuffer) =
      GST_TIMEVAL_TO_TIME (buf.timestamp);
  GST_BUFFER_DURATION (vidcapt_elem->videosinkbuffer) = GST_CLOCK_TIME_NONE;

  vidcapt_elem->numframe++;

  GST_LOG_OBJECT (vidcapt_elem, "Capture time : %lld ms (%ld sec %ld usec)",
      GST_TIME_AS_MSECONDS (GST_BUFFER_TIMESTAMP
          (vidcapt_elem->videosinkbuffer)), buf.timestamp.tv_sec,
      buf.timestamp.tv_usec);

  /* Push the buffer to downstream element */
  vidcapt_elem->srcresult =
      gst_pad_push (vidcapt_elem->srcpad, vidcapt_elem->videosinkbuffer);
  if (vidcapt_elem->srcresult != GST_FLOW_OK) {
    GST_ERROR_OBJECT (vidcapt_elem, "Failed to push buffer downstream - %s",
        gst_flow_get_name (vidcapt_elem->srcresult));
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_stvidcapture_stream_on (Gststvidcapture * vidcapt_elem, int fd)
{
  struct v4l2_buffer buf;

  GST_LOG_OBJECT (vidcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  memset (&buf, 0, sizeof (struct v4l2_buffer));
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl (fd, VIDIOC_STREAMON, &buf.type) < 0) {
    GST_ELEMENT_ERROR (vidcapt_elem, RESOURCE, FAILED,
        ("Failed to start streaming on device %d", vidcapt_elem->dev_id),
        ("VIDIOC_STREAMON failed : %s", strerror (errno)));
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_stvidcapture_stream_off (Gststvidcapture * vidcapt_elem, int fd)
{
  struct v4l2_buffer buf;

  GST_LOG_OBJECT (vidcapt_elem, "%s:%d", __FUNCTION__, __LINE__);

  memset (&buf, 0, sizeof (struct v4l2_buffer));
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl (fd, VIDIOC_STREAMOFF, &buf.type) < 0) {
    GST_ELEMENT_ERROR (vidcapt_elem, RESOURCE, FAILED,
        ("Failed to stop streaming on device %d", vidcapt_elem->dev_id),
        ("VIDIOC_STREAMOFF failed : %s", strerror (errno)));
    return FALSE;
  }

  return TRUE;
}

static void
gst_stvidcapture_thread (Gststvidcapture * vidcapt_elem)
{
  /* Push the empty buffer in the queue */
  if (!gst_stvidcapture_queue_buffer (vidcapt_elem)) {
    GST_ERROR_OBJECT (vidcapt_elem, "Could not queue buffer");
    goto error;
  }

  if (!gst_stvidcapture_dequeue_buffer (vidcapt_elem)) {
    GST_ERROR_OBJECT (vidcapt_elem, "Could not dequeue buffer");
    goto error;
  }

  return;

error:
  usleep (1000);

  return;
}

/* entry point to initialize the element */
gboolean
stvidcapture_init (GstPlugin * plugin)
{
  GST_DEBUG_CATEGORY_INIT (gst_stvidcapture_debug, "stvidcapture", 0,
      "ST capture");

  return gst_element_register (plugin, "stvidcapture", GST_RANK_PRIMARY + 10,
      GST_TYPE_STVIDCAPTURE);
}
