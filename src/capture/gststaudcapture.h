/* Gstreamer Audio-Capture
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License Type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STAUDCAPTURE_H__
#define __GST_STAUDCAPTURE_H__

#include <gst/gst.h>

G_BEGIN_DECLS
/* #defines don't like whitespacey bits */
#define GST_TYPE_STAUDCAPTURE \
  (gst_staudcapture_get_type())
#define GST_STAUDCAPTURE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_STAUDCAPTURE,Gststaudcapture))
#define GST_STAUDCAPTURE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_STAUDCAPTURE,GststaudcaptureClass))
#define GST_IS_STAUDCAPTURE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_STAUDCAPTURE))
#define GST_IS_STAUDCAPTURE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_STAUDCAPTURE))
typedef struct _Gststaudcapture Gststaudcapture;
typedef struct _GststaudcaptureClass GststaudcaptureClass;

struct _Gststaudcapture
{
  GstElement element;
  GstPad *sinkpad, *srcpad;
  GstSegment segment;           /* Segment to hold NEWSEGMENT information */

  gint v4l2_fd;
  guint dev_id;
  guint sample_rate;
  guint channels;
  guint depth;

  GstTask *task;
  GRecMutex task_lock;

  GstBuffer *audiosinkbuffer;
  GstCaps *srccaps;

  gint64 numframe;
  GstBufferPool *pool;
  GstFlowReturn srcresult;
  GstCaps *prev_caps;
};

struct _GststaudcaptureClass
{
  GstElementClass parent_class;
};

GType gst_staudcapture_get_type (void);

gboolean staudcapture_init (GstPlugin * plugin);

G_END_DECLS
#endif /* __GST_STAUDCAPTURE_H__ */
