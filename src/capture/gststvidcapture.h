/* Gstreamer Video-Capture
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STVIDCAPTURE_H__
#define __GST_STVIDCAPTURE_H__

#include <gst/gst.h>

G_BEGIN_DECLS
/* #defines don't like whitespacey bits */
#define GST_TYPE_STVIDCAPTURE \
  (gst_stvidcapture_get_type())
#define GST_STVIDCAPTURE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_STVIDCAPTURE,Gststvidcapture))
#define GST_STVIDCAPTURE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_STVIDCAPTURE,GststvidcaptureClass))
#define GST_IS_STVIDCAPTURE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_STVIDCAPTURE))
#define GST_IS_STVIDCAPTURE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_STVIDCAPTURE))
typedef struct _Gststvidcapture Gststvidcapture;
typedef struct _GststvidcaptureClass GststvidcaptureClass;

struct _Gststvidcapture
{
  GstElement element;
  GstPad *sinkpad, *srcpad;
  GstSegment segment;           /* Segment to hold NEWSEGMENT information */

  gint v4l2_fd;

  guint dev_id;
  gint width;
  gint height;
  gint bpp;
  guint pixel_format;

  GstTask *task;
  GRecMutex task_lock;

  GstBuffer *videosinkbuffer;
  GstCaps *srccaps;

  gint64 numframe;
  GstFlowReturn srcresult;
  GstBufferPool *pool;
  GstCaps *prev_caps;
};

struct _GststvidcaptureClass
{
  GstElementClass parent_class;
};

GType gst_stvidcapture_get_type (void);

gboolean stvidcapture_init (GstPlugin * plugin);

G_END_DECLS
#endif /* __GST_STVIDCAPTURE_H__ */
