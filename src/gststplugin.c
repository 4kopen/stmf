/* Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <gst/gst.h>
#include <config.h>
#include "gststvideo.h"
#include "gststaudio.h"
#include "gststm-tsdemux.h"
#include "gststdisplaysink.h"
#include "gststaudiosink.h"
#include "gststtextoverlay.h"
#include "gstsubtitleoverlay.h"
#include "gststh264encode.h"
#include "gststac3encode.h"
#include "gststaacencode.h"
#include "gststvidcapture.h"
#include "gststaudcapture.h"
#include "gststm-hwjpegdec.h"

/* entry point to initialize the plug-in
 * initialize the plug-in itself
 * register the element factories and other features
 */
static gboolean
plugin_init (GstPlugin * plugin)
{
  /* Debug category initializations */
  gst_pes_filter_dbg_init ();

  /* initialize stvideo */
  if (!stvideo_init (plugin))
    return FALSE;

  /* initialize staudio */
  if (!staudio_init (plugin))
    return FALSE;

  /* initialize sth264encode */
  if (!sth264encode_init (plugin))
    return FALSE;

  /* initialize stac3encode */
  if (!stac3encode_init (plugin))
    return FALSE;

  /* initialize staacencode */
  if (!staacencode_init (plugin))
    return FALSE;

  /* initialize stvidcapture */
  if (!stvidcapture_init (plugin))
    return FALSE;

  /* initialize stvidcapture */
  if (!staudcapture_init (plugin))
    return FALSE;

  /* initialize sttextoverlay */
  if (!sttextoverlay_init (plugin))
    return FALSE;

  /* initialize stsubtitleoverlay to overwrite native */
  if (!stsubtitleoverlay_init (plugin))
    return FALSE;

  /* initialize stdisplaysink to overwrite v4l2sink */
  if (!stdisplaysink_init (plugin))
    return FALSE;

  /* initialize sttsdemux to overwrite tsdemux */
  if (!sttsdemux_init (plugin))
    return FALSE;

  if (!sthwjpegdec_init (plugin))
    return FALSE;

  if (!staudiosink_init (plugin))
    return FALSE;
  return TRUE;
}

/* gstreamer looks for this structure to register plugins */
GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    st,
    "ST HW accelerated elements",
    plugin_init, VERSION, "LGPL", "STMicroelectronics R&D", "http://www.st.com")
