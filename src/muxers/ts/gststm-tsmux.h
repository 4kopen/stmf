/* Gstreamer ST TS Muxer Plugin
 *
 * Copyright (C) 2013 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STM_TS_MUX_H__
#define __GST_STM_TS_MUX_H__

#include <gst/gst.h>
#include <gst/base/gstcollectpads.h>

#define GST_TYPE_ST_TS_MUX (gst_stm_ts_mux_get_type())
#define GST_ST_TS_MUX(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_ST_TS_MUX,Gststtsmux))
#define GST_ST_TS_MUX_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_ST_TS_MUX,GststtsmuxClass))
#define GST_IS_ST_TS_MUX(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_ST_TS_MUX))
#define GST_IS_ST_TS_MUX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_ST_TS_MUX))

typedef struct _GststtsmuxPadData GststtsmuxPadData;
typedef struct _Gststtsmux Gststtsmux;
typedef struct _GststtsmuxClass GststtsmuxClass;

struct _GststtsmuxPadData
{
  GstCollectData collect_data;

  int fd;
  GstBuffer **buffers;
  int nbuffers;
  gboolean eos;
  GstPadEventFunction collect_event;
};

struct _Gststtsmux
{
  GstElement element;

  GstPad *srcpad;
  GstCollectPads *collectpads;
  GSList *sinkpads;

  int fd;
  guint dev_id;
  gboolean streaming;
  GstFlowReturn rflow;
  GstCaps *srccaps;
  int base_pid;
  int next_pid;
  guint64 bitrate;
  gboolean cbr;
  int pcr_period;
  gboolean gen_pcr_stream;
  gboolean gen_pat_pmt;
  gboolean gen_sdt;
  int table_period;
  char sdt_prov[17];
  char sdt_serv[17];
  char pmt_desc[17];
  int pcr_pid;
  int pmt_pid;
  int nbuffers;
  GstBuffer **buffers;

  GMutex lock;

  GMutex eos_lock;              /* Semaphore used for EOS event */
  GCond eos_cond;
  gboolean eos_received;
  gboolean flushing;
  GstCaps *prev_caps;
};

struct _GststtsmuxClass
{
  GstElementClass parent_class;
};

GType gst_stm_ts_mux_get_type (void);

gboolean sttsmux_init (GstPlugin * plugin);

G_END_DECLS
#endif /* __GST_STM_TS_MUX_H__ */
