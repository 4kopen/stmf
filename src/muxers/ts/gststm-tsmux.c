/* Gstreamer ST TS Muxer Plugin
 *
 * Copyright (C) 2013 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/* Portions from tests/test_utils/src/applications/transcode/stm-tsmux.c */

/**
 * SECTION:element-stm_ts_mux
 *
 * Handles muxing of ts streams using HW mux.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <poll.h>
#include <stdio.h>

#include <linux/videodev2.h>
#include "linux/dvb/dvb_v4l2_export.h"
#include <linux/dvb/stm_ioctls.h>

#include "gstmpegdefs.h"
#include "gststm-tsmux.h"
#include "gststbufferpool.h"

#if 1
/* A useful debugging helper */
#ifndef ioctl
#define ioctl(fd,ioctlno,args...) \
  ({int ioctl_ret; do { \
    GST_LOG_OBJECT (mux, "Calling ioctl "#ioctlno" on fd %d", fd); \
    do { \
      ioctl_ret = ioctl(fd,ioctlno,##args); \
    } while (ioctl_ret < 0 && errno == EINTR); \
    if (ioctl_ret < 0) { \
      GST_WARNING_OBJECT (mux, "ioctl "#ioctlno" on fd %d return: %d (%s)", \
          fd, ioctl_ret, strerror (errno)); \
    } else { \
      GST_LOG_OBJECT (mux, "ioctl "#ioctlno" on fd %d return: 0", fd); \
    } \
  } while(0); \
  ioctl_ret;})
#endif
#endif

#define COPY_ES_BUFFERS

GST_DEBUG_CATEGORY_STATIC (gst_stm_ts_mux_debug);
#define GST_CAT_DEFAULT gst_stm_ts_mux_debug

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

#define MIN_DEV_ID      0
#define MAX_DEV_ID      3
#define DEFAULT_DEV_ID  0

/* TODO: most defaults and ranges nicked from the kernel source,
   there might be a better way in case these change there later.
   Maybe there is a way to query them from the kernel. */

#define MIN_BASE_PID      1
#define MAX_BASE_PID      0x1ffd
#define DEFAULT_BASE_PID  0x100

#define MIN_BITRATE      (100*1024)
#define MAX_BITRATE      (60*1024*1024)
#define DEFAULT_BITRATE  (48*1024*1024)

#define DEFAULT_CBR      FALSE

#define MIN_PCR_PERIOD      900
#define MAX_PCR_PERIOD      9000
#define DEFAULT_PCR_PERIOD  4500

#define DEFAULT_GEN_PCR_STREAM TRUE

#define DEFAULT_GEN_PAT_PMT    TRUE
#define DEFAULT_GEN_SDT        TRUE

#define MIN_TABLE_PERIOD       2250
#define MAX_TABLE_PERIOD       9000
#define DEFAULT_TABLE_PERIOD   9000

#define MIN_PCR_PID            1
#define MAX_PCR_PID            0x1ffe
#define DEFAULT_PCR_PID        0x1ffe

#define MIN_PMT_PID            1
#define MAX_PMT_PID            0x1ffd
#define DEFAULT_PMT_PID        0x31

/* Seems to be max size of incoming buffers ? Set to same value as sample code */
#define STREAM_BUFFER_SIZE (512*1024)

/* Seems to be max size of muxed buffers, set to same value as sample code
   (with constants replaced by properties where applicable) */
#define MUXED_BUFFER_SIZE(mux) ((mux->bitrate) / (90000 / mux->pcr_period))

#define NUM_INPUT_BUFFERS 32
#define NUM_OUTPUT_BUFFERS 4

enum
{
  PROP_0,
  PROP_DEV_ID,
  PROP_BASE_PID,
  PROP_BITRATE,
  PROP_CBR,
  PROP_PCR_PERIOD,
  PROP_GEN_PCR_STREAM,
  PROP_GEN_PAT_PMT,
  PROP_GEN_SDT,
  PROP_TABLE_PERIOD,
  PROP_SDT_SERV,
  PROP_SDT_PROV,
  PROP_PMT_DESC,
  PROP_PCR_PID,
  PROP_PMT_PID,
  PROP_LAST
};

#define GST_TSMUX_MUTEX_LOCK(o,x) G_STMT_START {                                 \
  GST_LOG_OBJECT (o, "Locking mutex '"#x"' from thread %p", g_thread_self ());   \
  g_mutex_lock (&o->x);                                                   \
  GST_LOG_OBJECT (o, "Locked mutex '"#x"' from thread %p", g_thread_self ());    \
} G_STMT_END

#define GST_TSMUX_MUTEX_UNLOCK(o,x) G_STMT_START {                               \
  GST_LOG_OBJECT (o, "Unlocking mutex '"#x"' from thread %p", g_thread_self ()); \
  g_mutex_unlock (&o->x);                                                 \
} G_STMT_END

#define LOCK_IO(mux) GST_TSMUX_MUTEX_LOCK(mux,lock)
#define UNLOCK_IO(mux) GST_TSMUX_MUTEX_UNLOCK(mux,lock)

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate src_pad_template = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS
    ("video/mpegts, systemstream=(boolean)true, packetsize=(int){188,192}")
    );

static GstStaticPadTemplate sink_pad_template = GST_STATIC_PAD_TEMPLATE
    ("sink_%d",
    GST_PAD_SINK,
    GST_PAD_REQUEST,
    GST_STATIC_CAPS ("video/x-h264, stream-format=byte-stream, alignment=au;"
        "audio/mpeg, mpegversion=4, stream-format=adts;" "audio/x-ac3;"));

static void gst_stm_ts_mux_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_stm_ts_mux_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static void gst_stm_ts_mux_reset (Gststtsmux * mux);
static GstStateChangeReturn gst_stm_ts_mux_change_state (GstElement * element,
    GstStateChange transition);
static GstPad *gst_stm_ts_mux_request_new_pad (GstElement * element,
    GstPadTemplate * pad_template, const gchar * name, const GstCaps * caps);
static void gst_stm_ts_mux_release_pad (GstElement * element, GstPad * pad);
static gboolean gst_stm_ts_mux_pad_set_caps (GstPad * pad, GstCaps * caps);
static int gst_stm_ts_mux_dequeue_ready_buffer (Gststtsmux * mux,
    GststtsmuxPadData * pdata);
static GstFlowReturn gst_stm_ts_mux_collected (GstCollectPads * pads,
    Gststtsmux * mux);
static gboolean gst_stm_ts_mux_reap_eos (Gststtsmux * mux,
    GstCollectPads * pads, gboolean force);
static gboolean gst_stm_ts_mux_sink_event (GstPad * pad, GstObject * parent,
    GstEvent * event);
static void gst_stm_ts_mux_read_task (GstPad * pad);
static void gst_stm_ts_mux_finalize (GObject * object);

#define gst_stm_ts_mux_parent_class parent_class
G_DEFINE_TYPE (Gststtsmux, gst_stm_ts_mux, GST_TYPE_ELEMENT);

/* Initialize the STM TS mux class */
static void
gst_stm_ts_mux_class_init (GststtsmuxClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *element_class;

  GST_DEBUG ("gst_stm_ts_mux_class_init");

  gobject_class = (GObjectClass *) klass;
  element_class = (GstElementClass *) klass;
  parent_class = g_type_class_peek_parent (klass);

  gobject_class->set_property = GST_DEBUG_FUNCPTR (gst_stm_ts_mux_set_property);
  gobject_class->get_property = GST_DEBUG_FUNCPTR (gst_stm_ts_mux_get_property);
  gobject_class->finalize = GST_DEBUG_FUNCPTR (gst_stm_ts_mux_finalize);

  element_class->change_state = GST_DEBUG_FUNCPTR (gst_stm_ts_mux_change_state);

  element_class->request_new_pad = gst_stm_ts_mux_request_new_pad;
  element_class->release_pad = gst_stm_ts_mux_release_pad;

  g_object_class_install_property (gobject_class, PROP_DEV_ID,
      g_param_spec_int ("dev-id", "device id for muxer",
          "device id for muxer", MIN_DEV_ID, MAX_DEV_ID, DEFAULT_DEV_ID,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_BASE_PID,
      g_param_spec_int ("base-pid", "Base PID number",
          "First PID to use for numbering elementary streams",
          MIN_BASE_PID, MAX_BASE_PID, DEFAULT_BASE_PID,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_BITRATE,
      g_param_spec_uint64 ("bitrate", "Bitrate",
          "Bit rate in bits per second (fixed if CBR, maximum if not)",
          MIN_BITRATE, MAX_BITRATE, DEFAULT_BITRATE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_CBR,
      g_param_spec_boolean ("cbr", "CBR", "Constant bit rate", DEFAULT_CBR,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_PCR_PERIOD,
      g_param_spec_int ("pcr-period", "PCR period",
          "PCR period in units of 90 kHz (eg, 9000 for 100 ms)",
          MIN_PCR_PERIOD, MAX_PCR_PERIOD, DEFAULT_PCR_PERIOD,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_GEN_PCR_STREAM,
      g_param_spec_boolean ("gen-pcr-stream", "Generate a separate PCR stream",
          "Generate a separate PCR stream", DEFAULT_GEN_PCR_STREAM,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_GEN_PAT_PMT,
      g_param_spec_boolean ("gen-pat-pmt", "Generate PAT/PMT",
          "Generate PAT and PMT tables", DEFAULT_GEN_PAT_PMT,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_GEN_SDT,
      g_param_spec_boolean ("gen-sdt", "Generate SDT",
          "Generate SDT table", DEFAULT_GEN_SDT,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_TABLE_PERIOD,
      g_param_spec_int ("table-period", "Table period",
          "Table period in units of 90 kHz (eg, 9000 for 100 ms)",
          MIN_TABLE_PERIOD, MAX_TABLE_PERIOD, DEFAULT_TABLE_PERIOD,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_SDT_PROV,
      g_param_spec_string ("sdt-prov", "SDT provider name",
          "Provider name to place in SDT table (truncated at 16 characters)",
          "", G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_SDT_SERV,
      g_param_spec_string ("sdt-serv", "SDT service name",
          "Service name to place in SDT table (truncated at 16 characters)", "",
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_PMT_DESC,
      g_param_spec_string ("pmt-desc", "PMT descriptor",
          "Descriptor to place in PMT table (truncated at 16 characters)", "",
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_PCR_PID,
      g_param_spec_int ("pcr-pid", "PCR PID", "PID to use for the PCR stream",
          MIN_PCR_PID, MAX_PCR_PID, DEFAULT_PCR_PID,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_PMT_PID,
      g_param_spec_int ("pmt-pid", "PMT PID", "PID to use for the PMT stream",
          MIN_PMT_PID, MAX_PMT_PID, DEFAULT_PMT_PID,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  gst_element_class_set_static_metadata (element_class,
      "ST TS mux",
      "Codec/Muxer", "Multiplex audio/video to a ts stream", "www.st.com");

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_pad_template));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_pad_template));

}

static void
gst_stm_ts_mux_init (Gststtsmux * mux)
{
  mux->eos_received = FALSE;

  mux->collectpads = gst_collect_pads_new ();
  gst_collect_pads_set_function (mux->collectpads,
      (GstCollectPadsFunction) GST_DEBUG_FUNCPTR (gst_stm_ts_mux_collected),
      mux);

  mux->srcpad = gst_pad_new_from_static_template (&src_pad_template, "src");
  gst_element_add_pad (GST_ELEMENT (mux), mux->srcpad);
  mux->sinkpads = NULL;

  mux->base_pid = DEFAULT_BASE_PID;
  mux->bitrate = DEFAULT_BITRATE;
  mux->cbr = DEFAULT_CBR;
  mux->pcr_period = DEFAULT_PCR_PERIOD;
  mux->gen_pcr_stream = DEFAULT_GEN_PCR_STREAM;
  mux->gen_pat_pmt = DEFAULT_GEN_PAT_PMT;
  mux->gen_sdt = DEFAULT_GEN_SDT;
  mux->table_period = DEFAULT_TABLE_PERIOD;
  mux->sdt_prov[0] = 0;
  mux->sdt_serv[0] = 0;
  mux->pmt_desc[0] = 0;
  mux->pcr_pid = DEFAULT_PCR_PID;
  mux->pmt_pid = DEFAULT_PMT_PID;

  gst_stm_ts_mux_reset (mux);
  g_mutex_init (&mux->lock);
  g_mutex_init (&mux->eos_lock);
  g_cond_init (&mux->eos_cond);
}

/* GstElement vmethod implementations */
static void
gst_stm_ts_mux_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  Gststtsmux *mux = (Gststtsmux *) object;

  switch (prop_id) {
    case PROP_DEV_ID:
      mux->dev_id = g_value_get_int (value);
      break;
    case PROP_BASE_PID:
      mux->base_pid = g_value_get_int (value);
      mux->next_pid = mux->base_pid;
      break;
    case PROP_BITRATE:
      mux->bitrate = g_value_get_uint64 (value);
      break;
    case PROP_CBR:
      mux->cbr = g_value_get_boolean (value);
      break;
    case PROP_PCR_PERIOD:
      mux->pcr_period = g_value_get_int (value);
      break;
    case PROP_GEN_PCR_STREAM:
      mux->gen_pcr_stream = g_value_get_boolean (value);
      break;
    case PROP_GEN_PAT_PMT:
      mux->gen_pat_pmt = g_value_get_boolean (value);
      break;
    case PROP_GEN_SDT:
      mux->gen_sdt = g_value_get_boolean (value);
      break;
    case PROP_TABLE_PERIOD:
      mux->table_period = g_value_get_int (value);
      break;
    case PROP_SDT_PROV:
      g_strlcpy (mux->sdt_prov, g_value_get_string (value),
          sizeof (mux->sdt_prov));
      break;
    case PROP_SDT_SERV:
      g_strlcpy (mux->sdt_serv, g_value_get_string (value),
          sizeof (mux->sdt_serv));
      break;
    case PROP_PMT_DESC:
      g_strlcpy (mux->pmt_desc, g_value_get_string (value),
          sizeof (mux->pmt_desc));
      break;
    case PROP_PCR_PID:
      mux->pcr_pid = g_value_get_int (value);
      break;
    case PROP_PMT_PID:
      mux->pmt_pid = g_value_get_int (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_stm_ts_mux_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  Gststtsmux *mux = (Gststtsmux *) object;

  switch (prop_id) {
    case PROP_DEV_ID:
      g_value_set_int (value, mux->dev_id);
      break;
    case PROP_BASE_PID:
      g_value_set_int (value, mux->base_pid);
      break;
    case PROP_BITRATE:
      g_value_set_uint64 (value, mux->bitrate);
      break;
    case PROP_CBR:
      g_value_set_boolean (value, mux->cbr);
      break;
    case PROP_PCR_PERIOD:
      g_value_set_int (value, mux->pcr_period);
      break;
    case PROP_GEN_PCR_STREAM:
      g_value_set_boolean (value, mux->gen_pcr_stream);
      break;
    case PROP_GEN_PAT_PMT:
      g_value_set_boolean (value, mux->gen_pat_pmt);
      break;
    case PROP_GEN_SDT:
      g_value_set_boolean (value, mux->gen_sdt);
      break;
    case PROP_TABLE_PERIOD:
      g_value_set_int (value, mux->table_period);
      break;
    case PROP_SDT_PROV:
      g_value_set_string (value, mux->sdt_prov);
      break;
    case PROP_SDT_SERV:
      g_value_set_string (value, mux->sdt_serv);
      break;
    case PROP_PMT_DESC:
      g_value_set_string (value, mux->pmt_desc);
      break;
    case PROP_PCR_PID:
      g_value_set_int (value, mux->pcr_pid);
      break;
    case PROP_PMT_PID:
      g_value_set_int (value, mux->pmt_pid);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static int
gst_stm_ts_mux_open_device (Gststtsmux * mux, gboolean check)
{
  char dev_name[16];
  int fd = -1;

  snprintf (dev_name, sizeof (dev_name), "/dev/tsmux%u", mux->dev_id);
  dev_name[sizeof (dev_name) - 1] = 0;
  GST_DEBUG_OBJECT (mux, "Opening device %s", dev_name);
  fd = open (dev_name, O_RDWR);
  if (fd < 0) {
    GST_ERROR_OBJECT (mux, "Failed to open %s: %s", dev_name, strerror (errno));
    goto fail;
  }

  if (check) {
    struct v4l2_capability cap;

    if (ioctl (fd, VIDIOC_QUERYCAP, &cap) < 0) {
      GST_ERROR_OBJECT (mux, "Failed to query device %s caps: %s", dev_name,
          strerror (errno));
      goto fail;
    }
    if (!(cap.capabilities & V4L2_CAP_DVB_CAPTURE)) {
      GST_ERROR_OBJECT (mux, "Device %s is not a video capture device",
          dev_name);
      goto fail;
    }
    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
      GST_ERROR_OBJECT (mux, "Device %s does not support streaming I/O",
          dev_name);
      goto fail;
    }
  }

  GST_INFO_OBJECT (mux, "Device %s opened as %d", dev_name, fd);
  return fd;

fail:
  if (fd >= 0)
    close (fd);
  return -1;
}

static void
gst_stm_ts_mux_close_device (Gststtsmux * mux, int fd)
{
  if (fd < 0)
    return;
  GST_DEBUG_OBJECT (mux, "Closing fd %d", fd);
  if (close (fd) < 0) {
    GST_WARNING_OBJECT (mux, "Error closing device %d: %s", fd,
        strerror (errno));
  }
}

static int
gst_stm_ts_mux_reqbufs (Gststtsmux * mux, int fd, int type, int count)
{
  struct v4l2_requestbuffers req;

  memset (&req, 0, sizeof (req));

  req.count = count;
  req.type = type;
  req.memory = V4L2_MEMORY_USERPTR;
  if (ioctl (fd, VIDIOC_REQBUFS, &req) < 0 || req.count <= 0) {
    GST_ERROR_OBJECT (mux, "Failed to request %d type %d buffers on fd %d: %s",
        count, type, fd, strerror (errno));
    return -1;
  }
  if (count != req.count) {
    GST_WARNING_OBJECT (mux,
        "fd %d: request for type %d buffers yielded only %d (%d requested)", fd,
        type, req.count, count);
  }
  return req.count;
}

static gboolean
gst_stm_ts_mux_queue_muxed_buffer (Gststtsmux * mux, int idx,
    GstBuffer * buffer)
{
  struct v4l2_buffer buf;
  GstMapInfo info_write;
  gboolean ret = TRUE;

  memset (&buf, 0, sizeof (buf));
  gst_buffer_map (buffer, &info_write, GST_MAP_WRITE);
  buf.index = idx;
  buf.type = V4L2_BUF_TYPE_DVB_CAPTURE;
  buf.memory = V4L2_MEMORY_USERPTR;
  buf.m.userptr = (unsigned long) info_write.data;
  buf.length = info_write.size;
  buf.bytesused = 0;

  if (ioctl (mux->fd, VIDIOC_QBUF, &buf) < 0) {
    GST_ERROR_OBJECT (mux, "Failed to queue muxed buffer: %s",
        strerror (errno));
    ret = FALSE;
  }

  gst_buffer_unmap (buffer, &info_write);
  return ret;
}

static gboolean
gst_stm_ts_mux_set_output_parameters (Gststtsmux * mux, int fd)
{
  struct v4l2_ext_controls controls;
  struct v4l2_ext_control ctrl_array[] = {
    {
          .id = V4L2_CID_STM_TSMUX_PCR_PERIOD,
          .size = 0,
          .reserved2 = {0},
          /* Default is 4500 */
          .value = mux->pcr_period,
        },
    {
          .id = V4L2_CID_STM_TSMUX_GEN_PCR_STREAM,
          .size = 0,
          .reserved2 = {0},
          /* Default is 1 */
          .value = mux->gen_pcr_stream ? 1 : 0,
        },
    {
          .id = V4L2_CID_STM_TSMUX_TABLE_GEN,
          .size = 0,
          .reserved2 = {0},
          /* Default is PAT_PMT + SDT */
          .value = (mux->gen_pat_pmt ? V4L2_STM_TSMUX_TABLE_GEN_PAT_PMT : 0)
          | (mux->gen_sdt ? V4L2_STM_TSMUX_TABLE_GEN_SDT : 0),
        },
    {
          .id = V4L2_CID_STM_TSMUX_TABLE_PERIOD,
          .size = 0,
          .reserved2 = {0},
          /* Default is 4500 */
          .value = mux->table_period,
        },
    {
          .id = V4L2_CID_STM_TSMUX_BITRATE_IS_CONSTANT,
          .size = 0,
          .reserved2 = {0},
          /* Default is 0 i.e. variable */
          .value = mux->cbr ? 1 : 0,
        },
    {
          .id = V4L2_CID_STM_TSMUX_BITRATE,
          .size = 0,
          .reserved2 = {0},
          /* Default is 48*1024*1024 */
          .value = mux->bitrate,
        },
    {
          .id = V4L2_CID_STM_TSMUX_SDT_PROV_NAME,
          .size = 16,
          .reserved2 = {0},
          /* Default is empty string */
          .string = mux->sdt_prov,
        },
    {
          .id = V4L2_CID_STM_TSMUX_SDT_SERV_NAME,
          .size = 16,
          .reserved2 = {0},
          /* Default is empty string */
          .string = mux->sdt_serv,
        },
    {
          .id = V4L2_CID_STM_TSMUX_PMT_DESCRIPTOR,
          .size = 16,
          .reserved2 = {0},
          /* Default is empty string */
          .string = mux->pmt_desc,
        },
    {
          .id = V4L2_CID_STM_TSMUX_PCR_PID,
          .size = 0,
          .reserved2 = {0},
          /* Default is 0x1ffe */
          .value = mux->pcr_pid,
        },
    {
          .id = V4L2_CID_STM_TSMUX_PMT_PID,
          .size = 0,
          .reserved2 = {0},
          /* Default is 0x31 */
          .value = mux->pmt_pid,
        },
  };
  int i;
  struct v4l2_ext_control *ctrl;

  controls.ctrl_class = V4L2_CTRL_CLASS_TSMUX;
  controls.count = sizeof (ctrl_array) / sizeof (struct v4l2_ext_control);
  if (controls.count == 0)
    return TRUE;
  controls.reserved[0] = 0;
  controls.reserved[1] = 0;
  controls.controls = &ctrl_array[0];

  for (i = 0; i < controls.count; i++) {
    ctrl = &controls.controls[i];
    GST_DEBUG_OBJECT (mux, "Attempting to set tsmux control %d:", ctrl->id);
    GST_DEBUG_OBJECT (mux, "Size = %d", ctrl->size);
    if (ctrl->size != 0)
      GST_DEBUG_OBJECT (mux, "Value = %s", ctrl->string);
    else
      GST_DEBUG_OBJECT (mux, "Value = %d", ctrl->value);
  }
  if (ioctl (fd, VIDIOC_S_EXT_CTRLS, &controls) < 0) {
    GST_ERROR ("VIDIOC_S_EXT_CTRLS");
    return FALSE;
  }
  return TRUE;
}

static gboolean
gst_stm_ts_mux_negotiate_downstream (Gststtsmux * mux)
{
  GstCaps *caps;
  GstStructure *structure;

  if (mux->srccaps)
    return TRUE;

  GST_INFO_OBJECT (mux, "Negotiating with downstream");
  caps = gst_pad_get_allowed_caps (mux->srcpad);
  if (caps) {
    GST_DEBUG_OBJECT (mux->srcpad, "Allowed caps: %" GST_PTR_FORMAT, caps);
    caps = gst_caps_make_writable (caps);
    structure = gst_caps_get_structure (caps, 0);
    gst_structure_fixate_field_nearest_int (structure, "packetsize", 188);
  } else {
    caps =
        gst_caps_new_simple ("video/mpegts", "systemstream", G_TYPE_BOOLEAN,
        TRUE, "packetsize", G_TYPE_INT, 188, NULL);
    GST_DEBUG_OBJECT (mux->srcpad,
        "No allowed caps, using default %" GST_PTR_FORMAT, caps);
  }

  if (!gst_pad_set_caps (mux->srcpad, caps)) {
    gst_caps_unref (caps);
    return FALSE;
  }

  GST_INFO_OBJECT (mux, "Negotiated caps: %" GST_PTR_FORMAT, caps);
  mux->srccaps = caps;

  return TRUE;
}

static int
gst_stm_ts_mux_get_output_type (GstCaps * caps)
{
  int packetsize = 0;
  GstStructure *structure;

  if (!caps)
    return V4L2_DVB_FMT_TS;
  structure = gst_caps_get_structure (caps, 0);
  if (!gst_structure_get_int (structure, "packetsize", &packetsize))
    return V4L2_DVB_FMT_TS;
  switch (packetsize) {
    default:
    case 188:
      return V4L2_DVB_FMT_TS;
    case 192:
      return V4L2_DVB_FMT_DLNA_TS;
  }
}

static gboolean
gst_stm_ts_mux_setup_capture (Gststtsmux * mux)
{
  struct v4l2_format format;
  int n;

  GST_DEBUG_OBJECT (mux, "Setting up capture");

  mux->fd = gst_stm_ts_mux_open_device (mux, TRUE);
  if (mux->fd < 0)
    return mux->fd;

  memset (&format, 0, sizeof (format));
  format.type = V4L2_BUF_TYPE_DVB_CAPTURE;
  format.fmt.dvb.data_type = gst_stm_ts_mux_get_output_type (mux->srccaps);
  format.fmt.dvb.buffer_size = MUXED_BUFFER_SIZE (mux);
  if (ioctl (mux->fd, VIDIOC_S_FMT, &format) < 0) {
    GST_ERROR_OBJECT (mux, "Error setting up capture: %s", strerror (errno));
    goto close_error;
  }

  mux->nbuffers =
      gst_stm_ts_mux_reqbufs (mux, mux->fd, V4L2_BUF_TYPE_DVB_CAPTURE,
      NUM_OUTPUT_BUFFERS);
  if (mux->nbuffers < 0)
    goto close_error;

  if (!gst_stm_ts_mux_set_output_parameters (mux, mux->fd))
    goto close_error;

  mux->buffers = g_malloc0 (mux->nbuffers * sizeof (GstBuffer *));
  for (n = 0; n < mux->nbuffers; ++n) {
    GstBuffer *buffer = gst_buffer_new_and_alloc (MUXED_BUFFER_SIZE (mux));
    if (!gst_stm_ts_mux_queue_muxed_buffer (mux, n, buffer))
      gst_buffer_unref (buffer);
    else
      mux->buffers[n] = buffer;
  }

  return TRUE;

close_error:
  close (mux->fd);
  mux->fd = -1;
  return FALSE;
}

static void
gst_stm_ts_free_output_buffers (Gststtsmux * mux)
{
  int n;

  gst_stm_ts_mux_reqbufs (mux, mux->fd, V4L2_BUF_TYPE_DVB_CAPTURE, 0);
  for (n = 0; n < mux->nbuffers; ++n) {
    if (mux->buffers[n])
      gst_buffer_unref (mux->buffers[n]);
  }
  g_free (mux->buffers);
  mux->buffers = NULL;
  mux->nbuffers = 0;
}

static gboolean
gst_stm_ts_mux_set_streaming (Gststtsmux * mux, int fd, enum v4l2_buf_type type,
    gboolean on)
{
  int ret;

  if (on)
    ret = ioctl (fd, VIDIOC_STREAMON, &type);
  else
    ret = ioctl (fd, VIDIOC_STREAMOFF, &type);
  if (ret < 0) {
    GST_ERROR_OBJECT (mux, "Failed to set streaming %s on %s fd %d",
        on ? "on" : "off", fd == mux->fd ? "muxed" : "input", fd);
    return FALSE;
  }
  return TRUE;
}

static gboolean
gst_stm_ts_mux_ensure_streaming (Gststtsmux * mux)
{
  GstSegment segment;

  if (mux->streaming) {
    /* restart the read_task if necessary */
    GstTask *task = GST_PAD_TASK (mux->srcpad);
    if (!task || gst_task_get_state (task) != GST_TASK_STARTED) {
      GST_DEBUG_OBJECT (mux, "(Re) starting read_task");
      /* Start read task */
      if (!gst_pad_start_task (mux->srcpad,
              (GstTaskFunction) gst_stm_ts_mux_read_task, mux->srcpad, NULL)) {
        GST_ERROR_OBJECT (mux->srcpad, "Failed to restart src pad task");
        return FALSE;
      }
    }
    return TRUE;
  }

  if (!gst_stm_ts_mux_set_streaming (mux, mux->fd, V4L2_BUF_TYPE_DVB_CAPTURE,
          TRUE))
    return FALSE;

  if (!gst_pad_start_task (mux->srcpad,
          (GstTaskFunction) gst_stm_ts_mux_read_task, mux->srcpad, NULL)) {
    GST_ERROR_OBJECT (mux->srcpad, "Failed to start src pad task");
    return FALSE;
  }
  gst_object_set_name (GST_OBJECT (GST_PAD_TASK (mux->srcpad)), "tsmux");

  memset (&segment, 0, sizeof (GstSegment));
  segment.format = GST_FORMAT_BYTES;
  segment.rate = 1.0;
  segment.start = 0;
  segment.position = 0;
  segment.stop = 1;
  /* Send a canonical bytes segment downstream */
  gst_pad_push_event (mux->srcpad,
      gst_event_new_segment ((const GstSegment *) &segment));

  mux->streaming = TRUE;
  return TRUE;
}

static GstPad *
gst_stm_ts_mux_request_new_pad (GstElement * element,
    GstPadTemplate * pad_template, const gchar * name, const GstCaps * caps)
{
  Gststtsmux *mux = (Gststtsmux *) element;
  GstElementClass *klass = GST_ELEMENT_GET_CLASS (element);
  GststtsmuxPadData *pdata = NULL;
  GstPad *pad;
  int fd;

  if (GST_PAD_TEMPLATE_DIRECTION (pad_template) != GST_PAD_SINK) {
    GST_ERROR_OBJECT (element, "We only offer sink request pads");
    return NULL;
  }

  if (pad_template == gst_element_class_get_pad_template (klass, "sink_%d")) {
    GST_INFO_OBJECT (element, "Request for new pad");
  } else {
    GST_ERROR_OBJECT (element, "Invalid pad template");
    return NULL;
  }

  fd = gst_stm_ts_mux_open_device (mux, TRUE);
  if (fd < 0) {
    return NULL;
  }

  pad = gst_pad_new_from_template (pad_template, name);
  gst_pad_set_active (pad, TRUE);
  if (!gst_element_add_pad (element, pad)) {
    GST_ERROR_OBJECT (element, "Failed to add new pad");
    gst_stm_ts_mux_close_device (mux, fd);
    gst_object_unref (pad);
    return NULL;
  }

  pdata =
      (GststtsmuxPadData *) gst_collect_pads_add_pad (mux->collectpads, pad,
      sizeof (GststtsmuxPadData), NULL, FALSE);

  pdata->fd = fd;
  pdata->buffers = g_malloc0 (NUM_INPUT_BUFFERS * sizeof (GstBuffer *));
  pdata->eos = FALSE;
  gst_segment_init (&pdata->collect_data.segment, GST_FORMAT_TIME);
  mux->sinkpads = g_slist_append (mux->sinkpads, pdata);

  pdata->collect_event = (GstPadEventFunction) GST_PAD_EVENTFUNC (pad);
  gst_pad_set_event_function (pad,
      GST_DEBUG_FUNCPTR (gst_stm_ts_mux_sink_event));

  GST_INFO_OBJECT (pad, "New request pad");
  return pad;
}

static GststtsmuxPadData *
gst_stm_ts_mux_find_pad_data (Gststtsmux * mux, GstPad * pad)
{
  GSList *tmp;

  for (tmp = mux->sinkpads; tmp; tmp = g_slist_next (tmp)) {
    GststtsmuxPadData *pdata = tmp->data;
    if (pdata->collect_data.pad == pad)
      return pdata;
  }
  return NULL;
}

static void
gst_stm_ts_mux_release_pad (GstElement * element, GstPad * pad)
{
  Gststtsmux *mux = (Gststtsmux *) element;
  GststtsmuxPadData *pdata;

  GST_DEBUG_OBJECT (pad, "Pad released");

  LOCK_IO (mux);
  pdata = gst_stm_ts_mux_find_pad_data (mux, pad);
  if (pdata) {
    gst_stm_ts_mux_set_streaming (mux, pdata->fd, V4L2_BUF_TYPE_DVB_OUTPUT,
        FALSE);

    /* Dequeue all those buffers that we can */
    GST_DEBUG_OBJECT (pad, "Pad destroyed, dequeueing buffers");
    while (gst_stm_ts_mux_dequeue_ready_buffer (mux, pdata) >= 0);
    GST_DEBUG_OBJECT (pad, "Dequeueing done");

    gst_stm_ts_mux_reqbufs (mux, pdata->fd, V4L2_BUF_TYPE_DVB_OUTPUT, 0);
    g_free (pdata->buffers);
    gst_stm_ts_mux_close_device (mux, pdata->fd);

    mux->sinkpads = g_slist_remove (mux->sinkpads, pdata);
  } else {
    GST_WARNING_OBJECT (pad, "No pad data found");
  }
  UNLOCK_IO (mux);
  gst_collect_pads_remove_pad (mux->collectpads, pad);
  gst_element_remove_pad (element, pad);
}

static gboolean
gst_stm_ts_mux_get_codec_configuration (GstCaps * caps,
    unsigned int *type, unsigned int *bit_buffer_size, unsigned int *pes_type)
{
  const GstStructure *structure;
  const char *media_type;

  structure = gst_caps_get_structure (caps, 0);
  media_type = gst_structure_get_name (structure);
  if (!strcmp (media_type, "video/x-h264")) {
    *type = V4L2_STM_TSMUX_STREAM_TYPE_VIDEO_H264;
    *bit_buffer_size = 14000000;
    *pes_type = V4L2_DVB_FMT_ES;
  } else if (!strcmp (media_type, "audio/x-ac3")) {
    *type = V4L2_STM_TSMUX_STREAM_TYPE_PS_AUDIO_AC3;
    *bit_buffer_size = 256000;
    *pes_type = V4L2_DVB_FMT_ES;
  } else if (!strcmp (media_type, "audio/mpeg")) {
    *type = V4L2_STM_TSMUX_STREAM_TYPE_AUDIO_AAC;
    *bit_buffer_size = 256000;
    *pes_type = V4L2_DVB_FMT_ES;
  } else {
    GST_ERROR ("Unsupported caps: %" GST_PTR_FORMAT, caps);
    return FALSE;
  }
  return TRUE;
}

static gboolean
gst_stm_ts_mux_set_input_parameters (Gststtsmux * mux, int fd, unsigned int PID,
    unsigned int BitBufferSize)
{
  struct v4l2_ext_controls controls;
  struct v4l2_ext_control ctrl_array[] = {
    {
          .id = V4L2_CID_STM_TSMUX_INCLUDE_PCR,
          .size = 0,
          .reserved2 = {0},
          /* Default is 0 */
          .value = 0,
        },
    {
          .id = V4L2_CID_STM_TSMUX_STREAM_PID,
          .size = 0,
          .reserved2 = {0},
          /* Default is 0x100*(ProgId+1)+streamId */
          .value = PID,
        },
    {
          .id = V4L2_CID_STM_TSMUX_BIT_BUFFER_SIZE,
          .size = 0,
          .reserved2 = {0},
          /* Default is 2*1024*1024 */
          .value = BitBufferSize,
        },
  };
  int i;
  struct v4l2_ext_control *ctrl;

  controls.ctrl_class = V4L2_CTRL_CLASS_TSMUX;
  controls.count = sizeof (ctrl_array) / sizeof (struct v4l2_ext_control);
  if (controls.count == 0)
    return TRUE;
  controls.reserved[0] = 0;
  controls.reserved[1] = 0;
  controls.controls = &ctrl_array[0];

  for (i = 0; i < controls.count; i++) {
    ctrl = &controls.controls[i];
    GST_DEBUG_OBJECT (mux, "Attempting to set stream %d control %d:", PID,
        ctrl->id);
    GST_DEBUG_OBJECT (mux, "Size = %d", ctrl->size);
    if (ctrl->size != 0)
      GST_DEBUG_OBJECT (mux, "Value = %s", ctrl->string);
    else
      GST_DEBUG_OBJECT (mux, "Value = %d", ctrl->value);
  }
  if (ioctl (fd, VIDIOC_S_EXT_CTRLS, &controls) < 0) {
    GST_ERROR ("VIDIOC_S_EXT_CTRLS");
    return FALSE;
  }
  return TRUE;
}

static gboolean
gst_stm_ts_mux_configure_new_pad (Gststtsmux * mux, GststtsmuxPadData * pdata,
    GstCaps * caps)
{
  struct v4l2_format format;
  unsigned int bit_buffer_size;
  int pid;

  GST_DEBUG_OBJECT (mux, "Configuring pad for caps %" GST_PTR_FORMAT, caps);

  pid = mux->next_pid;
  while (pid == mux->pcr_pid || pid == mux->pmt_pid)
    pid++;
  if (pid > 0x1ffd) {
    GST_ERROR_OBJECT (mux, "No PID left from base %x", mux->base_pid);
    return FALSE;
  }
  mux->next_pid = pid + 1;

  memset (&format, 0, sizeof (format));
  format.type = V4L2_BUF_TYPE_DVB_OUTPUT;
  format.fmt.dvb.buffer_size = STREAM_BUFFER_SIZE;
  if (!gst_stm_ts_mux_get_codec_configuration (caps, &format.fmt.dvb.codec,
          &bit_buffer_size, &format.fmt.dvb.data_type)) {
    return FALSE;
  }
  if (ioctl (pdata->fd, VIDIOC_S_FMT, &format) < 0) {
    GST_ERROR_OBJECT (pdata->collect_data.pad, "Failed setting format: %s",
        strerror (errno));
    return FALSE;
  }

  pdata->nbuffers =
      gst_stm_ts_mux_reqbufs (mux, pdata->fd, V4L2_BUF_TYPE_DVB_OUTPUT,
      NUM_INPUT_BUFFERS);
  if (pdata->nbuffers < 0)
    return FALSE;

  if (!gst_stm_ts_mux_set_input_parameters (mux, pdata->fd, mux->next_pid,
          bit_buffer_size))
    return FALSE;

  if (!gst_stm_ts_mux_set_streaming (mux, pdata->fd, V4L2_BUF_TYPE_DVB_OUTPUT,
          TRUE))
    return FALSE;

  return TRUE;
}

static gboolean
gst_stm_ts_mux_pad_set_caps (GstPad * pad, GstCaps * caps)
{
  GststtsmuxPadData *pdata;
  Gststtsmux *mux;

  mux = GST_ST_TS_MUX (GST_PAD_PARENT (pad));

  GST_DEBUG_OBJECT (pad, "Setting caps to %" GST_PTR_FORMAT, caps);

  caps = gst_caps_intersect (caps, gst_pad_get_pad_template_caps (pad));
  if (!caps) {
    GST_ERROR_OBJECT (pad, "Caps can not intersect template caps, rejected");
    return FALSE;
  }
  GST_DEBUG_OBJECT (pad,
      "After intersection with template caps: %" GST_PTR_FORMAT, caps);

  pdata = gst_stm_ts_mux_find_pad_data (mux, pad);
  if (!pdata) {
    GST_ERROR_OBJECT (pad, "No pad data found for pad");
    gst_caps_unref (caps);
    return FALSE;
  }

  LOCK_IO (mux);
  if (!gst_stm_ts_mux_configure_new_pad (mux, pdata, caps)) {
    UNLOCK_IO (mux);
    gst_caps_unref (caps);
    return FALSE;
  }
  UNLOCK_IO (mux);

  GST_DEBUG_OBJECT (pad, "Configured with caps %" GST_PTR_FORMAT, caps);
  gst_caps_unref (caps);
  return TRUE;
}

static GstCollectData *
gst_stm_ts_mux_find_earliest_pad (Gststtsmux * mux, GstCollectPads * pads)
{
  GSList *tmp;
  GstCollectData *earliest_pad = NULL;
  GstClockTime earliest_rtime = GST_CLOCK_TIME_NONE, rtime;

  for (tmp = pads->data; tmp; tmp = g_slist_next (tmp)) {
    GstCollectData *cpdata = tmp->data;
    if (GST_COLLECT_PADS_STATE_IS_SET (cpdata, GST_COLLECT_PADS_STATE_EOS))
      continue;
    GstBuffer *buffer = gst_collect_pads_peek (pads, cpdata);
    if (buffer) {
      GstClockTime time = GST_BUFFER_TIMESTAMP (buffer);
      gst_buffer_unref (buffer);
      if (!GST_CLOCK_TIME_IS_VALID (time)) {
        earliest_rtime = GST_CLOCK_TIME_NONE;
        earliest_pad = cpdata;
      } else {
        rtime =
            gst_segment_to_running_time (&cpdata->segment, GST_FORMAT_TIME,
            time);
        GST_DEBUG_OBJECT (cpdata->pad, "rtime: %" GST_TIME_FORMAT,
            GST_TIME_ARGS (rtime));
        if (!earliest_pad || (GST_CLOCK_TIME_IS_VALID (earliest_rtime)
                && rtime < earliest_rtime)) {
          earliest_pad = cpdata;
          earliest_rtime = rtime;
        }
      }
    } else {
      GST_DEBUG_OBJECT (cpdata->pad, "No buffer available, need to wait");
      earliest_rtime = GST_CLOCK_TIME_NONE;
      earliest_pad = NULL;
      break;
    }
  }
  return earliest_pad;
}

static gboolean
gst_stm_ts_mux_is_data_ready (Gststtsmux * mux, int ms)
{
  struct pollfd fds;
  int ret;

  GST_LOG_OBJECT (mux, "Polling");
  fds.fd = mux->fd;
  fds.events = POLLIN;
  fds.revents = 0;
  ret = poll (&fds, 1, ms);
  if (ret < 0) {
    GST_ERROR_OBJECT (mux, "Error polling fd %d: %s", mux->fd,
        strerror (errno));
    return FALSE;
  } else if (ret > 0) {
    if (fds.revents & POLLERR) {
      GST_ERROR_OBJECT (mux, "Got error");
      return FALSE;
    }
    if (fds.revents & POLLHUP) {
      GST_ERROR_OBJECT (mux, "Got hangup");
      return FALSE;
    }
    if (fds.revents & POLLNVAL) {
      GST_ERROR_OBJECT (mux, "Got invalid");
      return FALSE;
    }
    if (fds.revents & POLLIN) {
      GST_DEBUG_OBJECT (mux, "Got data available");
      return TRUE;
    }
  }
  GST_LOG_OBJECT (mux, "Data not available");
  return FALSE;
}

static gboolean
gst_stm_ts_mux_read_one (Gststtsmux * mux, GstBuffer ** buffer)
{
  struct v4l2_buffer *buf;
//  GstMapInfo info_write;

  *buffer = NULL;

  GST_LOG_OBJECT (mux, "Reading from tsmux");
  buf = g_malloc0 (sizeof (struct v4l2_buffer));
  buf->type = V4L2_BUF_TYPE_DVB_CAPTURE;
  buf->memory = V4L2_MEMORY_USERPTR;
  if (ioctl (mux->fd, VIDIOC_DQBUF, buf) < 0) {
    g_free (buf);
    if (errno == EAGAIN)
      return TRUE;
    GST_ERROR_OBJECT (mux, "Error reading from tsmux fd %d: %s", mux->fd,
        strerror (errno));
    return FALSE;
  }

  GST_INFO_OBJECT (mux, "Got %u byte muxed buffer %d", buf->bytesused,
      buf->index);

  if (buf->bytesused == 0) {
    GST_INFO_OBJECT (mux, "Pausing task");
    g_free (buf);
    g_mutex_lock (&mux->eos_lock);
    gst_pad_pause_task (mux->srcpad);
    if (mux->eos_received) {
      GST_INFO_OBJECT (mux, "Sending EOS event");
      g_mutex_unlock (&mux->eos_lock);
      /* Do not reset the eos_received flag here, this will be done in PAUSED_TO_READY state change */
      return gst_pad_push_event (mux->srcpad, gst_event_new_eos ());
    } else {
      GST_INFO_OBJECT (mux, "Signalling EOS cond");
      g_cond_signal (&mux->eos_cond);
      g_mutex_unlock (&mux->eos_lock);
    }
    return TRUE;
  }
#if 0
  GST_ENCODE_BUFFER (*buffer)->fd = mux->fd;
  /* We don't use any of the rest of the buffer_t structure, and it has a v4l2_buffer
     as its first member, so we can use the pointer directly without temp malloc */
  GST_ENCODE_BUFFER (*buffer)->buffer = (struct buffer_t *) buf;
  buf->type = V4L2_BUF_TYPE_DVB_CAPTURE;

  gst_buffer_map (*buffer, &info_write, GST_MAP_WRITE);
  info_write.size = buf->bytesused;
  memcpy (info_write.data, (guint8 *) buf->m.userptr, info_write.size);
  gst_buffer_set_size (*buffer, buf->bytesused);
  GST_BUFFER_PTS (*buffer) = MPEGTIME_TO_GSTTIME (buf->timestamp.tv_usec);
  GST_BUFFER_DURATION (*buffer) = GST_CLOCK_TIME_NONE;
  gst_pad_set_caps (mux->srcpad, mux->srccaps);
  gst_buffer_unmap (*buffer, &info_write);
#endif
  return TRUE;
}

static void
gst_stm_ts_mux_read_task (GstPad * pad)
{
  Gststtsmux *mux = GST_ST_TS_MUX (GST_PAD_PARENT (pad));
  GstBuffer *buffer;
  GstFlowReturn rflow;

  GST_DEBUG_OBJECT (pad, "read task running");

  if (!gst_stm_ts_mux_is_data_ready (mux, 100))
    return;

  LOCK_IO (mux);
  if (gst_stm_ts_mux_read_one (mux, &buffer)) {
    if (buffer) {
      GST_DEBUG_OBJECT (mux->srcpad, "Pushing buffer %" GST_PTR_FORMAT, buffer);
      rflow = gst_pad_push (mux->srcpad, buffer);
      if (rflow != GST_FLOW_OK) {
        if (rflow == GST_FLOW_FLUSHING && mux->flushing) {
          /* If we are flushing, it's fine if buffers can't be pushed,
             we still want to wait till we get the last 0 size buffer */
          GST_DEBUG_OBJECT (mux, "Downstream is flushing, as we are.");
        } else {
          GST_DEBUG_OBJECT (mux->srcpad, "Pushing buffer got %s",
              gst_flow_get_name (rflow));
          mux->rflow = rflow;
          /* We pause the task, so we will not be able to receive any EOS frame
             that the PAUSED->READY code might be waiting. So we signal the eos
             condition variable now, to unblock it, since we're done (till we
             are restarted, if we end up being). */
          UNLOCK_IO (mux);
          g_mutex_lock (&mux->eos_lock);
          gst_pad_pause_task (mux->srcpad);
          g_cond_signal (&mux->eos_cond);
          g_mutex_unlock (&mux->eos_lock);
          goto beach;
        }
      } else {
        GST_DEBUG_OBJECT (mux->srcpad, "Buffer pushed succesfully");
      }
    } else {
      GST_DEBUG_OBJECT (mux, "No buffer to push");
    }
  } else {
    mux->rflow = GST_FLOW_ERROR;;
  }
  UNLOCK_IO (mux);
beach:
  GST_LOG_OBJECT (mux, "read_task exiting");
}

static int
gst_stm_ts_mux_dequeue_ready_buffer (Gststtsmux * mux,
    GststtsmuxPadData * pdata)
{
  struct v4l2_buffer buf;
  int ret;

  memset (&buf, 0, sizeof (buf));
  buf.type = V4L2_BUF_TYPE_DVB_OUTPUT;
  buf.memory = V4L2_MEMORY_USERPTR;
  UNLOCK_IO (mux);
  ret = ioctl (pdata->fd, VIDIOC_DQBUF, &buf);
  LOCK_IO (mux);
  if (ret >= 0) {
    if (!pdata->buffers[buf.index]) {
      GST_WARNING_OBJECT (mux, "Dequeued buffer was already empty");
    } else {
      gst_buffer_unref (pdata->buffers[buf.index]);
      pdata->buffers[buf.index] = NULL;
      return buf.index;
    }
  }
  return -1;
}

static gboolean
gst_stm_ts_mux_write (Gststtsmux * mux, GststtsmuxPadData * pdata, int slot)
{
  struct v4l2_format format;
  struct v4l2_buffer buf;
  struct v4l2_tsmux_src_es_metadata es_meta;
  GstBuffer *buffer = pdata->buffers[slot];
  int ret;
  GstMapInfo info_write;
  gboolean res = TRUE;

  memset (&format, 0, sizeof (format));
  format.type = V4L2_BUF_TYPE_DVB_OUTPUT;
  ret = ioctl (pdata->fd, VIDIOC_G_FMT, &format);
  if (ret < 0) {
    GST_ERROR_OBJECT (pdata->collect_data.pad, "Error getting format: %s",
        strerror (errno));
    return FALSE;
  }

  memset (&buf, 0, sizeof (buf));
  gst_buffer_map (buffer, &info_write, GST_MAP_WRITE);
  buf.index = slot;
  buf.type = V4L2_BUF_TYPE_DVB_OUTPUT;
  buf.memory = V4L2_MEMORY_USERPTR;
  buf.m.userptr = (unsigned long) info_write.data;
  buf.length = info_write.size;
  buf.bytesused = info_write.size;
  if (GST_BUFFER_TIMESTAMP_IS_VALID (buffer)) {
    buf.timestamp.tv_usec = GSTTIME_TO_MPEGTIME (GST_BUFFER_TIMESTAMP (buffer));

    /* G_FMT does not give us the data type we set in S_FMT, so stuff
       in the metadata all the time since we know we're using ES. */
    switch (format.fmt.dvb.data_type) {
      default:
      case V4L2_DVB_FMT_ES:
      case V4L2_DVB_FMT_PES:
        es_meta.DTS = buf.timestamp.tv_usec;
        es_meta.dit_transition =
            GST_BUFFER_FLAG_IS_SET (buffer, GST_BUFFER_FLAG_DISCONT);
        buf.reserved = (int) &es_meta;
        break;
    }
  } else {
    GST_WARNING_OBJECT (pdata->collect_data.pad,
        "No timestamp for incoming buffer");
  }

  GST_DEBUG_OBJECT (pdata->collect_data.pad,
      "Writing %u bytes on buffer %d, ts %" GST_TIME_FORMAT, buf.bytesused,
      buf.index, GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (buffer)));
  ret = ioctl (pdata->fd, VIDIOC_QBUF, &buf);
  if (ret < 0) {
    GST_ERROR_OBJECT (pdata->collect_data.pad, "Failed to queue buffer: %s",
        strerror (errno));
    res = FALSE;
  }

  gst_buffer_unmap (buffer, &info_write);
  return res;
}

static int
gst_stm_ts_mux_find_free_buffer_slot (GststtsmuxPadData * pdata)
{
  int n;

  for (n = 0; n < pdata->nbuffers; ++n)
    if (!pdata->buffers[n])
      return n;
  return -1;
}

static gboolean
gst_stm_ts_mux_reap_eos (Gststtsmux * mux, GstCollectPads * pads,
    gboolean force)
{
  GSList *tmp;
  int slot;
  gboolean res = TRUE;
  gboolean all_eos = TRUE;

  for (tmp = pads->data; tmp; tmp = g_slist_next (tmp)) {
    GstCollectData *cpdata = tmp->data;
    if (force
        || GST_COLLECT_PADS_STATE_IS_SET (cpdata, GST_COLLECT_PADS_STATE_EOS)) {
      GststtsmuxPadData *pdata = (GststtsmuxPadData *) cpdata;
      if (!pdata->eos) {
        if (GST_COLLECT_PADS_STATE_IS_SET (cpdata, GST_COLLECT_PADS_STATE_EOS))
          GST_INFO_OBJECT (cpdata->pad, "EOS");
        else
          GST_INFO_OBJECT (cpdata->pad, "Forced EOS");

        slot = gst_stm_ts_mux_find_free_buffer_slot (pdata);
        if (slot < 0) {
          GST_DEBUG_OBJECT (cpdata->pad,
              "No free buffer, trying to dequeue one");
          slot = gst_stm_ts_mux_dequeue_ready_buffer (mux, pdata);
          if (slot >= 0) {
            GST_DEBUG_OBJECT (cpdata->pad, "Dequeued buffer %d", slot);
          }
        }
        if (slot < 0) {
          GST_ERROR_OBJECT (cpdata->pad, "No free buffer to queue");
          res = FALSE;
          continue;
        } else {
          GstBuffer *buffer = gst_buffer_new_and_alloc (1);
          gst_buffer_set_size (buffer, 0);
          pdata->buffers[slot] = buffer;
          if (!gst_stm_ts_mux_write (mux, pdata, slot)) {
            res = FALSE;
            continue;
          }
          GST_INFO_OBJECT (cpdata->pad, "EOS buffer sent");
          pdata->eos = TRUE;
        }
      }
    } else {
      all_eos = FALSE;
    }
  }
  if (all_eos)
    mux->eos_received = TRUE;
  return res;
}

static GstFlowReturn
gst_stm_ts_mux_collected (GstCollectPads * pads, Gststtsmux * mux)
{
  GstCollectData *earliest;
  GstFlowReturn rflow = GST_FLOW_OK;

  GST_DEBUG_OBJECT (mux, "collected");

  LOCK_IO (mux);

  if (mux->rflow != GST_FLOW_OK) {
    rflow = mux->rflow;
    goto error;
  }

  /* On the first buffer received, we need to kick the muxer, as it will
     only be able to start after all request pads have been added */
  if (!gst_stm_ts_mux_ensure_streaming (mux)) {
    rflow = GST_FLOW_ERROR;
    goto error;
  }

  while ((earliest = gst_stm_ts_mux_find_earliest_pad (mux, pads))) {
    GststtsmuxPadData *pdata = (GststtsmuxPadData *) earliest;
    int slot = gst_stm_ts_mux_find_free_buffer_slot (pdata);
    if (slot < 0) {
      GST_DEBUG_OBJECT (earliest->pad, "No free buffer, trying to dequeue one");
      slot = gst_stm_ts_mux_dequeue_ready_buffer (mux, pdata);
      if (slot >= 0) {
        GST_DEBUG_OBJECT (earliest->pad, "Dequeued buffer %d", slot);
      }
    }
    if (slot < 0) {
      GST_ERROR_OBJECT (earliest->pad, "No free buffer to queue");
      rflow = GST_FLOW_ERROR;
      goto error;
    } else {
      GstBuffer *buffer = gst_collect_pads_pop (pads, earliest);
      if (buffer) {
#ifdef COPY_ES_BUFFERS
        GstBuffer *copy = gst_buffer_copy (buffer);
        gst_buffer_unref (buffer);
        buffer = copy;
#endif
        pdata->buffers[slot] = buffer;
        if (!gst_stm_ts_mux_write (mux, pdata, slot)) {
          rflow = GST_FLOW_ERROR;
          goto error;
        }
      }
    }
  }

  if (!gst_stm_ts_mux_reap_eos (mux, pads, FALSE)) {
    rflow = GST_FLOW_ERROR;
    goto error;
  }

  UNLOCK_IO (mux);
  return GST_FLOW_OK;

error:
  gst_pad_stop_task (mux->srcpad);
  UNLOCK_IO (mux);
  return rflow;
}

static void
gst_stm_ts_mux_finalize (GObject * object)
{
  Gststtsmux *mux = (Gststtsmux *) object;

  GST_LOG_OBJECT (mux, "gst_stm_ts_mux_finalize");
  gst_object_unref (mux->collectpads);
  if (mux->srccaps)
    gst_caps_unref (mux->srccaps);
  g_slist_free (mux->sinkpads);

  g_mutex_clear (&mux->lock);
  g_mutex_clear (&mux->eos_lock);
  g_cond_clear (&mux->eos_cond);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_stm_ts_mux_reset (Gststtsmux * mux)
{
  mux->fd = -1;
  mux->dev_id = 0;
  mux->streaming = FALSE;
  mux->rflow = GST_FLOW_OK;
  mux->next_pid = mux->base_pid;
  if (mux->srccaps) {
    gst_caps_unref (mux->srccaps);
    mux->srccaps = NULL;
  }
  g_slist_free (mux->sinkpads);
  mux->sinkpads = NULL;
  mux->eos_received = FALSE;
  mux->flushing = FALSE;
}

static GstStateChangeReturn
gst_stm_ts_mux_change_state (GstElement * element, GstStateChange transition)
{
  Gststtsmux *mux = GST_ST_TS_MUX (element);
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
  GstTask *task;

  GST_INFO_OBJECT (mux, "State change: %s -> %s",
      gst_element_state_get_name (GST_STATE_TRANSITION_CURRENT (transition)),
      gst_element_state_get_name (GST_STATE_TRANSITION_NEXT (transition)));

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      if (!gst_stm_ts_mux_negotiate_downstream (mux))
        return GST_STATE_CHANGE_FAILURE;
      if (!gst_stm_ts_mux_setup_capture (mux))
        return GST_STATE_CHANGE_FAILURE;
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      GST_DEBUG_OBJECT (mux, "Starting collectpads");
      gst_collect_pads_start (mux->collectpads);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      /* Push a EOS to be sure it will unblock the read task */
      LOCK_IO (mux);
      g_mutex_lock (&mux->eos_lock);
      gst_collect_pads_stop (mux->collectpads);
      task = GST_PAD_TASK (mux->srcpad);
      if (task && gst_task_get_state (task) == GST_TASK_STARTED) {
        if (mux->eos_received == FALSE) {
          GST_INFO_OBJECT (mux, "PAUSED->READY and no EOS, forcing EOS");
          if (gst_stm_ts_mux_reap_eos (mux, mux->collectpads, TRUE)) {
            UNLOCK_IO (mux);
            g_cond_wait (&mux->eos_cond, &mux->eos_lock);
            LOCK_IO (mux);
          } else {
            if (!gst_pad_stop_task (mux->srcpad)) {
              GST_ERROR_OBJECT (mux, "Failed to stop src pad task");
            }
          }
        } else {
          mux->eos_received = FALSE;
        }
      }
      g_mutex_unlock (&mux->eos_lock);
      UNLOCK_IO (mux);

      if (mux->streaming) {
        if (!gst_stm_ts_mux_set_streaming (mux, mux->fd,
                V4L2_BUF_TYPE_DVB_CAPTURE, FALSE)) {
          GST_ERROR_OBJECT (mux, "Failed to stop streaming on capture device");
        }
        mux->streaming = FALSE;
      }
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
  if (ret == GST_STATE_CHANGE_FAILURE)
    return ret;

  switch (transition) {
    case GST_STATE_CHANGE_READY_TO_NULL:
      gst_stm_ts_free_output_buffers (mux);
      gst_stm_ts_mux_close_device (mux, mux->fd);
      mux->fd = -1;
      break;
    default:
      break;
  }

  return ret;
}

static gboolean
gst_stm_ts_mux_flush (Gststtsmux * mux, GstPad * pad)
{
  GststtsmuxPadData *pdata;
  int slot;
  gboolean res = TRUE;

  pdata = gst_stm_ts_mux_find_pad_data (mux, pad);
  if (!pdata) {
    GST_ERROR_OBJECT (pad, "No pdata ??");
    return FALSE;
  }

  slot = gst_stm_ts_mux_find_free_buffer_slot (pdata);
  if (slot < 0) {
    GST_DEBUG_OBJECT (pad, "No free buffer, trying to dequeue one");
    slot = gst_stm_ts_mux_dequeue_ready_buffer (mux, pdata);
    if (slot >= 0) {
      GST_DEBUG_OBJECT (pad, "Dequeued buffer %d", slot);
    }
  }
  if (slot < 0) {
    GST_ERROR_OBJECT (pad, "No free buffer to queue");
    res = FALSE;
  } else {
    GstBuffer *buffer = gst_buffer_new_and_alloc (1);
    gst_buffer_set_size (buffer, 0);
    pdata->buffers[slot] = buffer;
    if (!gst_stm_ts_mux_write (mux, pdata, slot)) {
      res = FALSE;
    }
    GST_INFO_OBJECT (pad, "EOS buffer sent");
  }
  return res;
}

static gboolean
gst_stm_ts_mux_are_all_pads_flushing (Gststtsmux * mux,
    GstPad * assume_flushing)
{
  GSList *tmp;

  for (tmp = mux->collectpads->data; tmp; tmp = g_slist_next (tmp)) {
    GstCollectData *cpdata = tmp->data;
    if (cpdata->pad == assume_flushing)
      continue;
    if (!GST_COLLECT_PADS_STATE_IS_SET (cpdata,
            GST_COLLECT_PADS_STATE_FLUSHING)) {
      return FALSE;
    }
  }
  return TRUE;
}

static gboolean
gst_stm_ts_mux_sink_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
  Gststtsmux *mux = GST_ST_TS_MUX (gst_pad_get_parent (pad));
  gboolean ret = TRUE;
  gboolean wait;
  GstTask *task;
  GststtsmuxPadData *pdata;
  GstCaps *caps;

  GST_DEBUG_OBJECT (pad, "Got %s event", GST_EVENT_TYPE_NAME (event));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_FLUSH_START:
      LOCK_IO (mux);
      g_mutex_lock (&mux->eos_lock);
      mux->flushing = TRUE;
      ret = gst_stm_ts_mux_flush (mux, pad);
      if (ret == FALSE) {
        GST_DEBUG_OBJECT (mux, "flush failed");
      }
      /* When all sink pads are flushing, we must wait for the read task
         to have received and processed the EOS buffer from the kernel. */
      wait = gst_stm_ts_mux_are_all_pads_flushing (mux, pad);
      g_mutex_unlock (&mux->eos_lock);
      UNLOCK_IO (mux);
      if (wait) {
        GST_DEBUG_OBJECT (mux,
            "All sink pads are now flushing, waiting for read task");
        g_mutex_lock (&mux->eos_lock);
        task = GST_PAD_TASK (mux->srcpad);
        if (task && gst_task_get_state (task) == GST_TASK_STARTED) {
          GST_DEBUG_OBJECT (mux, "Waiting for read task");
          g_cond_wait (&mux->eos_cond, &mux->eos_lock);
          GST_DEBUG_OBJECT (mux, "read task has flushed");
        } else {
          GST_DEBUG_OBJECT (mux, "read task is not started, no need to wait");
        }
        g_mutex_unlock (&mux->eos_lock);
      }
      break;
    case GST_EVENT_FLUSH_STOP:
      mux->rflow = GST_FLOW_OK;
      mux->flushing = FALSE;
      break;
    case GST_EVENT_CAPS:
      gst_event_parse_caps (event, &caps);
      if ((mux->prev_caps != NULL)
          && (gst_caps_is_equal ((const GstCaps *) caps,
                  (const GstCaps *) mux->prev_caps))) {
        GST_DEBUG_OBJECT (mux, "caps are already set");
        return TRUE;
      }
      ret = gst_stm_ts_mux_pad_set_caps (pad, caps);
      if (ret) {
        if (mux->prev_caps) {
          gst_caps_unref (mux->prev_caps);
        }
        mux->prev_caps = gst_caps_ref (caps);
      } else {
        GST_ERROR_OBJECT (mux, "failed to set caps!");
      }

      gst_event_unref (event);
      goto done;
    default:
      break;
  }

  /* now GstCollectPad can take care of the rest, e.g. EOS */
  pdata = gst_stm_ts_mux_find_pad_data (mux, pad);
  if (pdata) {
    GST_DEBUG_OBJECT (pad, "Calling original event function");
    ret = pdata->collect_event (pad, parent, event);
    GST_DEBUG_OBJECT (pad, "Back from original event function, ret %d", ret);
  } else {
    GST_ERROR_OBJECT (pad, "No pad data ?");
    ret = FALSE;
  }

done:
  gst_object_unref (mux);
  return ret;
}

gboolean
sttsmux_init (GstPlugin * plugin)
{
  GST_DEBUG_CATEGORY_INIT (gst_stm_ts_mux_debug, "stts_mux", 0,
      "ST Muxer for TS stream");

  return gst_element_register (plugin, "stts_mux", (GST_RANK_SECONDARY),
      GST_TYPE_ST_TS_MUX);
}
