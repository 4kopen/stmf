/* GStreamer
 * Copyright (C) <1999> Erik Walthinsen <omega@cse.ogi.edu>
 * Copyright (C) <2003> David Schleef <ds@schleef.org>
 * Copyright (C) <2006> Julien Moutte <julien@moutte.net>
 * Copyright (C) <2006> Zeeshan Ali <zeeshan.ali@nokia.com>
 * Copyright (C) <2006-2008> Tim-Philipp Müller <tim centricular net>
 * Copyright (C) <2009> Young-Ho Cha <ganadist@gmail.com>
 * Copyright (C) <2012> STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * This file has been modified by STMicroelectronics, on 03/13/2012
 */

/**
 * SECTION:element-sttextoverlay
 *
 * Description: sttextoverlay element renders text on top of a video stream to replace
 * gstreamer native textoverlay element.
 * Only Upstream video elemented stvideo (whose source caps are video/x-fake-yuv) will be linked with sttextoverlay
 * Upstream element whose source caps is video/x-raw-yuv will still be linked to gstreamer textoverlay element
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch filesrc location=subtitle.avi ! stavi_demux name=demux demux. ! queue ! stvideo ! sttextoverlay text="sttextoverlay" ! ffmpegcolorspace ! stdisplaysink demux. ! queue ! staudio ! fakesink
 * gst-launch filesrc location=subtitle.mkv ! matroskademux name=demux demux. ! queue ! stvideo ! queue ! txt. filesrc location=subtitle.srt ! subparse ! queue ! sttextoverlay name=txt ! queue ! ffmpegcolorspace ! stdisplaysink demux. ! queue ! staudio ! queue ! fakesink
 * ]|
 * </refsect2>
 */

#include <glib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/dvb/video.h>
#include <linux/dvb/stm_ioctls.h>
#include <linux/videodev2.h>
#include <linux/stm/stmedia_export.h>
#include "gststtextoverlay.h"
#include "v4l2_utils.h"

/* V4L calls for user data extraction */
gint
gst_sttext_init_v4l_extract_ud (GstSTTextOverlay * overlay)
{
  gint ret = -1;
  gint error = GST_STTEXT_ERROR_UNKNOWN;
  struct v4l2_format fmt;
  struct v4l2_requestbuffers reqbuf;

  overlay->videofd =
      v4l2_open_by_name (V4L2_CAPTURE_DRIVER_NAME, V4L2_CAPTURE_CARD_NAME,
      O_RDWR | O_NONBLOCK);
  if (overlay->videofd < 0) {
    GST_ERROR_OBJECT (overlay, "Couldn't open video device %s - %s ",
        V4L2_CAPTURE_DRIVER_NAME, strerror (errno));
    return GST_STTEXT_ERROR_OPENING_DEVICE;
  }

  ret = v4l2_set_input_by_name (overlay->videofd, "dvb0.video0");
  if (ret < 0) {
    GST_ERROR_OBJECT (overlay, "Couldn't set device dvb0.video0 for input - %s",
        strerror (errno));
    return (error);
  }

  memset (&fmt, 0, sizeof (fmt));
  fmt.type = V4L2_BUF_TYPE_USER_DATA_CAPTURE;

  if ((error =
          ioctl (overlay->videofd, VIDIOC_S_FMT,
              &fmt)) != GST_STTEXT_NO_ERROR) {
    GST_ERROR_OBJECT (overlay, "Couldn't set height width or format - %s",
        strerror (errno));
    return (error);
  }

  memset (&reqbuf, 0, sizeof (struct v4l2_requestbuffers));
  reqbuf.memory = V4L2_MEMORY_USERPTR;
  reqbuf.type = V4L2_BUF_TYPE_USER_DATA_CAPTURE;
  reqbuf.count = 1;

  if (ioctl (overlay->videofd, VIDIOC_REQBUFS, &reqbuf) < 0) {
    GST_ERROR_OBJECT (overlay, "VIDIOC_REQBUFS %s", strerror (errno));
    return FALSE;
  }

  if ((error = gst_sttext_stream_on (overlay)) != GST_STTEXT_NO_ERROR) {
    GST_ERROR_OBJECT (overlay, "VIDIOC_STREAMON failed - %s", strerror (errno));
    return (error);
  }
  return GST_STTEXT_NO_ERROR;
}

void
gst_sttext_term_v4l_extract_ud (GstSTTextOverlay * overlay)
{
  if (overlay->videofd) {
    gst_sttext_stream_off (overlay);
    if (close (overlay->videofd) != GST_STTEXT_NO_ERROR) {
      GST_ERROR_OBJECT (overlay, "Couldn't close video device %s",
          strerror (errno));
    }
  }
}

void
gst_sttext_queue_buffer (GstSTTextOverlay * overlay, void *ptr)
{
  struct v4l2_buffer buf;

  memset (&buf, 0, sizeof (buf));

  buf.type = V4L2_BUF_TYPE_USER_DATA_CAPTURE;
  buf.memory = V4L2_MEMORY_USERPTR;
  buf.m.userptr = (unsigned long) ptr;
  buf.field = 0;
  buf.length = MAX_UD_BLOCK_LEN;
  if (ioctl (overlay->videofd, VIDIOC_QBUF,
          &buf) == GST_STTEXT_ERROR_QUEUE_BUFFER) {
    GST_ERROR_OBJECT (overlay, "Couldn't queue buffer for user data %s",
        strerror (errno));
  }
}

gint8
gst_sttext_dequeue_buffer (GstSTTextOverlay * overlay, GstUserData * user_data)
{
  struct v4l2_buffer buf;

  memset (&buf, 0, sizeof (buf));

  buf.type = V4L2_BUF_TYPE_USER_DATA_CAPTURE;
  buf.memory = V4L2_MEMORY_USERPTR;
  if (ioctl (overlay->videofd, VIDIOC_DQBUF,
          &buf) == GST_STTEXT_ERROR_DEQUEUE_BUFFER) {
    GST_ERROR_OBJECT (overlay, "Couldn't de-queue user buffer - %s",
        strerror (errno));
    return GST_STTEXT_ERROR_DEQUEUE_BUFFER;
  }

  user_data->user_data_ptr = (guint8 *) buf.m.userptr;
  user_data->user_data_length = buf.bytesused;
  return 0;
}

gint
gst_sttext_stream_on (GstSTTextOverlay * overlay)
{
  struct v4l2_buffer buf;
  gint error = 0;

  memset (&buf, 0, sizeof (buf));
  buf.type = V4L2_BUF_TYPE_USER_DATA_CAPTURE;
  if ((error = ioctl (overlay->videofd, VIDIOC_STREAMON, &buf.type)) < 0) {
    GST_ERROR_OBJECT (overlay, "Unable to start stream %s", strerror (errno));
    return (error);
  }
  return (GST_STTEXT_NO_ERROR);
}

gint
gst_sttext_stream_off (GstSTTextOverlay * overlay)
{
  struct v4l2_buffer buf;
  gint error = 0;

  memset (&buf, 0, sizeof (buf));
  buf.type = V4L2_BUF_TYPE_USER_DATA_CAPTURE;

  if ((error = ioctl (overlay->videofd, VIDIOC_STREAMOFF, &buf.type)) < 0) {
    GST_ERROR_OBJECT (overlay, "Unable to stop stream %s", strerror (errno));
    return (error);
  }
  return (GST_STTEXT_NO_ERROR);
}
