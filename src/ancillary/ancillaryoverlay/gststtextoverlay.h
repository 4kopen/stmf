/* GStreamer
 * Copyright (C) <1999> Erik Walthinsen <omega@cse.ogi.edu>
 * Copyright (C) <2003> David Schleef <ds@schleef.org>
 * Copyright (C) <2006> Julien Moutte <julien@moutte.net>
 * Copyright (C) <2006> Zeeshan Ali <zeeshan.ali@nokia.com>
 * Copyright (C) <2006-2008> Tim-Philipp Müller <tim centricular net>
 * Copyright (C) <2009> Young-Ho Cha <ganadist@gmail.com>
 * Copyright (C) <2012> STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * This file has been modified by STMicroelectronics, on 03/13/2012
 */

#ifndef __GSTSTTEXTOVERLAY_H__
#define __GSTSTTEXTOVERLAY_H__

#include <gst/gst.h>
#include <gst/video/video.h>
#include <pango/pangocairo.h>
#include "stdvb-sub.h"
#include "gstpesfilter.h"
#include <libzvbi.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "gst_stm_closedcaption.h"
G_BEGIN_DECLS
#define MPEGTS_MIN_PES_BUFFER_SIZE     256 * 1024
#define MAX_UD_BLOCK_LEN               2048
#define GST_TYPE_STTEXT_OVERLAY            (gst_sttext_overlay_get_type())
#define GST_STTEXT_OVERLAY(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj),\
                                            GST_TYPE_STTEXT_OVERLAY, GstSTTextOverlay))
#define GST_STTEXT_OVERLAY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass),\
                                            GST_TYPE_STTEXT_OVERLAY,GstSTTextOverlayClass))
#define GST_STTEXT_OVERLAY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),\
                                            GST_TYPE_STTEXT_OVERLAY, GstSTTextOverlayClass))
#define GST_IS_STTEXT_OVERLAY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj),\
                                            GST_TYPE_STTEXT_OVERLAY))
#define GST_IS_STTEXT_OVERLAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),\
                                            GST_TYPE_STTEXT_OVERLAY))
#define GST_STTEXT_OVERLAY_GET_COND(ov) (&((GstSTTextOverlay *)ov)->cond)
#define GST_STTEXT_OVERLAY_WAIT(ov)     (g_cond_wait (GST_STTEXT_OVERLAY_GET_COND (ov), GST_OBJECT_GET_LOCK (ov)))
#define GST_STTEXT_OVERLAY_BROADCAST(ov)(g_cond_broadcast (GST_STTEXT_OVERLAY_GET_COND (ov)))
#define GST_STTEXT_DECODE_GET_COND(ov) (&((GstSTTextOverlay *)ov)->decode_cond)
#define GST_STTEXT_DECODE_WAIT(ov)     (g_cond_wait (GST_STTEXT_DECODE_GET_COND (ov), GST_OBJECT_GET_LOCK (ov)))
#define GST_STTEXT_DECODE_BROADCAST(ov)(g_cond_broadcast (GST_STTEXT_DECODE_GET_COND (ov)))
#define DEFAULT_PROP_TEXT 	""
#define DEFAULT_PROP_SHADING	FALSE
#define DEFAULT_PROP_VALIGNMENT	GST_STTEXT_OVERLAY_VALIGN_BASELINE
#define DEFAULT_PROP_HALIGNMENT	GST_STTEXT_OVERLAY_HALIGN_CENTER
#define DEFAULT_PROP_VALIGN	"baseline"
#define DEFAULT_PROP_HALIGN	"center"
#define DEFAULT_PROP_XPAD	25
#define DEFAULT_PROP_YPAD	25
#define DEFAULT_PROP_DELTAX	0
#define DEFAULT_PROP_DELTAY	0
#define DEFAULT_PROP_XPOS       0.5
#define DEFAULT_PROP_YPOS       0.5
#define DEFAULT_PROP_WRAP_MODE  GST_STTEXT_OVERLAY_WRAP_MODE_WORD_CHAR
#define DEFAULT_PROP_FONT_DESC	""
#define DEFAULT_PROP_SILENT	FALSE
#define DEFAULT_PROP_LINE_ALIGNMENT GST_STTEXT_OVERLAY_LINE_ALIGN_CENTER
#define DEFAULT_PROP_WAIT_TEXT	TRUE
#define DEFAULT_PROP_AUTO_ADJUST_SIZE TRUE
#define DEFAULT_PROP_VERTICAL_RENDER  FALSE
#define DEFAULT_PROP_COLOR      0xffffffff
/* make a property of me */
#define DEFAULT_SHADING_VALUE    -80
/* Width for side black panels for teletext (in characters for width
 * and rows for height )
 */
#define TELETEXT_MAX_SIDE_BLACK_PANE_WIDTH    1
#define CC_MAX_SIDE_BLACK_PANE_WIDTH          2
#define TELETEXT_WIDTH_OF_CHARACTER           12
#define TELETEXT_HEIGHT_OF_CHARACTER          10
#define CC_WIDTH_OF_CHARACTER                 16
#define CC_HEIGHT_OF_CHARACTER                26
typedef struct _GstUserData GstUserData;
typedef struct _GstSTTextOverlay GstSTTextOverlay;
typedef struct _GstSTTextOverlayClass GstSTTextOverlayClass;
typedef struct _GstTeletextDec GstTeletextDec;
typedef struct _GstTeletextFrame GstTeletextFrame;
typedef struct _GstAncillarySem GstAncillarySem;
typedef struct _GstTeletextInfo GstTeletextInfo;

/**
 * GstSTTextOverlayVAlign:
 * @GST_STTEXT_OVERLAY_VALIGN_BASELINE: draw text on the baseline
 * @GST_STTEXT_OVERLAY_VALIGN_BOTTOM: draw text on the bottom
 * @GST_STTEXT_OVERLAY_VALIGN_TOP: draw test on top
 *
 * Vertical alignment of the text.
 */
typedef enum
{
  GST_STTEXT_OVERLAY_VALIGN_BASELINE,
  GST_STTEXT_OVERLAY_VALIGN_BOTTOM,
  GST_STTEXT_OVERLAY_VALIGN_TOP,
  GST_STTEXT_OVERLAY_VALIGN_POS
} GstSTTextOverlayVAlign;

/**
 * GstSTTextOverlayHAlign:
 * @GST_STTEXT_OVERLAY_HALIGN_LEFT: align text left
 * @GST_STTEXT_OVERLAY_HALIGN_CENTER: align text center
 * @GST_STTEXT_OVERLAY_HALIGN_RIGHT: align text right
 *
 * Horizontal alignment of the text.
 */
typedef enum
{
  GST_STTEXT_OVERLAY_HALIGN_LEFT,
  GST_STTEXT_OVERLAY_HALIGN_CENTER,
  GST_STTEXT_OVERLAY_HALIGN_RIGHT,
  GST_STTEXT_OVERLAY_HALIGN_TOP,
  GST_STTEXT_OVERLAY_HALIGN_POS
} GstSTTextOverlayHAlign;

/**
 * GstSTTextOverlayWrapMode:
 * @GST_STTEXT_OVERLAY_WRAP_MODE_NONE: no wrapping
 * @GST_STTEXT_OVERLAY_WRAP_MODE_WORD: do word wrapping
 * @GST_STTEXT_OVERLAY_WRAP_MODE_CHAR: do char wrapping
 * @GST_STTEXT_OVERLAY_WRAP_MODE_WORD_CHAR: do word and char wrapping
 *
 * Whether to wrap the text and if so how.
 */
typedef enum
{
  GST_STTEXT_OVERLAY_WRAP_MODE_NONE = -1,
  GST_STTEXT_OVERLAY_WRAP_MODE_WORD = PANGO_WRAP_WORD,
  GST_STTEXT_OVERLAY_WRAP_MODE_CHAR = PANGO_WRAP_CHAR,
  GST_STTEXT_OVERLAY_WRAP_MODE_WORD_CHAR = PANGO_WRAP_WORD_CHAR
} GstSTTextOverlayWrapMode;

/**
 * GstSTTextOverlayLineAlign:
 * @GST_STTEXT_OVERLAY_LINE_ALIGN_LEFT: lines are left-aligned
 * @GST_STTEXT_OVERLAY_LINE_ALIGN_CENTER: lines are center-aligned
 * @GST_STTEXT_OVERLAY_LINE_ALIGN_RIGHT: lines are right-aligned
 *
 * Alignment of text lines relative to each other
 */
typedef enum
{
  GST_STTEXT_OVERLAY_LINE_ALIGN_LEFT = PANGO_ALIGN_LEFT,
  GST_STTEXT_OVERLAY_LINE_ALIGN_CENTER = PANGO_ALIGN_CENTER,
  GST_STTEXT_OVERLAY_LINE_ALIGN_RIGHT = PANGO_ALIGN_RIGHT
} GstSTTextOverlayLineAlign;

/**
 * GstSTTextCCState:
 * @GST_STTEXT_CC_STATE_IDLE: task state is idle.
 * @GST_STTEXT_CC_STATE_INIT: initialize closed caption.
 * @GST_STTEXT_CC_STATE_GET_DATA: get input closed caption data.
 * @GST_STTEXT_CC_STATE_PARSE_DATA: parse the data.
 * @GST_STTEXT_CC_STATE_DECODE_DATA: decode data.
 * @GST_STTEXT_CC_STATE_DRAW_PAGE: draw a page with decoded closed caption content.
 * @GST_STTEXT_CC_STATE_TERM: terminate closed caption service
 *
 * Closed Caption task states
 */
typedef enum
{
  GST_STTEXT_CC_STATE_IDLE,
  GST_STTEXT_CC_STATE_INIT,
  GST_STTEXT_CC_STATE_GET_DATA,
  GST_STTEXT_CC_STATE_PARSE_DATA,
  GST_STTEXT_CC_STATE_DECODE_DATA,
  GST_STTEXT_CC_STATE_DRAW_PAGE,
  GST_STTEXT_CC_STATE_TERM
} GstSTTextCCState;

/**
 * GstSTTextCCCmd:
 * @GST_STTEXT_CC_CMD_START: start closed caption service.
 * @GST_STTEXT_CC_CMD_STOP: stop closed caption service.
 * @GST_STTEXT_CC_CMD_TERM: terminate closed caption service.
 *
 * Commands for closed caption service
 */
typedef enum
{
  GST_STTEXT_CC_CMD_START,
  GST_STTEXT_CC_CMD_STOP,
  GST_STTEXT_CC_CMD_TERM
} GstSTTextCCCmd;

/**
 * GstSttextError:
 * @GST_STTEXT_NO_ERROR: no error.
 * @GST_STTEXT_ERROR_OPENING_DEVICE: error in opening device.
 * @GST_STTEXT_ERROR_UNKNOWN: unknown error.
 * @GST_STTEXT_ERROR_QUEUE_BUFFER: error in buffer enqueue.
 * @GST_STTEXT_ERROR_DEQUEUE_BUFFER: error in buffer dequeue.
 * @GST_STTEXT_ERROR_ENUM_INPUT: error in input.
 *
 * error codes for sttext
 */
typedef enum
{
  GST_STTEXT_NO_ERROR = 0,
  GST_STTEXT_ERROR_OPENING_DEVICE = -1,
  GST_STTEXT_ERROR_UNKNOWN = -1,
  GST_STTEXT_ERROR_QUEUE_BUFFER = -1,
  GST_STTEXT_ERROR_DEQUEUE_BUFFER = -1,
  GST_STTEXT_ERROR_ENUM_INPUT = -2
} GstSttextError;

/**
 * GstSTTextOverlayType:
 * @GST_STTEXT_OVERLAY_UNKNOWN: unknown overlay type.
 * @GST_STTEXT_OVERLAY_FILE_SUBTITLE: file subtitle to be decoded.
 * @GST_STTEXT_OVERLAY_DVB_SUBTITLE: dvb subtitle to be decoded.
 * @GST_STTEXT_OVERLAY_TELETEXT: teletext to be decode.
 * @GST_STTEXT_OVERLAY_CC: closed caption to be decoded.
 *
 * overlay type to be used
 */

typedef enum
{
  GST_STTEXT_OVERLAY_UNKNOWN,
  GST_STTEXT_OVERLAY_FILE_SUBTITLE,
  GST_STTEXT_OVERLAY_DVB_SUBTITLE,
  GST_STTEXT_OVERLAY_TELETEXT,
  GST_STTEXT_OVERLAY_CC
} GstSTTextOverlayType;

/**
 * _GstTeletextInfo:
 *
 * structure for teletext page information
 */
struct _GstTeletextInfo
{
  GstClockTime pts;
  vbi_page *page;
};

/**
 * _GstUserData:
 *
 * structure for user data
 */
struct _GstUserData
{
  guint8 *user_data_ptr;
  guint32 user_data_length;
  guint32 user_data_processed;
};

/**
 * _GstTeletextDec:
 *
 * structure for vbi decoding
 */
struct _GstTeletextDec
{
  GstElement element;

  GstPad *sinkpad;
  GstPad *srcpad;

  GstClockTime in_timestamp;
  GstClockTime in_duration;
  gint rate_numerator;
  gint rate_denominator;

  /* Props */
  gint pageno;
  gint subno;
  gchar *font_description;
  gboolean subtitles_mode;
  gchar *subtitles_template;
  vbi_decoder *decoder;
  GQueue *queue;
  GMutex queue_lock;

  GstTeletextFrame *frame;
  float last_ts;
  vbi_page *page;
};

/**
 * _GstTeletextFrame:
 *
 * structure for vbi frame
 */
struct _GstTeletextFrame
{
  vbi_sliced *sliced_begin;
  vbi_sliced *sliced_end;
  vbi_sliced *current_slice;

  guint last_field;
  guint last_field_line;
  guint last_frame_line;
};

/**
 * _GstAncillarySem:
 *
 * structure for ancillary semaphore
 */
struct _GstAncillarySem
{
  GCond condition;
  GMutex mutex;
  guint counter;
};

/**
 * GstTextOverlay:
 *
 * Opaque textoverlay object structure
 */
struct _GstSTTextOverlay
{
  GstElement element;

  GstPad *video_sinkpad;
  GstPad *text_sinkpad;
  GstPad *srcpad;

  GstSegment segment;
  GstSegment text_segment;

  GstBuffer *text_buffer;
  gboolean text_linked;
  gboolean text_flushing;
  gboolean video_flushing;
  gdouble rate;                 /* rate,+ve represents forward trickmode,-ve represents reverse trickmode */
  gboolean text_eos;
  gboolean video_eos;
  gboolean first_video_frame_received;
  GCond cond;                   /* to signal removal of a queued text
                                 * buffer, arrival of a text buffer,
                                 * a text segment update, or a change
                                 * in status (e.g. shutdown, flushing) */

  gint width;
  gint height;
  gint number_of_text_lines;
  gint fps_n;
  gint fps_d;
  GstVideoFormat format;

  GstSTTextOverlayVAlign valign;
  GstSTTextOverlayHAlign halign;
  GstSTTextOverlayWrapMode wrap_mode;
  GstSTTextOverlayLineAlign line_align;

  gint xpad;
  gint ypad;
  gint deltax;
  gint deltay;
  gdouble xpos;
  gdouble ypos;
  gchar *default_text;

  gboolean want_shading;
  gboolean enable_ancillary;
  gboolean wait_text;
  guint color;

  PangoLayout *layout;
  gdouble shadow_offset;
  gdouble outline_offset;
  guchar *text_image;
  gint image_width;
  gint image_height;
  gint baseline_y;

  gboolean auto_adjust_size;
  gboolean need_render;
  gboolean new_text;
  GstClockTime current_text_render_time;
  gboolean init_pvbt;           /* initialize previous_video_buffer_time */
  GstClockTime previous_video_buffer_time;
  gint video_fd;

  gint shading_value;           /* for timeoverlay subclass */

  gboolean have_pango_markup;
  gboolean use_vertical_render;
  GstClockTimeDiff diff_time;

  /* Synchronization task */
  GstTask *render_task;
  GRecMutex task_mutex;

  gboolean blanking_done;
  GstClockTime blanking_frame_render_time;

  /* PES to ES Filter */
  GstPESFilter pesfilter;

  GstSTTextOverlayType type;


  /* DVB Subtitle Handling */
  GstSegment subtitle_segment;
  DVBSubtitles *current_subtitle;
  GQueue *pending_subtitles;    /* A queue of raw subtitle region sets with
                                 * metadata that are waiting their running time */
  DvbSub *dvb_sub;

  /* Dvb Subtitle decoding Task */
  GstTask *dvb_subtitle_task;

  GstBuffer *es_buffer;

  /* Teletext */
  gboolean page_change_req;
  GstTask *dvb_teletext_task;
  GstTeletextDec teletext_dec;
  GstPadTemplate *src_template;
  /*closed caption task */
  GstTask *dvb_cc_task;

  GstUserData user_data;
  gint videofd;                 /* Used for data extraction */
  GstCCData cc_data_block[MAX_CC_BLOCKS];       /* block for closed caption decoder */
  guint32 cc_data_index;
  GstCCUserData cc_user_data_block;     /* section of user data to detect closed caption */
  gboolean is_CC;
  GstTask *cc_task;
  GRecMutex cc_task_mutex;
  GstAncillarySem cc_task_sem;
  GstAncillarySem cc_sync_sem;
  gboolean cc_term;
  gboolean cc_start;
  gboolean cc_stop;
  guint32 cc_index;
  GstSTTextCCState cc_state;
  gint cc_user_cmd;
  gboolean cc_ud_ready;
  guchar user_data_ptr[2048];

  GstTeletextInfo *current_teletext;    /* current teletext page to be rendered */
  GQueue *pending_teletext;     /* a queue of teletext pages waiting for their time
                                 * to be rendered */
  GQueue *pending_closed_caption;       /* a queue of closed caption pages waiting for their time
                                         * to be rendered */

  GstCaps *prev_vid_caps;
  GstCaps *prev_text_caps;
  gboolean subtitle_flush_finish;       /* playsink custom flush finish marker */
};

struct _GstSTTextOverlayClass
{
  GstElementClass parent_class;

  PangoContext *pango_context;
  gchar *(*get_text) (GstSTTextOverlay * overlay);
};

GType gst_sttext_overlay_get_type (void);
gboolean sttextoverlay_init (GstPlugin * plugin);

/* PES To ES */
void gst_stm_pes_parser (GstSTTextOverlay * overlay, GstBuffer * buffer);
void gst_stm_pes_parser_init (GstSTTextOverlay * overlay);
void gst_stm_pes_parser_deinit (GstSTTextOverlay * overlay);


/* DVB Subtitles */
void gst_stm_dvb_subtitle_init (GstSTTextOverlay * overlay);
void gst_stm_dvb_subtitle_deinit (GstSTTextOverlay * overlay);
void gst_dvbsub_overlay_flush_subtitles (GstSTTextOverlay * overlay);
void gst_stm_dvb_get_subtitle_render (GstSTTextOverlay * overlay);
void gst_stm_dvb_blit_subtitle (GstSTTextOverlay * overlay, DVBSubtitles * subs,
    GstBuffer * buffer);
void gst_stm_dvb_subtitle_free (GstSTTextOverlay * overlay);
void gst_dvbsub_overlay_process_text (GstSTTextOverlay * overlay,
    GstBuffer * buffer, guint64 pts);


/* DVB Teletext */
void gst_stm_dvb_teletext_init (GstSTTextOverlay * overlay);
void gst_stm_dvb_teletext_deinit (GstSTTextOverlay * overlay);
GstFlowReturn gst_stm_dvb_get_teletext (GstSTTextOverlay * overlay,
    GstClockTime pts);
void gst_stm_dvb_blit_teletext (GstSTTextOverlay * overlay, GstBuffer * buf);
GstFlowReturn gst_sttext_push_text_frame (GstSTTextOverlay * overlay,
    gboolean Blit);
void gst_teletextdec_process_telx_buffer (GstTeletextDec * teletext,
    GstBuffer * buf);
void gst_teletextdec_cc_draw_page (GstTeletextDec * teletext);
void gst_stm_dvb_get_teletext_render (GstSTTextOverlay * overlay);
void gst_stm_dvb_get_cc_render (GstSTTextOverlay * overlay);
void gst_stm_dvb_teletext_free (GstSTTextOverlay * overlay);
void gst_dvb_overlay_flush_teletext (GstSTTextOverlay * overlay);
void gst_dvb_overlay_flush_closed_caption (GstSTTextOverlay * overlay);

/* Text Subtitle */
void gst_sttext_overlay_blit_frame (GstSTTextOverlay * overlay,
    GstBuffer * buffer);
GstFlowReturn gst_sttext_decode_text (GstSTTextOverlay * overlay);
void gst_sttext_overlay_update_wrap_mode (GstSTTextOverlay * overlay);
void gst_sttext_overlay_adjust_values_with_fontdesc (GstSTTextOverlay * overlay,
    PangoFontDescription * desc);
void gst_sttext_overlay_update_render_mode (GstSTTextOverlay * overlay);
void gst_sttext_overlay_render_text (GstSTTextOverlay * overlay,
    const gchar * text, gint textlen);
void gst_stm_overlay_init (GstSTTextOverlay * overlay);
void gst_stm_overlay_deinit (GstSTTextOverlay * overlay);

/* Closed captions */
gint gst_sttext_stream_on (GstSTTextOverlay * overlay);
gint gst_sttext_stream_off (GstSTTextOverlay * overlay);
void gst_sttext_queue_buffer (GstSTTextOverlay * overlay, void *ptr);
gint8 gst_sttext_dequeue_buffer (GstSTTextOverlay * overlay,
    GstUserData * user_data);
gint gst_sttext_init_v4l_extract_ud (GstSTTextOverlay * overlay);
void gst_cc_change_state (GstSTTextOverlay * overlay,
    GstSTTextCCState requested_cc_state);
void gst_sttext_cc_draw_page (GstSTTextOverlay * overlay);
void gst_sttext_term_v4l_extract_ud (GstSTTextOverlay * overlay);
void gst_stm_cc_draw_page (GstSTTextOverlay * overlay, GstClockTime pts);


/* semaphore for ancillary */
void gst_sttext_ancillarysem_down (GstAncillarySem * sem);
void gst_sttext_ancillarysem_up (GstAncillarySem * sem);
void gst_teletext_caption_render_page (GstSTTextOverlay * overlay,
    GstBuffer * buf, guint32 read_offset);

void gst_sttext_overlay_flush (GstSTTextOverlay * overlay);

G_END_DECLS
#endif /* __GSTSTTEXTOVERLAY_H__ */
