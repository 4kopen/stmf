/* GStreamer
 * Copyright (C) <1999> Erik Walthinsen <omega@cse.ogi.edu>
 * Copyright (C) <2003> David Schleef <ds@schleef.org>
 * Copyright (C) <2006> Julien Moutte <julien@moutte.net>
 * Copyright (C) <2006> Zeeshan Ali <zeeshan.ali@nokia.com>
 * Copyright (C) <2006-2008> Tim-Philipp Müller <tim centricular net>
 * Copyright (C) <2009> Young-Ho Cha <ganadist@gmail.com>
 * Copyright (C) <2009> Sebastian Pölsterl <sebp@k-d-w.org>
 * Copyright (C) <2010> Andoni Morales Alastruey <ylatuya@gmail.com>
 * Copyright (C) <2012> STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * This file has been modified by STMicroelectronics, on 03/13/2012
 */

/**
 * SECTION:element-sttextoverlay
 *
 * Description: sttextoverlay element renders text on top of a video stream to replace
 * gstreamer native textoverlay element.
 * Only Upstream video elemented stvideo (whose source caps are video/x-fake-yuv) will be linked with sttextoverlay
 * Upstream element whose source caps is video/x-raw-yuv will still be linked to gstreamer textoverlay element
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch filesrc location=subtitle.avi ! stavi_demux name=demux demux. ! queue ! stvideo ! sttextoverlay text="sttextoverlay" ! ffmpegcolorspace ! stdisplaysink demux. ! queue ! staudio ! fakesink
 * gst-launch filesrc location=subtitle.mkv ! matroskademux name=demux demux. ! queue ! stvideo ! queue ! txt. filesrc location=subtitle.srt ! subparse ! queue ! sttextoverlay name=txt ! queue ! ffmpegcolorspace ! stdisplaysink demux. ! queue ! staudio ! queue ! fakesink
 * ]|
 * </refsect2>
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gst/video/video.h>
#include "gststtextoverlay.h"
#include "gststm-demux-utils.h"
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/dvb/video.h>
#include <linux/dvb/stm_ioctls.h>
#include "pes.h"
#include <linux/videodev2.h>

GST_DEBUG_CATEGORY (sttextoverlay_debug);
#define GST_CAT_DEFAULT sttextoverlay_debug

#define DEFAULT_FONT_DESCRIPTION "verdana 12"

/*
 * Max plane limits
 */
#define MAX_PLANE_WIDTH               1920
#define MAX_PLANE_HEIGHT              1080

#define STTEXT_OVERLAY_RENDER_THRESHOLD     (gst_util_uint64_scale_int(200, GST_SECOND, 1000))
#define STTEXT_OVERLAY_DVB_DROP_THRESHOLD       (gst_util_uint64_scale_int(30000, GST_SECOND, 1000))
#define STTEXT_OVERLAY_TELETEXT_DROP_THRESHOLD       (gst_util_uint64_scale_int(30000, GST_SECOND, 1000))
#define STTEXT_OVERLAY_FILE_DROP_THRESHOLD       (gst_util_uint64_scale_int(2000, GST_SECOND, 1000))

enum
{
  PROP_0,
  PROP_TEXT,
  PROP_SHADING,
  PROP_VALIGN,                  /* deprecated */
  PROP_HALIGN,                  /* deprecated */
  PROP_HALIGNMENT,
  PROP_VALIGNMENT,
  PROP_XPAD,
  PROP_YPAD,
  PROP_DELTAX,
  PROP_DELTAY,
  PROP_XPOS,
  PROP_YPOS,
  PROP_WRAP_MODE,
  PROP_FONT_DESC,
  PROP_SILENT,
  PROP_LINE_ALIGNMENT,
  PROP_WAIT_TEXT,
  PROP_AUTO_ADJUST_SIZE,
  PROP_VERTICAL_RENDER,
  PROP_COLOR,
  PROP_TELETEXT_PAGENO,
  PROP_TELETEXT_SUBPAGENO,
  PROP_TELETEXT_SUBTITLES_MODE,
  PROP_TELETEXT_SUBTITLES_TEMPLATE,
  PROP_TELETEXT_FONT_DESCRIPTION,
  /* closed caption properties */
  PROP_CC_USER_CMD,
  PROP_LAST
};

#define DEFAULT_ENABLE (TRUE)
#define DEFAULT_MAX_PAGE_TIMEOUT (0)

static GstStaticPadTemplate src_template_factory =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("RGBA"))
    );

static GstStaticPadTemplate video_sink_template_factory =
GST_STATIC_PAD_TEMPLATE ("video_sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-fake-yuv")
    );

static GstStaticPadTemplate text_sink_template_factory =
    GST_STATIC_PAD_TEMPLATE ("text_sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("text/x-raw, format = { pango-markup, utf8 };"
        "text/plain;" "subpicture/x-dvb;" "private/teletext;"
        "private/closecaption;" "ancillary/none;")
    );

#define GST_TYPE_STTEXT_OVERLAY_VALIGN (gst_sttext_overlay_valign_get_type())
static GType
gst_sttext_overlay_valign_get_type (void)
{
  static GType sttext_overlay_valign_type = 0;

  static const GEnumValue sttext_overlay_valign[] = {
    {GST_STTEXT_OVERLAY_VALIGN_BASELINE, "baseline", "baseline"},
    {GST_STTEXT_OVERLAY_VALIGN_BOTTOM, "bottom", "bottom"},
    {GST_STTEXT_OVERLAY_VALIGN_TOP, "top", "top"},
    {GST_STTEXT_OVERLAY_VALIGN_POS, "position", "position"},
    {0, NULL, NULL},
  };

  if (!sttext_overlay_valign_type) {
    sttext_overlay_valign_type =
        g_enum_register_static ("GstSTTextOverlayVAlign",
        sttext_overlay_valign);
  }
  return sttext_overlay_valign_type;
}

#define GST_TYPE_STTEXT_OVERLAY_HALIGN (gst_sttext_overlay_halign_get_type())
static GType
gst_sttext_overlay_halign_get_type (void)
{
  static GType sttext_overlay_halign_type = 0;

  static const GEnumValue sttext_overlay_halign[] = {
    {GST_STTEXT_OVERLAY_HALIGN_LEFT, "left", "left"},
    {GST_STTEXT_OVERLAY_HALIGN_CENTER, "center", "center"},
    {GST_STTEXT_OVERLAY_HALIGN_RIGHT, "right", "right"},
    {GST_STTEXT_OVERLAY_HALIGN_POS, "position", "position"},
    {0, NULL, NULL},
  };

  if (!sttext_overlay_halign_type) {
    sttext_overlay_halign_type =
        g_enum_register_static ("GstSTTextOverlayHAlign",
        sttext_overlay_halign);
  }
  return sttext_overlay_halign_type;
}

#define GST_TYPE_TEXT_OVERLAY_WRAP_MODE (gst_sttext_overlay_wrap_mode_get_type())
static GType
gst_sttext_overlay_wrap_mode_get_type (void)
{
  static GType sttext_overlay_wrap_mode_type = 0;
  static const GEnumValue sttext_overlay_wrap_mode[] = {
    {GST_STTEXT_OVERLAY_WRAP_MODE_NONE, "none", "none"},
    {GST_STTEXT_OVERLAY_WRAP_MODE_WORD, "word", "word"},
    {GST_STTEXT_OVERLAY_WRAP_MODE_CHAR, "char", "char"},
    {GST_STTEXT_OVERLAY_WRAP_MODE_WORD_CHAR, "wordchar", "wordchar"},
    {0, NULL, NULL},
  };

  if (!sttext_overlay_wrap_mode_type) {
    sttext_overlay_wrap_mode_type =
        g_enum_register_static ("GstSTTextOverlayWrapMode",
        sttext_overlay_wrap_mode);
  }
  return sttext_overlay_wrap_mode_type;
}

#define GST_TYPE_STTEXT_OVERLAY_LINE_ALIGN (gst_sttext_overlay_line_align_get_type())
static GType
gst_sttext_overlay_line_align_get_type (void)
{
  static GType sttext_overlay_line_align_type = 0;
  static const GEnumValue sttext_overlay_line_align[] = {
    {GST_STTEXT_OVERLAY_LINE_ALIGN_LEFT, "left", "left"},
    {GST_STTEXT_OVERLAY_LINE_ALIGN_CENTER, "center", "center"},
    {GST_STTEXT_OVERLAY_LINE_ALIGN_RIGHT, "right", "right"},
    {0, NULL, NULL}
  };

  if (!sttext_overlay_line_align_type) {
    sttext_overlay_line_align_type =
        g_enum_register_static ("GstSTTextOverlayLineAlign",
        sttext_overlay_line_align);
  }
  return sttext_overlay_line_align_type;
}

static GstStateChangeReturn gst_sttext_overlay_change_state (GstElement *
    element, GstStateChange transition);

static gboolean gst_sttext_overlay_setcaps (GstSTTextOverlay * overlay,
    GstCaps * caps);
static gboolean gst_sttext_overlay_video_event (GstPad * pad,
    GstObject * parent, GstEvent * event);
static GstFlowReturn gst_sttext_overlay_video_chain (GstPad * pad,
    GstObject * parent, GstBuffer * buffer);

static gboolean gst_sttext_overlay_setcaps_txt (GstSTTextOverlay * overlay,
    GstCaps * caps);
static gboolean gst_sttext_overlay_text_event (GstPad * pad, GstObject * parent,
    GstEvent * event);
static GstPadLinkReturn gst_sttext_overlay_text_pad_link (GstPad * pad,
    GstObject * parent, GstPad * peer);
static void gst_sttext_overlay_text_pad_unlink (GstPad * pad,
    GstObject * parent);
static GstFlowReturn gst_sttext_overlay_text_chain (GstPad * pad,
    GstObject * parent, GstBuffer * buffer);

static gboolean gst_sttext_overlay_src_event (GstPad * pad, GstObject * parent,
    GstEvent * event);
static gboolean gst_sttext_overlay_src_query (GstPad * pad, GstObject * parent,
    GstQuery * query);

static void gst_sttext_overlay_finalize (GObject * object);
static void gst_sttext_overlay_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_sttext_overlay_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static gchar *gst_sttext_overlay_get_text (GstSTTextOverlay * overlay);

#define gst_sttext_overlay_parent_class parent_class
G_DEFINE_TYPE (GstSTTextOverlay, gst_sttext_overlay, GST_TYPE_ELEMENT);

static void
gst_sttext_overlay_class_init (GstSTTextOverlayClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;
  PangoFontMap *fontmap;
  gchar *str_escape = g_strescape ("%s\n", NULL);

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&src_template_factory));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&video_sink_template_factory));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&text_sink_template_factory));

  gst_element_class_set_static_metadata (gstelement_class,
      "ST Text overlay",
      "Filter/Video/Overlay/Subtitle",
      "Convert text to ARGB32 format to display on ST GDP layer", "www.st.com");

  gobject_class->finalize = gst_sttext_overlay_finalize;
  gobject_class->set_property = gst_sttext_overlay_set_property;
  gobject_class->get_property = gst_sttext_overlay_get_property;

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_sttext_overlay_change_state);

  klass->get_text = gst_sttext_overlay_get_text;

  fontmap = pango_cairo_font_map_get_default ();
  klass->pango_context =
      pango_font_map_create_context (PANGO_FONT_MAP (fontmap));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_TEXT,
      g_param_spec_string ("text", "text",
          "Text to be display.", DEFAULT_PROP_TEXT,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_SHADING,
      g_param_spec_boolean ("shaded-background", "shaded background",
          "Whether to shade the background under the text area",
          DEFAULT_PROP_SHADING, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_VALIGNMENT,
      g_param_spec_enum ("valignment", "vertical alignment",
          "Vertical alignment of the text", GST_TYPE_STTEXT_OVERLAY_VALIGN,
          DEFAULT_PROP_VALIGNMENT, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_HALIGNMENT,
      g_param_spec_enum ("halignment", "horizontal alignment",
          "Horizontal alignment of the text", GST_TYPE_STTEXT_OVERLAY_HALIGN,
          DEFAULT_PROP_HALIGNMENT, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_VALIGN,
      g_param_spec_string ("valign", "vertical alignment",
          "Vertical alignment of the ext (deprecated; use valignment)",
          DEFAULT_PROP_VALIGN, G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_HALIGN,
      g_param_spec_string ("halign", "horizontal alignment",
          "Horizontal alignment of the text (deprecated; use halignment)",
          DEFAULT_PROP_HALIGN, G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_XPAD,
      g_param_spec_int ("xpad", "horizontal paddding",
          "Horizontal paddding when using left/right alignment", 0, G_MAXINT,
          DEFAULT_PROP_XPAD, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_YPAD,
      g_param_spec_int ("ypad", "vertical padding",
          "Vertical padding when using top/bottom alignment", 0, G_MAXINT,
          DEFAULT_PROP_YPAD, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_DELTAX,
      g_param_spec_int ("deltax", "X position modifier",
          "Shift X position to the left or to the right. Unit is pixels.",
          G_MININT, G_MAXINT, DEFAULT_PROP_DELTAX,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_DELTAY,
      g_param_spec_int ("deltay", "Y position modifier",
          "Shift Y position up or down. Unit is pixels.", G_MININT, G_MAXINT,
          DEFAULT_PROP_DELTAY, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  /**
   * GstTextOverlay:xpos
   *
   * Horizontal position of the rendered text when using positioned alignment.
   *
   * Since: 0.10.31
   **/
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_XPOS,
      g_param_spec_double ("xpos", "horizontal position",
          "Horizontal position when using position alignment", 0, 1.0,
          DEFAULT_PROP_XPOS,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  /**
   * GstTextOverlay:ypos
   *
   * Vertical position of the rendered text when using positioned alignment.
   *
   * Since: 0.10.31
   **/
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_YPOS,
      g_param_spec_double ("ypos", "vertical position",
          "Vertical position when using position alignment", 0, 1.0,
          DEFAULT_PROP_YPOS,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_WRAP_MODE,
      g_param_spec_enum ("wrap-mode", "wrap mode",
          "Whether to wrap the text and if so how.",
          GST_TYPE_TEXT_OVERLAY_WRAP_MODE, DEFAULT_PROP_WRAP_MODE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_FONT_DESC,
      g_param_spec_string ("font-desc", "font description",
          "Pango font description of font to be used for rendering. "
          "See documentation of pango_font_description_from_string "
          "for syntax.", DEFAULT_PROP_FONT_DESC,
          G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS));
  /**
   * GstTextOverlay:color
   *
   * Color of the rendered text.
   *
   * Since: 0.10.31
   **/
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_COLOR,
      g_param_spec_uint ("color", "Color",
          "Color to use for text (big-endian ARGB).", 0, G_MAXUINT32,
          DEFAULT_PROP_COLOR,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));

  /**
   * GstTextOverlay:line-alignment
   *
   * Alignment of text lines relative to each other (for multi-line text)
   *
   * Since: 0.10.15
   **/
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_LINE_ALIGNMENT,
      g_param_spec_enum ("line-alignment", "line alignment",
          "Alignment of text lines relative to each other.",
          GST_TYPE_STTEXT_OVERLAY_LINE_ALIGN, DEFAULT_PROP_LINE_ALIGNMENT,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  /**
   * GstTextOverlay:silent
   *
   * If set, no text is rendered. Useful to switch off text rendering
   * temporarily without removing the textoverlay element from the pipeline.
   *
   * Since: 0.10.15
   **/
  /* FIXME 0.11: rename to "visible" or "text-visible" or "render-text" */
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_SILENT,
      g_param_spec_boolean ("silent", "silent",
          "Whether to render the text string",
          DEFAULT_PROP_SILENT,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));

  /**
   * GstTextOverlay:wait-text
   *
   * If set, the video will block until a subtitle is received on the text pad.
   * If video and subtitles are sent in sync, like from the same demuxer, this
   * property should be set.
   *
   * Since: 0.10.20
   **/
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_WAIT_TEXT,
      g_param_spec_boolean ("wait-text", "Wait Text",
          "Whether to wait for subtitles",
          DEFAULT_PROP_WAIT_TEXT, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_AUTO_ADJUST_SIZE, g_param_spec_boolean ("auto-resize", "auto resize",
          "Automatically adjust font size to screen-size.",
          DEFAULT_PROP_AUTO_ADJUST_SIZE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_VERTICAL_RENDER,
      g_param_spec_boolean ("vertical-render", "vertical render",
          "Vertical Render.", DEFAULT_PROP_VERTICAL_RENDER,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_TELETEXT_PAGENO,
      g_param_spec_int ("teletext-page", "Teletext Page number",
          "Teletext page number that should displayed",
          100, 999, 100, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_TELETEXT_SUBPAGENO,
      g_param_spec_int ("teletext-subpage", "Teletext Sub-page number",
          "Teletext sub-page number that should displayed (-1 for all)",
          -1, 0x99, -1, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_TELETEXT_SUBTITLES_MODE,
      g_param_spec_boolean ("subtitles-mode", "Enable subtitles mode",
          "Enables subtitles mode for text output stripping the blank lines and "
          "the teletext state lines", FALSE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class,
      PROP_TELETEXT_SUBTITLES_TEMPLATE,
      g_param_spec_string ("subtitles-template", "Subtitles output template",
          "Output template used to print each one of the subtitles lines",
          str_escape, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class,
      PROP_TELETEXT_FONT_DESCRIPTION, g_param_spec_string ("font-description",
          "Pango font description",
          "Font description used for the pango output.",
          DEFAULT_FONT_DESCRIPTION,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_CC_USER_CMD,
      g_param_spec_int ("cc-user-cmd", "cc_user_cmd  number",
          "Number of closed captions  that should displayed (-1 for all)", -1,
          0x99, -1, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_free (str_escape);
  return;
}

/* Render dvb subtitles */
static void
gst_sttext_push_dvb_subtitle (GstSTTextOverlay * overlay)
{
  if (overlay->enable_ancillary) {
    if (overlay->current_subtitle->num_rects == 0) {
      /* Send Blanking Frame */
      gst_sttext_push_text_frame (overlay, FALSE);
      GST_LOG_OBJECT (overlay, "render blank");
    } else {
      gst_sttext_push_text_frame (overlay, TRUE);
      GST_LOG_OBJECT (overlay, "render text");
    }
  }

  gst_stm_dvb_subtitle_free (overlay);

  return;
}

/* Render dvb teletexts */
static void
gst_sttext_push_dvb_teletext (GstSTTextOverlay * overlay)
{
  if (overlay->enable_ancillary) {
    gst_sttext_push_text_frame (overlay, TRUE);
    GST_LOG_OBJECT (overlay, "render text");
  }

  gst_stm_dvb_teletext_free (overlay);

  return;
}

/* Render dvb closed captions */
static void
gst_sttext_push_dvb_cc (GstSTTextOverlay * overlay)
{
  if (overlay->cc_start == TRUE) {
    gst_sttext_push_text_frame (overlay, TRUE);
    GST_LOG_OBJECT (overlay, "render cc");
  }
  gst_stm_dvb_teletext_free (overlay);
  return;
}

/*
  * Function name : gst_sttext_overlay_send_buf_event
  *  Input            : GstSTTextOverlay * overlay
  *
  *  Description  :
  *                   requests video dummy buffer after
  *                   playsink-custom-subtitle-flush-finish event
  *                   is received by sttextoverlay
  *
  *  Return        : TRUE if Success else FALSE
*/
static gboolean
gst_sttext_overlay_send_buf_event (GstSTTextOverlay * overlay)
{
  GstEvent *event;
  GstStructure *structure;
  GstPad *pad = overlay->video_sinkpad;

  structure = gst_structure_new_empty ("request-buffer");

  if (structure) {
    event = gst_event_new_custom (GST_EVENT_CUSTOM_UPSTREAM, structure);
    if (event) {
      if (gst_pad_push_event (pad, event) != TRUE) {
        GST_WARNING_OBJECT (overlay, "failed to push request-buffer event");
        return FALSE;
      }
    } else {
      GST_WARNING_OBJECT (overlay, "failed to create request-buffer event");
      gst_structure_free (structure);
      return FALSE;
    }
  } else {
    GST_WARNING_OBJECT (overlay, "failed to create request-buffer structure");
    return FALSE;
  }
  return TRUE;
}


/* This is the sync task which is scheduled every 200 ms for getting the Video PTS and to do the sync operation with the text */
/* this task is only for dvb subtitles and file subtitles. it does not provide sync for teletext or closed caption */
void
gst_sttext_av_text_sync (void *data)
{
  GstSTTextOverlay *overlay = (GstSTTextOverlay *) data;
  GstClockTime current_video_time = GST_CLOCK_TIME_NONE;

  usleep (200 * 1000);
  if (overlay->first_video_frame_received) {
    GST_OBJECT_LOCK (overlay);
    /* Get the PTS of the picture on Display */
    if (overlay->video_fd == -1) {
      current_video_time = GST_CLOCK_TIME_NONE;
    } else {
      video_play_info_t vid_play_info;
      if (ioctl (overlay->video_fd, VIDEO_GET_PLAY_INFO,
              (void *) &vid_play_info) == 0) {
        current_video_time =
            vid_play_info.pts ==
            INVALID_PTS_VALUE ? GST_CLOCK_TIME_NONE :
            gst_util_uint64_scale_int (vid_play_info.pts / 90, GST_SECOND,
            1000);
        if (overlay->init_pvbt == TRUE) {
          overlay->init_pvbt = FALSE;
          overlay->previous_video_buffer_time = current_video_time;
        }
      } else {
        current_video_time = GST_CLOCK_TIME_NONE;
      }
    }

    if (overlay->previous_video_buffer_time != current_video_time) {
      /* DVB Subtitle Handling */
      if (overlay->type == GST_STTEXT_OVERLAY_DVB_SUBTITLE) {
        gst_stm_dvb_get_subtitle_render (overlay);
        if (overlay->current_subtitle != NULL) {        /* subtitle ready to display */
          if (current_video_time == GST_CLOCK_TIME_NONE) {
            /* invalid video PTS, render immediately to un-block the pipeline */
            GST_LOG_OBJECT (overlay, "invalid video pts");
            gst_sttext_push_dvb_subtitle (overlay);
          } else {
            /* get the difference between pts of subtitle and current video on display */
            overlay->diff_time =
                (overlay->current_subtitle->pts >=
                current_video_time) ? (GST_CLOCK_DIFF (current_video_time,
                    overlay->current_subtitle->
                    pts)) : (GST_CLOCK_DIFF (overlay->current_subtitle->pts,
                    current_video_time));

            if (overlay->current_subtitle->pts >= current_video_time)
              GST_LOG_OBJECT (overlay, "subtitle advance %" GST_TIME_FORMAT,
                  GST_TIME_ARGS (overlay->diff_time));
            else
              GST_LOG_OBJECT (overlay, "subtitle later %" GST_TIME_FORMAT,
                  GST_TIME_ARGS (overlay->diff_time));

            if (overlay->current_subtitle->pts <= current_video_time) {
              /* subtitle is later or equal to video, render subtitle immediately */
              gst_sttext_push_dvb_subtitle (overlay);
            } else if (overlay->diff_time > STTEXT_OVERLAY_DVB_DROP_THRESHOLD) {
              /* subtitle is too advance than video, drop it */
              gst_stm_dvb_subtitle_free (overlay);
              GST_LOG_OBJECT (overlay, "drop");
            } else {
              /* subtitle is advance than video but not too big, keep it in the queue */
              GST_LOG_OBJECT (overlay, "keep");
            }
          }
        }
      }

      /* Teletext Handling */
      else if (overlay->type == GST_STTEXT_OVERLAY_TELETEXT) {
        gst_stm_dvb_get_teletext_render (overlay);
        if (overlay->current_teletext != NULL) {        /* teletext ready to display */
          if (current_video_time == GST_CLOCK_TIME_NONE) {
            /* invalid video PTS, render immediately to un-block the pipeline */
            GST_LOG_OBJECT (overlay, "invalid video pts");
            gst_sttext_push_dvb_teletext (overlay);
          } else {
            /* get the difference between pts of teletext and current video on display */
            overlay->diff_time =
                (overlay->current_teletext->pts >=
                current_video_time) ? (GST_CLOCK_DIFF (current_video_time,
                    overlay->current_teletext->
                    pts)) : (GST_CLOCK_DIFF (overlay->current_teletext->pts,
                    current_video_time));

            if (overlay->current_teletext->pts >= current_video_time)
              GST_LOG_OBJECT (overlay, "teletext advance %" GST_TIME_FORMAT,
                  GST_TIME_ARGS (overlay->diff_time));
            else
              GST_LOG_OBJECT (overlay, "teletext later %" GST_TIME_FORMAT,
                  GST_TIME_ARGS (overlay->diff_time));

            if (overlay->current_teletext->pts <= current_video_time) {
              /* teletext is later or equal to video, render subtitle immediately */
              gst_sttext_push_dvb_teletext (overlay);
            } else if (overlay->diff_time >
                STTEXT_OVERLAY_TELETEXT_DROP_THRESHOLD) {
              /* teletext is too advance than video, drop it */
              gst_stm_dvb_teletext_free (overlay);
              GST_LOG_OBJECT (overlay, "drop");
            } else {
              /* teletext is advance than video but not too big, keep it in the queue */
              GST_LOG_OBJECT (overlay, "keep");
            }
          }
        }
      } else if (overlay->type == GST_STTEXT_OVERLAY_CC) {
        gst_stm_dvb_get_cc_render (overlay);
        if (overlay->current_teletext != NULL) {        /* teletext ready to display */
          if (current_video_time == GST_CLOCK_TIME_NONE) {
            /* invalid video PTS, render immediately to un-block the pipeline */
            GST_LOG_OBJECT (overlay, "invalid video pts");
            gst_sttext_push_dvb_cc (overlay);
          } else {
            /* get the difference between pts of cc and current video on display */
            overlay->diff_time =
                (overlay->current_teletext->pts >=
                current_video_time) ? (GST_CLOCK_DIFF (current_video_time,
                    overlay->current_teletext->
                    pts)) : (GST_CLOCK_DIFF (overlay->current_teletext->pts,
                    current_video_time));

            if (overlay->current_teletext->pts >= current_video_time)
              GST_LOG_OBJECT (overlay, "teletext advance %" GST_TIME_FORMAT,
                  GST_TIME_ARGS (overlay->diff_time));
            else
              GST_LOG_OBJECT (overlay, "teletext later %" GST_TIME_FORMAT,
                  GST_TIME_ARGS (overlay->diff_time));

            if (overlay->current_teletext->pts <= current_video_time) {
              /* teletext is later or equal to video, render subtitle immediately */
              gst_sttext_push_dvb_cc (overlay);
            } else if (overlay->diff_time >
                STTEXT_OVERLAY_TELETEXT_DROP_THRESHOLD) {
              /* teletext is too advance than video, drop it */
              gst_stm_dvb_teletext_free (overlay);
              GST_LOG_OBJECT (overlay, "drop");
            } else {
              /* teletext is advance than video but not too big, keep it in the queue */
              GST_LOG_OBJECT (overlay, "keep");
            }
          }
        }
      }
      /* Normal text handling */
      else {
        /* Check whether text buffer available */
        if (overlay->text_buffer != NULL) {
          /* Compare the Video buffer time with the Text buffer time */
          if (overlay->new_text == TRUE) {
            overlay->diff_time =
                overlay->current_text_render_time >=
                current_video_time ? (GST_CLOCK_DIFF (current_video_time,
                    overlay->current_text_render_time))
                : (GST_CLOCK_DIFF (overlay->current_text_render_time,
                    current_video_time));
            if ((current_video_time >= overlay->current_text_render_time)
                && (overlay->diff_time > STTEXT_OVERLAY_FILE_DROP_THRESHOLD)) {
              /* subtitle is too old. drop it */
              if (!overlay->blanking_done) {
                gst_sttext_push_text_frame (overlay, FALSE);
                overlay->blanking_done = TRUE;
              }
              overlay->current_text_render_time =
                  overlay->blanking_frame_render_time;
              /* wakeup the text chain to receive the text buffer */
              GST_STTEXT_OVERLAY_BROADCAST (overlay);
            } else if (((overlay->diff_time <= STTEXT_OVERLAY_RENDER_THRESHOLD)
                    || (current_video_time >=
                        overlay->current_text_render_time))
                && (current_video_time != GST_CLOCK_TIME_NONE)) {
              if (overlay->enable_ancillary) {
                /* subtitle is later or equal to video, render subtitle immediately */
                if (overlay->blanking_done) {
                  gst_sttext_decode_text (overlay);
                  /* Push the text frame for Render */
                  gst_sttext_push_text_frame (overlay, TRUE);
                  overlay->blanking_done = FALSE;
                  overlay->current_text_render_time =
                      overlay->blanking_frame_render_time;
                } else {
                  /* Push the blank frame for Render */
                  gst_sttext_push_text_frame (overlay, FALSE);
                  overlay->blanking_done = TRUE;
                  /* wakeup the text chain to receive the text buffer */
                  GST_STTEXT_OVERLAY_BROADCAST (overlay);
                }
              } else {
                /* wakeup the text chain to receive the text buffer */
                GST_STTEXT_OVERLAY_BROADCAST (overlay);
              }
            }
          }
        }
      }
      overlay->previous_video_buffer_time = current_video_time;
    }
    GST_OBJECT_UNLOCK (overlay);
  } else {
    if (overlay->subtitle_flush_finish) {
      gst_sttext_overlay_send_buf_event (overlay);
    }
  }
}


void
gst_sttext_ancillarysem_down (GstAncillarySem * sem)
{
  g_mutex_lock (&sem->mutex);
  if (sem->counter == 0) {
    g_cond_wait (&sem->condition, &sem->mutex);
  }
  sem->counter--;
  g_mutex_unlock (&sem->mutex);
}

void
gst_sttext_ancillarysem_up (GstAncillarySem * sem)
{
  g_mutex_lock (&sem->mutex);
  sem->counter++;
  g_cond_signal (&sem->condition);
  g_mutex_unlock (&sem->mutex);
}

/* The function returns true only if state transition occurs. */

void
gst_cc_change_state (GstSTTextOverlay * overlay,
    GstSTTextCCState requested_cc_state)
{
  GstSTTextCCState *cc_state = &overlay->cc_state;
  switch (*cc_state) {
    case GST_STTEXT_CC_STATE_IDLE:
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_INIT) ? requested_cc_state : *cc_state;
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_IDLE) ? requested_cc_state : *cc_state;
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_TERM) ? requested_cc_state : *cc_state;
      break;
    case GST_STTEXT_CC_STATE_INIT:
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_GET_DATA) ? requested_cc_state : *cc_state;
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_TERM) ? requested_cc_state : *cc_state;
      break;
    case GST_STTEXT_CC_STATE_GET_DATA:
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_PARSE_DATA) ? requested_cc_state : *cc_state;
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_GET_DATA) ? requested_cc_state : *cc_state;
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_TERM) ? requested_cc_state : *cc_state;
      break;
    case GST_STTEXT_CC_STATE_PARSE_DATA:
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_DECODE_DATA) ? requested_cc_state : *cc_state;
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_TERM) ? requested_cc_state : *cc_state;
      break;
    case GST_STTEXT_CC_STATE_DECODE_DATA:
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_DRAW_PAGE) ? requested_cc_state : *cc_state;
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_TERM) ? requested_cc_state : *cc_state;
      break;
    case GST_STTEXT_CC_STATE_DRAW_PAGE:
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_GET_DATA) ? requested_cc_state : *cc_state;
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_TERM) ? requested_cc_state : *cc_state;
      break;
    case GST_STTEXT_CC_STATE_TERM:
      *cc_state =
          (requested_cc_state ==
          GST_STTEXT_CC_STATE_IDLE) ? requested_cc_state : *cc_state;
      break;
    default:
      break;
  }
  gst_sttext_ancillarysem_up (&overlay->cc_task_sem);
}

/* This function extracts the user data using the V4L2 calls. The data is checked for presence of
 * closed caption data, if present then the two byte closed caption data and the field information
 * is extracted from the user data. This information is updated in the cc_data structure.
 */
void
gst_sttext_cc_get_data (GstSTTextOverlay * overlay)
{
  gint8 res = FALSE;

  /* Provide a buffer pointer to v4l */
  gst_sttext_queue_buffer (overlay, overlay->user_data_ptr);
  usleep (100);
  /* Get the user data */
  res = gst_sttext_dequeue_buffer (overlay, &overlay->user_data);

  if (res == 0) {
    gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_PARSE_DATA);
  } else {
    gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_GET_DATA);
  }
}

/* this function parses closed caption data */
void
gst_sttext_cc_parse_data (GstSTTextOverlay * overlay)
{
  GstCCFormatMode detect_mode = GST_STTEXT_OVERLAY_CC_FORMAT_DETECT;
  GstUserData *user_data = &overlay->user_data;

  /* user_data_length can never be zero at the entry of parsing state */
  if (user_data->user_data_processed == user_data->user_data_length) {
    gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_DECODE_DATA);
    user_data->user_data_processed = 0;
    user_data->user_data_length = 0;
    memset (user_data->user_data_ptr, 0, user_data->user_data_length);
    return;
  }
  gst_closedcaption_extact_cc_ud_block (overlay);
  detect_mode = gst_closedcaption_detect_SCTE128 (&overlay->cc_user_data_block);
  if (detect_mode == GST_STTEXT_OVERLAY_CC_FORMAT_SCTE128) {
    gst_closedcaption_parse_SCTE128 (overlay);
    return;
  }
  detect_mode = gst_closedcaption_detect_SCTE21 (&overlay->cc_user_data_block);
  if (detect_mode == GST_STTEXT_OVERLAY_CC_FORMAT_SCTE21) {
    gst_closedcaption_parse_SCTE21 (overlay);
    gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_GET_DATA);
    return;
  }
}

void
gst_sttext_cc_decode_data (GstSTTextOverlay * overlay)
{
  vbi_sliced sliced;
  guint32 index;
  gint n_lines = 1;

  GstCCData *cc_data_block = overlay->cc_data_block;
  GstTeletextDec *teletext = &overlay->teletext_dec;
  static gfloat last_ts = 0.0;

  sliced.id = VBI_SLICED_CAPTION_525;

  for (index = 0; index < overlay->cc_data_index; index++) {
    sliced.line =
        (cc_data_block[index].vsync_field ==
        GST_STTEXT_CC_VSYNC_TOP) ? CC_LINE_TOP : CC_LINE_BOTTOM;
    sliced.data[GST_STTEXT_CC_BYTE0_INDEX] =
        cc_data_block[index].cc_bytes[GST_STTEXT_CC_BYTE0_INDEX];
    sliced.data[GST_STTEXT_CC_BYTE1_INDEX] =
        cc_data_block[index].cc_bytes[GST_STTEXT_CC_BYTE1_INDEX];

    vbi_decode (teletext->decoder, &sliced, n_lines, last_ts);
    last_ts += 1 / 29.97;
  }
  /*cc data has been consumed so reset counter state */
  overlay->cc_data_index = 0;

  gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_DRAW_PAGE);
}

/* this function draws a closed caption page */
void
gst_sttext_cc_draw_page (GstSTTextOverlay * overlay)
{
  gst_stm_cc_draw_page (overlay,
      (GstClockTime) overlay->cc_user_data_block.pts);
  gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_GET_DATA);
}



void
gst_sttext_cc_idle (GstSTTextOverlay * overlay)
{
  gst_sttext_push_text_frame (overlay, FALSE);
  if (overlay->cc_term == TRUE) {
    gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_IDLE);
  }
}

void
gst_sttext_cc_init (GstSTTextOverlay * overlay)
{
  if (gst_sttext_init_v4l_extract_ud (overlay) == GST_STTEXT_NO_ERROR) {
    gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_GET_DATA);
  } else {
    gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_TERM);
  }
  gst_sttext_ancillarysem_up (&overlay->cc_sync_sem);
}

void
gst_sttext_cc_term (GstSTTextOverlay * overlay)
{
  gst_sttext_term_v4l_extract_ud (overlay);
  gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_IDLE);
  gst_sttext_ancillarysem_up (&overlay->cc_sync_sem);
}

void
gst_sttext_cc_send_command (GstSTTextOverlay * overlay, GstSTTextCCCmd cmd)
{
  GstTeletextDec *teletext = &overlay->teletext_dec;

  teletext->pageno = 0x0;
  switch (cmd) {
    case GST_STTEXT_CC_CMD_START:
      if (overlay->cc_start == FALSE && overlay->cc_stop == TRUE) {
        gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_INIT);
        overlay->cc_start = TRUE;
        overlay->cc_stop = FALSE;
        GST_OBJECT_UNLOCK (overlay);
        gst_sttext_ancillarysem_down (&overlay->cc_sync_sem);
        GST_OBJECT_LOCK (overlay);
      }
      break;
    case GST_STTEXT_CC_CMD_STOP:
      if (overlay->cc_stop == FALSE && overlay->cc_start == TRUE) {
        gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_TERM);
        overlay->cc_stop = TRUE;
        overlay->cc_start = FALSE;
        GST_OBJECT_UNLOCK (overlay);
        gst_sttext_ancillarysem_down (&overlay->cc_sync_sem);
        GST_OBJECT_LOCK (overlay);
      }
      break;
    case GST_STTEXT_CC_CMD_TERM:
      if (overlay->cc_term == TRUE) {
        if (overlay->cc_stop == TRUE) {
          gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_IDLE);
        } else {
          gst_cc_change_state (overlay, GST_STTEXT_CC_STATE_TERM);
          gst_sttext_ancillarysem_down (&overlay->cc_sync_sem);
        }
        overlay->cc_stop = TRUE;
        overlay->cc_start = FALSE;
      }
      break;
    default:
      GST_ERROR_OBJECT (overlay, "illegal closed caption command\n");
      break;
  }
}


/*This task is responsible for extraction, decode and rendering of closed captions */

void
gst_sttext_cc_task (void *data)
{
  GstSTTextOverlay *overlay = (GstSTTextOverlay *) data;

  gst_sttext_ancillarysem_down (&overlay->cc_task_sem);

  GST_OBJECT_LOCK (overlay);

  switch (overlay->cc_state) {
    case GST_STTEXT_CC_STATE_IDLE:
      gst_sttext_cc_idle (overlay);
      break;
    case GST_STTEXT_CC_STATE_INIT:
      gst_sttext_cc_init (overlay);
      break;
    case GST_STTEXT_CC_STATE_GET_DATA:
      gst_sttext_cc_get_data (overlay);
      break;
    case GST_STTEXT_CC_STATE_PARSE_DATA:
      gst_sttext_cc_parse_data (overlay);
      break;
    case GST_STTEXT_CC_STATE_DECODE_DATA:
      gst_sttext_cc_decode_data (overlay);
      break;
    case GST_STTEXT_CC_STATE_DRAW_PAGE:
      gst_sttext_cc_draw_page (overlay);
      break;
    case GST_STTEXT_CC_STATE_TERM:
      gst_sttext_cc_term (overlay);
      break;
    default:
      break;
  }
  GST_OBJECT_UNLOCK (overlay);
  if (overlay->cc_state == GST_STTEXT_CC_STATE_GET_DATA)
    usleep (10 * 1000);
}

static void
gst_sttext_overlay_reset (GstSTTextOverlay * overlay)
{
  /* variable */
  /* This will be required by all */

  overlay->video_flushing = FALSE;
  overlay->video_eos = FALSE;
  overlay->video_fd = -1;

  overlay->text_linked = FALSE;
  overlay->have_pango_markup = FALSE;

  overlay->type = GST_STTEXT_OVERLAY_UNKNOWN;
  overlay->rate = 0;

  /* Video Segment */
  gst_segment_init (&overlay->segment, GST_FORMAT_TIME);

  overlay->init_pvbt = FALSE;   /* previous_video_buffer_time */
  overlay->previous_video_buffer_time = GST_CLOCK_TIME_NONE;
  overlay->first_video_frame_received = FALSE;
  overlay->cc_ud_ready = FALSE;
  overlay->fps_n = 25;
  overlay->fps_d = 1;
  overlay->prev_text_caps = NULL;
  overlay->prev_vid_caps = NULL;
  overlay->subtitle_flush_finish = FALSE;
}

static void
gst_sttext_overlay_init (GstSTTextOverlay * overlay)
{
  GstPadTemplate *template;
  /* video sink */
  overlay->src_template = template =
      gst_static_pad_template_get (&video_sink_template_factory);
  overlay->video_sinkpad = gst_pad_new_from_template (template, "video_sink");
  gst_object_unref (template);
  gst_pad_set_event_function (overlay->video_sinkpad,
      GST_DEBUG_FUNCPTR (gst_sttext_overlay_video_event));
  gst_pad_set_chain_function (overlay->video_sinkpad,
      GST_DEBUG_FUNCPTR (gst_sttext_overlay_video_chain));
  gst_element_add_pad (GST_ELEMENT (overlay), overlay->video_sinkpad);

  /* text sink */
  template = gst_static_pad_template_get (&text_sink_template_factory);
  overlay->text_sinkpad = gst_pad_new_from_template (template, "text_sink");
  gst_object_unref (template);
  gst_pad_set_event_function (overlay->text_sinkpad,
      GST_DEBUG_FUNCPTR (gst_sttext_overlay_text_event));
  gst_pad_set_chain_function (overlay->text_sinkpad,
      GST_DEBUG_FUNCPTR (gst_sttext_overlay_text_chain));
  gst_pad_set_link_function (overlay->text_sinkpad,
      GST_DEBUG_FUNCPTR (gst_sttext_overlay_text_pad_link));
  gst_pad_set_unlink_function (overlay->text_sinkpad,
      GST_DEBUG_FUNCPTR (gst_sttext_overlay_text_pad_unlink));
  gst_element_add_pad (GST_ELEMENT (overlay), overlay->text_sinkpad);

  /* source */
  template = gst_static_pad_template_get (&src_template_factory);
  overlay->srcpad = gst_pad_new_from_template (template, "src");
  gst_object_unref (template);
  gst_pad_set_event_function (overlay->srcpad,
      GST_DEBUG_FUNCPTR (gst_sttext_overlay_src_event));
  gst_pad_set_query_function (overlay->srcpad,
      GST_DEBUG_FUNCPTR (gst_sttext_overlay_src_query));
  gst_pad_set_active (overlay->srcpad, TRUE);
  gst_element_add_pad (GST_ELEMENT (overlay), overlay->srcpad);

  /* variable */
  /* This will be required by all */
  overlay->enable_ancillary = DEFAULT_PROP_SILENT;
  overlay->default_text = g_strdup (DEFAULT_PROP_TEXT);
  overlay->color = DEFAULT_PROP_COLOR;
  overlay->halign = DEFAULT_PROP_HALIGNMENT;
  overlay->valign = DEFAULT_PROP_VALIGNMENT;
  overlay->xpad = DEFAULT_PROP_XPAD;
  overlay->ypad = DEFAULT_PROP_YPAD;
  overlay->deltax = DEFAULT_PROP_DELTAX;
  overlay->deltay = DEFAULT_PROP_DELTAY;
  overlay->xpos = DEFAULT_PROP_XPOS;
  overlay->ypos = DEFAULT_PROP_YPOS;
  overlay->line_align = DEFAULT_PROP_LINE_ALIGNMENT;
  overlay->wrap_mode = DEFAULT_PROP_WRAP_MODE;
  overlay->want_shading = DEFAULT_PROP_SHADING;
  overlay->shading_value = DEFAULT_SHADING_VALUE;
  overlay->wait_text = DEFAULT_PROP_WAIT_TEXT;
  overlay->auto_adjust_size = DEFAULT_PROP_AUTO_ADJUST_SIZE;
  overlay->use_vertical_render = DEFAULT_PROP_VERTICAL_RENDER;

  overlay->teletext_dec.subtitles_template = NULL;
  overlay->teletext_dec.font_description = NULL;

  gst_sttext_overlay_reset (overlay);

  /* Initialize Cond/Mutex */
  g_cond_init (&overlay->cond);
  g_mutex_init (&overlay->cc_sync_sem.mutex);
  g_cond_init (&overlay->cc_sync_sem.condition);
  g_mutex_init (&overlay->cc_task_sem.mutex);
  g_cond_init (&overlay->cc_task_sem.condition);
  g_mutex_init (&overlay->teletext_dec.queue_lock);

  return;
}

static gchar *
gst_sttext_overlay_get_text (GstSTTextOverlay * overlay)
{
  return g_strdup (overlay->default_text);
}

static void
gst_sttext_overlay_finalize (GObject * object)
{
  GstSTTextOverlay *overlay = GST_STTEXT_OVERLAY (object);

  GST_DEBUG_OBJECT (overlay, "ancillary overlay finalize");

  if (overlay->default_text != NULL) {
    g_free (overlay->default_text);
    overlay->default_text = NULL;
  }
  if (overlay->teletext_dec.font_description != NULL) {
    g_free (overlay->teletext_dec.font_description);
    overlay->teletext_dec.font_description = NULL;
  }
  if (overlay->teletext_dec.subtitles_template != NULL) {
    g_free (overlay->teletext_dec.subtitles_template);
    overlay->teletext_dec.subtitles_template = NULL;
  }

  /* Initialize Cond/Mutex */
  g_cond_clear (&overlay->cond);
  g_mutex_clear (&overlay->cc_sync_sem.mutex);
  g_cond_clear (&overlay->cc_sync_sem.condition);
  g_mutex_clear (&overlay->cc_task_sem.mutex);
  g_cond_clear (&overlay->cc_task_sem.condition);
  g_mutex_clear (&overlay->teletext_dec.queue_lock);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_sttext_overlay_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstSTTextOverlay *overlay = GST_STTEXT_OVERLAY (object);
  GstTeletextDec *teletext = &overlay->teletext_dec;

  GST_OBJECT_LOCK (overlay);
  switch (prop_id) {
    case PROP_TEXT:
      if (overlay->default_text != NULL) {
        g_free (overlay->default_text);
      }
      overlay->default_text = g_value_dup_string (value);
      break;
    case PROP_SHADING:
      overlay->want_shading = g_value_get_boolean (value);
      break;
    case PROP_XPAD:
      overlay->xpad = g_value_get_int (value);
      break;
    case PROP_YPAD:
      overlay->ypad = g_value_get_int (value);
      break;
    case PROP_DELTAX:
      overlay->deltax = g_value_get_int (value);
      break;
    case PROP_DELTAY:
      overlay->deltay = g_value_get_int (value);
      break;
    case PROP_XPOS:
      overlay->xpos = g_value_get_double (value);
      break;
    case PROP_YPOS:
      overlay->ypos = g_value_get_double (value);
      break;
    case PROP_HALIGN:
    {
      const gchar *s = g_value_get_string (value);

      if (s && g_ascii_strcasecmp (s, "left") == 0)
        overlay->halign = GST_STTEXT_OVERLAY_HALIGN_LEFT;
      else if (s && g_ascii_strcasecmp (s, "center") == 0)
        overlay->halign = GST_STTEXT_OVERLAY_HALIGN_CENTER;
      else if (s && g_ascii_strcasecmp (s, "right") == 0)
        overlay->halign = GST_STTEXT_OVERLAY_HALIGN_RIGHT;
      else
        g_warning ("Invalid value '%s' for textoverlay property 'halign'",
            GST_STR_NULL (s));
      break;
    }
    case PROP_VALIGN:
    {
      const gchar *s = g_value_get_string (value);

      if (s && g_ascii_strcasecmp (s, "baseline") == 0)
        overlay->valign = GST_STTEXT_OVERLAY_VALIGN_BASELINE;
      else if (s && g_ascii_strcasecmp (s, "bottom") == 0)
        overlay->valign = GST_STTEXT_OVERLAY_VALIGN_BOTTOM;
      else if (s && g_ascii_strcasecmp (s, "top") == 0)
        overlay->valign = GST_STTEXT_OVERLAY_VALIGN_TOP;
      else
        g_warning ("Invalid value '%s' for textoverlay property 'valign'",
            GST_STR_NULL (s));
      break;
    }
    case PROP_VALIGNMENT:
      overlay->valign = g_value_get_enum (value);
      break;
    case PROP_HALIGNMENT:
      overlay->halign = g_value_get_enum (value);
      break;
    case PROP_WRAP_MODE:
      overlay->wrap_mode = g_value_get_enum (value);
      gst_sttext_overlay_update_wrap_mode (overlay);
      break;
    case PROP_FONT_DESC:
    {
      PangoFontDescription *desc;
      const gchar *fontdesc_str;

      fontdesc_str = g_value_get_string (value);
      desc = pango_font_description_from_string (fontdesc_str);
      if (desc) {
        GST_DEBUG_OBJECT (overlay, "font description set: %s", fontdesc_str);
        pango_layout_set_font_description (overlay->layout, desc);
        gst_sttext_overlay_adjust_values_with_fontdesc (overlay, desc);
        pango_font_description_free (desc);
      } else {
        GST_WARNING_OBJECT (overlay, "font description parse failed: %s",
            fontdesc_str);
      }
      break;
    }
    case PROP_COLOR:
      overlay->color = g_value_get_uint (value);
      break;
    case PROP_SILENT:
      overlay->enable_ancillary = g_value_get_boolean (value);
      gst_sttext_push_text_frame (overlay, FALSE);
      break;
    case PROP_LINE_ALIGNMENT:
      overlay->line_align = g_value_get_enum (value);
      pango_layout_set_alignment (overlay->layout,
          (PangoAlignment) overlay->line_align);
      break;
    case PROP_WAIT_TEXT:
      overlay->wait_text = g_value_get_boolean (value);
      break;
    case PROP_AUTO_ADJUST_SIZE:
      overlay->auto_adjust_size = g_value_get_boolean (value);
      overlay->need_render = TRUE;
      break;
    case PROP_VERTICAL_RENDER:
      overlay->use_vertical_render = g_value_get_boolean (value);
      gst_sttext_overlay_update_render_mode (overlay);
      overlay->need_render = TRUE;
      break;
      /* Closed Caption properties */
    case PROP_CC_USER_CMD:
      overlay->cc_user_cmd = g_value_get_int (value);
      if (overlay->cc_ud_ready == TRUE) {
        if (GST_IS_TASK (overlay->cc_task)) {
          if (overlay->cc_user_cmd) {
            gst_sttext_cc_send_command (overlay, GST_STTEXT_CC_CMD_START);
          } else {
            gst_sttext_cc_send_command (overlay, GST_STTEXT_CC_CMD_STOP);
            gst_sttext_push_text_frame (overlay, FALSE);
          }
        }
      }
      break;
    case PROP_TELETEXT_PAGENO:
    {
      gint cur_page_no;
      cur_page_no = teletext->pageno;
      teletext->pageno = (gint) vbi_bin2bcd (g_value_get_int (value));
      teletext->subno = -1;
      if ((cur_page_no != teletext->pageno) && (teletext->pageno == 0x888)) {
        gst_dvb_overlay_flush_teletext (overlay);
        gst_sttext_push_text_frame (overlay, FALSE);
      }
      break;
    }
    case PROP_TELETEXT_SUBPAGENO:
      teletext->subno = g_value_get_int (value);
      if (teletext->subno == 0)
        teletext->subno = -1;
      break;
    case PROP_TELETEXT_SUBTITLES_MODE:
      teletext->subtitles_mode = g_value_get_boolean (value);
      break;
    case PROP_TELETEXT_SUBTITLES_TEMPLATE:
      if (teletext->subtitles_template != NULL) {
        g_free (teletext->subtitles_template);
      }
      teletext->subtitles_template = g_value_dup_string (value);
      break;
    case PROP_TELETEXT_FONT_DESCRIPTION:
      if (teletext->font_description != NULL) {
        g_free (teletext->font_description);
      }
      teletext->font_description = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

  overlay->need_render = TRUE;
  GST_OBJECT_UNLOCK (overlay);

  return;
}

static void
gst_sttext_overlay_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstSTTextOverlay *overlay = GST_STTEXT_OVERLAY (object);
  GstTeletextDec *teletext = &overlay->teletext_dec;

  GST_OBJECT_LOCK (overlay);
  switch (prop_id) {
    case PROP_TEXT:
      g_value_set_string (value, overlay->default_text);
      break;
    case PROP_SHADING:
      g_value_set_boolean (value, overlay->want_shading);
      break;
    case PROP_XPAD:
      g_value_set_int (value, overlay->xpad);
      break;
    case PROP_YPAD:
      g_value_set_int (value, overlay->ypad);
      break;
    case PROP_DELTAX:
      g_value_set_int (value, overlay->deltax);
      break;
    case PROP_DELTAY:
      g_value_set_int (value, overlay->deltay);
      break;
    case PROP_XPOS:
      g_value_set_double (value, overlay->xpos);
      break;
    case PROP_YPOS:
      g_value_set_double (value, overlay->ypos);
      break;
    case PROP_VALIGNMENT:
      g_value_set_enum (value, overlay->valign);
      break;
    case PROP_HALIGNMENT:
      g_value_set_enum (value, overlay->halign);
      break;
    case PROP_WRAP_MODE:
      g_value_set_enum (value, overlay->wrap_mode);
      break;
    case PROP_SILENT:
      g_value_set_boolean (value, overlay->enable_ancillary);
      break;
    case PROP_LINE_ALIGNMENT:
      g_value_set_enum (value, overlay->line_align);
      break;
    case PROP_WAIT_TEXT:
      g_value_set_boolean (value, overlay->wait_text);
      break;
    case PROP_AUTO_ADJUST_SIZE:
      g_value_set_boolean (value, overlay->auto_adjust_size);
      break;
    case PROP_VERTICAL_RENDER:
      g_value_set_boolean (value, overlay->use_vertical_render);
      break;
    case PROP_COLOR:
      g_value_set_uint (value, overlay->color);
      break;
    case PROP_TELETEXT_PAGENO:
      g_value_set_int (value, (gint) vbi_bcd2dec (teletext->pageno));
      break;
    case PROP_TELETEXT_SUBPAGENO:
      g_value_set_int (value, teletext->subno);
      break;
    case PROP_CC_USER_CMD:
      g_value_set_int (value, overlay->cc_user_cmd);
      break;
    case PROP_TELETEXT_SUBTITLES_MODE:
      g_value_set_boolean (value, teletext->subtitles_mode);
      break;
    case PROP_TELETEXT_SUBTITLES_TEMPLATE:
      g_value_set_string (value, teletext->subtitles_template);
      break;
    case PROP_TELETEXT_FONT_DESCRIPTION:
      g_value_set_string (value, teletext->font_description);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

  GST_OBJECT_UNLOCK (overlay);

  return;
}

static GstStateChangeReturn
gst_sttext_overlay_change_state (GstElement * element,
    GstStateChange transition)
{
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
  GstStructure *structure;
  GstMessage *message;
  GstSTTextOverlay *overlay = GST_STTEXT_OVERLAY (element);

  GST_DEBUG_OBJECT (overlay, "%d -> %d",
      GST_STATE_TRANSITION_CURRENT (transition),
      GST_STATE_TRANSITION_NEXT (transition));

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      /* Initialize the pes to es filter */
      gst_stm_pes_parser_init (overlay);
      /* Initialize dvb subtitles */
      gst_stm_dvb_subtitle_init (overlay);
      /* Initialize teletext */
      gst_stm_dvb_teletext_init (overlay);
      /* Initialize overlay */
      gst_stm_overlay_init (overlay);

      /* Create the audio/video and text sync task */
      g_rec_mutex_init (&overlay->task_mutex);
      overlay->render_task =
          gst_task_new ((GstTaskFunction) gst_sttext_av_text_sync, overlay,
          NULL);
      gst_object_set_name (GST_OBJECT_CAST (overlay->render_task),
          "SMF-Overlay Sync");
      if (GST_IS_TASK (overlay->render_task)) {
        gst_task_set_lock (overlay->render_task, &overlay->task_mutex);
      }
      /* Closed caption task setup */

      overlay->cc_task_sem.counter = 0;
      overlay->cc_sync_sem.counter = 0;
      overlay->type = GST_STTEXT_OVERLAY_CC;
      overlay->cc_term = FALSE;
      overlay->cc_start = FALSE;
      overlay->cc_stop = TRUE;
      overlay->cc_state = GST_STTEXT_CC_STATE_IDLE;
      g_rec_mutex_init (&overlay->cc_task_mutex);
      overlay->cc_task =
          gst_task_new ((GstTaskFunction) gst_sttext_cc_task, overlay, NULL);
      gst_object_set_name (GST_OBJECT_CAST (overlay->cc_task),
          "SMF-Overlay CC");
      if (GST_IS_TASK (overlay->cc_task)) {
        gst_task_set_lock (overlay->cc_task, &overlay->cc_task_mutex);
        gst_task_start (overlay->cc_task);
      }
      break;

    case GST_STATE_CHANGE_READY_TO_PAUSED:
      structure =
          gst_structure_new ("sttextoverlay", "state-change", G_TYPE_BOOLEAN,
          TRUE, NULL);
      message = gst_message_new_element (GST_OBJECT (element), structure);
      gst_element_post_message (element, message);
      GST_OBJECT_LOCK (overlay);
      /* Video States */
      overlay->video_flushing = FALSE;
      overlay->video_eos = FALSE;
      gst_segment_init (&overlay->segment, GST_FORMAT_TIME);
      overlay->text_flushing = FALSE;
      /* Dvb or Text Subtitle States */
      if (overlay->type == GST_STTEXT_OVERLAY_DVB_SUBTITLE) {
        gst_segment_init (&overlay->subtitle_segment, GST_FORMAT_TIME);
      } else {
        overlay->text_eos = FALSE;
        gst_segment_init (&overlay->text_segment, GST_FORMAT_TIME);
        overlay->new_text = FALSE;
      }
      /* start the task */
      if (overlay->render_task) {
        gst_task_start (overlay->render_task);
      }
      GST_OBJECT_UNLOCK (overlay);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      /* Pause render task so that it does not keep pad busy */
      if (GST_IS_TASK (overlay->render_task)) {
        gst_task_pause (overlay->render_task);
        g_rec_mutex_lock (&overlay->task_mutex);
        g_rec_mutex_unlock (&overlay->task_mutex);
      }
      /* need to broadcast here otherwise change state may get blocked */
      /* if *_text_chain is waiting on condition during the change state */
      overlay->video_flushing = TRUE;
      overlay->text_flushing = TRUE;
      GST_STTEXT_OVERLAY_BROADCAST (overlay);
      break;

    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
  if (ret == GST_STATE_CHANGE_FAILURE)
    return ret;

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      GST_OBJECT_LOCK (overlay);
      /* flush dvb subtitles */
      if (overlay->type == GST_STTEXT_OVERLAY_DVB_SUBTITLE) {
        gst_dvbsub_overlay_flush_subtitles (overlay);
      }
      GST_OBJECT_UNLOCK (overlay);
      GST_STTEXT_OVERLAY_BROADCAST (overlay);
      gst_sttext_overlay_reset (overlay);
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      /* Clean up for render task */
      if (GST_IS_TASK (overlay->render_task)) {
        gst_task_stop (overlay->render_task);
        g_rec_mutex_lock (&overlay->task_mutex);
        g_rec_mutex_unlock (&overlay->task_mutex);
        gst_task_join (overlay->render_task);
        gst_object_unref (overlay->render_task);
        overlay->render_task = NULL;
      }
      /* Clean up for closed caption task */
      if (GST_IS_TASK (overlay->cc_task)) {
        overlay->cc_term = TRUE;
        gst_sttext_cc_send_command (overlay, GST_STTEXT_CC_CMD_TERM);
        gst_task_stop (overlay->cc_task);
        g_rec_mutex_lock (&overlay->cc_task_mutex);
        g_rec_mutex_unlock (&overlay->cc_task_mutex);
        gst_task_join (overlay->cc_task);
        gst_object_unref (overlay->cc_task);
        overlay->cc_task = NULL;
      }
      g_rec_mutex_clear (&overlay->task_mutex);
      g_rec_mutex_clear (&overlay->cc_task_mutex);
      /* free the pes to es filter */
      gst_stm_pes_parser_deinit (overlay);
      /* de-init dvb subtitles */
      gst_stm_dvb_subtitle_deinit (overlay);
      /* de-init teletext */
      gst_stm_dvb_teletext_deinit (overlay);
      /* de-init overlay */
      gst_stm_overlay_deinit (overlay);
      break;
    default:
      break;
  }

  return ret;
}

static gboolean
gst_sttext_overlay_setcaps (GstSTTextOverlay * overlay, GstCaps * caps)
{
  GstStructure *structure;
  const GValue *fps;
  gboolean ret = FALSE;
  gint width = 0, height = 0;

  g_return_val_if_fail (gst_caps_is_fixed (caps), FALSE);

  GST_DEBUG_OBJECT (overlay, "caps to set %" GST_PTR_FORMAT, caps);

  overlay->width = 0;
  overlay->height = 0;

  structure = gst_caps_get_structure (caps, 0);

  if (gst_structure_has_name (structure, "video/x-fake-yuv") == FALSE)
    goto done;

  fps = gst_structure_get_value (structure, "framerate");
  if (fps == NULL) {
    GST_WARNING_OBJECT (overlay, "unknown frame rate");
    overlay->fps_n = 25;
    overlay->fps_d = 1;
  } else {
    overlay->fps_n = gst_value_get_fraction_numerator (fps);
    overlay->fps_d = gst_value_get_fraction_denominator (fps);
  }

  /* parse more structures */
  /* if width is not available in structure, take default as 720 pi */
  if (gst_structure_get_int (structure, "width", &width)) {
    if ((overlay->width == 0) && (width == 0)) {
      GST_WARNING_OBJECT (overlay, "unknown width");
      overlay->width = 720;
      GST_DEBUG_OBJECT (overlay, "width is being set to default value %d",
          overlay->width);
    } else if (width != 0) {
      overlay->width = width;
      GST_DEBUG_OBJECT (overlay, "width is being set to value %d",
          overlay->width);
    } else {
      GST_DEBUG_OBJECT (overlay, "width will remain same as earlier %d",
          overlay->width);
    }
  } else {
    GST_DEBUG_OBJECT (overlay, "width will remain same as earlier %d",
        overlay->width);
  }

  /* if height is not available in structure, take default as 400 lines */
  if (gst_structure_get_int (structure, "height", &height)) {
    if ((overlay->height == 0) && (height == 0)) {
      GST_WARNING_OBJECT (overlay, "unknown height");
      overlay->height = 400;
      GST_DEBUG_OBJECT (overlay, "height is being set to default value %d",
          overlay->height);
    } else if (height != 0) {
      overlay->height = height;
      GST_DEBUG_OBJECT (overlay, "height is being set to value %d",
          overlay->height);
    } else {
      GST_DEBUG_OBJECT (overlay, "height will remain same as earlier %d",
          overlay->height);
    }
  }

  if (gst_structure_get_int (structure, "video-fd", &overlay->video_fd)) {
    if (overlay->video_fd == 0) {
      overlay->video_fd = -1;
    }
  }
  GST_DEBUG_OBJECT (overlay, "width %d height %d", overlay->width,
      overlay->height);

  overlay->format = GST_VIDEO_FORMAT_BGRx;

  /* generate a new caps to set src pad caps */
  {
    GstStructure *s;
    GstCaps *c;


    s = gst_structure_new ("video/x-raw",
        "width", G_TYPE_INT, overlay->width,
        "height", G_TYPE_INT, overlay->height,
        "framerate", GST_TYPE_FRACTION, overlay->fps_n, overlay->fps_d,
        "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
        "is_ancillary", G_TYPE_BOOLEAN, FALSE,
        "is_ancillary_silent", G_TYPE_BOOLEAN, FALSE,
        "is_upstream_ancillary", G_TYPE_BOOLEAN, TRUE,
        "format", G_TYPE_STRING, "BGRA", NULL);

    if (s == NULL)
      goto done;

    c = gst_caps_new_full (s, NULL);
    if (c == NULL) {
      gst_structure_free (s);
      goto done;
    }

    ret = gst_pad_push_event (overlay->srcpad, gst_event_new_caps (c));
    if (ret == FALSE)
      GST_DEBUG_OBJECT (overlay, "source pad caps set failed");
    gst_caps_unref (c);
  }
  overlay->cc_ud_ready = TRUE;
done:

  return ret;
}

static gboolean
gst_sttext_overlay_video_event (GstPad * pad, GstObject * parent,
    GstEvent * event)
{
  gboolean ret = FALSE;
  GstSTTextOverlay *overlay = GST_STTEXT_OVERLAY (parent);

  GST_DEBUG_OBJECT (overlay, "%s - (%s)", __FUNCTION__,
      gst_event_type_get_name (GST_EVENT_TYPE (event)));


  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_SEGMENT:
    {
      GstFormat format;
      const GstSegment *segment;

      gst_event_parse_segment (event, &segment);

      format = segment->format;

      if (format == GST_FORMAT_TIME) {
        GST_DEBUG_OBJECT (overlay, "VIDEO SEGMENT now: %" GST_SEGMENT_FORMAT,
            &overlay->segment);
        gst_segment_copy_into (segment, &overlay->segment);
      } else {
        GST_WARNING_OBJECT (overlay,
            "received non-TIME newsegment event on video input");
      }

      ret = gst_pad_event_default (pad, parent, event);
    }
      break;
    case GST_EVENT_EOS:
      GST_OBJECT_LOCK (overlay);
      GST_INFO_OBJECT (overlay, "video EOS");
      overlay->video_eos = TRUE;
      GST_OBJECT_UNLOCK (overlay);
      ret = gst_pad_event_default (pad, parent, event);
      break;
    case GST_EVENT_FLUSH_START:
      GST_OBJECT_LOCK (overlay);
      GST_INFO_OBJECT (overlay, "video flush start");
      overlay->video_flushing = TRUE;
      overlay->first_video_frame_received = FALSE;
      GST_OBJECT_UNLOCK (overlay);
      ret = gst_pad_event_default (pad, parent, event);
      break;
    case GST_EVENT_FLUSH_STOP:
      GST_OBJECT_LOCK (overlay);
      GST_INFO_OBJECT (overlay, "video flush stop");
      overlay->video_flushing = FALSE;
      overlay->video_eos = FALSE;
      overlay->init_pvbt = TRUE;        /* previous_video_buffer_time */
      gst_segment_init (&overlay->segment, GST_FORMAT_TIME);
      GST_OBJECT_UNLOCK (overlay);
      ret = gst_pad_event_default (pad, parent, event);
      break;
    case GST_EVENT_CAPS:
    {
      GstCaps *caps;
      gst_event_parse_caps (event, &caps);
      if ((overlay->prev_vid_caps != NULL)
          && (gst_caps_is_equal ((const GstCaps *) overlay->prev_vid_caps,
                  (const GstCaps *) caps))) {
        GST_DEBUG_OBJECT (overlay, "caps are already set");
        return TRUE;
      }
      ret = gst_sttext_overlay_setcaps (overlay, caps);
      if (ret) {
        if (overlay->prev_vid_caps) {
          gst_caps_unref (overlay->prev_vid_caps);
        }
        overlay->prev_vid_caps = gst_caps_ref (caps);
      } else {
        GST_ERROR_OBJECT (overlay, "failed to set caps!");
      }
      gst_event_unref (event);
    }
      break;
    default:
      ret = gst_pad_event_default (pad, parent, event);
      break;
  }

  return ret;
}

/* allocate a frame for rendering */
GstFlowReturn
gst_sttext_overlay_allocate_frame (GstSTTextOverlay * overlay,
    GstBuffer ** buffer)
{
  GstFlowReturn ret = GST_FLOW_OK;
  GstStructure *s;
  GstCaps *c;
  gint size = 0;
  GstMapInfo info_write;
  /* REVIEW/FIXME : Just use a static string for format */
  gchar *format;

  if (overlay->first_video_frame_received) {
    if ((overlay->type == GST_STTEXT_OVERLAY_TELETEXT)
        && (overlay->current_teletext != NULL)) {
      if (overlay->current_teletext->page != NULL) {
        /* for teletext, each symbol/character is of size 12*10 as per zvbi lib */
        overlay->width =
            (overlay->current_teletext->page->columns +
            2 * TELETEXT_MAX_SIDE_BLACK_PANE_WIDTH) *
            TELETEXT_WIDTH_OF_CHARACTER;
        overlay->height =
            (overlay->current_teletext->page->rows +
            2 * TELETEXT_MAX_SIDE_BLACK_PANE_WIDTH) *
            TELETEXT_HEIGHT_OF_CHARACTER;
        size = overlay->width * overlay->height * sizeof (vbi_rgba);
      }
    } else if ((overlay->type == GST_STTEXT_OVERLAY_DVB_SUBTITLE)
        && (overlay->current_subtitle != NULL)) {
      overlay->width = overlay->current_subtitle->display_def.display_width;
      overlay->height = overlay->current_subtitle->display_def.display_height;
      size = overlay->width * overlay->height * 4;
    } else if ((overlay->type == GST_STTEXT_OVERLAY_CC)
        && (overlay->current_teletext != NULL)) {
      if (overlay->current_teletext->page != NULL) {
        /* for closed caption, each symbol/character is of size 16*26 as per zvbi lib */
        overlay->fps_n = 25;
        overlay->fps_d = 1;
        overlay->width = (overlay->current_teletext->page->columns +
            2 * CC_MAX_SIDE_BLACK_PANE_WIDTH) * CC_WIDTH_OF_CHARACTER;
        overlay->height = (overlay->current_teletext->page->rows +
            2 * CC_MAX_SIDE_BLACK_PANE_WIDTH) * CC_HEIGHT_OF_CHARACTER;
        size = overlay->width * overlay->height * sizeof (vbi_rgba);
      }
    } else {
      /* default value */
      overlay->width =
          (overlay->width >
          MAX_PLANE_WIDTH) ? overlay->width >> 1 : overlay->width;
      overlay->height =
          (overlay->height >
          MAX_PLANE_HEIGHT) ? overlay->height >> 1 : overlay->height;
      size = overlay->width * overlay->height * 4;
    }
  }
  if (size == 0) {
    overlay->width =
        (overlay->width >
        MAX_PLANE_WIDTH) ? overlay->width >> 1 : overlay->width;
    overlay->height =
        (overlay->height >
        MAX_PLANE_HEIGHT) ? overlay->height >> 1 : overlay->height;
    size = overlay->width * overlay->height * 4;
  }
  /* REVIEW/FIXME : Just use a static string for format */
  if (overlay->type == GST_STTEXT_OVERLAY_TELETEXT && overlay->enable_ancillary) {
    /* zvbi supports only RGBA format */
    format = g_strdup_printf ("%s", "RGBA");
  } else {
    /* for subtitles, BGRA format will be used */
    format = g_strdup_printf ("%s", "BGRA");
  }

  /* get the size of new buffer needed since we used ARGB32, so the buffer size should be width x height x 4 */
  s = gst_structure_new ("video/x-raw",
      "width", G_TYPE_INT, overlay->width,
      "height", G_TYPE_INT, overlay->height,
      "framerate", GST_TYPE_FRACTION, overlay->fps_n, overlay->fps_d,
      "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
      "is_ancillary", G_TYPE_BOOLEAN, TRUE,
      "is_ancillary_silent", G_TYPE_BOOLEAN, !(overlay->enable_ancillary),
      "is_upstream_ancillary", G_TYPE_BOOLEAN, TRUE,
      "format", G_TYPE_STRING, format, NULL);

  if (s == NULL)
    goto alloc_failed;
  c = gst_caps_new_full (s, NULL);
  if (c == NULL) {
    gst_structure_free (s);
    goto alloc_failed;
  }
  g_free (format);

  *buffer = gst_buffer_new_and_alloc (size);
  if (*buffer == NULL)
    goto alloc_failed;

  gst_buffer_map (*buffer, &info_write, GST_MAP_WRITE);

  GST_OBJECT_UNLOCK (overlay);
  gst_pad_push_event (overlay->srcpad, gst_event_new_caps (c));
  GST_OBJECT_LOCK (overlay);
  gst_caps_unref (c);

  memset (info_write.data, 0, size);
  GST_BUFFER_TIMESTAMP (*buffer) = GST_CLOCK_TIME_NONE;
  GST_BUFFER_DURATION (*buffer) = GST_CLOCK_TIME_NONE;
  gst_buffer_unmap (*buffer, &info_write);
  return ret;

alloc_failed:
  GST_DEBUG_OBJECT (overlay, "pad_alloc failed %d (%s)", ret,
      gst_flow_get_name (ret));
  return ret;
}

/* this function renders ancillary data */
GstFlowReturn
gst_sttext_push_text_frame (GstSTTextOverlay * overlay, gboolean Blit)
{
  GstFlowReturn ret = GST_FLOW_OK;
  GstBuffer *outbuf = NULL;
  ret = gst_sttext_overlay_allocate_frame (overlay, &outbuf);
  if (Blit && ret == GST_FLOW_OK) {
    switch (overlay->type) {
      case GST_STTEXT_OVERLAY_DVB_SUBTITLE:
        gst_stm_dvb_blit_subtitle (overlay, overlay->current_subtitle, outbuf);
        break;
      case GST_STTEXT_OVERLAY_TELETEXT:
        gst_stm_dvb_blit_teletext (overlay, outbuf);
        break;
      case GST_STTEXT_OVERLAY_CC:
        gst_closedcaption_blit_cc (overlay, outbuf);
        break;
      case GST_STTEXT_OVERLAY_FILE_SUBTITLE:
        gst_sttext_overlay_blit_frame (overlay, outbuf);
        break;
      default:
        break;
    }
  }

  if (ret == GST_FLOW_OK) {
    GST_OBJECT_UNLOCK (overlay);
    ret = gst_pad_push (overlay->srcpad, outbuf);
    GST_OBJECT_LOCK (overlay);
  }
  return ret;
}

static GstFlowReturn
gst_sttext_overlay_video_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buffer)
{
  GstFlowReturn ret = GST_FLOW_OK;
  GstSTTextOverlay *overlay = GST_STTEXT_OVERLAY (parent);;
  GstSTTextOverlayClass *klass = GST_STTEXT_OVERLAY_GET_CLASS (overlay);
  gchar *text = NULL;

  if (overlay->video_flushing) {
    ret = GST_FLOW_FLUSHING;
    goto done;
  }

  if (overlay->video_eos) {
    ret = GST_FLOW_EOS;
    goto done;
  }
  overlay->subtitle_flush_finish = FALSE;

  /* text pad not linked, rendering internal text */
  if (!overlay->text_linked) {
    GST_OBJECT_LOCK (overlay);
    if (klass->get_text)
      text = klass->get_text (overlay);
    else
      text = g_strdup (overlay->default_text);

    GST_DEBUG_OBJECT (overlay,
        "Text pad not linked, rendering default " "text: '%s'",
        GST_STR_NULL (text));

    if (text != NULL) {
      if (overlay->need_render == TRUE) {
        /* for text blit overlay type should be file */
        overlay->type = GST_STTEXT_OVERLAY_FILE_SUBTITLE;
        if (*text != '\0') {
          GST_DEBUG_OBJECT (overlay, "rendering and pushing text %s \n", text);
          gst_sttext_overlay_render_text (overlay, text, -1);
          ret = gst_sttext_push_text_frame (overlay, TRUE);
        } else {
          GST_DEBUG_OBJECT (overlay, "no text so pushing blank frame \n");
          ret = gst_sttext_push_text_frame (overlay, FALSE);
        }
        overlay->need_render = FALSE;
      }
      g_free (text);
    }
    GST_OBJECT_UNLOCK (overlay);
  } else {
    /* send one dummy buffer to drive the pipeline */
    GST_OBJECT_LOCK (overlay);
    ret = gst_sttext_push_text_frame (overlay, FALSE);

    /* need to notify the text chain that source pad caps has been set */
    if (overlay->first_video_frame_received == FALSE) {
      GST_STTEXT_OVERLAY_BROADCAST (overlay);
    }

    if (ret == GST_FLOW_OK) {
      overlay->first_video_frame_received = TRUE;
      overlay->blanking_done = TRUE;
    }
    GST_OBJECT_UNLOCK (overlay);
  }
  gst_buffer_unref (buffer);

done:
  return ret;
}

static gboolean
gst_sttext_overlay_setcaps_txt (GstSTTextOverlay * overlay, GstCaps * caps)
{
  GstStructure *structure;

  /* stop previous decoding in case of channel switch */
  if (overlay->type == GST_STTEXT_OVERLAY_DVB_SUBTITLE
      || overlay->type == GST_STTEXT_OVERLAY_TELETEXT) {
    gst_pes_filter_drain (&overlay->pesfilter);
    if (overlay->type == GST_STTEXT_OVERLAY_DVB_SUBTITLE) {
      gst_dvbsub_overlay_flush_subtitles (overlay);
      gst_stm_dvb_subtitle_deinit (overlay);
      gst_stm_dvb_subtitle_init (overlay);
    } else if (overlay->type == GST_STTEXT_OVERLAY_TELETEXT) {
      gst_stm_dvb_teletext_deinit (overlay);
      gst_stm_dvb_teletext_init (overlay);
    }
    gst_sttext_push_text_frame (overlay, FALSE);
  }

  structure = gst_caps_get_structure (caps, 0);
  if (gst_structure_has_name (structure, "ancillary/none")) {
    overlay->type = GST_STTEXT_OVERLAY_UNKNOWN;
  } else if (gst_structure_has_name (structure, "subpicture/x-dvb")) {

    overlay->type = GST_STTEXT_OVERLAY_DVB_SUBTITLE;
  } else if (gst_structure_has_name (structure, "private/teletext") == TRUE) {
    overlay->type = GST_STTEXT_OVERLAY_TELETEXT;
  } else if (gst_structure_has_name (structure, "private/closecaption") == TRUE) {

    overlay->type = GST_STTEXT_OVERLAY_CC;
  } else
    overlay->type = GST_STTEXT_OVERLAY_FILE_SUBTITLE;

  /* SRT/SSA/ASS Subtitle */
  overlay->have_pango_markup =
      gst_structure_has_name (structure, "text/x-pango-markup");
  gst_sttext_overlay_update_wrap_mode (overlay);

  return TRUE;
}

static gboolean
gst_sttext_overlay_text_event (GstPad * pad, GstObject * parent,
    GstEvent * event)
{
  gboolean ret = FALSE;
  GstSTTextOverlay *overlay = GST_STTEXT_OVERLAY (parent);

  GST_DEBUG_OBJECT (overlay, "%s - (%s)", __FUNCTION__,
      gst_event_type_get_name (GST_EVENT_TYPE (event)));


  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_SEGMENT:
    {
      GstFormat fmt;
      const GstSegment *segment;

      overlay->text_eos = FALSE;

      gst_event_parse_segment (event, &segment);

      fmt = segment->format;

      if (fmt == GST_FORMAT_TIME) {
        GST_OBJECT_LOCK (overlay);
        gst_segment_copy_into (segment, &overlay->text_segment);
        GST_DEBUG_OBJECT (overlay, "TEXT SEGMENT now: %" GST_SEGMENT_FORMAT,
            &overlay->text_segment);
        GST_OBJECT_UNLOCK (overlay);
      } else {
        GST_WARNING_OBJECT (overlay,
            "received non-TIME newsegment event on text input");
      }

      gst_event_unref (event);
      ret = TRUE;
    }
      break;
    case GST_EVENT_FLUSH_STOP:
    {
      GST_OBJECT_LOCK (overlay);
      GST_INFO_OBJECT (overlay, "text flush stop");
      gst_sttext_overlay_flush (overlay);

      overlay->text_eos = FALSE;
      gst_segment_init (&overlay->text_segment, GST_FORMAT_TIME);

      overlay->text_flushing = FALSE;
      GST_OBJECT_UNLOCK (overlay);
      gst_event_unref (event);
      ret = TRUE;
    }
      break;
    case GST_EVENT_FLUSH_START:
    {
      GST_INFO_OBJECT (overlay, "text flush start");
      GST_OBJECT_LOCK (overlay);
      overlay->text_flushing = TRUE;
      GST_STTEXT_OVERLAY_BROADCAST (overlay);
      GST_OBJECT_UNLOCK (overlay);
      gst_event_unref (event);
      ret = TRUE;
    }
      break;
    case GST_EVENT_EOS:
    {
      GST_OBJECT_LOCK (overlay);
      overlay->text_eos = TRUE;
      GST_INFO_OBJECT (overlay, "text EOS");
      GST_OBJECT_UNLOCK (overlay);
      gst_event_unref (event);
      ret = TRUE;
    }
      break;
    case GST_EVENT_CAPS:
    {
      GstCaps *caps;
      GST_OBJECT_LOCK (overlay);
      gst_event_parse_caps (event, &caps);
      if ((overlay->prev_text_caps != NULL)
          && (gst_caps_is_equal ((const GstCaps *) overlay->prev_text_caps,
                  (const GstCaps *) caps))) {
        GST_DEBUG_OBJECT (overlay, "caps are already set");
        GST_OBJECT_UNLOCK (overlay);
        return TRUE;
      }
      ret = gst_sttext_overlay_setcaps_txt (overlay, caps);
      if (ret) {
        if (overlay->prev_text_caps) {
          gst_caps_unref (overlay->prev_text_caps);
        }
        overlay->prev_text_caps = gst_caps_ref (caps);
      } else {
        GST_ERROR_OBJECT (overlay, "failed to set caps!");
      }
      GST_OBJECT_UNLOCK (overlay);
      gst_event_unref (event);
    }
      break;
    case GST_EVENT_CUSTOM_DOWNSTREAM_OOB:
    {
      /* event pushed from playbin in response to input selector change of active sink pad */
      if (gst_event_has_name (event, "playsink-custom-subtitle-flush-finish")) {
        overlay->subtitle_flush_finish = TRUE;
      }
      gst_event_unref (event);
      ret = TRUE;
    }
      break;
    default:
      ret = gst_pad_event_default (pad, parent, event);
      break;
  }

  return ret;
}

static GstPadLinkReturn
gst_sttext_overlay_text_pad_link (GstPad * pad, GstObject * parent,
    GstPad * peer)
{
  GstSTTextOverlay *overlay;

  overlay = GST_STTEXT_OVERLAY (parent);

  GST_DEBUG_OBJECT (overlay, "Text pad linked");

  overlay->text_linked = TRUE;

  return GST_PAD_LINK_OK;
}

static void
gst_sttext_overlay_text_pad_unlink (GstPad * pad, GstObject * parent)
{
  GstSTTextOverlay *overlay;

  overlay = GST_STTEXT_OVERLAY (parent);

  GST_DEBUG_OBJECT (overlay, "Text pad unlinked");

  overlay->text_linked = FALSE;

  gst_segment_init (&overlay->text_segment, GST_FORMAT_UNDEFINED);

  return;
}

static GstFlowReturn
gst_sttext_overlay_text_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buffer)
{
  GstFlowReturn ret = GST_FLOW_OK;
  GstClockTime stop = GST_CLOCK_TIME_NONE;
  GstSTTextOverlay *overlay = GST_STTEXT_OVERLAY (parent);
  gboolean in_seg = FALSE;
  guint64 clip_start = 0, clip_stop = 0;

  if (overlay->type == GST_STTEXT_OVERLAY_UNKNOWN) {
    gst_buffer_unref (buffer);
    ret = GST_FLOW_OK;
    goto done;
  }

  if (overlay->type == GST_STTEXT_OVERLAY_CC) {
    gst_buffer_unref (buffer);
    ret = GST_FLOW_OK;
    goto done;
  }

  if (overlay->text_flushing) {
    gst_buffer_unref (buffer);
    ret = GST_FLOW_FLUSHING;
    goto done;
  }

  if (overlay->text_eos) {
    gst_buffer_unref (buffer);
    ret = GST_FLOW_EOS;
    goto done;
  }

  if (overlay->first_video_frame_received == FALSE) {
    GST_OBJECT_LOCK (overlay);
    GST_STTEXT_OVERLAY_WAIT (overlay);
    GST_OBJECT_UNLOCK (overlay);
  }

  /* free buffer and do not inject for PES parsing */
  if (overlay->rate < 0) {
    if (overlay->type != GST_STTEXT_OVERLAY_TELETEXT) {
      gst_buffer_unref (buffer);
      ret = GST_FLOW_OK;
      goto done;
    }
  }

  if (overlay->type == GST_STTEXT_OVERLAY_DVB_SUBTITLE
      || overlay->type == GST_STTEXT_OVERLAY_TELETEXT) {
    GST_OBJECT_LOCK (overlay);
    /* PES to ES conversion */

    gst_stm_pes_parser (overlay, buffer);

    gst_buffer_unref (buffer);
    GST_OBJECT_UNLOCK (overlay);
  } else {
    GST_LOG_OBJECT (overlay, "%" GST_SEGMENT_FORMAT
        " BUFFER: start=%" GST_TIME_FORMAT
        ", end=%" GST_TIME_FORMAT,
        &overlay->segment,
        GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (buffer)),
        GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (buffer) +
            GST_BUFFER_DURATION (buffer)));

    /* check timing in segment (in case of seek) */
    if (GST_BUFFER_TIMESTAMP_IS_VALID (buffer)) {
      if (GST_BUFFER_DURATION_IS_VALID (buffer))
        stop = GST_BUFFER_TIMESTAMP (buffer) + GST_BUFFER_DURATION (buffer);
      else
        stop = GST_CLOCK_TIME_NONE;

      in_seg =
          gst_segment_clip (&overlay->text_segment, GST_FORMAT_TIME,
          GST_BUFFER_TIMESTAMP (buffer), stop, &clip_start, &clip_stop);
    } else {
      in_seg = TRUE;
    }
    if (in_seg == TRUE) {
      /* adjust start and duration after segament clip */
      if (GST_BUFFER_TIMESTAMP_IS_VALID (buffer))
        GST_BUFFER_TIMESTAMP (buffer) = clip_start;
      else if (GST_BUFFER_DURATION_IS_VALID (buffer))
        GST_BUFFER_DURATION (buffer) = clip_stop - clip_start;

      GST_OBJECT_LOCK (overlay);

      overlay->text_buffer = buffer;
      overlay->new_text = TRUE;

      overlay->current_text_render_time = GST_BUFFER_TIMESTAMP (buffer);
      overlay->blanking_frame_render_time = stop;

      overlay->need_render = TRUE;

      /* wait for video chain signal */
      if (overlay->text_flushing == FALSE) {
        GST_STTEXT_OVERLAY_WAIT (overlay);
      }

      if (overlay->text_flushing) {
        gst_buffer_unref (buffer);
        overlay->text_buffer = NULL;
        GST_OBJECT_UNLOCK (overlay);
        ret = GST_FLOW_FLUSHING;
        goto done;
      }

      overlay->new_text = FALSE;
      gst_buffer_unref (buffer);
      overlay->text_buffer = NULL;
      GST_OBJECT_UNLOCK (overlay);
    } else {
      gst_buffer_unref (buffer);        // out of segament and drop the buffer
    }
  }
done:
  return ret;
}

static gboolean
gst_sttext_overlay_src_event (GstPad * pad, GstObject * parent,
    GstEvent * event)
{
  gboolean ret = FALSE;
  gdouble rate;
  GstSTTextOverlay *overlay = GST_STTEXT_OVERLAY (parent);

  GST_DEBUG_OBJECT (overlay, "%s - (%s)", __FUNCTION__,
      gst_event_type_get_name (GST_EVENT_TYPE (event)));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_SEEK:
    {
      GstSeekFlags flags;

      gst_event_parse_seek (event, &rate, NULL, &flags, NULL, NULL, NULL, NULL);
      /* we don't handle seek if we have no text pad */
      /* we don't give seek to text pad during reverse trickmode */
      if ((!overlay->text_linked)
          || ((rate == overlay->rate) && (rate < 0.0))) {
        GST_DEBUG_OBJECT (overlay, "seek received, pushing upstream");
        ret = gst_pad_push_event (overlay->video_sinkpad, event);
        goto done;
      }

      overlay->rate = rate;

      /* Seek on each sink pad */
      gst_event_ref (event);
      ret = gst_pad_push_event (overlay->video_sinkpad, event);
      if ((ret == TRUE) && (rate > 0.0)) {
        ret = gst_pad_push_event (overlay->text_sinkpad, event);
      } else {
        gst_event_unref (event);
      }
    }
      break;
    default:
      if (overlay->text_linked) {
        gst_event_ref (event);
        ret = gst_pad_push_event (overlay->video_sinkpad, event);
        if (ret == TRUE) {
          ret = gst_pad_push_event (overlay->text_sinkpad, event);
        }
      } else {
        ret = gst_pad_push_event (overlay->video_sinkpad, event);
      }
      break;
  }

done:
  return ret;
}

static gboolean
gst_sttext_overlay_src_query (GstPad * pad, GstObject * parent,
    GstQuery * query)
{
  gboolean ret = FALSE;
  GstSTTextOverlay *overlay = GST_STTEXT_OVERLAY (parent);

  GST_DEBUG_OBJECT (overlay, "%s - (%s)", __FUNCTION__,
      gst_query_type_get_name (GST_QUERY_TYPE (query)));

  ret = gst_pad_peer_query (overlay->video_sinkpad, query);

  return ret;
}

void
gst_sttext_overlay_flush (GstSTTextOverlay * overlay)
{
  GST_LOG_OBJECT (overlay, "flush as per overlay_type=%d", overlay->type);
  if (overlay->type == GST_STTEXT_OVERLAY_DVB_SUBTITLE) {
    gst_pes_filter_drain (&overlay->pesfilter);
    gst_dvbsub_overlay_flush_subtitles (overlay);
  } else if (overlay->type == GST_STTEXT_OVERLAY_TELETEXT) {
    gst_pes_filter_drain (&overlay->pesfilter);
    gst_dvb_overlay_flush_teletext (overlay);
  } else if (overlay->type == GST_STTEXT_OVERLAY_CC) {
    gst_dvb_overlay_flush_closed_caption (overlay);
  } else {
    GST_DEBUG_OBJECT (overlay, "Unknown ancillary type, nothing to flush.");
  }
  return;
}

gboolean
sttextoverlay_init (GstPlugin * plugin)
{
  GST_DEBUG_CATEGORY_INIT (sttextoverlay_debug, "sttextoverlay", 0,
      "ST text overlay element");
  return gst_element_register (plugin, "sttextoverlay", (GST_RANK_PRIMARY + 10),
      GST_TYPE_STTEXT_OVERLAY);
}
