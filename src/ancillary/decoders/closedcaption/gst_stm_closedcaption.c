/*
 * GStreamer
 * Copyright (C) 2009 Sebastian Pölsterl <sebp@k-d-w.org>
 * Copyright (C) 2010 Andoni Morales Alastruey <ylatuya@gmail.com>
 * Copyright (C) <2012> STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA
 *
 * This file has been modified by STMicroelectronics, on 05/19/2012
 */


#include "gststtextoverlay.h"
#include "string.h"
#include "gst_stm_closedcaption.h"

#define UNUSED(x) ((void)(x))

guint32
cc_get_bits (guint32 NbRequestedBits, guint8 * const DataBuff_p,
    GstWindowCommand WindowCommand)
{
  guint32 RequestedBits = 0;    /* Returned value */
  static struct
  {
    guint32 Value;              /* Current U32 Window value */
    guint32 NumBits;            /* Current valid Window bits */
    guint8 *DataPointer_p;      /* Current Window data pointer */
  } BitWindows;                 /* Parsing window */

  guint32 Value = 0;
  guint8 *DataPointer_p = NULL;
  guint32 NumBits = 0;

  if (WindowCommand == GST_STTEXT_CC_RESET_WINDOW) {
    /* Reset the the bits window */
    BitWindows.Value = 0;
    BitWindows.NumBits = 0;
    BitWindows.DataPointer_p = (guint8 *) DataBuff_p;
  }

  /* Get parsing window settings */
  Value = BitWindows.Value;
  DataPointer_p = BitWindows.DataPointer_p;
  NumBits = BitWindows.NumBits;

  /* Take bits in the bits window */
  if (NumBits != 0) {
    RequestedBits = Value >> (32 - NbRequestedBits);
  }

  if (NbRequestedBits <= NumBits) {
    /* If enough bits, return the bits */
    Value = Value << (NbRequestedBits);
    /* and update the bits windows */
    NumBits -= NbRequestedBits;
  } else {
    /* If not enough bits, re-fill the bits window */
    NbRequestedBits -= NumBits;
    NumBits = 32;
    Value = (*DataPointer_p++) << 24;
    Value |= (*DataPointer_p++) << 16;
    Value |= (*DataPointer_p++) << 8;
    Value |= (*DataPointer_p++) << 0;
    RequestedBits |= (Value >> (32 - NbRequestedBits));
    Value = Value << (NbRequestedBits);
    NumBits -= NbRequestedBits;

  }

  if (WindowCommand != GST_STTEXT_CC_BACKUP_WINDOW) {
    /* Store new parsing window settings */
    BitWindows.Value = Value;
    BitWindows.NumBits = NumBits;
    BitWindows.DataPointer_p = DataPointer_p;
  }

  return (RequestedBits);
}

void
parseEIA708_cc_data (GstCCUserData * cc_user_data_block,
    GstCCFormatMode FormatMode, GstSTTextOverlay * overlay)
{
  guint8 *DataBuff_p = NULL;
  guint32 Index;
  gboolean process_cc_data_flag = FALSE, additional_data_flag =
      FALSE, cc_valid = FALSE;
  guint8 em_data = 0, cc_count = 0, marker_bits = 0;
  guint8 one_bit = 0, cc_type = 0, cc_data1 = 0, cc_data2 = 0;

  UNUSED (one_bit);
  UNUSED (em_data);
  UNUSED (marker_bits);

  DataBuff_p = cc_user_data_block->data;

  cc_get_bits (1, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW);     /* Reserved */
  process_cc_data_flag =
      (cc_get_bits (1, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW) & 0x01);
  if (process_cc_data_flag) {
    additional_data_flag = (guint8) (cc_get_bits (1, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW) & 0x01);  /* This flag shall be set to 0 (SCTE128 --> zero_bit). */
    cc_count =
        (guint8) cc_get_bits (5, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW);
    em_data = cc_get_bits (8, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW); /* Reserved */
    for (Index = 0; Index < cc_count; Index++) {
      one_bit = (guint8) (cc_get_bits (1, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW) & 0x01);     /* SCTE128 --> one_bit */
      cc_get_bits (4, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW); /* Marker_bits (SCTE128 --> Reserved) */
      cc_valid =
          (cc_get_bits (1, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW) & 0x01);
      cc_type =
          (guint8) cc_get_bits (2, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW);
      cc_data1 =
          (guint8) cc_get_bits (8, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW);
      cc_data2 =
          (guint8) cc_get_bits (8, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW);
      if (cc_valid) {
        if ((cc_type == 0) || (cc_type == 1)) { /* NTSC line 21 data field or valid DTV Channel Data */

          overlay->cc_data_block[overlay->cc_data_index].vsync_field =
              cc_type ? 1 : 0;
          overlay->cc_data_block[overlay->cc_data_index].cc_bytes[0] = cc_data1;
          overlay->cc_data_block[overlay->cc_data_index].cc_bytes[1] = cc_data2;

          overlay->cc_data_index++;

        }
      } else {
        if ((cc_type == 2) || (cc_type == 3)) {
          /*TBD: SlotData_p->DTVCC_ChannelPacketStart = FALSE; */
        }
      }
    }
    marker_bits = cc_get_bits (8, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW);     /* marker_bits */
  }

  if (additional_data_flag) {
    /* to be done : additional user data */
  }
}


GstCCFormatMode
gst_closedcaption_detect_SCTE21 (GstCCUserData * cc_user_data_block)
{
  guint32 identifier;

  identifier =
      (guint32) cc_get_bits (32, cc_user_data_block->data,
      GST_STTEXT_CC_RESET_WINDOW);

  if (identifier == ATSC_IDENTIFIER) {
    return (GST_STTEXT_OVERLAY_CC_FORMAT_SCTE21);
  }

  return (GST_STTEXT_OVERLAY_CC_FORMAT_DETECT);

}

GstCCFormatMode
gst_closedcaption_detect_SCTE128 (GstCCUserData * cc_user_data_block)
{
  guint32 user_identifier;

  user_identifier =
      (guint32) cc_get_bits (32, cc_user_data_block->data,
      GST_STTEXT_CC_RESET_WINDOW);
  if ((cc_user_data_block->is_registered)
      && (cc_user_data_block->itu_t_t35_country_code == USA_COUNTRY_CODE)
#if defined(CC_H264_DETECT_MANUFACTURER_CODE)
      && (cc_user_data_block->itu_t_t35_provider_code == NEW_MANUFACTURER_CODE)
#endif /* CC_H264_DETECT_MANUFACTURER_CODE */
      && (user_identifier == ATSC_IDENTIFIER)
      ) {
    return (GST_STTEXT_OVERLAY_CC_FORMAT_SCTE128);
  }

  return (GST_STTEXT_OVERLAY_CC_FORMAT_DETECT);
}




void
gst_closedcaption_parse_SCTE21 (GstSTTextOverlay * overlay)
{
  guint32 identifier;
  guint8 *data_buffer;
  guint8 user_data_type_code;
  GstCCUserData *cc_user_data_block = &overlay->cc_user_data_block;

  data_buffer = cc_user_data_block->data;
  identifier =
      (guint32) cc_get_bits (32, data_buffer, GST_STTEXT_CC_RESET_WINDOW);
  if (identifier == ATSC_IDENTIFIER) {
    user_data_type_code = (guint8) cc_get_bits (8, data_buffer, GST_STTEXT_CC_UPDATE_WINDOW);   /* 0x03 : cc_data structure */
    /* 0x06 : bar_data(structure */
    if (user_data_type_code == GST_STTEXT_CC_CLOSED_CAPTION_EIA) {
      parseEIA708_cc_data (&overlay->cc_user_data_block,
          GST_STTEXT_OVERLAY_CC_FORMAT_SCTE21, overlay);
    }
  }
}

void
gst_closedcaption_parse_SCTE128 (GstSTTextOverlay * overlay)
{

  guint8 *DataBuff_p = NULL;
  guint8 user_data_type_code = 0;
  guint32 user_identifier = 0;

  if ((overlay->cc_user_data_block.length >= 11)
      && (overlay->cc_user_data_block.is_registered)
      && (overlay->cc_user_data_block.itu_t_t35_country_code ==
          USA_COUNTRY_CODE)
#if defined(CC_H264_DETECT_MANUFACTURER_CODE)
      && (overlay->cc_user_data_block.itu_t_t35_provider_code ==
          NEW_MANUFACTURER_CODE)
#endif /* CC_H264_DETECT_MANUFACTURER_CODE */
      ) {
    DataBuff_p = overlay->cc_user_data_block.data;
    user_identifier =
        (guint32) cc_get_bits (32, DataBuff_p, GST_STTEXT_CC_RESET_WINDOW);
    user_data_type_code =
        (guint8) cc_get_bits (8, DataBuff_p, GST_STTEXT_CC_UPDATE_WINDOW);

    if ((user_identifier == ATSC_IDENTIFIER)
        && (user_data_type_code == GST_STTEXT_CC_CLOSED_CAPTION_EIA)) {
      parseEIA708_cc_data (&overlay->cc_user_data_block,
          GST_STTEXT_OVERLAY_CC_FORMAT_SCTE128, overlay);
    }
  }
}

void
gst_closedcaption_extact_cc_ud_block (GstSTTextOverlay * overlay)
{
  guint8 *user_data_buffer_ptr = NULL;
  GstUserData *user_data = &overlay->user_data;
  GstCCUserData *cc_user_data_block = &overlay->cc_user_data_block;

  GstUserdataGeneric *cc_data_generic = NULL;
  GstUserDataH264 *cc_data_h264 = NULL;

  user_data_buffer_ptr = user_data->user_data_ptr;
  cc_data_generic = (GstUserdataGeneric *) user_data_buffer_ptr;
  user_data_buffer_ptr += sizeof (GstUserdataGeneric);

  if (cc_data_generic->is_there_a_pts) {
    if (cc_data_generic->pts_msb)
      cc_user_data_block->pts =
          (guint64) (0x100000000 | (guint64) cc_data_generic->pts);
    cc_user_data_block->pts =
        gst_util_uint64_scale_int (cc_data_generic->pts / 90, GST_SECOND, 1000);
  }

  if (cc_data_generic->codec_id == GST_STTEXT_CC_CODEC_TYPE_H264) {
    cc_data_h264 = (GstUserDataH264 *) user_data_buffer_ptr;
    cc_user_data_block->is_registered = cc_data_h264->is_registered;
    cc_user_data_block->itu_t_t35_country_code =
        cc_data_h264->itu_t_t35_country_code;
    cc_user_data_block->itu_t_t35_country_code_extension_byte =
        cc_data_h264->itu_t_t35_country_code_extension_byte;
    cc_user_data_block->itu_t_t35_provider_code =
        cc_data_h264->itu_t_t35_provider_code;
    memcpy (cc_user_data_block->uuid_iso_iec_11578,
        cc_data_h264->uuid_iso_iec_11578, 16);
    user_data_buffer_ptr += sizeof (GstUserDataH264);
  }
  if (cc_data_generic->codec_id == GST_STTEXT_CC_CODEC_TYPE_MPEG2) {
    user_data_buffer_ptr += sizeof (GstUserDataMpeg2);
  }
  user_data_buffer_ptr += cc_data_generic->padding_bytes;
  memcpy (cc_user_data_block->data, user_data_buffer_ptr,
      cc_data_generic->block_length - (cc_data_generic->header_length +
          cc_data_generic->padding_bytes));

  /* Though the complete block is processed later on but we declare it as consumed */
  overlay->user_data.user_data_processed += cc_data_generic->block_length;
  overlay->user_data.user_data_ptr += cc_data_generic->block_length;
}

void
gst_closedcaption_blit_cc (GstSTTextOverlay * overlay, GstBuffer * buf)
{

  GstTeletextDec *teletext = &overlay->teletext_dec;
  guint32 read_offset = 0;
  GstMapInfo info_read;

  g_mutex_lock (&teletext->queue_lock);
  gst_buffer_map (buf, &info_read, GST_MAP_READ);

  read_offset =
      (overlay->width) * (overlay->height) * sizeof (vbi_rgba) -
      (overlay->width -
      2 * CC_WIDTH_OF_CHARACTER * CC_MAX_SIDE_BLACK_PANE_WIDTH) *
      (overlay->height -
      2 * CC_HEIGHT_OF_CHARACTER * CC_MAX_SIDE_BLACK_PANE_WIDTH) *
      sizeof (vbi_rgba);
  if (overlay->current_teletext->page != NULL) {

    vbi_draw_cc_page (overlay->current_teletext->page, VBI_PIXFMT_RGBA32_LE,
        (vbi_rgba *) ((guint8 *) info_read.data + read_offset));

    vbi_unref_page (overlay->current_teletext->page);

    g_free (overlay->current_teletext->page);
    overlay->current_teletext->page = NULL;
  }

  gst_teletext_caption_render_page (overlay, buf, read_offset);

  gst_buffer_unmap (buf, &info_read);
  g_mutex_unlock (&teletext->queue_lock);
  return;
}
