/*
 * GStreamer
 * Copyright (C) 2009 Sebastian Pölsterl <sebp@k-d-w.org>
 * Copyright (C) 2010 Andoni Morales Alastruey <ylatuya@gmail.com>
 * Copyright (C) <2012> STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA
 *
 * This file has been modified by STMicroelectronics, on 05/19/2012
 */


#ifndef _CLOSED_CAPTION_H_
#define _CLOSED_CAPTION_H_

#include <glib.h>
#include "gststtextoverlay.h"

G_BEGIN_DECLS
#define MAX_CC_BLOCKS           7000
#define COPY_SIZE               300     /* max size of a user data copy */
#define ATSC_IDENTIFIER         0x47413934
#define AFD_IDENTIFIER          0x44544731
#define SA_IDENTIFIER           0x53415544
#define USA_COUNTRY_CODE        0xB5    /* USA country code, defined by ITU T35 as 0xB5. */
#if defined(STCC_H264_DETECT_MANUFACTURER_CODE)
#define DTV_MANUFACTURER_CODE   0x002F  /* DIRECTV manufacturers code, 0x002F. */
#define NEW_MANUFACTURER_CODE   0x0031  /* UNKNOWN manufacturers code, 0x0031. */
#endif /* STCC_H264_DETECT_MANUFACTURER_CODE */
#define CC_LINE_TOP             21
#define CC_LINE_BOTTOM          284
typedef struct _GstSTTextOverlay *GstSTTextOverlay_h;
typedef struct _GstTeletextDec *GstTeletextDec_h;

typedef enum
{
  GST_STTEXT_CC_BYTE0_INDEX = 0,
  GST_STTEXT_CC_BYTE1_INDEX,
  GST_STTEXT_CC_MAX_BYTES
} GstSTTextCCByteIndex;

typedef enum
{
  GST_STTEXT_CC_VSYNC_TOP = 0,
  GST_STTEXT_CC_VSYNC_BOTTOM
} GstSTTextVsync;

typedef enum
{
  GST_STTEXT_CC_FORBIDDEN_0,
  GST_STTEXT_CC_SA_CUE_TRIGGER,
  GST_STTEXT_CC_REESSERVED_2,
  GST_STTEXT_CC_CLOSED_CAPTION_EIA,
  GST_STTEXT_CC_ADDITIONAL_EIA_608,
  GST_STTEXT_CC_LUMA_PAM,
  GST_STTEXT_CC_BAR_DATA_EIA
} GstUserDataTypeCode;

typedef enum
{
  GST_STTEXT_CC_CODEC_TYPE_MPEG2 = 0,
  GST_STTEXT_CC_CODEC_TYPE_H264 = 1
} GstVideoCodecType;



typedef struct _GstCCData GstCCData;
typedef struct _GstCCUserData GstCCUserData;
typedef struct _GstUserDataHeader GstUserDataHeader;

typedef struct _GstUserdataGeneric GstUserdataGeneric;
typedef struct _GstUserDataH264 GstUserDataH264;
typedef struct _GstUserDataMpeg2 GstUserDataMpeg2;
struct _GstCCData
{
  gboolean vsync_field;
  guint8 cc_bytes[GST_STTEXT_CC_MAX_BYTES];
};

struct _GstCCUserData
{

  guint32 used;
  gboolean is_registered;
  guint8 itu_t_t35_country_code;
  guint8 itu_t_t35_country_code_extension_byte;
  guint16 itu_t_t35_provider_code;
  gchar uuid_iso_iec_11578[16];
  guint32 length;
  guint64 pts;
  guint8 data[COPY_SIZE];

};

struct _GstUserDataHeader
{
  guint32 STUD;                 /* reserved = 32    */
  guint16 BlockLength;          /* block_length=16  */
  guint8 HeaderLength;          /* header_length=8  */
  guint8 Flags;                 /* reserved=6 / stream_abridgement=1 / buffering_overflow=1 */
  guint8 CodecId;               /* codec_id=8  */
  guint8 PTSFlags;              /* reserved=6 / is_there_a_pts=1 / is_pts_interpolated=1 */
  guint64 PTS;                  /* reserved=31 / PTS=33  */
};

struct _GstUserdataGeneric
{
  gchar id[4];
  guint block_length:16;
  guint header_length:16;
  guint reserved_1:1;
  guint padding_bytes:5;
  guint stream_abridgement:1;
  guint overflow:1;
  guint codec_id:8;
  guint reserved_2:6;
  guint is_there_a_pts:1;
  guint is_pts_interpolated:1;
  guint reserved_3:7;
  guint pts_msb:1;
  guint pts:32;
};

struct _GstUserDataH264
{
  guint reserved:31;
  guint is_registered:1;
  guint itu_t_t35_country_code:8;
  guint itu_t_t35_country_code_extension_byte:8;
  guint itu_t_t35_provider_code:16;
  gchar uuid_iso_iec_11578[16];
};

struct _GstUserDataMpeg2
{
  guint reserved:31;
  guint top_field_first:1;
};

typedef enum
{
  GST_STTEXT_OVERLAY_CC_FORMAT_DETECT, GST_STTEXT_OVERLAY_CC_FORMAT_DTVVID21, GST_STTEXT_OVERLAY_CC_FORMAT_EIA708, GST_STTEXT_OVERLAY_CC_FORMAT_DVS157, GST_STTEXT_OVERLAY_CC_FORMAT_UDATA130, GST_STTEXT_OVERLAY_CC_FORMAT_EIA608, GST_STTEXT_OVERLAY_CC_FORMAT_DVB, GST_STTEXT_OVERLAY_CC_FORMAT_ITU_T35, GST_STTEXT_OVERLAY_CC_FORMAT_SCTE128        /* User Data - SCTE 128 */
      , GST_STTEXT_OVERLAY_CC_FORMAT_ATSC       /* User Data - A/53, Part 4 */
      , GST_STTEXT_OVERLAY_CC_FORMAT_SCTE20 = GST_STTEXT_OVERLAY_CC_FORMAT_EIA608       /* User Data - SCTE 20 */
      , GST_STTEXT_OVERLAY_CC_FORMAT_SCTE21 = GST_STTEXT_OVERLAY_CC_FORMAT_EIA708       /* User Data - SCTE 21 */
      , GST_STTEXT_OVERLAY_CC_FORMAT_DTV_MPEG2 = GST_STTEXT_OVERLAY_CC_FORMAT_DTVVID21  /* User Data - DTV MPEG-2 Video */
      , GST_STTEXT_OVERLAY_CC_FORMAT_DTV_AVC = GST_STTEXT_OVERLAY_CC_FORMAT_ITU_T35     /* User Data - DTV AVC Video */
} GstCCFormatMode;

typedef enum WindowCommand_e
{
  GST_STTEXT_CC_RESET_WINDOW,
  GST_STTEXT_CC_UPDATE_WINDOW,
  GST_STTEXT_CC_BACKUP_WINDOW
} GstWindowCommand;



#define get_byte(data) (*data++)

GstCCFormatMode gst_closedcaption_detect_SCTE21 (GstCCUserData *
    cc_user_data_block);
GstCCFormatMode gst_closedcaption_detect_SCTE128 (GstCCUserData *
    cc_user_data_block);
void parseEIA708_cc_data (GstCCUserData * cc_user_data_block,
    GstCCFormatMode FormatMode, GstSTTextOverlay_h overlay);
guint32 cc_get_bits (guint32 NbRequestedBits, guint8 * const DataBuff_p,
    GstWindowCommand WindowCommand);
void gst_closedcaption_parse_SCTE21 (GstSTTextOverlay_h overlay);
void gst_closedcaption_blit_cc (GstSTTextOverlay_h overlay, GstBuffer * buf);
void gst_closedcaption_extact_cc_ud_block (GstSTTextOverlay_h overlay);
void gst_closedcaption_parse_SCTE128 (GstSTTextOverlay_h overlay);

G_END_DECLS
#endif /* _CLOSED_CAPTION_H_ */
