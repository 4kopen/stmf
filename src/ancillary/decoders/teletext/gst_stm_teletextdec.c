/*
 * GStreamer
 * Copyright (C) 2009 Sebastian Pölsterl <sebp@k-d-w.org>
 * Copyright (C) 2010 Andoni Morales Alastruey <ylatuya@gmail.com>
 * Copyright (C) <2012> STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA
 *
 * This file has been modified by STMicroelectronics, on 03/13/2012
 */

#include "gststtextoverlay.h"

#define MAX_SLICES 64           /*slice size of 64 accommodates for >1 page data per pes packet */

enum
{
  VBI_ERROR = -1,
  VBI_SUCCESS = 0,
  VBI_NEW_FRAME = 1
};

typedef struct
{
  int pgno;
  int subno;
} page_info;

typedef enum
{
  DATA_UNIT_EBU_TELETEXT_NON_SUBTITLE = 0x02,
  DATA_UNIT_EBU_TELETEXT_SUBTITLE = 0x03,
  DATA_UNIT_EBU_TELETEXT_INVERTED = 0x0C,

  DATA_UNIT_ZVBI_WSS_CPR1204 = 0xB4,
  DATA_UNIT_ZVBI_CLOSED_CAPTION_525 = 0xB5,
  DATA_UNIT_ZVBI_MONOCHROME_SAMPLES_525 = 0xB6,

  DATA_UNIT_VPS = 0xC3,
  DATA_UNIT_WSS = 0xC4,
  DATA_UNIT_CLOSED_CAPTION = 0xC5,
  DATA_UNIT_MONOCHROME_SAMPLES = 0xC6,

  DATA_UNIT_STUFFING = 0xFF,
} data_unit_id;

typedef enum
{
  SYSTEM_525 = 0,
  SYSTEM_625
} systems;


static void
gst_teletextdec_event_handler (vbi_event * ev, void *user_data)
{
  page_info *pi;
  vbi_pgno pgno;
  vbi_subno subno;

  GstTeletextDec *teletext = (GstTeletextDec *) user_data;

  switch (ev->type) {
    case VBI_EVENT_TTX_PAGE:
      pgno = ev->ev.ttx_page.pgno;
      subno = ev->ev.ttx_page.subno;

      if (pgno != teletext->pageno
          || (teletext->subno != -1 && subno != teletext->subno))
        return;

      pi = g_new (page_info, 1);
      pi->pgno = pgno;
      pi->subno = subno;

      g_mutex_lock (&teletext->queue_lock);

      g_queue_push_tail (teletext->queue, pi);
      g_mutex_unlock (&teletext->queue_lock);
      break;
    case VBI_EVENT_CAPTION:
      pgno = ev->ev.caption.pgno;
      pi = g_new (page_info, 1);
      pi->pgno = pgno;

      g_mutex_lock (&teletext->queue_lock);

      g_queue_push_tail (teletext->queue, pi);
      g_mutex_unlock (&teletext->queue_lock);
      break;
    default:
      break;
  }
  return;
}

static void
gst_teletextdec_zvbi_init (GstTeletextDec * teletext)
{
  g_return_if_fail (teletext != NULL);

  GST_LOG_OBJECT (teletext, "Initializing structures");

  teletext->decoder = vbi_decoder_new ();

  vbi_event_handler_register (teletext->decoder,
      VBI_EVENT_TTX_PAGE | VBI_EVENT_CAPTION,
      gst_teletextdec_event_handler, teletext);

  g_mutex_lock (&teletext->queue_lock);
  teletext->queue = g_queue_new ();
  g_mutex_unlock (&teletext->queue_lock);
}

static void
gst_teletextdec_reset_frame (GstTeletextDec * teletext)
{
  teletext->frame->current_slice = teletext->frame->sliced_begin;
  teletext->frame->sliced_end = teletext->frame->sliced_begin + MAX_SLICES;
  teletext->frame->last_field = 0;
  teletext->frame->last_field_line = 0;
  teletext->frame->last_frame_line = 0;
}


/* Converts the line_offset / field_parity byte of a VBI data unit. */
static void
gst_teletextdec_lofp_to_line (guint * field, guint * field_line,
    guint * frame_line, guint lofp, systems system)
{
  guint line_offset;

  /* field_parity */
  *field = !(lofp & (1 << 5));

  line_offset = lofp & 31;

  if (line_offset > 0) {
    static const guint field_start[2][2] = {
      {0, 263},
      {0, 313},
    };

    *field_line = line_offset;
    *frame_line = field_start[system][*field] + line_offset;
  } else {
    *field_line = 0;
    *frame_line = 0;
  }
}

static int
gst_teletextdec_line_address (GstTeletextDec * teletext,
    GstTeletextFrame * frame, vbi_sliced ** spp, guint lofp, systems system)
{
  guint field;
  guint field_line;
  guint frame_line;

  if (G_UNLIKELY (frame->current_slice >= frame->sliced_end)) {
    GST_LOG_OBJECT (teletext, "Out of sliced VBI buffer space (%d lines).",
        (int) (frame->sliced_end - frame->sliced_begin));
    return VBI_ERROR;
  }

  gst_teletextdec_lofp_to_line (&field, &field_line, &frame_line, lofp, system);

  GST_LOG_OBJECT (teletext, "Line %u/%u=%u.", field, field_line, frame_line);

  if (frame_line != 0) {
    GST_LOG_OBJECT (teletext, "Last frame Line %u.", frame->last_frame_line);
    if (frame_line <= frame->last_frame_line) {
      GST_LOG_OBJECT (teletext, "New frame");
      return VBI_NEW_FRAME;
    }

    frame->last_field = field;
    frame->last_field_line = field_line;
    frame->last_frame_line = frame_line;

    *spp = frame->current_slice++;
    (*spp)->line = frame_line;
  } else {
    /* Undefined line. */
    return VBI_ERROR;
  }

  return VBI_SUCCESS;
}


static gboolean
gst_teletextdec_extract_data_units (GstTeletextDec * teletext,
    GstTeletextFrame * f, guint8 * packet, guint * offset, gint size)
{
  guint8 *data_unit;
  guint i;

  while (*offset < size) {
    vbi_sliced *s = NULL;
    gint data_unit_id, data_unit_length;

    data_unit = packet + *offset;
    data_unit_id = data_unit[0];
    data_unit_length = data_unit[1];
    GST_LOG_OBJECT (teletext, "vbi header %02x %02x %02x\n", data_unit[0],
        data_unit[1], data_unit[2]);

    switch (data_unit_id) {
      case DATA_UNIT_STUFFING:
      {
        *offset += 2 + data_unit_length;
        break;
      }

      case DATA_UNIT_EBU_TELETEXT_NON_SUBTITLE:
      case DATA_UNIT_EBU_TELETEXT_SUBTITLE:
      {
        gint res;

        if (G_UNLIKELY (data_unit_length != 1 + 1 + 42)) {
          /* Skip this data unit */
          GST_WARNING_OBJECT (teletext, "The data unit length is not 44 bytes");
          *offset += 2 + data_unit_length;
          break;
        }

        res =
            gst_teletextdec_line_address (teletext, f, &s, data_unit[2],
            SYSTEM_625);
        if (G_UNLIKELY (res == VBI_ERROR)) {
          /* Can't retrieve line address, skip this data unit */
          GST_WARNING_OBJECT (teletext,
              "Could not retrieve line address for this data unit");
          return VBI_ERROR;
        }
        if (G_UNLIKELY (f->last_field_line > 0
                && (f->last_field_line - 7 >= 23 - 7))) {
          GST_WARNING_OBJECT (teletext, "Bad line: %d", f->last_field_line - 7);
          return VBI_ERROR;
        }
        if (res == VBI_NEW_FRAME) {
          /* New frame */
          return VBI_NEW_FRAME;
        }
        s->id = VBI_SLICED_TELETEXT_B;
        for (i = 0; i < 42; i++)
          s->data[i] = vbi_rev8 (data_unit[4 + i]);
        *offset += 46;
        break;
      }

      case DATA_UNIT_ZVBI_WSS_CPR1204:
      case DATA_UNIT_ZVBI_CLOSED_CAPTION_525:
      case DATA_UNIT_ZVBI_MONOCHROME_SAMPLES_525:
      case DATA_UNIT_VPS:
      case DATA_UNIT_WSS:
      case DATA_UNIT_CLOSED_CAPTION:
      case DATA_UNIT_MONOCHROME_SAMPLES:
      {
        /*Not supported yet */
        *offset += 2 + data_unit_length;
        break;
      }

      default:
      {
        /* corrupted stream, increase the offset by one until we sync */
        GST_LOG_OBJECT (teletext, "Corrupted, increasing offset by one");
        *offset += 1;
        break;
      }
    }
  }
  return VBI_SUCCESS;
}

void
gst_stm_cc_draw_page (GstSTTextOverlay * overlay, GstClockTime pts)
{
  page_info *page = NULL;
  gboolean success;
  GstTeletextInfo *teletext_info;
  GstTeletextDec *teletext = &overlay->teletext_dec;

  g_mutex_lock (&teletext->queue_lock);
  if (!g_queue_is_empty (teletext->queue)) {
    teletext_info = g_slice_new0 (GstTeletextInfo);
    teletext_info->page = g_new (vbi_page, 1);
    teletext_info->pts = pts;

    /* peek one closed caption page from the queue */
    page = (page_info *) g_queue_pop_head (teletext->queue);

    success =
        vbi_fetch_cc_page (teletext->decoder, teletext_info->page, page->pgno,
        FALSE);
    if (success == FALSE) {
      g_free (teletext_info->page);
      teletext_info->page = NULL;
    } else {
      if (teletext_info->page->pgno == 1)
        g_queue_push_tail (overlay->pending_closed_caption, teletext_info);
    }
    g_free (page);
  } else {
    teletext->page = NULL;
  }
  g_mutex_unlock (&teletext->queue_lock);
}

void
gst_teletextdec_process_telx_buffer (GstTeletextDec * teletext, GstBuffer * buf)
{
  guint8 *data;
  gint size;
  guint offset = 0;
  gint res;
  GstMapInfo info_read;

  gst_buffer_map (buf, &info_read, GST_MAP_READ);
  data = info_read.data;
  size = info_read.size;

  teletext->in_timestamp = GST_BUFFER_TIMESTAMP (buf);
  teletext->in_duration = GST_BUFFER_DURATION (buf);

  while (offset < size) {
    res =
        gst_teletextdec_extract_data_units (teletext, teletext->frame, data,
        &offset, size);

    if (res == VBI_NEW_FRAME) {
      /* We have a new frame, it's time to feed the decoder */
      vbi_sliced *s;
      gint n_lines;

      n_lines = teletext->frame->current_slice - teletext->frame->sliced_begin;
      GST_LOG_OBJECT (teletext, "Completed frame, decoding new %d lines",
          n_lines);
      s = g_memdup (teletext->frame->sliced_begin,
          n_lines * sizeof (vbi_sliced));
      vbi_decode (teletext->decoder, s, n_lines, teletext->last_ts);
      /* From vbi_decode():
       * timestamp shall advance by 1/30 to 1/25 seconds whenever calling this
       * function. Failure to do so will be interpreted as frame dropping, which
       * starts a resynchronization cycle, eventually a channel switch may be assumed
       * which resets even more decoder state. So even if a frame did not contain
       * any useful data this function must be called, with lines set to zero.
       */
      teletext->last_ts += 0.04;

      g_free (s);
      gst_teletextdec_reset_frame (teletext);
    } else if (res == VBI_ERROR) {
      gst_teletextdec_reset_frame (teletext);
      gst_buffer_unmap (buf, &info_read);
      return;
    }
  }
  gst_buffer_unmap (buf, &info_read);
  return;
}

void
gst_dvb_overlay_flush_teletext (GstSTTextOverlay * overlay)
{
  GstTeletextInfo *teletext = NULL;

  while ((teletext = g_queue_pop_head (overlay->pending_teletext))) {
    if (teletext->page != NULL) {
      vbi_unref_page (teletext->page);
      g_free (teletext->page);
      teletext->page = NULL;
    }
    g_slice_free (GstTeletextInfo, teletext);
  }
  gst_stm_dvb_teletext_free (overlay);
  return;
}

void
gst_dvb_overlay_flush_closed_caption (GstSTTextOverlay * overlay)
{
  GstTeletextInfo *teletext = NULL;

  while ((teletext = g_queue_pop_head (overlay->pending_closed_caption))) {
    if (teletext->page != NULL) {
      vbi_unref_page (teletext->page);
      g_free (teletext->page);
      teletext->page = NULL;
    }
    g_slice_free (GstTeletextInfo, teletext);
  }
  gst_stm_dvb_teletext_free (overlay);
  return;
}


void
gst_stm_dvb_teletext_free (GstSTTextOverlay * overlay)
{
  if (overlay->current_teletext) {
    overlay->current_teletext->pts = GST_CLOCK_TIME_NONE;
    if (overlay->current_teletext->page != NULL) {
      vbi_unref_page (overlay->current_teletext->page);
      g_free (overlay->current_teletext->page);
      overlay->current_teletext->page = NULL;
    }
    g_slice_free (GstTeletextInfo, overlay->current_teletext);
  }
  overlay->current_teletext = NULL;
}

void
gst_stm_dvb_get_teletext_render (GstSTTextOverlay * overlay)
{
  if (overlay->current_teletext == NULL) {      /* no pending teletext to display */
    if (!g_queue_is_empty (overlay->pending_teletext)) {        /* check pending teletext available */
      overlay->current_teletext = g_queue_peek_head (overlay->pending_teletext);        /* peek one teletext from the queue */
      g_queue_pop_head (overlay->pending_teletext);     /* pop from the queue since we already get the pointer */
    }
  }
}

void
gst_stm_dvb_get_cc_render (GstSTTextOverlay * overlay)
{
  if (overlay->current_teletext == NULL) {      /* no pending teletext to display */
    if (!g_queue_is_empty (overlay->pending_closed_caption)) {  /* check pending teletext available */
      overlay->current_teletext = g_queue_peek_head (overlay->pending_closed_caption);  /* peek one cc from the queue */
      g_queue_pop_head (overlay->pending_closed_caption);       /* pop from the queue since we already get the pointer */
    }
  }
}


GstFlowReturn
gst_stm_dvb_get_teletext (GstSTTextOverlay * overlay, GstClockTime pts)
{
  page_info *pi;
  gboolean success;
  GstTeletextInfo *teletext_info;

  GstTeletextDec *teletext = &overlay->teletext_dec;

  g_mutex_lock (&teletext->queue_lock);

  if (!g_queue_is_empty (teletext->queue)) {
    teletext_info = g_slice_new0 (GstTeletextInfo);
    teletext_info->page = g_new (vbi_page, 1);
    teletext_info->pts = pts;
    pi = (page_info *) g_queue_pop_head (teletext->queue);      /* peek one subtitle from the queue */
    success =
        vbi_fetch_vt_page (teletext->decoder, teletext_info->page, pi->pgno,
        pi->subno, VBI_WST_LEVEL_3p5, 25, FALSE);
    if (success == FALSE) {
      g_free (teletext_info->page);
      teletext_info->page = NULL;
    } else {
      g_queue_push_tail (overlay->pending_teletext, teletext_info);
    }
    g_free (pi);
  }
  g_mutex_unlock (&teletext->queue_lock);
  return GST_FLOW_OK;
}


  /*  render closed caption page to have black panels */
void
gst_teletext_caption_render_page (GstSTTextOverlay * overlay, GstBuffer * buf,
    guint32 read_offset)
{
  guint8 *write_ptr = NULL, *read_ptr = NULL;
  guint32 row_index, pixel_index;
  GstMapInfo info_read;

  gst_buffer_map (buf, &info_read, GST_MAP_READ);
  write_ptr = info_read.data;
  read_ptr = info_read.data + read_offset;
  guint character_width = 0, black_pane_width = 0, character_height = 0;

  if (overlay->type == GST_STTEXT_OVERLAY_TELETEXT) {
    GST_LOG_OBJECT (overlay, "setting scale params for teletext");
    character_width = TELETEXT_WIDTH_OF_CHARACTER;
    black_pane_width = TELETEXT_MAX_SIDE_BLACK_PANE_WIDTH;
    character_height = TELETEXT_HEIGHT_OF_CHARACTER;
  } else if (overlay->type == GST_STTEXT_OVERLAY_CC) {
    GST_LOG_OBJECT (overlay, "setting scale params for closed caption");
    character_width = CC_WIDTH_OF_CHARACTER;
    black_pane_width = CC_MAX_SIDE_BLACK_PANE_WIDTH;
    character_height = CC_HEIGHT_OF_CHARACTER;
  }

/*We shift the data location from its original position and ensure that it fits into
  the specified scaled region */

  for (row_index = 0; row_index < (overlay->height); row_index++) {
    for (pixel_index = 0; pixel_index < (overlay->width); pixel_index++) {
      if ((pixel_index >= character_width * black_pane_width)
          && (pixel_index < overlay->width - character_width * black_pane_width)
          && (row_index >= character_height * black_pane_width)
          && (row_index <
              (overlay->height - character_height * black_pane_width))) {
        /* actual teletext page */
        write_ptr[0] = read_ptr[0];
        write_ptr[1] = read_ptr[1];
        write_ptr[2] = read_ptr[2];
        write_ptr[3] = read_ptr[3];
        read_ptr = read_ptr + 4;
      } else {
        /* black side panels */
        write_ptr[0] = 0;
        write_ptr[1] = 0;
        write_ptr[2] = 0;
        write_ptr[3] = 0;
        /*for page 888 we want the lane to be transparent so that video is visible along with teletext subtitles */
        if ((overlay->teletext_dec.pageno != 0x888)
            && (overlay->type == GST_STTEXT_OVERLAY_TELETEXT))
          write_ptr[3] = 255;
        else
          write_ptr[3] = 0;

      }
      write_ptr = write_ptr + 4;
    }
  }
  gst_buffer_unmap (buf, &info_read);
}


void
gst_stm_dvb_blit_teletext (GstSTTextOverlay * overlay, GstBuffer * buf)
{
  GstTeletextDec *teletext = &overlay->teletext_dec;
  guint32 read_offset = 0;
  GstMapInfo info_read;

  gst_buffer_map (buf, &info_read, GST_MAP_READ);
  g_mutex_lock (&teletext->queue_lock);
  read_offset =
      (overlay->width) * (overlay->height) * sizeof (vbi_rgba) -
      (overlay->width -
      2 * TELETEXT_WIDTH_OF_CHARACTER * TELETEXT_MAX_SIDE_BLACK_PANE_WIDTH) *
      (overlay->height -
      2 * TELETEXT_HEIGHT_OF_CHARACTER * TELETEXT_MAX_SIDE_BLACK_PANE_WIDTH) *
      sizeof (vbi_rgba);
  if (overlay->current_teletext->page != NULL) {
    vbi_draw_vt_page (overlay->current_teletext->page, VBI_PIXFMT_RGBA32_LE,
        (vbi_rgba *) ((guint8 *) info_read.data + read_offset), FALSE, TRUE);
    vbi_unref_page (overlay->current_teletext->page);
    g_free (overlay->current_teletext->page);
    overlay->current_teletext->page = NULL;
  }

  gst_teletext_caption_render_page (overlay, buf, read_offset);

  gst_buffer_unmap (buf, &info_read);
  g_mutex_unlock (&teletext->queue_lock);
  return;
}


static void
gst_teletextdec_zvbi_clear (GstTeletextDec * teletext)
{
  g_return_if_fail (teletext != NULL);

  GST_LOG_OBJECT (teletext, "Clearing structures");

  if (teletext->decoder != NULL) {
    vbi_decoder_delete (teletext->decoder);
    teletext->decoder = NULL;
  }

  g_mutex_lock (&teletext->queue_lock);
  if (teletext->queue != NULL) {
    g_queue_free (teletext->queue);
    teletext->queue = NULL;
  }
  g_mutex_unlock (&teletext->queue_lock);
}


void
gst_stm_dvb_teletext_deinit (GstSTTextOverlay * overlay)
{
  GstTeletextDec *teletext = &overlay->teletext_dec;

  gst_teletextdec_zvbi_clear (teletext);

  if (teletext->frame != NULL) {
    if (teletext->frame->sliced_begin != NULL) {
      g_free (teletext->frame->sliced_begin);
      teletext->frame->sliced_begin = NULL;
    }
    g_free (teletext->frame);
    teletext->frame = NULL;
  }

  teletext->in_timestamp = GST_CLOCK_TIME_NONE;
  teletext->in_duration = GST_CLOCK_TIME_NONE;
  teletext->pageno = 0x100;
  teletext->subno = -1;
  teletext->last_ts = 0;
}



void
gst_stm_dvb_teletext_init (GstSTTextOverlay * overlay)
{
  GstTeletextDec *teletext = &overlay->teletext_dec;
  overlay->current_teletext = NULL;
  overlay->pending_teletext = g_queue_new ();
  overlay->pending_closed_caption = g_queue_new ();

  teletext->decoder = NULL;
  teletext->pageno = 0x100;
  teletext->subno = -1;
  teletext->in_timestamp = GST_CLOCK_TIME_NONE;
  teletext->in_duration = GST_CLOCK_TIME_NONE;

  teletext->rate_numerator = 0;
  teletext->rate_denominator = 1;

  teletext->queue = NULL;

  teletext->frame = g_new0 (GstTeletextFrame, 1);
  teletext->frame->sliced_begin = g_new (vbi_sliced, MAX_SLICES);

  teletext->last_ts = 0;
  teletext->page = NULL;

  gst_teletextdec_zvbi_init (teletext);
  return;
}
