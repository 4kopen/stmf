/* GStreamer DVB subtitles overlay
 * Copyright (c) 2010 Mart Raudsepp <mart.raudsepp@collabora.co.uk>
 * Copyright (C) <2012> STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * This file has been modified by STMicroelectronics, on 03/13/2012
 */

#include "gststtextoverlay.h"

void
gst_dvbsub_overlay_process_text (GstSTTextOverlay * overlay, GstBuffer * buffer,
    guint64 pts)
{
  guint8 *data;
  guint size;
  GstMapInfo info_read;

  gst_buffer_map (buffer, &info_read, GST_MAP_READ);
  data = (guint8 *) info_read.data;
  size = info_read.size;

  GST_DEBUG_OBJECT (overlay,
      "Processing subtitles with fake PTS=%" G_GUINT64_FORMAT
      " which is a running time of %" GST_TIME_FORMAT,
      pts, GST_TIME_ARGS (pts));
  GST_DEBUG_OBJECT (overlay, "Feeding %u bytes to libdvbsub", size);
  dvb_sub_feed_with_pts (overlay->dvb_sub, pts, data, size);
  gst_buffer_unmap (buffer, &info_read);
  gst_buffer_unref (buffer);
}

static void
new_dvb_subtitles_cb (DvbSub * dvb_sub, DVBSubtitles * subs, gpointer user_data)
{
  GstSTTextOverlay *overlay = GST_STTEXT_OVERLAY (user_data);
  g_queue_push_tail (overlay->pending_subtitles, subs);
}


void
gst_stm_dvb_subtitle_free (GstSTTextOverlay * overlay)
{
  if (overlay->current_subtitle) {
    dvb_subtitles_free (overlay->current_subtitle);
  }
  overlay->current_subtitle = NULL;
}

void
gst_dvbsub_overlay_flush_subtitles (GstSTTextOverlay * overlay)
{
  DVBSubtitles *subs;

  while ((subs = g_queue_pop_head (overlay->pending_subtitles))) {
    dvb_subtitles_free (subs);
  }

  gst_stm_dvb_subtitle_free (overlay);

  return;
}


void
gst_stm_dvb_subtitle_init (GstSTTextOverlay * overlay)
{
  DvbSubCallbacks dvbsub_callbacks = { &new_dvb_subtitles_cb, };
  /* DVB Subtitle Segment */
  gst_segment_init (&overlay->subtitle_segment, GST_FORMAT_TIME);
  overlay->current_subtitle = NULL;
  overlay->pending_subtitles = g_queue_new ();
  overlay->dvb_sub = dvb_sub_new ();
  /* Set the callback function pointer */
  dvb_sub_set_callbacks (overlay->dvb_sub, &dvbsub_callbacks, overlay);
  return;
}

void
gst_stm_dvb_subtitle_deinit (GstSTTextOverlay * overlay)
{
  if (overlay->pending_subtitles) {
    g_queue_free (overlay->pending_subtitles);
    overlay->pending_subtitles = NULL;
  }
  if (overlay->dvb_sub) {
    dvb_sub_free (overlay->dvb_sub);
    overlay->dvb_sub = NULL;
  }
  return;
}


void
gst_stm_dvb_get_subtitle_render (GstSTTextOverlay * overlay)
{
  if (overlay->current_subtitle == NULL) {      /* no pending subtitle to display */
    if (!g_queue_is_empty (overlay->pending_subtitles)) {       /* check pending subtitle available */
      overlay->current_subtitle = g_queue_peek_head (overlay->pending_subtitles);       /* peek one subtitle from the queue */
      g_queue_pop_head (overlay->pending_subtitles);    /* pop from the queue since we already get the pointer */
    }
  }
}


void
gst_stm_dvb_blit_subtitle (GstSTTextOverlay * overlay, DVBSubtitles * subs,
    GstBuffer * buffer)
{
  guint counter;
  DVBSubtitleRect *sub_region;
  guint32 color;
  const guint8 *src;
  guint8 *dst_y;
  gint x, y;
  gint y_stride;
  GstMapInfo info_read;

  /* TODO: here y_stride is 4 but it may be different so dont use hardcode value */
  y_stride = 4 * subs->display_def.display_width;

  gst_buffer_map (buffer, &info_read, GST_MAP_READ);
  for (counter = 0; counter < subs->num_rects; counter++) {
    gint dw, dh, dx, dy;

    sub_region = &subs->rects[counter];

    /* blend subtitles onto the video frame */
    dx = sub_region->x;
    dy = sub_region->y;
    dw = sub_region->w;
    dh = sub_region->h;

    if (subs->display_def.window_flag) {
      dx += subs->display_def.window_x;
      dy += subs->display_def.window_y;
    }

    src = sub_region->pict.data;

    if (dx) {
      /* TODO: here y_stride is 4 but it may be different so dont use hardcode value */
      dx = dx * 4;
    }

    dst_y = info_read.data + (dy * y_stride) + dx;

    for (y = 0; y < dh; y++) {
      for (x = 0; x < dw; x++) {
        color = sub_region->pict.palette[src[(y) * dw + (x)]];
        dst_y[0] = (color) & 0xff;      // B
        dst_y[1] = (color >> 8) & 0xff; // G
        dst_y[2] = ((color >> 16) & 0xff);      // R
        dst_y[3] = (color >> 24) & 0xff;        // alpha
        dst_y += 4;
      }
      for (x = dw; x < overlay->width; x++) {
        dst_y += 4;
      }
    }
  }
  gst_buffer_unmap (buffer, &info_read);
  GST_LOG_OBJECT (overlay, "amount of rendered DVBSubtitleRect: %u", counter);
}
