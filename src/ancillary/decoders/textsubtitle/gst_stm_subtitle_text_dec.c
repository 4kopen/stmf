/* GStreamer
 * Copyright (C) <1999> Erik Walthinsen <omega@cse.ogi.edu>
 * Copyright (C) <2003> David Schleef <ds@schleef.org>
 * Copyright (C) <2006> Julien Moutte <julien@moutte.net>
 * Copyright (C) <2006> Zeeshan Ali <zeeshan.ali@nokia.com>
 * Copyright (C) <2006-2008> Tim-Philipp Müller <tim centricular net>
 * Copyright (C) <2009> Young-Ho Cha <ganadist@gmail.com>
 * Copyright (C) <2012> STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * This file has been modified by STMicroelectronics, on 03/13/2012
 */

#include "gststtextoverlay.h"

#include <string.h>

#define CAIRO_UNPREMULTIPLY(a,r,g,b) G_STMT_START { \
  b = (a > 0) ? MIN ((b * 255 + a / 2) / a, 255) : 0; \
  g = (a > 0) ? MIN ((g * 255 + a / 2) / a, 255) : 0; \
  r = (a > 0) ? MIN ((r * 255 + a / 2) / a, 255) : 0; \
} G_STMT_END

#if G_BYTE_ORDER == G_LITTLE_ENDIAN
#define CAIRO_ARGB_A 3
#define CAIRO_ARGB_R 2
#define CAIRO_ARGB_G 1
#define CAIRO_ARGB_B 0
#else
#define CAIRO_ARGB_A 0
#define CAIRO_ARGB_R 1
#define CAIRO_ARGB_G 2
#define CAIRO_ARGB_B 3
#endif
#define MINIMUM_OUTLINE_OFFSET 1.0
#define DEFAULT_SCALE_BASIS    640


static void
gst_sttext_overlay_render_pangocairo (GstSTTextOverlay * overlay,
    const gchar * string, gint textlen)
{
  cairo_t *cr;
  cairo_surface_t *surface;
  PangoRectangle ink_rect, logical_rect;
  cairo_matrix_t cairo_matrix;
  int width, height;
  double scalef = 1.0;
  double a, r, g, b;

  if (overlay->auto_adjust_size) {
    /* 640 pixel is default */
    scalef = (double) (overlay->width) / DEFAULT_SCALE_BASIS;
  }
  pango_layout_set_width (overlay->layout, -1);
  /* set text on pango layout */
  pango_layout_set_markup (overlay->layout, string, textlen);

  /* get subtitle image size */
  pango_layout_get_pixel_extents (overlay->layout, &ink_rect, &logical_rect);

  width = (logical_rect.width + overlay->shadow_offset) * scalef;

  if (width + overlay->deltax >
      (overlay->use_vertical_render ? overlay->height : overlay->width)) {
    /*
     * subtitle image width is larger then overlay width
     * so rearrange overlay wrap mode.
     */
    gst_sttext_overlay_update_wrap_mode (overlay);
    pango_layout_get_pixel_extents (overlay->layout, &ink_rect, &logical_rect);
    width = overlay->width;
  }

  height =
      (logical_rect.height + logical_rect.y + overlay->shadow_offset) * scalef;
  if (height > overlay->height) {
    height = overlay->height;
  }

  if (overlay->use_vertical_render) {
    PangoRectangle rect;
    PangoContext *context;
    PangoMatrix matrix = PANGO_MATRIX_INIT;
    int tmp;

    context = pango_layout_get_context (overlay->layout);

    pango_matrix_rotate (&matrix, -90);

    rect.x = rect.y = 0;
    rect.width = width;
    rect.height = height;
    pango_matrix_transform_pixel_rectangle (&matrix, &rect);
    matrix.x0 = -rect.x;
    matrix.y0 = -rect.y;

    pango_context_set_matrix (context, &matrix);

    cairo_matrix.xx = matrix.xx;
    cairo_matrix.yx = matrix.yx;
    cairo_matrix.xy = matrix.xy;
    cairo_matrix.yy = matrix.yy;
    cairo_matrix.x0 = matrix.x0;
    cairo_matrix.y0 = matrix.y0;
    cairo_matrix_scale (&cairo_matrix, scalef, scalef);

    tmp = height;
    height = width;
    width = tmp;
  } else {
    cairo_matrix_init_scale (&cairo_matrix, scalef, scalef);
  }

  /* reallocate surface */
  overlay->text_image = g_realloc (overlay->text_image, 4 * width * height);

  surface =
      cairo_image_surface_create_for_data (overlay->text_image,
      CAIRO_FORMAT_ARGB32, width, height, width * 4);
  cr = cairo_create (surface);

  /* clear surface */
  cairo_set_operator (cr, CAIRO_OPERATOR_CLEAR);
  cairo_paint (cr);

  cairo_set_operator (cr, CAIRO_OPERATOR_OVER);

  if (overlay->want_shading)
    cairo_paint_with_alpha (cr, overlay->shading_value);

  /* apply transformations */
  cairo_set_matrix (cr, &cairo_matrix);

  /* FIXME: We use show_layout everywhere except for the surface
   * because it's really faster and internally does all kinds of
   * caching. Unfortunately we have to paint to a cairo path for
   * the outline and this is slow. Once Pango supports user fonts
   * we should use them, see
   * https://bugzilla.gnome.org/show_bug.cgi?id=598695
   *
   * Idea would the be, to create a cairo user font that
   * does shadow, outline, text painting in the
   * render_glyph function.
   */

  /* draw shadow text */
  cairo_save (cr);
  cairo_translate (cr, overlay->shadow_offset, overlay->shadow_offset);
  cairo_set_source_rgba (cr, 0.0, 0.0, 0.0, 0.5);
  pango_cairo_show_layout (cr, overlay->layout);
  cairo_restore (cr);

  /* draw outline text */
  cairo_save (cr);
  cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
  cairo_set_line_width (cr, overlay->outline_offset);
  pango_cairo_layout_path (cr, overlay->layout);
  cairo_stroke (cr);
  cairo_restore (cr);

  a = (overlay->color >> 24) & 0xff;
  r = (overlay->color >> 16) & 0xff;
  g = (overlay->color >> 8) & 0xff;
  b = (overlay->color >> 0) & 0xff;

  /* draw text */
  cairo_save (cr);
  cairo_set_source_rgba (cr, r / 255.0, g / 255.0, b / 255.0, a / 255.0);
  pango_cairo_show_layout (cr, overlay->layout);
  cairo_restore (cr);

  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  overlay->image_width = width;
  overlay->image_height = height;
  overlay->baseline_y = ink_rect.y;

  GST_DEBUG_OBJECT (overlay, "image_width %d image_height %d",
      overlay->image_width, overlay->image_height);

  return;
}



static inline void
gst_text_overlay_blit_BGRx (GstSTTextOverlay * overlay, guint8 * rgb_pixels,
    gint xpos, gint ypos)
{
  int a, r, g, b;
  int i, j;
  int h, w;
  guchar *pimage, *dest;

  w = overlay->image_width;
  h = overlay->image_height;

  if (xpos < 0) {
    xpos = 0;
  }

  if (xpos + w > overlay->width) {
    w = overlay->width - xpos;
  }

  if (ypos + h > overlay->height) {
    h = overlay->height - ypos;
  }

  for (i = 0; i < h; i++) {
    pimage = overlay->text_image + i * overlay->image_width * 4;
    dest = rgb_pixels + (i + ypos) * 4 * overlay->width + xpos * 4;
    for (j = 0; j < w; j++) {
      a = pimage[CAIRO_ARGB_A];
      b = pimage[CAIRO_ARGB_B];
      g = pimage[CAIRO_ARGB_G];
      r = pimage[CAIRO_ARGB_R];
      CAIRO_UNPREMULTIPLY (a, r, g, b);
      b = (b * a + dest[2] * (255 - a)) / 255;
      g = (g * a + dest[1] * (255 - a)) / 255;
      r = (r * a + dest[0] * (255 - a)) / 255;
      dest[0] = b;
      dest[1] = g;
      dest[2] = r;
      pimage += 4;
      dest += 4;
    }
  }
}


void
gst_sttext_overlay_blit_frame (GstSTTextOverlay * overlay, GstBuffer * buffer)
{
  gint xpos, ypos;
  gint width, height;
  GstSTTextOverlayVAlign valign;
  GstSTTextOverlayHAlign halign;

  /* blit the image into the buffer */
  width = overlay->image_width;
  height = overlay->image_height;

  if (overlay->use_vertical_render)
    halign = GST_STTEXT_OVERLAY_HALIGN_RIGHT;
  else
    halign = overlay->halign;

  switch (halign) {
    case GST_STTEXT_OVERLAY_HALIGN_LEFT:
      xpos = overlay->xpad;
      break;
    case GST_STTEXT_OVERLAY_HALIGN_CENTER:
      xpos = (overlay->width - width) / 2;
      break;
    case GST_STTEXT_OVERLAY_HALIGN_RIGHT:
      xpos = overlay->width - width - overlay->xpad;
      break;
    case GST_STTEXT_OVERLAY_HALIGN_POS:
      xpos = (gint) (overlay->width * overlay->xpos) - width / 2;
      xpos = CLAMP (xpos, 0, overlay->width - width);
      if (xpos < 0)
        xpos = 0;
      break;
    default:
      xpos = 0;
  }
  xpos += overlay->deltax;

  if (overlay->use_vertical_render)
    valign = GST_STTEXT_OVERLAY_VALIGN_TOP;
  else
    valign = overlay->valign;

  switch (valign) {
    case GST_STTEXT_OVERLAY_VALIGN_BOTTOM:
      /* as per divx-hd standard, subtitle should be displayed in 
         the lower 25% of display region 
       */
      ypos =
          overlay->height - (overlay->height) / 4 +
          height / ((overlay->number_of_text_lines) * 3);
      break;
    case GST_STTEXT_OVERLAY_VALIGN_BASELINE:
      ypos = overlay->height - (height + overlay->ypad);
      break;
    case GST_STTEXT_OVERLAY_VALIGN_TOP:
      ypos = overlay->ypad;
      break;
    case GST_STTEXT_OVERLAY_VALIGN_POS:
      ypos = (gint) (overlay->height * overlay->ypos) - height / 2;
      ypos = CLAMP (ypos, 0, overlay->height - height);
      break;
    default:
      ypos = overlay->ypad;
      break;
  }

  ypos += overlay->deltay;
  if (ypos < 0)
    ypos = 0;

  GST_DEBUG_OBJECT (overlay, "halign %d valign %d x position %d y position %d",
      halign, valign, xpos, ypos);

  /* shaded background box */
  if (overlay->want_shading)
    printf ("gst_sttext_overlay_blit_frame shading TO BE IMPLEMENTED");


  if (overlay->text_image) {
    switch (overlay->format) {
      case GST_VIDEO_FORMAT_BGRx:
      {
        GstMapInfo info_read;
        gst_buffer_map (buffer, &info_read, GST_MAP_READ);
        gst_text_overlay_blit_BGRx (overlay, info_read.data, xpos, ypos);
        gst_buffer_unmap (buffer, &info_read);
      }
        break;
      default:
        printf
            ("gst_sttext_overlay_blit_frame other format TO BE IMPLEMENTED\n");
        break;
    }
  }

  return;
}


void
gst_sttext_overlay_render_text (GstSTTextOverlay * overlay, const gchar * text,
    gint textlen)
{
  gchar *string;


  /* -1 is the whole string */
  if (text != NULL && textlen < 0) {
    textlen = strlen (text);
  }
  if (text != NULL) {
    string = g_strndup (text, textlen);
  } else {
    string = g_strdup (" ");
  }
  g_strdelimit (string, "\r\t", ' ');
  textlen = strlen (string);

  GST_DEBUG_OBJECT (overlay, "Rendering '%s'", string);
  gst_sttext_overlay_render_pangocairo (overlay, string, textlen);

  g_free (string);

  return;
}



GstFlowReturn
gst_sttext_decode_text (GstSTTextOverlay * overlay)
{
  GstFlowReturn ret = GST_FLOW_OK;
  gchar *text = NULL;
  /* specific handling to validate utf8 */
  gchar *in_text;
  gsize in_size;
  guint32 in_text_index = 0;
  guint32 text_write_index = 0;
  gboolean tag_text = FALSE;
  GstMapInfo info_read;

  gst_buffer_map (overlay->text_buffer, &info_read, GST_MAP_READ);
  in_text = (gchar *) info_read.data;
  in_size = info_read.size;
  while (in_text_index < in_size) {
    if (in_text[in_text_index] == '{') {
      tag_text = TRUE;
    }
    if (in_text[in_text_index] == '}') {
      tag_text = FALSE;
      in_text_index++;
    }
    if (tag_text == FALSE) {
      in_text[text_write_index] = in_text[in_text_index];
      text_write_index++;
    }
    in_text_index++;
  }
  in_size = text_write_index;

  /* get the number of lines in text. its required for positionaling */
  overlay->number_of_text_lines = 0;
  for (in_text_index = 0; in_text_index < in_size; in_text_index++) {
    if (in_text[in_text_index] == 0x0A) {
      overlay->number_of_text_lines++;
    }
  }
  overlay->number_of_text_lines++;

  /* g_markup_escape_text() absolutely requires valid UTF8 input, it
   * might crash otherwise. We don't fall back on GST_SUBTITLE_ENCODING
   * here on purpose, this is something that needs fixing upstream
   */
  if (!g_utf8_validate (in_text, in_size, NULL)) {
    const gchar *end = NULL;
    GST_WARNING_OBJECT (overlay, "received invalid UTF-8");
    in_text = g_strndup (in_text, in_size);
    while (!g_utf8_validate (in_text, in_size, &end) && end) {
      *((gchar *) end) = '*';
    }
  }

  /* Get the string */
  if (overlay->have_pango_markup) {
    text = g_strndup (in_text, in_size);
  } else {
    text = g_markup_escape_text (in_text, in_size);
  }

  if (text != NULL && *text != '\0') {
    gint text_len = strlen (text);
    while ((text_len > 0) && ((text[text_len - 1] == '\n')
            || (text[text_len - 1] == '\r'))) {
      --text_len;
    }
    GST_DEBUG_OBJECT (overlay, "Rendering text '%*s'", text_len, text);
    gst_sttext_overlay_render_text (overlay, text, text_len);
  }

  if (in_text != (gchar *) info_read.data) {
    g_free (in_text);
  }
  if (text != NULL) {
    g_free (text);
  }
  gst_buffer_unmap (overlay->text_buffer, &info_read);

  return ret;
}


void
gst_sttext_overlay_update_wrap_mode (GstSTTextOverlay * overlay)
{
  if (overlay->wrap_mode == GST_STTEXT_OVERLAY_WRAP_MODE_NONE) {
    GST_DEBUG_OBJECT (overlay, "Set wrap mode NONE");
    pango_layout_set_width (overlay->layout, -1);
  } else {
    int width;

    if (overlay->auto_adjust_size) {
      width = DEFAULT_SCALE_BASIS * PANGO_SCALE;
      if (overlay->use_vertical_render) {
        width = width * (overlay->height - overlay->ypad * 2) / overlay->width;
      }
    } else {
      width =
          (overlay->use_vertical_render ? overlay->height : overlay->width) *
          PANGO_SCALE;
    }

    GST_DEBUG_OBJECT (overlay, "Set layout width %d", overlay->width);
    GST_DEBUG_OBJECT (overlay, "Set wrap mode    %d", overlay->wrap_mode);
    pango_layout_set_width (overlay->layout, width);
    pango_layout_set_wrap (overlay->layout, (PangoWrapMode) overlay->wrap_mode);
  }

  return;
}


void
gst_sttext_overlay_adjust_values_with_fontdesc (GstSTTextOverlay * overlay,
    PangoFontDescription * desc)
{
  gint font_size = pango_font_description_get_size (desc) / PANGO_SCALE;

  overlay->shadow_offset = (double) (font_size) / 13.0;
  overlay->outline_offset = (double) (font_size) / 15.0;

  if (overlay->outline_offset < MINIMUM_OUTLINE_OFFSET)
    overlay->outline_offset = MINIMUM_OUTLINE_OFFSET;

  return;
}


void
gst_sttext_overlay_update_render_mode (GstSTTextOverlay * overlay)
{
  PangoMatrix matrix = PANGO_MATRIX_INIT;
  PangoContext *context = pango_layout_get_context (overlay->layout);

  if (overlay->use_vertical_render) {
    pango_matrix_rotate (&matrix, -90);
    pango_context_set_base_gravity (context, PANGO_GRAVITY_AUTO);
    pango_context_set_matrix (context, &matrix);
    pango_layout_set_alignment (overlay->layout, PANGO_ALIGN_LEFT);
  } else {
    pango_context_set_base_gravity (context, PANGO_GRAVITY_SOUTH);
    pango_context_set_matrix (context, &matrix);
    pango_layout_set_alignment (overlay->layout, overlay->line_align);
  }

  return;
}


void
gst_stm_overlay_init (GstSTTextOverlay * overlay)
{

  PangoFontDescription *desc;
  overlay->layout =
      pango_layout_new (GST_STTEXT_OVERLAY_GET_CLASS (overlay)->pango_context);
  desc =
      pango_context_get_font_description (GST_STTEXT_OVERLAY_GET_CLASS
      (overlay)->pango_context);
  gst_sttext_overlay_adjust_values_with_fontdesc (overlay, desc);
  overlay->need_render = TRUE;
  overlay->text_image = NULL;
  gst_sttext_overlay_update_render_mode (overlay);
  overlay->text_buffer = NULL;
  overlay->text_flushing = FALSE;
  overlay->text_eos = FALSE;
  overlay->blanking_done = TRUE;
  overlay->number_of_text_lines = 0;
  gst_segment_init (&overlay->text_segment, GST_FORMAT_TIME);
  overlay->new_text = FALSE;

  return;
}


void
gst_stm_overlay_deinit (GstSTTextOverlay * overlay)
{
  if (overlay->text_image) {
    g_free (overlay->text_image);
    overlay->text_image = NULL;
  }

  if (overlay->layout) {
    g_object_unref (overlay->layout);
    overlay->layout = NULL;
  }

  if (overlay->text_buffer) {
    gst_buffer_unref (overlay->text_buffer);
    overlay->text_buffer = NULL;
  }
}
