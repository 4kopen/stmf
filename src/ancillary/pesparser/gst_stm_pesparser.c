/* This file is part of Gstreamer sttextovelay
 *
 * Copyright (C) 2011 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "gststtextoverlay.h"

/*
   Function    : gst_stm_subtitle_es_cb
   Input
                   : filter - pes filter pointer
                   : first - now unused
                   : buffer - gst buffer pointer carrying es payload for decoding
                   : user_data - callback data, pointer to GstSTTextOverlay
   Description     : dvb subtitle/teletext decode callback
   Return          : GstFlowReturn
 */
static GstFlowReturn
gst_stm_subtitle_es_cb (GstPESFilter * filter, gboolean first,
    GstBuffer * buffer, gpointer user_data)
{
  GstFlowReturn ret = GST_FLOW_OK;
  GstSTTextOverlay *overlay = (GstSTTextOverlay *) user_data;
  if (filter->pts != -1) {
    GST_BUFFER_TIMESTAMP (buffer) = MPEGTIME_TO_GSTTIME (filter->pts);
  } else {
    /* set the pts to buffer time */
    GST_BUFFER_TIMESTAMP (buffer) = GST_CLOCK_TIME_NONE;
  }
  GST_BUFFER_DURATION (buffer) = GST_CLOCK_TIME_NONE;

  /* DVB subtitle packets are required to carry the PTS */
  if (G_UNLIKELY (!GST_BUFFER_TIMESTAMP_IS_VALID (buffer))) {
    gst_buffer_unref (buffer);
    return GST_FLOW_OK;
  }


  if (overlay->type == GST_STTEXT_OVERLAY_DVB_SUBTITLE) {
    GstClockTime sub_running_time;
    /* Process the ES buffer for subtitle */
    overlay->current_text_render_time = GST_BUFFER_TIMESTAMP (buffer);
    overlay->subtitle_segment.position = GST_BUFFER_TIMESTAMP (buffer);

    sub_running_time =
        gst_segment_to_running_time (&overlay->subtitle_segment,
        GST_FORMAT_TIME, GST_BUFFER_TIMESTAMP (buffer));
    GST_DEBUG_OBJECT (overlay, "SUBTITLE real running time: %" GST_TIME_FORMAT,
        GST_TIME_ARGS (sub_running_time));
    gst_dvbsub_overlay_process_text (overlay, buffer, sub_running_time);
  } else {
    /* Process the ES buffer for teletext */
    overlay->current_text_render_time = GST_BUFFER_TIMESTAMP (buffer);
    gst_teletextdec_process_telx_buffer (&overlay->teletext_dec, buffer);
    gst_stm_dvb_get_teletext (overlay, overlay->current_text_render_time);
    gst_buffer_unref (buffer);
  }
  return ret;
}

/*
  Function   : gst_stm_pes_parser_init
  Input        : none
  Description     : This function implements pes filter initalization and setup decode callback
  Return          : None.
 */
void
gst_stm_pes_parser_init (GstSTTextOverlay * overlay)
{
  gst_pes_filter_init (&overlay->pesfilter, NULL, NULL);
  gst_pes_filter_set_callbacks (&overlay->pesfilter,
      (GstPESFilterData) gst_stm_subtitle_es_cb, NULL, overlay);
  return;

}

/*
   Function   : gst_stm_pes_parser
   Input           : none
   Description     : This function calls gst_pes_filter_push which actually pushes pes packets
                      into circular buffer for processing
   Return          : none
 */
void
gst_stm_pes_parser (GstSTTextOverlay * overlay, GstBuffer * Buffer)
{
  gst_pes_filter_push (&overlay->pesfilter, Buffer);
}

/*
   Function : gst_stm_pes_parser_deinit
   Input           : none
   Description     : This function flushes pes filter and frees up all resources
   Return          : none
*/
void
gst_stm_pes_parser_deinit (GstSTTextOverlay * overlay)
{
  gst_pes_filter_drain (&overlay->pesfilter);
  gst_pes_filter_uninit (&overlay->pesfilter);
  return;

}
