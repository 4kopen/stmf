/* GStreamer
 *
 * Copyright (C) 2001-2002 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *               2006 Edgard Lima <edgard.lima@indt.org.br>
 * Copyright (C) 2011 STMicroelectronics
 *
 * gstv4l2object.c: base class for V4L2 elements
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 * USA.
 *
 * This file has been modified by STMicroelectronics, on 03/19/2012
 */

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <linux/fb.h>
#include <linux/videodev2.h>
#include <linux/stmvout.h>
#include "gststdisplayobject.h"
#include "v4l2_utils.h"
#include <linux/dvb/dvb_v4l2_export.h>

GST_DEBUG_CATEGORY_EXTERN (stdisplaysink_debug);
#define GST_CAT_DEFAULT stdisplaysink_debug

#define V4L2_PIX_FMT_STMFAKE    v4l2_fourcc('F','A','K','E')    /* STMicroelectronics 420 Macro Block */
#define GST_V4L2_FORMAT_COUNT   (G_N_ELEMENTS (gst_st_v4l2_formats))

/*
 * Common format / caps utilities:
 */
typedef struct
{
  guint32 format;
  gboolean dimensions;
} GstV4L2FormatDesc;


static const GstV4L2FormatDesc gst_st_v4l2_formats[] = {
  {V4L2_PIX_FMT_STMFAKE, FALSE},
  {V4L2_PIX_FMT_RGB32, TRUE},
  {V4L2_PIX_FMT_BGR32, TRUE}
};

gboolean
gst_st_v4l2_set_aspect_ratio (GstSTV4l2Object * v4l2object, gint DAR_n,
    gint DAR_d)
{
  struct v4l2_crop crop;
  gdouble plane_aspect_ratio, image_aspect_ratio;

  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

/* Set the output size and position if downscale requested */
  memset (&crop, 0, sizeof (struct v4l2_crop));
  crop.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

  plane_aspect_ratio = ((gdouble) v4l2object->plane.width) /
      ((gdouble) v4l2object->plane.height);

  image_aspect_ratio = ((gdouble) DAR_n) / ((gdouble) DAR_d);

  if (image_aspect_ratio > plane_aspect_ratio) {
    crop.c.width = v4l2object->plane.width;
    crop.c.height = (v4l2object->plane.width * DAR_d) / DAR_n;
    crop.c.left = 0;
    crop.c.top = (v4l2object->plane.height - crop.c.height) >> 1;
  } else if (image_aspect_ratio < plane_aspect_ratio) {
    crop.c.width = (v4l2object->plane.height * DAR_n) / DAR_d;
    crop.c.height = v4l2object->plane.height;
    crop.c.left = (v4l2object->plane.width - crop.c.width) >> 1;
    crop.c.top = 0;
  } else {
    crop.c.left = 0;
    crop.c.top = 0;
    crop.c.width = v4l2object->plane.width;
    crop.c.height = v4l2object->plane.height;
  }

  /* Set output crop */
  GST_DEBUG_OBJECT (v4l2object->element,
      "video_fd_output %d, left %d, top %d, width %d, height %d",
      v4l2object->video_fd_output, crop.c.left, crop.c.top, crop.c.width,
      crop.c.height);
  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_S_CROP, &crop) < 0) {
    GST_ERROR ("VIDIOC_S_CROP failed - %s", strerror (errno));
    return FALSE;
  }

  GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);

  return TRUE;

}

gboolean
gst_st_v4l2_set_display_plane (GstSTV4l2Object * v4l2object)
{
  if ((!v4l2object->display_plane_open)
      && (v4l2object->display_plane_index != -1)) {

    if (v4l2object->output_video_plane_open) {
      if (!gst_st_v4l2_close_device (v4l2object)) {
        GST_ERROR_OBJECT (v4l2object->element, "Failed to close device");
      }
      if (!gst_st_v4l2_open_device (v4l2object)) {
        GST_ERROR_OBJECT (v4l2object->element, "Failed to open device");
      }
    }

    GST_DEBUG_OBJECT (v4l2object->element,
        "video_fd_output %d, display_plane_index %d",
        v4l2object->video_fd_output, v4l2object->display_plane_index);
    if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_S_OUTPUT,
            &(v4l2object->display_plane_index)) >= 0) {
      v4l2object->display_plane_open = TRUE;
      return TRUE;
    }
    GST_ERROR_OBJECT (v4l2object->element,
        "VIDIOC_S_OUTPUT failed (index=%d) - %s",
        v4l2object->display_plane_index, strerror (errno));
    return FALSE;
  }

  GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);

  return TRUE;
}

void
gst_st_v4l2_set_plane_order (GstSTV4l2Object * v4l2object)
{
  struct v4l2_control cntrl;

  cntrl.id = V4L2_CID_STM_Z_ORDER;

  if (v4l2object->is_upstream_ancillary) {
    /* For PIP set graphics plane order Main -> PIP -> GDP */
    GST_DEBUG_OBJECT (v4l2object->element, "set plane order video_fd_output %d",
        v4l2object->video_fd_output);
    if (v4l2object->dev_id == 1) {
      cntrl.value = 4;
      if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_S_CTRL, &cntrl) < 0) {
        GST_WARNING_OBJECT (v4l2object->element,
            "VIDIOC_S_CTRL failed: could not set GDP order %d for PIP - %s",
            cntrl.value, strerror (errno));
      }
    } else {
      /* For Main set graphics plane order Main -> GDP -> PIP */
      cntrl.value = 2;
      if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_S_CTRL, &cntrl) < 0) {
        GST_WARNING_OBJECT (v4l2object->element,
            "VIDIOC_S_CTRL failed: could not set GDP order %d for Main - %s",
            cntrl.value, strerror (errno));
      }
    }
  }
}

static gboolean
gst_st_v4l2_current_display_plane_free (GstSTV4l2Object * v4l2object)
{
  enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  struct v4l2_requestbuffers breq;
  struct v4l2_cropcap cropcap;

  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  if (gst_st_v4l2_set_display_plane (v4l2object)) {
    /* Set the crop */
    memset (&cropcap, 0, sizeof (cropcap));
    cropcap.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

    if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_CROPCAP, &cropcap) < 0) {
      GST_DEBUG_OBJECT (v4l2object->element, "VIDIOC_CROPCAP failed - %s",
          strerror (errno));
      return FALSE;
    }

    v4l2object->plane.width = cropcap.bounds.width;
    v4l2object->plane.height = cropcap.bounds.height;

    GST_DEBUG_OBJECT (v4l2object->element, "Plane width=%u, height=%u",
        v4l2object->plane.width, v4l2object->plane.height);

    if (!gst_st_v4l2_object_set_format (v4l2object)) {
      /* Error already posted */
      return FALSE;
    }

    /* Set output aspect ratio */
    if (!gst_st_v4l2_set_aspect_ratio (v4l2object, v4l2object->DAR_n,
            v4l2object->DAR_d)) {
      /* Error already posted */
      return FALSE;
    }

    /* Request one buffers from V4l2 */
    memset (&breq, 0, sizeof (struct v4l2_requestbuffers));
    breq.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    breq.count = 1;
    breq.memory = V4L2_MEMORY_MMAP;

    if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_REQBUFS, &breq) < 0) {
      GST_ERROR_OBJECT (v4l2object->element, "VIDIOC_REQBUFS failed - %s",
          strerror (errno));
      return FALSE;
    }

    memset (&v4l2object->first_buffer, 0, sizeof (struct v4l2_buffer));
    v4l2object->first_buffer.index = 0;
    v4l2object->first_buffer.type = type;

    /* Query Buffers and map buffers */
    if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_QUERYBUF,
            &v4l2object->first_buffer) < 0) {
      GST_ERROR_OBJECT (v4l2object->element, "VIDIOC_QUERYBUF failed - %s",
          strerror (errno));
      return FALSE;
    }

    v4l2object->first_buffer.field = V4L2_FIELD_NONE;

    v4l2object->first_bufferdata =
        (guchar *) mmap (0, v4l2object->first_buffer.length,
        PROT_READ | PROT_WRITE, MAP_SHARED, v4l2object->video_fd_output,
        v4l2object->first_buffer.m.offset);

    if (v4l2object->first_bufferdata == ((void *) -1)) {
      GST_ERROR_OBJECT (v4l2object->element, "mmap failed - %s\n",
          strerror (errno));
      return FALSE;
    }

    memset (v4l2object->first_bufferdata, 0, v4l2object->image_size);   /* image_size is set by gst_st_v4l2_object_set_format */

    /* Queue buffer */
    if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_QBUF,
            &v4l2object->first_buffer) < 0) {
      GST_ERROR_OBJECT (v4l2object->element, "VIDIOC_QBUF failed - %s",
          strerror (errno));
      return FALSE;
    }
    v4l2object->queued_buffer_index = 0;

    if (!gst_st_v4l2_object_start_streaming (v4l2object)) {
      GST_DEBUG_OBJECT (v4l2object->element, "VIDIOC_STREAMON failed - %s",
          strerror (errno));
      munmap (v4l2object->first_bufferdata, v4l2object->first_buffer.length);
      v4l2object->first_bufferdata = NULL;
      return FALSE;
    }

    v4l2object->state = STATE_STREAMING;

    /* set the plane order */
    gst_st_v4l2_set_plane_order (v4l2object);
    return TRUE;
  } else {
    GST_ERROR_OBJECT (v4l2object->element, "Failed to set output plane");
    return FALSE;
  }
}

static gboolean
gst_st_v4l2_display_plane_available (GstSTV4l2Object * v4l2object,
    gchar * plane_name)
{
  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  if (!gst_st_v4l2_current_display_plane_free (v4l2object)) {
    /* Close the device and reopen it to undo previous S_OUTPUT */
    if (!gst_st_v4l2_close_device (v4l2object)) {
      GST_ERROR_OBJECT (v4l2object->element, "Failed to close device");
    }
    if (!gst_st_v4l2_open_device (v4l2object)) {
      GST_ERROR_OBJECT (v4l2object->element, "Failed to open device");
    }
    return FALSE;
  } else {
    if (strlen (plane_name) > sizeof (v4l2object->display_plane_name)) {
      GST_ERROR_OBJECT (v4l2object->element, "Incorrect plane name");
      return FALSE;
    }
    g_strlcpy (v4l2object->display_plane_name, plane_name, PLANE_NAME_LENGTH);
    GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);
    return TRUE;
  }
}

static gboolean
gst_st_v4l2_get_free_display_plane (GstSTV4l2Object * v4l2object)
{
  gchar *plane_name = v4l2object->display_plane_name;
  struct v4l2_output output;

  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  output.index = 0;
  if (strcmp (plane_name, DEFAULT_PLANE_NAME) != 0) {
    /* Find if the user provided plane exists */
    while (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_ENUMOUTPUT,
            &output) == 0) {
      if (!strcmp ((gchar *) output.name, plane_name)) {
        GST_DEBUG_OBJECT (v4l2object->element,
            "Testing if %s display plane is available (index=%d)",
            (gchar *) output.name, output.index);
        v4l2object->display_plane_index = output.index;
        if (gst_st_v4l2_display_plane_available (v4l2object,
                (gchar *) output.name)) {
          GST_DEBUG_OBJECT (v4l2object->element,
              "%s display plane is available, index=%d", (gchar *) output.name,
              output.index);
          return TRUE;
        }
      }
      output.index++;
    }
  } else {
    /* User specified plane not found */
    while (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_ENUMOUTPUT,
            &output) == 0) {
      if (strstr ((gchar *) output.name, v4l2object->plane_finder)) {
        GST_DEBUG_OBJECT (v4l2object->element,
            "Testing if %s display plane is available (index=%d)",
            (gchar *) output.name, output.index);
        v4l2object->display_plane_index = output.index;
        if (gst_st_v4l2_display_plane_available (v4l2object,
                (gchar *) output.name)) {
          GST_DEBUG_OBJECT (v4l2object->element,
              "%s display plane is available, index=%d", (gchar *) output.name,
              output.index);
          return TRUE;
        }
      }
      output.index++;
    }
  }
  GST_ERROR_OBJECT (v4l2object->element, "ERROR!! %s plane is not available",
      plane_name);
  return FALSE;
}

gboolean
gst_st_v4l2_open_device (GstSTV4l2Object * v4l2object)
{

  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  /* Open the device for overlay */
  if (v4l2object->video_fd_overlay < 0) {

    v4l2object->video_fd_overlay =
        v4l2_open_by_name (V4L2_OVERLAY_DRIVER_NAME, V4L2_OVERLAY_CARD_NAME,
        O_RDWR);
    GST_DEBUG_OBJECT (v4l2object->element,
        "opened device %s, video_fd_overlay %d", V4L2_OVERLAY_DRIVER_NAME,
        v4l2object->video_fd_overlay);
  }

  /* Open the device for output */
  if (v4l2object->video_fd_output < 0) {
    v4l2object->video_fd_output =
        v4l2_open_by_name (V4L2_OUTPUT_DRIVER_NAME, V4L2_OUTPUT_CARD_NAME,
        O_RDWR);
    GST_DEBUG_OBJECT (v4l2object->element,
        "opened device %s, video_fd_output %d", V4L2_OUTPUT_DRIVER_NAME,
        v4l2object->video_fd_output);
  }

  if ((v4l2object->video_fd_overlay < 0) || (v4l2object->video_fd_output < 0)) {
    if (v4l2object->video_fd_overlay > 0) {
      v4l2_close (v4l2object->video_fd_overlay);
      v4l2object->video_fd_overlay = -1;
      GST_ERROR_OBJECT (v4l2object->element, "Failed to open %s device - %s",
          V4L2_OVERLAY_DRIVER_NAME, strerror (errno));
    }
    if (v4l2object->video_fd_output > 0) {
      v4l2_close (v4l2object->video_fd_output);
      v4l2object->video_fd_output = -1;
      GST_ERROR_OBJECT (v4l2object->element, "Failed to open %s device - %s",
          V4L2_OUTPUT_DRIVER_NAME, strerror (errno));
    }
    return FALSE;
  }

  GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);
  return TRUE;
}

/******************************************************
 * gst_st_v4l2_close_device():
 *   close the video device (v4l2object->video_fd_overlay and v4l2object->video_fd_output)
 * return value: TRUE on success, FALSE on error
 ******************************************************/
gboolean
gst_st_v4l2_close_device (GstSTV4l2Object * v4l2object)
{
  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  /* close video device */
  if (v4l2object->video_fd_output != -1) {
    GST_DEBUG_OBJECT (v4l2object->element, "v4l2 close output device %d ",
        v4l2object->video_fd_output);
    if (v4l2_close (v4l2object->video_fd_output) != 0) {
      GST_ERROR_OBJECT (v4l2object->element,
          "Failed to close V4L2 output device");
      return FALSE;
    }
    v4l2object->video_fd_output = -1;
  }
  if (v4l2object->video_fd_overlay != -1) {
    GST_DEBUG_OBJECT (v4l2object->element, "v4l2 close overlay device %d ",
        v4l2object->video_fd_overlay);
    if (v4l2_close (v4l2object->video_fd_overlay) != 0) {
      GST_ERROR_OBJECT (v4l2object->element,
          "Failed to close V4L2 overlay device");
      return FALSE;
    }
    v4l2object->video_fd_overlay = -1;
  }
  v4l2object->display_plane_open = FALSE;
  v4l2object->input_video_plane_open = FALSE;
  v4l2object->output_video_plane_open = FALSE;

  GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);

  return TRUE;
}

/*
 function: gst_st_v4l2_object_reset
 input params:
           v4l2object - v4l2object context
           type - v4l2 buffer type
 description : will reset stdisplayobject parameters.
 return : void.
 */
void
gst_st_v4l2_object_reset (GstSTV4l2Object * v4l2object, enum v4l2_buf_type type)
{
  memset (v4l2object, 0, sizeof (GstSTV4l2Object));
  /* Default values */
  v4l2object->video_fd_overlay = -1;
  v4l2object->video_fd_output = -1;
  v4l2object->width = 0;
  v4l2object->height = 0;
  v4l2object->pixelformat = 0;
  v4l2object->type = type;
  v4l2object->formats = NULL;
  v4l2object->stream_plane_info_set = FALSE;
  v4l2object->display_plane_open = FALSE;
  v4l2object->input_video_plane_open = FALSE;
  v4l2object->output_video_plane_open = FALSE;
  v4l2object->stream.x_pos = 0;
  v4l2object->stream.y_pos = 0;
  v4l2object->stream.width = DEFAULT_STREAM_WIDTH;
  v4l2object->stream.height = DEFAULT_STREAM_HEIGHT;
  v4l2object->plane.x_pos = 0;
  v4l2object->plane.y_pos = 0;
  v4l2object->plane.width = DEFAULT_ALLOC_WIDTH;
  v4l2object->plane.height = DEFAULT_ALLOC_HEIGHT;
  v4l2object->display_plane_index = -1;
  v4l2object->state = STATE_OFF;
  v4l2object->first_bufferdata = NULL;
  v4l2object->queued_buffer_index = 0;
  v4l2object->DAR_n = 0;
  v4l2object->DAR_d = 0;
  v4l2object->input_plane_mode = VCSPLANE_AUTO_MODE;
  v4l2object->output_plane_mode = VCSPLANE_AUTO_MODE;
  g_strlcpy (v4l2object->display_plane_name, DEFAULT_PLANE_NAME,
      PLANE_NAME_LENGTH);
  v4l2object->is_upstream_ancillary = FALSE;
  v4l2object->PAR_n = 1;
  v4l2object->PAR_d = 1;
}

GstSTV4l2Object *
gst_st_v4l2_object_new (GstElement * element, enum v4l2_buf_type type)
{
  GstSTV4l2Object *v4l2object;

  v4l2object = g_new0 (GstSTV4l2Object, 1);

  if (v4l2object != NULL) {
    gst_st_v4l2_object_reset (v4l2object, type);
    v4l2object->element = element;
  }

  return v4l2object;
}

static gboolean
gst_st_v4l2_object_clear_format_list (GstSTV4l2Object * v4l2object)
{
  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  g_slist_foreach (v4l2object->formats, (GFunc) g_free, NULL);
  g_slist_free (v4l2object->formats);
  v4l2object->formats = NULL;

  GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);

  return TRUE;
}

void
gst_st_v4l2_object_destroy (GstSTV4l2Object * v4l2object)
{
  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  g_return_if_fail (v4l2object != NULL);

  if (v4l2object->formats) {
    if (!gst_st_v4l2_object_clear_format_list (v4l2object)) {
      GST_WARNING_OBJECT (v4l2object->element, "could not clear format list");
    }
  }

  GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);

  g_free (v4l2object);
}

gboolean
gst_st_v4l2_object_start (GstSTV4l2Object * v4l2object)
{
  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  if (!gst_st_v4l2_open_device (v4l2object)) {
    GST_ERROR_OBJECT (v4l2object->element, "Failed to open device");
    return FALSE;
  }

  if (!gst_st_v4l2_get_free_display_plane (v4l2object)) {
    if (!gst_st_v4l2_close_device (v4l2object)) {
      GST_ERROR_OBJECT (v4l2object->element, "failed to close device");
    }
    g_strlcpy (v4l2object->display_plane_name, DEFAULT_PLANE_NAME,
        PLANE_NAME_LENGTH);
    GST_INFO_OBJECT (v4l2object->element, "Failed to start v4l2 object");
    return FALSE;
  }

  GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);

  return TRUE;
}

gboolean
gst_st_v4l2_object_stop (GstSTV4l2Object * v4l2object)
{
  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  if (!gst_st_v4l2_close_device (v4l2object)) {
    GST_ERROR_OBJECT (v4l2object->element, "Failed to close device");
    return FALSE;
  }

  if (v4l2object->formats) {
    if (gst_st_v4l2_object_clear_format_list (v4l2object)) {
      GST_WARNING_OBJECT (v4l2object->element, "could not clear format list");
    }
  }

  GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);

  return TRUE;
}

GstStructure *
gst_st_v4l2_object_v4l2fourcc_to_structure (guint32 fourcc)
{
  GstStructure *structure = NULL;

  /* REVIEW/FIXME: Use GstVideoInfo to avoid wrong caps */
  switch (fourcc) {
    case V4L2_PIX_FMT_STMFAKE: /* Fake MIME type   */
      structure = gst_structure_new_empty ("video/x-fake-yuv");
      break;
    case V4L2_PIX_FMT_NV12:
    {
      structure = gst_structure_new ("video/x-raw",
          "format", G_TYPE_STRING, "NV12", NULL);
      break;
    }
    case V4L2_PIX_FMT_NV16:
    {
      structure = gst_structure_new ("video/x-raw",
          "format", G_TYPE_STRING, "NV16", NULL);
      break;
    }
    case V4L2_PIX_FMT_NV24:
    {
      structure = gst_structure_new ("video/x-raw",
          "format", G_TYPE_STRING, "NV24", NULL);
      break;
    }
    case V4L2_PIX_FMT_BGR32:
    {
      structure = gst_structure_new ("video/x-raw",
          "format", G_TYPE_STRING, "BGRA", NULL);
      break;
    }
    case V4L2_PIX_FMT_RGB32:
    {
      structure = gst_structure_new ("video/x-raw",
          "format", G_TYPE_STRING, "ARGB", NULL);
      break;
    }
    default:
      GST_DEBUG ("Unknown fourcc 0x%08x %" GST_FOURCC_FORMAT,
          fourcc, GST_FOURCC_ARGS (fourcc));
      break;
  }

  return structure;
}


GstCaps *
gst_st_v4l2_object_get_selective_caps (GstSTV4l2Object * v4l2object)
{
  static GstCaps *caps = NULL;

  if (caps == NULL) {
    GstStructure *structure;
    guint i;

    caps = gst_caps_new_empty ();
    for (i = 0; i < GST_V4L2_FORMAT_COUNT; i++) {
      if ((gst_st_v4l2_formats[i].format != V4L2_PIX_FMT_NV12)
          && (gst_st_v4l2_formats[i].format != V4L2_PIX_FMT_NV16)
          && (gst_st_v4l2_formats[i].format != V4L2_PIX_FMT_NV24)) {
        structure =
            gst_st_v4l2_object_v4l2fourcc_to_structure (gst_st_v4l2_formats
            [i].format);
        if (structure) {
          if (gst_st_v4l2_formats[i].dimensions) {
            gst_structure_set (structure,
                "width", GST_TYPE_INT_RANGE, 1, v4l2object->plane.width,
                "height", GST_TYPE_INT_RANGE, 1, v4l2object->plane.height,
                "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, 100, 1, NULL);
          }
          gst_caps_append_structure (caps, structure);
        }
      }
    }
  }
  return caps;
}

GstCaps *
gst_st_v4l2_object_get_all_caps (GstSTV4l2Object * v4l2object)
{
  static GstCaps *caps = NULL;
  guint width = DEFAULT_ALLOC_WIDTH;
  guint height = DEFAULT_ALLOC_HEIGHT;

  if (v4l2object) {
    width = v4l2object->plane.width;
    height = v4l2object->plane.height;
  }

  if (caps == NULL) {
    GstStructure *structure;
    guint i;

    caps = gst_caps_new_empty ();
    for (i = 0; i < GST_V4L2_FORMAT_COUNT; i++) {
      structure =
          gst_st_v4l2_object_v4l2fourcc_to_structure (gst_st_v4l2_formats
          [i].format);
      if (structure) {
        if (gst_st_v4l2_formats[i].dimensions) {
          gst_structure_set (structure,
              "width", GST_TYPE_INT_RANGE, 1, width,
              "height", GST_TYPE_INT_RANGE, 1, height,
              "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, 100, 1, NULL);
        }
        gst_caps_append_structure (caps, structure);
      }
    }
  }
  return caps;
}

/* collect data for the given caps
 * @caps: given input caps
 * @format: location for the v4l format
 * @w/@h: location for width and height
 * @fps_n/@fps_d: location for framerate
 * @size: location for expected size of the frame or 0 if unknown
 */
gboolean
gst_st_v4l2_object_get_caps_info (GstSTV4l2Object * v4l2object, GstCaps * caps,
    guint32 * format, gint * w, gint * h, guint * fps_n,
    guint * fps_d, gboolean * fake_mimetype, gboolean * is_ancillary,
    gboolean * is_upstream_ancillary, gboolean * release_plane)
{
  GstStructure *structure;
  const GValue *framerate;
  const gchar *mimetype;
  const gchar *inputformat = NULL;
  guint32 fourcc = 0;
  gint PAR_n = 0, PAR_d = 0;

  /* REVIEW/FIXME: Use GstVideoInfo to avoid wrong caps */
  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  structure = gst_caps_get_structure (caps, 0);
  mimetype = gst_structure_get_name (structure);

  if (!strcmp (mimetype, "video/x-fake-yuv")) {
    *fake_mimetype = TRUE;
    GST_INFO_OBJECT (v4l2object->element, "fake mime type received");
    return TRUE;
  }

  if (!gst_structure_get_int (structure, "width", w)) {
    GST_ERROR_OBJECT (v4l2object->element,
        "could not get width from caps structure");
    return FALSE;
  }

  if (!gst_structure_get_int (structure, "height", h)) {
    GST_ERROR_OBJECT (v4l2object->element,
        "could not get height from caps structure");
    return FALSE;
  }

  framerate = gst_structure_get_value (structure, "framerate");
  if (!framerate) {
    GST_ERROR_OBJECT (v4l2object->element,
        "could not get framerate from caps structure");
    return FALSE;
  }

  inputformat = gst_structure_get_string (structure, "format");
  if (inputformat == NULL) {
    GST_ERROR_OBJECT (v4l2object->element,
        "could not get format from caps structure");
    return FALSE;
  }

  if (gst_structure_get_boolean (structure, "is_ancillary",
          is_ancillary) != TRUE) {
    GST_DEBUG_OBJECT (v4l2object->element,
        "Failed to get [is_ancillary] field from structure");
    *is_ancillary = FALSE;
  }

  if (gst_structure_get_boolean (structure, "is_ancillary_silent",
          release_plane) != TRUE) {
    GST_DEBUG_OBJECT (v4l2object->element,
        "Failed to get [is_ancillary_silent] field from structure");
    *release_plane = FALSE;
  }

  if (gst_structure_get_boolean (structure, "is_upstream_ancillary",
          is_upstream_ancillary) != TRUE) {
    GST_DEBUG_OBJECT (v4l2object->element,
        "Failed to get [is_upstream_ancillary] field from structure");
    *is_upstream_ancillary = FALSE;
  }
  v4l2object->is_upstream_ancillary = *is_upstream_ancillary;
  if (gst_structure_get_fraction (structure, "pixel-aspect-ratio", &PAR_n,
          &PAR_d)) {
    if (*is_upstream_ancillary == FALSE) {
      if ((PAR_n != 0) && (PAR_d != 0) && (*w != 0) && (*h != 0)) {
        v4l2object->DAR_n = (PAR_n * (*w));
        v4l2object->DAR_d = (PAR_d * (*h));
      } else {
        v4l2object->DAR_n = *w;
        v4l2object->DAR_d = *h;
      }
    } else if ((*w != 0) && (*h != 0) && (*is_upstream_ancillary == FALSE)) {
      v4l2object->DAR_n = *w;
      v4l2object->DAR_d = *h;
    } else {
      v4l2object->DAR_n = v4l2object->plane.width;
      v4l2object->DAR_d = v4l2object->plane.height;
    }
  } else if ((*w != 0) && (*h != 0) && (*is_upstream_ancillary == FALSE)) {
    v4l2object->DAR_n = *w;
    v4l2object->DAR_d = *h;
  } else {
    v4l2object->DAR_n = 0;
    v4l2object->DAR_d = 0;
  }

  *fps_n = gst_value_get_fraction_numerator (framerate);
  *fps_d = gst_value_get_fraction_denominator (framerate);

  GST_DEBUG_OBJECT (v4l2object->element, "format=%s, %s", inputformat,
      gst_caps_to_string (caps));

  if (!strcmp (inputformat, "RGBA") || !strcmp (inputformat, "ARGB")) {
    fourcc = V4L2_PIX_FMT_RGB32;
  } else if (!strcmp (inputformat, "BGRA")) {
    fourcc = V4L2_PIX_FMT_BGR32;
  } else if (!strcmp (inputformat, "NV12")) {
    fourcc = V4L2_PIX_FMT_NV12;
  } else if (!strcmp (inputformat, "NV16")) {
    fourcc = V4L2_PIX_FMT_NV16;
  } else if (!strcmp (inputformat, "NV24")) {
    fourcc = V4L2_PIX_FMT_NV24;
  } else {
    GST_ERROR_OBJECT (v4l2object->element,
        "format %s is not supported by displaysink", inputformat);
    return FALSE;
  }
  *format = fourcc;

  GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);

  return TRUE;
}

gboolean
gst_st_v4l2_object_set_format (GstSTV4l2Object * v4l2object)
{
  gint fd = v4l2object->video_fd_output;
  struct v4l2_format format;

  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  if (v4l2object->pixelformat == V4L2_PIX_FMT_NV12) {
    /* NV12 format is 12 bits per pixel */
    v4l2object->bytes_per_line = v4l2object->width * 1;
  } else if (v4l2object->pixelformat == V4L2_PIX_FMT_NV16) {
    v4l2object->bytes_per_line = v4l2object->width * 1;
  } else if (v4l2object->pixelformat == V4L2_PIX_FMT_NV24) {
    v4l2object->bytes_per_line = v4l2object->width * 2;
  } else {
    /* Other are RGB32, therefore 32 bits per pixel */
    v4l2object->bytes_per_line = v4l2object->width * 4;
  }

  memset (&format, 0, sizeof (struct v4l2_format));
  format.type = v4l2object->type;
  format.fmt.pix.width = v4l2object->width;
  format.fmt.pix.height = v4l2object->height;
  format.fmt.pix.bytesperline = v4l2object->bytes_per_line;

  /* TODO :SP The piel format sent by the setcaps should be set for the display ideally not working fine for 7108C2 */
  format.fmt.pix.pixelformat = v4l2object->pixelformat;
  format.fmt.pix.colorspace = V4L2_COLORSPACE_SMPTE170M;
  format.fmt.pix.field = V4L2_FIELD_ANY;

  if (v4l2_ioctl (fd, VIDIOC_S_FMT, &format) < 0) {
    goto set_fmt_failed;
  } else {
    /* Check if driver has changed input parameters based on hardware abilities */
    if (format.fmt.pix.width != v4l2object->width) {
      GST_WARNING_OBJECT (v4l2object->element,
          "VIDIOC_S_FMT: width has been changed, old value=%d, new value=%d!!\n",
          v4l2object->width, format.fmt.pix.width);
      v4l2object->width = format.fmt.pix.width;
    }
    if (format.fmt.pix.height != v4l2object->height) {
      GST_WARNING_OBJECT (v4l2object->element,
          "VIDIOC_S_FMT: height has been changed, old value=%d, new value=%d!!\n",
          v4l2object->height, format.fmt.pix.height);
      v4l2object->height = format.fmt.pix.height;
    }
    if (format.fmt.pix.bytesperline != v4l2object->bytes_per_line) {
      GST_WARNING_OBJECT (v4l2object->element,
          "VIDIOC_S_FMT: bytes_per_line has been changed, old value=%d, new value=%d!!\n",
          v4l2object->bytes_per_line, format.fmt.pix.bytesperline);
      v4l2object->bytes_per_line = format.fmt.pix.bytesperline;
    }
  }

  /* image size to be computed after call to VIDIOC_S_FMT, in case values have been changed by driver */
  if (v4l2object->pixelformat == V4L2_PIX_FMT_NV12) {
    /* NV12 format is 12 bits per pixel */
    v4l2object->image_size = v4l2object->width * v4l2object->height;
    v4l2object->image_size += (v4l2object->width * v4l2object->height) / 2;
  } else if (v4l2object->pixelformat == V4L2_PIX_FMT_NV16) {
    v4l2object->image_size = v4l2object->width * v4l2object->height * 2;
  } else {
    /* Other are RGB32, therefore 32 bits per pixel */
    v4l2object->image_size = v4l2object->width * v4l2object->height * 4;
  }

  GST_DEBUG_OBJECT (v4l2object->element,
      "Setting format to %dx%d (image_size=%d), format " "%" GST_FOURCC_FORMAT,
      v4l2object->width, v4l2object->height, v4l2object->image_size,
      GST_FOURCC_ARGS (v4l2object->pixelformat));

  v4l2object->stream.width = v4l2object->width;
  v4l2object->stream.height = v4l2object->height;

  GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);

  return TRUE;

/* Error */
set_fmt_failed:
  {
    GST_ERROR_OBJECT (v4l2object->element, "VIDIOC_S_FMT failed - %s",
        strerror (errno));
    return FALSE;
  }
}

gboolean
gst_st_v4l2_object_start_streaming (GstSTV4l2Object * v4l2object)
{
  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_STREAMON,
          &(v4l2object->type)) < 0) {
    GST_ERROR_OBJECT (v4l2object->element, "VIDIOC_STREAMON failed - %s",
        strerror (errno));
    return FALSE;
  }

  GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);

  return TRUE;
}

gboolean
gst_st_v4l2_object_stop_streaming (GstSTV4l2Object * v4l2object)
{
  GST_LOG_OBJECT (v4l2object->element, "> %s ", __FUNCTION__);

  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_STREAMOFF,
          &(v4l2object->type)) < 0) {
    GST_ERROR_OBJECT (v4l2object->element, "VIDIOC_STREAMOFF failed - %s",
        strerror (errno));
    return FALSE;
  }

  GST_LOG_OBJECT (v4l2object->element, "< %s ", __FUNCTION__);

  return TRUE;
}
