/* Gstreamer ST Audio Sink Plugin
 *
 * Copyright (C) 2013 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STAUDIO_SINK_H__
#define __GST_STAUDIO_SINK_H__

#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include "gststvolume.h"

#define DEFAULT_DEV_ID    0
#define MIN_DEV_ID        0
#define MAX_DEV_ID        2

G_BEGIN_DECLS
#define GST_TYPE_STAUDIO_SINK \
  (gst_staudio_sink_get_type())
#define GST_STAUDIO_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_STAUDIO_SINK,GstStaudioSink))
#define GST_STAUDIO_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_STAUDIO_SINK,GstStaudioSinkClass))
#define GST_STAUDIO_SINK_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_STAUDIO_SINK, GstStaudioSinkClass))
#define GST_IS_STAUDIO_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_STAUDIO_SINK))
#define GST_IS_STAUDIO_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_STAUDIO_SINK))
#define GST_STAUDIO_SINK_CAST(obj) ((GstStaudioSink *)obj)
typedef struct _GstStaudioSink GstStaudioSink;
typedef struct _GstStaudioSinkClass GstStaudioSinkClass;

/**
 * GstStaudioSink:
 *
 * The opaque #GstStaudioSink data structure.
 */
struct _GstStaudioSink
{
  GstBaseSink element;
  GstStVolume volume;
  GstPad *sinkpad;
  GstCaps *prev_caps;
  gint dev_id;
};

struct _GstStaudioSinkClass
{
  GstBaseSinkClass parent_class;
    gboolean (*query) (GstElement * element, GstQuery * query);
};

GType gst_staudio_sink_get_type (void);
gboolean staudiosink_init (GstPlugin * plugin);

G_END_DECLS
#endif /* __GST_STAUDIO_SINK_H__ */
