/* Gstreamer ST Audio Volume Control
 *
 * Copyright (C) 2016 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <gst/gst.h>
#include <alsa/asoundlib.h>

#include "gststaudio.h"
#include "gststvolume.h"

/* ALSA helper functions */

#define HW_MIXER_0   "hw:MIXER0"

int
stm_volume_manager_open (GstStVolume * self)
{
  int err;
  snd_ctl_t *hnd;

  if (self->snd_ctl_handle)
    return 1;

  /* only dev-id 0/1 are allowed */
  if ((self->dev_id != 0) && (self->dev_id != 1))
    return -1;

  err = snd_ctl_open (&hnd, HW_MIXER_0, 0);
  if (err < 0) {
    GST_ERROR ("Control %s open error: %s\n", HW_MIXER_0, snd_strerror (err));
  } else {
    self->snd_ctl_handle = (void *) hnd;
    self->snd_num_id = self->dev_id + 1;
  }

  return err;
}

static int
stm_volume_manager_set_volume (GstStVolume * self, double volume)
{
  int err;
  long min, max;
  int val;
  int ch, maxch = 0;

  snd_ctl_elem_info_t *info;
  snd_ctl_elem_id_t *id;
  snd_ctl_elem_value_t *control;

  snd_ctl_elem_info_alloca (&info);
  snd_ctl_elem_id_alloca (&id);
  snd_ctl_elem_value_alloca (&control);

  snd_ctl_t *hnd = (snd_ctl_t *) self->snd_ctl_handle;

  if ((hnd == NULL) || (volume < 0.0) || (volume > 1.0))
    return -1;

  snd_ctl_elem_id_set_interface (id, SND_CTL_ELEM_IFACE_MIXER);
  snd_ctl_elem_id_set_numid (id, self->snd_num_id);
  snd_ctl_elem_info_set_id (info, id);

  if ((err = snd_ctl_elem_info (hnd, info)) < 0) {
    GST_ERROR ("Cannot find the given element from control %s\n", HW_MIXER_0);
    return -1;
  }

  snd_ctl_elem_info_get_id (info, id);
  snd_ctl_elem_value_set_id (control, id);

  min = MIN_VOLUME_MIXER;       // snd_ctl_elem_info_get_min (info);
  max = MAX_VOLUME_MIXER;       // snd_ctl_elem_info_get_max (info);
  val = (int) (min + (volume * ((double) (max - min)) + 0.5));

  maxch = snd_ctl_elem_info_get_count (info);

  for (ch = 0; ch < maxch; ch++)
    snd_ctl_elem_value_set_integer (control, ch, val);

  if ((err = snd_ctl_elem_write (hnd, control)) < 0) {
    GST_ERROR ("Control %s element write error: %s\n", HW_MIXER_0,
        snd_strerror (err));
    return -1;
  }

  return 0;
}


void
stm_volume_manager_close (GstStVolume * self)
{
  if (self->snd_ctl_handle) {
    snd_ctl_close ((snd_ctl_t *) self->snd_ctl_handle);
    self->snd_ctl_handle = NULL;
    self->snd_num_id = -1;
  }

  self->dev_id = -1;
}

void
gst_stvolume_init (GstStVolume * self)
{

  self->mute = DEFAULT_PROP_MUTE;
  self->volume = DEFAULT_PROP_VOLUME;

  self->dev_id = -1;
  self->snd_num_id = -1;
  self->snd_ctl_handle = NULL;

}

void
stvolume_set_property (GstStVolume * self, guint prop_id, const GValue * value,
    GParamSpec * pspec)
{
  int err;
  gboolean mute;
  double volume;
  int dev_id;

  switch (prop_id) {
    case PROP_MUTE:
      mute = g_value_get_boolean (value);
      if (mute != self->mute) {
        if (mute)
          err = stm_volume_manager_set_volume (self, 0);
        else
          err = stm_volume_manager_set_volume (self, self->volume);

        if (!err)
          self->mute = mute;
        else
          GST_ERROR ("Cannot mute device");
      }
      break;
    case PROP_VOLUME:
      volume = g_value_get_double (value);
      if (volume != self->volume) {
        err = stm_volume_manager_set_volume (self, volume);
        if (!err)
          self->volume = volume;
        else
          GST_ERROR ("Cannot set volume to %f", volume);
      }
      break;
    case PROP_DEV_ID:
      dev_id = g_value_get_int (value);
      if (dev_id != self->dev_id) {

        /* if open, close the volume device manager */
        if (self->snd_ctl_handle)
          stm_volume_manager_close (self);

        /* open it again ... */
        self->dev_id = dev_id;
        err = stm_volume_manager_open (self);
        if (err) {
          GST_WARNING
              ("Could not open volume manager on device id = %d, volume will not work!",
              self->dev_id);
          self->dev_id = -1;
        } else {
          /* ... and set the default volume value */
          err = stm_volume_manager_set_volume (self, self->volume);
        }
      }
      break;
    default:
      GST_WARNING ("Invalid Property ID");
      break;
  }

}

void
stvolume_get_property (GstStVolume * self, guint prop_id, GValue * value,
    GParamSpec * pspec)
{

  switch (prop_id) {
    case PROP_MUTE:
      g_value_set_boolean (value, self->mute);
      break;
    case PROP_VOLUME:
      g_value_set_double (value, self->volume);
      break;
    default:
      GST_WARNING ("Invalid Property ID");
      break;
  }
}
