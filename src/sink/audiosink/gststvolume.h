/* Gstreamer ST Audio Volume Control
 *
 * Copyright (C) 2016 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_VOLUME_H__
#define __GST_VOLUME_H__

#include <gst/gst.h>

/* some defines for audio processing */
/* the volume factor is a range from 0.0 to VOLUME_MAX_DOUBLE = 8.0 */
/* digital boost above DEFAULT_PROP_VOLUME i.e. 0dB may lead to audio saturation */
#define VOLUME_MAX_DOUBLE       8.0     /* digital boost: 18dB --> 0XFFFF */
#define DEFAULT_PROP_VOLUME     1.0     /* 0dB --> 0X2000 */
#define DEFAULT_PROP_MUTE       FALSE

/* alsa sound ctrl APIs are returning 0xFFFF as max value */
#define MIN_VOLUME_MIXER        0
#define MAX_VOLUME_MIXER        8192    /* 0dB --> 0X2000 */

G_BEGIN_DECLS typedef struct _GstStVolume GstStVolume;

enum
{
  PROP_0,
  PROP_MUTE,
  PROP_VOLUME,
  PROP_DEV_ID
};

/**
 * GstStVolume:
 *
 * Opaque data structure.
 */
struct _GstStVolume
{
  GstElement *element;          /* Parent element */

  gboolean mute;
  gfloat volume;

  void *snd_ctl_handle;
  int snd_num_id;
  int dev_id;
};

extern void gst_stvolume_init (GstStVolume * self);

extern void stvolume_set_property (GstStVolume * self, guint prop_id,
    const GValue * value, GParamSpec * pspec);
extern void stvolume_get_property (GstStVolume * self, guint prop_id,
    GValue * value, GParamSpec * pspec);

extern int stm_volume_manager_open (GstStVolume * self);
extern void stm_volume_manager_close (GstStVolume * self);

G_END_DECLS
#endif /* __GST_VOLUME_H__ */
