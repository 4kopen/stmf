/* Gstreamer ST Audio Sink Plugin
 *
 * Copyright (C) 2013 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
* element-staudiosink
*/

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include "gststaudiosink.h"
#include <gst/audio/streamvolume.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <linux/dvb/audio.h>


GST_DEBUG_CATEGORY_STATIC (gst_staudio_sink_debug);

static GstStaticPadTemplate audiosink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-fake-int-stm," "dev-id = (int) [0, MAX];"
        "audio/x-raw, "
        "  depth = (int) 32, "
        "  width = (int) 32, "
        "  channels = (int) 8, "
        "  endianness = (int) LITTLE_ENDIAN, "
        "  signed = (boolean) true, " "  rate = (int) [ 1, MAX ]; ")
    );

#define DEFAULT_SYNC FALSE

#define gst_staudio_sink_parent_class parent_class
G_DEFINE_TYPE_WITH_CODE (GstStaudioSink, gst_staudio_sink,
    GST_TYPE_BASE_SINK, G_IMPLEMENT_INTERFACE (GST_TYPE_STREAM_VOLUME, NULL)
    );

static void gst_staudiosink_dispose (GObject * object);
static void gst_staudiosink_finalize (GstStaudioSink * staudiosink);
static void gst_staudio_sink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_staudio_sink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static gboolean gst_staudio_sink_set_caps (GstBaseSink * bsink, GstCaps * caps);
static GstCaps *gst_staudio_sink_get_caps (GstBaseSink * bsink,
    GstCaps * filter);
static GstStateChangeReturn gst_staudiosink_change_state (GstElement * element,
    GstStateChange transition);
static gboolean gst_staudiosink_element_query (GstElement * element,
    GstQuery * query);

static void
gst_staudio_sink_class_init (GstStaudioSinkClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;
  GstBaseSinkClass *basesink_class;

  gobject_class = G_OBJECT_CLASS (klass);
  gstelement_class = (GstElementClass *) klass;
  basesink_class = GST_BASE_SINK_CLASS (klass);

  gobject_class->set_property =
      GST_DEBUG_FUNCPTR (gst_staudio_sink_set_property);
  gobject_class->get_property =
      GST_DEBUG_FUNCPTR (gst_staudio_sink_get_property);
  gobject_class->dispose = gst_staudiosink_dispose;
  gobject_class->finalize = (GObjectFinalizeFunc) gst_staudiosink_finalize;

  /* override the basesink element query function
   * we store the actual basesink handle for forwarding some queries
   * IT IS HACK: as basesink has no provision to send element query to sub class */
  klass->query = gstelement_class->query;
  gstelement_class->query = GST_DEBUG_FUNCPTR (gst_staudiosink_element_query);

  gstelement_class->change_state = gst_staudiosink_change_state;

  g_object_class_install_property (gobject_class, PROP_MUTE,
      g_param_spec_boolean ("mute", "Mute", "mute channel", DEFAULT_PROP_MUTE,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_VOLUME,
      g_param_spec_double ("volume", "Volume", "volume factor, 1.0=100%", 0.0,
          VOLUME_MAX_DOUBLE, DEFAULT_PROP_VOLUME,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_DEV_ID,
      g_param_spec_int ("dev-id", "audiosink device id", "audiosink device id",
          MIN_DEV_ID, MAX_DEV_ID, DEFAULT_DEV_ID,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  gst_element_class_set_static_metadata (gstelement_class,
      "ST Audio Sink",
      "Sink/Audio", "GStreamer Audio Sink Element for ST", "http://www.st.com");

  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&audiosink_factory));

  basesink_class->get_caps = GST_DEBUG_FUNCPTR (gst_staudio_sink_get_caps);
  basesink_class->set_caps = GST_DEBUG_FUNCPTR (gst_staudio_sink_set_caps);
}

static GstCaps *
gst_staudio_sink_get_caps (GstBaseSink * bsink, GstCaps * filter)
{
  GstStaudioSink *audiosink = GST_STAUDIO_SINK (bsink);
  GstCaps *caps;

  caps =
      gst_caps_new_simple ("audio/x-fake-int-stm", "dev-id", G_TYPE_INT,
      audiosink->dev_id, NULL);
  GST_LOG_OBJECT (audiosink, "dev-id caps: %" GST_PTR_FORMAT, caps);

  return caps;
}

static void
gst_staudio_sink_init (GstStaudioSink * audiosink)
{
  gst_base_sink_set_sync (GST_BASE_SINK (audiosink), DEFAULT_SYNC);

  audiosink->sinkpad =
      gst_element_get_static_pad (GST_ELEMENT (audiosink), "sink");

  audiosink->dev_id = DEFAULT_DEV_ID;
  audiosink->prev_caps = NULL;
  gst_stvolume_init (&audiosink->volume);
}

static void
gst_staudio_sink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstStaudioSink *sink;

  sink = GST_STAUDIO_SINK (object);

  switch (prop_id) {
    case PROP_DEV_ID:
      GST_OBJECT_LOCK (sink);
      sink->dev_id = g_value_get_int (value);
      stvolume_set_property (&sink->volume, prop_id, value, pspec);
      GST_OBJECT_UNLOCK (sink);
      break;
    case PROP_MUTE:
    case PROP_VOLUME:
      stvolume_set_property (&sink->volume, prop_id, value, pspec);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_staudio_sink_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  GstStaudioSink *sink;

  sink = GST_STAUDIO_SINK (object);
  switch (prop_id) {
    case PROP_DEV_ID:
      g_value_set_int (value, sink->dev_id);
      break;
    case PROP_MUTE:
    case PROP_VOLUME:
      stvolume_get_property (&sink->volume, prop_id, value, pspec);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static gboolean
gst_staudio_sink_set_caps (GstBaseSink * bsink, GstCaps * caps)
{
  GstStaudioSink *audiosink = GST_STAUDIO_SINK (bsink);
  const gchar *mimetype;
  const GstStructure *structure;
  gboolean ret = FALSE;

  structure = gst_caps_get_structure (caps, 0);
  mimetype = gst_structure_get_name (structure);

  GST_DEBUG_OBJECT (audiosink, "caps: %" GST_PTR_FORMAT, caps);

  if (!strcmp (mimetype, "audio/x-fake-int-stm")) {
    ret = TRUE;
  }

  return ret;
}

/*
 * Function name : gst_staudiosink_element_query
 *  Input        :
 *   element - element handle for query (its own handle)
 *   parent - parent element
 *   query - query done
 *  Description  : This method is overriding the basesink for position query.
 *                 position query in time format is forwarded to staudio element
 *                 as custom query.other queries are forwarded to basesink as usual.
 *
 * Return        :  gboolean
 */
gboolean
gst_staudiosink_element_query (GstElement * element, GstQuery * query)
{
  gboolean res = FALSE;
  GstBaseSink *basesink = GST_BASE_SINK (element);
  GstStaudioSink *audiosink = GST_STAUDIO_SINK_CAST (basesink);
  GstStaudioSinkClass *klass = GST_STAUDIO_SINK_GET_CLASS (audiosink);

  GST_DEBUG_OBJECT (audiosink, "%s - (%s)", __FUNCTION__,
      gst_query_type_get_name (GST_QUERY_TYPE (query)));

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_POSITION:
    {
      GstFormat format;
      GstStructure *structure;
      GstQuery *cust_query = NULL;
      gint64 pts = 0;
      const GValue *val = NULL;

      gst_query_parse_position (query, &format, NULL);
      /* Get the current stream position from staudio */
      /* do custome query instead of position query
       * because position query is answered wrongly by non-ST element
       * and therefore position query can not reach staudio element
       */
      if (format == GST_FORMAT_TIME) {
        GST_DEBUG_OBJECT (audiosink, "get stream position from staudio");
        structure =
            gst_structure_new ("stream-pos", "stream_pos", G_TYPE_INT64, pts,
            NULL);
        cust_query = gst_query_new_custom (GST_QUERY_CUSTOM, structure);
        res = gst_pad_peer_query (basesink->sinkpad, cust_query);
        if (res == TRUE) {
          val = gst_structure_get_value (structure, "stream_pos");
          if (val) {
            pts = g_value_get_int64 (val);
          }
        }
        gst_query_set_position (query, format, pts);
        gst_query_unref (cust_query);
      } else {
        if (klass->query) {
          GST_DEBUG_OBJECT (audiosink, "get stream position from basesink");
          res = klass->query (element, query);
        }
      }
    }
      break;
    default:
    {
      if (klass->query)
        res = klass->query (element, query);
    }
      break;
  }

  return res;
}

static GstStateChangeReturn
gst_staudiosink_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret;
  GstStaudioSink *audiosink = GST_STAUDIO_SINK (element);

  GST_DEBUG_OBJECT (audiosink, "%d -> %d",
      GST_STATE_TRANSITION_CURRENT (transition),
      GST_STATE_TRANSITION_NEXT (transition));

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      GST_OBJECT_LOCK (audiosink);
      audiosink->volume.dev_id = audiosink->dev_id;
      stm_volume_manager_open (&audiosink->volume);
      GST_OBJECT_UNLOCK (audiosink);
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      stm_volume_manager_close (&audiosink->volume);
      break;
    default:
      break;
  }

  return ret;
}

static void
gst_staudiosink_dispose (GObject * object)
{
  GstStaudioSink *audiosink = GST_STAUDIO_SINK (object);

  G_OBJECT_CLASS (parent_class)->dispose (object);

  GST_LOG_OBJECT (audiosink, "< %s ", __FUNCTION__);
  return;
}

static void
gst_staudiosink_finalize (GstStaudioSink * audiosink)
{
  GST_LOG_OBJECT (audiosink, "> %s ", __FUNCTION__);

  G_OBJECT_CLASS (parent_class)->finalize ((GObject *) audiosink);

  GST_LOG_OBJECT (audiosink, "< %s ", __FUNCTION__);
  return;
}

gboolean
staudiosink_init (GstPlugin * plugin)
{
  GST_DEBUG_CATEGORY_INIT (gst_staudio_sink_debug, "staudiosink", 0,
      "STAUDIO sink element");

  return gst_element_register (plugin, "staudiosink", (GST_RANK_PRIMARY + 10),
      GST_TYPE_STAUDIO_SINK);
}
