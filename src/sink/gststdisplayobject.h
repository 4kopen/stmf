/* GStreamer
 *
 * Copyright (C) 2001-2002 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *               2006 Edgard Lima <edgard.lima@indt.org.br>
 * Copyright (C) 2012 STMicroelectronics
 *
 * gstv4l2object.h: base class for V4L2 elements
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * This file has been modified by STMicroelectronics, on 03/19/2012
 */

#ifndef __GST_STDISPLAY_OBJECT_H__
#define __GST_STDISPLAY_OBJECT_H__

#include <sys/ioctl.h>
#include <sys/types.h>

#include <linux/videodev2.h>
#include <linux/stmvout.h>
#include <gst/gst.h>

#define v4l2_fd_open(fd, flags) (fd)
#define v4l2_close    close
#define v4l2_dup      dup
#define v4l2_ioctl    ioctl
#define v4l2_read     read
#define v4l2_mmap     mmap
#define v4l2_munmap   munmap

#define DEFAULT_PLANE_NAME  "NO-PLANE"
#define PLANE_NAME_LENGTH 20

#define DEFAULT_DEV_ID    0
#define MIN_DEV_ID        0
#define MAX_DEV_ID        2

/*
Max limits for plane allocation.
*/
#define DEFAULT_ALLOC_WIDTH   720
#define DEFAULT_ALLOC_HEIGHT  576
#define DEFAULT_STREAM_WIDTH   720
#define DEFAULT_STREAM_HEIGHT  576
#define MAX_PLANE_NAME_SIZE 4

/* State values */
enum
{
  STATE_OFF = 0,
  STATE_PENDING_STREAMON,
  STATE_STREAMING
};

G_BEGIN_DECLS
#define GST_STDISPLAY_OBJECT(obj) (GstSTV4l2Object*)(obj)
typedef struct _GstSTV4l2Object GstSTV4l2Object;

typedef struct
{
  guint32 x_pos;
  guint32 y_pos;
  guint32 width;
  guint32 height;
  gboolean enabled;
} gststrectangle;

struct _GstSTV4l2Object
{
  GstElement *element;          /* Parent element */
  guint dev_id;
  gint video_fd_overlay;        /* Video device's file descriptor for overlay */
  gint video_fd_output;         /* Video device's file descriptor for output */
  struct v4l2_buffer first_buffer;
  guchar *first_bufferdata;
  enum v4l2_buf_type type;      /*  V4L2_BUF_TYPE_VIDEO_CAPTURE,
                                   V4L2_BUF_TYPE_VIDEO_OUTPUT  */
  guint8 state;
  gchar display_plane_name[PLANE_NAME_LENGTH];
  guint32 display_plane_index;
  gboolean display_plane_open;
  gboolean input_video_plane_open;
  gboolean output_video_plane_open;
  gint input_plane_mode;        /* if input plane is in manual mode or auto mode */
  gint output_plane_mode;       /* if output plane is in manual mode or auto mode */
  gststrectangle plane;
  gststrectangle stream;
  gboolean stream_plane_info_set;
  gint width;
  gint height;
  gint pixelformat;
  gint bytes_per_line;
  gint image_size;
  guint8 queued_buffer_index;
  GSList *formats;              /* List of available capture formats */
  gint DAR_n;
  gint DAR_d;
  gchar plane_finder[MAX_PLANE_NAME_SIZE];
  gboolean is_upstream_ancillary;
  gint PAR_n; // stream pixel-aspect-ratio num
  gint PAR_d; // stream pixel-aspect-ratio denum
};

/* Create/Destroy */
GstSTV4l2Object *gst_st_v4l2_object_new (GstElement * element,
    enum v4l2_buf_type type);
void gst_st_v4l2_object_destroy (GstSTV4l2Object * v4l2object);

/* Starting/Stopping */
gboolean gst_st_v4l2_object_start (GstSTV4l2Object * v4l2object);
gboolean gst_st_v4l2_object_stop (GstSTV4l2Object * v4l2object);

GstCaps *gst_st_v4l2_object_probe_caps_for_format (GstSTV4l2Object * v4l2object,
    guint32 pixelformat, const GstStructure * template);
gboolean gst_st_v4l2_object_get_caps_info (GstSTV4l2Object * v4l2object,
    GstCaps * caps, guint32 * format, gint * w, gint * h, guint * fps_n,
    guint * fps_d, gboolean * fake_mimetype, gboolean * is_ancillary,
    gboolean * is_upstream_ancillary, gboolean * release_plane);

gboolean gst_st_v4l2_open_device (GstSTV4l2Object * v4l2object);
gboolean gst_st_v4l2_set_display_plane (GstSTV4l2Object * v4l2object);

GSList *gst_st_v4l2_object_get_format_list (GstSTV4l2Object * v4l2object);

GstCaps *gst_st_v4l2_object_get_selective_caps (GstSTV4l2Object * v4l2object);

GstCaps *gst_st_v4l2_object_get_all_caps (GstSTV4l2Object * v4l2object);

GstStructure *gst_st_v4l2_object_v4l2fourcc_to_structure (guint32 fourcc);

gboolean gst_st_v4l2_object_set_format (GstSTV4l2Object * v4l2object);

gboolean gst_st_v4l2_object_start_streaming (GstSTV4l2Object * v4l2object);
gboolean gst_st_v4l2_object_stop_streaming (GstSTV4l2Object * v4l2object);
gboolean gst_st_v4l2_close_device (GstSTV4l2Object * v4l2object);
gboolean gst_st_v4l2_set_aspect_ratio (GstSTV4l2Object * v4l2object, gint DAR_n,
    gint DAR_d);
void gst_st_v4l2_object_reset (GstSTV4l2Object * v4l2object,
    enum v4l2_buf_type type);
void gst_st_v4l2_set_plane_order (GstSTV4l2Object * v4l2object);

G_END_DECLS
#endif /* __GST_STDISPLAY_OBJECT_H__ */
