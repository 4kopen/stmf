/* Gstreamer ST TS Muxer Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GSTSTDISPLAYSINK_H__
#define __GSTSTDISPLAYSINK_H__

#include <gst/video/gstvideosink.h>
#include "gststdisplayobject.h"


/* Crops/Scale default values */
#define DEFAULT_PROP_SRC_RECT            ""
#define DEFAULT_PROP_DST_RECT            ""
#define DEFAULT_PROP_PLANE_SIZE            ""
#define DEFAULT_PROP_STREAM_DISPLAY_SIZE            ""

#define DEFAULT_PLANE_ALPHA              255
#define MIN_PLANE_ALPHA                  0
#define MAX_PLANE_ALPHA                  255

/* Aspect Radio Control Mode Constants */
#define DEFAULT_ARC_MODE                 0
#define MIN_ARC_MODE                     0
#define MAX_ARC_MODE                     3

/* Aspect Radio Constants */
#define DEFAULT_DISPLAY_ASPECT_RATIO     0
#define MIN_DISPLAY_ASPECT_RATIO         0
#define MAX_DISPLAY_ASPECT_RATIO         2

GST_DEBUG_CATEGORY_EXTERN (stdisplaysink_debug);

G_BEGIN_DECLS
#define GST_TYPE_STDISPLAYSINK \
  (gst_stdisplaysink_get_type())
#define GST_STDISPLAYSINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_STDISPLAYSINK, GstSTV4l2Sink))
#define GST_STDISPLAYSINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_STDISPLAYSINK, GstSTV4l2SinkClass))
#define GST_STDISPLAYSINK_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_STDISPLAYSINK, GstSTV4l2SinkClass))
#define GST_IS_STDISPLAYSINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_STDISPLAYSINK))
#define GST_IS_STDISPLAYSINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_STDISPLAYSINK))
typedef struct _GstSTV4l2Sink GstSTV4l2Sink;
typedef struct _GstSTV4l2SinkClass GstSTV4l2SinkClass;

#define GST_STDISPLAYSINK_REQ_NONE                              0x0000
#define GST_STDISPLAYSINK_REQ_RECONFIG                          0x0001
#define GST_STDISPLAYSINK_REQ_PLANE_NAME                        0x0002
#define GST_STDISPLAYSINK_REQ_DST_RECT                          0x0004
#define GST_STDISPLAYSINK_REQ_SRC_RECT                          0x0008
#define GST_STDISPLAYSINK_REQ_ROTATE                            0x0010
#define GST_STDISPLAYSINK_REQ_FLIP_VERTICAL                     0x0020
#define GST_STDISPLAYSINK_REQ_FLIP_HORIZONTAL                   0x0040
#define GST_STDISPLAYSINK_REQ_PLANE_TRANSPARENCY                0x0080
#define GST_STDISPLAYSINK_REQ_ARC_MODE                          0x0100
#define GST_STDISPLAYSINK_REQ_DAR                               0x0200
#define GST_STDISPLAYSINK_REQ_MUTE                              0x0400


struct _GstSTV4l2Sink
{
  GstVideoSink videosink;
  /* private */
  GstSTV4l2Object *v4l2object;
  GstPad *sinkpad;
  GstCaps *probed_caps;         /* All supported caps of underlying v4l2 device */
  GstCaps *current_caps;        /* Current negotiated caps                      */
  GstCaps *prev_caps;
  guint32 num_buffers;
  struct v4l2_buffer buffer[20];
  guchar *bufferdata[20];
  gboolean buffer_allocated;
  gboolean fake_mimetype;
  gststrectangle input_rect;
  gststrectangle output_rect;
  gboolean rotate;
  guint32 rotate_degree;
  gboolean vertical_flip;
  gboolean horizontal_flip;
  gboolean is_ancillary;
  guint32 graphics_alpha;
  guint32 prev_width;
  guint prev_height;
  guint32 prev_fourcc;
  guint32 arc_mode;
  guint32 display_aspect_ratio;
  GstTask *monitor_task;
  GRecMutex monitor_task_lock;
  guint16 pending_req;
  gint dst_width;
  gint dst_height;
  gboolean mute;                /* whether video is mute or not */
};

struct _GstSTV4l2SinkClass
{
  GstVideoSinkClass parent_class;
    gboolean (*query) (GstElement * element, GstQuery * query);
};

GType gst_stdisplaysink_get_type (void);
gboolean stdisplaysink_init (GstPlugin * plugin);

G_END_DECLS
#endif /* __GSTSTDISPLAYSINK_H__ */
