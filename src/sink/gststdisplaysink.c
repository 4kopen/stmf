/* Gstreamer ST Display Sink Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:element-stsidiplaysink
 *
 * Description: stdisplaysink element is used for invoking the ST display with v4l2 interface ioctl
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch videotestsrc ! videoconvert ! stdisplaysink
 * ]|
 * </refsect2>
 */

#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include "gststdisplaysink.h"
#include <linux/dvb/dvb_v4l2_export.h>
#include "v4l2_utils.h"
#include <stdlib.h>

GST_DEBUG_CATEGORY (stdisplaysink_debug);
#define GST_CAT_DEFAULT stdisplaysink_debug

/* minimum stmfb requirement is 2 buffers for orly */
#define PROP_DEF_QUEUE_SIZE     2

#define LOG_CAPS(obj, caps) GST_DEBUG_OBJECT (obj, "%" GST_PTR_FORMAT, caps)

enum
{
  PROP_0,
  PROP_SRC_RECT,
  PROP_DST_RECT,
  PROP_ROTATE_DEGREE,
  PROP_FLIP_VERTICAL,
  PROP_FLIP_HORIZONTAL,
  PROP_PLANE_NAME,
  PROP_CTRL_PLANE_TRANSPARENCY,
  PROP_DEV_ID,
  PROP_ARC_MODE,
  PROP_DISPLAY_ASPECT_RATIO_VALUE,
  PROP_PLANE_SIZE,
  PROP_STREAM_DISPLAY_SIZE,
  PROP_STREAM_PIXEL_ASPECT_RATIO,
  PROP_MUTE,
  PROP_LAST
};


#define gst_stdisplaysink_parent_class parent_class
G_DEFINE_TYPE (GstSTV4l2Sink, gst_stdisplaysink, GST_TYPE_VIDEO_SINK);

enum
{
  GRAPHICS_PLANE = 0,
  VIDEO_PLANE
};

#define DEFAULT_MUTE FALSE

static void gst_stdisplaysink_dispose (GObject * object);
static void gst_stdisplaysink_finalize (GstSTV4l2Sink * v4l2sink);

/* GObject methods: */
static void gst_stdisplaysink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_stdisplaysink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

/* GstElement methods: */
static GstStateChangeReturn gst_stdisplaysink_change_state (GstElement *
    element, GstStateChange transition);

/* GstBaseSink methods: */
static gboolean gst_stdisplaysink_reset (GstSTV4l2Sink * v4l2sink, guint w,
    guint h, guint32 fourcc);
static gboolean gst_stdisplaysink_set_caps (GstSTV4l2Sink * v4l2sink,
    GstCaps * caps);
static GstFlowReturn gst_stdisplaysink_show_frame (GstVideoSink * bsink,
    GstBuffer * buf);

/* local methods */
static gboolean gststv4l2sink_reset_iowindows (GstSTV4l2Sink * v4l2sink);
static gboolean gststv4l2sink_graphics_swap_and_refresh (GstSTV4l2Sink *
    v4l2sink);
static gboolean gst_stdisplaysink_resize_graphics_plane (GstSTV4l2Sink *
    v4l2sink);
static void gst_stdisplaysink_create_monitor_task (GstSTV4l2Sink * v4l2sink);
static void gst_stdisplaysink_stop_monitor_task (GstSTV4l2Sink * v4l2sink);
static void gst_stdisplaysink_monitor_loop (GstSTV4l2Sink * v4l2sink);
static void gst_stdisplaysink_set_video_mute_unmute (GstSTV4l2Sink * v4l2sink);
static gboolean gst_stdisplaysink_query (GstPad * pad, GstObject * parent,
    GstQuery * query);
static gboolean gst_stdisplaysink_sink_pad_event (GstBaseSink * bsink,
    GstEvent * event);
static void gst_stdisplaysink_deallocate (GstSTV4l2Sink * v4l2sink);
static gboolean gst_stdisplaysink_element_query (GstElement * element,
    GstQuery * query);
static GstCaps *gst_stdisplaysink_get_caps (GstSTV4l2Sink * v4l2sink,
    GstCaps * filter);

static void
gst_stdisplaysink_class_init (GstSTV4l2SinkClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *element_class;
  GstBaseSinkClass *basesink_class;
  GstVideoSinkClass *vsink_class;

  GST_LOG ("> %s ", __FUNCTION__);

  gobject_class = G_OBJECT_CLASS (klass);
  element_class = GST_ELEMENT_CLASS (klass);
  basesink_class = GST_BASE_SINK_CLASS (klass);
  vsink_class = GST_VIDEO_SINK_CLASS (klass);

  gobject_class->dispose = gst_stdisplaysink_dispose;
  gobject_class->finalize = (GObjectFinalizeFunc) gst_stdisplaysink_finalize;
  gobject_class->set_property = gst_stdisplaysink_set_property;
  gobject_class->get_property = gst_stdisplaysink_get_property;

  element_class->change_state = gst_stdisplaysink_change_state;

  /* override the basesink element query function
   * we store the actual basesink handle for forwarding some queries
   * IT IS HACK: as basesink has no provision to send element query to sub class */
  klass->query = element_class->query;
  element_class->query = GST_DEBUG_FUNCPTR (gst_stdisplaysink_element_query);

  /* Set Source Rectangle */
  g_object_class_install_property (gobject_class, PROP_SRC_RECT,
      g_param_spec_string ("src-rect",
          "Colon separated list of input window parameters",
          "Parameters for input window crop, "
          "x:Xs0/y:Ys0/w:Hs/h:Vs, "
          "eg x:50/y:70/w:300/h:400", DEFAULT_PROP_SRC_RECT,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  /* Set Destination Rectangle */
  g_object_class_install_property (gobject_class, PROP_DST_RECT,
      g_param_spec_string ("dst-rect",
          "Colon separated list of output window parameters",
          "Parameters for output window resize, "
          "x:Xo0/y:Yo0/w:Ho/h:Vo, "
          "eg x:50/y:70/w:300/h:400", DEFAULT_PROP_DST_RECT,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  /* Rotate clockwise */
  g_object_class_install_property (gobject_class, PROP_ROTATE_DEGREE,
      g_param_spec_uint ("r", "rotate", "rotate", 0, 270, 0,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));
  /* Vertical flip */
  g_object_class_install_property (gobject_class, PROP_FLIP_VERTICAL,
      g_param_spec_boolean ("vf", "vertical-flip", "vertical-flip", FALSE,
          G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));
  /* Horizontal flip */
  g_object_class_install_property (gobject_class, PROP_FLIP_HORIZONTAL,
      g_param_spec_boolean ("hf", "horizontal-flip", "horizontal-flip", FALSE,
          G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));
  /* Display plane */
  g_object_class_install_property (gobject_class, PROP_PLANE_NAME,
      g_param_spec_string ("plane-name", "video/graphics plane name",
          "video/graphics plane name e.g Main-GDP1,Main-GDP2,Main-VID,...",
          DEFAULT_PLANE_NAME, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  /* Control transparency of graphics plane */
  g_object_class_install_property (gobject_class, PROP_CTRL_PLANE_TRANSPARENCY,
      g_param_spec_uint ("plane-transp", "control graphics transparency",
          "control graphics transparency", MIN_PLANE_ALPHA, MAX_PLANE_ALPHA,
          DEFAULT_PLANE_ALPHA,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));
  /* Display Device */
  g_object_class_install_property (gobject_class, PROP_DEV_ID,
      g_param_spec_uint ("dev-id", "video device id", "video device id",
          MIN_DEV_ID, MAX_DEV_ID, DEFAULT_DEV_ID,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));
  /* Aspect Ratio */
  g_object_class_install_property (gobject_class, PROP_ARC_MODE,
      g_param_spec_uint ("arc-mode", "aspect ratio conversion mode",
          "desired aspect ratio conversion mode", MIN_ARC_MODE, MAX_ARC_MODE,
          MIN_ARC_MODE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));
  g_object_class_install_property (gobject_class,
      PROP_DISPLAY_ASPECT_RATIO_VALUE,
      g_param_spec_uint ("display-aspect-ratio", "display aspect ratio",
          "desired display aspect ratio", MIN_DISPLAY_ASPECT_RATIO,
          MAX_DISPLAY_ASPECT_RATIO, MIN_DISPLAY_ASPECT_RATIO,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  /* Get Plane size */
  g_object_class_install_property (gobject_class, PROP_PLANE_SIZE,
      g_param_spec_string ("plane-size",
          "Colon separated list of plane window parameters",
          "Parameters for plane size, "
          "x:Xs0/y:Ys0/w:Hs/h:Vs, "
          "eg x:50/y:70/w:300/h:400", DEFAULT_PROP_PLANE_SIZE,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /* Get Stream size */
  g_object_class_install_property (gobject_class, PROP_STREAM_DISPLAY_SIZE,
      g_param_spec_string ("stream-input-rect",
          "Colon separated list of stream input window parameters",
          "Parameters for input stream display size, "
          "x:Xo0/y:Yo0/w:Ho/h:Vo, "
          "eg x:50/y:70/w:300/h:400", DEFAULT_PROP_STREAM_DISPLAY_SIZE,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /* Get Stream Pixel Aspect Ratio */ 
  g_object_class_install_property (gobject_class, PROP_STREAM_PIXEL_ASPECT_RATIO,
      gst_param_spec_fraction ("stream-pixel-aspect-ratio",
          "Pixel Aspect Ratio",
          "The pixel aspect ratio of the stream", 0, 1, G_MAXINT, G_MAXINT, 1, 1,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /* Video mute */
  g_object_class_install_property (gobject_class, PROP_MUTE,
      g_param_spec_boolean ("mute", "mute video", "mute/unmute video",
          DEFAULT_MUTE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));


  gst_element_class_set_static_metadata (element_class, "ST Display Sink",
      "Sink/Video/ST", "Control Display output", "http://www.st.com");

  gst_element_class_add_pad_template (element_class,
      gst_pad_template_new ("sink", GST_PAD_SINK, GST_PAD_ALWAYS,
          gst_st_v4l2_object_get_all_caps (NULL)));

  basesink_class->event = GST_DEBUG_FUNCPTR (gst_stdisplaysink_sink_pad_event);
  vsink_class->show_frame = GST_DEBUG_FUNCPTR (gst_stdisplaysink_show_frame);

  GST_LOG ("< %s ", __FUNCTION__);
  return;
}

/*
 function: gst_stdisplaysink_reset_params
 input params:
           v4l2sink - v4l2sink context
 description : will reset stdisplaysink parameters.
 return : void.
 */
static void
gst_stdisplaysink_reset_params (GstSTV4l2Sink * v4l2sink)
{
  /* Number of buffers requested */
  v4l2sink->num_buffers = PROP_DEF_QUEUE_SIZE;
  v4l2sink->probed_caps = NULL;
  v4l2sink->current_caps = NULL;
  v4l2sink->prev_caps = NULL;
  v4l2sink->buffer_allocated = FALSE;
  v4l2sink->fake_mimetype = FALSE;
  v4l2sink->rotate_degree = 0;
  v4l2sink->input_rect.x_pos = 0;
  v4l2sink->input_rect.y_pos = 0;
  v4l2sink->input_rect.width = 0;
  v4l2sink->input_rect.height = 0;
  v4l2sink->input_rect.enabled = TRUE;
  v4l2sink->output_rect.x_pos = 0;
  v4l2sink->output_rect.y_pos = 0;
  v4l2sink->output_rect.width = 0;
  v4l2sink->output_rect.height = 0;
  v4l2sink->output_rect.enabled = TRUE;
  v4l2sink->is_ancillary = FALSE;
  v4l2sink->graphics_alpha = 0xFF;
  v4l2sink->prev_width = 0;
  v4l2sink->prev_height = 0;
  v4l2sink->prev_fourcc = 0;
  v4l2sink->arc_mode = 0;
  v4l2sink->display_aspect_ratio = 0;
  v4l2sink->pending_req = GST_STDISPLAYSINK_REQ_NONE;
  v4l2sink->dst_width = 0;
  v4l2sink->dst_height = 0;
  v4l2sink->mute = DEFAULT_MUTE;
}

static void
gst_stdisplaysink_init (GstSTV4l2Sink * v4l2sink)
{
  v4l2sink->v4l2object =
      gst_st_v4l2_object_new (GST_ELEMENT (v4l2sink),
      V4L2_BUF_TYPE_VIDEO_OUTPUT);
  v4l2sink->v4l2object->dev_id = DEFAULT_DEV_ID;
  gst_stdisplaysink_reset_params (v4l2sink);
  v4l2sink->sinkpad =
      gst_element_get_static_pad (GST_ELEMENT (v4l2sink), "sink");
  gst_pad_set_query_function (v4l2sink->sinkpad,
      GST_DEBUG_FUNCPTR (gst_stdisplaysink_query));
}

static void
gst_stdisplaysink_dispose (GObject * object)
{
  GstSTV4l2Sink *v4l2sink = GST_STDISPLAYSINK (object);

  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);

  if (v4l2sink->probed_caps) {
    gst_caps_unref (v4l2sink->probed_caps);
  }

  if (v4l2sink->current_caps) {
    gst_caps_unref (v4l2sink->current_caps);
  }

  if (v4l2sink->prev_caps) {
    gst_caps_unref (v4l2sink->prev_caps);
  }

  G_OBJECT_CLASS (parent_class)->dispose (object);

  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return;
}

static void
gst_stdisplaysink_finalize (GstSTV4l2Sink * v4l2sink)
{
  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);

  gst_st_v4l2_object_destroy (v4l2sink->v4l2object);
  v4l2sink->v4l2object = NULL;

  G_OBJECT_CLASS (parent_class)->finalize ((GObject *) v4l2sink);

  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return;
}

/*
 * Function name : gst_stdisplaysink_element_query
 *  Input        :
 *   element - element handle for query (its own handle)
 *   query - query done
 *  Description  : This method is overriding the basesink for position query.
 *  position query in time format is forwarded to stvideo element as custom query.
 *  other queries are forwarded to basesink as usual.
 *
 * Return        :  gboolean
 */
gboolean
gst_stdisplaysink_element_query (GstElement * element, GstQuery * query)
{
  gboolean res = FALSE;
  GstBaseSink *basesink = GST_BASE_SINK (element);
  GstSTV4l2Sink *v4l2sink = GST_STDISPLAYSINK (basesink);
  GstSTV4l2SinkClass *klass = GST_STDISPLAYSINK_GET_CLASS (v4l2sink);

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_POSITION:
    {
      GstFormat format;
      GstStructure *structure;
      GstQuery *cust_query = NULL;
      gint64 pts = 0;
      const GValue *val = NULL;

      gst_query_parse_position (query, &format, NULL);
      /* Get the current stream position from stvideo */
      /* do custome query instead of position query
       * because position query is answered wrongly by non-ST element
       * and therefore position query can not reach stvideo element */
      if (format == GST_FORMAT_TIME) {
        structure =
            gst_structure_new ("stream-pos", "stream_pos", G_TYPE_INT64, pts,
            NULL);
        cust_query = gst_query_new_custom (GST_QUERY_CUSTOM, structure);
        res = gst_pad_peer_query (basesink->sinkpad, cust_query);
        if (res == TRUE) {
          val = gst_structure_get_value (structure, "stream_pos");
          if (val) {
            pts = g_value_get_int64 (val);
          }
        }
        gst_query_set_position (query, format, pts);
        gst_query_unref (cust_query);
      } else {
        if (klass->query) {
          res = klass->query (element, query);
        }
      }
    }
      break;
    default:
    {
      if (klass->query)
        res = klass->query (element, query);
    }
      break;
  }

  return res;
}

GstElementClass *element_class;

static gboolean
gst_stdisplaysink_query (GstPad * pad, GstObject * parent, GstQuery * query)
{
  GstSTV4l2Sink *v4l2sink = GST_STDISPLAYSINK (parent);
  gboolean res = FALSE;
  GstStructure *structure;
  const gchar *plane_name = NULL;

  GST_DEBUG_OBJECT (v4l2sink, "%s - (%s)", __FUNCTION__,
      gst_query_type_get_name (GST_QUERY_TYPE (query)));

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CUSTOM:
      structure = gst_query_writable_structure (query);
      if (structure) {
        if (!strcmp ("video-entity", gst_structure_get_name (structure))) {
          if (v4l2sink->v4l2object->dev_id == 0) {
            plane_name = "Main-VID";
          } else if (v4l2sink->v4l2object->dev_id == 1) {
            plane_name = "Main-PIP";
          } else if (v4l2sink->v4l2object->dev_id == 2) {
            plane_name = "Aux-VID";
          } else {
            plane_name = "none";
          }
          gst_structure_set (structure, "video-entity", G_TYPE_STRING,
              plane_name, NULL);
          res = TRUE;
        }
      }
      break;
    case GST_QUERY_CAPS:
    {
      GstCaps *caps;
      gst_query_parse_caps (query, &caps);
      caps = gst_stdisplaysink_get_caps (v4l2sink, caps);
      gst_query_set_caps_result (query, caps);
      gst_caps_unref (caps);
      res = TRUE;
    }
      break;

    default:
      res = gst_pad_query_default (pad, parent, query);
      break;
  }

  return res;
}

static gboolean
gst_stdisplaysink_set_output_video_plane (GstSTV4l2Sink * v4l2sink)
{
  gint videofd = v4l2sink->v4l2object->video_fd_output;
  gchar plane_name[16];
  struct v4l2_control Control;
  gint ret = -1;

  /* stdisplaysink only handles MAIN and PIP video planes    */
  /* For now it is determined based on dev_id: 0:MAIN, 1:PIP */
  if (!v4l2sink->v4l2object->output_video_plane_open) {
    if (v4l2sink->v4l2object->dev_id == 0) {
      g_strlcpy (plane_name, "Main-VID", sizeof (plane_name));
    } else if (v4l2sink->v4l2object->dev_id == 1) {
      g_strlcpy (plane_name, "Main-PIP", sizeof (plane_name));
    } else {
      GST_ERROR_OBJECT (v4l2sink, "dev_id value not supported - %d",
          v4l2sink->v4l2object->dev_id);
      return FALSE;
    }

    GST_DEBUG_OBJECT (v4l2sink, "set output video plane %s", plane_name);
    ret = v4l2_set_output_by_name (videofd, plane_name);

    if (ret < 0) {
      GST_ERROR_OBJECT (v4l2sink, "VIDIOC_S_OUTPUT failed - %s",
          strerror (errno));
      return FALSE;
    }

    /* initialize hide mode */
    Control.id = V4L2_CID_STM_PLANE_HIDE_MODE;
    Control.value = VCSPHM_MIXER_DISABLE;
    if (ioctl (videofd, VIDIOC_S_CTRL, &Control) != 0) {
      GST_WARNING_OBJECT (v4l2sink,
          "Failed to set V4L2_CID_STM_PLANE_HIDE_MODE to VCSPHM_MIXER_DISABLE");
    }
    v4l2sink->v4l2object->output_video_plane_open = TRUE;
  }
  return TRUE;
}

static gboolean
gst_stdisplaysink_set_input_video_plane (GstSTV4l2Sink * v4l2sink)
{
  gint ret = -1;
  gint videofd = v4l2sink->v4l2object->video_fd_overlay;
  gchar plane_name[16];

  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);

  if ((!v4l2sink->v4l2object->input_video_plane_open) && (videofd != -1)) {
    g_snprintf (plane_name, sizeof (plane_name), "dvb0.video%d",
        v4l2sink->v4l2object->dev_id);

    GST_DEBUG_OBJECT (v4l2sink, "set input video plane %s", plane_name);
    ret = v4l2_set_input_by_name (videofd, plane_name);

    if (ret < 0) {
      GST_ERROR_OBJECT (v4l2sink, "failed to set input on %s - %s", plane_name,
          strerror (errno));
      return FALSE;
    }

    v4l2sink->v4l2object->input_video_plane_open = TRUE;
  }

  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return TRUE;
}

static gboolean
gst_stdisplaysink_get_output_video_plane_info (GstSTV4l2Sink * v4l2sink,
    gststrectangle * rect)
{
  gint videofd = v4l2sink->v4l2object->video_fd_output;
  struct v4l2_cropcap cropcap;

  if (!gst_stdisplaysink_set_output_video_plane (v4l2sink)) {
    return FALSE;
  }

  /* Set the crop */
  memset (&cropcap, 0, sizeof (cropcap));
  cropcap.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

  if (v4l2_ioctl (videofd, VIDIOC_CROPCAP, &cropcap) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_CROPCAP failed - %s", strerror (errno));
    return FALSE;
  }

  GST_DEBUG_OBJECT (v4l2sink,
      "output video plane info: video_fd_output %d, x_pos %d, y_pos %d, width %u, height %u",
      videofd, cropcap.bounds.left, cropcap.bounds.top, cropcap.bounds.width,
      cropcap.bounds.height);
  rect->x_pos = cropcap.bounds.left;
  rect->y_pos = cropcap.bounds.top;
  rect->width = cropcap.bounds.width;
  rect->height = cropcap.bounds.height;

  return TRUE;
}

static gboolean
gst_stdisplaysink_dequeue_buffer (GstSTV4l2Sink * v4l2sink,
    struct v4l2_buffer *dqbuffer)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;

  if (v4l2sink->num_buffers < PROP_DEF_QUEUE_SIZE) {
    GST_DEBUG_OBJECT (v4l2sink, "No transform buffer available");
    return FALSE;
  }

  dqbuffer->type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  dqbuffer->memory = V4L2_MEMORY_MMAP;

  /* Dqueue buffer */
  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_DQBUF, dqbuffer) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_DQBUF failed - %s", strerror (errno));
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_stdisplaysink_enqueue_buffer (GstSTV4l2Sink * v4l2sink,
    struct v4l2_buffer *dqbuffer)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;

  /* Queue buffer */
  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_QBUF,
          &v4l2sink->buffer[dqbuffer->index]) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_QBUF failed - %s", strerror (errno));
    return FALSE;
  }

  return TRUE;
}

static void
gst_stdisplaysink_flip_vertical (GstSTV4l2Sink * v4l2sink, guchar * dst_buf,
    guchar * src_buf)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  gint dst_width = v4l2object->bytes_per_line;
  gint src_height = v4l2object->height;
  gint row = 0, col = 0;

  for (row = 0; row < src_height; row++) {
    for (col = 0; col < dst_width; col++) {
      dst_buf[((((src_height - 1) - row) * dst_width) + col)] =
          src_buf[col + (row * dst_width)];
    }
  }
}

static void
gst_stdisplaysink_flip_horizontal (GstSTV4l2Sink * v4l2sink, guchar * dst_buf,
    guchar * src_buf)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  gint dst_width = v4l2object->bytes_per_line;
  gint src_height = v4l2object->height;
  gint row = 0, col = 0;

  for (row = 0; row < src_height; row++) {
    for (col = 0; col < dst_width; col += 4) {
      dst_buf[(row * dst_width) + col] =
          src_buf[((dst_width - 1) - col - 3) + (row * dst_width)];
      dst_buf[(row * dst_width) + col + 1] =
          src_buf[((dst_width - 1) - col - 2) + (row * dst_width)];
      dst_buf[(row * dst_width) + col + 2] =
          src_buf[((dst_width - 1) - col - 1) + (row * dst_width)];
      dst_buf[(row * dst_width) + col + 3] =
          src_buf[((dst_width - 1) - col) + (row * dst_width)];
    }
  }
}

static void
gst_stdisplaysink_rotate_90_degree (GstSTV4l2Sink * v4l2sink, guchar * dst_buf,
    guchar * src_buf)
{
  gint dst_width = v4l2sink->dst_width;
  gint src_height = v4l2sink->dst_height;
  gint row = 0, col = 0;

  for (row = 0; row < src_height; row++) {
    for (col = 0; col < dst_width; col += 4) {
      dst_buf[((col >> 2) + 1) * (src_height << 2) - 4 - (row << 2)] =
          src_buf[(row * dst_width) + col];
      dst_buf[((col >> 2) + 1) * (src_height << 2) - 3 - (row << 2)] =
          src_buf[(row * dst_width) + col + 1];
      dst_buf[((col >> 2) + 1) * (src_height << 2) - 2 - (row << 2)] =
          src_buf[(row * dst_width) + col + 2];
      dst_buf[((col >> 2) + 1) * (src_height << 2) - 1 - (row << 2)] =
          src_buf[(row * dst_width) + col + 3];
    }
  }
}

static void
gst_stdisplaysink_rotate_180_degree (GstSTV4l2Sink * v4l2sink, guchar * dst_buf,
    guchar * src_buf)
{
  gint dst_width = v4l2sink->dst_width;
  gint src_height = v4l2sink->dst_height;
  gint row = 0, col = 0;

  for (row = 0; row < src_height; row++) {
    for (col = 0; col < dst_width; col += 4) {
      dst_buf[(src_height - row) * (dst_width) - col - 4] =
          src_buf[row * dst_width + col + 0];
      dst_buf[(src_height - row) * (dst_width) - col - 3] =
          src_buf[row * dst_width + col + 1];
      dst_buf[(src_height - row) * (dst_width) - col - 2] =
          src_buf[row * dst_width + col + 2];
      dst_buf[(src_height - row) * (dst_width) - col - 1] =
          src_buf[row * dst_width + col + 3];
    }
  }
}

static void
gst_stdisplaysink_rotate_270_degree (GstSTV4l2Sink * v4l2sink, guchar * dst_buf,
    guchar * src_buf)
{
  gint dst_width = v4l2sink->dst_width;
  gint src_height = v4l2sink->dst_height;
  gint row = 0, col = 0;

  for (row = 0; row < src_height; row++) {
    for (col = dst_width; col > 0; col -= 4) {
      dst_buf[(((dst_width - col) >> 2) * (src_height << 2)) +
          (row << 2)] = src_buf[(row * dst_width) + col - 4];
      dst_buf[(((dst_width - col) >> 2) * (src_height << 2)) + 1 +
          (row << 2)] = src_buf[(row * dst_width) + col - 3];
      dst_buf[(((dst_width - col) >> 2) * (src_height << 2)) + 2 +
          (row << 2)] = src_buf[(row * dst_width) + col - 2];
      dst_buf[(((dst_width - col) >> 2) * (src_height << 2)) + 3 +
          (row << 2)] = src_buf[(row * dst_width) + col - 1];
    }
  }
}

static void
gst_stdisplaysink_rotate (GstSTV4l2Sink * v4l2sink, guchar * dst_buf,
    guchar * src_buf, struct v4l2_buffer *dqbuffer)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  guint32 rotate_degree = ((v4l2sink->rotate_degree) % 360);

  v4l2sink->dst_width = v4l2object->bytes_per_line;
  v4l2sink->dst_height = v4l2object->height;

  if ((v4l2object->height != v4l2object->width) && (rotate_degree == 90
          || rotate_degree == 270)) {
    gint temp = v4l2object->DAR_n;
    v4l2object->DAR_n = v4l2object->DAR_d;
    v4l2object->DAR_d = temp;
    gst_stdisplaysink_reset (v4l2sink, v4l2object->height,
        v4l2object->width, v4l2sink->prev_fourcc);

    if (!gst_stdisplaysink_dequeue_buffer (v4l2sink, dqbuffer)) {
      GST_WARNING_OBJECT (v4l2sink, "No transform buffer available");
      return;
    }
    dst_buf = v4l2sink->bufferdata[dqbuffer->index];
  }

  switch (rotate_degree) {
    case 90:
      gst_stdisplaysink_rotate_90_degree (v4l2sink, dst_buf, src_buf);
      break;
    case 180:
      gst_stdisplaysink_rotate_180_degree (v4l2sink, dst_buf, src_buf);
      break;
    case 270:
      gst_stdisplaysink_rotate_270_degree (v4l2sink, dst_buf, src_buf);
      break;
    default:
      memcpy (dst_buf, src_buf, v4l2object->image_size);
      break;
  }
}


static void
gststv4l2sink_set_transform (GstSTV4l2Sink * v4l2sink, guint16 req)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;

  if (!v4l2sink->fake_mimetype && !v4l2sink->is_ancillary) {
    struct v4l2_buffer dqbuffer;
    guchar *dst_buf = NULL;
    guchar *src_buf = NULL;
    gint imagesize = v4l2object->image_size;

    memset (&dqbuffer, 0, sizeof (dqbuffer));

    /* get the source buffer data */
    src_buf = (guchar *) g_malloc0 (imagesize);
    if (src_buf == NULL) {
      GST_WARNING_OBJECT (v4l2sink, "No memory available for source buffer");
      return;
    }
    memcpy (src_buf, v4l2sink->bufferdata[v4l2object->queued_buffer_index],
        imagesize);

    if (!gst_stdisplaysink_dequeue_buffer (v4l2sink, &dqbuffer)) {
      GST_WARNING_OBJECT (v4l2sink, "No transform buffer available");
      g_free (src_buf);
      return;
    }
    dst_buf = v4l2sink->bufferdata[dqbuffer.index];

    switch (req) {
      case GST_STDISPLAYSINK_REQ_FLIP_HORIZONTAL:
        gst_stdisplaysink_flip_horizontal (v4l2sink, dst_buf, src_buf);
        break;
      case GST_STDISPLAYSINK_REQ_FLIP_VERTICAL:
        gst_stdisplaysink_flip_vertical (v4l2sink, dst_buf, src_buf);
        break;
      case GST_STDISPLAYSINK_REQ_ROTATE:
        gst_stdisplaysink_rotate (v4l2sink, dst_buf, src_buf, &dqbuffer);
        break;
      default:
        memcpy (dst_buf, src_buf, v4l2object->image_size);
        break;
    }

    /* Queue buffer */
    gst_stdisplaysink_enqueue_buffer (v4l2sink, &dqbuffer);
    v4l2object->queued_buffer_index = dqbuffer.index;
    g_free (src_buf);
  }
}


static gboolean
gststv4l2sink_set_iowindows (GstSTV4l2Sink * v4l2sink, guint32 new_req,
    guint plane_type)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  struct v4l2_crop crop;
  struct v4l2_format format;
  struct v4l2_control control;

  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);

  if (plane_type == VIDEO_PLANE) {
    /* Set the Video Plane */
    if (!gst_stdisplaysink_set_input_video_plane (v4l2sink)) {
      GST_ERROR_OBJECT (v4l2sink, "unable to set input video plane");
      return FALSE;
    }
  } else {
    /* Set the Display Plane */
    if (!gst_st_v4l2_set_display_plane (v4l2object)) {
      GST_ERROR_OBJECT (v4l2sink, "unable to set display plane");
      return FALSE;
    }
  }

  memset (&crop, 0, sizeof (struct v4l2_crop));
  memset (&format, 0, sizeof (struct v4l2_format));
  memset (&control, 0, sizeof (struct v4l2_control));

  switch (new_req) {
    case GST_STDISPLAYSINK_REQ_SRC_RECT:
      /* set cropping parameters */
      crop.c.left = v4l2sink->input_rect.x_pos;
      crop.c.top = v4l2sink->input_rect.y_pos;
      crop.c.width = v4l2sink->input_rect.width;
      crop.c.height = v4l2sink->input_rect.height;
      if (plane_type == VIDEO_PLANE) {
        /* Switch input window mode to manual */
        control.id = V4L2_CID_STM_INPUT_WINDOW_MODE;
        control.value = VCSPLANE_MANUAL_MODE;
        if (v4l2_ioctl (v4l2object->video_fd_overlay, VIDIOC_S_CTRL,
                &control) != 0) {
          GST_ERROR_OBJECT (v4l2sink,
              "VIDIOC_S_CTRL(V4L2_CID_STM_INPUT_WINDOW_MODE) failed (%s).\n",
              strerror (errno));
          return FALSE;
        }
        v4l2object->input_plane_mode = VCSPLANE_MANUAL_MODE;

        /* set crop */
        GST_DEBUG_OBJECT (v4l2sink,
            "GST_STDISPLAYSINK_REQ_SRC_RECT: VIDEO_PLANE video_fd_overlay %d, x_pos %d, y_pos %d, width %u, height %u",
            v4l2object->video_fd_overlay, crop.c.left, crop.c.top, crop.c.width,
            crop.c.height);
        crop.type = V4L2_BUF_TYPE_VIDEO_OVERLAY;
        if (v4l2_ioctl (v4l2object->video_fd_overlay, VIDIOC_S_CROP, &crop) < 0) {
          GST_ERROR_OBJECT (v4l2sink, "VIDIOC_S_CROP failed - %s",
              strerror (errno));
          return FALSE;
        }
      } else {
        crop.type = V4L2_BUF_TYPE_PRIVATE;

        /* set crop */
        GST_DEBUG_OBJECT (v4l2sink,
            "GST_STDISPLAYSINK_REQ_SRC_RECT: GRAPHICS_PLANE video_fd_output %d, x_pos %d, y_pos %d, width %u, height %u",
            v4l2object->video_fd_output, crop.c.left, crop.c.top, crop.c.width,
            crop.c.height);
        if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_S_CROP, &crop) < 0) {
          GST_ERROR_OBJECT (v4l2sink, "VIDIOC_S_CROP failed - %s",
              strerror (errno));
          return FALSE;
        }
      }
      break;
    case GST_STDISPLAYSINK_REQ_DST_RECT:
      /* set scaling parameters */
      if (plane_type == VIDEO_PLANE) {
        /* Switch output window mode to manual */
        control.id = V4L2_CID_STM_OUTPUT_WINDOW_MODE;
        control.value = VCSPLANE_MANUAL_MODE;
        if (v4l2_ioctl (v4l2object->video_fd_overlay, VIDIOC_S_CTRL,
                &control) != 0) {
          GST_ERROR_OBJECT (v4l2sink,
              "VIDIOC_S_CTRL(V4L2_CID_STM_OUTPUT_WINDOW_MODE) failed (%s).\n",
              strerror (errno));
          return FALSE;
        }
        v4l2object->output_plane_mode = VCSPLANE_MANUAL_MODE;

        /* get current format */
        format.type = V4L2_BUF_TYPE_VIDEO_OVERLAY;
        if (v4l2_ioctl (v4l2object->video_fd_overlay, VIDIOC_G_FMT,
                &format) < 0) {
          GST_WARNING_OBJECT (v4l2sink, "VIDIOC_G_FMT failed - %s",
              strerror (errno));
        }

        format.fmt.win.w.left = v4l2sink->output_rect.x_pos;
        format.fmt.win.w.top = v4l2sink->output_rect.y_pos;
        format.fmt.win.w.width = v4l2sink->output_rect.width;
        format.fmt.win.w.height = v4l2sink->output_rect.height;

        /* set scale */
        GST_DEBUG_OBJECT (v4l2sink,
            "GST_STDISPLAYSINK_REQ_DST_RECT: VIDEO_PLANE video_fd_overlay %d, x_pos %d, y_pos %d, width %u, height %u",
            v4l2object->video_fd_overlay, format.fmt.win.w.left,
            format.fmt.win.w.top, format.fmt.win.w.width,
            format.fmt.win.w.height);
        if (v4l2_ioctl (v4l2object->video_fd_overlay, VIDIOC_S_FMT,
                &format) < 0) {
          GST_ERROR_OBJECT (v4l2sink, "VIDIOC_S_FMT failed - %s",
              strerror (errno));
          return FALSE;
        }
      } else {
        crop.c.left = v4l2sink->output_rect.x_pos;
        crop.c.top = v4l2sink->output_rect.y_pos;
        crop.c.width = v4l2sink->output_rect.width;
        crop.c.height = v4l2sink->output_rect.height;
        crop.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

        /* set scale */
        GST_DEBUG_OBJECT (v4l2sink,
            "GST_STDISPLAYSINK_REQ_DST_RECT: GRAPHICS_PLANE video_fd_output %d, x_pos %d, y_pos %d, width %u, height %u",
            v4l2object->video_fd_output, crop.c.left, crop.c.top, crop.c.width,
            crop.c.height);
        if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_S_CROP, &crop) < 0) {
          GST_ERROR_OBJECT (v4l2sink, "VIDIOC_S_CROP failed - %s",
              strerror (errno));
          return FALSE;
        }
      }
      break;
    default:
      break;
  }

  if (plane_type == GRAPHICS_PLANE) {
    if (v4l2sink->num_buffers < PROP_DEF_QUEUE_SIZE) {
      GST_WARNING_OBJECT (v4l2sink, "No transform buffer available");
      return FALSE;
    }

    /* Redraw picture with new format */
    if (gststv4l2sink_graphics_swap_and_refresh (v4l2sink) == FALSE) {
      GST_ERROR_OBJECT (v4l2sink, "Failed to refresh picture display");
      return FALSE;
    }
  }

  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return TRUE;
}

static gboolean
gststv4l2sink_set_input_window (GstSTV4l2Sink * v4l2sink)
{
  if (v4l2sink->fake_mimetype || v4l2sink->is_ancillary) {
    if (!gststv4l2sink_set_iowindows (v4l2sink, GST_STDISPLAYSINK_REQ_SRC_RECT,
            VIDEO_PLANE)) {
      return FALSE;
    }
  } else {
    if (!gststv4l2sink_set_iowindows (v4l2sink, GST_STDISPLAYSINK_REQ_SRC_RECT,
            GRAPHICS_PLANE)) {
      return FALSE;
    }
  }
  v4l2sink->input_rect.enabled = TRUE;

  return TRUE;
}

static gboolean
gststv4l2sink_set_output_window (GstSTV4l2Sink * v4l2sink)
{
  if (v4l2sink->fake_mimetype) {
    if (!gststv4l2sink_set_iowindows (v4l2sink, GST_STDISPLAYSINK_REQ_DST_RECT,
            VIDEO_PLANE)) {
      return FALSE;
    }
  } else if (v4l2sink->is_ancillary) {
    /* Do scaling on video plane */
    if (!gststv4l2sink_set_iowindows (v4l2sink, GST_STDISPLAYSINK_REQ_DST_RECT,
            VIDEO_PLANE)) {
      return FALSE;
    }
    /* wait for one frame to refresh vibe (VIBE bz 28767) */
    usleep (50 * 1000);
    /* resize graphics plane based on video plane */
    if (!gst_stdisplaysink_resize_graphics_plane (v4l2sink)) {
      GST_ERROR_OBJECT (v4l2sink, "Failed to resize GFX window");
      return FALSE;
    }
  } else {
    if (!gststv4l2sink_set_iowindows (v4l2sink, GST_STDISPLAYSINK_REQ_DST_RECT,
            GRAPHICS_PLANE)) {
      return FALSE;
    }
  }
  v4l2sink->output_rect.enabled = TRUE;

  return TRUE;
}

/**
 * This function allows to refresh the picture currently displayed:
 * It swaps the currently queued V4L2 buffer, in order to get back the
 * buffer data corresponding to the picture that is currently displayed.
 * => This avoids a memcpy for just refresh
 */
static gboolean
gststv4l2sink_graphics_swap_and_refresh (GstSTV4l2Sink * v4l2sink)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  guint32 transition_transparency = 0;
  struct v4l2_buffer dqbuffer;

  /*
   * SWAP V4L2 BUFFERS (to avoid memcpy later)
   */

  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);

  /* Set full transparency to avoid garbage to be visible on screen during swap */
  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_S_OUTPUT_ALPHA,
          &transition_transparency) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_S_OUTPUT_ALPHA failed - %s",
        strerror (errno));
    return FALSE;
  }

  dqbuffer.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  dqbuffer.memory = V4L2_MEMORY_MMAP;
  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_DQBUF, &dqbuffer) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_DQBUF failed - %s", strerror (errno));
    return FALSE;
  }
  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_QBUF,
          &v4l2sink->buffer[dqbuffer.index]) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_QBUF failed - %s", strerror (errno));
    return FALSE;
  }

  /*  Restore previous transparency back */
  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_S_OUTPUT_ALPHA,
          &v4l2sink->graphics_alpha) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_S_OUTPUT_ALPHA failed - %s",
        strerror (errno));
    return FALSE;
  }

  /*
   * REFRESH GRAPHIC => Pushing same image again
   */

  dqbuffer.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  dqbuffer.memory = V4L2_MEMORY_MMAP;
  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_DQBUF, &dqbuffer) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_DQBUF failed - %s", strerror (errno));
    return FALSE;
  }
  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_QBUF,
          &v4l2sink->buffer[dqbuffer.index]) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_QBUF failed - %s", strerror (errno));
    return FALSE;
  }
  v4l2object->queued_buffer_index = dqbuffer.index;

  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return TRUE;
}

static gboolean
gststv4l2sink_graphics_set_transparency (GstSTV4l2Sink * v4l2sink)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;

  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);

  if (!v4l2sink->fake_mimetype) {
    /* Set requested transparency */
    if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_S_OUTPUT_ALPHA,
            &v4l2sink->graphics_alpha) < 0) {
      GST_ERROR_OBJECT (v4l2sink, "VIDIOC_S_OUTPUT_ALPHA failed - %s",
          strerror (errno));
      return FALSE;
    }
    /* No need to refresh now for ancillary, as it will be done for next ancillary */
    if (!v4l2sink->is_ancillary) {
      /* Redraw picture with new transparency */
      if (gststv4l2sink_graphics_swap_and_refresh (v4l2sink) == FALSE) {
        GST_ERROR_OBJECT (v4l2sink, "Failed to refresh picture display");
        return FALSE;
      }
    }
  }

  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return TRUE;
}

static void
gst_stdisplaysink_set_video_mute_unmute (GstSTV4l2Sink * v4l2sink)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  struct v4l2_control control;

  /* for video, as per stlinuxtv, 0 is for Mute and 1 for Show so
     need to reverse value provided by user since for user 1
     is for Mute and 0 for Show
   */

  control.id = V4L2_CID_MPEG_STM_VIDEO_DECODER_BLANK;
  control.value = !(v4l2sink->mute);

  if (ioctl (v4l2object->video_fd_overlay, VIDIOC_S_CTRL, &control) != 0) {
    GST_WARNING_OBJECT (v4l2sink, "Failed to set video to %s",
        (v4l2sink->mute ? "mute" : "unmute"));
  }

  return;
}

static gboolean
gststv4l2sink_handle_aspect_ratio_conversion (GstSTV4l2Sink * v4l2sink,
    guint32 new_req, guint plane_type)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  struct v4l2_control control;

  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);

  if (plane_type == VIDEO_PLANE) {
    /* Set the Video Plane */
    if (!gst_stdisplaysink_set_input_video_plane (v4l2sink)) {
      GST_ERROR_OBJECT (v4l2sink, "unable to set input video plane");
      return FALSE;
    }
  } else {
    /* Set the Graphics Plane */
    if (!gst_st_v4l2_set_display_plane (v4l2object)) {
      GST_ERROR_OBJECT (v4l2sink, "unable to set display plane");
      return FALSE;
    }
  }

  memset (&control, 0, sizeof (struct v4l2_control));

  if (new_req == GST_STDISPLAYSINK_REQ_ARC_MODE) {
    control.id = V4L2_CID_STM_ASPECT_RATIO_CONV_MODE;
    control.value = v4l2sink->arc_mode;
  } else {
    control.id = V4L2_CID_DV_STM_TX_ASPECT_RATIO;
    control.value = v4l2sink->display_aspect_ratio;
  }

  if (v4l2_ioctl (v4l2object->video_fd_overlay, VIDIOC_S_CTRL, &control) != 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_S_CTRL failed - %s", strerror (errno));
    return FALSE;
  }

  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return TRUE;
}

static gboolean
gststv4l2sink_set_aspect_ratio (GstSTV4l2Sink * v4l2sink, guint32 new_req)
{
  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);

  if (v4l2sink->fake_mimetype) {
    if (!gststv4l2sink_handle_aspect_ratio_conversion (v4l2sink, new_req,
            VIDEO_PLANE)) {
      GST_ERROR_OBJECT (v4l2sink, "could not set aspect ration on video plane");
      return FALSE;
    }
  } else if (v4l2sink->is_ancillary) {
    /* Do AR conversion on video plane */
    if (!gststv4l2sink_handle_aspect_ratio_conversion (v4l2sink, new_req,
            VIDEO_PLANE)) {
      GST_ERROR_OBJECT (v4l2sink, "could not set aspect ration on video plane");
      return FALSE;
    }
    /* Do AR conversion on graphics plane */
    if (!gststv4l2sink_handle_aspect_ratio_conversion (v4l2sink, new_req,
            GRAPHICS_PLANE)) {
      GST_ERROR_OBJECT (v4l2sink,
          "could not set aspect ration on graphics plane");
      return FALSE;
    }
  } else {
    if (!gststv4l2sink_handle_aspect_ratio_conversion (v4l2sink, new_req,
            GRAPHICS_PLANE)) {
      GST_ERROR_OBJECT (v4l2sink,
          "could not set aspect ration on graphics plane");
      return FALSE;
    }
  }

  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return TRUE;
}

static void
gst_stdisplaysink_get_input_rect_params (GstSTV4l2Sink * v4l2sink,
    const GValue * value)
{
  gchar **params;
  gchar **arg;
  guint num_params, num_arg, i;
  gint rect_x = 0;
  gint rect_y = 0;
  gint rect_width = 0;
  gint rect_height = 0;

  params = g_strsplit (g_value_get_string (value), "/", 0);
  num_params = g_strv_length (params);

  for (i = 0; i < num_params; i++) {
    arg = g_strsplit (params[i], ":", 0);
    num_arg = g_strv_length (arg);
    if (num_arg == 2) {
      if (strcmp (arg[0], "x") == 0) {
        rect_x = strtol (arg[1], NULL, 0);
      }
      if (strcmp (arg[0], "y") == 0) {
        rect_y = strtol (arg[1], NULL, 0);
      }
      if (strcmp (arg[0], "w") == 0) {
        rect_width = strtol (arg[1], NULL, 0);
      }
      if (strcmp (arg[0], "h") == 0) {
        rect_height = strtol (arg[1], NULL, 0);
      }
    }
    g_strfreev (arg);
  }
  g_strfreev (params);

  GST_DEBUG_OBJECT (v4l2sink,
      "rect_x %d, rect_y %d, rect_width %u, rect_height %u", rect_x, rect_y,
      rect_width, rect_height);
  v4l2sink->input_rect.x_pos = rect_x;
  v4l2sink->input_rect.y_pos = rect_y;
  v4l2sink->input_rect.width = rect_width;
  v4l2sink->input_rect.height = rect_height;
  v4l2sink->input_rect.enabled = FALSE;

}

static void
gst_stdisplaysink_get_output_rect_params (GstSTV4l2Sink * v4l2sink,
    const GValue * value)
{
  gchar **params;
  gchar **arg;
  guint num_params, num_arg, i;
  gint rect_x = 0;
  gint rect_y = 0;
  gint rect_width = 0;
  gint rect_height = 0;

  params = g_strsplit (g_value_get_string (value), "/", 0);
  num_params = g_strv_length (params);
  for (i = 0; i < num_params; i++) {
    arg = g_strsplit (params[i], ":", 0);
    num_arg = g_strv_length (arg);
    if (num_arg == 2) {
      if (strcmp (arg[0], "x") == 0) {
        rect_x = strtol (arg[1], NULL, 0);
      }
      if (strcmp (arg[0], "y") == 0) {
        rect_y = strtol (arg[1], NULL, 0);
      }
      if (strcmp (arg[0], "w") == 0) {
        rect_width = strtol (arg[1], NULL, 0);
      }
      if (strcmp (arg[0], "h") == 0) {
        rect_height = strtol (arg[1], NULL, 0);
      }
    }
    g_strfreev (arg);
  }
  g_strfreev (params);

  GST_DEBUG_OBJECT (v4l2sink,
      "rect_x %d, rect_y %d, rect_width %u, rect_height %u", rect_x, rect_y,
      rect_width, rect_height);
  v4l2sink->output_rect.x_pos = rect_x;
  v4l2sink->output_rect.y_pos = rect_y;
  v4l2sink->output_rect.width = rect_width;
  v4l2sink->output_rect.height = rect_height;
  v4l2sink->output_rect.enabled = FALSE;
}

static void
gst_stdisplaysink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstSTV4l2Sink *v4l2sink = GST_STDISPLAYSINK (object);
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  const gchar *disp_plane;
  guint dev_id;

  switch (prop_id) {
    case PROP_SRC_RECT:
      gst_stdisplaysink_get_input_rect_params (v4l2sink, value);
      v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_SRC_RECT;
      break;
    case PROP_DST_RECT:
      gst_stdisplaysink_get_output_rect_params (v4l2sink, value);
      if (GST_STATE (object) < GST_STATE_PAUSED) {
        v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_DST_RECT;
      } else {
        GST_DEBUG_OBJECT (v4l2sink, "set output window");
        if (!gststv4l2sink_set_output_window (v4l2sink)) {
          GST_ERROR_OBJECT (v4l2sink, "Failed to set output rectangle");
        }
      }
      break;
    case PROP_ROTATE_DEGREE:
      v4l2sink->rotate_degree = g_value_get_uint (value);
      v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_ROTATE;
      break;
    case PROP_FLIP_VERTICAL:
      v4l2sink->vertical_flip = g_value_get_boolean (value);
      v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_FLIP_VERTICAL;
      break;
    case PROP_FLIP_HORIZONTAL:
      v4l2sink->horizontal_flip = g_value_get_boolean (value);
      v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_FLIP_HORIZONTAL;
      break;
    case PROP_PLANE_NAME:
      disp_plane = g_value_get_string (value);
      g_strlcpy (v4l2object->display_plane_name, disp_plane, PLANE_NAME_LENGTH);
      v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_PLANE_NAME;
      break;
    case PROP_CTRL_PLANE_TRANSPARENCY:
      v4l2sink->graphics_alpha = g_value_get_uint (value);
      v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_PLANE_TRANSPARENCY;
      break;
    case PROP_DEV_ID:
      dev_id = g_value_get_uint (value);
      if (dev_id != v4l2object->dev_id) {
        v4l2object->dev_id = dev_id;
        if (GST_STATE (G_OBJECT (v4l2sink)) == GST_STATE_PLAYING) {
          v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_RECONFIG;
        }
      }
      break;
    case PROP_ARC_MODE:
      v4l2sink->arc_mode = g_value_get_uint (value);
      v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_ARC_MODE;
      break;
    case PROP_DISPLAY_ASPECT_RATIO_VALUE:
      v4l2sink->display_aspect_ratio = g_value_get_uint (value);
      v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_DAR;
      break;
    case PROP_MUTE:
      v4l2sink->mute = g_value_get_boolean (value);
      v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_MUTE;
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

  return;
}


static void
gst_stdisplaysink_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  GstSTV4l2Sink *v4l2sink = GST_STDISPLAYSINK (object);
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  struct v4l2_format format;
  struct v4l2_crop crop;
  gchar str[50] = { 0, };

  /* Set the Video Plane */
  if (!gst_stdisplaysink_set_input_video_plane (v4l2sink)) {
    GST_ERROR_OBJECT (v4l2sink, "unable to set input video plane");
  }

  switch (prop_id) {
    case PROP_SRC_RECT:
      /* Set the crop */
      memset (&crop, 0, sizeof (crop));
      crop.type = V4L2_BUF_TYPE_VIDEO_OVERLAY;
      if (v4l2_ioctl (v4l2object->video_fd_overlay, VIDIOC_G_CROP, &crop) < 0) {
        GST_ERROR_OBJECT (v4l2sink, "VIDIOC_G_CROP failed - %s",
            strerror (errno));
      } else {
        v4l2sink->input_rect.width = crop.c.width;
        v4l2sink->input_rect.height = crop.c.height;
        v4l2sink->input_rect.x_pos = crop.c.left;
        v4l2sink->input_rect.y_pos = crop.c.top;
      }
      GST_DEBUG_OBJECT (v4l2sink,
          "get PROP_SRC_RECT: rect_x %d, rect_y %d, rect_width %u, rect_height %u",
          v4l2sink->input_rect.x_pos, v4l2sink->input_rect.y_pos,
          v4l2sink->input_rect.width, v4l2sink->input_rect.height);
      g_snprintf (str, sizeof (str), "x:%d/y:%d/w:%d/h:%d/",
          v4l2sink->input_rect.x_pos, v4l2sink->input_rect.y_pos,
          v4l2sink->input_rect.width, v4l2sink->input_rect.height);
      g_value_set_string (value, str);
      break;
    case PROP_DST_RECT:
      format.type = V4L2_BUF_TYPE_VIDEO_OVERLAY;
      if (v4l2_ioctl (v4l2object->video_fd_overlay, VIDIOC_G_FMT, &format) < 0) {
        GST_ERROR_OBJECT (v4l2sink, "VIDIOC_CROPCAP failed - %s",
            strerror (errno));
      } else {
        v4l2sink->output_rect.x_pos = format.fmt.win.w.left;
        v4l2sink->output_rect.y_pos = format.fmt.win.w.top;
        v4l2sink->output_rect.width = format.fmt.win.w.width;
        v4l2sink->output_rect.height = format.fmt.win.w.height;
      }
      GST_DEBUG_OBJECT (v4l2sink,
          "get PROP_DST_RECT: rect_x %d, rect_y %d, rect_width %u, rect_height %u",
          v4l2sink->output_rect.x_pos, v4l2sink->output_rect.y_pos,
          v4l2sink->output_rect.width, v4l2sink->output_rect.height);
      g_snprintf (str, sizeof (str), "x:%d/y:%d/w:%d/h:%d/",
          v4l2sink->output_rect.x_pos, v4l2sink->output_rect.y_pos,
          v4l2sink->output_rect.width, v4l2sink->output_rect.height);
      g_value_set_string (value, str);
      break;
    case PROP_ROTATE_DEGREE:
      g_value_set_uint (value, v4l2sink->rotate_degree);
      break;
    case PROP_PLANE_NAME:
      g_value_set_string (value, v4l2object->display_plane_name);
      break;
    case PROP_CTRL_PLANE_TRANSPARENCY:
      if (!v4l2sink->fake_mimetype) {
        g_value_set_uint (value, v4l2sink->graphics_alpha);
      }
      break;
    case PROP_DEV_ID:
      g_value_set_uint (value, v4l2object->dev_id);
      break;
    case PROP_ARC_MODE:
      g_value_set_uint (value, v4l2sink->arc_mode);
      break;
    case PROP_DISPLAY_ASPECT_RATIO_VALUE:
      g_value_set_uint (value, v4l2sink->display_aspect_ratio);
      break;
    case PROP_PLANE_SIZE:
      g_snprintf (str, sizeof (str), "x:%d/y:%d/w:%d/h:%d/",
          v4l2object->plane.x_pos, v4l2object->plane.y_pos,
          v4l2object->plane.width, v4l2object->plane.height);
      g_value_set_string (value, str);
      break;
    case PROP_STREAM_DISPLAY_SIZE:
      g_snprintf (str, sizeof (str), "x:%d/y:%d/w:%d/h:%d/",
          v4l2object->stream.x_pos, v4l2object->stream.y_pos,
          v4l2object->stream.width, v4l2object->stream.height);
      g_value_set_string (value, str);
      break;
    case PROP_STREAM_PIXEL_ASPECT_RATIO:
      gst_value_set_fraction (value, v4l2object->PAR_n, v4l2object->PAR_d);
      break;
    case PROP_MUTE:
      g_value_set_boolean (value, v4l2sink->mute);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_stdisplaysink_create_monitor_task (GstSTV4l2Sink * v4l2sink)
{
  /* Create and initialize the auddec task
     This task will be use as multi purpose monitoring */
  g_rec_mutex_init (&v4l2sink->monitor_task_lock);
  v4l2sink->monitor_task =
      gst_task_new ((GstTaskFunction) gst_stdisplaysink_monitor_loop,
      v4l2sink, NULL);
  gst_object_set_name (GST_OBJECT_CAST (v4l2sink->monitor_task),
      "SMF-Disp Monitr");
  gst_task_set_lock (v4l2sink->monitor_task, &v4l2sink->monitor_task_lock);
}

static void
gst_stdisplaysink_stop_monitor_task (GstSTV4l2Sink * v4l2sink)
{
  if (v4l2sink->monitor_task) {
    gst_task_stop (v4l2sink->monitor_task);
    g_rec_mutex_lock (&v4l2sink->monitor_task_lock);
    g_rec_mutex_unlock (&v4l2sink->monitor_task_lock);
    gst_task_join (v4l2sink->monitor_task);
    gst_object_unref (v4l2sink->monitor_task);
    g_rec_mutex_clear (&v4l2sink->monitor_task_lock);
    v4l2sink->monitor_task = NULL;
  }
}

static GstStateChangeReturn
gst_stdisplaysink_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
  GstSTV4l2Sink *v4l2sink = GST_STDISPLAYSINK (element);
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;

  GST_DEBUG_OBJECT (v4l2sink, "> %d -> %d",
      GST_STATE_TRANSITION_CURRENT (transition),
      GST_STATE_TRANSITION_NEXT (transition));

  switch (transition) {
    case GST_STATE_CHANGE_READY_TO_PAUSED:
    {
      GstStructure *structure;
      GstMessage *message;

      gst_stdisplaysink_create_monitor_task (v4l2sink);
      /* Send message to application so that application can set the display plane */
      structure = gst_structure_new_empty ("stdisplaysink");
      message = gst_message_new_element (GST_OBJECT (v4l2sink), structure);
      gst_element_post_message (GST_ELEMENT (v4l2sink), message);
      if (!gststv4l2sink_reset_iowindows (v4l2sink)) {
        GST_WARNING_OBJECT (v4l2sink, "could not reset io windows");
      }
      /* Retrieve the plane info here, otherwise next retrieval will be done in set_caps */
      gst_stdisplaysink_get_output_video_plane_info (v4l2sink,
          &v4l2object->plane);
    }
      break;

    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
    {
      if (!v4l2sink->output_rect.enabled) {
        v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_DST_RECT;
      }
      if (!v4l2sink->input_rect.enabled) {
        v4l2sink->pending_req |= GST_STDISPLAYSINK_REQ_SRC_RECT;
      }
    }
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      gst_stdisplaysink_stop_monitor_task (v4l2sink);
      if (!gststv4l2sink_reset_iowindows (v4l2sink)) {
        GST_WARNING_OBJECT (v4l2sink, "could not reset io windows");
      }
      if (v4l2object->state == STATE_STREAMING) {
        if (!gst_st_v4l2_object_stop_streaming (v4l2object)) {
          GST_ERROR_OBJECT (v4l2sink, "VIDIOC_STREAMOFF failed - %s",
              strerror (errno));
          return GST_STATE_CHANGE_FAILURE;
        }
        gst_stdisplaysink_deallocate (v4l2sink);
        v4l2object->state = STATE_OFF;
      }
      v4l2sink->prev_height = 0;
      v4l2sink->prev_width = 0;
      v4l2sink->prev_fourcc = 0;
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      /* Close the device */
      if (!gst_st_v4l2_close_device (v4l2object)) {
        GST_ERROR_OBJECT (v4l2object->element, "Failed to close device");
        return FALSE;
      }

      if (!gst_st_v4l2_object_stop (v4l2object)) {
        GST_ERROR_OBJECT (v4l2sink, "could not stop object");
        return GST_STATE_CHANGE_FAILURE;
      }
      gst_st_v4l2_object_reset (v4l2object, V4L2_BUF_TYPE_VIDEO_OUTPUT);
      gst_stdisplaysink_reset_params (v4l2sink);
    default:
      break;
  }
  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return ret;
}

static GstCaps *
gst_stdisplaysink_get_caps (GstSTV4l2Sink * v4l2sink, GstCaps * filter)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  GstCaps *caps;

  if (v4l2sink->probed_caps) {
    LOG_CAPS (v4l2sink, v4l2sink->probed_caps);
    return gst_caps_ref (v4l2sink->probed_caps);
  }

  /*Setting up default values for plane test logic in gst_st_v4l2_object_start() */
  v4l2object->width = DEFAULT_ALLOC_WIDTH;
  v4l2object->height = DEFAULT_ALLOC_HEIGHT;
  v4l2object->pixelformat = V4L2_PIX_FMT_NV16;

  if (strcmp (v4l2object->display_plane_name, DEFAULT_PLANE_NAME) == 0) {
    if (v4l2object->dev_id == 0) {
      g_strlcpy (v4l2object->plane_finder, "VID", MAX_PLANE_NAME_SIZE);
    } else {
      g_strlcpy (v4l2object->plane_finder, "PIP", MAX_PLANE_NAME_SIZE);
    }

    GST_DEBUG_OBJECT (v4l2sink, "v4l2 object start plane %s",
        v4l2object->plane_finder);
    if (!gst_st_v4l2_object_start (v4l2object)) {
      caps = gst_st_v4l2_object_get_selective_caps (v4l2object);
      g_strlcpy (v4l2object->plane_finder, "GDP", MAX_PLANE_NAME_SIZE);
    } else {
      if (!gst_st_v4l2_object_stop_streaming (v4l2object)) {
        GST_LOG_OBJECT (v4l2sink, "VIDIOC_STREAMOFF failed - %s",
            strerror (errno));
      }
      munmap (v4l2object->first_bufferdata, v4l2object->first_buffer.length);
      v4l2object->first_bufferdata = NULL;

      if (!gst_st_v4l2_object_stop (v4l2object)) {
        GST_ERROR_OBJECT (v4l2sink, "unable stop object \n");
      }
      if (v4l2object->dev_id == 0) {
        g_strlcpy (v4l2object->plane_finder, "VID", MAX_PLANE_NAME_SIZE);
      } else {
        g_strlcpy (v4l2object->plane_finder, "PIP", MAX_PLANE_NAME_SIZE);
      }

      caps = gst_st_v4l2_object_get_all_caps (v4l2object);
    }
    g_strlcpy (v4l2object->display_plane_name, DEFAULT_PLANE_NAME,
        PLANE_NAME_LENGTH);
  } else {
    if (strstr (v4l2object->display_plane_name, "VID") != 0) {
      caps = gst_st_v4l2_object_get_all_caps (v4l2object);
    } else {
      caps = gst_st_v4l2_object_get_selective_caps (v4l2object);
    }
  }
  /* Resetting the modified parameters */
  v4l2object->width = 0;
  v4l2object->height = 0;
  v4l2object->pixelformat = 0;

  v4l2sink->probed_caps = gst_caps_ref (caps);
  if (filter) {
    caps =
        gst_caps_intersect_full (filter, v4l2sink->probed_caps,
        GST_CAPS_INTERSECT_FIRST);
    gst_caps_unref (v4l2sink->probed_caps);
    v4l2sink->probed_caps = gst_caps_ref (caps);
  }
  LOG_CAPS (v4l2sink, caps);
  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return caps;
}

static void
gst_stdisplaysink_deallocate (GstSTV4l2Sink * v4l2sink)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  guint i;

  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);

  if (v4l2object->first_bufferdata != NULL) {
    munmap (v4l2object->first_bufferdata, v4l2object->first_buffer.length);
    v4l2object->first_bufferdata = NULL;
  }

  for (i = 0; i < v4l2sink->num_buffers; i++) {
    if (v4l2sink->bufferdata[i] != NULL) {
      munmap (v4l2sink->bufferdata[i], v4l2sink->buffer[i].length);
      v4l2sink->bufferdata[i] = NULL;
    }
  }

  v4l2sink->buffer_allocated = FALSE;

  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return;
}

static gboolean
gst_stdisplaysink_allocate (GstSTV4l2Sink * v4l2sink)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  struct v4l2_requestbuffers breq;
  gint requested, granted;
  guint i;

  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);

  /* Request Buffers from V4l2 */
  memset (&breq, 0, sizeof (struct v4l2_requestbuffers));
  breq.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  breq.count = v4l2sink->num_buffers;
  breq.memory = V4L2_MEMORY_MMAP;

  requested = breq.count;
  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_REQBUFS, &breq) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_REQBUFS failed - %s", strerror (errno));
    return FALSE;
  } else {
    granted = breq.count;
    if (granted != requested) {
      GST_WARNING_OBJECT (v4l2sink, "request %d buffer granted %d", requested,
          granted);
      v4l2sink->num_buffers = granted;
    }
  }

  /* Query Buffers and map buffers */
  for (i = 0; i < v4l2sink->num_buffers; i++) {
    v4l2sink->buffer[i].index = i;
    v4l2sink->buffer[i].type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

    /* query buffer */
    if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_QUERYBUF,
            &v4l2sink->buffer[i]) < 0) {
      GST_ERROR_OBJECT (v4l2sink, "VIDIOC_QUERYBUF failed - %s",
          strerror (errno));
      return FALSE;
    }

    v4l2sink->buffer[i].field = V4L2_FIELD_NONE;
    v4l2sink->bufferdata[i] =
        (guchar *) mmap (0, v4l2sink->buffer[i].length, PROT_READ | PROT_WRITE,
        MAP_SHARED, v4l2object->video_fd_output, v4l2sink->buffer[i].m.offset);

    if (v4l2sink->bufferdata[i] == ((void *) -1)) {
      GST_ERROR_OBJECT (v4l2sink, "mmap failed - %s", strerror (errno));
      return FALSE;
    }
    memset (v4l2sink->bufferdata[i], 0, v4l2sink->buffer[i].length);

    GST_DEBUG_OBJECT (v4l2sink, "buffer mmaped: buffer length=%d",
        v4l2sink->buffer[i].length);
  }

  for (i = 0; i < v4l2sink->num_buffers; i++) {
    /* Queue buffer */
    if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_QBUF,
            &v4l2sink->buffer[i]) < 0) {
      GST_ERROR_OBJECT (v4l2sink, "VIDIOC_QBUF failed - %s", strerror (errno));
      return FALSE;
    }
  }

  v4l2sink->buffer_allocated = TRUE;

  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return TRUE;
}

static gboolean
gststv4l2sink_reset_iowindows (GstSTV4l2Sink * v4l2sink)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  struct v4l2_control control;

  memset (&control, 0, sizeof (struct v4l2_control));

  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);
  /* Set the Video Plane */
  if (!gst_stdisplaysink_set_input_video_plane (v4l2sink)) {
    GST_ERROR_OBJECT (v4l2sink, "unable to set input video plane");
    return FALSE;
  }

  /* Switch input window mode to auto */
  if (v4l2object->input_plane_mode == VCSPLANE_MANUAL_MODE) {
    /* Switch input window mode to auto */
    control.id = V4L2_CID_STM_INPUT_WINDOW_MODE;
    control.value = VCSPLANE_AUTO_MODE;
    if (v4l2_ioctl (v4l2object->video_fd_overlay, VIDIOC_S_CTRL, &control) != 0) {
      GST_ERROR_OBJECT (v4l2sink,
          "VIDIOC_S_CTRL(V4L2_CID_STM_INPUT_WINDOW_MODE) failed (%s).\n",
          strerror (errno));
      return FALSE;
    }
    v4l2object->input_plane_mode = VCSPLANE_AUTO_MODE;
  }

  /* Switch output window mode to auto */
  if (v4l2object->output_plane_mode == VCSPLANE_MANUAL_MODE) {
    /* Switch output window mode to auto */
    control.id = V4L2_CID_STM_OUTPUT_WINDOW_MODE;
    control.value = VCSPLANE_AUTO_MODE;
    if (v4l2_ioctl (v4l2object->video_fd_overlay, VIDIOC_S_CTRL, &control) != 0) {
      GST_ERROR_OBJECT (v4l2sink,
          "VIDIOC_S_CTRL(V4L2_CID_STM_OUTPUT_WINDOW_MODE) failed (%s).\n",
          strerror (errno));
      return FALSE;
    }
    v4l2object->output_plane_mode = VCSPLANE_AUTO_MODE;
  }
  return TRUE;
}

static gboolean
gst_stdisplaysink_resize_graphics_plane (GstSTV4l2Sink * v4l2sink)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  struct v4l2_format format;

  /* Set the Video Plane */
  if (!gst_stdisplaysink_set_input_video_plane (v4l2sink)) {
    GST_DEBUG_OBJECT (v4l2sink, "unable to set input video plane");
    return FALSE;
  }

  /* in case of subtitles, GDP should be aligned with VDP but if
     there is no subtitle but a jpeg image in pip, its dimentions
     should be specified by application itself because there is no
     video to align with but only to resize GDP as app wants
   */

  if (v4l2sink->is_ancillary) {
    /* its ancillary data */
    /* get the dimensions of currently displayed video plane */
    format.type = V4L2_BUF_TYPE_VIDEO_OVERLAY;
    if (v4l2_ioctl (v4l2object->video_fd_overlay, VIDIOC_G_FMT, &format) < 0) {
      GST_DEBUG_OBJECT (v4l2sink, "VIDIOC_G_FMT failed - %s", strerror (errno));
      return FALSE;
    }

    /* resize graphics plane to the displayed video plane */
    v4l2sink->output_rect.x_pos = format.fmt.win.w.left;
    v4l2sink->output_rect.y_pos = format.fmt.win.w.top;
    v4l2sink->output_rect.width = format.fmt.win.w.width;
    v4l2sink->output_rect.height = format.fmt.win.w.height;
    GST_DEBUG_OBJECT (v4l2sink,
        "resize graphics plane rect_x %d, rect_y %d, rect_width %u, rect_height %u",
        v4l2sink->output_rect.x_pos, v4l2sink->output_rect.y_pos,
        v4l2sink->output_rect.width, v4l2sink->output_rect.height);
  } else {
    GST_DEBUG_OBJECT (v4l2sink, "set aspect ratio");
    if (!gst_st_v4l2_set_aspect_ratio (v4l2object, v4l2object->DAR_n,
            v4l2object->DAR_d)) {
      /* Error already posted */
      return FALSE;
    }
    return TRUE;
  }

  /* resize graphics plane */
  if (!gststv4l2sink_set_iowindows (v4l2sink, GST_STDISPLAYSINK_REQ_DST_RECT,
          GRAPHICS_PLANE)) {
    GST_DEBUG_OBJECT (v4l2sink, "Failed to resize GFX PIP window");
    return FALSE;
  }
  v4l2sink->output_rect.enabled = TRUE;

  return TRUE;
}

static gboolean
gst_stdisplaysink_reset (GstSTV4l2Sink * v4l2sink, guint w, guint h,
    guint32 fourcc)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;

  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);

  GST_DEBUG_OBJECT (v4l2sink, "width %d, height %d, fourcc %d", w, h, fourcc);
  /* streaming is started so the buffer must be allocated. then stop streaming and deallocate the buffer */
  if (v4l2object->state == STATE_STREAMING) {
    if (!gst_st_v4l2_object_stop_streaming (v4l2object)) {
      GST_ERROR_OBJECT (v4l2sink, "VIDIOC_STREAMOFF failed - %s",
          strerror (errno));
      return FALSE;
    }

    v4l2object->state = STATE_OFF;
    gst_stdisplaysink_deallocate (v4l2sink);
  } else if (v4l2sink->buffer_allocated == TRUE) {
    /* streaming not started but buffer allocated so deallocate buffer only */
    gst_stdisplaysink_deallocate (v4l2sink);
  }

  if ((!w) && (!h) && (!fourcc)) {
    /* Close the device */
    if (!gst_st_v4l2_object_stop (v4l2object)) {
      GST_ERROR_OBJECT (v4l2sink, "could not stop object");
      return GST_STATE_CHANGE_FAILURE;
    }
    v4l2sink->prev_width = 0;
    v4l2sink->prev_height = 0;
    v4l2sink->prev_fourcc = 0;
    g_strlcpy (v4l2object->display_plane_name, DEFAULT_PLANE_NAME,
        PLANE_NAME_LENGTH);
    GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
    return TRUE;
  }

  /* store new parameters and set new format */
  if (v4l2object->width != w || v4l2object->height != h
      || v4l2object->pixelformat != fourcc) {
    v4l2object->width = w;
    v4l2object->height = h;
    v4l2object->pixelformat = fourcc;

    if (!gst_st_v4l2_object_set_format (v4l2object)) {
      /* Error already posted */
      GST_ERROR_OBJECT (v4l2sink, "Failed to set format");
      return FALSE;
    }
  }

  /* allocate new buffer to render if upstream element is not stvideo */
  /* because stvideo send fake buffer and no need to render */
  if ((v4l2sink->fake_mimetype == FALSE)
      && (v4l2sink->buffer_allocated == FALSE)) {
    /* allocate buffer */
    if (!gst_stdisplaysink_allocate (v4l2sink)) {
      gst_stdisplaysink_deallocate (v4l2sink);
      GST_ERROR_OBJECT (v4l2sink, "Failed to allocate buffer");
      return FALSE;
    }

    /* start streaming */
    if (!gst_st_v4l2_object_start_streaming (v4l2object)) {
      GST_ERROR_OBJECT (v4l2object->element, "VIDIOC_STREAMON failed - %s",
          strerror (errno));
      return FALSE;
    }

    /* set transparency */
    if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_S_OUTPUT_ALPHA,
            &v4l2sink->graphics_alpha) < 0) {
      GST_ERROR_OBJECT (v4l2sink, "VIDIOC_S_OUTPUT_ALPHA failed");
      return FALSE;
    }

    /* resize graphics plane based on video plane */
    if (!gst_stdisplaysink_resize_graphics_plane (v4l2sink)) {
      GST_WARNING_OBJECT (v4l2sink, "Failed to resize GFX window");
    }

    v4l2object->state = STATE_STREAMING;
    /* set the plane order */
    gst_st_v4l2_set_plane_order (v4l2object);
  }

  v4l2sink->prev_width = v4l2object->width;
  v4l2sink->prev_height = v4l2object->height;
  v4l2sink->prev_fourcc = fourcc;

  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return TRUE;
}

static gboolean
gst_stdisplaysink_set_caps (GstSTV4l2Sink * v4l2sink, GstCaps * caps)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  gint w = 0, h = 0;
  guint32 fourcc = 0;
  guint fps_n, fps_d;
  gboolean fake_mimetype = FALSE;
  gboolean is_upstream_ancillary = FALSE;
  gboolean release_plane = FALSE;
  gboolean ret = TRUE;

  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);
  LOG_CAPS (v4l2sink, caps);

  /* We wanted our own v4l2 type of fourcc codes */
  if (!gst_st_v4l2_object_get_caps_info (v4l2object, caps, &fourcc, &w, &h,
          &fps_n, &fps_d, &fake_mimetype, &v4l2sink->is_ancillary,
          &is_upstream_ancillary, &release_plane)) {
    GST_DEBUG_OBJECT (v4l2sink, "Failed to get capture format from caps %p",
        caps);
    ret = FALSE;
    goto exit;
  }

  /* only run once to start the v4l2 device */
  if (!v4l2sink->current_caps) {
    if (fake_mimetype == TRUE) {
      v4l2sink->fake_mimetype = TRUE;
      v4l2sink->current_caps = gst_caps_ref (caps);
      goto exit;

    }

    if (is_upstream_ancillary) {
      if (!v4l2sink->is_ancillary || release_plane) {
        v4l2sink->fake_mimetype = TRUE;
        goto exit;
      } else {
        v4l2sink->fake_mimetype = FALSE;
      }
    }

    /* Store New parameters */
    v4l2object->width = w;
    v4l2object->height = h;
    v4l2object->pixelformat = fourcc;

    /* Start the v4l2 device */
    GST_DEBUG_OBJECT (v4l2sink, "v4l2 object start");
    if (!gst_st_v4l2_object_start (v4l2object)) {
      v4l2sink->fake_mimetype = TRUE;
      ret = FALSE;
      goto exit;
    }

    v4l2sink->current_caps = gst_caps_ref (caps);
  } else if (is_upstream_ancillary && release_plane) {
    gst_caps_unref (v4l2sink->current_caps);
    v4l2sink->current_caps = NULL;
  }

  if (w != v4l2sink->prev_width || h != v4l2sink->prev_height
      || fourcc != v4l2sink->prev_fourcc) {
    gst_stdisplaysink_reset (v4l2sink, w, h, fourcc);
  }

exit:
  if (ret) {
    gst_task_start (v4l2sink->monitor_task);
  }
  return ret;
}

/**
 * Function name : gst_stdisplaysink_sink_pad_event
 * Input         :
 *  bsink - GstBaseSink
 *  event - GstEvent to be handled
 *
 * Description   : This function is called when an event is received
 *                 by basesink. It allows a subclass to perform some
 *                 specific functions in addition to default implementation
 *                 of basesink corresponding to the event. This function is
 *                 allowing to clear gdp for the streams with subtitles/
 *                 teletext/CC after every seek using zero memset V4L2 buffer
 *                 in flush start event.
 * Return        : gboolean
 */
static gboolean
gst_stdisplaysink_sink_pad_event (GstBaseSink * bsink, GstEvent * event)
{
  GstSTV4l2Sink *v4l2sink = GST_STDISPLAYSINK (bsink);
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  guint8 *dstbuf;
  struct v4l2_buffer dqbuffer;
  gboolean ret = TRUE;
  GstCaps *caps;

  GST_DEBUG_OBJECT (v4l2sink, "%s - (%s)", __FUNCTION__,
      gst_event_type_get_name (GST_EVENT_TYPE (event)));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_FLUSH_START:
      if (v4l2sink->buffer_allocated) {
        dqbuffer.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
        dqbuffer.memory = V4L2_MEMORY_MMAP;

        /* Dqueue buffer */
        if (v4l2_ioctl (v4l2sink->v4l2object->video_fd_output, VIDIOC_DQBUF,
                &dqbuffer) < 0) {
          GST_ERROR_OBJECT (v4l2sink, "VIDIOC_DQBUF failed - %s",
              strerror (errno));
          ret = FALSE;
          goto exit;
        }
        dstbuf = v4l2sink->bufferdata[dqbuffer.index];
        memset (dstbuf, 0, dqbuffer.length);

        /* Queue buffer */
        if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_QBUF,
                &v4l2sink->buffer[dqbuffer.index]) < 0) {
          GST_ERROR_OBJECT (v4l2sink, "VIDIOC_QBUF failed - %s",
              strerror (errno));
          ret = FALSE;
          goto exit;
        }
        v4l2object->queued_buffer_index = dqbuffer.index;
      }
      break;

    case GST_EVENT_CAPS:
      gst_event_parse_caps (event, &caps);
      if ((v4l2sink->prev_caps != NULL)
          && (gst_caps_is_equal ((const GstCaps *) v4l2sink->prev_caps,
                  (const GstCaps *) caps))) {
        GST_DEBUG_OBJECT (v4l2sink, "caps are already set");
        gst_event_unref (event);
        return TRUE;
      }
      ret = gst_stdisplaysink_set_caps (v4l2sink, caps);
      if (ret) {
        if (v4l2sink->prev_caps) {
          gst_caps_unref (v4l2sink->prev_caps);
          v4l2sink->prev_caps = NULL;
        }
        v4l2sink->prev_caps = gst_caps_ref (caps);
      } else {
        GST_ERROR_OBJECT (v4l2sink, "failed to set caps!");
        goto exit;
      }
      break;

    case GST_EVENT_GAP:
      ret = TRUE;
      goto exit;
      break;
    default:
      break;
  }

  return GST_BASE_SINK_CLASS (parent_class)->event (bsink, event);

exit:
  gst_event_unref (event);
  return ret;
}

/* Called after A/V sync to render frame */
static GstFlowReturn
gst_stdisplaysink_show_frame (GstVideoSink * vsink, GstBuffer * buf)
{
  GstSTV4l2Sink *v4l2sink = GST_STDISPLAYSINK (vsink);
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  guint8 *srcbuf;
  guint8 *dstbuf;
  struct v4l2_buffer dqbuffer;
  guint in_image_size = 0;
  GstMapInfo info_read;
  GstFlowReturn ret = GST_FLOW_OK;

  GST_LOG_OBJECT (v4l2sink, "> %s ", __FUNCTION__);
  gst_buffer_map (buf, &info_read, GST_MAP_READ);

  /* If fake mimetype has been negotiated then nothing should be done here */
  if (v4l2sink->fake_mimetype == TRUE) {
    GST_INFO_OBJECT (v4l2sink, "fake mime type");
    goto done;
  }

  if (v4l2sink->num_buffers < PROP_DEF_QUEUE_SIZE) {
    /* Do not break the Audio and Video. just drop data. */
    GST_ERROR_OBJECT (v4l2sink,
        "number of allocated buffers are less than required so droppign data");
    goto done;
  }


  in_image_size = info_read.size;

  /* if older data to be render before set caps, the older data buffer size can be less than the v4l2 buffer size */
  /* so go to drop it */
  if (v4l2object->image_size < in_image_size) {
    GST_WARNING_OBJECT (v4l2sink,
        "dropping frame: image_size=%d, GST_BUFFER_SIZE=%d!!",
        v4l2object->image_size, in_image_size);
    goto done;
  }

  dqbuffer.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  dqbuffer.memory = V4L2_MEMORY_MMAP;
  /* Dqueue buffer */
  if (v4l2_ioctl (v4l2sink->v4l2object->video_fd_output, VIDIOC_DQBUF,
          &dqbuffer) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_DQBUF failed - %s", strerror (errno));
    ret = GST_FLOW_ERROR;
    goto done;
  }

  dstbuf = v4l2sink->bufferdata[dqbuffer.index];
  srcbuf = info_read.data;

  memcpy (dstbuf, srcbuf, in_image_size);

  if (!v4l2sink->is_ancillary) {
    /* This copy is required for the orientation and overlay feature of images */
    if (v4l2sink->num_buffers < PROP_DEF_QUEUE_SIZE)
      GST_WARNING_OBJECT (v4l2sink, "No transform buffer available");
    else {
      memcpy (v4l2sink->bufferdata[v4l2object->queued_buffer_index], dstbuf,
          in_image_size);
    }
  }

  /* Queue buffer */
  if (v4l2_ioctl (v4l2object->video_fd_output, VIDIOC_QBUF,
          &v4l2sink->buffer[dqbuffer.index]) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_QBUF failed - %s", strerror (errno));
    ret = GST_FLOW_ERROR;
    goto done;
  }
  v4l2object->queued_buffer_index = dqbuffer.index;

  if (!v4l2sink->current_caps) {
    if (!gst_stdisplaysink_reset (v4l2sink, 0, 0, 0)) {
      GST_ERROR_OBJECT (v4l2sink, "failed to reset displaysink");
      ret = GST_FLOW_ERROR;
      goto done;
    }
    v4l2sink->fake_mimetype = TRUE;
  }
  GST_LOG_OBJECT (v4l2sink, "rendering done!");

done:
  gst_buffer_unmap (buf, &info_read);

  GST_LOG_OBJECT (v4l2sink, "< %s ", __FUNCTION__);
  return ret;
}

static guint16
gst_stdisplaysink_get_req_id (guint16 req)
{
  guint i = 0;
  guint request = GST_STDISPLAYSINK_REQ_NONE;

  for (i = 0; i < 16; i++) {
    if (req & (1 << i)) {
      break;
    }
  }

  if (i != 15) {
    request = 1 << i;
  }

  return request;
}

static void
gst_stdisplaysink_monitor_loop (GstSTV4l2Sink * v4l2sink)
{
  GstSTV4l2Object *v4l2object = v4l2sink->v4l2object;
  guint16 req;
  struct v4l2_cropcap cropcap;

  if (!gst_st_v4l2_open_device (v4l2object)) {
    GST_ERROR_OBJECT (v4l2sink, "unable to open device");
    goto exit;
  }

  if (!gst_stdisplaysink_set_input_video_plane (v4l2sink)) {
    GST_ERROR_OBJECT (v4l2sink, "unable to set input video plane");
    goto exit;
  }

  /* Get Stream Size and Pxiel Aspect Ration */
  /* This is done continuously 
     to handle case of stream with a size change
  */
  memset (&cropcap, 0, sizeof (cropcap));
  cropcap.type = V4L2_BUF_TYPE_VIDEO_OVERLAY;
  if (v4l2_ioctl (v4l2object->video_fd_overlay, VIDIOC_CROPCAP, &cropcap) < 0) {
    GST_ERROR_OBJECT (v4l2sink, "VIDIOC_CROPCAP failed on %d - %s", v4l2object->video_fd_overlay,strerror (errno));
  } else {
    // in that case stream size is not set
    if ((cropcap.bounds.width != 0) && (cropcap.bounds.height !=0))
    {
      v4l2object->stream.width = cropcap.bounds.width;
      v4l2object->stream.height = cropcap.bounds.height;
    }
    GST_LOG_OBJECT (v4l2sink, "overlayfd %d stream width %d, stream height %d",v4l2object->video_fd_overlay,
        v4l2object->stream.width, v4l2object->stream.height);

    if ((cropcap.pixelaspect.numerator != 0) && (cropcap.pixelaspect.denominator !=0))
    {
      v4l2object->PAR_n = cropcap.pixelaspect.numerator;
      v4l2object->PAR_d = cropcap.pixelaspect.denominator;
    }

    GST_LOG_OBJECT (v4l2sink, "stream PAR_n %d, stream PAR_d %d",
        v4l2object->PAR_n, v4l2object->PAR_d);
  }


  if (!v4l2object->stream_plane_info_set && (v4l2sink->fake_mimetype
          || v4l2sink->is_ancillary)) {
    /* Set the Video Plane */
    GST_DEBUG_OBJECT (v4l2sink, "set video plane info");
    if (!gst_stdisplaysink_set_input_video_plane (v4l2sink)) {
      GST_ERROR_OBJECT (v4l2sink, "unable to set input video plane");
      goto exit;
    }
    gst_stdisplaysink_get_output_video_plane_info (v4l2sink,
        &v4l2object->plane);

    v4l2object->stream_plane_info_set = TRUE;
  }

  req = gst_stdisplaysink_get_req_id (v4l2sink->pending_req);
  switch (req) {
    case GST_STDISPLAYSINK_REQ_ARC_MODE:
    case GST_STDISPLAYSINK_REQ_DAR:
      GST_DEBUG_OBJECT (v4l2sink, "GST_STDISPLAYSINK_REQ_DAR");
      if (!gststv4l2sink_set_aspect_ratio (v4l2sink, req)) {
        GST_DEBUG_OBJECT (v4l2sink, "Failed to set ARC_MODE/ASPECT_RATIO");
      }
      break;
    case GST_STDISPLAYSINK_REQ_RECONFIG:
    {
      GstStructure *structure;
      GstEvent *event;
      GST_DEBUG_OBJECT (v4l2sink, "GST_STDISPLAYSINK_REQ_RECONFIG");
      structure = gst_structure_new_empty ("re-configure");
      if (structure) {
        event = gst_event_new_custom (GST_EVENT_CUSTOM_UPSTREAM, structure);
        if (event) {
          GST_INFO_OBJECT (v4l2sink, "pushing re-configure event");
          if (gst_pad_push_event (v4l2sink->sinkpad, event) == FALSE) {
            GST_DEBUG_OBJECT (v4l2sink,
                "failed to push audio-live-resync event upstream");
          }
        } else {
          GST_DEBUG_OBJECT (v4l2sink,
              "failed to create audio-live-resync event");
        }
      }
    }
      break;
    case GST_STDISPLAYSINK_REQ_DST_RECT:
      GST_DEBUG_OBJECT (v4l2sink, "GST_STDISPLAYSINK_REQ_DST_RECT");
      if (!gststv4l2sink_set_output_window (v4l2sink)) {
        GST_ERROR_OBJECT (v4l2sink, "Failed to set output rectangle");
      }
      break;
    case GST_STDISPLAYSINK_REQ_SRC_RECT:
      GST_DEBUG_OBJECT (v4l2sink, "GST_STDISPLAYSINK_REQ_SRC_RECT");
      if (!gststv4l2sink_set_input_window (v4l2sink)) {
        GST_ERROR_OBJECT (v4l2sink, "Failed to set input rectangle");
      }
      break;
    case GST_STDISPLAYSINK_REQ_FLIP_HORIZONTAL:
    case GST_STDISPLAYSINK_REQ_FLIP_VERTICAL:
    case GST_STDISPLAYSINK_REQ_ROTATE:
      gststv4l2sink_set_transform (v4l2sink, req);
      break;
    case GST_STDISPLAYSINK_REQ_PLANE_NAME:
      break;
    case GST_STDISPLAYSINK_REQ_PLANE_TRANSPARENCY:
      gststv4l2sink_graphics_set_transparency (v4l2sink);
      break;
    case GST_STDISPLAYSINK_REQ_MUTE:
      gst_stdisplaysink_set_video_mute_unmute (v4l2sink);
      break;
    default:
      break;
  }
  v4l2sink->pending_req &= (~req);

exit:
  usleep (50 * 1000);
}

gboolean
stdisplaysink_init (GstPlugin * plugin)
{
  GST_DEBUG_CATEGORY_INIT (stdisplaysink_debug, "stdisplaysink", 0,
      "STDISPLAY sink element");

  return gst_element_register (plugin, "stdisplaysink", (GST_RANK_PRIMARY + 10),
      GST_TYPE_STDISPLAYSINK);
}
