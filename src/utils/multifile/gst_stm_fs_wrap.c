/* Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "gst_stm_fs_wrap.h"

/*
 function: gst_stm_internal_fs_open
 input params:
           filehandle - multifile handle.
           mode - mode in which file has to be opened. (rb,wb,r+b)
 description : will open a file in the given mode.
 return : TRUE if file opened successfully else FALSE.
 */
static gboolean
gst_stm_internal_fs_open (GstStmFileHandle * filehandle, gchar * mode)
{
  gboolean res = FALSE;
  gchar *full_path;

  /* get the full file path */
  if (!filehandle->file_info.cur_file_num) {
    full_path =
        g_strdup_printf ("%s.%s", filehandle->file_name,
        filehandle->file_extension);
  } else {
    full_path =
        g_strdup_printf ("%s%03u", filehandle->file_name,
        filehandle->file_info.cur_file_num);
  }

  /* Open ts file */
  filehandle->fd = fopen (full_path, mode);
  if (filehandle->fd == NULL) {
    res = FALSE;
  } else {
    /* file opened successfully */
    res = TRUE;
  }
  g_free (full_path);
  return res;
}

/*
 function: gst_stm_internal_fs_close
 input params:
           filehandle - multifile handle.
 description : will close the current file (filehandle->fd carries)
               current file's handle
 return : void
 */
static void
gst_stm_internal_fs_close (GstStmFileHandle * filehandle)
{
  if (filehandle->fd != NULL) {
    fclose (filehandle->fd);
  }
  filehandle->fd = NULL;
  return;
}

/*
 function: gst_stm_fs_init
 input params:
           filehandle - multifile handle.
 description : will initialize members of multifile structure
 return : void
 */
static void
gst_stm_fs_init (GstStmFileHandle * filehandle)
{
  GstStmFsStats *file_info = NULL;
  GstStmFsParams *max_size = NULL;
  if (filehandle == NULL) {
    return;
  }
  file_info = &filehandle->file_info;
  max_size = &file_info->max_size;

  filehandle->fd = NULL;
  filehandle->file_name = NULL;
  filehandle->file_extension = NULL;

  file_info->cur_packet_pos = 0;
  file_info->cur_file_num = 0;
  file_info->total_file_num = 0;
  file_info->total_circular_runs = 0;

  max_size->file_max_circular_size = 0;
  max_size->file_max_size = MAX_SUB_FILE_SIZE;
  max_size->file_record_size = 0;

  return;
}

/*
 function: gst_stm_fs_open
 input params:
           filename - file to be opened.
           mode - mode in which file will be opened.
           params - params for max_size (liner & circular).
 description : will allocate a GstStmFileHandle structure and
               open provided file.
 return : GstStmFileHandle * - allocated GstStmFileHandle pointer.
 */
GstStmFileHandle *
gst_stm_fs_open (gchar * filename, gchar * mode, GstStmFsParams * params)
{
  GstStmFileHandle *filehandle;
  gchar **param;
  guint num_param;

  filehandle = (GstStmFileHandle *) g_new0 (GstStmFileHandle, 1);
  if (filehandle == NULL) {
    return NULL;
  }
  gst_stm_fs_init (filehandle);

  /* get file name and extension */
  param = g_strsplit (filename, ".", 0);
  num_param = g_strv_length (param);
  if (num_param < 2) {
    g_print ("path not correct!\n");
  }

  filehandle->file_name = g_strdup_printf ("%s", param[0]);
  filehandle->file_extension = g_strdup_printf ("%s", param[num_param - 1]);
  g_strfreev (param);

  /* Open ts file */
  if (!gst_stm_internal_fs_open (filehandle, mode)) {
    GST_WARNING ("failed to open file %s.%s",
        filehandle->file_name, filehandle->file_extension);
    return NULL;
  }

  /* update file max size and circular size */
  /* default circular size is 0 (linear buffer) and max size is 2GB
     for single file */
  if (params != NULL) {
    filehandle->file_info.max_size.file_max_circular_size =
        params->file_max_circular_size;
    filehandle->file_info.max_size.file_record_size = params->file_record_size;
    if (params->file_max_size != 0) {
      filehandle->file_info.max_size.file_max_size = params->file_max_size;
    }
  }

  return (filehandle);
}

/*
 function: gst_stm_internal_fs_write
 input params:
           filehandle - multifile handle.
           buf - data pointer to write.
           size - size of data to write.
 description : will write given amount of data into currently
               opened file. if file size exceeds 2GB, will open
               new file.
 return : amount of data written successfully.
 */
guint32
gst_stm_internal_fs_write (GstStmFileHandle * filehandle, guint8 * buf,
    guint32 size)
{
  size_t write = 0;
  GstStmFsStats *file_info = NULL;
  GstStmFsParams *max_size = NULL;
  guint8 *temp_buf;
  size_t bytes_written = 0;
  guint32 size_left = 0;

  if (filehandle == NULL) {
    return 0;
  }

  file_info = &filehandle->file_info;
  max_size = &file_info->max_size;

  /* Check for 2GB boundary */
  if (((file_info->cur_packet_pos + size) >
          (guint64) ((file_info->cur_file_num +
                  1) * (guint64) (file_info->max_size.file_max_size) +
              (file_info->total_circular_runs *
                  max_size->file_max_circular_size)))
      && file_info->cur_packet_pos) {
    temp_buf = buf;
    size_left =
        ((file_info->cur_file_num +
            1) * (guint64) (file_info->max_size.file_max_size)) +
        (file_info->total_circular_runs * max_size->file_max_circular_size) -
        file_info->cur_packet_pos;
    /* Write the left over bytes onto old file */
    if (filehandle->fd) {
      bytes_written = fwrite (buf, 1, size_left, filehandle->fd);
      if (bytes_written > 0) {
        /* Update no of recorded ts packet */
        filehandle->file_info.cur_packet_pos += bytes_written;
        temp_buf += bytes_written;
      }
    }

    /* close current file */
    GST_LOG ("Opening new file as size reached 2GB cur_file_num=%u",
        file_info->cur_file_num);
    gst_stm_internal_fs_close (filehandle);
    /* Generate sub file name */
    file_info->cur_file_num++;

    /* Open next sub file */
    if ((max_size->file_max_circular_size)
        && (file_info->cur_packet_pos >= max_size->file_max_circular_size)) {
      /* If we have a wrap already */
      if (!gst_stm_internal_fs_open (filehandle, "r+b")) {
        GST_WARNING ("Failed to open file");
        return 0;
      }
    } else {
      if (!gst_stm_internal_fs_open (filehandle, "wb")) {
        GST_WARNING ("Failed to open file");
        return 0;
      }
    }

    /* Write (size-size_left) bytes to new file */
    if (filehandle->fd) {
      write = fwrite (temp_buf, 1, (size - bytes_written), filehandle->fd);
      if (write > 0) {
        /* Update no of recorded ts packet */
        filehandle->file_info.cur_packet_pos += write;
      }
    }
    return (write + bytes_written);
  }

  if (filehandle->fd) {
    write = fwrite (buf, 1, size, filehandle->fd);
    if (write > 0) {
      /* Update no of recorded ts packet */
      filehandle->file_info.cur_packet_pos += write;
    }
  }

  return write;
}


/*
 function: gst_stm_fs_write
 input params:
           filehandle - multifile handle.
           buf - data pointer to write.
           size - size of data to write.
 description : will write given amount of data into currently
               opened file. if file size exceeds 2GB, will open
               new file. in case of circular, will wrap if circular
               limit is reached.
 return : amount of data written successfully.
 */
guint32
gst_stm_fs_write (GstStmFileHandle * filehandle, guint8 * buf, guint32 size)
{
  size_t write = 0;
  GstStmFsStats *file_info = NULL;
  GstStmFsParams *max_size = NULL;
  guint8 *temp_buf;
  size_t bytes_written = 0;
  guint32 size_left = 0;

  if (filehandle == NULL) {
    return 0;
  }

  file_info = &filehandle->file_info;
  max_size = &file_info->max_size;

  if ((size > 0) && (buf != NULL)) {
    /* Check for circular file wrap */
    if ((max_size->file_max_circular_size) &&
        (((file_info->cur_packet_pos + size) >
                (max_size->file_max_circular_size *
                    (file_info->total_circular_runs + 1)))
            && file_info->cur_packet_pos)) {
      temp_buf = buf;
      size_left =
          (max_size->file_max_circular_size * (file_info->total_circular_runs +
              1)) - file_info->cur_packet_pos;
      /* Write the left over bytes onto old file */
      if (filehandle->fd) {
        bytes_written = gst_stm_internal_fs_write (filehandle, buf, size_left);
        if (bytes_written != size_left) {
          GST_WARNING ("Couldnt write %d bytes. Bytes_written=%d\n", size_left,
              bytes_written);
        }
      }
      temp_buf += bytes_written;

      GST_DEBUG ("Max Circular Size reached. Write reset");
      file_info->total_file_num = file_info->cur_file_num;
      file_info->cur_file_num = 0;
      file_info->total_circular_runs++;

      /* Close current sub file */
      gst_stm_internal_fs_close (filehandle);

      /* Open first sub file */
      if (!gst_stm_internal_fs_open (filehandle, "r+b")) {
        GST_WARNING ("Failed to open file");
        return 0;
      }

      /* Write (size-size_left) bytes to new file */
      if (filehandle->fd) {
        write =
            gst_stm_internal_fs_write (filehandle, temp_buf,
            size - bytes_written);
        if (write != size - bytes_written) {
          GST_WARNING ("Couldnt write %d bytes. Bytes_written=%d\n",
              size - bytes_written, write);
        }
      }
      return (write + bytes_written);
    }

    if (filehandle->fd) {
      write = gst_stm_internal_fs_write (filehandle, buf, size);
      if (write != size) {
        GST_WARNING ("Couldnt write %d bytes. Bytes_written=%d\n", size, write);
      }
    }
  }

  return write;
}

/*
 function: gst_stm_internal_fs_read
 input params:
           filehandle - multifile handle.
           buf - data pointer in which data will be read.
           size - size of data to read.
 description : will read given amount of data into currently
               opened file. if file size exceeds 2GB, will open
               new file.
 return : amount of data read successfully.
 */
guint32
gst_stm_internal_fs_read (GstStmFileHandle * filehandle, guint8 * buf,
    guint32 size)
{
  size_t read = 0;
  GstStmFsStats *file_info = NULL;
  GstStmFsParams *max_size = NULL;
  guint8 *temp_buf;
  size_t bytes_read = 0;
  guint32 size_left = 0;

  if (filehandle == NULL) {
    return 0;
  }

  file_info = &filehandle->file_info;
  max_size = &file_info->max_size;

  /* Check for 2GB boundary */
  if (((file_info->cur_packet_pos + size) >
          (guint64) ((file_info->cur_file_num +
                  1) * (guint64) (file_info->max_size.file_max_size) +
              (file_info->total_circular_runs *
                  max_size->file_max_circular_size)))
      && file_info->cur_packet_pos) {
    temp_buf = buf;
    size_left =
        ((file_info->cur_file_num +
            1) * (guint64) (file_info->max_size.file_max_size)) +
        (file_info->total_circular_runs * max_size->file_max_circular_size) -
        file_info->cur_packet_pos;
    /* Read the left over bytes onto old file */
    if (filehandle->fd) {
      bytes_read = fread (buf, 1, size_left, filehandle->fd);
      if (bytes_read > 0) {
        /* Update no of recorded ts packet */
        filehandle->file_info.cur_packet_pos += bytes_read;
        temp_buf += bytes_read;
      }
    }

    /* close current file */
    GST_LOG ("Opening new file as size reached 2GB cur_file_num=%u",
        file_info->cur_file_num);
    gst_stm_internal_fs_close (filehandle);

    /* Generate sub file name */
    file_info->cur_file_num++;

    /* Open next sub file */
    if (!gst_stm_internal_fs_open (filehandle, "rb")) {
      GST_WARNING ("Failed to open file");
      return 0;
    }

    /* Read (size-size_left) bytes from new file */
    if (filehandle->fd) {
      read = fread (temp_buf, 1, (size - bytes_read), filehandle->fd);
      if (read > 0) {
        /* Update no of recorded ts packet */
        filehandle->file_info.cur_packet_pos += read;
      }
    }
    return (read + bytes_read);
  }

  if (filehandle->fd) {
    read = fread (buf, 1, size, filehandle->fd);
    if (read > 0) {
      /* Update no of recorded ts packet */
      filehandle->file_info.cur_packet_pos += read;
    }
  }

  return read;
}

/*
 function: gst_stm_fs_read
 input params:
           filehandle - multifile handle.
           buf - data pointer in which data will be read.
           size - size of data to read.
 description : will read given amount of data into currently
               opened file. if file size exceeds 2GB, will open
               new file. in case of circular, will wrap if circular
               limit is reached.
 return : amount of data read successfully.
 */
guint32
gst_stm_fs_read (GstStmFileHandle * filehandle, guint8 * buf, guint32 size)
{
  size_t read = 0;
  GstStmFsStats *file_info = NULL;
  GstStmFsParams *max_size = NULL;

  guint8 *temp_buf;
  size_t bytes_read = 0;
  guint32 size_left = 0;

  if (filehandle == NULL) {
    return 0;
  }
  file_info = &filehandle->file_info;
  max_size = &file_info->max_size;

  if ((size) && (buf != NULL)) {
    /* Check for circular file wrap */
    if ((max_size->file_max_circular_size) &&
        (((file_info->cur_packet_pos + size) >
                (max_size->file_max_circular_size *
                    (file_info->total_circular_runs + 1)))
            && file_info->cur_packet_pos)) {
      temp_buf = buf;
      size_left =
          (max_size->file_max_circular_size * (file_info->total_circular_runs +
              1)) - file_info->cur_packet_pos;

      /* Read the left over bytes onto old file */
      if (filehandle->fd) {
        bytes_read = gst_stm_internal_fs_read (filehandle, buf, size_left);
        if (bytes_read != size_left) {
          GST_WARNING ("Couldnt read %d bytes. Bytes_read=%d\n", size_left,
              bytes_read);
        }
      }
      temp_buf += bytes_read;

      GST_DEBUG ("Circular File Size reached. Read reset");
      file_info->cur_file_num = 0;
      file_info->total_circular_runs++;

      /* Close current sub file */
      gst_stm_internal_fs_close (filehandle);

      /* Open first sub file */
      if (!gst_stm_internal_fs_open (filehandle, "rb")) {
        GST_WARNING ("Failed to open file");
        return 0;
      }

      /* Read (size-size_left) bytes to new file */
      if (filehandle->fd) {
        read =
            gst_stm_internal_fs_read (filehandle, temp_buf, size - bytes_read);
        if (read != size - bytes_read) {
          GST_WARNING ("Couldnt read %d bytes. Bytes_read=%d\n",
              size - bytes_read, read);
        }
      }
      return (read + bytes_read);
    }

    if (filehandle->fd) {
      read = gst_stm_internal_fs_read (filehandle, buf, size);
      if (read != size) {
        GST_WARNING ("Couldnt read %d bytes. Bytes_read=%d\n", size, read);
      }
    }
  }
  return read;
}

/*
 function: gst_stm_fs_seek
 input params:
           filehandle - multifile handle.
           offset - offset to seek.
           origin - origin from where to seek.
 description : will get the correct file and offset.
               will open the required file and will seek
               in it.
 return : TRUE if seek is successful else FALSE.
 */
gboolean
gst_stm_fs_seek (GstStmFileHandle * filehandle, gint64 offset, gint origin)
{
  gboolean res = FALSE;
  gint32 file_offset;
  gint32 file_num;
  GstStmFsStats *file_info = NULL;
  GstStmFsParams *max_size = NULL;
  gint64 seek_offset = 0;
  guint64 circular_seek_offset = 0;
  guint circular_runs = 0;

  if (filehandle == NULL) {
    return 0;
  }
  file_info = &filehandle->file_info;
  max_size = &file_info->max_size;

  switch (origin) {
    case SEEK_SET:
    {
      seek_offset = offset;
    }
      break;
    case SEEK_CUR:
    {
      seek_offset = file_info->cur_packet_pos + offset;
    }
      break;
    case SEEK_END:
    {
      seek_offset = max_size->file_record_size + offset;
    }
      break;
    default:
      break;
  }

  /* for circular file wrap the location */
  if (max_size->file_max_circular_size) {
    circular_runs = seek_offset / max_size->file_max_circular_size;
    circular_seek_offset = seek_offset % max_size->file_max_circular_size;
  } else {
    circular_seek_offset = seek_offset;
  }

  /* We find the correct sub file to open */
  file_num = circular_seek_offset / (max_size->file_max_size);
  file_offset = circular_seek_offset % (max_size->file_max_size);

  /* open the required file */
  if (file_num != file_info->cur_file_num) {
    gst_stm_internal_fs_close (filehandle);
    /* Generate the name of sub file */
    file_info->cur_file_num = file_num;
    if (!gst_stm_internal_fs_open (filehandle, "rb")) {
      GST_WARNING ("Failed to open file");
      res = FALSE;
    }
  }

  /* seek to required location */
  if (filehandle->fd) {
    /* Do seek on the playback file */
    if (fseek (filehandle->fd, file_offset, origin) != 0) {
      GST_WARNING ("failed to seek");
      res = FALSE;
    } else {
      file_info->cur_packet_pos = seek_offset;
      file_info->total_circular_runs = circular_runs;
      res = TRUE;
    }
  }

  return res;
}

/*
 function: gst_stm_fs_tell
 input params:
           filehandle - multifile handle.
 description : will tell the total size from start of
               first file to current location in current file.
 return : total size.
 */
guint64
gst_stm_fs_tell (GstStmFileHandle * filehandle)
{
  if (filehandle == NULL) {
    return 0;
  }
  return filehandle->file_info.cur_packet_pos;
}

/*
 function: gst_stm_fs_flush
 input params:
           filehandle - multifile handle.
 description : flush the data into current file.
 return : TRUE if seek is successful else FALSE.
 */
gboolean
gst_stm_fs_flush (GstStmFileHandle * filehandle)
{
  if (filehandle == NULL) {
    return FALSE;
  }
  if (fflush (filehandle->fd) != 0) {
    return FALSE;
  }

  return TRUE;
}

/*
 function: gst_stm_fs_close
 input params:
           filehandle - multifile handle.
 description : will close the currently opened file and will
               free filehandle memory.
 return : void.
 */
void
gst_stm_fs_close (GstStmFileHandle * filehandle)
{
  if (filehandle == NULL) {
    return;
  }
  gst_stm_internal_fs_close (filehandle);
  g_free (filehandle->file_name);
  g_free (filehandle->file_extension);
  gst_stm_fs_init (filehandle);
  g_free (filehandle);
  return;
}

/* gst_stm_fs_remove and gst_stm_fs_rename functions are not being
    used currently but may be used later for enahancements in DVR so being
    commented for the moment
  */
#if 0
/*
 function: gst_stm_fs_remove
 input params:
           filehandle - multifile handle.
 description : will delete all created multifiles.
 return : TRUE if successful else FALSE.
 */
gboolean
gst_stm_fs_remove (GstStmFileHandle * filehandle)
{
  gboolean res = TRUE;

  gchar *full_path = NULL;
  guint file_num = 0;

  /* remove files one by one */
  for (file_num = 0; file_num <= filehandle->file_info.total_file_num;
      file_num++) {
    /* get the full file path */
    if (!file_num) {
      full_path = g_strdup_printf ("%s.ts", filehandle->file_name);
    } else {
      full_path = g_strdup_printf ("%s%03u", filehandle->file_name, file_num);
    }
    /* remove file */
    if (remove (full_path) != 0) {
      GST_WARNING ("failed to remove file %s", full_path);
      res = FALSE;
    }
    g_free (full_path);
  }

  return res;
}

/*
 function: gst_stm_fs_rename
 input params:
           filehandle - multifile handle.
 description : will rename given multifiles.
 return : TRUE if successful else FALSE.
 */
gboolean
gst_stm_fs_rename (gchar * old_name, gchar * new_name)
{
  gboolean res = TRUE;
  gchar **param;
  gchar *old_file_name;
  gchar *new_file_name;
  gchar *old_file_path, *new_file_path;
  guint file_num = 0;

  /* get file name and extension */
  param = g_strsplit (old_name, ".", 0);
  old_file_name = g_strdup_printf ("%s", param[0]);
  param = g_strsplit (new_name, ".", 0);
  new_file_name = g_strdup_printf ("%s", param[0]);

  /* rename files one by one */
  while (res) {
    /* get the full file path */
    if (!file_num) {
      old_file_path = g_strdup_printf ("%s", old_name);
      new_file_path = g_strdup_printf ("%s", new_name);
    } else {
      old_file_path = g_strdup_printf ("%s%03u", old_file_name, file_num);
      new_file_path = g_strdup_printf ("%s%03u", new_file_name, file_num);
    }
    /* remove file */
    if (rename (old_file_path, new_file_path) != 0) {
      res = FALSE;
    }
    g_free (old_file_path);
    g_free (new_file_path);

    file_num++;
  }
  g_free (old_file_name);
  g_free (new_file_name);

  return res;
}
#endif
/*
 function: gst_stm_fs_error
 input params:
           filehandle - multifile handle.
 description : will check for any error during last file operation.
 return : return 0 if no error in last operation else non zero.
 */
gint
gst_stm_fs_error (GstStmFileHandle * filehandle)
{
  gint res = 0;

  if (filehandle == NULL) {
    return FALSE;
  }

  if (filehandle->fd) {
    res = ferror (filehandle->fd);
  }
  return res;
}

/*
 function: gst_stm_fs_eof
 input params:
           filehandle - multifile handle.
 description : will check if eof occured in file last operation.
 return : return 0 if no error in last operation else non zero.
 */
gint
gst_stm_fs_eof (GstStmFileHandle * filehandle)
{
  gint res = 0;

  if (filehandle == NULL) {
    return FALSE;
  }

  if (filehandle->fd) {
    res = feof (filehandle->fd);
  }
  return res;
}

/*
 function: gst_stm_fs_stats
 input params:
           filehandle - multifile handle.
           params - stats structure
 description : will fill params with current stats of filehandle.
 return : TRUE of successful else FALSE.
 */
gboolean
gst_stm_fs_stats (GstStmFileHandle * filehandle, GstStmFsStats * params)
{
  GstStmFsStats *file_info = NULL;
  GstStmFsParams *max_size = NULL;

  if (filehandle == NULL) {
    return FALSE;
  }
  file_info = &filehandle->file_info;
  max_size = &file_info->max_size;

  g_snprintf (params->filename, sizeof (params->filename), "%s.%s",
      filehandle->file_name, filehandle->file_extension);
  params->cur_packet_pos = file_info->cur_packet_pos;
  params->cur_file_num = file_info->cur_file_num;
  params->total_file_num = file_info->total_file_num;
  params->total_circular_runs = file_info->total_circular_runs;
  params->max_size.file_max_size = max_size->file_max_size;
  params->max_size.file_max_circular_size = max_size->file_max_circular_size;

  return TRUE;
}
