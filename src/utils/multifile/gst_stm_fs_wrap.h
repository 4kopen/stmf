/* Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STM_MULTI_FILE_H__
#define __GST_STM_MULTI_FILE_H__

#include <stdio.h>
#include <gst/gst.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>

G_BEGIN_DECLS
/* MultiFile Handle Structure */
#define MAX_SUB_FILE_SIZE   (188*192*2*29746)
    struct _GstStmFsParams
{
  guint32 file_max_size;
  guint64 file_max_circular_size;
  guint64 file_record_size;
};
typedef struct _GstStmFsParams GstStmFsParams;

struct _GstStmFsStats
{
  gchar filename[512];
  guint64 cur_packet_pos;
  guint32 cur_file_num;
  guint32 total_file_num;
  guint32 total_circular_runs;
  GstStmFsParams max_size;
};
typedef struct _GstStmFsStats GstStmFsStats;

struct _GstStmFileHandle
{
  FILE *fd;
  gchar *file_name;
  gchar *file_extension;
  GstStmFsStats file_info;
};
typedef struct _GstStmFileHandle GstStmFileHandle;

/* MultiFile Handling */
GstStmFileHandle *gst_stm_fs_open (gchar * file_path, gchar * mode,
    GstStmFsParams * Params);
guint32 gst_stm_fs_write (GstStmFileHandle * filehandle, guint8 * buf,
    guint32 size);
guint32 gst_stm_fs_read (GstStmFileHandle * filehandle, guint8 * buf,
    guint32 size);
gboolean gst_stm_fs_seek (GstStmFileHandle * filehandle, gint64 offset,
    gint origin);
guint64 gst_stm_fs_tell (GstStmFileHandle * filehandle);
gboolean gst_stm_fs_flush (GstStmFileHandle * filehandle);
void gst_stm_fs_close (GstStmFileHandle * filehandle);
#if 0
gboolean gst_stm_fs_remove (GstStmFileHandle * filehandle);
gboolean gst_stm_fs_rename (gchar * old_name, gchar * new_name);
#endif
gint gst_stm_fs_error (GstStmFileHandle * filehandle);
gint gst_stm_fs_eof (GstStmFileHandle * filehandle);
gboolean gst_stm_fs_stats (GstStmFileHandle * filehandle,
    GstStmFsStats * params);

G_END_DECLS
#endif
