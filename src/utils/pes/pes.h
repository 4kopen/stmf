/* linuxdvb output/writer handling.
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _PES_H
#define _PES_H

#include <gst/gst.h>

#define MAX_PES_PACKET_SIZE                     65400

#define MPEG_AUDIO_PES_START_CODE               0xc0
#define AAC_AUDIO_PES_START_CODE                0xcf
#define PRIVATE_STREAM_1_PES_START_CODE         0xbd
#define PES_MAX_HEADER_SIZE                     64
#define PES_MIN_HEADER_SIZE                     9
#define MPEG_VIDEO_PES_START_CODE               0xe0
#define H264_VIDEO_PES_START_CODE               0xe2
#define PES_START_CODE_RESERVED_4               0xfd
#define PES_START_CODE_RESERVED_5               0xfe
#define VC1_VIDEO_PES_START_CODE                PES_START_CODE_RESERVED_4
#define H263_VIDEO_PES_START_CODE               PES_START_CODE_RESERVED_5

#define PES_LENGTH_BYTE_0                       5
#define PES_LENGTH_BYTE_1                       4
#define PES_FLAGS_BYTE                          7
#define PES_EXTENSION_DATA_PRESENT              0x01
#define PES_PRIVATE_DATA_LENGTH                 8
#define PES_EXTENSION_FLAG_BYTE                 7
#define PES_EXTENSION_FLAG                      0x01
#define PES_HEADER_DATA_LENGTH_BYTE             8
#define PES_PRIVATE_DATA_FLAG                   0x80
#define PES_VERSION_FAKE_START_CODE             0x31

#define MAX_PTS_VALUE			        0x1ffffffffull  /* maximum pts value (33 bits) */
#define INVALID_PTS_VALUE                       0x200000000ull  /* valid PTS range is from Bit 0 to Bit 33 */
#define IS_PTS_VALUE_VALID(x)                   ((guint64)(x) < INVALID_PTS_VALUE ? TRUE : FALSE)

#ifdef __cplusplus
extern "C"
{
#endif

  typedef struct BitPacker_s
  {
    guint8 *ptr;                /* write pointer */
    guint bit_buffer;           /* bitreader shifter */
    gint remaining;             /* number of remaining in the shifter */
  } BitPacker_t;

  void PutBits (BitPacker_t * ld, guint code, guint length);
  void FlushBits (BitPacker_t * ld);
  void AlignBits (BitPacker_t * ld);
  gint InsertVideoPrivateDataHeader (guint8 * data, gint payload_size,
      gint encoding);
  gint InsertAudioPrivateDataHeader (guint8 * data, gint payload_size,
      gint encoding);
  gint InsertPesHeader (guint8 * data, gint size, guint8 stream_id, guint64 pts,
      gint pic_start_code);

#ifdef __cplusplus
}
#endif

#endif
