 /*
  * This library is licensed under 2 different licenses and you
  * can choose to use it under the terms of either one of them. The
  * two licenses are the MPL 1.1 and the LGPL.
  *
  * MPL:
  *
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License"); you may not use this file except in
  * compliance with the License. You may obtain a copy of the License at
  * http://www.mozilla.org/MPL/.
  *
  * Software distributed under the License is distributed on an "AS IS"
  * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
  * License for the specific language governing rights and limitations
  * under the License.
  *
  * LGPL:
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Library General Public
  * License as published by the Free Software Foundation; either
  * version 2 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Library General Public License for more details.
  *
  * You should have received a copy of the GNU Library General Public
  * License along with this library; if not, write to the
  * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  * Boston, MA 02111-1307, USA.
  *
  * The Original Code is Fluendo MPEG Demuxer plugin.
  *
  * The Initial Developer of the Original Code is Fluendo, S.L.
  * Portions created by Fluendo, S.L. are Copyright (C) 2005
  * Fluendo, S.L. All Rights Reserved.
  *
  * Contributor(s): Wim Taymans <wim@fluendo.com>
  *
  * This file has been modified by STMicroelectronics, on 03/13/2012
  */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstpesfilter.h"
#include <string.h>

GST_DEBUG_CATEGORY (gststpesfilter_debug);
#define GST_CAT_DEFAULT (gststpesfilter_debug)

static GstFlowReturn gst_pes_filter_data_push (GstPESFilter * filter,
    gboolean first, GstBuffer * buffer);

void gst_pes_es_task (void *data);


/*
   Function   : gst_pes_sem_down
   Input           : none
   Description     : This function takes semaphore, decrements count
   Return          : none
 */
void
gst_pes_sem_down (GstPesSema * sem)
{
  g_mutex_lock (&sem->mutex);
  if (sem->counter == 0) {
    g_cond_wait (&sem->condition, &sem->mutex);
  }
  sem->counter--;
  g_mutex_unlock (&sem->mutex);
}

/*
   Function name   : gst_pes_sem_up
   Input           : none
   Description     : This function takes signals semaphore, increments count
   Return          : none
 */
void
gst_pes_sem_up (GstPesSema * sem)
{
  g_mutex_lock (&sem->mutex);
  sem->counter++;
  g_cond_signal (&sem->condition);
  g_mutex_unlock (&sem->mutex);
}

/*
   Function   : gst_pes_filter_init
   Input           : filter - pes filter pointer
                   : adapter - pointer to gst adapter (now unused - can be removed)
                   : adapter_offset - pointer to adapter offset (now unused - can be removed)
   Description     : This function initializes pes filter, allocations, consumer task creation
                      and task start
   Return          : none
 */
void
gst_pes_filter_init (GstPESFilter * filter, GstAdapter * adapter,
    guint64 * adapter_offset)
{
  g_return_if_fail (filter != NULL);
  GST_INFO ("initializing pes filter");

  memset (filter, 0, sizeof (GstPESFilter));
  filter->PESBufBase_p = (guint8 *) g_malloc0 (PES_BUFFER_SIZE);
  filter->ESBufBase_p = (guint8 *) g_malloc0 (ES_BUFFER_SIZE);
  filter->parserflush = FALSE;
  /* pes es task setup */
  filter->pes_es_sync_sem.counter = 0;
  g_mutex_init (&filter->pes_es_sync_sem.mutex);
  g_cond_init (&filter->pes_es_sync_sem.condition);
  filter->ParserState = PES_MP2_PARSE_STATE_SYNC0;
  g_rec_mutex_init (&filter->pes_es_task_mutex);
  filter->pes_es_task =
      gst_task_new ((GstTaskFunction) gst_pes_es_task, filter, NULL);
  gst_object_set_name (GST_OBJECT_CAST (filter->pes_es_task), "SMF-Pes Filter");
  if (filter->pes_es_task && GST_IS_TASK (filter->pes_es_task)) {
    gst_task_set_lock (filter->pes_es_task, &filter->pes_es_task_mutex);
    gst_task_start (filter->pes_es_task);
  }
}

/*
   Function name   : gst_pes_filter_uninit
   Input           : filter - pes filter pointer
   Description     : This function performs final cleanup, frees resources
   Return          : none
 */
void
gst_pes_filter_uninit (GstPESFilter * filter)
{
  g_return_if_fail (filter != NULL);
  GST_INFO ("uninitializing pesfilter, final cleanup");

  /* clean up for pes-es task */
  if (GST_IS_TASK (filter->pes_es_task)) {
    filter->taskexit = TRUE;
    gst_pes_sem_up (&filter->pes_es_sync_sem);
    gst_task_stop (filter->pes_es_task);
    g_rec_mutex_lock (&filter->pes_es_task_mutex);
    g_rec_mutex_unlock (&filter->pes_es_task_mutex);
    filter->parserflush = TRUE;
    gst_task_join (filter->pes_es_task);
    GST_INFO ("uninitializing pesfilter, gst_task_join done");
    gst_object_unref (filter->pes_es_task);
    filter->pes_es_task = NULL;
    filter->taskexit = FALSE;
    filter->parserflush = FALSE;
  }
  g_free (filter->PESBufBase_p);
  filter->PESBufBase_p = NULL;
  g_free (filter->ESBufBase_p);
  filter->ESBufBase_p = NULL;

  g_mutex_clear (&filter->pes_es_sync_sem.mutex);
  g_cond_clear (&filter->pes_es_sync_sem.condition);
  g_rec_mutex_clear (&filter->pes_es_task_mutex);

  GST_INFO ("uninitializing pesfilter, final cleanup==> EXIT");
}

/*
   Function  : gst_pes_filter_drain
   Input           : filter - pes filter pointer
   Description     : This function first pauses consumer task, flushes pes filter,
                     reset state variables and then resume consumer task
   Return          : GST_FLOW_ERROR/GST_FLOW_OK
 */
GstFlowReturn
gst_pes_filter_drain (GstPESFilter * filter)
{
  g_return_val_if_fail (filter != NULL, GST_FLOW_ERROR);
  GST_INFO ("flushing pes filter");
  GST_DEBUG ("going to pause pesestask");
  gst_task_pause (filter->pes_es_task);
  filter->parserflush = TRUE;
  gst_pes_sem_up (&filter->pes_es_sync_sem);
  g_rec_mutex_lock (&filter->pes_es_task_mutex);
  g_rec_mutex_unlock (&filter->pes_es_task_mutex);
  filter->ParserState = PES_MP2_PARSE_STATE_SYNC0;
  memset (filter->ESBufBase_p, 0, ES_BUFFER_SIZE);
  memset (filter->PESBufBase_p, 0, PES_BUFFER_SIZE);
  filter->WriteOffset = 0;
  filter->ReadOffset = 0;
  filter->ByteOffset = 0;
  filter->ESBufBaseOffset = 0;
  filter->BytesToSkipInPESHeader = 0;
  filter->RemainingBytesForCurrentPES = 0;
  filter->ptsdtsflag = 0;
  filter->headerflag = 0;
  filter->pts = 0;
  filter->taskexit = 0;
  filter->PacketHeaderLength = 0;
  filter->TotalPacketLength = 0;
  filter->parserflush = FALSE;
  gst_task_start (filter->pes_es_task);
  GST_DEBUG ("pesestask resumed");
  return GST_FLOW_OK;
}

/*
   Function   : gst_pes_filter_set_callbacks
   Input           : filter - pes filter pointer
                   : data_cb - decoder callback
                   : resync_cb - unused (can be removed in future)
                   : user_data - callback data (filter control block)
   Description     : This function sets up necessary callbacks
   Return          : none
 */
void
gst_pes_filter_set_callbacks (GstPESFilter * filter,
    GstPESFilterData data_cb, GstPESFilterResync resync_cb, gpointer user_data)
{
  g_return_if_fail (filter != NULL);

  GST_INFO ("Setting pes filter callbacks");

  filter->data_cb = data_cb;
  filter->resync_cb = resync_cb;
  filter->user_data = user_data;
}

/*
   Function   : gst_pes_parse_sync0
   Input           : filter - pes filter pointer
                   : value - byte read, invoked from consumer task
   Description     : This function tests sync word, if found then move to next state
   Return          : none
 */
void
gst_pes_parse_sync0 (GstPESFilter * filter, guint8 value)
{
  if (value == PES_MP2_PARSE_DATA_SYNC0) {
    filter->ParserState = PES_MP2_PARSE_STATE_SYNC1;
  }
}

/*
   Function  : gst_pes_parse_sync1
   Input           : filter - pes filter pointer
                   : value - byte read, invoked from consumer task
   Description     : This function tests sync word, if found then move to next state else
                     return back to start looking for start of pes sync word
   Return          : none
 */
void
gst_pes_parse_sync1 (GstPESFilter * filter, guint8 value)
{
  if (value == PES_MP2_PARSE_DATA_SYNC1) {
    filter->ParserState = PES_MP2_PARSE_STATE_SYNC2;
    filter->ReadOffset++;
  } else {
    filter->ParserState = PES_MP2_PARSE_STATE_SYNC0;
  }
}

/*
   Function   : gst_pes_parse_sync2
   Input           : filter - pes filter pointer
                   : value - byte read, invoked from consumer task
   Description     : This function tests sync word, if found then move to next state else
                     return back to start looking for start of pes sync word
   Return          : none
 */
void
gst_pes_parse_sync2 (GstPESFilter * filter, guint8 value)
{
  if (value == PES_MP2_PARSE_DATA_SYNC2) {
    filter->ParserState = PES_MP2_PARSE_STATE_STREAMID;
    filter->ReadOffset++;
  } else if (value == PES_MP2_PARSE_DATA_SYNC1) {
    filter->ParserState = PES_MP2_PARSE_STATE_SYNC1;
  } else {
    filter->ParserState = PES_MP2_PARSE_STATE_SYNC0;
  }
}

/*
   Function  : gst_pes_parse_stream_id
   Input           : filter - pes filter pointer
                   : value - byte read, invoked from consumer task
   Description     : This function tests streamid as per standard
   Return          : none
 */
void
gst_pes_parse_stream_id (GstPESFilter * filter, guint8 value)
{
  if (PES_MP2_PARSE_DATA_STREAMID != value) {
    filter->ParserState = PES_MP2_PARSE_STATE_SYNC0;
  } else {
    filter->ParserState = PES_MP2_PARSE_STATE_PCKT_LENGTH0;
  }
}

/*
   Function   : gst_pes_parse_length
   Input           : filter - pes filter pointer
                   : value - byte read, invoked from consumer task
   Description     : This function calculates complete PES packet length
   Return          : none
 */
void
gst_pes_parse_length (GstPESFilter * filter, guint8 value)
{
  filter->PacketLength[DATA_OFFSET_FOR_BYTE1] = value;
  filter->TotalPacketLength =
      EXTRACT_PCKT_LENGTH (filter->PacketLength[DATA_OFFSET_FOR_BYTE0],
      filter->PacketLength[DATA_OFFSET_FOR_BYTE1]);
  GST_DEBUG ("TotalPacketLength=%u", filter->TotalPacketLength);
  filter->ParserState = PES_MP2_PARSE_STATE_DATA0;
}

/*
   Function   : gst_pes_parse_data0
   Input           : filter - pes filter pointer
                   : value - byte read, invoked from consumer task
   Description     : This function tests for stream error
   Return          : none
 */
void
gst_pes_parse_data0 (GstPESFilter * filter, guint8 value)
{
  if ((value & MASK_BIT_SEVEN_TO_SIX) == PES_MP2_PARSE_DATA0) {
    filter->ParserState = PES_MP2_PARSE_STATE_DATA1;
  } else {
    GST_ERROR ("PES MPEG2 Stream Error");
  }
}

/*
   Function   : gst_pes_parse_data1
   Input           : filter - pes filter pointer
                   : value - byte read, invoked from consumer task
   Description     : This function copies necessary flags in control block while
                      parsing PES data
   Return          : none
*/
void
gst_pes_parse_data1 (GstPESFilter * filter, guint8 value)
{
  filter->ptsdtsflag =
      ((value & MASK_BIT_SEVEN_TO_SIX) >> SHIFT_WORD_BY_SIX_BIT);
  filter->headerflag = value;
  filter->ParserState = PES_MP2_PARSE_STATE_HEADER_LENGTH0;
}

/*
   Function   : gst_pes_parse_skip_header
   Input           : filter - pes filter pointer
   Description     : This function calculates RemainingBytesForCurrentPES and updates
                      parser state, reports error in case of invalid size
   Return          : none
 */
void
gst_pes_parse_skip_header (GstPESFilter * filter)
{
  guint32 ByteToSkip = 0;
  guint32 RemainingBytesForCurrentPES = 0;
  ByteToSkip =
      CAL_LESSER_VAL (filter->BytesToSkipInPESHeader,
      (filter->ByteOffset - filter->ReadOffset));
  filter->BytesToSkipInPESHeader -= ByteToSkip;
  filter->ReadOffset += ByteToSkip;

  if (filter->BytesToSkipInPESHeader == 0) {
    filter->RemainingBytesForCurrentPES =
        filter->TotalPacketLength - (PES_EXTN_HEADER_SKIP +
        filter->PacketHeaderLength);
    RemainingBytesForCurrentPES = filter->RemainingBytesForCurrentPES;
    if (RemainingBytesForCurrentPES > 0
        && RemainingBytesForCurrentPES < ES_BUFFER_SIZE) {
      filter->ParserState = PES_MP2_PARSE_STATE_ES_DATA;
    } else {
      if (RemainingBytesForCurrentPES > ES_BUFFER_SIZE) {
        GST_ERROR
            ("Increment ES_BUFFER_SIZE, it's less than subtitle payload, dropping subtitle.. ");
      } else {
        GST_ERROR ("gst_pes_parse_skip_header: Error: Invalid Size");
      }
      filter->ParserState = PES_MP2_PARSE_STATE_SYNC0;
    }
  }
}

/*
   Function   : gst_pes_parse_es_payload
   Input           : filter - pes filter pointer
   Description     : This function copies es payload from circular buffer to ES buffer whose
                     max size is kept as ES_BUFFER_SIZE, the payload is copied and invokes
                     decoding callback, if complete bytes required for es payload are not
                     available in buffer then it partially copies it in ES Buffer
   Return          : none
*/
void
gst_pes_parse_es_payload (GstPESFilter * filter)
{
  GstBuffer *out;
  guint32 bytesavailable = 0;
  GstMapInfo info_write;

  bytesavailable = filter->ByteOffset - filter->ReadOffset;
  if (filter->RemainingBytesForCurrentPES <= bytesavailable) {
    GST_DEBUG ("RemainingBytesForCurrentPES=%u RO=%u, WO=%u",
        filter->RemainingBytesForCurrentPES, filter->ReadOffset,
        filter->WriteOffset);
    memcpy (filter->ESBufBase_p + filter->ESBufBaseOffset,
        &filter->PESBufBase_p[filter->ReadOffset],
        filter->RemainingBytesForCurrentPES);
    filter->ESBufBaseOffset += filter->RemainingBytesForCurrentPES;
    out = gst_buffer_new_and_alloc (filter->ESBufBaseOffset);
    if (out == NULL) {
      GST_WARNING ("failed to allocate buffer");
      return;
    }

    gst_buffer_map (out, &info_write, GST_MAP_WRITE);
    memcpy (info_write.data, filter->ESBufBase_p, filter->ESBufBaseOffset);
    gst_buffer_unmap (out, &info_write);
    GST_DEBUG ("final ES Payload: Size=%d Buffer Address:%p",
        filter->ESBufBaseOffset, filter->ESBufBase_p);
    gst_pes_filter_data_push (filter, TRUE, out);
    filter->ReadOffset += filter->RemainingBytesForCurrentPES;
    filter->RemainingBytesForCurrentPES = 0;
    filter->ParserState = PES_MP2_PARSE_STATE_SYNC0;
    filter->ESBufBaseOffset = 0;
  } else {
    GST_DEBUG ("Bytesavailable= %u RO=%u WO=%u", bytesavailable,
        filter->ReadOffset, filter->WriteOffset);
    memcpy (filter->ESBufBase_p + filter->ESBufBaseOffset,
        &filter->PESBufBase_p[filter->ReadOffset], bytesavailable);
    filter->ESBufBaseOffset += bytesavailable;
    filter->RemainingBytesForCurrentPES -= bytesavailable;
    filter->ReadOffset += bytesavailable;
  }
}

/*
   Function   : gst_pes_parse_header_length
   Input           : filter - pes filter pointer
                   : value - byte read, invoked from consumer task
   Description     : This function calculates Bytes to skip in PESHeader based
                     on PacketHeaderLength & ptsdts flags
   Return          : none
 */
void
gst_pes_parse_header_length (GstPESFilter * filter, guint8 value)
{
  filter->PacketHeaderLength = value;
  if (filter->PacketHeaderLength) {
    if (filter->ptsdtsflag == PTS_DTS_FLAG_1) {
      if (filter->PacketHeaderLength < PTS_HEADER_BYTE) {
        GST_ERROR ("PES MPEG2 Stream Error");
        filter->ParserState = PES_MP2_PARSE_STATE_SYNC1;
      } else {
        filter->BytesToSkipInPESHeader =
            filter->PacketHeaderLength - PES_EXTN_HEADER_BYTE;
        filter->ParserState = PES_MP2_PARSE_STATE_PTS_32TO30;
      }
    } else if (filter->ptsdtsflag == PTS_DTS_FLAG_2) {
      if (filter->PacketHeaderLength < PTS_DTS_HEADER_BYTE) {
        GST_ERROR ("PES MPEG2 Stream Error");
        filter->ParserState = PES_MP2_PARSE_STATE_SYNC1;
      } else {
        filter->BytesToSkipInPESHeader =
            filter->PacketHeaderLength - PES_EXTN_HEADER_BYTE;
        filter->ParserState = PES_MP2_PARSE_STATE_PTS_32TO30;
      }
    } else {
      filter->BytesToSkipInPESHeader = filter->PacketHeaderLength;
      filter->ParserState = PES_MP2_PARSE_STATE_SKIP_HEADER;
    }
  } else {
    filter->BytesToSkipInPESHeader = 0;
    filter->ParserState = PES_MP2_PARSE_STATE_SKIP_HEADER;
  }
}

/*
   Function   : gst_pes_parse_calc_pts
   Input           : filter - pes filter pointer
                   : value - byte read, invoked from consumer task
   Description     : This function computes actual pts based on PTS bytes stored inside
                      PTS array member of GstPESFilter control block
   Return          : none
*/
void
gst_pes_parse_calc_pts (GstPESFilter * filter, guint8 value)
{
  filter->PTS[DATA_OFFSET_FOR_BYTE4] = value;
  filter->pts =
      ((guint64) (filter->PTS[DATA_OFFSET_FOR_BYTE0] & MASK_BIT_THREE_TO_ONE))
      << SHIFT_WORD_BY_TWENTRY_NINE_BIT;
  filter->pts |=
      ((guint64) (filter->PTS[DATA_OFFSET_FOR_BYTE1])) <<
      SHIFT_WORD_BY_TEWNTY_TWO_BIT;
  filter->pts |=
      ((guint64) (filter->PTS[DATA_OFFSET_FOR_BYTE2] & MASK_BIT_SEVEN_TO_ONE))
      << SHIFT_WORD_BY_FOURTEEN_BIT;
  filter->pts |=
      ((guint64) (filter->PTS[DATA_OFFSET_FOR_BYTE3])) <<
      SHIFT_WORD_BY_SEVEN_BIT;
  filter->pts |=
      ((guint64) (filter->PTS[DATA_OFFSET_FOR_BYTE4] & MASK_BIT_SEVEN_TO_ONE))
      >> SHIFT_WORD_BY_ONE_BIT;
  filter->ParserState = PES_MP2_PARSE_STATE_SKIP_HEADER;
}

/*
   Function : gst_pes_parse_check_state
   Input           : filter - pes filter pointer
                   : value - byte read, invoked from consumer task
   Description     : This function implements pes parser state machine
   Return          : none
*/
void
gst_pes_parse_check_state (GstPESFilter * filter, guint8 value)
{
  switch (filter->ParserState) {
    case PES_MP2_PARSE_STATE_SYNC0:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_SYNC0");
      gst_pes_parse_sync0 (filter, value);
      filter->ReadOffset++;
      break;
    case PES_MP2_PARSE_STATE_SYNC1:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_SYNC1");
      gst_pes_parse_sync1 (filter, value);
      break;
    case PES_MP2_PARSE_STATE_SYNC2:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_SYNC2");
      gst_pes_parse_sync2 (filter, value);
      break;
    case PES_MP2_PARSE_STATE_STREAMID:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_STREAMID");
      gst_pes_parse_stream_id (filter, value);
      filter->ReadOffset++;
      break;
    case PES_MP2_PARSE_STATE_PCKT_LENGTH0:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_PCKT_LENGTH0");
      filter->PacketLength[DATA_OFFSET_FOR_BYTE0] = value;
      filter->ParserState = PES_MP2_PARSE_STATE_PCKT_LENGTH1;
      filter->ReadOffset++;
      break;
    case PES_MP2_PARSE_STATE_PCKT_LENGTH1:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_PCKT_LENGTH1");
      gst_pes_parse_length (filter, value);
      filter->ReadOffset++;
      break;
    case PES_MP2_PARSE_STATE_DATA0:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_DATA0");
      gst_pes_parse_data0 (filter, value);
      filter->ReadOffset++;
      break;
    case PES_MP2_PARSE_STATE_DATA1:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_DATA1");
      gst_pes_parse_data1 (filter, value);
      filter->ReadOffset++;
      break;
    case PES_MP2_PARSE_STATE_HEADER_LENGTH0:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_HEADER_LENGTH0");
      gst_pes_parse_header_length (filter, value);
      filter->ReadOffset++;
      break;
    case PES_MP2_PARSE_STATE_PTS_32TO30:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_PTS_32TO30");
      filter->ParserState = PES_MP2_PARSE_STATE_PTS_29TO22;
      filter->PTS[DATA_OFFSET_FOR_BYTE0] = value;
      filter->ReadOffset++;
      break;
    case PES_MP2_PARSE_STATE_PTS_29TO22:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_PTS_29TO22");
      filter->ParserState = PES_MP2_PARSE_STATE_PTS_21TO15;
      filter->PTS[DATA_OFFSET_FOR_BYTE1] = value;
      filter->ReadOffset++;
      break;
    case PES_MP2_PARSE_STATE_PTS_21TO15:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_PTS_21TO15");
      filter->ParserState = PES_MP2_PARSE_STATE_PTS_14TO7;
      filter->PTS[DATA_OFFSET_FOR_BYTE2] = value;
      filter->ReadOffset++;
      break;
    case PES_MP2_PARSE_STATE_PTS_14TO7:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_PTS_14TO7");
      filter->ParserState = PES_MP2_PARSE_STATE_PTS_6TO0;
      filter->PTS[DATA_OFFSET_FOR_BYTE3] = value;
      filter->ReadOffset++;
      break;
    case PES_MP2_PARSE_STATE_PTS_6TO0:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_PTS_6TO0");
      gst_pes_parse_calc_pts (filter, value);
      filter->ReadOffset++;
      break;
    case PES_MP2_PARSE_STATE_SKIP_HEADER:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_SKIP_HEADER");
      gst_pes_parse_skip_header (filter);
      break;
    case PES_MP2_PARSE_STATE_ES_DATA:
      GST_DEBUG ("State:PES_MP2_PARSE_STATE_ES_DATA");
      gst_pes_parse_es_payload (filter);
      break;
    default:
      GST_DEBUG ("default case");
      break;
  }
}

/*
   Function   : gst_pes_filter_data_push
   Input           : filter - pes filter pointer
                   : first - unused
                   : buffer - es payload buffer
   Description     : This function invokes dvb subtitle/teletext es payload decoder callback
                      function [gst_stm_subtitle_es_cb] which further invokes native decoder
   Return          : GstFlowReturn
 */
static GstFlowReturn
gst_pes_filter_data_push (GstPESFilter * filter, gboolean first,
    GstBuffer * buffer)
{
  GstFlowReturn ret;
  GST_DEBUG ("pushing, first: %d", first);

  if (filter->data_cb) {
    ret = filter->data_cb (filter, first, buffer, filter->user_data);
  } else {
    gst_buffer_unref (buffer);
    ret = GST_FLOW_OK;
  }
  return ret;
}

/*
   Function  : gst_pes_filter_push
   Input           : filter - pes filter pointer
                   : buffer - buffer carrying pes payload (complete or incomplete)
   Description     : This function writes into the circular buffer based on available free space,
                      size of PES Buffer is governed by PES_BUFFER_SIZE,
                     it drops input buffer if circular buffer is already full
   Return          : GST_FLOW_OK/GST_FLOW_ERROR
 */
GstFlowReturn
gst_pes_filter_push (GstPESFilter * filter, GstBuffer * buffer)
{
  guint8 *data_p = NULL;
  guint32 sizetowrite = 0, remainsize = 0, sizewrap = 0;
  gint32 availablespacetowrite = 0;
  guint32 ReadOffset = 0;
  GstMapInfo info_read;

  g_return_val_if_fail (filter != NULL, GST_FLOW_ERROR);
  g_return_val_if_fail (buffer != NULL, GST_FLOW_ERROR);

  gst_buffer_map (buffer, &info_read, GST_MAP_READ);
  data_p = info_read.data;
  sizetowrite = info_read.size;
  /*lockless: read once and use it, ReadOffset may change */
  ReadOffset = filter->ReadOffset;
  if (ReadOffset > filter->WriteOffset) {
    /* actual available space in circular buffer */
    availablespacetowrite = ReadOffset - filter->WriteOffset - 1;
    if (!availablespacetowrite)
      GST_ERROR ("buffer is full: no write [availablespacetowrite=%d]",
          availablespacetowrite);
    if (sizetowrite > availablespacetowrite) {
      GST_ERROR ("insufficient space: sizetowrite=%u availablespacetowrite=%d",
          sizetowrite, availablespacetowrite);
      sizetowrite = availablespacetowrite;
    }
    memcpy (filter->PESBufBase_p + filter->WriteOffset, data_p, sizetowrite);
    filter->WriteOffset += sizetowrite;
  } else {
    /* actual available space in circular buffer */
    availablespacetowrite =
        PES_BUFFER_SIZE - filter->WriteOffset + ReadOffset - 1;
    if (availablespacetowrite > 0) {
      if (sizetowrite > availablespacetowrite) {
        GST_ERROR
            ("insufficient space: sizetowrite=%u availablespacetowrite=%d",
            sizetowrite, availablespacetowrite);
        sizetowrite = availablespacetowrite;
      }
      if (sizetowrite <= PES_BUFFER_SIZE - filter->WriteOffset) {
        memcpy (filter->PESBufBase_p + filter->WriteOffset, data_p,
            sizetowrite);
        filter->WriteOffset += sizetowrite;
      } else {
        sizewrap = PES_BUFFER_SIZE - filter->WriteOffset;
        memcpy (filter->PESBufBase_p + filter->WriteOffset, data_p, sizewrap);
        remainsize = sizetowrite - (PES_BUFFER_SIZE - filter->WriteOffset);
        filter->WriteOffset = 0;
        memcpy (filter->PESBufBase_p, data_p + sizewrap, remainsize);
        filter->WriteOffset += remainsize;
      }
    } else {
      GST_ERROR ("buffer is full: no write [availablespacetowrite=%d]",
          availablespacetowrite);
    }
  }
  gst_buffer_unmap (buffer, &info_read);
  /* signal consumer task to process pes packets */
  gst_pes_sem_up (&filter->pes_es_sync_sem);
  return GST_FLOW_OK;
}

/*
   Function   : gst_pes_es_task (consumer task)
   Input           : data - pes filter control block pointer
   Description     : consumer task to process pes data in circular buffer, invokes
                      parser state machine, pes to es conversion and dvb subtitle/teletext
                      decode callback, it also exits from gst_pes_es_task in gstreamer
                      purview after every es/payload processing to allow acceptance of commands
                      from top like flush pes filter
   Return          : none
*/
void
gst_pes_es_task (void *data)
{
  GstPESFilter *filter = (GstPESFilter *) data;
  guint32 WriteOffset = 0;

  if (!filter->taskexit)
    gst_pes_sem_down (&filter->pes_es_sync_sem);
  WriteOffset = filter->WriteOffset;
  if (filter->ReadOffset < WriteOffset) {
    filter->ByteOffset = WriteOffset;
    while (filter->ReadOffset < WriteOffset && !filter->parserflush) {
      gst_pes_parse_check_state (filter,
          filter->PESBufBase_p[filter->ReadOffset]);
    }
  } else {
    if (filter->ReadOffset != WriteOffset) {
      filter->ByteOffset = PES_BUFFER_SIZE;
      while (filter->ReadOffset < PES_BUFFER_SIZE && !filter->parserflush) {
        gst_pes_parse_check_state (filter,
            filter->PESBufBase_p[filter->ReadOffset]);
      }
      filter->ReadOffset = 0;
      filter->ByteOffset = WriteOffset;
      while (filter->ReadOffset < WriteOffset && !filter->parserflush) {
        gst_pes_parse_check_state (filter,
            filter->PESBufBase_p[filter->ReadOffset]);
      }
    }
  }
}

/*
   Function   : gst_pes_filter_dbg_init
   Input           : none
   Description     : This function initializes debugging log interface for the element
   Return          : none
*/
void
gst_pes_filter_dbg_init ()
{
  GST_DEBUG_CATEGORY_INIT (gststpesfilter_debug, "stmpegpesfilter", 0,
      "ST MPEG-TS/PS PES filter output");
  return;
}
