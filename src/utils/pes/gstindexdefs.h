/*
 * This library is licensed under 2 different licenses and you
 * can choose to use it under the terms of either one of them. The
 * two licenses are the MPL 1.1 and the LGPL.
 *
 * MPL:
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * LGPL:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * The Original Code is Fluendo MPEG Demuxer plugin.
 *
 * The Initial Developer of the Original Code is Fluendo, S.L.
 * Portions created by Fluendo, S.L. are Copyright (C) 2005
 * Fluendo, S.L. All Rights Reserved.
 *
 * Contributor(s): Wim Taymans <wim@fluendo.com>
 */

#ifndef __GST_INDEX_DEFS_H__
#define __GST_INDEX_DEFS_H__


/* MPEG1/2 Start Codes
 * video-iso13818-2.pdf
 * Table 6-1 - Start code values
 * Picture_start_code     00
 * Sequence_header_code   B3
 * picture_start_code -- The picture_start_code is a string of 32 bits having
 * the value 0x00000100
 * After 32bit picture start code, we have temporal_reference of 10 bits and
 * then next 3 bits contains picture_coding_type
 * Table 6-12 - picture coding type
 * 000 forbidden
 * 001 intra-coded (I)
 * 010 predictive-coded (P)
 * 011 bidirectionally-predictive-coded (B)
 * ....
 */
#define MPEG_SEQUENCE_HEADER_SC         (0xB3)
#define MPEG_PICTURE_SC                 (0x00)
#define MPEG_PICTURE_CODING_TYPE_P      (0x02)
#define MPEG_PICTURE_CODING_TYPE_B      (0x03)
#define MPEG_PICTURE_TYPE_MASK          (0x38)

/* H.264 Start Codes
 ******************************************************************************
 * T-REC-H.264-200503-S!!PDF-E.pdf
 * Section 7.3.1 NAL Unit syntax
 * forbidden zero bit | nal_ref_idc u(2) | nal_unit_type u(5)
 * Section: 7.4.1. nal_ref_idc : not equal to 0 specifies that the content of
 * NAL unit contains a sps or pps or a slice of a reference picture or a slice
 * data partition of a reference picture
 * This leaves us with 3 possible valus of nal_ref_idc, namely 01, 10, 11
 * We search for nal_unit_type equal to SPS, which has value 7.
 * This leaves us with three start sequences to search for:
 * 0 01 00111 (0x27), 0 10 00111 (0x47), 0 11 00111 (0x67)
 * Similarly, for start sequence of a non IDR picture, we search for start code
 * having nal_ref_idc equal to 0 and nal unit type set to "Coded slice of a
 * non-IDR picture.
 *****************************************************************************/

#define H264_SPS_SC_1                   (0x27)
#define H264_SPS_SC_2                   (0x47)
#define H264_SPS_SC_3                   (0x67)
#define H264_NON_IDR_FRAME_SC_1         (0x09)
#define H264_NON_IDR_FRAME_SC_2         (0x01)

/******************************************************************************
 * HEVC start code sequences for IRAP and non IRAP pictures
 *
 * T-REC-H.265-201304-I!!PDF-E.pdf
 * Table 7-1 NAL unit type codes and NAL unit type classes
 * For Non IRAP pictures, following nal_unit_types are searched for:
 * TRAIL_N  0
 * TRAIL_R  1
 * TSA_N    2
 * TSA_R    3
 * STSA_N   4
 * STSA_R   5
 * RADL_N   6
 * RADL_R   7
 * RASL_N   8
 * RASL_R   9
 *
 * For IRAP pictures, following nal_unit_types are searched for:
 * SPS_NUT      33
 * BLA_W_LP     16
 * BLA_W_RADL   17
 * BLA_N_LP     18
 * IDR_W_RADL   19
 * IDR_N_LP     20
 * CRA_NUT      21
 * Table 7.3.1.2 Nal unit header syntax
 * forbidden zero bit | nal_unit_type u(6) | nuh_layer_id (u6) | nuh_temporalid_plus1 u(3)
 * Currently spec specifies that nuh_layer_id should be 0.
 * Therefore, reading first byte of HEVC nal header, which is forbidden bit + nal_unit_type
 * (SPS_NUT) + 1 bit of nuh_layer_id, gives us below start codes.
 * e.g. 0 010000 0 => 0x20
 *****************************************************************************/

#define HEVC_IRAP_SC_1                  (0x20)
#define HEVC_IRAP_SC_2                  (0x22)
#define HEVC_IRAP_SC_3                  (0x24)
#define HEVC_IRAP_SC_4                  (0x26)
#define HEVC_IRAP_SC_5                  (0x28)
#define HEVC_IRAP_SC_6                  (0x2A)
#define HEVC_SPS_SC                     (0x42)

#define HEVC_NON_IRAP_FRAME_SC_1         (0x00)
#define HEVC_NON_IRAP_FRAME_SC_2         (0x02)
#define HEVC_NON_IRAP_FRAME_SC_3         (0x04)
#define HEVC_NON_IRAP_FRAME_SC_4         (0x06)
#define HEVC_NON_IRAP_FRAME_SC_5         (0x08)
#define HEVC_NON_IRAP_FRAME_SC_6         (0x0A)
#define HEVC_NON_IRAP_FRAME_SC_7         (0x0C)
#define HEVC_NON_IRAP_FRAME_SC_8         (0x0E)
#define HEVC_NON_IRAP_FRAME_SC_9         (0x10)
#define HEVC_NON_IRAP_FRAME_SC_10        (0x12)

/* AVS Start Codes
 ******************************************************************************
 * Sequence_header_code   B0
 * P/B start_code         B6
 *****************************************************************************/
#define AVS_SEQUENCE_HEADER_SC           (0xB0)
#define AVS_P_B_PICTURE_SC               (0xB6)

#endif /* __GST_INDEX_DEFS_H__ */
