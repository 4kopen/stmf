/*
 * This library is licensed under 2 different licenses and you
 * can choose to use it under the terms of either one of them. The
 * two licenses are the MPL 1.1 and the LGPL.
 *
 * MPL:
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * LGPL:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * The Original Code is Fluendo MPEG Demuxer plugin.
 *
 * The Initial Developer of the Original Code is Fluendo, S.L.
 * Portions created by Fluendo, S.L. are Copyright (C) 2005
 * Fluendo, S.L. All Rights Reserved.
 *
 * Contributor(s): Wim Taymans <wim@fluendo.com>
 *
 * This file has been modified by STMicroelectronics, on 03/13/2012
 */

#ifndef __GST_PES_FILTER_H__
#define __GST_PES_FILTER_H__

#include <gst/gst.h>
#include <gst/base/gstadapter.h>
#include <string.h>
#include "gstmpegdefs.h"

G_BEGIN_DECLS typedef struct _GstPESFilter GstPESFilter;

typedef GstFlowReturn (*GstPESFilterData) (GstPESFilter * filter,
    gboolean first, GstBuffer * buffer, gpointer user_data);

typedef void (*GstPESFilterResync) (GstPESFilter * filter, gpointer user_data);

typedef void (*GstPESFilterIndex) (GstPESFilter * filter, gpointer user_data);

typedef struct _GstPesSema GstPesSema;

typedef enum
{
  PES_MP2_PARSE_STATE_SYNC0 = 0,
  PES_MP2_PARSE_STATE_SYNC1,
  PES_MP2_PARSE_STATE_SYNC2,
  PES_MP2_PARSE_STATE_STREAMID,
  PES_MP2_PARSE_STATE_PCKT_LENGTH0,
  PES_MP2_PARSE_STATE_PCKT_LENGTH1,
  PES_MP2_PARSE_STATE_DATA0,
  PES_MP2_PARSE_STATE_DATA1,
  PES_MP2_PARSE_STATE_HEADER_LENGTH0,
  PES_MP2_PARSE_STATE_PTS_32TO30,
  PES_MP2_PARSE_STATE_PTS_29TO22,
  PES_MP2_PARSE_STATE_PTS_21TO15,
  PES_MP2_PARSE_STATE_PTS_14TO7,
  PES_MP2_PARSE_STATE_PTS_6TO0,
  PES_MP2_PARSE_STATE_SKIP_HEADER,
  PES_MP2_PARSE_STATE_ES_DATA,
} GstPESFilterState;

/* PES buffer size can be revisited in future if required */
#define PES_BUFFER_SIZE 50*10*1024
/* ES buffer size can be revisited */
#define ES_BUFFER_SIZE 30*1024

#define PES_MP2_PARSE_DATA_SYNC0 0x00
#define PES_MP2_PARSE_DATA_SYNC1 0x00
#define PES_MP2_PARSE_DATA_SYNC2 0x01
#define PES_MP2_PARSE_DATA_STREAMID 0xBD
#define PES_MP2_PARSE_DATA0 0x80

#define DATA_OFFSET_FOR_BYTE0 0
#define DATA_OFFSET_FOR_BYTE1 1
#define DATA_OFFSET_FOR_BYTE2 2
#define DATA_OFFSET_FOR_BYTE3 3
#define DATA_OFFSET_FOR_BYTE4 4
#define PES_PACKET_LENGTH_IN_BYTES 2
#define PTS_SIZE_IN_BYTES 5
#define PTS_HEADER_BYTE 5
#define PTS_DTS_HEADER_BYTE 10
#define PES_EXTN_HEADER_SKIP 3
#define PES_EXTN_HEADER_BYTE 5
#define PTS_DTS_FLAG_1 0x2
#define PTS_DTS_FLAG_2 0x3

/* bit masking helpers */
#define MASK_FOUR_LSB_BYTES 0xFFFFFFFF
#define MASK_THREE_LSB_BYTES 0x00FFFFFF
#define MASK_TWO_LSB_BYTES 0x0000FFFF
#define MASK_ONE_LSB_BYTE 0x000000FF
#define MASK_BIT_THREE_TO_ONE 0x0E
#define MASK_BIT_SEVEN_TO_ONE 0xFE
#define MASK_BIT_SEVEN_TO_SIX 0xC0

/* bit shift helpers */
#define SHIFT_WORD_BY_ONE_BYTE 8
#define SHIFT_WORD_BY_TWO_BYTE 16
#define SHIFT_WORD_BY_THREE_BYTE 24
#define SHIFT_WORD_BY_FOUR_BYTE 32
#define SHIFT_WORD_BY_ZERO_BIT 0
#define SHIFT_WORD_BY_ONE_BIT 1
#define SHIFT_WORD_BY_TWO_BIT 2
#define SHIFT_WORD_BY_THREE_BIT 3
#define SHIFT_WORD_BY_ONE_NIBBLE 4
#define SHIFT_WORD_BY_SIX_BIT 6
#define SHIFT_WORD_BY_TWENTRY_NINE_BIT 29
#define SHIFT_WORD_BY_TEWNTY_TWO_BIT 22
#define SHIFT_WORD_BY_FOURTEEN_BIT 14
#define SHIFT_WORD_BY_SEVEN_BIT 7

#define LSHIFT_WORD_BY_X_BYTES(Word,ByteShift)          (Word << ByteShift)
#define RSHIFT_WORD_BY_X_BYTES(Word,ByteShift)          (Word >> ByteShift)
#define LSHIFT_BYTE_BY_X_BITS(Byte,BitShift)            (Byte << BitShift)
#define RSHIFT_BYTE_BY_X_BITS(Byte,BitShift)            (Byte >> BitShift)
#define EXTRACT_PCKT_LENGTH(Byte0,Byte1)                (LSHIFT_WORD_BY_X_BYTES(Byte0,SHIFT_WORD_BY_ONE_BYTE)+(Byte1))
#define EXTRACT_PTS_FLAG(Byte0)                         (RSHIFT_WORD_BY_X_BYTES(Byte0,(SHIFT_WORD_BY_ONE_BYTE - SHIFT_WORD_BY_TWO_BIT))& (MASK_TWO_BITS))
#define EXTRACT_PTS_LENGTH(Byte0,BitShift,BitMask)      ((RSHIFT_BYTE_BY_X_BITS(Byte0,BitShift)& (BitMask)) * PTS_SIZE_IN_BYTES)
#define EXTRACT_PTS_VALUE(Byte0,BitShift,BitMask)       (RSHIFT_BYTE_BY_X_BITS(((Byte0 & BitMask),BitShift))
#define CAL_GREATER_VAL(Val1,Val2)                      ((Val1>=Val2)?Val1:Val2)
#define CAL_LESSER_VAL(Val1,Val2)                       ((Val1<=Val2)?Val1:Val2)
#define ABSOLUTE_SUBSTRACTION(Val1,Val2)                ((Val1>=Val2)?(Val1-Val2):(Val2-Val1))

/**
 * _GstAncillarySem:
 *
 * structure for ancillary semaphore
 */
struct _GstPesSema
{
  GCond condition;
  GMutex mutex;
  guint counter;
};

struct _GstPESFilter
{
  guint8 *PESBufBase_p;         /* PES buffer base address */
  guint8 *ESBufBase_p;          /* ES Buffer address */
  guint32 ReadOffset;           /* Read Offset */
  guint32 WriteOffset;          /* Write Offset */
  guint32 ByteOffset;
  guint32 ESBufBaseOffset;      /* ES Buffer Offset */
  GstPESFilterState ParserState;        /* Parser state */
  guint8 PacketLength[PES_PACKET_LENGTH_IN_BYTES];      /* Packet Length */
  guint8 PacketHeaderLength;    /* Packet Header Length */
  guint32 TotalPacketLength;    /* Total Packet Length */
  guint32 RemainingBytesForCurrentPES;
  guint32 BytesToSkipInPESHeader;
  guint8 PTS[PTS_SIZE_IN_BYTES];        /* PTS bytes */
  GstPESFilterData data_cb;     /* decoder callback */
  GstPESFilterResync resync_cb; /* unused */
  gpointer user_data;           /* callback data */
  guint8 ptsdtsflag;            /* actual pts dts flag */
  guint8 headerflag;            /* pts dts original byte */
  gint64 pts;                   /* computed pts */
  gint64 dts;                   /* dts, curently unused */
  gboolean taskexit;
  GstTask *pes_es_task;         /*task pointer */
  GRecMutex pes_es_task_mutex;
  GstPesSema pes_es_sync_sem;   /* semaphore premitives */
  gboolean completepkt;         /* complete es packet */
  gboolean parserflush;         /* parser flush ongoing */
};
/* pes filter initialization */
void gst_pes_filter_init (GstPESFilter * filter, GstAdapter * adapter,
    guint64 * adapter_offset);
/* pes filter un-initialization */
void gst_pes_filter_uninit (GstPESFilter * filter);
/* pes filter callback setup */
void gst_pes_filter_set_callbacks (GstPESFilter * filter,
    GstPESFilterData data_cb, GstPESFilterResync resync_cb, gpointer user_data);
/* writes pes packets into circular buffer */
GstFlowReturn gst_pes_filter_push (GstPESFilter * filter, GstBuffer * buffer);
/* flush pes filter */
GstFlowReturn gst_pes_filter_drain (GstPESFilter * filter);
/* initialize debugging log interface */
void gst_pes_filter_dbg_init ();

G_END_DECLS
#endif /* __GST_PES_FILTER_H__ */
