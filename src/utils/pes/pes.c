/* linuxdvb output/writer handling.
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <linux/dvb/video.h>
#include <linux/dvb/stm_ioctls.h>

#include "pes.h"

void
PutBits (BitPacker_t * ld, guint code, guint length)
{
  guint bit_buf;
  gint bit_left;

  bit_buf = ld->bit_buffer;
  bit_left = ld->remaining;

  if ((length < 32) && ((code & ~((1 << length) - 1)) != 0)) {
    GST_ERROR ("value 0x%x must be clipped to fit in %u bits", code, length);
    code = code & ((1 << length) - 1);
  }

  if (length < bit_left) {
    /* Fits into current 4 bytes */
    bit_buf = (bit_buf << length) | code;
    bit_left -= length;
  } else {
    /* Does not fit, write to remaining bytes */
    bit_buf <<= bit_left;
    bit_buf |= code >> (length - bit_left);
    ld->ptr[0] = (gchar) (bit_buf >> 24);
    ld->ptr[1] = (gchar) (bit_buf >> 16);
    ld->ptr[2] = (gchar) (bit_buf >> 8);
    ld->ptr[3] = (gchar) (bit_buf);
    /* Write into next 4 bytes */
    ld->ptr += 4;
    length -= bit_left;
    bit_buf = code & ((1 << length) - 1);
    bit_left = 32 - length;
  }

  /* Update to the latest value */
  ld->bit_buffer = bit_buf;
  ld->remaining = bit_left;
}

void
AlignBits (BitPacker_t * ld)
{
  PutBits (ld, 0, (ld->remaining & 7));
}

void
FlushBits (BitPacker_t * ld)
{
  ld->bit_buffer <<= ld->remaining;
  while (ld->remaining < 32) {
    *ld->ptr++ = ld->bit_buffer >> 24;
    ld->bit_buffer <<= 8;
    ld->remaining += 8;
  }
  ld->remaining = 32;
  ld->bit_buffer = 0;
}

gint
InsertVideoPrivateDataHeader (guint8 * data, gint payload_size, gint encoding)
{
  BitPacker_t ld2 = { data, 0, 32 };
  gint header_length;
  gint i;

  switch (encoding) {
    case VIDEO_ENCODING_H263:
    case VIDEO_ENCODING_FLV1:
    case VIDEO_ENCODING_VP6:
    case VIDEO_ENCODING_WMV:
    case VIDEO_ENCODING_RMV:
    case VIDEO_ENCODING_THEORA:
    case VIDEO_ENCODING_VP3:
    case VIDEO_ENCODING_VP8:
    case VIDEO_ENCODING_VP9:
      PutBits (&ld2, PES_PRIVATE_DATA_FLAG, 8);
      PutBits (&ld2, payload_size & 0xff, 8);
      PutBits (&ld2, (payload_size >> 8) & 0xff, 8);
      PutBits (&ld2, (payload_size >> 16) & 0xff, 8);

      for (i = 4; i < (PES_PRIVATE_DATA_LENGTH + 1); i++) {
        PutBits (&ld2, 0, 8);
      }

      FlushBits (&ld2);

      header_length = PES_PRIVATE_DATA_LENGTH + 1;
      break;

    default:
      header_length = 0;
      break;
  }

  return header_length;
}

gint
InsertAudioPrivateDataHeader (guint8 * data, gint payload_size, gint encoding)
{
  BitPacker_t ld2 = { data, 0, 32 };
  gint header_length;
  gint i;

  switch (encoding) {
    case AUDIO_ENCODING_RMA:
    case AUDIO_ENCODING_VORBIS:
      PutBits (&ld2, PES_PRIVATE_DATA_FLAG, 8);
      PutBits (&ld2, payload_size & 0xff, 8);
      PutBits (&ld2, (payload_size >> 8) & 0xff, 8);
      PutBits (&ld2, (payload_size >> 16) & 0xff, 8);

      for (i = 4; i < (PES_PRIVATE_DATA_LENGTH + 1); i++) {
        PutBits (&ld2, 0, 8);
      }

      FlushBits (&ld2);

      header_length = PES_PRIVATE_DATA_LENGTH + 1;
      break;

    default:
      header_length = 0;
      break;
  }

  return header_length;
}

gint
InsertPesHeader (guint8 * data, gint size, guint8 stream_id, guint64 pts,
    gint pic_start_code)
{
  BitPacker_t ld2 = { data, 0, 32 };

  /* PES packet start code */
  PutBits (&ld2, 0x0, 8);
  PutBits (&ld2, 0x0, 8);
  PutBits (&ld2, 0x1, 8);
  /* Stream_id */
  PutBits (&ld2, stream_id, 8);
  /* Byte 4 */
  /* PES_packet_length */
  if ((size + 13) >= G_MAXUINT16) {
    PutBits (&ld2, 0, 16);
  } else {
    PutBits (&ld2,
        size + 3 + (IS_PTS_VALUE_VALID (pts) ? 5 : 0) +
        (pic_start_code ? (5 /*+1 */ ) : 0), 16);
  }
  /* Byte 6 */
  /* Optional PES header */
  PutBits (&ld2, 0x2, 2);       /* 0x10                     */
  PutBits (&ld2, 0x0, 2);       /* PES_scrambling_control   */
  PutBits (&ld2, 0x0, 1);       /* PES priority             */
  PutBits (&ld2, 0x0, 1);       /* data alignment indicator */
  PutBits (&ld2, 0x0, 1);       /* copyright                */
  PutBits (&ld2, 0x0, 1);       /* original or copy         */
  /* Byte 7 */
  /* 7 flags */
  if (IS_PTS_VALUE_VALID (pts)) {       /* PTS DTS flag */
    PutBits (&ld2, 0x2, 2);
  } else {
    PutBits (&ld2, 0x0, 2);
  }
  PutBits (&ld2, 0x0, 1);       /* ESCR flag                 */
  PutBits (&ld2, 0x0, 1);       /* ES rate flag              */
  PutBits (&ld2, 0x0, 1);       /* DSM trickmode flag        */
  PutBits (&ld2, 0x0, 1);       /* additional copy info flag */
  PutBits (&ld2, 0x0, 1);       /* PES CRC flag              */
  PutBits (&ld2, 0x0, 1);       /* PES extension flag        */
  /* Byte 8 */
  /* PES header data length */
  if (IS_PTS_VALUE_VALID (pts)) {
    PutBits (&ld2, 0x5, 8);
  } else {
    PutBits (&ld2, 0x0, 8);
  }
  /* Byte 9 */
  /* PTS DTS */
  if (IS_PTS_VALUE_VALID (pts)) {
    PutBits (&ld2, 0x2, 4);
    PutBits (&ld2, (pts >> 30) & 0x7, 3);
    PutBits (&ld2, 0x1, 1);
    PutBits (&ld2, (pts >> 15) & 0x7fff, 15);
    PutBits (&ld2, 0x1, 1);
    PutBits (&ld2, pts & 0x7fff, 15);
    PutBits (&ld2, 0x1, 1);
  }
  /* Byte 14 */
  if (pic_start_code) {
    /* Start code */
    PutBits (&ld2, 0x0, 8);
    PutBits (&ld2, 0x0, 8);
    PutBits (&ld2, 0x1, 8);
    PutBits (&ld2, (pic_start_code & 0xff), 8); /* 00, for picture start */
    PutBits (&ld2, ((pic_start_code >> 8) & 0xff), 8);  /* For any extra information (like in mpeg4p2, the pic_start_code) */
  }

  FlushBits (&ld2);

  return (ld2.ptr - data);
}
