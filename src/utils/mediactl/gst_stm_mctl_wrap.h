/* Copyright (C) 2014 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STM_MCTL_FILE_H__
#define __GST_STM_MCTL_FILE_H__

#include <gst/gst.h>

G_BEGIN_DECLS gchar *gst_stm_mctl_get_entity_source (const gchar * entity);
gchar *gst_stm_mctl_get_entity_sink (const gchar * entity);

G_END_DECLS
#endif
