/* Copyright (C) 2014 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <gst/gst.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <linux/videodev2.h>
#include <fcntl.h>
#include <mediactl/mediactl.h>
#include "gst_stm_mctl_wrap.h"

#define MAX_ENTITY_LEN_EXPECTED 50      /* max number of character returned by media-ctl */
#define STM_DRIVER_NAME "stm_media"

/*
 function: gst_stm_mctl_get_entity_source
 input params:
           entity - entity name.
 description : this function will get device name connected.
               like dvb0.video0
 return : device name if successful or NULL. caller of this
          function should free returned string with g_free.
 */
gchar *
gst_stm_mctl_get_entity_source (const gchar * entity)
{
  struct media_device *md;
  struct media_entity *me;
  struct media_pad *sink_pad = NULL;
  struct media_link *link;
  gchar *source_info = NULL;
  gchar *source = NULL;
  char name[30] = { '\0' };
  gint i = 0;

  if (entity == NULL) {
    goto exit;
  }

  /* Seek for STM Media device */
  do {
    snprintf (name, sizeof (name), "/dev/media%d", i);
    md = media_open (name);

    if (NULL != md) {
      if (strncasecmp (md->info.driver, STM_DRIVER_NAME,
              strlen (STM_DRIVER_NAME)) == 0)
        /* Found */
        break;
      media_close (md);
    }
  } while (++i < 255);

  if (NULL == md) {
    /* could not open media */
    goto exit;
  }

  /* Search for the entity */
  me = media_get_entity_by_name (md, entity, strlen (entity));
  if (!me) {
    /* No media entity exists for given entity name */
    goto mediaclose;
  }

  /* Check if it has a sink pad */
  for (i = 0; i < me->info.pads; i++) {
    if (me->pads[i].flags == MEDIA_PAD_FL_SINK) {
      sink_pad = &me->pads[i];
    }
  }

  /* Go through links */
  for (i = 0; i < me->num_links; i++) {
    link = &me->links[i];

    /* Skip all links not related to that sink pad */
    if (link->sink != sink_pad)
      continue;

    if (link->flags & MEDIA_LNK_FL_ENABLED) {
      source_info = link->source->entity->info.name;
    }
  }

  if (source_info != NULL) {
    if (strlen (source_info) > MAX_ENTITY_LEN_EXPECTED) {
      /* unexpected entity length received from media-ctl */
      goto mediaclose;
    }
    source = (gchar *) g_malloc0 (strlen (source_info) + 1);
    if (source != NULL) {
      memcpy (source, source_info, strlen (source_info));
    }
  }

mediaclose:
  media_close (md);
exit:
  return source;
}

/*
 function: gst_stm_mctl_get_entity_sink
 input params:
           entity - entity name.
 description : this function will get sink name connected.
               like mixer0
 return : sink name if successfull else NULL. caller of this
          function should free returned string with g_free.
 */
gchar *
gst_stm_mctl_get_entity_sink (const gchar * entity)
{
  struct media_device *md;
  struct media_entity *me;
  struct media_pad *source_pad = NULL;
  struct media_link *link;
  gchar *sink_info = NULL;
  gchar *sink = NULL;
  char name[30] = { '\0' };
  gint i = 0;

  if (entity == NULL) {
    goto exit;
  }

  /* Seek for STM Media device */
  do {
    snprintf (name, sizeof (name), "/dev/media%d", i);
    md = media_open (name);

    if (NULL != md) {
      if (strncasecmp (md->info.driver, STM_DRIVER_NAME,
              strlen (STM_DRIVER_NAME)) == 0)
        /* Found */
        break;
      media_close (md);
    }
  } while (++i < 255);

  if (NULL == md) {
    /* could not open media */
    goto exit;
  }

  /* Search for the entity */
  me = media_get_entity_by_name (md, entity, strlen (entity));
  if (!me) {
    /* No media entity exists for given entity name */
    goto mediaclose;
  }

  /* Check if it has a sink pad */
  for (i = 0; i < me->info.pads; i++) {
    if (me->pads[i].flags == MEDIA_PAD_FL_SOURCE) {
      source_pad = &me->pads[i];
    }
  }

  /* Go through links */
  for (i = 0; i < me->num_links; i++) {
    link = &me->links[i];

    /* Skip all links not related to that sink pad */
    if (link->source != source_pad)
      continue;

    if (link->flags & MEDIA_LNK_FL_ENABLED) {
      sink_info = link->sink->entity->info.name;
    }
  }

  if (sink_info != NULL) {
    if (strlen (sink_info) > MAX_ENTITY_LEN_EXPECTED) {
      /* unexpected entity length received from media-ctl */
      goto mediaclose;
    }
    sink = (gchar *) g_malloc0 (strlen (sink_info) + 1);
    if (sink != NULL) {
      memcpy (sink, sink_info, strlen (sink_info));
    }
  }

mediaclose:
  media_close (md);
exit:
  return sink;
}
