/* Copyright (C) 2008-2010 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __V4L2_UTILS__
#define __V4L2_UTILS__

#include <sys/ioctl.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <strings.h>
#include <linux/videodev2.h>
#include <fcntl.h>

#define NOCOL      "\033[0m"

#define BLACKCOL   "\033[0;30m"
#define LGRAYCOL   "\033[0;37m"

#define REDCOL     "\033[0;31m" // dvb warning
#define GREENCOL   "\033[0;32m" // ui message
#define BROWNCOL   "\033[0;33m"
#define BLUECOL    "\033[0;34m" // dvb message
#define PURPLECOL  "\033[0;35m" // ui warning
#define CYANCOL    "\033[0;36m"

#define DGRAYCOL   "\033[1;30m" // ca message
#define WHITECOL   "\033[1;37m" // ca warning

#define LREDCOL    "\033[1;31m"
#define LGREENCOL  "\033[1;32m" // dfb message
#define YELLOWCOL  "\033[1;33m" // gfx print
#define LBLUECOL   "\033[1;34m" // osd layer print
#define LPURPLECOL "\033[1;35m" // dfb warning
#define LCYANCOL   "\033[1;36m" // scl layer print


#ifndef ltv_print
#define ltv_print(format, args...) \
  ( { GST_DEBUG ("(%.5d): " format, getpid (), ## args); } )
#endif
#ifndef ltv_message
#define ltv_message(format, args...) \
  ( { GST_INFO (BLUECOL "(%.5d) message: " format NOCOL , getpid (), ## args); } )
#endif
#ifndef ltv_warning
#define ltv_warning(format, args...) \
  ( { GST_WARNING (REDCOL "(%.5d) warning: " __FILE__ "(%d): " format NOCOL , getpid (), __LINE__, ## args); } )
#endif


#define LTV_CHECK(x...)                                               \
  ( {                                                                 \
    int ret = x;                                                      \
    if (ret < 0)                                                      \
      {                                                               \
        typeof(errno) errno_backup = errno;                           \
        ltv_warning ("%s: %s: %d (%m)", __FUNCTION__, #x, errno);     \
        errno = errno_backup;                                         \
      }                                                               \
    ret;                                                              \
  } )

#define V4L2_OVERLAY_DRIVER_NAME    "AV Decoder"
#define V4L2_OVERLAY_CARD_NAME      "STMicroelectronics"

#define V4L2_CAPTURE_DRIVER_NAME    "AV Decoder"
#define V4L2_CAPTURE_CARD_NAME      "STMicroelectronics"

#define V4L2_OUTPUT_DRIVER_NAME     "Planes"
#define V4L2_OUTPUT_CARD_NAME       "STMicroelectronics"

#define V4L2_ENCODE_DRIVER_NAME     "Encoder"
#define V4L2_ENCODE_CARD_NAME       "STMicroelectronics"

#define V4L2_HWJPEG_DRIVER_NAME     "JPEG Decoder"
#define V4L2_HWJPEG_CARD_NAME       "STMicroelectronics"
static int __attribute__ ((unused))
    v4l2_open_by_name (const char *const driver, const char *const card, int flags)
{
  int videofd;
  struct v4l2_capability cap;
  char name[30];
  int ret = -1;
  unsigned int i = 0;

  do {
    snprintf (name, sizeof (name), "/dev/video%d", i);
    videofd = open (name, flags);
    if (videofd < 0)
      continue;

    ret = ioctl (videofd, VIDIOC_QUERYCAP, &cap);
    if (ret < 0) {
      ltv_warning
          ("Driver BUG: device %s doesn't answer to VIDIOC_QUERYCAP - ignoring\n",
          name);
      close (videofd);
      continue;
    }

    if ((strcasecmp ((char *) cap.driver, driver) == 0) &&
        (strcasecmp ((char *) cap.card, card) == 0)) {
      ltv_print ("Found proper device file: %s\n", name);
      break;
    }

    close (videofd);
  } while (++i < 255);

  return videofd;
}

static void __attribute__ ((unused))
    v4l2_list_outputs (int videofd)
{
  struct v4l2_output output;

  output.index = 0;
  while (ioctl (videofd, VIDIOC_ENUMOUTPUT, &output) == 0) {
    ltv_message
        ("videofd %d: Found output '%s' w/ idx/std/type/audioset/modulator: %d/%.16llx/%d/%d/%d",
        videofd, output.name, output.index, output.std, output.type,
        output.audioset, output.modulator);
    ++output.index;
  }
}

static int __attribute__ ((unused))
    v4l2_set_output_by_index (int videofd, int output_idx)
{
  int output_idx_read;
  int ret = -1;
  typeof (errno) errno_backup;

  if (output_idx < 0)
    /* invalid output */
    return ret;

  ltv_print ("Setting output to %u\n", output_idx);
  ret = LTV_CHECK (ioctl (videofd, VIDIOC_S_OUTPUT, &output_idx));
  errno_backup = errno;
  LTV_CHECK (ioctl (videofd, VIDIOC_G_OUTPUT, &output_idx_read));
  if (output_idx != output_idx_read)
    ltv_warning ("Driver BUG: output index is %d (should be: %d) - ignoring\n",
        output_idx_read, output_idx);

  errno = errno_backup;
  return ret;
}

static int __attribute__ ((unused))
    v4l2_set_output_by_name (int videofd, const char *const name)
{
  struct v4l2_output output;
  int output_idx;

  output.index = 0;
  output_idx = -1;
  while (ioctl (videofd, VIDIOC_ENUMOUTPUT, &output) == 0) {
    if (strcasecmp ((char *) output.name, name) == 0) {
      output_idx = output.index;
      break;
    }

    ++output.index;
  }

  return v4l2_set_output_by_index (videofd, output_idx);
}


static void __attribute__ ((unused))
    v4l2_list_inputs (int videofd)
{
  struct v4l2_input input;

  input.index = 0;
  while (ioctl (videofd, VIDIOC_ENUMINPUT, &input) == 0) {
    ltv_message
        ("videofd %d: Found input '%s' w/ idx/std/type/audioset/tuner/status: %d/%.16llx/%d/%d/%d/%d",
        videofd, input.name, input.index, input.std, input.type, input.audioset,
        input.tuner, input.status);
    ++input.index;
  }
}

static int __attribute__ ((unused))
    v4l2_set_input_by_index (int videofd, int input_idx)
{
  int input_idx_read;
  int ret = -1;
  typeof (errno) errno_backup;

  if (input_idx < 0)
    /* invalid output */
    return ret;

  ltv_print ("Setting input to %u\n", input_idx);
  ret = LTV_CHECK (ioctl (videofd, VIDIOC_S_INPUT, &input_idx));
  errno_backup = errno;
  LTV_CHECK (ioctl (videofd, VIDIOC_G_INPUT, &input_idx_read));
  if (input_idx != input_idx_read)
    ltv_warning ("Driver BUG: input index is %d (should be: %d) - ignoring\n",
        input_idx_read, input_idx);

  errno = errno_backup;
  return ret;
}

static int __attribute__ ((unused))
    v4l2_set_input_by_name (int videofd, const char *const name)
{
  struct v4l2_input input;
  int input_idx;

  input.index = 0;
  input_idx = -1;
  while (ioctl (videofd, VIDIOC_ENUMINPUT, &input) == 0) {
    if (name && strcasecmp ((char *) input.name, name) == 0) {
      input_idx = input.index;
      break;
    }

    ++input.index;
  }

  return v4l2_set_input_by_index (videofd, input_idx);
}


#endif /* __V4L2_UTILS__ */
