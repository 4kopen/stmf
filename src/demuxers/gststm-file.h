/* Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STM_FILE_H__
#define __GST_STM_FILE_H__

#include <inttypes.h>

#include <gst/gst.h>
#include <gst/base/gstadapter.h>

#define MAX_STFILE_FILENAME_SIZE 512

/* Flags for file system */
/* --------------------- */
typedef enum STSTREAM_FSflags_e
{
  /* Open Flags */
  STSTREAM_FS_RD = 0x01,        /* Open for read  */
  STSTREAM_FS_WR = 0x02,        /* Open for write */
  STSTREAM_FS_CIRCULAR = 0x04,
  /* Seek whence */
  STSTREAM_FS_SEEK_SET = 0x08,
  STSTREAM_FS_SEEK_CUR = 0x10,
  STSTREAM_FS_SEEK_END = 0x20
} STSTREAM_FSflags_t;

/* Pull mode file access */
typedef struct _GstFileContext GstFileContext;
struct _GstFileContext
{
  GstPad *pad;
  gint64 offset;
  gboolean eof;                 /* TRUE when end of file */
};

/* Push mode pipe access */
typedef struct _GstPipeContext GstPipeContext;
struct _GstPipeContext          /* from gstffmpegpipe.h */
{
  GMutex tlock;                 /* lock for syncing                                     */
  GCond cond;                   /* signals counterpart thread to have a look            */
  GstFlowReturn srcresult;      /* flowreturn obtained by src task                      */
  GstAdapter *adapter;          /* adpater collecting data                              */
  GstPad *pad;                  /* send the seek event from the pad to upcoming element */
  guint needed;                 /* amount needed in adapter by src task                 */
  gint64 offset;                /* keep track of file offset                            */
  gboolean eof;                 /* TRUE when end of file                                */
  gboolean closed;              /* TRUE when the pipe is close                          */
  guint rewind_blocksize;       /* size of chunks of data received in Rewind            */
  gboolean src_supports_negative_rate;  /* source supports negative playback rate       */
  gint64 rewind_start_position;
  gboolean is_rev_playback;     /* TRUE when using negative rate at src                 */
  guchar *rewind_buffer;        /* intenal buffer to store rewind data chunks           */
  gint rewind_bufdatasize;      /* data remaining in rewind buffer                      */
  gboolean exit_rewindloop;     /* exit the data loop , return false from pipe_read     */
};

/* File system interface */
/* --------------------- */
typedef struct _GstStmDemuxIOInterface GstStmDemuxIOInterface;
struct _GstStmDemuxIOInterface
{
  gboolean (*CALL_Open) (gchar * FileName, STSTREAM_FSflags_t Flags,
      guint * handle);
  gboolean (*CALL_Close) (guint handle);
  gint (*CALL_Read) (guint handle, guchar * buffer, guint size_in_byte);
  gint (*CALL_Write) (guint handle, guchar * buffer, guint size_in_byte);
  gboolean (*CALL_Seeko) (guint handle, gint64 offset_in_byte,
      STSTREAM_FSflags_t flags);
  gint64 (*CALL_Tello) (guint handle);
  gboolean (*CALL_Eof) (guint handle);
  gboolean (*CALL_Rewind_Seeko) (guint handle, gint64 offset_in_byte,
      STSTREAM_FSflags_t flags, gint64 start_position);
};

/* Pipe protection */
#define GST_STM_PIPE_MUTEX_LOCK(m) G_STMT_START { \
    g_mutex_lock (&m->tlock); \
}G_STMT_END

#define GST_STM_PIPE_MUTEX_UNLOCK(m) G_STMT_START { \
    g_mutex_unlock (&m->tlock); \
}G_STMT_END

#define GST_STM_PIPE_WAIT(m) G_STMT_START { \
    g_cond_wait (&m->cond, &m->tlock); \
}G_STMT_END

#define GST_STM_PIPE_SIGNAL(m) G_STMT_START { \
    g_cond_signal (&m->cond); \
}G_STMT_END

#define GST_STM_PIPE_WAIT_UNTIL(m,t)  g_cond_wait_until (&m->cond, &m->tlock,t)

GST_DEBUG_CATEGORY_EXTERN (gst_stm_file_debug);
extern GstStmDemuxIOInterface gst_stm_fs_interface;
extern GstStmDemuxIOInterface gst_stm_pipe_interface;

#endif
