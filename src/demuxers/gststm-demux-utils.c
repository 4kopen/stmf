/* Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <linux/dvb/stm_ioctls.h>

#include "gststm-demux-utils.h"

GstCaps *
gst_stm_demux_map_caps (guint codec)
{
  GstCaps *caps = NULL;

  switch (codec) {
      /* Known video codec */
    case GST_STM_VIDEO_CODEC_NONE:
      caps = gst_caps_new_empty_simple ("video/none");
      break;

    case GST_STM_VIDEO_MPEG1:
      caps = gst_caps_new_simple ("video/mpeg",
          "mpegversion", G_TYPE_INT, 1,
          "systemstream", G_TYPE_BOOLEAN, FALSE, NULL);
      break;

    case GST_STM_VIDEO_MPEG2:
      caps = gst_caps_new_simple ("video/mpeg",
          "mpegversion", G_TYPE_INT, 2,
          "systemstream", G_TYPE_BOOLEAN, FALSE, NULL);
      break;

    case GST_STM_VIDEO_MSMPEG4V3:
      caps = gst_caps_new_simple ("video/x-msmpeg",
          "msmpegversion", G_TYPE_INT, 43, NULL);
      break;

    case GST_STM_VIDEO_DIVX3:
      caps = gst_caps_new_simple ("video/x-divx",
          "divxversion", G_TYPE_INT, 3, NULL);
      break;

    case GST_STM_VIDEO_DIVX4:
      caps = gst_caps_new_simple ("video/x-divx",
          "divxversion", G_TYPE_INT, 4, NULL);
      break;

    case GST_STM_VIDEO_MPEG4:
      caps = gst_caps_new_simple ("video/x-divx",
          "divxversion", G_TYPE_INT, 5, NULL);
      break;

    case GST_STM_VIDEO_H264:
      caps = gst_caps_new_empty_simple ("video/x-h264");
      break;

    case GST_STM_VIDEO_WMV1:
      caps = gst_caps_new_simple ("video/x-wmv",
          "wmvversion", G_TYPE_INT, 1, NULL);
      break;

    case GST_STM_VIDEO_WMV2:
      caps = gst_caps_new_simple ("video/x-wmv",
          "wmvversion", G_TYPE_INT, 2, NULL);
      break;

    case GST_STM_VIDEO_WMV3:
      caps = gst_caps_new_simple ("video/x-wmv",
          "wmvversion", G_TYPE_INT, 3, NULL);
      break;

    case GST_STM_VIDEO_VC1:
      caps = gst_caps_new_simple ("video/x-wmv",
          "wmvversion", G_TYPE_INT, 3, "format", G_TYPE_STRING, "WVC1", NULL);
      break;

    case GST_STM_VIDEO_FLV1:
    case GST_STM_VIDEO_VP6:
    case GST_STM_VIDEO_VP6F:
      break;

    case GST_STM_VIDEO_MJPEG:
      caps = gst_caps_new_simple ("image/jpeg",
          "format", G_TYPE_STRING, "MJPEG", NULL);
      break;

      /* Known audio codec */
    case GST_STM_AUDIO_CODEC_NONE:
      caps = gst_caps_new_empty_simple ("audio/none");
      break;

    case GST_STM_AUDIO_MPEG1:
      caps = gst_caps_new_simple ("audio/mpeg",
          "mpegversion", G_TYPE_INT, 1, "layer", G_TYPE_INT, 1, NULL);
      break;

    case GST_STM_AUDIO_MPEG2:
      caps = gst_caps_new_simple ("audio/mpeg",
          "mpegversion", G_TYPE_INT, 1, "layer", G_TYPE_INT, 2, NULL);
      break;

    case GST_STM_AUDIO_MP3:
      caps = gst_caps_new_simple ("audio/mpeg",
          "mpegversion", G_TYPE_INT, 1, "layer", G_TYPE_INT, 3, NULL);
      break;

    case GST_STM_AUDIO_AC3:
      caps = gst_caps_new_empty_simple ("audio/x-ac3");
      break;

    case GST_STM_AUDIO_DTS:
      caps = gst_caps_new_empty_simple ("audio/x-dts");
      break;

    case GST_STM_AUDIO_AAC:
      caps = gst_caps_new_simple ("audio/mpeg",
          "mpegversion", G_TYPE_INT, 4, NULL);
      break;

    case GST_STM_AUDIO_PCM_U8:
      caps = gst_caps_new_simple ("audio/x-raw",
          "width", G_TYPE_INT, 8,
          "depth", G_TYPE_INT, 8,
          "endianness", G_TYPE_INT, G_BYTE_ORDER,
          "signed", G_TYPE_BOOLEAN, FALSE, NULL);
      break;

    case GST_STM_AUDIO_PCM_S8:
      caps = gst_caps_new_simple ("audio/x-raw",
          "width", G_TYPE_INT, 8,
          "depth", G_TYPE_INT, 8,
          "endianness", G_TYPE_INT, G_BYTE_ORDER,
          "signed", G_TYPE_BOOLEAN, TRUE, NULL);
      break;

    case GST_STM_AUDIO_PCM_S16LE:
      caps = gst_caps_new_simple ("audio/x-raw",
          "width", G_TYPE_INT, 16,
          "depth", G_TYPE_INT, 16,
          "endianness", G_TYPE_INT, G_LITTLE_ENDIAN,
          "signed", G_TYPE_BOOLEAN, TRUE, NULL);
      break;

    case GST_STM_AUDIO_PCM_S16BE:
      caps = gst_caps_new_simple ("audio/x-raw",
          "width", G_TYPE_INT, 16,
          "depth", G_TYPE_INT, 16,
          "endianness", G_TYPE_INT, G_BIG_ENDIAN,
          "signed", G_TYPE_BOOLEAN, TRUE, NULL);
      break;

    case GST_STM_AUDIO_PCM_U16LE:
      caps = gst_caps_new_simple ("audio/x-raw",
          "width", G_TYPE_INT, 16,
          "depth", G_TYPE_INT, 16,
          "endianness", G_TYPE_INT, G_LITTLE_ENDIAN,
          "signed", G_TYPE_BOOLEAN, FALSE, NULL);
      break;

    case GST_STM_AUDIO_PCM_U16BE:
      caps = gst_caps_new_simple ("audio/x-raw",
          "width", G_TYPE_INT, 16,
          "depth", G_TYPE_INT, 16,
          "endianness", G_TYPE_INT, G_BIG_ENDIAN,
          "signed", G_TYPE_BOOLEAN, FALSE, NULL);
      break;

    case GST_STM_AUDIO_PCM_S24LE:
      caps = gst_caps_new_simple ("audio/x-raw",
          "width", G_TYPE_INT, 24,
          "depth", G_TYPE_INT, 24,
          "endianness", G_TYPE_INT, G_LITTLE_ENDIAN,
          "signed", G_TYPE_BOOLEAN, TRUE, NULL);
      break;

    case GST_STM_AUDIO_PCM_S32LE:
      caps = gst_caps_new_simple ("audio/x-raw",
          "width", G_TYPE_INT, 32,
          "depth", G_TYPE_INT, 32,
          "endianness", G_TYPE_INT, G_LITTLE_ENDIAN,
          "signed", G_TYPE_BOOLEAN, TRUE, NULL);
      break;

    case GST_STM_AUDIO_LPCMB:
      caps = gst_caps_new_empty_simple ("audio/x-private-ts-lpcm");
      break;

    case GST_STM_AUDIO_WMA1:
      caps = gst_caps_new_simple ("audio/x-wma",
          "wmaversion", G_TYPE_INT, 1, NULL);
      break;
    case GST_STM_AUDIO_WMA2:
      caps = gst_caps_new_simple ("audio/x-wma",
          "wmaversion", G_TYPE_INT, 2, NULL);
      break;
    case GST_STM_AUDIO_WMAPRO:
      caps = gst_caps_new_simple ("audio/x-wma",
          "wmaversion", G_TYPE_INT, 3, NULL);
      break;
    case GST_STM_AUDIO_ADPCM_MS:
      caps = gst_caps_new_simple ("audio/x-adpcm",
          "layout", G_TYPE_STRING, "microsoft", NULL);
      break;
    case GST_STM_AUDIO_IMA_WAV:
      caps = gst_caps_new_simple ("audio/x-adpcm",
          "layout", G_TYPE_STRING, "dvi", NULL);
      break;
    case GST_STM_AUDIO_MLP:
      caps = gst_caps_new_empty_simple ("audio/x-mlp");
      break;
    case GST_STM_AUDIO_DRA:
      caps = gst_caps_new_empty_simple ("audio/x-dra");
      break;
      /* For Subtitle */
    case GST_STM_ANCILLARY_CODEC_NONE:
      caps = gst_caps_new_empty_simple ("ancillary/none");
      break;
    case GST_STM_DVB_SUBTITLE:
      caps = gst_caps_new_empty_simple ("subpicture/x-dvb");
      break;
    case GST_STM_DVB_TELETEXT:
      caps = gst_caps_new_empty_simple ("private/teletext");
      break;
    case GST_STM_CLOSECAPTION:
      caps = gst_caps_new_empty_simple ("private/closecaption");
      break;

    default:
      break;
  }

  if (caps == NULL) {
    GST_LOG ("No caps found for codec_id=%d", codec);
  }

  return caps;
}

gst_stm_av_codec_t
gst_stm_demux_get_gststm_codec_type (stream_content_t content, guint codec)
{
  if (content == STREAM_CONTENT_VIDEO) {
    switch (codec) {
      case VIDEO_ENCODING_MPEG1:
        return GST_STM_VIDEO_MPEG1;
      case VIDEO_ENCODING_MPEG2:
        return GST_STM_VIDEO_MPEG2;
      case VIDEO_ENCODING_H264:
        return GST_STM_VIDEO_H264;
      case VIDEO_ENCODING_WMV:
        return GST_STM_VIDEO_WMV3;
      case VIDEO_ENCODING_VC1:
        return GST_STM_VIDEO_VC1;
      case VIDEO_ENCODING_HEVC:
        return GST_STM_VIDEO_HEVC;
      case VIDEO_ENCODING_AVS:
        return GST_STM_VIDEO_AVS;
      case VIDEO_ENCODING_AVSP:
        return GST_STM_VIDEO_AVSP;
      case VIDEO_ENCODING_NONE:
        return GST_STM_VIDEO_CODEC_NONE;
      default:
        return GST_STM_CODEC_UNKNOWN;
    }
  } else if (content == STREAM_CONTENT_AUDIO) {
    switch (codec) {
      case AUDIO_ENCODING_MPEG1:
        return GST_STM_AUDIO_MPEG1;
      case AUDIO_ENCODING_MPEG2:
        return GST_STM_AUDIO_MPEG2;
      case AUDIO_ENCODING_MP3:
        return GST_STM_AUDIO_MP3;
      case AUDIO_ENCODING_AC3:
        return GST_STM_AUDIO_AC3;
      case AUDIO_ENCODING_AAC:
        return GST_STM_AUDIO_AAC;
      case AUDIO_ENCODING_DTS:
        return GST_STM_AUDIO_DTS;
      case AUDIO_ENCODING_PCM:
      case AUDIO_ENCODING_LPCMB:
        return GST_STM_AUDIO_LPCMB;
      case AUDIO_ENCODING_MLP:
        return GST_STM_AUDIO_MLP;
      case AUDIO_ENCODING_DRA:
        return GST_STM_AUDIO_DRA;
      case AUDIO_ENCODING_LPCM:
        return GST_STM_AUDIO_LPCM;
      case AUDIO_ENCODING_NONE:
        return GST_STM_AUDIO_CODEC_NONE;
      default:
        return GST_STM_CODEC_UNKNOWN;
    }
  } else if (content == STREAM_CONTENT_SUBTITLE) {
    switch (codec) {
      case META_ENCODING_SUBTITLE:
        return GST_STM_DVB_SUBTITLE;
      case META_ENCODING_TELETEXT:
        return GST_STM_DVB_TELETEXT;
      case META_ENCODING_CLOSECAPTION:
        return GST_STM_CLOSECAPTION;
      case META_ENCODING_NONE:
        return GST_STM_ANCILLARY_CODEC_NONE;
      default:
        return GST_STM_CODEC_UNKNOWN;
    }
  }

  return GST_STM_CODEC_UNKNOWN;
}
