/* Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <string.h>
#include <errno.h>

#include <gst/gst.h>
#include "gststm-file.h"
#include <stdio.h>

GST_DEBUG_CATEGORY (gst_stm_file_debug);
#define GST_CAT_DEFAULT gst_stm_file_debug

static gboolean
gst_stm_fs_open (char *filename, STSTREAM_FSflags_t flag, guint * handle)
{
  GstFileContext *cxt;
  GstPad *pad;

  GST_LOG ("opening file");

  cxt = g_new0 (GstFileContext, 1);

  /* Read the pad pointer from the filename string */
  if (sscanf (filename, "%p", &pad) != 1) {
    GST_WARNING ("no pad");
    g_free (cxt);
    return FALSE;
  }

  /* Check for correct pad type */
  g_return_val_if_fail (GST_IS_PAD (pad), FALSE);

  switch (flag) {
    case STSTREAM_FS_RD:
      g_return_val_if_fail (GST_PAD_IS_SINK (pad), FALSE);
      break;
    case STSTREAM_FS_WR:
      g_return_val_if_fail (GST_PAD_IS_SRC (pad), FALSE);
      break;
    default:
      GST_WARNING ("invalid flag");
      break;
  }

  cxt->pad = pad;
  cxt->offset = 0;
  cxt->eof = FALSE;
  *handle = (guint) cxt;
  GST_DEBUG ("created file context 0x%08x", (guint) cxt);

  return TRUE;
}

static gboolean
gst_stm_fs_close (guint handle)
{
  GstFileContext *cxt;

  GST_LOG ("gst_stm_fs_close");

  cxt = (GstFileContext *) handle;
  if (cxt == NULL) {
    GST_WARNING ("file context not created");
    return FALSE;
  }
  GST_DEBUG ("closed file context 0x%08x", (guint) cxt);
  g_free (cxt);

  return TRUE;
}

static gint
gst_stm_fs_read (guint handle, guchar * buffer, guint size_in_byte)
{
  GstFileContext *cxt;
  GstBuffer *inbuf = NULL;
  GstFlowReturn ret;
  gint total = 0;
  GstMapInfo info_read;

  cxt = (GstFileContext *) handle;
  if (cxt == NULL) {
    GST_WARNING ("file context not created");
    return 0;
  }

  /* Read from source with offset, do we need to check the offset?? */
  ret =
      gst_pad_pull_range (cxt->pad, cxt->offset, (guint) size_in_byte, &inbuf);

  switch (ret) {
    case GST_FLOW_OK:
    {
      if (inbuf) {
        gst_buffer_map (inbuf, &info_read, GST_MAP_READ);
        total = (gint) info_read.size;
        /* When buffer == NULL, assume it is a fake read */
        if ((buffer != NULL) && (info_read.data)) {
          memcpy (buffer, info_read.data, total);
        }
        gst_buffer_unmap (inbuf, &info_read);
        gst_buffer_unref (inbuf);
      }
      break;
    }
    case GST_FLOW_FLUSHING:
    {
      cxt->eof = TRUE;
      total = -1;
      GST_WARNING ("GST_FLOW_WRONG_STATE");
      break;
    }
    case GST_FLOW_ERROR:
    {
      GST_WARNING ("GST_FLOW_ERROR");
    }
      break;
    default:
    {
      GST_LOG ("end of file reached");
      cxt->eof = TRUE;
      total = -2;
      break;
    }
  }
  if (total > 0) {
    cxt->offset += total;
    GST_DEBUG ("read %d bytes", total);
  }

  return total;
}

static gint
gst_stm_fs_write (guint handle, guchar * buffer, guint size_in_byte)
{
  GST_ERROR ("not implemented");

  return 0;
}

static gboolean
gst_stm_fs_seeko (guint handle, gint64 offset_in_byte, STSTREAM_FSflags_t flag)
{
  GstFileContext *cxt;
  gint64 newpos = 0;
  gboolean res = TRUE;

  cxt = (GstFileContext *) handle;
  if (cxt == NULL) {
    GST_WARNING ("file context not created");
    return FALSE;
  }

  switch (flag) {
    case STSTREAM_FS_SEEK_SET:
    {
      newpos = offset_in_byte;
      break;
    }
    case STSTREAM_FS_SEEK_CUR:
    {
      newpos = cxt->offset + offset_in_byte;
      break;
    }
    case STSTREAM_FS_SEEK_END:
    {
      GstFormat format = GST_FORMAT_BYTES;
      gint64 duration;

      if (gst_pad_is_linked (cxt->pad)) {
        if (gst_pad_query_duration (GST_PAD_PEER (cxt->pad), format, &duration)) {
          newpos = duration + offset_in_byte;
        } else {
          GST_WARNING ("failed to get file size");
          res = FALSE;
        }
      } else {
        GST_WARNING ("no source file");
        res = FALSE;
      }
      break;
    }
    default:
      GST_WARNING ("invalid seek flag");
      break;
  }

  if (res == TRUE) {
    cxt->offset = newpos;
    cxt->eof = FALSE;
    GST_DEBUG ("new offset %" G_GINT64_FORMAT, cxt->offset);
  }

  return res;
}

static gint64
gst_stm_fs_tello (guint handle)
{
  GstFileContext *cxt;

  cxt = (GstFileContext *) handle;
  if (cxt == NULL) {
    GST_WARNING ("file context not created");
    return 0;
  }

  GST_DEBUG ("current offset %" G_GINT64_FORMAT, cxt->offset);

  return (gint64) cxt->offset;
}

static gboolean
gst_stm_fs_eof (guint handle)
{
  GstFileContext *cxt;

  cxt = (GstFileContext *) handle;
  if (cxt == NULL) {
    GST_WARNING ("file context not created");
    return FALSE;
  }

  if (cxt->eof) {
    return TRUE;
  }

  return FALSE;
}

static gboolean
gst_stm_pipe_open (gchar * filename, STSTREAM_FSflags_t flag, guint * handle)
{
  GstPipeContext *pipe;

  GST_LOG ("opening pipe");

  /* Read the pipe context pointer from the filename string */
  if (sscanf (filename, "%p", &pipe) != 1) {
    GST_WARNING ("no pipe context");
    return FALSE;
  }

  /* Sanity check */
  g_return_val_if_fail (GST_IS_ADAPTER (pipe->adapter), FALSE);

  pipe->offset = 0;
  pipe->is_rev_playback = FALSE;
  pipe->rewind_blocksize = 0;
  pipe->src_supports_negative_rate = FALSE;
  pipe->rewind_buffer = NULL;
  pipe->rewind_start_position = 0;
  pipe->exit_rewindloop = FALSE;
  *handle = (guint) pipe;

  return TRUE;
}

static gboolean
gst_stm_pipe_close (guint handle)
{
  GstPipeContext *pipe = (GstPipeContext *) handle;
  if (pipe->rewind_buffer != NULL) {
    g_free (pipe->rewind_buffer);
    pipe->rewind_buffer = NULL;
    pipe->rewind_start_position = 0;
    pipe->rewind_bufdatasize = 0;
  }

  return TRUE;
}

static gint
gst_stm_pipe_read (guint handle, guchar * buffer, guint size_in_byte)
{
  GstPipeContext *pipe;
  const guint8 *data;
  guint available;
  guint size;

  GST_LOG ("requested size %d", size_in_byte);
  pipe = (GstPipeContext *) handle;
  if (pipe == NULL) {
    GST_WARNING ("pipe context not created");
    return 0;
  }

  /* Lock the pipe to update some fields */
  if (pipe->is_rev_playback != TRUE) {
    GST_STM_PIPE_MUTEX_LOCK (pipe);

    /* Update the required size */
    pipe->needed = size_in_byte;

    available = gst_adapter_available (pipe->adapter);
    /* Data in the pipe not enough and file not end yet */
    while (available < size_in_byte && (pipe->eof == FALSE)) {
      GST_DEBUG ("available:%d, requested:%d", available, size_in_byte);
      /* Signal the chain task to read data into the pipe */
      GST_STM_PIPE_SIGNAL (pipe);
      /* Wait for the chain task to signal */
      GST_STM_PIPE_WAIT (pipe);
      /* check data from the pipe again */
      available = gst_adapter_available (pipe->adapter);
    }

    /* Data in the buffer >= requested size or end of file reached */
    size = MIN (available, size_in_byte);
    if (size > 0) {
      GST_LOG ("getting %d bytes", size);
      /* Read data from the adapter to dest buffer */
      data = gst_adapter_map (pipe->adapter, size);
      if (buffer != NULL) {
        memcpy (buffer, data, size);
      }
      gst_adapter_unmap (pipe->adapter);
      /* Flush the pipe to receive new data */
      gst_adapter_flush (pipe->adapter, size);
      GST_LOG ("%d bytes left in adapter",
          gst_adapter_available (pipe->adapter));
      pipe->needed = 0;
      pipe->offset += size;
    }

    /* Unlock the pipe now */
    GST_STM_PIPE_MUTEX_UNLOCK (pipe);

    return size;
  } else {
    GST_STM_PIPE_MUTEX_LOCK (pipe);
    if (pipe->exit_rewindloop == TRUE) {
      pipe->exit_rewindloop = FALSE;
      GST_STM_PIPE_MUTEX_UNLOCK (pipe);
      return 0;
    }

    if (pipe->offset < pipe->rewind_start_position) {
      GST_STM_PIPE_MUTEX_UNLOCK (pipe);
      return 0;
    }

    pipe->needed = size_in_byte;
    if (pipe->rewind_bufdatasize > size_in_byte) {
      memcpy (buffer,
          pipe->rewind_buffer + pipe->rewind_bufdatasize - size_in_byte,
          size_in_byte);
      pipe->rewind_bufdatasize -= size_in_byte;
      pipe->offset -= size_in_byte;
      pipe->needed = 0;
      GST_STM_PIPE_MUTEX_UNLOCK (pipe);
      return size_in_byte;
    }

    if (pipe->rewind_bufdatasize != 0) {
      memcpy (buffer + pipe->needed - pipe->rewind_bufdatasize,
          pipe->rewind_buffer, pipe->rewind_bufdatasize);
      pipe->needed -= pipe->rewind_bufdatasize;
      pipe->offset -= pipe->rewind_bufdatasize;
      pipe->rewind_bufdatasize = 0;
    }

    while (pipe->needed != 0) {
      available = gst_adapter_available (pipe->adapter);
      /* Data in the pipe not enough and file not end yet */
      while (available == 0 && (pipe->eof == FALSE)) {
        GST_DEBUG ("available:%d, requested:%d", available, size_in_byte);
        /* Signal the chain task to read data into the pipe */
        GST_STM_PIPE_SIGNAL (pipe);
        /* Wait for the chain task to signal */
        GST_STM_PIPE_WAIT (pipe);
        if (pipe->exit_rewindloop == TRUE) {
          GST_STM_PIPE_MUTEX_UNLOCK (pipe);
          pipe->exit_rewindloop = FALSE;
          return 0;
        }

        /* check data from the pipe again */
        available = gst_adapter_available (pipe->adapter);
      }

      if (pipe->rewind_buffer == NULL) {
        pipe->rewind_buffer = (guchar *) g_malloc (pipe->rewind_blocksize);
        pipe->rewind_bufdatasize = 0;
      }

      /* Data in the buffer >= requested size or end of file reached */
      if (available > 0) {
        GST_LOG ("getting %d bytes", available);
        /* Read data from the adapter to dest buffer */
        data = gst_adapter_map (pipe->adapter, available);
        if (pipe->rewind_buffer != NULL) {
          memcpy (pipe->rewind_buffer, data, available);
          pipe->rewind_bufdatasize = available;
        }
        gst_adapter_unmap (pipe->adapter);
        /* Flush the pipe to receive new data */
        gst_adapter_flush (pipe->adapter, available);
        GST_LOG ("%d bytes left in adapter",
            gst_adapter_available (pipe->adapter));

      }

      if (pipe->needed > 0) {
        if (pipe->needed <= pipe->rewind_bufdatasize) {
          memcpy (buffer,
              pipe->rewind_buffer + pipe->rewind_bufdatasize - pipe->needed,
              pipe->needed);
          pipe->rewind_bufdatasize = pipe->rewind_bufdatasize - pipe->needed;
          pipe->offset -= pipe->needed;
          pipe->needed = 0;
          GST_STM_PIPE_MUTEX_UNLOCK (pipe);
          return size_in_byte;
        } else {
          memcpy (buffer + pipe->needed - pipe->rewind_bufdatasize,
              pipe->rewind_buffer, pipe->rewind_bufdatasize);
          pipe->needed -= pipe->rewind_bufdatasize;
          pipe->offset -= pipe->rewind_bufdatasize;
          pipe->rewind_bufdatasize = 0;
        }
      }
    }
  }

  GST_STM_PIPE_MUTEX_UNLOCK (pipe);
  GST_WARNING ("pipe_read returning zero bytes");
  return 0;
}

static gint
gst_stm_pipe_write (guint Handle, unsigned char *Buffer,
    unsigned int size_in_byte)
{
  GST_ERROR ("not implemented");

  return 0;
}

static gboolean
gst_stm_pipe_seeko (guint handle, gint64 offset_in_byte,
    STSTREAM_FSflags_t flag)
{
  GstPipeContext *pipe;
  GstEvent *event;
  gint64 newpos = 0;
  gboolean res = TRUE;

  pipe = (GstPipeContext *) handle;
  if (pipe == NULL) {
    GST_WARNING ("pipe context not created");
    return FALSE;
  }

  switch (flag) {
    case STSTREAM_FS_SEEK_SET:
    {
      GST_STM_PIPE_MUTEX_LOCK (pipe);
      pipe->eof = FALSE;
      GST_STM_PIPE_MUTEX_UNLOCK (pipe);

      newpos = offset_in_byte;
      event = gst_event_new_seek (1.0, GST_FORMAT_BYTES,
          GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE,
          GST_SEEK_TYPE_SET, newpos, GST_SEEK_TYPE_SET, -1);
      if (gst_pad_push_event (pipe->pad, event) == TRUE) {
        pipe->offset = newpos;
        pipe->is_rev_playback = FALSE;
        pipe->rewind_start_position = 0;
      } else {
        GST_WARNING ("seek set failed");
        res = FALSE;
      }
      break;
    }
    case STSTREAM_FS_SEEK_CUR:
    {
      GST_STM_PIPE_MUTEX_LOCK (pipe);
      pipe->eof = FALSE;
      GST_STM_PIPE_MUTEX_UNLOCK (pipe);

      newpos = pipe->offset + offset_in_byte;
      event = gst_event_new_seek (1.0, GST_FORMAT_BYTES,
          GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE,
          GST_SEEK_TYPE_SET, newpos, GST_SEEK_TYPE_SET, -1);
      if (gst_pad_push_event (pipe->pad, event) == TRUE) {
        pipe->offset = newpos;
        pipe->is_rev_playback = FALSE;
        pipe->rewind_start_position = 0;
      } else {
        GST_WARNING ("seek cur failed");
        res = FALSE;
      }
      break;
    }
    case STSTREAM_FS_SEEK_END:
    {
      GstFormat format = GST_FORMAT_BYTES;
      gint64 duration;

      if (gst_pad_is_linked (pipe->pad)) {
        if (gst_pad_query_duration (GST_PAD_PEER (pipe->pad), format,
                &duration)) {
          GST_STM_PIPE_MUTEX_LOCK (pipe);
          pipe->eof = FALSE;
          GST_STM_PIPE_MUTEX_UNLOCK (pipe);

          newpos = duration + offset_in_byte;
          event = gst_event_new_seek (1.0, GST_FORMAT_BYTES,
              GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE,
              GST_SEEK_TYPE_SET, newpos, GST_SEEK_TYPE_SET, -1);
          if (gst_pad_push_event (pipe->pad, event) == TRUE) {
            pipe->offset = newpos;
            pipe->is_rev_playback = FALSE;
            pipe->rewind_start_position = 0;
          } else {
            GST_WARNING ("seek end failed");
            res = FALSE;
          }
        } else {
          GST_WARNING ("failed to get file size");
          res = FALSE;
        }
      } else {
        GST_WARNING ("no source file");
        res = FALSE;
      }
      break;
    }
    default:
      GST_WARNING ("invalid seek flag");
      res = FALSE;
      break;
  }

  if (res == TRUE) {
    GST_DEBUG ("new offset %" G_GINT64_FORMAT, pipe->offset);
  }

  return res;
}

static gint64
gst_stm_pipe_tello (guint handle)
{
  GstPipeContext *pipe;

  pipe = (GstPipeContext *) handle;
  if (pipe == NULL) {
    GST_WARNING ("pipe context not created");
    return 0;
  }

  GST_DEBUG ("current offset %" G_GINT64_FORMAT, pipe->offset);

  return (gint64) pipe->offset;
}

static gboolean
gst_stm_pipe_eof (guint handle)
{
  GstPipeContext *pipe;

  pipe = (GstPipeContext *) handle;
  if (pipe == NULL) {
    GST_WARNING ("pipe context not created");
    return FALSE;
  }

  if (pipe->is_rev_playback != TRUE) {
    if (pipe->eof) {
      return TRUE;
    }
  } else {
    if (pipe->offset < pipe->rewind_start_position && pipe->eof)
      return TRUE;
    else
      return FALSE;
  }

  return FALSE;
}

static gboolean
gst_stm_pipe_rewind_seeko (guint handle, gint64 offset_in_byte,
    STSTREAM_FSflags_t flag, gint64 start_position)
{
  GstPipeContext *pipe;
  GstEvent *event;
  gint64 newpos = 0;
  gboolean res = TRUE;

  pipe = (GstPipeContext *) handle;
  if (pipe == NULL) {
    GST_WARNING ("pipe context not created");
    return FALSE;
  }

  if (pipe->rewind_buffer != NULL) {
    g_free (pipe->rewind_buffer);
    pipe->rewind_buffer = NULL;
    pipe->rewind_bufdatasize = 0;

  }

  switch (flag) {
    case STSTREAM_FS_SEEK_SET:
    {
      GST_STM_PIPE_MUTEX_LOCK (pipe);
      pipe->eof = FALSE;
      pipe->exit_rewindloop = FALSE;
      GST_STM_PIPE_MUTEX_UNLOCK (pipe);

      newpos = offset_in_byte;
      event = gst_event_new_seek (-1.0, GST_FORMAT_BYTES,
          GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE,
          GST_SEEK_TYPE_SET, 0, GST_SEEK_TYPE_SET, newpos);
      if (gst_pad_push_event (pipe->pad, event) == TRUE) {
        pipe->offset = newpos;
        pipe->is_rev_playback = TRUE;
        pipe->rewind_start_position = start_position;
        GST_DEBUG ("new offset %" G_GINT64_FORMAT, pipe->offset);
      } else {
        GST_WARNING ("seek set failed");
        res = FALSE;
      }
      break;
    }
    default:
      GST_WARNING ("invalid seek flag");
      res = FALSE;
      break;
  }

  return res;
}

GstStmDemuxIOInterface gst_stm_fs_interface = {
  gst_stm_fs_open,
  gst_stm_fs_close,
  gst_stm_fs_read,
  gst_stm_fs_write,
  gst_stm_fs_seeko,
  gst_stm_fs_tello,
  gst_stm_fs_eof,
  NULL,
};

GstStmDemuxIOInterface gst_stm_pipe_interface = {
  gst_stm_pipe_open,
  gst_stm_pipe_close,
  gst_stm_pipe_read,
  gst_stm_pipe_write,
  gst_stm_pipe_seeko,
  gst_stm_pipe_tello,
  gst_stm_pipe_eof,
  gst_stm_pipe_rewind_seeko,
};
