/* Copyright (C) 2011 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * @file    ststream_common_utils.c
 * @brief   This is the STSTREAM utilities file
 *          Ported from ststream_common_utils.c
 * @author  STMicroelectronics
 * @date    2011
*/

/* Include */
/* ------- */

#include "ststream_common_utils.h"


/* ========================================================================
Name:        STSTREAM_AudioCodecIdToTag / STSTREAM_AudioTagToCodecId
Description: Returns the codec Id according to one codec tag and vice versa
======================================================================== */
typedef struct STSTREAM_AudioCodecIdToTag_s
{
  STSTREAM_CodecId_t id;
  unsigned int tag;
} STSTREAM_AudioCodecIdToTag_t;

STSTREAM_AudioCodecIdToTag_t audioCodecIdToTag[] = {
  {STSTREAM_CODEC_AAC, 0x706d},
  {STSTREAM_CODEC_AC3, 0x2000},
  {STSTREAM_CODEC_MP2, 0x50},
  {STSTREAM_CODEC_MP3, 0x55},
  {STSTREAM_CODEC_WMAV1, 0x160},
  {STSTREAM_CODEC_WMAV2, 0x161},
  {STSTREAM_CODEC_WMA_PRO, 0x162},
  {STSTREAM_CODEC_DTS, 0x2001},
  {STSTREAM_CODEC_PCM_U8, 0x01},
  {STSTREAM_CODEC_PCM_S16LE, 0x01},
  {STSTREAM_CODEC_PCM_S16BE, 0x01},
  {STSTREAM_CODEC_LPCM, 0x01},
  {STSTREAM_CODEC_ADPCM_MS, 0x02},

  /* Etc Etc : only the above types are used so far in our appli */

  {STSTREAM_CODEC_NONE, 0x0}    /* Pls keep This as last item */
};

/* ========================================================================
* STSTREAM_AudioTagToCodecId
* ========================================================================= */
/**
* @brief    Convert one tag (0x160) to CodecID .(STSTREAM_CODEC_WMAV1)
* @param    tag    [in]
* @return   codecID
**/
STSTREAM_CodecId_t
STSTREAM_AudioTagToCodecId (unsigned int tag)
{
  int i;

  for (i = 0;
      audioCodecIdToTag[i].tag != tag
      && audioCodecIdToTag[i].id != STSTREAM_CODEC_NONE; i++) {
  }

  return audioCodecIdToTag[i].id;
}

/* ========================================================================
* STSTREAM_VideoTagToCodecId
* ========================================================================= */
/**
* @brief    Convert one tag ('WMV3') to CodecID .(STSTREAM_CODEC_WMV3)
* @param    tag    [in]
* @return   codecID
**/
STSTREAM_CodecId_t
STSTREAM_VideoTagToCodecId (unsigned int tag)
{
#define CodeToInteger(a,b,c,d) ((a << 0) | (b << 8) | (c << 16) | (d <<24))

  switch (tag) {
    case CodeToInteger ('w', 'm', 'v', '3'):
    case CodeToInteger ('W', 'M', 'V', '3'):
      return STSTREAM_CODEC_WMV3;
      break;

    case CodeToInteger ('w', 'v', 'c', '1'):
    case CodeToInteger ('W', 'V', 'C', '1'):
      return STSTREAM_CODEC_VC1;
      break;

      /* Etc Etc : only the above types are used so far in our appli */

    default:
      return STSTREAM_CODEC_INVALID;
      break;
  }
  return STSTREAM_CODEC_INVALID;
}


/* ========================================================================
* STSTREAM_StreamType
* ========================================================================= */
/**
* @brief    Returns the stream type according to the codec Id
* @param    codecId    [in]
* @return   stream type
**/
STSTREAM_StreamType_t
STSTREAM_StreamType (STSTREAM_CodecId_t codecId)
{
  switch (codecId) {
    case STSTREAM_CODEC_MPEG1VIDEO:
      return STSTREAM_STREAMTYPE_MP1V;
      break;
    case STSTREAM_CODEC_MPEG2VIDEO:
      return STSTREAM_STREAMTYPE_MP2V;
      break;
    case STSTREAM_CODEC_H264:
      return STSTREAM_STREAMTYPE_H264;
      break;
    case STSTREAM_CODEC_MSMPEG4V3:
      return STSTREAM_STREAMTYPE_MSMPEG4V3;
      break;
    case STSTREAM_CODEC_MPEG4:
      return STSTREAM_STREAMTYPE_MPEG4P2;
      break;
    case STSTREAM_CODEC_XVID:
      return STSTREAM_STREAMTYPE_MPEG4P2;
      break;
    case STSTREAM_CODEC_VC1:
      return STSTREAM_STREAMTYPE_VC1;
      break;
      /*
         case STSTREAM_CODEC_WMV1      :  return STSTREAM_STREAMTYPE_WMV;     break;
         case STSTREAM_CODEC_WMV2      :  return STSTREAM_STREAMTYPE_WMV;     break;
       */
    case STSTREAM_CODEC_WMV3:
      return STSTREAM_STREAMTYPE_VC1;
      break;
    case STSTREAM_CODEC_AAC:
      return STSTREAM_STREAMTYPE_AAC;
      break;
    case STSTREAM_CODEC_AC3:
      return STSTREAM_STREAMTYPE_AC3;
      break;
    case STSTREAM_CODEC_MP2:
      return STSTREAM_STREAMTYPE_MP2A;
      break;
    case STSTREAM_CODEC_MP3:
      return STSTREAM_STREAMTYPE_MP3A;
      break;
      /* case CODEC_ID_MPEG4AAC      :  return STSTREAM_STREAMTYPE_HEAAC;   break; */// needed for last version of STREAM
    case STSTREAM_CODEC_WMAV1:
      return STSTREAM_STREAMTYPE_WMA;
      break;
    case STSTREAM_CODEC_WMAV2:
      return STSTREAM_STREAMTYPE_WMA;
      break;
    case STSTREAM_CODEC_WMA_PRO:
      return STSTREAM_STREAMTYPE_WMA;
      break;
    case STSTREAM_CODEC_DTS:
      return STSTREAM_STREAMTYPE_DTS;
      break;
    case STSTREAM_CODEC_PCM_U8:
      return STSTREAM_STREAMTYPE_PCM;
      break;
    case STSTREAM_CODEC_PCM_S8:
      return STSTREAM_STREAMTYPE_PCM;
      break;
    case STSTREAM_CODEC_PCM_S16LE:
      return STSTREAM_STREAMTYPE_PCM;
      break;
    case STSTREAM_CODEC_PCM_S16BE:
      return STSTREAM_STREAMTYPE_PCM;
      break;
    case STSTREAM_CODEC_PCM_U16LE:
      return STSTREAM_STREAMTYPE_PCM;
      break;
    case STSTREAM_CODEC_PCM_U16BE:
      return STSTREAM_STREAMTYPE_PCM;
      break;
    case STSTREAM_CODEC_LPCM:
      return STSTREAM_STREAMTYPE_LPCM;
      break;
    case STSTREAM_CODEC_ADPCM_IMA_WAV:
      return STSTREAM_STREAMTYPE_ADPCM;
      break;
    case STSTREAM_CODEC_ADPCM_MS:
      return STSTREAM_STREAMTYPE_ADPCM;
      break;
    case STSTREAM_CODEC_FLV1:
      return STSTREAM_STREAMTYPE_FLV1;
      break;
    case STSTREAM_CODEC_VP6:
      return STSTREAM_STREAMTYPE_VP6;
      break;
    case STSTREAM_CODEC_VP6F:
      return STSTREAM_STREAMTYPE_VP6F;
      break;
    case STSTREAM_CODEC_VORBIS:
      return STSTREAM_STREAMTYPE_VORBIS;
      break;
    case STSTREAM_CODEC_MJPEG:
      return STSTREAM_STREAMTYPE_MJPEG;
      break;
    case STSTREAM_CODEC_FLAC:
      return STSTREAM_STREAMTYPE_FLAC;
      break;
    default:
      return STSTREAM_STREAMTYPE_INVALID;
      break;
  }

  return STSTREAM_STREAMTYPE_INVALID;
}

/* ========================================================================
* STSTREAM_CodecIdToStr
* ========================================================================= */
/**
* @brief    Returns the codec Id as a string
* @param    codecId    [in]
* @return   codec Id
**/
const char *
STSTREAM_CodecIdToStr (STSTREAM_CodecId_t codecID)
{
#define E(e) case e: return #e
  switch (codecID) {
      E (STSTREAM_CODEC_INVALID);

      /* Video Codec */
      E (STSTREAM_CODEC_MPEG1VIDEO);
      E (STSTREAM_CODEC_MPEG2VIDEO);
      E (STSTREAM_CODEC_H264);
      E (STSTREAM_CODEC_MSMPEG4V3);
      E (STSTREAM_CODEC_MPEG4);
      E (STSTREAM_CODEC_XVID);
      E (STSTREAM_CODEC_VC1);
      E (STSTREAM_CODEC_WMV1);
      E (STSTREAM_CODEC_WMV2);
      E (STSTREAM_CODEC_WMV3);
      E (STSTREAM_CODEC_FLV1);
      E (STSTREAM_CODEC_VP6);
      E (STSTREAM_CODEC_VP6F);

      /* Audio Codec */
      E (STSTREAM_CODEC_AAC);
      E (STSTREAM_CODEC_AC3);
      E (STSTREAM_CODEC_MP2);
      E (STSTREAM_CODEC_MP3);
      E (STSTREAM_CODEC_MPEG4AAC);
      E (STSTREAM_CODEC_WMAV1);
      E (STSTREAM_CODEC_WMAV2);
      E (STSTREAM_CODEC_WMA_PRO);
      E (STSTREAM_CODEC_DTS);
      E (STSTREAM_CODEC_PCM_U8);
      E (STSTREAM_CODEC_PCM_S8);
      E (STSTREAM_CODEC_PCM_S16LE);
      E (STSTREAM_CODEC_PCM_S16BE);
      E (STSTREAM_CODEC_PCM_U16LE);
      E (STSTREAM_CODEC_PCM_U16BE);
      E (STSTREAM_CODEC_LPCM);
      E (STSTREAM_CODEC_ADPCM_MS);
      E (STSTREAM_CODEC_VORBIS);
      E (STSTREAM_CODEC_MJPEG);
      E (STSTREAM_CODEC_FLAC);
    default:
      return "ILLEGAL CODEC";
  }
#undef E
}

gst_stm_av_codec_t
STSTREAM_CodecIdToGstStmCodecId (STSTREAM_CodecId_t codec)
{
  switch (codec) {
    case STSTREAM_CODEC_MPEG1VIDEO:
      return GST_STM_VIDEO_MPEG1;
    case STSTREAM_CODEC_MPEG2VIDEO:
      return GST_STM_VIDEO_MPEG2;
    case STSTREAM_CODEC_H264:
      return GST_STM_VIDEO_H264;
    case STSTREAM_CODEC_MSMPEG4V3:
      return GST_STM_VIDEO_MSMPEG4V3;
    case STSTREAM_CODEC_DIVX3:
      return GST_STM_VIDEO_DIVX3;
    case STSTREAM_CODEC_DIVX4:
      return GST_STM_VIDEO_DIVX4;
    case STSTREAM_CODEC_MPEG4:
      return GST_STM_VIDEO_MPEG4;
    case STSTREAM_CODEC_VC1:
      return GST_STM_VIDEO_VC1;
    case STSTREAM_CODEC_WMV1:
      return GST_STM_VIDEO_WMV1;
    case STSTREAM_CODEC_WMV2:
      return GST_STM_VIDEO_WMV2;
    case STSTREAM_CODEC_WMV3:
      return GST_STM_VIDEO_WMV3;
    case STSTREAM_CODEC_FLV1:
      return GST_STM_VIDEO_FLV1;
    case STSTREAM_CODEC_VP6:
      return GST_STM_VIDEO_VP6;
    case STSTREAM_CODEC_VP6F:
      return GST_STM_VIDEO_VP6F;
    case STSTREAM_CODEC_MJPEG:
      return GST_STM_VIDEO_MJPEG;
    case STSTREAM_CODEC_AAC:
      return GST_STM_AUDIO_AAC;
    case STSTREAM_CODEC_AC3:
      return GST_STM_AUDIO_AC3;
    case STSTREAM_CODEC_MP2:
      return GST_STM_AUDIO_MPEG2;
    case STSTREAM_CODEC_MP3:
      return GST_STM_AUDIO_MP3;
    case STSTREAM_CODEC_MPEG4AAC:
      return GST_STM_AUDIO_MPEG4AAC;
    case STSTREAM_CODEC_WMAV1:
      return GST_STM_AUDIO_WMA1;
    case STSTREAM_CODEC_WMAV2:
      return GST_STM_AUDIO_WMA2;
    case STSTREAM_CODEC_WMA_PRO:
      return GST_STM_AUDIO_WMAPRO;
    case STSTREAM_CODEC_DTS:
      return GST_STM_AUDIO_DTS;
    case STSTREAM_CODEC_PCM_U8:
      return GST_STM_AUDIO_PCM_U8;
    case STSTREAM_CODEC_PCM_S8:
      return GST_STM_AUDIO_PCM_S8;
    case STSTREAM_CODEC_PCM_S16LE:
      return GST_STM_AUDIO_PCM_S16LE;
    case STSTREAM_CODEC_PCM_S16BE:
      return GST_STM_AUDIO_PCM_S16BE;
    case STSTREAM_CODEC_PCM_U16LE:
      return GST_STM_AUDIO_PCM_U16LE;
    case STSTREAM_CODEC_PCM_U16BE:
      return GST_STM_AUDIO_PCM_U16BE;
    case STSTREAM_CODEC_PCM_S24LE:
      return GST_STM_AUDIO_PCM_S24LE;
    case STSTREAM_CODEC_PCM_S32LE:
      return GST_STM_AUDIO_PCM_S32LE;
    case STSTREAM_CODEC_LPCM:
      return GST_STM_AUDIO_LPCM;
    case STSTREAM_CODEC_ADPCM_MS:
      return GST_STM_AUDIO_ADPCM_MS;
    case STSTREAM_CODEC_ADPCM_IMA_WAV:
      return GST_STM_AUDIO_IMA_WAV;
    default:
      return GST_STM_CODEC_UNKNOWN;
  }

  return GST_STM_CODEC_UNKNOWN;
}
