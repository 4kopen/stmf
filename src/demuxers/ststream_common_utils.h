/* Copyright (C) 2011 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * @file    ststream_common_utils.h
 * @brief   This is the STSTREAM utilities header file
 * @author  STMicroelectronics
 * @date    2011
*/


#ifndef _COMMON_UTILS_H_
#define _COMMON_UTILS_H_

#include "ststream.h"
#include "gststm-demux-utils.h"

#define ENABLE_SYNCHRO                  1
#define PCM_PES_ENABLED                 1
#define ES_BIT_BUFFER_OVERFLOW_RATIO    20      /* Must be in sync with LINEAR_BIT_BUFFER_OVERFLOW_RATIO defined in Vid_dec.c
                                                   Used to indicate the upper threshold of the ES Bit Buffer */

/* ========================================================================
* Helper functions
* ======================================================================== */
STSTREAM_StreamType_t STSTREAM_StreamType (STSTREAM_CodecId_t codecId);
const char *STSTREAM_CodecIdToStr (STSTREAM_CodecId_t codecID);
STSTREAM_CodecId_t STSTREAM_AudioTagToCodecId (unsigned int tag);
STSTREAM_CodecId_t STSTREAM_VideoTagToCodecId (unsigned int tag);
gst_stm_av_codec_t STSTREAM_CodecIdToGstStmCodecId (STSTREAM_CodecId_t codec);

#endif //_COMMON_UTILS_H_
