/* Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STM_DEMUX_UTILS_H__
#define __GST_STM_DEMUX_UTILS_H__

#include <gst/gst.h>

#define GST_STM_PID_NONE 0xE000

/*
 * List of possible video/audio encodings - used to select frame parser and codec.
 */

typedef enum
{
  META_ENCODING_NONE,
  META_ENCODING_SUBTITLE,
  META_ENCODING_TELETEXT,
  META_ENCODING_CLOSECAPTION,
  META_ENCODING_AUTO
} meta_encoding_t;

typedef enum
{
  GST_STM_CODEC_UNKNOWN,

  GST_STM_VIDEO_MPEG1,
  GST_STM_VIDEO_MPEG2,
  GST_STM_VIDEO_MSMPEG4V3,
  GST_STM_VIDEO_DIVX3,
  GST_STM_VIDEO_DIVX4,
  GST_STM_VIDEO_MPEG4,
  GST_STM_VIDEO_H264,
  GST_STM_VIDEO_WMV1,
  GST_STM_VIDEO_WMV2,
  GST_STM_VIDEO_WMV3,
  GST_STM_VIDEO_VC1,
  GST_STM_VIDEO_FLV1,
  GST_STM_VIDEO_VP6,
  GST_STM_VIDEO_VP6F,
  GST_STM_VIDEO_MJPEG,
  GST_STM_VIDEO_HEVC,
  GST_STM_VIDEO_AVS,
  GST_STM_VIDEO_AVSP,
  GST_STM_VIDEO_CODEC_NONE,

  GST_STM_AUDIO_MPEG1,
  GST_STM_AUDIO_MPEG2,
  GST_STM_AUDIO_MP3,
  GST_STM_AUDIO_MPEG4AAC,
  GST_STM_AUDIO_AC3,
  GST_STM_AUDIO_DTS,
  GST_STM_AUDIO_AAC,
  GST_STM_AUDIO_PCM_U8,
  GST_STM_AUDIO_PCM_S8,
  GST_STM_AUDIO_PCM_S16LE,
  GST_STM_AUDIO_PCM_S16BE,
  GST_STM_AUDIO_PCM_U16LE,
  GST_STM_AUDIO_PCM_U16BE,
  GST_STM_AUDIO_PCM_S24LE,
  GST_STM_AUDIO_PCM_S32LE,
  GST_STM_AUDIO_LPCM,
  GST_STM_AUDIO_LPCMB,
  GST_STM_AUDIO_ADPCM_MS,
  GST_STM_AUDIO_IMA_WAV,
  GST_STM_AUDIO_WMA1,
  GST_STM_AUDIO_WMA2,
  GST_STM_AUDIO_WMAPRO,
  GST_STM_AUDIO_MLP,
  GST_STM_AUDIO_DRA,
  GST_STM_AUDIO_CODEC_NONE,

  GST_STM_DVB_SUBTITLE,
  GST_STM_DVB_TELETEXT,
  GST_STM_CLOSECAPTION,
  GST_STM_ANCILLARY_CODEC_NONE
} gst_stm_av_codec_t;

typedef enum
{
  STREAM_CONTENT_VIDEO,
  STREAM_CONTENT_AUDIO,
  STREAM_CONTENT_SUBTITLE,
  STREAM_CONTENT_PCR,
  STREAM_CONTENT_ECM,
  STREAM_CONTENT_OTHER
} stream_content_t;

GstCaps *gst_stm_demux_map_caps (guint codec);
gst_stm_av_codec_t gst_stm_demux_get_gststm_codec_type (stream_content_t
    content, guint codec);

#endif
