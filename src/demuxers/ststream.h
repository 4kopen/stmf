/* Copyright (C) 2011 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * @file     sttype.h
 * @brief    This is a ST stream type header file
 * @author   STMicroelectronics 
 * @date     May 2011
 * @see      sttype.h
*/

#ifndef _STSTREAM_H
#define _STSTREAM_H

#include <gst/gst.h>

/* ========================================================================
* STSTREAMER_Frac_t
* ======================================================================== */
/**
* @brief   Rational number
**/
typedef struct STSTREAMER_Frac_s
{
  int numerator;
  int denominator;

} STSTREAMER_Frac_t;

/* ========================================================================
* STSTREAMER_Status_t
* ======================================================================== */
/**
* @brief   Status of the Demux
**/
typedef enum
{
  I_FRAME = 0,
  P_FRAME = 1,
  B_FRAME = 2,
  AUDIO_FRAME = 3,
  UNKNOWN_FRAME = 4
} STSTREAMER_FrameType_t;

/* ========================================================================
* STSTREAMER_TimeUnit_t
* ======================================================================== */
/**
* @brief   Time unit : Milliconds, 90 KHz, others
**/
typedef enum
{
  MILLISECS = 0,                /* Milliseconds                                         */
  PTS90KHZ = 1,                 /* 1/90 milliseconds                                    */
  PRIVATE_UNIT = 2              /* For backward compatibility with Demux various Units  */
      /*       this may be frame mumber                       */
} STSTREAMER_TimeUnit_t;

/* ========================================================================
* STSTREAMER_Time_t
* ======================================================================== */
/**
* @brief   Time with Validity
**/
typedef struct STSTREAMER_Time_s
{
  gint64 value;
  STSTREAMER_TimeUnit_t unit;
  int valid;

} STSTREAMER_Time_t;

/* ========================================================================
* STSTREAMER_FDur_t
* ======================================================================== */
/**
* @brief   Number of frames and total frame duration
**/
typedef struct STSTREAMER_FDur_s
{
  int nbframes;
  int duration;

} STSTREAMER_FDur_t;

/* ========================================================================
*  min, max, ASSERT
* ======================================================================== */
/**
* @brief    min, max, ASSERT
**/
#ifndef min
#define min(a,b) (a < b ? a : b)
#endif

#ifndef max
#define max(a,b) (a > b ? a : b)
#endif

#ifndef abs
#define abs(a) (a >= 0 ? a : -a)
#endif

#ifndef ASSERT
#if defined (DEVELOPMENT)
#define ASSERT(cond) if (!(cond)) {printf("Assert FAILED (program halted) : (%s, %d) : "  #cond "!\n", __FILE__, __LINE__); assert(false);} else while (0)
#else
#define ASSERT(cond) while(0) {}
#endif
#endif

#ifndef RETURN_ON_ERROR
#define RETURN_ON_ERROR( checkValue, retValue, x) {if (x != checkValue) { return retValue;}}
#endif

/* Flags for  seek mode */
/* -------------------- */
typedef enum STSTREAM_SeekFlags_e
{
  STSTREAM_PLAY_SEEK_SET = 0,
  STSTREAM_PLAY_SEEK_CUR,
  STSTREAM_PLAY_SEEK_END,
  STSTREAM_PLAY_TRICK_FWD,      /* required for trick modes. Bring options within index tables */
  STSTREAM_PLAY_TRICK_BWD       /* for accuracy of result */
} STSTREAM_SeekFlags_t;

/* Status of the Demux */
typedef enum
{
  /* STSTREAMER_Whatever = -x,         Status is whatever             */
  STSTREAMER_UNDEFINED_ERROR = -3,      /* For DRM                     */
  STSTREAMER_TRANSFER_ERROR = -2,       /* Transfer is broken          */
  STSTREAMER_TIMEOUT_ERROR = -1,        /* Session Timeout             */
  STSTREAMER_OK = 0,            /* Status is ok                */
  STSTREAMER_UNDEFINED_STATUS = 1       /* Status is undefined         */
} STSTREAMER_Status_t;

/* Stream types */
/* ------------ */
typedef enum
{
  STSTREAM_AV_STREAM_INVALID,
  STSTREAM_AV_STREAM_AUDIO,
  STSTREAM_AV_STREAM_VIDEO,
  STSTREAM_AV_STREAM_SUBTITLE
} STSTREAM_AVStreamType_t;

/* Stream codec */
/* ------------ */
typedef enum
{
  STSTREAM_STREAMTYPE_INVALID,
  STSTREAM_STREAMTYPE_MP1V,
  STSTREAM_STREAMTYPE_MP2V,
  STSTREAM_STREAMTYPE_MP1A,
  STSTREAM_STREAMTYPE_MP2A,
  STSTREAM_STREAMTYPE_MP3A,
  STSTREAM_STREAMTYPE_TTXT,
  STSTREAM_STREAMTYPE_XSUB,
  STSTREAM_STREAMTYPE_XSUBHD,
  STSTREAM_STREAMTYPE_PCR,
  STSTREAM_STREAMTYPE_AC3,
  STSTREAM_STREAMTYPE_H264,
  STSTREAM_STREAMTYPE_MPEG4P2,
  STSTREAM_STREAMTYPE_VC1,
  STSTREAM_STREAMTYPE_AAC,
  STSTREAM_STREAMTYPE_HEAAC,
  STSTREAM_STREAMTYPE_WMA,
  STSTREAM_STREAMTYPE_DDPLUS,
  STSTREAM_STREAMTYPE_DTS,
  STSTREAM_STREAMTYPE_PCM,
  STSTREAM_STREAMTYPE_LPCM,
  STSTREAM_STREAMTYPE_MSMPEG4V3,
  STSTREAM_STREAMTYPE_ADPCM,
  STSTREAM_STREAMTYPE_FLV1,
  STSTREAM_STREAMTYPE_VP6,
  STSTREAM_STREAMTYPE_VP6F,
  STSTREAM_STREAMTYPE_VORBIS,
  STSTREAM_STREAMTYPE_MJPEG,
  STSTREAM_STREAMTYPE_FLAC,
  STSTREAM_STREAMTYPE_OTHER
} STSTREAM_StreamType_t;

/* Codec id */
/* -------- */
typedef enum
{
  STSTREAM_CODEC_INVALID,

  /* Video Codec */
  STSTREAM_CODEC_MPEG1VIDEO,
  STSTREAM_CODEC_MPEG2VIDEO,
  STSTREAM_CODEC_H264,
  STSTREAM_CODEC_MSMPEG4V3,
  STSTREAM_CODEC_DIVX3,
  STSTREAM_CODEC_DIVX4,
  STSTREAM_CODEC_MPEG4,
  STSTREAM_CODEC_XVID,
  STSTREAM_CODEC_VC1,
  STSTREAM_CODEC_WMV1,
  STSTREAM_CODEC_WMV2,
  STSTREAM_CODEC_WMV3,
  STSTREAM_CODEC_FLV1,
  STSTREAM_CODEC_VP6,
  STSTREAM_CODEC_VP6F,
  STSTREAM_CODEC_MJPEG,

  /* Audio Codec */
  STSTREAM_CODEC_AAC,
  STSTREAM_CODEC_AC3,
  STSTREAM_CODEC_MP2,
  STSTREAM_CODEC_MP3,
  STSTREAM_CODEC_MPEG4AAC,
  STSTREAM_CODEC_WMAV1,
  STSTREAM_CODEC_WMAV2,
  STSTREAM_CODEC_WMA_PRO,
  STSTREAM_CODEC_DTS,
  STSTREAM_CODEC_PCM_U8,
  STSTREAM_CODEC_PCM_S8,
  STSTREAM_CODEC_PCM_S16LE,
  STSTREAM_CODEC_PCM_S16BE,
  STSTREAM_CODEC_PCM_U16LE,
  STSTREAM_CODEC_PCM_U16BE,
  STSTREAM_CODEC_PCM_S24LE,
  STSTREAM_CODEC_PCM_S32LE,
  STSTREAM_CODEC_LPCM,
  STSTREAM_CODEC_ADPCM_MS,
  STSTREAM_CODEC_ADPCM_IMA_WAV,
  STSTREAM_CODEC_VORBIS,
  STSTREAM_CODEC_FLAC,
  STSTREAM_CODEC_NONE
} STSTREAM_CodecId_t;

#endif
