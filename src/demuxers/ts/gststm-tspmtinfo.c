/* Gstreamer ST TS Demuxer Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <gst/gst.h>

#include "gststm-tspmtinfo.h"
#include "gststm-tsstreaminfo.h"

enum
{
  PROP_0,
  PROP_PROGRAM_NO,
  PROP_VERSION_NO,
  PROP_PCR_PID,
  PROP_DESCRIPTORS,
  PROP_STREAM_INFO,
  PROP_LAST
};

#define gst_stm_pmt_info_parent_class parent_class
G_DEFINE_TYPE (GstStmTsPmtInfo, gst_stm_pmt_info, G_TYPE_OBJECT);

static void gst_stm_pmt_info_finalize (GObject * object);
static void gst_stm_pmt_info_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * spec);
static void gst_stm_pmt_info_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * spec);

static void
gst_stm_pmt_info_class_init (GstStmTsPmtInfoClass * klass)
{
  GObjectClass *gobject_klass = (GObjectClass *) klass;

  gobject_klass->finalize = gst_stm_pmt_info_finalize;
  gobject_klass->set_property = gst_stm_pmt_info_set_property;
  gobject_klass->get_property = gst_stm_pmt_info_get_property;

  g_object_class_install_property (gobject_klass, PROP_PROGRAM_NO,
      g_param_spec_uint ("program-number", "Program Number",
          "Program Number for this program", 0, G_MAXUINT16, 1,
          G_PARAM_READABLE));

  g_object_class_install_property (gobject_klass, PROP_PCR_PID,
      g_param_spec_uint ("pcr-pid", "PCR PID",
          "PCR PID", 1, G_MAXUINT16, 1, G_PARAM_READABLE));

  g_object_class_install_property (gobject_klass, PROP_STREAM_INFO,
      g_param_spec_value_array ("stream-info",
          "Array which contains stream object",
          "Array which contains stream object",
          g_param_spec_object ("stream-info", "GstStmPMTStreamInfo",
              " PMT Stream info object",
              GST_STM_TYPE_PMT_STREAM_INFO, G_PARAM_READABLE),
          G_PARAM_READABLE));

  g_object_class_install_property (gobject_klass, PROP_VERSION_NO,
      g_param_spec_uint ("version-number", "Version Number",
          "Version number of this program", 0, G_MAXUINT8, 1,
          G_PARAM_READABLE));

  g_object_class_install_property (gobject_klass, PROP_DESCRIPTORS,
      g_param_spec_value_array ("descriptors",
          "Descriptors",
          "Array of strings",
          g_param_spec_boxed ("descriptor",
              "descriptor",
              "", G_TYPE_GSTRING, G_PARAM_READABLE), G_PARAM_READABLE));
}

static void
gst_stm_pmt_info_init (GstStmTsPmtInfo * pmt_info)
{
  pmt_info->streams = g_value_array_new (0);
  pmt_info->descriptors = g_value_array_new (0);
}

void
gst_stm_pmt_info_add (GstStmTsPmtInfo * info, guint16 program_no,
    guint16 pcr_pid, guint8 version_no)
{
  info->program_no = program_no;
  info->pcr_pid = pcr_pid;
  info->version_no = version_no;

  return;
}

static void
gst_stm_pmt_info_finalize (GObject * object)
{
  GstStmTsPmtInfo *info = GST_STM_PMT_INFO (object);

  g_value_array_free (info->streams);
  g_value_array_free (info->descriptors);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_stm_pmt_info_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * spec)
{
  g_return_if_fail (GST_STM_IS_PMT_INFO (object));

  /* No settable properties */
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, spec);
}

static void
gst_stm_pmt_info_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * spec)
{
  GstStmTsPmtInfo *pmt_info;
  g_return_if_fail (GST_STM_IS_PMT_INFO (object));
  pmt_info = GST_STM_PMT_INFO (object);

  switch (prop_id) {
    case PROP_PROGRAM_NO:
      g_value_set_uint (value, pmt_info->program_no);
      break;
    case PROP_PCR_PID:
      g_value_set_uint (value, pmt_info->pcr_pid);
      break;
    case PROP_STREAM_INFO:
      g_value_set_boxed (value, pmt_info->streams);
      break;
    case PROP_VERSION_NO:
      g_value_set_uint (value, pmt_info->version_no);
      break;
    case PROP_DESCRIPTORS:
      g_value_set_boxed (value, pmt_info->descriptors);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, spec);
      break;
  }
}

void
gst_stm_pmt_info_add_descriptor (GstStmTsPmtInfo * pmt_info,
    const gchar * descriptor, guint length)
{
  GValue value = { 0 };
  GString *string;

  g_return_if_fail (GST_STM_IS_PMT_INFO (pmt_info));

  string = g_string_new_len (descriptor, length);

  g_value_init (&value, G_TYPE_GSTRING);
  g_value_take_boxed (&value, string);
  g_value_array_append (pmt_info->descriptors, &value);
  g_value_unset (&value);
}

void
gst_stm_pmt_info_add_stream (GstStmTsPmtInfo * pmt_info,
    GstStmTsPmtStreamInfo * stream)
{
  GValue v = { 0, };

  g_return_if_fail (GST_STM_IS_PMT_INFO (pmt_info));
  g_return_if_fail (GST_STM_IS_PMT_STREAM_INFO (stream));

  g_value_init (&v, G_TYPE_OBJECT);
  g_value_take_object (&v, stream);
  g_value_array_append (pmt_info->streams, &v);
}
