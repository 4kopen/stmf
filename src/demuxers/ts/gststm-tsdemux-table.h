/* Gstreamer ST TS Demuxer Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STM_TS_DEMUX_TABLE_H__
#define __GST_STM_TS_DEMUX_TABLE_H__

#include "gststm-tsdemux.h"

G_BEGIN_DECLS
/* Defaults PIDs */
#define GST_STM_TS_PAT_PID                      0x0000
#define GST_STM_TS_CAT_PID                      0x0001
#define GST_STM_TS_NIT_PID                      0x0010
#define GST_STM_TS_SDT_PID                      0x0011
#define GST_STM_TS_BAT_PID                      0x0011
#define GST_STM_TS_EIT_PID                      0x0012
#define GST_STM_TS_TDT_PID                      0x0014
#define GST_STM_TS_TOT_PID                      0x0014
/* Tables IDs */
#define GST_STM_TS_PAT_TABLE_ID                 0x00    /* Program Association Table */
#define GST_STM_TS_CAT_TABLE_ID                 0x01    /* Conditional Access Table */
#define GST_STM_TS_PMT_TABLE_ID                 0x02    /* Program Map Table */
#define GST_STM_TS_NIT_TABLE_ID                 0x40    /* Network Info Table (actual transport) */
#define GST_STM_TS_NITO_TABLE_ID                0x41    /* Network Info Table (other transport) */
#define GST_STM_TS_SDT_TABLE_ID                 0x42    /* Service Descriptor Table (actual transport) */
#define GST_STM_TS_SDTO_TABLE_ID                0x46    /* Service Descriptor Table (other transport) */
#define GST_STM_TS_BAT_TABLE_ID                 0x4a    /* Bouquet Association Table */
#define GST_STM_TS_EIT_TABLE_ID                 0x4e    /* Event Information Table (actual transport) */
#define GST_STM_TS_EITO_TABLE_ID                0x4f    /* Event Information Table (other transport) */
#define GST_STM_TS_TDT_TABLE_ID                 0x70    /* Time Date Table */
#define GST_STM_TS_RST_TABLE_ID                 0x71    /* Running Status Table */
#define GST_STM_TS_ST_TABLE_ID                  0x72    /* Stuffing Table */
#define GST_STM_TS_TOT_TABLE_ID                 0x73    /* Time Offset Table */
#define GST_STM_TS_INVALID_TABLE                0x100   /* Invalid Table */
/* PMT Element */
/* ----------- */
    typedef struct
{
  guint16 pid;
  GstStm_ts_StreamType_t type;
  guint32 num_descriptors;
  GstStm_ts_Descriptor_t *descriptor;
  GstTagList *tag_list;
} GstStm_ts_PMTElement_t;

/* PAT Element */
/* ----------- */
typedef struct
{
  guint16 program_num;
  guint16 pid;
} GstStm_ts_PATElement_t;

/* SDT Element */
/* ----------- */
typedef struct
{
  guint16 service_id;
  gboolean eit_schedule_flag;
  gboolean eit_present_following_flag;
  gboolean scrambled;
  guint32 num_descriptors;
  GstStm_ts_Descriptor_t *descriptor;
} GstStm_ts_SDTElement_t;

/* PAT */
/* --- */
typedef struct
{
  guint8 *buffer;
  guint32 buffer_size;
  guint16 ts_id;
  guint32 num_elements;
  GstStm_ts_PATElement_t *element;
} GstStm_ts_PATData_t;

/* PMT */
/* --- */
typedef struct
{
  guint8 *buffer;
  guint8 version_number;        //To obtain the version number for pmtinfo
  guint32 buffer_size;
  guint16 pcr_pid;
  guint16 program_num;
  guint32 num_descriptors;
  GstStm_ts_Descriptor_t *descriptor;
  guint32 num_elements;
  GstStm_ts_PMTElement_t *element;
  gboolean scrambled;
} GstStm_ts_PMTData_t;

/* SDT */
/* --- */
typedef struct
{
  guint16 transport_stream_id;
  guint16 original_networkd_id;
  guint32 num_elements;
  GstStm_ts_SDTElement_t *element;
} GstStm_ts_SDTData_t;

/* Table Data */
/* ---------- */
typedef struct
{
  guint16 pid;
  guint32 table_id;
  guint32 program_num;
  guint32 section_num;
  guint32 cur_not_next;
  guint8 *buf_data;
  guint32 buf_size;
  guint32 *read_size;
} GstStm_ts_TableData_t;

/* Section Data */
/* ------------ */
typedef struct
{
  guint32 num_bytes_to_copy_by_section;
  guint8 section_num_to_filter;
  guint8 section_syntax_indicator;
  guint8 pusi_to_filter;
  guint8 *ts_data;
  guint32 ts_size;
  gboolean ts_section_found;
} GstStm_ts_SectionData_t;

/* ECM */
/* ------------ */
typedef struct
{
  guint16 pid;
  guint codec;
  guint stream_type;
  stream_content_t content;
  gboolean is_found;
} GstStm_ts_ECMData_t;

struct _Gststm_ts_pmt_info
{
  GstObject gstobject;

  guint program_number;
  guint version_number;
  guint pcr_pid;
  GstStm_ts_Descriptor_t *descriptors;
};

/* For table parsing */
gboolean gst_stm_ts_demux_parse_table (Gststm_ts_demux * demux);
gboolean gst_stm_ts_demux_pat_acquire (Gststm_ts_demux * demux,
    guint16 pid, GstStm_ts_PATData_t * table_data, GstBuffer ** section_buf);
gboolean gst_stm_ts_demux_pat_delete (GstStm_ts_PATData_t * table_data);
gboolean gst_stm_ts_demux_pmt_acquire (Gststm_ts_demux * demux,
    guint16 pid, GstStm_ts_PMTData_t * table_data, GstBuffer ** section_buf);
gboolean gst_stm_ts_demux_pmt_delete (GstStm_ts_PMTData_t * table_data);
gboolean gst_stm_ts_demux_sdt_acquire (Gststm_ts_demux * demux,
    guint16 pid, GstStm_ts_SDTData_t * table_data, GstBuffer ** section_buf);
gboolean gst_stm_ts_demux_sdt_delete (GstStm_ts_SDTData_t * table_data);
gboolean gst_stm_ts_demux_pmt_get_info (GstStm_ts_PMTElement_t *
    pmt_element, guint * codec, guint16 * pid, stream_content_t * content,
    gchar * string_type, GstStm_ts_ECMData_t * ecm_data);
gboolean gst_stm_ts_demux_descriptor_registration (GstStm_ts_Descriptor_t
    * descriptor, guint8 * format_id);
gboolean gst_stm_ts_demux_xxx_acquire (Gststm_ts_demux * demux,
    GstStm_ts_TableData_t * table_data, GstBuffer ** section_buf);
gboolean gst_stm_ts_demux_section_parse (Gststm_ts_demux * demux,
    GstStm_ts_TableData_t * table_data, GstStm_ts_SectionData_t * section_data);
void gst_stm_ts_demux_save_table_info (Gststm_ts_demux * demux,
    GstStm_ts_PATElement_t * pat_element, stream_content_t content, guint16 pid,
    guint codec, gchar * string_type, gboolean scrambled);
void gst_stm_ts_demux_save_pmt_info (Gststm_ts_demux * demux,
    GstStm_ts_PMTData_t * pmt_data, gint stream_index,
    Gststm_ts_stream * stream);
G_END_DECLS
#endif /* __GST_STM_TS_DEMUX_TABLE_H__ */
