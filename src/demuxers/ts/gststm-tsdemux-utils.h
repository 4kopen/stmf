/* Gstreamer ST TS Demuxer Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STM_TS_DEMUX_UTILS_H__
#define __GST_STM_TS_DEMUX_UTILS_H__

/* the code here will be removed to one generic demux utils file not ts specific later */

GstCaps *gst_stm_ts_demux_map_caps (Gststm_ts_demux * demux, gint index);
GstClockTime gst_stm_ts_demux_time_to_gst (guint64 time);
guint64 gst_stm_time_gst_to_ts_demux (GstClockTime gst_time);
guint64 gst_stm_ts_demux_pts_diff (Gststm_ts_demux * demux);
guint gst_stm_ts_demux_get_time_in_ms (void);
#endif
