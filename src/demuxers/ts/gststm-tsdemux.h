/* Gstreamer ST TS Demuxer Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STM_TS_DEMUX_H__
#define __GST_STM_TS_DEMUX_H__

#include <gst/gst.h>
#include <gst/base/gstadapter.h>
#include <linux/dvb/dmx.h>
#include "gststm-file.h"
#include "gststm-demux-utils.h"
#include "pes.h"
#include "gststm-tspmtinfo.h"

#define MULTIPLE_AV_STREAM_WA 1
#define GST_STM_TS_DEMUX_MAX_STREAMS 128

/* Constant */
/* -------- */
#define DEFAULT_PROP_ES_PIDS                    ""
#define DEFAULT_PROP_TS_TABLE                   ""
#define GST_STM_TS_DEMUX_FIRST_PTS              0
#define GST_STM_TS_DEMUX_LAST_PTS               1
#define GST_STM_TS_DEMUX_NO_SEEK                0xdeadbeef
#define GST_STM_TS_DEMUX_MAX_DEVICE_NUM         16
#define GST_STM_TS_DEMUX_FLUSH_RETRY            50
#define GST_STM_TS_DEMUX_NUM_DISCONTINUE_PACKET 1
#define GST_STM_RTP_PACKET_SIZE                 1328

#define SCAN_COUNT                              5
#define BLOCK_COUNT                             2
#define GST_STM_TS_DEMUX_TS_HEADER_SIZE         4
#define GST_STM_TS_DEMUX_TS_PACKET_SIZE         188
#define BD_TP_PACKET_SIZE                       192
#define TIMEOUT_MS                              100
#define NUMBER_PACKETS                          (GST_STM_TS_DEMUX_TS_PACKET_SIZE*BLOCK_COUNT)
#define BUFFER_SIZE                             (GST_STM_TS_DEMUX_TS_PACKET_SIZE*BD_TP_PACKET_SIZE*BLOCK_COUNT)
#define NUMBER_SCAN_PACKETS                     (GST_STM_TS_DEMUX_TS_PACKET_SIZE*SCAN_COUNT)

#define SYNC_BYTE                               0x47
#define SYNC_BYTE_MASK                          0x000000ff

#define PACKET_ERROR_MASK                       0x00008000
#define PID_HIGH_MASK                           0x00001f00
#define PID_LOW_MASK                            0x00ff0000
#define PAYLOAD_UNIT_START_MASK                 0x00004000
#define CONTINUITY_COUNT_MASK                   0x0f000000
#define PAYLOAD_PRESENT_MASK                    0x10000000
#define ADAPTATION_FIELD_MASK                   0x20000000
#define DISCONTINUITY_INDICATOR_MASK            0x80

#define VALID_PACKET( H )                       (((H & SYNC_BYTE_MASK) == SYNC_BYTE) && ((H & PACKET_ERROR_MASK) == 0))
#define PID( H )                                ((H & PID_HIGH_MASK) | ((H & PID_LOW_MASK) >> 16))
#define PUSI( H )                               ((H & PAYLOAD_UNIT_START_MASK) != 0)
#define CONTINUITY_COUNT( H )                   ((H & CONTINUITY_COUNT_MASK) >> 24)
#define PAYLOAD_PRESENT( H )                    ((H & PAYLOAD_PRESENT_MASK) != 0)
#define ADAPTATION_FIELD( H )                   ((H & ADAPTATION_FIELD_MASK) != 0)

/* MPEG */
#define SEQUENCE_HEADER                         0xB3
#define PICTURE_TYPE_OFFSET                     0x01
#define PICTURE_TYPE_SHIFT                      0x03
#define PICTURE_TYPE_MASK                       0x07
#define PICTURE_TYPE( P )                       ((P >> PICTURE_TYPE_SHIFT) & PICTURE_TYPE_MASK)
#define I_FRAME( P )                            (PICTURE_TYPE( P ) == 0x01)
#define MPEG_SEQUENCE_START_CODE                0xb3

/* H264 */
#define SliceOrIDR(V)                           (((V)&0x1b) == 1)       /* is this nal unit a slice, or IDR */
#define IDRUnit(V)                              (((V)&0x1f) == 5)       /* is this nal unit an IDR */
                                                                        /* first byte of header, is first macroblock 0, and frame type I(3), or I_ALL(7) */
#define IFrameUnit(V)                           ((((V)&0xf0) == 0xb0) || (((V)&0xff) == 0x88))

#define VC1_SEQUENCE_START_CODE                 0x0f
#define AVS_SEQUENCE_START_CODE                 0xb0

/* HEVC */
#define IDRorCRAorBLA(V)                        (((((V)>>1)&0x3f) >= 16) &&  ((((V)>>1)&0x3f) <= 21))   /* is this nal unit a CRA a BLA or an IDR */
#define FirstISlice(V)                          (((V)&0x80) != 0)       /* is this nal unit belong to first slice */


/* Table buffer size */
#define GST_STM_TS_DEMUX_TABLE_MAX_BUFFER_SIZE  256*1024

/* Enum to avoid to filter some particular fields */
#define GST_STM_TS_DEMUX_FILTER_DONT_CARE       0x80000000
#define GST_STM_TS_DEMUX_FILTER_AUTOMATIC       0x40000000

/* time diff of target seek position, less than 5000 msec is ignored in
 * ptswrap checking. This define is sensitive to change
 * value has been taken based on experiments */
#define GST_STM_TS_DEMUX_SEEKPOS_TIMEDIFF_DONTCARE    (5000*GST_MSECOND)

/* pts diff less than 30 sec during normal play back is ignored
 * This define is sensitive to change */
#define GST_STM_TS_DEMUX_PLAYBACK_PTSDIFF_DONTCARE    (5*GST_SECOND)

/* RTM is reverse trick mode */
/* this define is to indicate the playback time equal to data injected
 * in single injection before any discontinuity */
#define GST_STM_TS_DEMUX_RTM_INJECTION_TIME           (1*GST_SECOND)

typedef struct _Gststm_ts_pmt_info Gststm_ts_pmt_info;

/* Stream Types */
/* ------------ */
typedef enum
{
  GST_STM_TS_DEMUX_STREAM_RESERVED = 0x00,
  GST_STM_TS_DEMUX_STREAM_VIDEO_MPEG1 = 0x01,
  GST_STM_TS_DEMUX_STREAM_VIDEO_MPEG2 = 0x02,
  GST_STM_TS_DEMUX_STREAM_AUDIO_MPEG1 = 0x03,
  GST_STM_TS_DEMUX_STREAM_AUDIO_MPEG2 = 0x04,
  GST_STM_TS_DEMUX_STREAM_PRIVATE_SECTIONS = 0x05,
  GST_STM_TS_DEMUX_STREAM_PRIVATE_PES = 0x06,
  GST_STM_TS_DEMUX_STREAM_MHEG = 0x07,
  GST_STM_TS_DEMUX_STREAM_DSM_CC = 0x08,
  GST_STM_TS_DEMUX_STREAM_TYPE_H2221 = 0x09,
  GST_STM_TS_DEMUX_STREAM_TYPE_A = 0x0A,
  GST_STM_TS_DEMUX_STREAM_TYPE_B = 0x0B,
  GST_STM_TS_DEMUX_STREAM_TYPE_C = 0x0C,
  GST_STM_TS_DEMUX_STREAM_TYPE_D = 0x0D,
  GST_STM_TS_DEMUX_STREAM_TYPE_AUX = 0x0E,
  GST_STM_TS_DEMUX_STREAM_AUDIO_AAC_ADTS = 0x0F,
  GST_STM_TS_DEMUX_STREAM_AUDIO_AAC_LATM = 0x11,
  GST_STM_TS_DEMUX_STREAM_AUDIO_AAC_RAW1 = 0x12,
  GST_STM_TS_DEMUX_STREAM_AUDIO_AAC_RAW2 = 0x13,
  GST_STM_TS_DEMUX_STREAM_VIDEO_MPEG4 = 0x1B,
  GST_STM_TS_DEMUX_STREAM_VIDEO_HEVC = 0x24,
  GST_STM_TS_DEMUX_STREAM_VIDEO_AVS = 0x42,
  GST_STM_TS_DEMUX_STREAM_VIDEO_AVSP = 0x43,
  GST_STM_TS_DEMUX_STREAM_AUDIO_LPCM = 0x80,
  GST_STM_TS_DEMUX_STREAM_AUDIO_AC3 = 0x81,
  GST_STM_TS_DEMUX_STREAM_AUDIO_DTS = 0x82,
  GST_STM_TS_DEMUX_STREAM_AUDIO_MLP = 0x83,
  GST_STM_TS_DEMUX_STREAM_AUDIO_DDPLUS = 0x84,
  GST_STM_TS_DEMUX_STREAM_AUDIO_DTSHD = 0x85,
  GST_STM_TS_DEMUX_STREAM_AUDIO_DTSHD_XLL = 0x86,
  GST_STM_TS_DEMUX_STREAM_AUDIO_EAC3 = 0x87,
  GST_STM_TS_DEMUX_STREAM_AUDIO_DDPLUS_2 = 0xA1,
  GST_STM_TS_DEMUX_STREAM_AUDIO_DTSHD_2 = 0xA2,
  GST_STM_TS_DEMUX_STREAM_VIDEO_VC1 = 0xEA,
  GST_STM_TS_DEMUX_STREAM_AUDIO_WMA = 0xE6,
  /* stream ID 0xF0 (fixed ECM stream value) defined in ISO/IEC 13818-1 */
  GST_STM_TS_DEMUX_STREAM_ECM = 0xF0
} GstStm_ts_StreamType_t;

/* Descriptor Types */
/* ---------------- */
typedef enum
{
  GST_STM_TS_DEMUX_DESCRIPTOR_REGISTRATION = 0x05,
  GST_STM_TS_DEMUX_DESCRIPTOR_TARGET_BACKGROUND_GRID = 0x07,
  GST_STM_TS_DEMUX_DESCRIPTOR_CA = 0x09,
  GST_STM_TS_DEMUX_DESCRIPTOR_LANGUAGE_NAME = 0x0A,
  GST_STM_TS_DEMUX_DESCRIPTOR_MPEG4_VIDEO = 0x1B,
  GST_STM_TS_DEMUX_DESCRIPTOR_MPEG4_AUDIO = 0x1C,
  GST_STM_TS_DEMUX_DESCRIPTOR_TELETEXT = 0x56,
  GST_STM_TS_DEMUX_DESCRIPTOR_SUBTITLING = 0x59,
  GST_STM_TS_DEMUX_DESCRIPTOR_AC3 = 0x6A,
  GST_STM_TS_DEMUX_DESCRIPTOR_ENHANCED_AC3 = 0x7A,
  GST_STM_TS_DEMUX_DESCRIPTOR_DTS = 0x7B,
  GST_STM_TS_DEMUX_DESCRIPTOR_AAC = 0x7C
} GstStm_ts_DescriptorType_t;

/* Descriptor Element */
/* ------------------ */
typedef struct
{
  GstStm_ts_DescriptorType_t tag;
  guint8 size;
  guint8 *data;
} GstStm_ts_Descriptor_t;

G_BEGIN_DECLS
/* #defines don't like whitespacey bits */
#define GST_TYPE_STM_TS_DEMUX (gst_stm_ts_demux_get_type())
#define GST_STM_TS_DEMUX(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_STM_TS_DEMUX,Gststm_ts_demux))
#define GST_STM_TS_DEMUX_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_STM_TS_DEMUX,Gststm_ts_demuxClass))
#define GST_IS_STM_TS_DEMUX(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_STM_TS_DEMUX))
#define GST_IS_STM_TS_DEMUX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_STM_TS_DEMUX))
typedef struct _Gststm_ts_stream Gststm_ts_stream;
typedef struct _Gststm_ts_demux Gststm_ts_demux;
typedef struct _Gststm_ts_demuxClass Gststm_ts_demuxClass;

/* ECM Structures */
typedef struct
{
  guint32 systemID[4];          /* UUID to define content protection system */
  guint32 data_size;            /* size of protection system specific data in bytes */
  guint8 *data;                 /* protection system specific data */
} GstStm_ts_PSSH_ECM_Data;

typedef struct
{
  guint16 bytes_clear_data;     /* number of bytes of clear data */
  guint32 bytes_encrypted_data; /* number of bytes of encrypted data */
} GstStm_ts_SENC_ECM_Pes_Subsample_Data;

typedef struct
{
  guint8 *initialization_vector;/* initialization vector value */
  guint16 subsamples_count;     /* number of subsamples entries for the sample */
  GstStm_ts_SENC_ECM_Pes_Subsample_Data *senc_pes_subsample_data;
} GstStm_ts_SENC_ECM_Pes_Sample_Data;

typedef struct
{
  guint64 pts_timestamp;        /* PTS timestamp of encrypted PES packet */
  guint8 sample_count;          /* number of elementary stream samples */
  GstStm_ts_SENC_ECM_Pes_Sample_Data *senc_pes_sample_data;
} GstStm_ts_SENC_ECM_Pes_Data;

typedef struct
{
  guint8 scrambling_control;    /* scrambling control value, must be 01 or 10 */
  guint8 stream_params_included;/* specifies the presence of stream encryption parameter */
  guint16 scrambled_es_pid;     /* PID of the scrambled ES */
  guint32 algorithm_ID;         /* identifier of the encryption algorithm */
  guint8 iv_size;               /* size in bytes of initialization vector field */
  guint8 kid[16];               /* UUID to identifies the key for decryption */
  guint8 pes_count;             /* number of pes packets described by this structure */
  GstStm_ts_SENC_ECM_Pes_Data *senc_pes_data;
} GstStm_ts_SENC_ECM_Data;

typedef struct
{
  guint16 pes_packet_length;    /* PES packet length as in PES header */
  guint8 pssh_count;            /* count of PSSH_ECM_Data structures included */
  guint8 scrambled_stream_count;/* count of SENC_ECM_Data structures included */
  GstStm_ts_PSSH_ECM_Data *pssh_data;
  GstStm_ts_SENC_ECM_Data *senc_data;
} GstStm_ts_ECM_PES_Packet;

typedef struct _SencProtectionMeta SencProtectionMeta;

struct _SencProtectionMeta
{
  guint iv_size;                /* size in bytes of initialization vector field */
  guint subsample_count;        /* size in bytes of subsample info field */
  GValue iv;                    /* initialization vector value */
  GValue subsample_info;        /* subsample info for the sample */
};

struct _Gststm_ts_stream
{
  stream_content_t content;
  guint pid;
  guint codec;
  guint stream_id;
  gboolean discontinue;
  GstFlowReturn last_flow;      /* used to store last flow error on the stream pad */
  gboolean first_buffer_sent;   /* TRUE if a buffer has been pushed for a pid */
  GstTagList *tag_list;

  GstPad *pad;

  guint dev_id;                 /* demux device for stream                      */
  gint fd;
  gchar *padname;               /* name of the pad */
  guint64 prev_pts;             /* prev valid timestamp */
  /* protected streams */
  gboolean is_protected;        /* stream is protected or not */
  GstStm_ts_SENC_ECM_Data *ecm_data;/* SENC ECM data for the stream */
  GstAdapter *adapter;          /* adapter to hold stream packets */
  GQueue ecm_queue;             /* associated ecm packet queue for this stream */
  gboolean header_required;     /* to check if header is parsed or not */
  guint32 pes_packet_size;      /* current pes packet size calculated from header*/
  guint8 pes_header[6];         /* current pes header */
  guint32 pes_counter;          /* to sync with pes packets repsesented by current ecm */
  guint32 scrambled_pes_count;  /* number of pes packets represented by current ecm */
  guint8 zero_header[4];        /* pes header with non-deterministic packet size */
  guint32 zero_pes_size;        /* packet size calculated for non-deterministic pes */
  gboolean zero_pes;            /* if non-deterministic pes found in the stream */
};

struct _Gststm_ts_demux
{
  GstElement element;
  GstPad *sinkpad;

  guint dev_id;                 /* dvr device                                   */
  gint dvr_fd;
  gint pcr_fd;                  /* pcr device fd */
  gint pcr_pid;                 /* pcr pid */
  GstClockTime start_time;      /* stream start time                            */
  GstClockTime duration;        /* stream duration                              */
  GstClockTime rec_duration;    /* recorded stream duration                     */
  gint64 strm_pos;              /* stream position */
  GstSegment segment;
  gboolean newsegment;          /* tsdemux should send a new segment event */
  gboolean seek_event;          /*  a seek event was processed */
  gint vpads_num;               /* number of video pad created                  */
  gint apads_num;               /* number of audio pad created                  */
  gint spads_num;               /* number of subtitle pad created               */
  gint num_streams;
  guint ref_index;              /* normally it is the stream index for video    */
  guint push_index;             /* current stream index that needs to be push   */
  guint prog_num;
  gboolean streaming;           /* TRUE if working in pull-mode                 */
  gboolean flushing;            /* used during seek operation                   */
  gboolean running;
  gboolean opened;
  gboolean dev_opened;
  gboolean demux_opened;
  gboolean seeking;
  gboolean internal_seek;       /* demuxer initiated upstream seeks */
  gboolean enable_playback;
  gboolean eos;
  gboolean eos_event_sent;
  gboolean disc_read;           /* TRUE after getting discontinue dummy pes */
  gboolean hw_demux;
  gboolean upstream_time_index_support;
  gboolean upstream_rap_index_support;
  gboolean upstream_seekable;   /* TRUE if upstream element is seekable */
  gboolean prev_seek_res;
  gint64 prev_seek_seqnum;
  guint group_id;               /* unique id common for all streams to be played together, flows with STREAM START event */
  gboolean have_group_id;       /* flag to identify if group id is available from upstream STREAM START event or to be newly generated */

  gchar *table;
  guint table_ptr;

  Gststm_ts_stream *streams[GST_STM_TS_DEMUX_MAX_STREAMS];

  GstStmTsPmtInfo *pmt_info;
  GstStm_ts_Descriptor_t *descriptor;

  guint8 no_pes_count;
  guint packetsize;
  guint packetoffset;
  guint streamoffset;
  guint64 filesize;
  gboolean live;                /* True if source element is live */
  guint64 first_pts;
  guint64 start_pts;
  guint64 last_pts;
  guint64 prev_pts;
  guint64 offset;
  guint64 first_offset;
  guint64 last_offset;
  gboolean srcresult;
  gboolean pids_set_by_property;        /* flag to identify if pids have been set through property */
  gboolean pids_set_by_event;   /* flag to identify if pids have been set through event received from st_recorder */

  /* For push mode */
  GstPipeContext pipe;
  GstTask *write_task;          /* This task is only used in push mode. In pull mode, the pad task is created */
  GRecMutex write_task_lock;    /* By setting gst_task_set_lock(), the mutex must be acquired
                                   before calling the GstTaskFunc. After calling gst_task_pause(),
                                   we need to lock and unlock this mutex to be sure the task is
                                   really paused (gst_task_pause() just set the task state to pause)
                                   For pull mode, stream lock will be use instead of this */
  GstTask *read_task;
  GRecMutex read_task_lock;
  gboolean sync_lock;           /* sync is locked or not */
  gint requested_program_number;        /* requested program number to be played */
  guint table_id;               /* table id to be parsed */
  gboolean need_unlock;         /* need to unlock loop */
/* protection support */
  GPtrArray *protection_system_ids;/* protection system unique identification */
  GQueue protection_event_queue;/* queue for protection events */
  gchar *src_uri;               /* source uri type */
};

struct _Gststm_ts_demuxClass
{
  GstElementClass parent_class;
};

GType gst_stm_ts_demux_get_type (void);
gboolean sttsdemux_init (GstPlugin * plugin);

void gst_stm_ts_demux_register_stream (Gststm_ts_demux * demux,
    stream_content_t content, guint pid, guint codec, gboolean protected);
/* For file operation */
gboolean gst_stm_ts_demux_read (Gststm_ts_demux * demux,
    GstBuffer ** buf, guint64 offset, guint size, gboolean copy);
gboolean gst_stm_ts_demux_is_upstream_seekable (Gststm_ts_demux * demux,
    GstFormat format);
gboolean gst_stm_ts_demux_push_mode_seek (Gststm_ts_demux * demux, gdouble rate,
    GstFormat format, GstSeekType start_type, gint64 start, GstSeekFlags flags);
/* For reverse playback */
gboolean gst_stm_ts_demux_reverse (Gststm_ts_demux * demux);
/* For packet validation */
gboolean gst_stm_ts_demux_scan_valid_packets (Gststm_ts_demux * demux,
    guint8 * data, guint size, guint * packet_size, guint * packets_start,
    guint * num_packets, gboolean lock);

G_END_DECLS
#endif /* __GST_STM_TS_DEMUX_H__ */
