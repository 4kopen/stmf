/* Gstreamer ST TS Demuxer Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>

#include "gststm-tsstreaminfo.h"

enum
{
  PROP_0,
  PROP_PID,
  PROP_LANGUAGES,
  PROP_STREAM_TYPE,
  PROP_DESCRIPTORS,
  PROP_CONTENT,
  PROP_LAST
};

G_DEFINE_TYPE (GstStmTsPmtStreamInfo, gst_stm_pmt_stream_info, G_TYPE_OBJECT);

static void gst_stm_pmt_stream_info_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * spec);
static void gst_stm_pmt_stream_info_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * spec);
static void gst_stm_pmt_stream_info_finalize (GObject * object);

static void
gst_stm_pmt_stream_info_class_init (GstStmTsPmtStreamInfoClass * klass)
{
  GObjectClass *gobject_klass = (GObjectClass *) klass;

  gobject_klass->set_property = gst_stm_pmt_stream_info_set_property;
  gobject_klass->get_property = gst_stm_pmt_stream_info_get_property;
  gobject_klass->finalize = gst_stm_pmt_stream_info_finalize;

  g_object_class_install_property (gobject_klass, PROP_PID,
      g_param_spec_uint ("pid", "PID carrying this stream",
          "PID which carries this stream", 1, G_MAXUINT16, 1,
          G_PARAM_READABLE));

  g_object_class_install_property (gobject_klass, PROP_CONTENT,
      g_param_spec_uint ("content",
          "Content type of the stream",
          "Stream type of this stream video/audio/subtitle/pcr/other", 0,
          G_MAXUINT8, 0, G_PARAM_READABLE));

  g_object_class_install_property (gobject_klass, PROP_LANGUAGES,
      g_param_spec_value_array ("languages", "Languages of this stream",
          "Value array of the languages of this stream",
          g_param_spec_string ("language", "language", "language", "",
              G_PARAM_READABLE), G_PARAM_READABLE));

  g_object_class_install_property (gobject_klass, PROP_STREAM_TYPE,
      g_param_spec_uint ("stream-type",
          "Stream type", "Stream type", 0, G_MAXUINT8, 0, G_PARAM_READABLE));

  g_object_class_install_property (gobject_klass, PROP_DESCRIPTORS,
      g_param_spec_value_array ("descriptors",
          "Descriptors",
          "Value array of strings containing stream descriptors",
          g_param_spec_boxed ("descriptor",
              "descriptor",
              "", G_TYPE_GSTRING, G_PARAM_READABLE), G_PARAM_READABLE));
}

static void
gst_stm_pmt_stream_info_init (GstStmTsPmtStreamInfo * pmt_stream_info)
{
  pmt_stream_info->languages = g_value_array_new (0);
  pmt_stream_info->descriptors = g_value_array_new (0);
}

static void
gst_stm_pmt_stream_info_finalize (GObject * object)
{
  GstStmTsPmtStreamInfo *info = GST_STM_PMT_STREAM_INFO (object);

  g_value_array_free (info->languages);
  g_value_array_free (info->descriptors);
}


GstStmTsPmtStreamInfo *
gst_stm_pmt_stream_info_new (guint16 pid, guint8 type, stream_content_t content)
{
  /* Create the pmt information handle and save the data */
  GstStmTsPmtStreamInfo *info;
  info = g_object_new (GST_STM_TYPE_PMT_STREAM_INFO, NULL);

  info->pid = pid;
  info->stream_type = type;
  info->content = content;
  return info;
}

void
gst_stm_pmt_stream_info_add_language (GstStmTsPmtStreamInfo * pmt_info,
    gchar * language)
{
  GValue v = { 0, };

  g_return_if_fail (GST_STM_IS_PMT_STREAM_INFO (pmt_info));

  g_value_init (&v, G_TYPE_STRING);
  g_value_take_string (&v, language);
  /* Append the languages to the array */
  g_value_array_append (pmt_info->languages, &v);
  g_value_unset (&v);
}

void
gst_stm_pmt_stream_info_add_descriptor (GstStmTsPmtStreamInfo * pmt_info,
    const gchar * descriptor, guint length)
{
  GValue value = { 0 };
  GString *string;

  g_return_if_fail (GST_STM_IS_PMT_STREAM_INFO (pmt_info));

  string = g_string_new_len (descriptor, length);

  g_value_init (&value, G_TYPE_GSTRING);
  g_value_take_boxed (&value, string);
  g_value_array_append (pmt_info->descriptors, &value);
  g_value_unset (&value);
}

static void
gst_stm_pmt_stream_info_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * spec)
{
  g_return_if_fail (GST_STM_IS_PMT_STREAM_INFO (object));

  /* No settable properties */
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, spec);
}

static void
gst_stm_pmt_stream_info_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * spec)
{
  GstStmTsPmtStreamInfo *si;

  g_return_if_fail (GST_STM_IS_PMT_STREAM_INFO (object));

  si = GST_STM_PMT_STREAM_INFO (object);

  switch (prop_id) {
    case PROP_STREAM_TYPE:
      g_value_set_uint (value, si->stream_type);
      break;
    case PROP_PID:
      g_value_set_uint (value, si->pid);
      break;
    case PROP_LANGUAGES:
      g_value_set_boxed (value, si->languages);
      break;
    case PROP_DESCRIPTORS:
      g_value_set_boxed (value, si->descriptors);
      break;

    case PROP_CONTENT:
      g_value_set_uint (value, si->content);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, spec);
      break;
  }
}
