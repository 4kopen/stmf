/* Gstreamer ST TS Demuxer Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STM_PMT_INFO_H__
#define __GST_STM_PMT_INFO_H__

#include <glib.h>
#include "gststm-tsstreaminfo.h"



G_BEGIN_DECLS
#define GST_TYPE_STM_PMT_INFO (gst_stm_pmt_info_get_type ())
#define GST_STM_IS_PMT_INFO(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_STM_PMT_INFO))
#define GST_STM_PMT_INFO(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_STM_PMT_INFO, GstStmTsPmtInfo))
    typedef struct GstStmTsPmtInfo
{
  GstElement parent;

  guint16 program_no;
  guint16 pcr_pid;

  guint8 version_no;

  GValueArray *descriptors;
  GValueArray *streams;
} GstStmTsPmtInfo;

typedef struct GstStmTsPmtInfoClass
{
  GstElementClass parent_class;
} GstStmTsPmtInfoClass;

void gst_stm_pmt_info_add_stream (GstStmTsPmtInfo * pmt_info,
    GstStmTsPmtStreamInfo * stream);
void gst_stm_pmt_info_add_descriptor (GstStmTsPmtInfo * pmt_info,
    const gchar * descriptor, guint length);
void gst_stm_pmt_info_add (GstStmTsPmtInfo * info, guint16 program_no,
    guint16 pcr_pid, guint8 version_no);
GType gst_stm_pmt_info_get_type (void);

G_END_DECLS
#endif
