/* Gstreamer ST TS Demuxer Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __GST_STM_PMT_STREAM_INFO_H__
#define __GST_STM_PMT_STREAM_INFO_H__
#include <gststm-demux-utils.h>
#include <glib.h>


G_BEGIN_DECLS
#define GST_STM_TYPE_PMT_STREAM_INFO (gst_stm_pmt_stream_info_get_type ())
#define GST_STM_IS_PMT_STREAM_INFO(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_STM_TYPE_PMT_STREAM_INFO))
#define GST_STM_PMT_STREAM_INFO(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_STM_TYPE_PMT_STREAM_INFO, GstStmTsPmtStreamInfo))
    typedef struct GstStmTsPmtStreamInfo
{
  GObject parent;

  guint16 pid;
  GValueArray *languages;       /* null terminated 3 character ISO639 language code */
  guint8 stream_type;
  GValueArray *descriptors;
  stream_content_t content;
} GstStmTsPmtStreamInfo;

typedef struct GstStmTsPmtStreamInfoClass
{
  GObjectClass parent_class;
} GstStmTsPmtStreamInfoClass;

GstStmTsPmtStreamInfo *gst_stm_pmt_stream_info_new (guint16 pid, guint8 type,
    stream_content_t content);

void gst_stm_pmt_stream_info_add_language (GstStmTsPmtStreamInfo * si,
    gchar * language);
void gst_stm_pmt_stream_info_add_descriptor (GstStmTsPmtStreamInfo * pmt_info,
    const gchar * descriptor, guint length);

GType gst_stm_pmt_stream_info_get_type (void);

G_END_DECLS
#endif
