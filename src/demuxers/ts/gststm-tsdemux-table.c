/* Gstreamer ST TS Demuxer Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:element-stm_ts_demux
 *
 * TS packets table parsing.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/tag/tag.h>
#include <string.h>
#include <stdio.h>

#include <linux/dvb/stm_ioctls.h>

#include "gststm-tsdemux.h"
#include "gststm-tsdemux-utils.h"
#include "gststm-file.h"

#include "gststm-tspmtinfo.h"
#include "gststm-tsstreaminfo.h"
#include "gststm-tsdemux-table.h"

#define STRING_TYPE_MAX_SIZE 256
#define CA_SYSTEM_ID 0x4b13     //CENC Specification

gboolean
gst_stm_ts_demux_parse_table (Gststm_ts_demux * demux)
{
  GstStm_ts_PATData_t pat;
  GstStm_ts_PMTData_t pmt;
  GstStm_ts_SDTData_t sdt;
  GstBuffer *section_buf = NULL;
  gint i, j, k;
  gboolean scrambled = FALSE;
  gboolean vid_found = FALSE;
  gboolean aud_found = FALSE;
  /* subtitle and teletext will share the same GDP plane */
  /* so 1st subtitle or teletext will only be parsed and selected */
  gboolean sub_found = FALSE;
  gboolean ecm_found = FALSE;
  Gststm_ts_stream stream;

  demux->table_id = GST_STM_TS_SDT_TABLE_ID;
  gst_stm_ts_demux_sdt_acquire (demux, GST_STM_TS_SDT_PID, &sdt, &section_buf);

  demux->table_id = GST_STM_TS_PAT_TABLE_ID;
  /* Try to identify the pids */
  if (gst_stm_ts_demux_pat_acquire (demux, GST_STM_TS_PAT_PID, &pat,
          &section_buf) == TRUE) {
    /* Look for the program requested into the PAT */
    for (i = 0; i < pat.num_elements; i++) {
      /* Get the PMT for this program */
      if (pat.element[i].program_num != 0) {
        pmt.program_num = pat.element[i].program_num;
        demux->table_id = GST_STM_TS_PMT_TABLE_ID;
        if (gst_stm_ts_demux_pmt_acquire (demux, pat.element[i].pid,
                &pmt, &section_buf) == TRUE) {
          scrambled = FALSE;

          /* Get SDT with for this PMT */
          for (k = 0; k < sdt.num_elements; k++) {
            if (sdt.element[k].service_id == pmt.program_num) {
              scrambled = sdt.element[k].scrambled;
              break;
            }
          }

          /* Get the list of pids */
          for (j = 0; j < pmt.num_elements; j++) {
            guint codec;
            guint16 pid;
            stream_content_t content;
            GstStm_ts_ECMData_t ecm_data;
            gchar string_type[STRING_TYPE_MAX_SIZE];

            ecm_data.codec = VIDEO_ENCODING_NONE;
            ecm_data.stream_type = GST_STM_TS_DEMUX_STREAM_RESERVED;
            ecm_data.content = STREAM_CONTENT_OTHER;
            ecm_data.is_found = FALSE;

            pmt.element[j].tag_list = gst_tag_list_new_empty ();
            /* Analysis of each pmt element */
            if (gst_stm_ts_demux_pmt_get_info (&pmt.element[j], &codec, &pid,
                    &content, string_type, &ecm_data) == TRUE) {
              /* check if user has requested for a specific program to play */
              if ((demux->requested_program_number == -1)
                  || (demux->requested_program_number == pmt.program_num)) {
                if ((content == STREAM_CONTENT_VIDEO) && (vid_found == FALSE)
                    && (scrambled == FALSE)) {
                  if (((vid_found == FALSE) && (!aud_found))
                      || demux->prog_num == pmt.program_num) {
                    if ((ecm_data.is_found == TRUE) && (ecm_found == FALSE)) {
                      gst_stm_ts_demux_register_stream (demux,
                          ecm_data.content, ecm_data.pid,
                          ecm_data.codec, FALSE);
                      ecm_found = TRUE;
                    }

                    vid_found = TRUE;
                    demux->prog_num = pmt.program_num;
                    gst_stm_ts_demux_register_stream (demux, content, pid,
                        codec, ecm_found);
                    /* Inside the gst_stm_ts_demux_register_stream() call the num_streams
                     * is incremented after update, so updaing num_streams-1 entry
                     */
                    demux->streams[demux->num_streams - 1]->tag_list =
                        pmt.element[j].tag_list;
                  }
                } else if ((content == STREAM_CONTENT_AUDIO)
                    && (scrambled == FALSE)) {
                  if (((vid_found == FALSE) && (!aud_found))
                      || demux->prog_num == pmt.program_num) {
                    if ((ecm_data.is_found == TRUE) && (ecm_found == FALSE)) {
                      gst_stm_ts_demux_register_stream (demux,
                          ecm_data.content, ecm_data.pid,
                          ecm_data.codec, FALSE);
                      ecm_found = TRUE;
                    }

                    aud_found++;
                    demux->prog_num = pmt.program_num;
                    gst_stm_ts_demux_register_stream (demux, content, pid,
                        codec, ecm_found);
                    /* Inside the gst_stm_ts_demux_register_stream() call the num_streams
                     * is incremented after update, so updaing num_streams-1 entry
                     */
                    demux->streams[demux->num_streams - 1]->tag_list =
                        pmt.element[j].tag_list;
                  }
                } else if ((content == STREAM_CONTENT_SUBTITLE)
                    && (sub_found == FALSE) && (scrambled == FALSE)) {
                  sub_found = TRUE;
                  demux->prog_num = pmt.program_num;
                  gst_stm_ts_demux_register_stream (demux, content, pid, codec,
                      FALSE);
                  /* Inside the gst_stm_ts_demux_register_stream() call the num_streams
                   * is incremented after update, so updaing num_streams-1 entry
                   */
                  demux->streams[demux->num_streams - 1]->tag_list =
                      pmt.element[j].tag_list;

                }

                if (demux->prog_num == pmt.program_num) {
                  demux->pcr_pid = pmt.pcr_pid;
                  /* Save the pmt informations required for mediaplayer */
                  memset (&stream, 0, sizeof (Gststm_ts_stream));
                  stream.pid = pid;
                  stream.codec = codec;
                  stream.content = content;
                  gst_stm_ts_demux_save_pmt_info (demux, &pmt, j, &stream);
                }
              }
              gst_stm_ts_demux_save_table_info (demux, &pat.element[i], content,
                  pid, codec, string_type, scrambled);
            }
          }
          if (demux->prog_num == pmt.program_num) {
            /* Now emit a signal to inform the arrival of pmt,
               since current playing program information is
               available in pmt-info */
            g_signal_emit_by_name (demux, "notify::pmt-info", NULL);
          }

          gst_stm_ts_demux_pmt_delete (&pmt);
        } else {
          if ((vid_found == TRUE) || (aud_found == TRUE)) {
            break;
          }
        }
      }
    }
    if (sub_found == FALSE) {
      gst_stm_ts_demux_register_stream (demux, STREAM_CONTENT_SUBTITLE,
          GST_STM_PID_NONE, META_ENCODING_CLOSECAPTION, FALSE);
    }
  } else {
    return FALSE;
  }

  if (section_buf) {
    gst_buffer_unref (section_buf);
  }

  gst_stm_ts_demux_sdt_delete (&sdt);
  gst_stm_ts_demux_pat_delete (&pat);

  if ((scrambled == TRUE) && (vid_found == FALSE) && (aud_found == FALSE)) {
    GST_WARNING_OBJECT (demux, "Scrambled stream detected but not supported");
    gst_element_post_message (GST_ELEMENT_CAST (demux),
        gst_message_new_element (GST_OBJECT (demux),
            gst_structure_new_empty ("scrambled")));
    return FALSE;
  }

  return TRUE;
}

gboolean
gst_stm_ts_demux_pat_acquire (Gststm_ts_demux * demux, guint16 pid,
    GstStm_ts_PATData_t * table_data, GstBuffer ** section_buf)
{
  guint8 *buf;
  guint32 buf_size = 0, read_size = 0, num_of_entries = 0;
  guint32 section_length = 0, i = 0, j = 0;
  GstStm_ts_TableData_t pat_table_data;
  guint32 element_id;

  /* Clear the structure */
  memset (table_data, 0, sizeof (GstStm_ts_PATData_t));

  /* Allocate buffer to receive the PAT section */
  buf_size = GST_STM_TS_DEMUX_TABLE_MAX_BUFFER_SIZE;
  buf = g_malloc0 (buf_size);
  if (buf == NULL) {
    GST_WARNING_OBJECT (demux, "failed to allocate pat table buffer");
    memset (table_data, 0, sizeof (GstStm_ts_PATData_t));
    return FALSE;
  }

  pat_table_data.pid = pid;
  pat_table_data.table_id = GST_STM_TS_PAT_TABLE_ID;
  pat_table_data.program_num = GST_STM_TS_DEMUX_FILTER_DONT_CARE;
  pat_table_data.section_num = GST_STM_TS_DEMUX_FILTER_AUTOMATIC;
  pat_table_data.cur_not_next = 1;
  pat_table_data.buf_data = buf;
  pat_table_data.buf_size = buf_size;
  pat_table_data.read_size = &read_size;

  /* Filter the complete section */
  if (gst_stm_ts_demux_xxx_acquire (demux, &pat_table_data,
          section_buf) == FALSE) {
    g_free (buf);
    memset (table_data, 0, sizeof (GstStm_ts_PATData_t));
    *section_buf = NULL;
    return FALSE;
  }

  /* Copy the raw PAT datas */
  table_data->buffer_size = read_size;
  table_data->buffer = g_malloc0 (read_size);
  if (table_data->buffer == NULL) {
    g_free (buf);
    memset (table_data, 0, sizeof (GstStm_ts_PATData_t));
    return FALSE;
  }
  memcpy (table_data->buffer, buf, table_data->buffer_size);

  /* Parse the complete buffer */

  /* Get the transport stream id */
  table_data->ts_id = ((buf[3] << 8) | (buf[4]));

  /* Get the number of entries */
  num_of_entries = i = 0;
  /* first byte is table id and other flags */
  /* table_id (8 bits) ! section_syntax_indicator (1 bit) ! 0 (1 bit)
     ! reserved (2 bits) */
  /* Section length is represented by next 12 bits */
  while ((i + 2) < read_size) {
    section_length = (((buf[i + 1] & 0xF) << 8) | (buf[i + 2])) + 3;
    num_of_entries += (section_length < 12) ? 0 : (section_length - 12) / 4;
    i += section_length;
  }

  /* If the PAT is empty, go out now */
  if (num_of_entries == 0) {
    g_free (buf);
    g_free (table_data->buffer);
    table_data->buffer = NULL;
    table_data->num_elements = 0;
    table_data->element = NULL;
    return FALSE;
  }

  /* Allocate the PAT elements */
  table_data->element =
      (GstStm_ts_PATElement_t *) g_malloc0 (num_of_entries *
      sizeof (GstStm_ts_PATElement_t));
  if (table_data->element == NULL) {
    g_free (table_data->buffer);
    table_data->buffer = NULL;
    g_free (buf);
    memset (table_data, 0, sizeof (GstStm_ts_PATData_t));
    return FALSE;
  }

  /* Fill up the elements */
  num_of_entries = i = 0;
  element_id = 0;
  /* Section length is represented by next two bytes */
  while ((i + 2) < read_size) {
    j = 8;                      /* 9 bytes not used in the PAT */
    section_length = (((buf[i + 1] & 0xF) << 8) | (buf[i + 2])) + 3;
    num_of_entries += (section_length < 12) ? 0 : (section_length - 12) / 4;
    while (element_id < num_of_entries) {
      table_data->element[element_id].program_num =
          (((buf[i + j] << 8)) | (buf[i + j + 1]));
      table_data->element[element_id].pid =
          (((buf[i + j + 2] & 0x1F) << 8) | (buf[i + j + 3]));
      element_id++;
      j += 4;
    }
    i += section_length;
  }
  table_data->num_elements = num_of_entries;

  /* Free the section buffer */
  g_free (buf);

  return TRUE;
}

gboolean
gst_stm_ts_demux_pat_delete (GstStm_ts_PATData_t * table_data)
{
  /* Check if something to delete */
  if (table_data == NULL) {
    return TRUE;
  }

  /* Free the memory */
  if (table_data->element != NULL) {
    g_free (table_data->element);
    table_data->element = NULL;
  }
  if (table_data->buffer != NULL) {
    g_free (table_data->buffer);
    table_data->buffer = NULL;
  }

  /* Reset the structure */
  table_data->num_elements = 0;

  return TRUE;
}

gboolean
gst_stm_ts_demux_pmt_acquire (Gststm_ts_demux * demux, guint16 pid,
    GstStm_ts_PMTData_t * table_data, GstBuffer ** section_buf)
{
  guint8 *buf;
  guint32 buf_size, read_size, num_of_entries;
  guint32 program_info_length, es_info_length, section_length, i, j, k;
  GstStm_ts_TableData_t pmt_table_data;

  /* Clear the structure by keeping ProgramNumber */
  i = table_data->program_num;
  memset (table_data, 0, sizeof (GstStm_ts_PMTData_t));
  table_data->program_num = i;

  /* Allocate buffer to receive the PMT section */
  buf_size = GST_STM_TS_DEMUX_TABLE_MAX_BUFFER_SIZE;
  buf = g_malloc0 (buf_size);
  if (buf == NULL) {
    memset (table_data, 0, sizeof (GstStm_ts_PMTData_t));
    return FALSE;
  }

  pmt_table_data.pid = pid;
  pmt_table_data.table_id = GST_STM_TS_PMT_TABLE_ID;
  pmt_table_data.program_num = table_data->program_num;
  pmt_table_data.section_num = GST_STM_TS_DEMUX_FILTER_AUTOMATIC;
  pmt_table_data.cur_not_next = 1;
  pmt_table_data.buf_data = buf;
  pmt_table_data.buf_size = buf_size;
  pmt_table_data.read_size = &read_size;

  /* Filter the complete section */
  if (gst_stm_ts_demux_xxx_acquire (demux, &pmt_table_data,
          section_buf) == FALSE) {
    g_free (buf);
    memset (table_data, 0, sizeof (GstStm_ts_PMTData_t));
    *section_buf = NULL;
    return FALSE;
  }

  /* Copy the raw PMT datas */
  table_data->buffer_size = read_size;
  table_data->buffer = g_malloc0 (read_size);
  if (table_data->buffer == NULL) {
    g_free (buf);
    memset (table_data, 0, sizeof (GstStm_ts_PMTData_t));
    return FALSE;
  }
  memcpy (table_data->buffer, buf, table_data->buffer_size);

  /* Parse the complete buffer */

  /* Get the program number */
  table_data->program_num = ((buf[3] << 8) | (buf[4]));

  /* Get the pcr pid */
  table_data->pcr_pid = (((buf[8] & 0x1F) << 8) | (buf[9]));

  /* Get the version number */
  table_data->version_number = ((buf[5] & 0x3E) >> 1);

  /* Get the number of elements */
  num_of_entries = i = 0;
  while (i < read_size) {
    section_length = (((buf[i + 1] & 0xF) << 8) | (buf[i + 2])) + 3;
    program_info_length = (((buf[i + 10] & 0xF) << 8) | (buf[i + 11]));
    j = program_info_length + 12;
    while (j < ((section_length < 4) ? 0 : (section_length - 4))) {
      es_info_length = (((buf[i + j + 3] & 0xF) << 8) | (buf[i + j + 4]));
      num_of_entries++;
      j += es_info_length + 5;
    }
    i += section_length;
  }

  /* If the PMT is empty, go out now */
  if (num_of_entries == 0) {
    table_data->num_elements = 0;
    table_data->element = NULL;
    g_free (table_data->buffer);
    table_data->buffer = NULL;
    g_free (buf);
    memset (table_data, 0, sizeof (GstStm_ts_PMTData_t));
    return TRUE;
  }

  /* Allocate the element memory */
  table_data->element =
      g_malloc0 (num_of_entries * sizeof (GstStm_ts_PMTElement_t));
  if (table_data->element == NULL) {
    g_free (table_data->buffer);
    table_data->buffer = NULL;
    g_free (buf);
    memset (table_data, 0, sizeof (GstStm_ts_PMTData_t));
    return FALSE;
  }

  /* Get the number of byte to allocate for general descriptors */
  num_of_entries = i = 0;
  while (i < read_size) {
    section_length = (((buf[i + 1] & 0xF) << 8) | (buf[i + 2])) + 3;
    program_info_length = (((buf[i + 10] & 0xF) << 8) | (buf[i + 11]));
    j = 0;
    while (j < program_info_length) {
      num_of_entries++;
      j += buf[i + j + 13] + 2;
    }
    i += section_length;
  }

  /* Allocate the general descriptor memory */
  if (num_of_entries == 0) {
    table_data->descriptor = NULL;
    table_data->num_descriptors = 0;
  } else {
    table_data->descriptor =
        (GstStm_ts_Descriptor_t *) g_malloc0 (num_of_entries *
        sizeof (GstStm_ts_Descriptor_t));
    if (table_data->descriptor == NULL) {
      g_free (table_data->element);
      g_free (table_data->buffer);
      table_data->element = NULL;
      table_data->buffer = NULL;
      g_free (buf);
      memset (table_data, 0, sizeof (GstStm_ts_PMTData_t));
      return FALSE;
    }
  }

  /* Fill up the general descriptors */
  if (table_data->descriptor != NULL) {
    table_data->num_descriptors = i = 0;
    while (i < read_size) {
      section_length = (((buf[i + 1] & 0xF) << 8) | (buf[i + 2])) + 3;
      program_info_length = (((buf[i + 10] & 0xF) << 8) | (buf[i + 11]));
      j = 0;
      while (j < program_info_length) {
        table_data->descriptor[table_data->num_descriptors].tag =
            buf[i + j + 12];
        table_data->descriptor[table_data->num_descriptors].size =
            buf[i + j + 13];
        table_data->descriptor[table_data->num_descriptors].data = NULL;
        if (table_data->descriptor[table_data->num_descriptors].size != 0) {
          table_data->descriptor[table_data->num_descriptors].data =
              g_malloc0 (table_data->descriptor[table_data->
                  num_descriptors].size);
          if (table_data->descriptor[table_data->num_descriptors].data == NULL) {
            gst_stm_ts_demux_pmt_delete (table_data);
            g_free (buf);
            return FALSE;
          }
          memcpy (table_data->descriptor[table_data->num_descriptors].data,
              &buf[i + j + 14],
              table_data->descriptor[table_data->num_descriptors].size);
        }
        j += table_data->descriptor[table_data->num_descriptors].size + 2;
        table_data->num_descriptors++;
      }
      i += section_length;
    }
  }

  /* Fill up the elements list */
  table_data->num_elements = i = 0;
  while (i < read_size) {
    section_length = (((buf[i + 1] & 0xF) << 8) | (buf[i + 2])) + 3;
    program_info_length = (((buf[i + 10] & 0xF) << 8) | (buf[i + 11]));
    j = program_info_length + 12;
    while (j < ((section_length < 4) ? 0 : (section_length - 4))) {
      table_data->element[table_data->num_elements].type = buf[i + j];
      table_data->element[table_data->num_elements].pid =
          (((buf[i + j + 1] & 0x1F) << 8) | (buf[i + j + 2]));
      table_data->element[table_data->num_elements].num_descriptors = 0;
      table_data->element[table_data->num_elements].descriptor = NULL;
      es_info_length = (((buf[i + j + 3] & 0xF) << 8) | (buf[i + j + 4]));
      /* Get the number of descriptors to allocate */
      for (k = 0, num_of_entries = 0; k < es_info_length;) {
        k = k + buf[i + j + 5 + k + 1] + 2;
        num_of_entries++;
      }
      /* If there are some descriptors */
      if (es_info_length != 0) {
        table_data->element[table_data->num_elements].descriptor =
            (GstStm_ts_Descriptor_t *) g_malloc0 (num_of_entries *
            sizeof (GstStm_ts_Descriptor_t));
        if (table_data->element[table_data->num_elements].descriptor == NULL) {
          gst_stm_ts_demux_pmt_delete (table_data);
          g_free (buf);
          return FALSE;
        }
        k = 0;
        while (k < es_info_length) {
          table_data->element[table_data->num_elements].
              descriptor[table_data->element[table_data->num_elements].
              num_descriptors].tag = buf[i + j + 5 + k];
          table_data->element[table_data->num_elements].
              descriptor[table_data->element[table_data->num_elements].
              num_descriptors].size = buf[i + j + 5 + k + 1];
          table_data->element[table_data->num_elements].
              descriptor[table_data->element[table_data->num_elements].
              num_descriptors].data = NULL;
          if (table_data->element[table_data->
                  num_elements].descriptor[table_data->element[table_data->
                      num_elements].num_descriptors].size != 0) {
            table_data->element[table_data->
                num_elements].descriptor[table_data->element[table_data->
                    num_elements].num_descriptors].data =
                g_malloc0 (table_data->element[table_data->
                    num_elements].descriptor[table_data->element[table_data->
                        num_elements].num_descriptors].size);
            if (table_data->element[table_data->
                    num_elements].descriptor[table_data->element[table_data->
                        num_elements].num_descriptors].data == NULL) {
              gst_stm_ts_demux_pmt_delete (table_data);
              g_free (buf);
              return FALSE;
            }
            memcpy (table_data->element[table_data->
                    num_elements].descriptor[table_data->element[table_data->
                        num_elements].num_descriptors].data,
                &buf[i + j + 5 + k + 2],
                table_data->element[table_data->
                    num_elements].descriptor[table_data->element[table_data->
                        num_elements].num_descriptors].size);
          }
          k = k + buf[i + j + 5 + k + 1] + 2;
          table_data->element[table_data->num_elements].num_descriptors++;
        }
      }
      table_data->num_elements++;
      j += es_info_length + 5;
    }
    i += section_length;
  }

  /* Free the section buffer */
  g_free (buf);

  return TRUE;
}

gboolean
gst_stm_ts_demux_pmt_delete (GstStm_ts_PMTData_t * table_data)
{
  guint i, j;

  /* Check if something to delete */
  if (table_data == NULL) {
    return TRUE;
  }

  /* Free the global descriptors */
  if (table_data->descriptor != NULL) {
    /* For each global descriptors */
    for (i = 0; i < table_data->num_descriptors; i++) {
      if (table_data->descriptor[i].data != NULL) {
        g_free (table_data->descriptor[i].data);
        table_data->descriptor[i].data = NULL;
      }
    }
    g_free (table_data->descriptor);
    table_data->num_descriptors = 0;
    table_data->descriptor = NULL;
  }

  /* Free the elements */
  if (table_data->element != NULL) {
    /* For each element */
    for (i = 0; i < table_data->num_elements; i++) {
      if (table_data->element[i].descriptor != NULL) {
        for (j = 0; j < table_data->element[i].num_descriptors; j++) {
          if (table_data->element[i].descriptor[j].data != NULL) {
            g_free (table_data->element[i].descriptor[j].data);
            table_data->element[i].descriptor[j].data = NULL;
          }
        }
        g_free (table_data->element[i].descriptor);
        table_data->element[i].descriptor = NULL;
      }
    }

    /* Clear the element structure */
    g_free (table_data->element);
    table_data->num_elements = 0;
    table_data->element = NULL;
  }

  /* Free the raw buffer */
  if (table_data->buffer != NULL) {
    g_free (table_data->buffer);
    table_data->buffer = NULL;
  }

  return TRUE;
}

gboolean
gst_stm_ts_demux_sdt_acquire (Gststm_ts_demux * demux, guint16 pid,
    GstStm_ts_SDTData_t * table_data, GstBuffer ** section_buf)
{
  guint8 *buf;
  guint32 buf_size, read_size, num_of_entries;
  guint32 descriptor_length, section_length, i, j, k;
  GstStm_ts_TableData_t sdt_table_data;

  /* Clear the structure */
  memset (table_data, 0, sizeof (GstStm_ts_SDTData_t));

  /* Allocate buffer to receive the SDT section */
  buf_size = GST_STM_TS_DEMUX_TABLE_MAX_BUFFER_SIZE;
  buf = g_malloc0 (buf_size);
  if (buf == NULL) {
    return FALSE;
  }

  sdt_table_data.pid = pid;
  sdt_table_data.table_id = GST_STM_TS_SDT_TABLE_ID;
  sdt_table_data.program_num = GST_STM_TS_DEMUX_FILTER_DONT_CARE;
  sdt_table_data.section_num = GST_STM_TS_DEMUX_FILTER_AUTOMATIC;
  sdt_table_data.cur_not_next = 1;
  sdt_table_data.buf_data = buf;
  sdt_table_data.buf_size = buf_size;
  sdt_table_data.read_size = &read_size;

  /* Filter the complete section */
  if (gst_stm_ts_demux_xxx_acquire (demux, &sdt_table_data,
          section_buf) == FALSE) {
    g_free (buf);
    *section_buf = NULL;
    return FALSE;
  }

  /* Parse the complete buffer */

  /* Get the transport stream id */
  table_data->transport_stream_id = ((buf[3] << 8) | (buf[4]));

  /* Get the network id */
  table_data->original_networkd_id = ((buf[8] << 8) | (buf[9]));

  /* Get the number of elements */
  num_of_entries = i = 0;
  while (i < read_size) {
    section_length = (((buf[i + 1] & 0xF) << 8) | (buf[i + 2])) + 3;
    j = 11;
    while (j < ((section_length < 4) ? 0 : (section_length - 4))) {
      descriptor_length = (((buf[i + j + 3] & 0xF) << 8) | (buf[i + j + 4]));
      num_of_entries++;
      j += descriptor_length + 5;
    }
    i += section_length;
  }

  /* If the SDT is empty, go out now */
  if (num_of_entries == 0) {
    table_data->num_elements = 0;
    table_data->element = NULL;
    g_free (buf);
    return TRUE;
  }

  /* Allocate the element memory */
  table_data->element =
      (GstStm_ts_SDTElement_t *) g_malloc0 (num_of_entries *
      sizeof (GstStm_ts_SDTElement_t));
  if (table_data->element == NULL) {
    g_free (buf);
    return FALSE;
  }

  /* Fill up the elements list */
  table_data->num_elements = i = 0;
  while (i < read_size) {
    section_length = (((buf[i + 1] & 0xF) << 8) | (buf[i + 2])) + 3;
    j = 11;
    while (j < ((section_length < 4) ? 0 : (section_length - 4))) {
      table_data->element[table_data->num_elements].service_id =
          ((buf[i + j] << 8) | (buf[i + j + 1]));
      table_data->element[table_data->num_elements].eit_schedule_flag =
          ((buf[i + j + 2] >> 1) & 0x01);
      table_data->element[table_data->num_elements].eit_present_following_flag =
          ((buf[i + j + 2]) & 0x01);
      table_data->element[table_data->num_elements].scrambled =
          ((buf[i + j + 3] >> 4) & 0x01);
      table_data->element[table_data->num_elements].num_descriptors = 0;
      descriptor_length = (((buf[i + j + 3] & 0xF) << 8) | (buf[i + j + 4]));
      /* Get the number of descriptors to allocate */
      for (k = 0, num_of_entries = 0; k < descriptor_length;) {
        k = k + buf[i + j + 5 + k + 1] + 2;
        num_of_entries++;
      }
      /* If there are some descriptors */
      if (descriptor_length != 0) {
        table_data->element[table_data->num_elements].descriptor =
            (GstStm_ts_Descriptor_t *) g_malloc0 (num_of_entries *
            sizeof (GstStm_ts_Descriptor_t));
        if (table_data->element[table_data->num_elements].descriptor == NULL) {
          gst_stm_ts_demux_sdt_delete (table_data);
          g_free (buf);
          return FALSE;
        }
        k = 0;
        while (k < descriptor_length) {
          table_data->element[table_data->num_elements].
              descriptor[table_data->element[table_data->num_elements].
              num_descriptors].tag = buf[i + j + 5 + k];
          table_data->element[table_data->num_elements].
              descriptor[table_data->element[table_data->num_elements].
              num_descriptors].size = buf[i + j + 5 + k + 1];
          table_data->element[table_data->num_elements].
              descriptor[table_data->element[table_data->num_elements].
              num_descriptors].data = NULL;
          if (table_data->element[table_data->
                  num_elements].descriptor[table_data->element[table_data->
                      num_elements].num_descriptors].size != 0) {
            table_data->element[table_data->
                num_elements].descriptor[table_data->element[table_data->
                    num_elements].num_descriptors].data =
                (guint8 *) g_malloc0 (table_data->
                element[table_data->num_elements].descriptor[table_data->
                    element[table_data->num_elements].num_descriptors].size);
            if (table_data->element[table_data->
                    num_elements].descriptor[table_data->element[table_data->
                        num_elements].num_descriptors].data == NULL) {
              gst_stm_ts_demux_sdt_delete (table_data);
              g_free (buf);
              return FALSE;
            }
            memcpy (table_data->element[table_data->
                    num_elements].descriptor[table_data->element[table_data->
                        num_elements].num_descriptors].data,
                &buf[i + j + 5 + k + 2],
                table_data->element[table_data->
                    num_elements].descriptor[table_data->element[table_data->
                        num_elements].num_descriptors].size);
          }
          k = k + buf[i + j + 5 + k + 1] + 2;
          table_data->element[table_data->num_elements].num_descriptors++;
        }
      }
      table_data->num_elements++;
      j += descriptor_length + 5;
    }
    i += section_length;
  }

  /* Free the section buffer */
  g_free (buf);

  return TRUE;
}

gboolean
gst_stm_ts_demux_sdt_delete (GstStm_ts_SDTData_t * table_data)
{
  guint32 i, j;

  /* Check if something to delete */
  if (table_data == NULL) {
    return TRUE;
  }

  /* Free the elements */
  if (table_data->element != NULL) {
    /* For each element */
    for (i = 0; i < table_data->num_elements; i++) {
      if (table_data->element[i].descriptor != NULL) {
        for (j = 0; j < table_data->element[i].num_descriptors; j++) {
          if (table_data->element[i].descriptor[j].data != NULL) {
            g_free (table_data->element[i].descriptor[j].data);
            table_data->element[i].descriptor[j].data = NULL;
          }
        }
        g_free (table_data->element[i].descriptor);
        table_data->element[i].descriptor = NULL;
      }
    }

    /* Clear the element structure */
    g_free (table_data->element);
    table_data->num_elements = 0;
    table_data->element = NULL;
  }

  return TRUE;
}

void
gst_stm_update_tag_list (GstStm_ts_PMTElement_t * pmt_element,
    guint descriptor_index)
{
  gchar lang_code[4];
  const gchar *lc = NULL;
  lang_code[0] = pmt_element->descriptor[descriptor_index].data[0];
  lang_code[1] = pmt_element->descriptor[descriptor_index].data[1];
  lang_code[2] = pmt_element->descriptor[descriptor_index].data[2];
  lang_code[3] = 0;
  lc = gst_tag_get_language_code (lang_code);
  gst_tag_list_add (pmt_element->tag_list, GST_TAG_MERGE_REPLACE,
      GST_TAG_LANGUAGE_CODE, (lc) ? lc : lang_code, NULL);
}

gboolean
gst_stm_ts_demux_pmt_get_info (GstStm_ts_PMTElement_t * pmt_element,
    guint * codec, guint16 * pid, stream_content_t * content,
    gchar * string_type, GstStm_ts_ECMData_t * ecm_data)
{
  guint i;

  /* Check parameters */
  if ((pmt_element == NULL) || (codec == NULL) || (content == NULL)
      || (pid == NULL) || (string_type == NULL)) {
    return FALSE;
  }

  /* Reset fields */
  *codec = VIDEO_ENCODING_NONE;
  *content = STREAM_CONTENT_OTHER;
  *pid = 0;
  g_strlcpy (string_type, "NONE", STRING_TYPE_MAX_SIZE);

  /* Analysis of each pmt element */
  switch (pmt_element->type) {
    case GST_STM_TS_DEMUX_STREAM_VIDEO_MPEG1:
      *codec = VIDEO_ENCODING_MPEG1;
      *content = STREAM_CONTENT_VIDEO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "MP1V", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_VIDEO_MPEG2:
      *codec = VIDEO_ENCODING_MPEG2;
      *content = STREAM_CONTENT_VIDEO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "MP2V", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_VIDEO_MPEG4:
      *codec = VIDEO_ENCODING_H264;
      *content = STREAM_CONTENT_VIDEO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "MP4V", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_VIDEO_HEVC:
      *codec = VIDEO_ENCODING_HEVC;
      *content = STREAM_CONTENT_VIDEO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "HEVC", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_VIDEO_VC1:
      *codec = VIDEO_ENCODING_VC1;
      *content = STREAM_CONTENT_VIDEO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "VC1", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_VIDEO_AVS:
      *codec = VIDEO_ENCODING_AVS;
      *content = STREAM_CONTENT_VIDEO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "AVS", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_VIDEO_AVSP:
      *codec = VIDEO_ENCODING_AVSP;
      *content = STREAM_CONTENT_VIDEO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "AVSP", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_MPEG1:
      *codec = AUDIO_ENCODING_MPEG1;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "MP1A", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_MPEG2:
      *codec = AUDIO_ENCODING_MPEG2;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "MP2A", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_WMA:
      //*codec   = PLAYREC_STREAMTYPE_WMA;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "WMA", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_AAC_ADTS:
      *codec = AUDIO_ENCODING_AAC;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "AAC", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_AAC_LATM:
      *codec = AUDIO_ENCODING_AAC;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "HEAAC", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_AAC_RAW1:
      *codec = AUDIO_ENCODING_AAC;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "HEAAC", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_AAC_RAW2:
      *codec = AUDIO_ENCODING_AAC;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "HEAAC", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_LPCM:
      *codec = AUDIO_ENCODING_LPCM;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "LPCM", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_AC3:
      *codec = AUDIO_ENCODING_AC3;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "AC3", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_DTS:
      *codec = AUDIO_ENCODING_DTS;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "DTS", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_MLP:
      *codec = AUDIO_ENCODING_MLP;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "MLP", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_DTSHD:
    case GST_STM_TS_DEMUX_STREAM_AUDIO_DTSHD_XLL:
    case GST_STM_TS_DEMUX_STREAM_AUDIO_DTSHD_2:
      *codec = AUDIO_ENCODING_DTS;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "DTS", STRING_TYPE_MAX_SIZE);
      break;
    case GST_STM_TS_DEMUX_STREAM_AUDIO_EAC3:
    case GST_STM_TS_DEMUX_STREAM_AUDIO_DDPLUS:
    case GST_STM_TS_DEMUX_STREAM_AUDIO_DDPLUS_2:
      *codec = AUDIO_ENCODING_AC3;
      *content = STREAM_CONTENT_AUDIO;
      *pid = pmt_element->pid;
      g_strlcpy (string_type, "DDPLUS", STRING_TYPE_MAX_SIZE);
      break;
    default:
      break;
  }

  /* Look for sub descriptors */
  for (i = 0; i < pmt_element->num_descriptors; i++) {
    switch (pmt_element->descriptor[i].tag) {
      case GST_STM_TS_DEMUX_DESCRIPTOR_REGISTRATION:
      {
        guint8 registration_string[16];
        gst_stm_ts_demux_descriptor_registration (&(pmt_element->descriptor[i]),
            registration_string);
        if (strcmp ((gchar *) registration_string, "HEVC") == 0) {
          *codec = VIDEO_ENCODING_HEVC;
          *content = STREAM_CONTENT_VIDEO;
          *pid = pmt_element->pid;
          g_strlcpy (string_type, "HEVC", STRING_TYPE_MAX_SIZE);
        }
        if (strcmp ((gchar *) registration_string, "VC-1") == 0) {
          *codec = VIDEO_ENCODING_VC1;
          *content = STREAM_CONTENT_VIDEO;
          *pid = pmt_element->pid;
          g_strlcpy (string_type, "VC1", STRING_TYPE_MAX_SIZE);
        }
        if (strcmp ((gchar *) registration_string, "WMA9") == 0) {
          //*codec   = PLAYREC_STREAMTYPE_WMA;
          *content = STREAM_CONTENT_AUDIO;
          *pid = pmt_element->pid;
          g_strlcpy (string_type, "WMA", STRING_TYPE_MAX_SIZE);
        }
        if (strcmp ((gchar *) registration_string, "AC-3") == 0) {
          if (*codec != AUDIO_ENCODING_MLP) {
            *codec = AUDIO_ENCODING_AC3;
            *content = STREAM_CONTENT_AUDIO;
            *pid = pmt_element->pid;
            g_strlcpy (string_type, "AC3", STRING_TYPE_MAX_SIZE);
          }
        }
        if (strcmp ((gchar *) registration_string, "DTS1") == 0) {
          *codec = AUDIO_ENCODING_DTS;
          *content = STREAM_CONTENT_AUDIO;
          *pid = pmt_element->pid;
          g_strlcpy (string_type, "DTS", STRING_TYPE_MAX_SIZE);
        }
        if (strcmp ((gchar *) registration_string, "DTS2") == 0) {
          *codec = AUDIO_ENCODING_DTS;
          *content = STREAM_CONTENT_AUDIO;
          *pid = pmt_element->pid;
          g_strlcpy (string_type, "DTS", STRING_TYPE_MAX_SIZE);
        }
        if (strcmp ((char *) registration_string, "DRA1") == 0) {
          *codec = AUDIO_ENCODING_DRA;
          *content = STREAM_CONTENT_AUDIO;
          *pid = pmt_element->pid;
          g_strlcpy (string_type, "DRA", STRING_TYPE_MAX_SIZE);
        }
      }
        break;
      case GST_STM_TS_DEMUX_DESCRIPTOR_MPEG4_VIDEO:
      {
        *codec = VIDEO_ENCODING_H264;
        *content = STREAM_CONTENT_VIDEO;
        *pid = pmt_element->pid;
        g_strlcpy (string_type, "H264", STRING_TYPE_MAX_SIZE);
      }
        if (pmt_element->descriptor[i].tag ==
            GST_STM_TS_DEMUX_DESCRIPTOR_MPEG4_AUDIO) {
          /* /!\ If stream type is telling us it's ADTS or LATM AAC type, the information is clear, no need to parse descriptors /!\ */
          if ((pmt_element->type != GST_STM_TS_DEMUX_STREAM_AUDIO_AAC_ADTS)
              && (pmt_element->type != GST_STM_TS_DEMUX_STREAM_AUDIO_AAC_LATM)) {
            //*codec   = PLAYREC_STREAMTYPE_MP4A;
            *content = STREAM_CONTENT_AUDIO;
            *pid = pmt_element->pid;
            g_strlcpy (string_type, "MP4A", STRING_TYPE_MAX_SIZE);
          }
        }
        break;
      case GST_STM_TS_DEMUX_DESCRIPTOR_AAC:
      {
        /* /!\ If stream type is telling us it's ADTS or LATM AAC type, the information is clear, no need to parse descriptors /!\ */
        if ((pmt_element->type != GST_STM_TS_DEMUX_STREAM_AUDIO_AAC_ADTS)
            && (pmt_element->type != GST_STM_TS_DEMUX_STREAM_AUDIO_AAC_LATM)) {
          *codec = AUDIO_ENCODING_AAC;
          *content = STREAM_CONTENT_AUDIO;
          *pid = pmt_element->pid;
          g_strlcpy (string_type, "AAC", STRING_TYPE_MAX_SIZE);
        }
      }
        break;
      case GST_STM_TS_DEMUX_DESCRIPTOR_AC3:
      {
        *codec = AUDIO_ENCODING_AC3;
        *content = STREAM_CONTENT_AUDIO;
        *pid = pmt_element->pid;
        g_strlcpy (string_type, "AC3", STRING_TYPE_MAX_SIZE);
      }
        break;
      case GST_STM_TS_DEMUX_DESCRIPTOR_ENHANCED_AC3:
      {
        *codec = AUDIO_ENCODING_AC3;
        *content = STREAM_CONTENT_AUDIO;
        *pid = pmt_element->pid;
        g_strlcpy (string_type, "DDPLUS", STRING_TYPE_MAX_SIZE);
      }
        break;
      case GST_STM_TS_DEMUX_DESCRIPTOR_DTS:
      {
        *codec = AUDIO_ENCODING_DTS;
        *content = STREAM_CONTENT_AUDIO;
        *pid = pmt_element->pid;
        g_strlcpy (string_type, "DTS", STRING_TYPE_MAX_SIZE);
      }
        break;
      case GST_STM_TS_DEMUX_DESCRIPTOR_TELETEXT:
      {
        *codec = META_ENCODING_TELETEXT;
        *content = STREAM_CONTENT_SUBTITLE;
        *pid = pmt_element->pid;
        g_strlcpy (string_type, "TTXT", STRING_TYPE_MAX_SIZE);
      }
        break;
      case GST_STM_TS_DEMUX_DESCRIPTOR_SUBTITLING:
      {
        *codec = META_ENCODING_SUBTITLE;
        *content = STREAM_CONTENT_SUBTITLE;
        *pid = pmt_element->pid;
        g_strlcpy (string_type, "SUBT", STRING_TYPE_MAX_SIZE);
      }
        break;
      case GST_STM_TS_DEMUX_DESCRIPTOR_LANGUAGE_NAME:
      {
        gst_stm_update_tag_list (pmt_element, i);
        if (pmt_element->descriptor[i].data[3] == 3) {
          /* This is audio descriptor stream */
        }
      }
        break;
      case GST_STM_TS_DEMUX_DESCRIPTOR_CA:
      {
        if (((pmt_element->descriptor[i].data[0]) == (CA_SYSTEM_ID >> 8)) &&
            ((pmt_element->descriptor[i].data[1]) == (CA_SYSTEM_ID & 0xFF))) {
          ecm_data->is_found = TRUE;
          ecm_data->stream_type = GST_STM_TS_DEMUX_STREAM_ECM;
          ecm_data->content = STREAM_CONTENT_ECM;
          ecm_data->pid =
              (pmt_element->descriptor[i].
              data[2] << 8) | pmt_element->descriptor[i].data[3];
        }
      }
        break;
      default:
        break;
    }
  }

  return TRUE;
}

gboolean
gst_stm_ts_demux_descriptor_registration (GstStm_ts_Descriptor_t * descriptor,
    guint8 * format_id)
{
  /* Check parameters */
  if ((descriptor == NULL) || (format_id == NULL)) {
    return FALSE;
  }
  format_id[0] = 0;

  /* If descriptor is not correct */
  if ((descriptor->tag != GST_STM_TS_DEMUX_DESCRIPTOR_REGISTRATION)
      || (descriptor->size == 0)) {
    return FALSE;
  }

  /* Get the descriptor details */
  format_id[0] = (descriptor->data[0]);
  format_id[1] = (descriptor->data[1]);
  format_id[2] = (descriptor->data[2]);
  format_id[3] = (descriptor->data[3]);
  format_id[4] = 0;

  return FALSE;
}

gboolean
gst_stm_ts_demux_xxx_acquire (Gststm_ts_demux * demux,
    GstStm_ts_TableData_t * table_data, GstBuffer ** section_buf)
{
  GstBuffer *ts_buf = NULL;
  GstStm_ts_SectionData_t section_data;
  section_data.ts_data = NULL;
  section_data.ts_size = 0;
  section_data.ts_section_found = FALSE;
  section_data.section_num_to_filter = 0;
  section_data.num_bytes_to_copy_by_section = 0;
  section_data.section_syntax_indicator = 0;
  section_data.pusi_to_filter = 0;
  guint32 packet_size = 0;
  guint64 read_offset = 0;
  guint32 start_time_in_ms = 0;
  guint32 cur_time_in_ms = 0;
  guint32 ts_num_bytes_read = 0;
  GstMapInfo info_read;

  /* Set variables */
  *table_data->read_size = 0;
  packet_size = demux->packetsize;
  section_data.section_num_to_filter =
      (table_data->section_num == GST_STM_TS_DEMUX_FILTER_AUTOMATIC) ? 0 :
      table_data->section_num;
  section_data.pusi_to_filter = 1;

  /* Now, we loop until we reach the end of the file or find the complete section needed */
  section_data.ts_section_found = FALSE;
  ts_num_bytes_read = 0;
  read_offset = demux->streamoffset;

  start_time_in_ms = gst_stm_ts_demux_get_time_in_ms ();

  /* Look for section in the first 23.5Mbytes of datas (packet_size*128*1024) */
  while ((section_data.ts_section_found == FALSE)
      && (ts_num_bytes_read <= (packet_size * 1024 * 1024))) {
    if (table_data->table_id == GST_STM_TS_SDT_TABLE_ID) {
      cur_time_in_ms = gst_stm_ts_demux_get_time_in_ms ();
      if ((cur_time_in_ms - start_time_in_ms) > 1000) {
        GST_WARNING_OBJECT (demux, "failed to find SDT in 1s");
        return FALSE;
      }
    }
    if (*section_buf == NULL) {
      /* Read ts packets */
      ts_buf = NULL;
      if (gst_stm_ts_demux_read (demux, &ts_buf, read_offset,
              (packet_size * 32), TRUE) == FALSE) {
        return FALSE;
      }
      gst_buffer_map (ts_buf, &info_read, GST_MAP_READ);
      section_data.ts_data = info_read.data;
      section_data.ts_size = info_read.size;
      section_data.ts_size =
          ((section_data.ts_size / packet_size) * packet_size);

      if (section_data.ts_size == 0) {
        GST_WARNING_OBJECT (demux, "ts size is 0");
        gst_buffer_unmap (ts_buf, &info_read);
        gst_buffer_unref (ts_buf);
        break;
      }
      ts_num_bytes_read += section_data.ts_size;
      read_offset += section_data.ts_size;

      if ((gst_stm_ts_demux_section_parse (demux, table_data,
                  &section_data) == TRUE)
          && (table_data->table_id == GST_STM_TS_PAT_TABLE_ID)) {
        *section_buf = ts_buf;
        gst_buffer_unmap (ts_buf, &info_read);
      } else {
        gst_buffer_unmap (ts_buf, &info_read);
        gst_buffer_unref (ts_buf);
        *section_buf = NULL;
      }
    } else {
      ts_buf = *section_buf;
      gst_buffer_map (ts_buf, &info_read, GST_MAP_READ);
      section_data.ts_data = info_read.data;
      section_data.ts_size = info_read.size;
      section_data.ts_size =
          ((section_data.ts_size / packet_size) * packet_size);

      gst_stm_ts_demux_section_parse (demux, table_data, &section_data);
      gst_buffer_unmap (ts_buf, &info_read);
      gst_buffer_unref (ts_buf);
      *section_buf = NULL;
    }
  }

  if (section_data.ts_section_found == TRUE) {
    return TRUE;
  } else {
    GST_WARNING_OBJECT (demux, "failed to find section");
    if (*section_buf) {
      gst_buffer_unref (*section_buf);
      *section_buf = NULL;
    }
    return FALSE;
  }
}

gboolean
gst_stm_ts_demux_section_parse (Gststm_ts_demux * demux,
    GstStm_ts_TableData_t * table_data, GstStm_ts_SectionData_t * section_data)
{
  guint32 i = 0;
  guint32 j = 0;
  guint8 sync_byte = 0;
  guint8 transport_error_indicator = 0;
  guint8 pusi = 0;
  guint16 pid_read = 0;
  guint8 adaptation_field_control = 0;
  guint16 adaptation_field_length = 0;
  guint16 offset = 0;
  guint8 table_id_read = 0;
  guint16 section_length = 0;
  guint16 program_num_read = 0;
  guint8 current_next_indicator = 0;
  guint8 section_num_read = 0;
  guint8 last_section_num = 0;
  guint32 size_to_copy = 0;
  guint32 packet_size = 0;
  guint32 packet_header_size = 0;

  packet_size = demux->packetsize;

  /* Parse the packets */
  for (i = demux->packetoffset; ((i < section_data->ts_size)
          && (section_data->ts_section_found == FALSE));) {
    j = i + packet_header_size;
    sync_byte = section_data->ts_data[j];
    transport_error_indicator = (section_data->ts_data[j + 1] >> 7) & 0x1;
    pusi = (section_data->ts_data[j + 1] >> 6) & 0x1;
    pid_read =
        ((section_data->ts_data[j +
                1] & 0x1F) << 8) | (section_data->ts_data[j + 2]);
    adaptation_field_control = (section_data->ts_data[j + 3] >> 4) & 0x3;
    adaptation_field_length = 0;

    /* If we detect a sync byte */
    if (sync_byte == SYNC_BYTE) {
      /* If there is no error inside the packet */
      if (transport_error_indicator == 0) {
        /* If the pid matchs and the packet contain a payload */
        if ((pid_read == table_data->pid) && (adaptation_field_control != 0)
            && (adaptation_field_control != 2)) {
          /* If there is an adapation field, we need to bypass it */
          if ((adaptation_field_control == 2)
              || (adaptation_field_control == 3)) {
            adaptation_field_length = (section_data->ts_data[j + 4]) + 1;
          }
          /* Offset to point to the beginning of the payload */
          offset = adaptation_field_length + 4;
          /* Pointer field incremented when PUSI is there */
          if (pusi == 1) {
            /* If we were waiting for a real pusi indicator we want to filter */
            if (section_data->pusi_to_filter == 1) {
              offset += section_data->ts_data[j + offset] + 1;
            } else {
              /* Just bypass pointer field increment, as we are just dumping the section now */
              offset++;
            }
          }
          /* This while is done here to parse several sections inside a single packet */
          while (offset <= ((packet_size - packet_header_size) - 8)) {
            /* Section not yet matched, look for it */
            if ((section_data->num_bytes_to_copy_by_section == 0)
                && (pusi == 1)) {
              /* We now process the section */
              table_id_read = section_data->ts_data[j + offset];
              section_data->section_syntax_indicator =
                  (section_data->ts_data[j + offset + 1] >> 7) & 0x1;
              section_length =
                  ((section_data->ts_data[j + offset +
                          1] & 0xF) << 8) | (section_data->ts_data[j + offset +
                      2]);
              /* If there is the syntax indicator for PSI */
              if (section_data->section_syntax_indicator == 1) {
                program_num_read =
                    ((section_data->ts_data[j + offset + 3]) << 8) |
                    (section_data->ts_data[j + offset + 4]);
                //version_num            = (ts_data[j + offset + 5] >> 1) & 0x1F;
                current_next_indicator =
                    section_data->ts_data[j + offset + 5] & 0x1;
                section_num_read = section_data->ts_data[j + offset + 6];
                last_section_num = section_data->ts_data[j + offset + 7];
                /* If table id matchs */
                if ((table_data->table_id == table_id_read)
                    || (table_data->table_id ==
                        GST_STM_TS_DEMUX_FILTER_DONT_CARE)) {
                  /* If this is the current psi applied */
                  if ((table_data->cur_not_next ==
                          GST_STM_TS_DEMUX_FILTER_DONT_CARE)
                      || ((table_data->cur_not_next !=
                              GST_STM_TS_DEMUX_FILTER_DONT_CARE)
                          && (current_next_indicator ==
                              table_data->cur_not_next))) {
                    /* If we filter the program number, match it ? */
                    if ((table_data->program_num ==
                            GST_STM_TS_DEMUX_FILTER_DONT_CARE)
                        || ((table_data->program_num !=
                                GST_STM_TS_DEMUX_FILTER_DONT_CARE)
                            && (table_data->program_num == program_num_read))) {
                      /* Look for the correct section number */
                      if ((table_data->section_num ==
                              GST_STM_TS_DEMUX_FILTER_DONT_CARE)
                          || ((table_data->section_num !=
                                  GST_STM_TS_DEMUX_FILTER_DONT_CARE)
                              && (section_data->section_num_to_filter ==
                                  section_num_read))) {
                        section_data->num_bytes_to_copy_by_section =
                            section_length + 3;
                        section_data->pusi_to_filter = 0;
                      }
                    }
                  }
                }
              }
              /* Else it's private section */
              else {
                /* If table id matchs */
                if ((table_data->table_id == table_id_read)
                    || (table_data->table_id ==
                        GST_STM_TS_DEMUX_FILTER_DONT_CARE)) {
                  section_data->num_bytes_to_copy_by_section =
                      section_length + 3;
                  section_data->pusi_to_filter = 0;
                }
              }
            }
            /* Section reception in progress, just copy packet */
            if (section_data->num_bytes_to_copy_by_section != 0) {
              size_to_copy =
                  ((packet_size - packet_header_size - offset) >
                  section_data->num_bytes_to_copy_by_section) ?
                  section_data->num_bytes_to_copy_by_section :
                  (packet_size - packet_header_size - offset);
              /* If not enough space in the buffer, go out */
              if ((*table_data->read_size + size_to_copy) >
                  table_data->buf_size) {
                GST_WARNING_OBJECT (demux, "read buffer is too small");
                break;
              }
              /* Copy the datas */
              memcpy (table_data->buf_data + *table_data->read_size,
                  &section_data->ts_data[j + offset], size_to_copy);
              *table_data->read_size += size_to_copy;
              section_data->num_bytes_to_copy_by_section -= size_to_copy;
              /* If we reach the end of the section */
              if (section_data->section_syntax_indicator == 0) {
                section_data->ts_section_found = TRUE;
                break;
              }
              if (section_data->num_bytes_to_copy_by_section == 0) {
                if (((table_data->section_num ==
                            GST_STM_TS_DEMUX_FILTER_AUTOMATIC)
                        && (last_section_num == section_num_read))
                    || (table_data->section_num ==
                        GST_STM_TS_DEMUX_FILTER_DONT_CARE)) {
                  section_data->ts_section_found = TRUE;
                  break;
                } else {
                  section_data->pusi_to_filter = 1;
                  section_data->section_num_to_filter++;
                }
              }
            }
            /* Manage offset increment, offset is the current position in the current ts packet */
            if ((section_data->num_bytes_to_copy_by_section == 0)
                && (section_data->section_syntax_indicator == 1)) {
              /* Here, we have found nothing, so try next section in the same ts packet */
              offset += section_length + 3;
            } else {
              /* Here, two cases :                                                    */
              /* 1: The section parsed is not a valid section, try next ts packet now */
              /* 2: Current section reception in progressed, need next ts packet      */
              offset = packet_size - packet_header_size;
            }
          }
        }
      }
      i += packet_size;
    } else {
      i++;                      /* to scan the sync byte */
    }
  }

  if (section_data->ts_section_found == TRUE) {
    return TRUE;
  } else {
    GST_WARNING_OBJECT (demux, "failed to find section");
    return FALSE;
  }
}

void
gst_stm_ts_demux_save_table_info (Gststm_ts_demux * demux,
    GstStm_ts_PATElement_t * pat_element, stream_content_t content, guint16 pid,
    guint codec, gchar * string_type, gboolean scrambled)
{
  if (demux->table) {
    if ((pat_element) && (string_type)) {
      sprintf (demux->table + demux->table_ptr, "%d:%s:%d:%d:%d:%d/",
          pat_element->program_num, string_type, content, pid, codec,
          scrambled);
      demux->table_ptr = strlen (demux->table);
    }
  }
}

/*
 function: gst_stm_ts_demux_save_pmt_info
 input params:
           demux - stts_demux context
           pmt_data - pmt data struct
           stream_index - index of stream in pmt
           stream - stream information (pid/codec/content)
 description : this function will add pmt info
 return : void.
 */
void
gst_stm_ts_demux_save_pmt_info (Gststm_ts_demux * demux,
    GstStm_ts_PMTData_t * pmt_data, gint stream_index,
    Gststm_ts_stream * stream)
{

  GstStmTsPmtInfo *info = demux->pmt_info;
  GstStmTsPmtStreamInfo *streaminfo = NULL;
  GstStm_ts_PMTElement_t *element = NULL;
  guint index;
  guint8 desc_size;
  gchar *language = NULL;
  gchar *data = NULL;
  guint16 pid = stream->pid;
  guint8 codec = stream->codec;
  stream_content_t content = stream->content;

  g_return_if_fail (pmt_data != NULL);

  /* get the current program */
  element = &pmt_data->element[stream_index];

  if ((info == NULL) || (element == NULL)) {
    /* pmt_info not allocated or invalid element */
    GST_ERROR_OBJECT (demux, "info = %p, element = %p ", info, element);
    return;
  }

  if (info->program_no != pmt_data->program_num) {
    /* pmt_info not updated for this program number */
    /* Save the pmt informations and obtain the handle */
    gst_stm_pmt_info_add (info, pmt_data->program_num, pmt_data->pcr_pid,
        pmt_data->version_number);

    for (index = 0; index < pmt_data->num_descriptors; index++) {
      /* Save the general descriptors */
      gst_stm_pmt_info_add_descriptor (info,
          (gchar *) pmt_data->descriptor[index].data,
          pmt_data->descriptor[index].size);
    }
  }

  /* update stream information */
  streaminfo = gst_stm_pmt_stream_info_new (pid, codec, content);

  if (streaminfo == NULL) {
    /* streaminfo could not be allocated */
    GST_ERROR_OBJECT (demux, "streaminfo not allocated");
    return;
  }
  /* Save the element descriptors */
  for (index = 0; index < element->num_descriptors; index++) {
    if (element->descriptor[index].tag ==
        GST_STM_TS_DEMUX_DESCRIPTOR_LANGUAGE_NAME) {
      language = (gchar *) element->descriptor[index].data;
      /* Add the language to the stream info table. */
      gst_stm_pmt_stream_info_add_language (streaminfo, g_strndup (language,
              3));
    }

    data = (gchar *) element->descriptor[index].data;
    desc_size = element->descriptor[index].size;
    gst_stm_pmt_stream_info_add_descriptor (streaminfo, data, desc_size);
  }
  /* Add the streams to the array */
  gst_stm_pmt_info_add_stream (info, streaminfo);

  return;
}
