/* Gstreamer ST TS Demuxer Plugin
 *
 * Copyright (c) 2005 Gergely Nagy  <algernon@bonehunter.rulez.org>
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:element-stm_ts_demux
 *
 * Handles demuxing of ts packets using HW demux.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! stm_ts_demux ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <errno.h>
#include <poll.h>
#include <stdio.h>

#include <linux/dvb/stm_ioctls.h>
#include <linux/dvb/stm_dmx.h>

#include "gststm-tsdemux.h"
#include "gststm-tsdemux-utils.h"
#include "gststm-file.h"
#include <linux/dvb/stm_dmx.h>
#include "gststm-tspmtinfo.h"
#include "gststm-tsstreaminfo.h"
#include "gststm-tsdemux-table.h"



GST_DEBUG_CATEGORY_STATIC (gst_stm_ts_demux_debug);
#define GST_CAT_DEFAULT gst_stm_ts_demux_debug

#define CLOCK_BASE 9LL
#define MPEGTIME_TO_GSTTIME(time) (gst_util_uint64_scale ((time), \
            GST_MSECOND/10, CLOCK_BASE))
#define GST_STM_PES_PTS_SCAN_SIZE 14
#define GST_STM_PES_HEADER_SIZE 6
#define GST_STM_PTS_VARIANCE 5  /* Variance allowed in PTS comparison */

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

#define OFFSET_PIDH 1
#define OFFSET_PIDL 2

#define STM_MARKER_TS_SIZE 188
static guint8 static_ts_marker[STM_MARKER_TS_SIZE] = {
/*                |<--- adaptation        S     T     M           F     a */
  0x47, 0x40, 0x00, 0x30, 0x9D, 0x82, 0x10, 0x53, 0x54, 0x4D, 0x20, 0x46, 0x61,
/* k  e     T     S     P     a     c     k     e     t     <-stuffing bytes */
  0x6B, 0x65, 0x54, 0x53, 0x50, 0x61, 0x63, 0x6B, 0x65, 0x74, 0xFF, 0xFF, 0xFF,
/*---------------------------------------------------------------------------*/
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
/*---------------------------------------------------------------------------*/
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
/*---------------------------------------------------------------------------*/
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
/*---------------------------------------------------------------------------*/
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
/*---------------------------------------------------------------------------*/
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
/*---------------------------------------------------------------------------*/
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
/*---------------------------------------------------------------------------*/
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
/*---------------------------------------------------------------------------*/
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
/*---------------------------------------------------------------------------*/
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
/*---------------------------------------------------------------------------*/
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
/*------------------------------->|<---PES packets --------------------------*/
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x01, 0xFB, 0x00, 0x14, 0x80,
/*---------------------------------------------------------------------------*/
  0x01, 0x11, 0x80, 0x53, 0x54, 0x4D, 0x4D, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
/*-------------------------------->*/
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

#define MIN_DEV_ID      0
#define MAX_DEV_ID      16
#define DEFAULT_DEV_ID  0

enum
{
  PROP_0,
  PROP_ES_PIDS,
  PROP_TS_TABLE,
  PROP_ENABLE_PLAYBACK,
  PROP_PMT_INFO,
  PROP_PROGRAM_NUMBER,
  PROP_DEMUX_DEV_ID,
  PROP_LAST
};

#define GST_TSDEMUX_MUTEX_LOCK(x) G_STMT_START {                     \
    GST_CAT_DEBUG (gst_stm_ts_demux_debug,                            \
      "locking mutex %p from thread %p", x,                          \
      g_thread_self ());                                             \
  g_rec_mutex_lock (x);                                       \
  GST_CAT_DEBUG (gst_stm_ts_demux_debug,                              \
      "locked mutex %p from thread %p", x,                           \
      g_thread_self ());                                             \
} G_STMT_END

#define GST_TSDEMUX_MUTEX_UNLOCK(x) G_STMT_START {                   \
    GST_CAT_DEBUG (gst_stm_ts_demux_debug,                            \
      "unlocking mutex %p from thread %p", x,                        \
      g_thread_self ());                                             \
  g_rec_mutex_unlock (x);                                     \
} G_STMT_END

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_pad_template = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/mpegts")
    );

static GstStaticPadTemplate video_src_pad_template = GST_STATIC_PAD_TEMPLATE
    ("video_%04d",
    GST_PAD_SRC,
    GST_PAD_SOMETIMES,
    GST_STATIC_CAPS ("ANY")
    );

static GstStaticPadTemplate audio_src_pad_template = GST_STATIC_PAD_TEMPLATE
    ("audio_%04d",
    GST_PAD_SRC,
    GST_PAD_SOMETIMES,
    GST_STATIC_CAPS ("ANY")
    );

static GstStaticPadTemplate subtitle_src_pad_template = GST_STATIC_PAD_TEMPLATE
    ("subtitle_%04d",
    GST_PAD_SRC,
    GST_PAD_SOMETIMES,
    GST_STATIC_CAPS ("ANY")
    );

#define gst_stm_ts_demux_parent_class parent_class
G_DEFINE_TYPE (Gststm_ts_demux, gst_stm_ts_demux, GST_TYPE_ELEMENT);

/* Demux init functions */
/* -------------------- */
static void gst_stm_ts_demux_class_init (Gststm_ts_demuxClass * klass);
static void gst_stm_ts_demux_init (Gststm_ts_demux * filter);

/* Resource Management  */
/* -------------------- */
static void gst_stm_ts_demux_resource_alloc (Gststm_ts_demux * demux);
static void gst_stm_ts_demux_resource_dealloc (Gststm_ts_demux * demux);


/* Demux vmethods */
/* -------------- */
static void gst_stm_ts_demux_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_stm_ts_demux_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static GstFlowReturn gst_stm_ts_demux_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buf);
static void gst_stm_ts_demux_finalize (GObject * object);
static GstStateChangeReturn gst_stm_ts_demux_change_state (GstElement * element,
    GstStateChange transition);

/* Demux sink pad functions */
/* ------------------------ */
static gboolean gst_stm_ts_demux_sink_activate (GstPad * sinkpad,
    GstObject * parent);
static gboolean gst_stm_ts_demux_sink_activatemode (GstPad * sinkpad,
    GstObject * parent, GstPadMode mode, gboolean active);
static gboolean gst_stm_ts_demux_sink_activate_pull (GstPad * sinkpad,
    GstObject * parent, gboolean active);
static gboolean gst_stm_ts_demux_sink_activate_push (GstPad * sinkpad,
    GstObject * parent, gboolean active);
static gboolean gst_stm_ts_demux_handle_sink_event (GstPad * sinkpad,
    GstObject * parent, GstEvent * event);
static gboolean gst_stm_ts_demux_push_avsync_event (Gststm_ts_demux * demux,
    gboolean avsync);
static gboolean gst_stm_ts_demux_push_ptswrap_event (Gststm_ts_demux * demux);

/* Demux source pad functions */
static gboolean gst_stm_ts_demux_src_query (GstPad * pad, GstObject * parent,
    GstQuery * query);
static gboolean gst_stm_ts_demux_handle_src_event (GstPad * pad,
    GstObject * parent, GstEvent * event);

/* Demux local functions */
/* --------------------- */
static gboolean gst_stm_ts_demux_open (Gststm_ts_demux * demux);
static void gst_stm_ts_demux_close (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_create_devices (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_delete_devices (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_create_dvr_device (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_delete_dvr_device (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_delete_av_device (Gststm_ts_demux * demux,
    gint index);
static gboolean gst_stm_ts_demux_setup_pcr_device (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_delete_pcr_device (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_program_pcr_filter (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_open_demux (Gststm_ts_demux * demux,
    gint * fd);
static gboolean gst_stm_ts_demux_close_demux (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream);
static gboolean gst_stm_ts_demux_program_demux (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream);
static gboolean gst_stm_ts_demux_stop_demux (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream);
static void gst_stm_ts_demux_write_loop (GstPad * pad);
static void gst_stm_ts_demux_read_loop (GstPad * pad);
static gboolean gst_stm_ts_demux_read_send_pes (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream, gboolean ref_index, gboolean drop_pes);
static gboolean gst_stm_ts_demux_push_buf (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream, GstBuffer * buf, GstCaps * caps,
    guint64 timestamp, gboolean ref_index);
static void gst_stm_ts_demux_reset (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_create_stream (Gststm_ts_demux * demux,
    gint index);
static gboolean gst_stm_ts_demux_delete_stream (Gststm_ts_demux * demux,
    gint index);
static gboolean gst_stm_ts_demux_change_stream (Gststm_ts_demux * demux,
    stream_content_t content, guint pid, guint codec);
static gboolean gst_stm_ts_demux_push_event (Gststm_ts_demux * demux,
    GstEvent * event);
static gboolean gst_stm_ts_handle_pull_mode_seek (Gststm_ts_demux * demux,
    GstPad * pad, GstEvent * event);
static void gst_stm_ts_demux_seek_flush_start (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_handle_push_mode_seek (Gststm_ts_demux * demux,
    GstPad * pad, GstObject * parent, GstEvent * event);
/* For file operation */
static gboolean gst_stm_ts_demux_do_seek (Gststm_ts_demux * demux,
    GstSegment * segment, gboolean upstream_seek, GstSeekType cur_type,
    GstSeekType stop_type);
static gboolean gst_stm_ts_demux_push_mode_read (Gststm_ts_demux * demux,
    GstBuffer ** buf, guint size, gboolean copy, guint64 offset);
/* For stream info */
static gboolean gst_stm_ts_demux_parse_stream_info (Gststm_ts_demux * demux);
/* For pts/time operation */
static gboolean gst_stm_ts_demux_get_duration (Gststm_ts_demux * demux,
    GstClockTime * duration);
static void gst_stm_ts_demux_get_pts (Gststm_ts_demux * demux,
    GstBuffer * buf, guint64 offset);
static gboolean gst_stm_ts_demux_parse_pts (Gststm_ts_demux * demux,
    GstBuffer * buf, guint search, guint64 * pts, guint64 * offset);
static gboolean gst_stm_ts_demux_extract_pts (guint8 * pes_header,
    guint64 * pts);
/* Simple operations */
static void gst_stm_ts_demux_handle_chain_discontinue (Gststm_ts_demux * demux,
    GstPipeContext * pipe);
static gboolean gst_stm_ts_demux_pause_read_write_task (Gststm_ts_demux *
    demux);
static gboolean gst_stm_ts_demux_pause_write_task (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_resume_write_task (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_pause_read_task (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_resume_read_write_task (Gststm_ts_demux *
    demux);
static gboolean
gst_stm_ts_demux_handle_create_delete_av_devices (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_check_live_mode (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_push_pcr_event (Gststm_ts_demux * demux,
    GstEvent * event);
static gboolean gst_stm_ts_demux_send_pcr_data (Gststm_ts_demux * demux);
/* For hardware demuxer flush */
static gboolean gst_stm_ts_hwdemux_flush (Gststm_ts_demux * demux);
static void gst_stm_ts_demux_update_segment_to_rel (Gststm_ts_demux * demux,
    GstSegment * segment);
static void gst_stm_ts_demux_update_segment_to_abs (Gststm_ts_demux * demux,
    GstSegment * segment);
static gboolean gst_stm_ts_demux_check_ptswrap (Gststm_ts_demux * demux,
    guint64 timestamp);
static void gst_stm_ts_demux_stream_position (Gststm_ts_demux * demux,
    guint64 timestamp, gboolean ptswrap);
/* For protected streams */
static void
gst_stm_ts_demux_append_protection_system_id (Gststm_ts_demux * tsdemux,
    const gchar * system_id);
static GstBuffer *gst_stm_ts_demux_read_get_buffer (Gststm_ts_stream * stream);
static gboolean gst_stm_ts_demux_read_ecm_data (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream);
static void gst_stm_ts_demux_update_stream_ecm_queue (Gststm_ts_demux *
    demux, GstBuffer * buf);
static gboolean gst_stm_ts_demux_update_stream_ecm_data (Gststm_ts_stream *
    stream);
static GstStructure
    * gst_stm_ts_demux_get_stream_protection_metadata (Gststm_ts_stream *
    stream, guint64 pts);
static gboolean gst_stm_ts_demux_read_parse_pes_protected (Gststm_ts_demux *
    demux, Gststm_ts_stream * stream, gboolean ref_index, gboolean drop_pes);
static gboolean gst_stm_ts_demux_read_send_pes_protected (Gststm_ts_demux *
    demux, Gststm_ts_stream * stream, gboolean ref_index, gboolean drop_pes,
    GstBuffer * buf);
static gboolean gst_stm_ts_demux_protected_stream_flush (Gststm_ts_demux *
    demux);
static void gst_stm_ts_demux_ecm_free (GstStm_ts_SENC_ECM_Data * senc_ecm_data);

/* Demux variables */
/* --------------- */
/* Initialize the STM TS demux class */
static void
gst_stm_ts_demux_class_init (Gststm_ts_demuxClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *element_class;

  GST_DEBUG ("gst_stm_ts_demux_class_init");

  gobject_class = (GObjectClass *) klass;
  element_class = (GstElementClass *) klass;

  gst_element_class_set_details_simple (element_class,
      "ST TS demux",
      "Codec/Demuxer",
      "Demultiplex a ts file into audio and video", "www.st.com");

  /* Add source pad template */
  /* Add source pad for audio */
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&audio_src_pad_template));
  /* Add source pad for video */
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&video_src_pad_template));
  /* Add source pad for subtitle */
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&subtitle_src_pad_template));

  /* Add sink pad template */
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_pad_template));

  gobject_class->set_property =
      GST_DEBUG_FUNCPTR (gst_stm_ts_demux_set_property);
  gobject_class->get_property =
      GST_DEBUG_FUNCPTR (gst_stm_ts_demux_get_property);

  g_object_class_install_property (gobject_class, PROP_ES_PIDS,
      g_param_spec_string ("es-pids",
          "Colon separated list of PIDs containing Elementary Streams",
          "PIDs to treat as Elementary Streams in the absence of a PMT, "
          "v:pid:codec/a:pid:codec/m:pid:codec, "
          "eg v:4129:2/a:4133:4/m:4134:1", DEFAULT_PROP_ES_PIDS,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_TS_TABLE,
      g_param_spec_string ("ts-table",
          "Colon separated list of TS streams information",
          "Information gather from PAT, PMT and SDT, "
          "program number:codec name:codec type:pid:codec:scrambled, "
          "eg 1:MP4V:STREAM_CONTENT_VIDEO:33:2:1", DEFAULT_PROP_TS_TABLE,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_ENABLE_PLAYBACK,
      g_param_spec_boolean ("enable-playback",
          "Enable/Disable playback of ts streams",
          "Enable/Disable playback of ts streams", FALSE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_PMT_INFO,
      g_param_spec_object ("pmt-info",
          "All informations regarding a program",
          "All informations regarding a program",
          GST_TYPE_STM_PMT_INFO, G_PARAM_READABLE));
  g_object_class_install_property (gobject_class, PROP_PROGRAM_NUMBER,
      g_param_spec_int ("program-number", "program number",
          "Program Number (-1 to ignore) for demux to play", -1, G_MAXINT,
          -1, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_DEMUX_DEV_ID,
      g_param_spec_int ("dev-id", "The demux device number",
          "The demux device number (eg. 0 for demux0)",
          MIN_DEV_ID, MAX_DEV_ID, DEFAULT_DEV_ID, G_PARAM_READWRITE));
  gobject_class->finalize = GST_DEBUG_FUNCPTR (gst_stm_ts_demux_finalize);

  element_class->change_state =
      GST_DEBUG_FUNCPTR (gst_stm_ts_demux_change_state);

  return;
}

/* All resource allocations are done here */

static void
gst_stm_ts_demux_resource_alloc (Gststm_ts_demux * demux)
{
  /* Initialize push mode data members */
  g_mutex_init (&demux->pipe.tlock);
  g_cond_init (&demux->pipe.cond);

  demux->pipe.adapter = gst_adapter_new ();

  demux->table = NULL;
  demux->table = g_malloc0 (128 * 1024);
  if (demux->table) {
    demux->table_ptr = 0;
  }

  demux->pmt_info = NULL;
  demux->pmt_info = g_object_new (GST_TYPE_STM_PMT_INFO, NULL);

  /* Create and initialize the demux task for writing to dvr device */
  g_rec_mutex_init (&demux->write_task_lock);
  demux->write_task =
      gst_task_new ((GstTaskFunction) gst_stm_ts_demux_write_loop,
      demux->sinkpad, NULL);
  gst_object_set_name (GST_OBJECT_CAST (demux->write_task), "SMF-TSDmx Write");
  gst_task_set_lock (demux->write_task, &demux->write_task_lock);

  /* Create and initialize the demux task for reading from demux device */
  g_rec_mutex_init (&demux->read_task_lock);
  demux->read_task =
      gst_task_new ((GstTaskFunction) gst_stm_ts_demux_read_loop,
      demux->sinkpad, NULL);
  gst_object_set_name (GST_OBJECT_CAST (demux->read_task), "SMF-TSDmx Read");
  gst_task_set_lock (demux->read_task, &demux->read_task_lock);
}


/* All resource freed here */

static void
gst_stm_ts_demux_resource_dealloc (Gststm_ts_demux * demux)
{
  if (demux->table) {
    g_free (demux->table);
    demux->table = NULL;
  }

  if (demux->pmt_info) {
    g_object_unref (demux->pmt_info);
    demux->pmt_info = NULL;
  }

  gst_object_unref (demux->read_task);
  /* Release push mode data members */
  gst_object_unref (demux->write_task);
  gst_object_unref (demux->pipe.adapter);

  g_mutex_clear (&demux->pipe.tlock);
  g_cond_clear (&demux->pipe.cond);
  g_rec_mutex_clear (&demux->write_task_lock);
  g_rec_mutex_clear (&demux->read_task_lock);
}


/* Initialize the demux element
 * Instantiate pads and add them to element
 * Set pad calback functions
 * Initialize instance structure
 */
static void
gst_stm_ts_demux_init (Gststm_ts_demux * demux)
{
  GST_DEBUG ("gst_stm_ts_demux_init");

  /* Initialize demux data members */
  gst_stm_ts_demux_reset (demux);

  /* Reset demux device id */
  demux->dev_id = -1;

  /* Create sink pad */
  demux->sinkpad =
      gst_pad_new_from_static_template (&sink_pad_template, "sink");
  /* Add vmethod for sink pad */
  gst_pad_set_activate_function (demux->sinkpad,
      GST_DEBUG_FUNCPTR (gst_stm_ts_demux_sink_activate));
  gst_pad_set_activatemode_function (demux->sinkpad,
      GST_DEBUG_FUNCPTR (gst_stm_ts_demux_sink_activatemode));
  gst_pad_set_chain_function (demux->sinkpad,
      GST_DEBUG_FUNCPTR (gst_stm_ts_demux_chain));
  gst_pad_set_event_function (demux->sinkpad,
      GST_DEBUG_FUNCPTR (gst_stm_ts_demux_handle_sink_event));
  /* Add sink pad to element */
  gst_element_add_pad (GST_ELEMENT (demux), demux->sinkpad);

  g_queue_init (&demux->protection_event_queue);

  return;
}

/* GstElement vmethod implementations */
static void
gst_stm_ts_demux_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  Gststm_ts_demux *demux = GST_STM_TS_DEMUX (object);
  gchar **streams;
  gchar **param;
  gint i, pid_index;
  guint num_streams, num_param;
  gboolean register_stream = FALSE;

  switch (prop_id) {
    case PROP_ES_PIDS:
      GST_DEBUG_OBJECT (demux, "set PROP_ES_PIDS %s",
          g_value_get_string (value));

      if (demux->pids_set_by_event == TRUE) {
        /* pids have already been set by st_recorder so no more pid updates is allowed */
        GST_DEBUG_OBJECT (demux, "PIDs are already set by st_recorder \n");
        /* post a message  to application to notify wrong usage */
        gst_element_post_message (GST_ELEMENT_CAST (demux),
            gst_message_new_element (GST_OBJECT (demux),
                gst_structure_new_empty ("wrong-pid-set-usage")));
        break;
      }
      gst_stm_ts_demux_pause_read_write_task (demux);
      if (!demux->num_streams) {
        register_stream = TRUE;
      }
      streams = g_strsplit (g_value_get_string (value), "/", 0);
      num_streams = g_strv_length (streams);
      GST_DEBUG_OBJECT (demux, "num streams %d", num_streams);
      for (i = 0; i < num_streams; i++) {
        guint pid, codec;
        stream_content_t content;
        param = g_strsplit (streams[i], ":", 0);
        num_param = g_strv_length (param);
        if (num_param >= 3) {
          if ((!strcmp (param[0], "a")) || (!strcmp (param[0], "m"))) {
            if ((!strcmp (param[0], "a"))) {
              content = STREAM_CONTENT_AUDIO;
            } else {
              content = STREAM_CONTENT_SUBTITLE;
            }
            for (pid_index = 1; pid_index < num_param; pid_index += 2) {
              pid = (guint) strtol (param[pid_index], NULL, 0);
              codec = (guint) strtol (param[pid_index + 1], NULL, 0);
              GST_DEBUG_OBJECT (demux, "pid %d codec %d\n", pid, codec);
              if (!register_stream) {
                gst_stm_ts_demux_change_stream (demux, content, pid, codec);
              } else {
                gst_stm_ts_demux_register_stream (demux, content, pid, codec,
                    FALSE);
              }
            }
            continue;
          }
          pid = (guint) strtol (param[1], NULL, 0);
          codec = (guint) strtol (param[2], NULL, 0);
          GST_DEBUG_OBJECT (demux, "pid %d codec %d\n", pid, codec);
          if (strcmp (param[0], "v") == 0) {
            content = STREAM_CONTENT_VIDEO;
          } else {
            content = STREAM_CONTENT_OTHER;
          }
          GST_DEBUG_OBJECT (demux, "pid %d codec %d\n", pid, codec);
          gst_stm_ts_demux_change_stream (demux, content, pid, codec);
        } else if (num_param == 2) {
          /* set pcr pid to create a PCR filter */
          if (strcmp (param[0], "j") == 0) {
            demux->pcr_pid = (gint) strtol (param[1], NULL, 0);
            GST_DEBUG_OBJECT (demux, "pcr pid %d\n", demux->pcr_pid);
          }
        }
        g_strfreev (param);
      }
      g_strfreev (streams);
      demux->pids_set_by_property = TRUE;
      gst_stm_ts_demux_resume_read_write_task (demux);
      break;
    case PROP_ENABLE_PLAYBACK:
      GST_DEBUG_OBJECT (demux, "set PROP_ENABLE_PLAYBACK %d",
          g_value_get_boolean (value));
      demux->enable_playback = g_value_get_boolean (value);
      break;
    case PROP_PMT_INFO:
      break;
    case PROP_PROGRAM_NUMBER:
      demux->requested_program_number = g_value_get_int (value);
      break;
    case PROP_DEMUX_DEV_ID:
      demux->dev_id = g_value_get_int (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

  return;
}

static void
gst_stm_ts_demux_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  Gststm_ts_demux *demux = GST_STM_TS_DEMUX (object);
  guint i;

  switch (prop_id) {
    case PROP_ES_PIDS:
    {
      gchar str[100];
      gchar str1[20];
      gboolean audio_found = FALSE;
      gboolean ancillary_found = FALSE;

      memset (str, '\0', sizeof (str));
      memset (str1, '\0', sizeof (str1));

      for (i = 0; i < demux->num_streams; i++) {
        if (demux->streams[i]->content == STREAM_CONTENT_VIDEO) {
          g_snprintf (str, sizeof (str), "v:%d:%d/", demux->streams[i]->pid,
              demux->streams[i]->codec);
        }
        if (demux->streams[i]->content == STREAM_CONTENT_AUDIO) {
          if (!audio_found) {
            g_strlcat (str, "a", sizeof (str));
          }
          g_snprintf (str1, sizeof (str1), ":%d:%d", demux->streams[i]->pid,
              demux->streams[i]->codec);
          g_strlcat (str, str1, sizeof (str));
          audio_found = TRUE;
        }
        if (demux->streams[i]->content == STREAM_CONTENT_SUBTITLE) {
          if (audio_found) {
            g_strlcat (str, "/", sizeof (str));
            audio_found = FALSE;
          }
          if (!ancillary_found) {
            g_strlcat (str, "m", sizeof (str));
          }
          g_snprintf (str1, sizeof (str1), ":%d:%d", demux->streams[i]->pid,
              demux->streams[i]->codec);
          g_strlcat (str, str1, sizeof (str));
          ancillary_found = TRUE;
        }
      }
      if (audio_found && (!ancillary_found)) {
        g_strlcat (str, "/", sizeof (str));
      }
      if (demux->pcr_pid != G_MAXUINT16) {
        g_snprintf (str1, sizeof (str1), "j:%d/", demux->pcr_pid);
        g_strlcat (str, str1, sizeof (str));
      }
      g_value_set_string (value, str);
    }
      break;
    case PROP_TS_TABLE:
      if (demux->table) {
        g_value_set_string (value, demux->table);
      }
      break;
    case PROP_ENABLE_PLAYBACK:
      g_value_set_boolean (value, demux->enable_playback);
      break;
    case PROP_PMT_INFO:
      GST_OBJECT_LOCK (demux);
      /* Give the GObject to application */
      g_value_set_object (value, demux->pmt_info);
      GST_OBJECT_UNLOCK (demux);
      break;
    case PROP_PROGRAM_NUMBER:
      g_value_set_int (value, demux->requested_program_number);
      break;
    case PROP_DEMUX_DEV_ID:
      g_value_set_int (value, demux->dev_id);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

  return;
}

/*
  * Returns the aggregated GstFlowReturn.
  * a demux element should report unlinked flow error only if all its src pads have reported unlinked error
  * so this aggregated here
  */
static GstFlowReturn
gst_stm_ts_combine_flows (Gststm_ts_demux * demux, Gststm_ts_stream * stream,
    GstFlowReturn ret)
{
  guint i;

  /* store the value */
  stream->last_flow = ret;
  GST_LOG_OBJECT (demux, "setting last_flow on pad %s to %s",
      stream->padname, gst_flow_get_name (ret));

  /* any other error that is not-linked can be returned right away */
  if (ret != GST_FLOW_NOT_LINKED)
    goto done;

  /* only return NOT_LINKED if all other pads returned NOT_LINKED */
  for (i = 0; i < demux->num_streams; i++) {
    Gststm_ts_stream *ostream = demux->streams[i];

    if (ostream == NULL)
      continue;

    ret = ostream->last_flow;
    GST_LOG_OBJECT (demux, "stream %d: last_flow: %s, num_streams=%d", i,
        gst_flow_get_name (ret), demux->num_streams);

    /* some other return value (must be SUCCESS but we can return
     * other values as well) */

    if (ret != GST_FLOW_NOT_LINKED)
      goto done;
  }

  /* if we get here, all other pads were unlinked and we return
   * NOT_LINKED then */

done:
  GST_LOG_OBJECT (demux, "combined return %s for stream with pid %d (pad %s)",
      gst_flow_get_name (ret), stream->pid, stream->padname);
  return ret;
}


/* Chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_stm_ts_demux_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
  Gststm_ts_demux *demux = GST_STM_TS_DEMUX (parent);
  GstPipeContext *pipe;
  guint i;
  GstFlowReturn ret = GST_FLOW_OK;
  pipe = &demux->pipe;

  if (demux->srcresult != GST_FLOW_OK) {
    GST_ERROR_OBJECT (demux, "error in data flow %s ",
        gst_flow_get_name (demux->srcresult));
    ret = demux->srcresult;
    goto done;
  }

  /* If devices are not open after we open the demux,
     just send dummy buffer to downstream to change the pipeline to play state.
     This is the case for recording pipeline. */
  if ((demux->dev_opened == FALSE) && (demux->opened == TRUE)) {
    for (i = 0; ((i < demux->num_streams) && (demux->push_index == 0)); i++) {
      if (demux->streams[i]) {
        if (gst_stm_ts_demux_push_buf (demux,
                demux->streams[i], NULL, NULL, GST_CLOCK_TIME_NONE,
                FALSE) == TRUE) {
          demux->push_index++;
        }
      }
    }
    /* Since av and demux device not open, drop data here to avoid blocking the chain */
    goto done;
  }

  /* Handle discontinue buffer */
  if (GST_BUFFER_IS_DISCONT (buf)) {
    gst_stm_ts_demux_handle_chain_discontinue (demux, pipe);
  }

  if (gst_buffer_get_size (buf) == 0) {
    goto done;
  }

  GST_STM_PIPE_MUTEX_LOCK (pipe);

  /* If we reach end of file */
  if (pipe->eof == TRUE) {
    GST_DEBUG_OBJECT (demux, "ignoring buffer at end-of-stream");
    GST_STM_PIPE_MUTEX_UNLOCK (pipe);
    ret = GST_FLOW_EOS;
    goto done;
  }

  /* Source may have some problem or pause for seeking */
  if (pipe->srcresult != GST_FLOW_OK) {
    GST_DEBUG_OBJECT (demux,
        "ignoring buffer because source task encountered %s",
        gst_flow_get_name (pipe->srcresult));
    GST_STM_PIPE_MUTEX_UNLOCK (pipe);
    ret = GST_FLOW_FLUSHING;
    goto done;
  }

  GST_TRACE_OBJECT (demux, "getting a buffer of %d bytes",
      gst_buffer_get_size (buf));


  /* Push data to adapter */
  gst_adapter_push (pipe->adapter, buf);
  /* Check data available in the adapter, signal the loop task to read data if we have more than needed */
  while ((gst_adapter_available (pipe->adapter) >= pipe->needed)) {
    GST_TRACE_OBJECT (demux, "adapter has more than requested (%i)",
        pipe->needed);
    GST_STM_PIPE_SIGNAL (pipe);
    GST_STM_PIPE_WAIT (pipe);
    /* Flushing may happen here */
    if (pipe->srcresult != GST_FLOW_OK) {
      GST_DEBUG_OBJECT (demux,
          "ignoring buffer because source task encountered %s",
          gst_flow_get_name (pipe->srcresult));
      break;
    }
  }

  /* Unlock the pipe */
  GST_STM_PIPE_MUTEX_UNLOCK (pipe);

  return GST_FLOW_OK;

done:
  gst_buffer_unref (buf);
  /* If upstream is not seekable, don't send GST_FLOW_WRONG_STATE to upstream element */
  if ((ret == GST_FLOW_FLUSHING) && (demux->upstream_seekable == FALSE)) {
    return GST_FLOW_OK;
  }

  return ret;
}

static void
gst_stm_ts_demux_handle_chain_discontinue (Gststm_ts_demux * demux,
    GstPipeContext * pipe)
{
  GstBuffer *temp_buf;
  gint i;
  guint remain = 0;
  GstMapInfo info_read;

  if ((demux->upstream_rap_index_support == FALSE)
      && (demux->upstream_time_index_support == FALSE)) {
    Gststm_ts_stream *stream = NULL;
    if (demux->ref_index != 0xffffffff)
      stream = demux->streams[demux->ref_index];
    if (stream != NULL && stream->pid != GST_STM_PID_NONE) {
      static_ts_marker[OFFSET_PIDH] = (static_ts_marker[OFFSET_PIDH] & 0xC0) |
          ((stream->pid & 0x1f00) >> 8);
      static_ts_marker[OFFSET_PIDL] = (stream->pid & 0xff);
      temp_buf = gst_buffer_new_allocate (NULL, STM_MARKER_TS_SIZE, NULL);
      if (temp_buf != NULL) {
        GstMapInfo info_write;
        gst_buffer_map (temp_buf, &info_write, GST_MAP_WRITE);
        memcpy (info_write.data, static_ts_marker, STM_MARKER_TS_SIZE);
        gst_buffer_unmap (temp_buf, &info_write);
        gst_buffer_set_size (temp_buf, STM_MARKER_TS_SIZE);
        gst_adapter_push (pipe->adapter, temp_buf);
      }
    }
    /* We have a discontinue buffer */
    /* FIXME: stts_demux should handle upstream discontinuity
       for non DVR too
     */
    GST_DEBUG_OBJECT (demux, "receiving discontinue buffer");
    demux->sync_lock = FALSE;
  } else {
    /* Handle discontinue buffer coming from strecorder element.
       We will get the discontinue buffer during rap injection. */
    GST_STM_PIPE_MUTEX_LOCK (pipe);
    remain = gst_adapter_available (pipe->adapter);
    if ((remain >=
            (GST_STM_TS_DEMUX_NUM_DISCONTINUE_PACKET * demux->packetsize))
        || (demux->disc_read == FALSE)) {
      i = 0;
      /* Read data from the adapter to dest buffer */
      if (remain >=
          (GST_STM_TS_DEMUX_NUM_DISCONTINUE_PACKET * demux->packetsize)) {
        temp_buf = gst_adapter_take_buffer (pipe->adapter, remain);
        gst_buffer_map (temp_buf, &info_read, GST_MAP_READ);
        write (demux->dvr_fd, info_read.data, info_read.size);
        gst_buffer_unmap (temp_buf, &info_read);
        gst_buffer_unref (temp_buf);
      }
      /* FIXME ! we need to use a better way to detect the discontunuity dummy */
      /* probably after fixing BZ 25398, (use discontinuity markers) we will have
         better mechanism to handle discontinuites for DVR and also for file playback
         use cases to avoid dummy PES and polling disc_read flag mechanism.
       */
      while ((demux->disc_read == FALSE) && (i < 500)) {
        usleep (10 * 1000);
        i++;
      }
    }
    demux->disc_read = FALSE;
    GST_STM_PIPE_MUTEX_UNLOCK (pipe);
  }
}

static gboolean
gst_stm_ts_demux_check_live_mode (Gststm_ts_demux * demux)
{
  GstQuery *query = NULL;
  GstStructure *structure;
  gboolean rap_index_support = FALSE, time_index_support = FALSE;
  gboolean res = FALSE;

  if (demux->live) {
    /* its live streaming. check if upstream element is in timeshift */
    if (gst_pad_is_linked (demux->sinkpad)) {
      /* Check if upstream supports rap index or time index */
      structure =
          gst_structure_new ("index-support", "index", G_TYPE_BOOLEAN, FALSE,
          NULL);
      query = gst_query_new_custom (GST_QUERY_CUSTOM, structure);
      if (query) {
        if (gst_pad_peer_query (demux->sinkpad, query) == TRUE) {
          gst_structure_get_boolean (structure, "rap-index",
              &rap_index_support);
          gst_structure_get_boolean (structure, "time-index",
              &time_index_support);
        }
        gst_query_unref (query);
      }
    }
    if ((!rap_index_support) && (!time_index_support)) {
      GST_DEBUG_OBJECT (demux, "tsdemux is in live mode  \n");
      res = TRUE;
    }
  }
  return res;
}

static void
gst_stm_ts_demux_finalize (GObject * object)
{
  Gststm_ts_demux *demux = (Gststm_ts_demux *) object;

  GST_LOG_OBJECT (demux, "gst_stm_ts_demux_finalize");

  G_OBJECT_CLASS (parent_class)->finalize (object);

  return;
}

static GstStateChangeReturn
gst_stm_ts_demux_change_state (GstElement * element, GstStateChange transition)
{
  Gststm_ts_demux *demux = GST_STM_TS_DEMUX (element);
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;

  GST_DEBUG_OBJECT (demux, "%d -> %d",
      GST_STATE_TRANSITION_CURRENT (transition),
      GST_STATE_TRANSITION_NEXT (transition));

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      gst_stm_ts_demux_resource_alloc (demux);
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
    {
      GstQuery *query = NULL;
      GstClockTime min_latency, max_latency;
      /* to find if pipeline has live source or not, send a latency query upstream */
      query = gst_query_new_latency ();
      if (query != NULL) {
        if (gst_pad_peer_query (demux->sinkpad, query)) {
          gst_query_parse_latency (query, &demux->live, &min_latency,
              &max_latency);
          GST_DEBUG_OBJECT (demux,
              "Peer latency: live %s, min %" GST_TIME_FORMAT " max %"
              GST_TIME_FORMAT, demux->live ? "TRUE" : "FALSE",
              GST_TIME_ARGS (min_latency), GST_TIME_ARGS (max_latency));
        }
        gst_query_unref (query);
      }
    }
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    return ret;
  }

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      gst_task_stop (demux->read_task);
      GST_TSDEMUX_MUTEX_LOCK (&demux->read_task_lock);
      GST_TSDEMUX_MUTEX_UNLOCK (&demux->read_task_lock);
      gst_task_join (demux->read_task);
      gst_stm_ts_demux_close (demux);
      if (demux->src_uri) {
        g_free (demux->src_uri);
      }
      gst_stm_ts_demux_reset (demux);
      gst_adapter_clear (demux->pipe.adapter);
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      gst_stm_ts_demux_resource_dealloc (demux);
      break;
    default:
      break;
  }

  return ret;
}

static gboolean
gst_stm_ts_demux_sink_activate (GstPad * sinkpad, GstObject * parent)
{
  GstQuery *query;
  gboolean pull_mode;

  query = gst_query_new_scheduling ();

  if (!gst_pad_peer_query (sinkpad, query)) {
    gst_query_unref (query);
    goto activate_push;
  }

  pull_mode = gst_query_has_scheduling_mode_with_flags (query,
      GST_PAD_MODE_PULL, GST_SCHEDULING_FLAG_SEEKABLE);
  gst_query_unref (query);

  if (!pull_mode) {
    goto activate_push;
  }

  GST_DEBUG_OBJECT (sinkpad, "activating pull");
  return gst_pad_activate_mode (sinkpad, GST_PAD_MODE_PULL, TRUE);

activate_push:
  {
    GST_DEBUG_OBJECT (sinkpad, "activating push");
    return gst_pad_activate_mode (sinkpad, GST_PAD_MODE_PUSH, TRUE);
  }
}

static gboolean
gst_stm_ts_demux_sink_activatemode (GstPad * sinkpad, GstObject * parent,
    GstPadMode mode, gboolean active)
{
  gboolean res;

  if (mode == GST_PAD_MODE_PUSH) {
    res = gst_stm_ts_demux_sink_activate_push (sinkpad, parent, active);
  } else {
    res = gst_stm_ts_demux_sink_activate_pull (sinkpad, parent, active);
  }
  return res;
}

static gboolean
gst_stm_ts_demux_sink_activate_pull (GstPad * sinkpad, GstObject * parent,
    gboolean active)
{
  Gststm_ts_demux *demux = GST_STM_TS_DEMUX (parent);
  gboolean res = FALSE;

  GST_LOG_OBJECT (demux, "gst_stm_ts_demux_sink_activate_pull ");

  if (active) {
    GST_LOG_OBJECT (demux, "start pulling data...");
    demux->streaming = FALSE;
    demux->running = TRUE;
    /* Start the sink pad task */
    res =
        gst_pad_start_task (sinkpad,
        (GstTaskFunction) gst_stm_ts_demux_write_loop, sinkpad, NULL);
  } else {
    gst_stm_ts_hwdemux_flush (demux);
    GST_LOG_OBJECT (demux, "stop pulling data");
    /* Stop the sink pad task */
    res = gst_pad_stop_task (sinkpad);
    demux->running = FALSE;
  }

  return res;
}

static gboolean
gst_stm_ts_demux_sink_activate_push (GstPad * sinkpad, GstObject * parent,
    gboolean active)
{
  Gststm_ts_demux *demux = GST_STM_TS_DEMUX (parent);
  GstPipeContext *pipe;
  gboolean res = TRUE;

  GST_LOG_OBJECT (demux, "gst_stm_ts_demux_sink_activate_push ");
  pipe = &demux->pipe;

  if (active) {
    GST_LOG_OBJECT (demux, "start receiving data...");
    pipe->eof = FALSE;
    pipe->closed = FALSE;
    pipe->srcresult = GST_FLOW_OK;
    pipe->needed = BUFFER_SIZE;
    pipe->pad = sinkpad;
    demux->streaming = TRUE;
    demux->running = TRUE;
    /* Start the task */
    res = gst_task_start (demux->write_task);
  } else {
    GST_LOG_OBJECT (demux, "stop receiving data");

    demux->disc_read = TRUE;
    GST_STM_PIPE_MUTEX_LOCK (pipe);
    /* Stop the chain processing */
    pipe->srcresult = GST_FLOW_FLUSHING;
    pipe->eof = TRUE;
    pipe->closed = TRUE;
    /* Trigger a signal to end it immediately */
    GST_STM_PIPE_SIGNAL (pipe);
    GST_STM_PIPE_MUTEX_UNLOCK (pipe);
    /* flush the demux */
    gst_stm_ts_hwdemux_flush (demux);
    /* Stopping the task */
    res = gst_task_stop (demux->write_task);
    GST_TSDEMUX_MUTEX_LOCK (&demux->write_task_lock);
    GST_TSDEMUX_MUTEX_UNLOCK (&demux->write_task_lock);
    /* Wait for the task to stop */
    res |= gst_task_join (demux->write_task);
    demux->running = FALSE;
  }

  return res;
}

static gboolean
gst_stm_ts_demux_handle_sink_event (GstPad * sinkpad, GstObject * parent,
    GstEvent * event)
{
  Gststm_ts_demux *demux = GST_STM_TS_DEMUX (parent);
  GstPipeContext *pipe;
  gboolean res = FALSE;
  const GstSegment *segment;

  pipe = &demux->pipe;
  GST_DEBUG_OBJECT (demux, "%s - (%s)", __FUNCTION__,
      gst_event_type_get_name (GST_EVENT_TYPE (event)));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CAPS:
    {
      gst_event_unref (event);
      return TRUE;
    }
    case GST_EVENT_SEGMENT:
    {
      gst_event_parse_segment (event, &segment);
      if ((segment->format == GST_FORMAT_TIME)
          && (demux->internal_seek == FALSE)) {
        if (GST_CLOCK_TIME_IS_VALID (segment->start)) {
          demux->offset = segment->start;
          gst_segment_copy_into (segment, &demux->segment);
          res = gst_pad_event_default (sinkpad, parent, event);
        } else {
          /* received a segment with invalid start time */
          /* stts_demux should send a new segment with valid start */
          demux->prev_pts = 0;
          demux->strm_pos = 0;
          demux->start_pts = 0;
          demux->start_time = GST_CLOCK_TIME_NONE;
          demux->segment.rate = segment->rate;
          gst_event_unref (event);
          res = TRUE;
        }
      } else {
        GST_DEBUG_OBJECT (demux, "consume the event");
        /* we must send a segment event */
        demux->newsegment = TRUE;
        gst_event_unref (event);
        res = TRUE;
      }
      break;
    }
    case GST_EVENT_FLUSH_START:
      /* Forward event */
      GST_STM_PIPE_MUTEX_LOCK (pipe);
      pipe->srcresult = GST_FLOW_FLUSHING;
      demux->flushing = TRUE;
      GST_STM_PIPE_SIGNAL (pipe);
      GST_STM_PIPE_MUTEX_UNLOCK (pipe);
      if ((demux->internal_seek == FALSE) && (demux->upstream_seekable == TRUE)) {
        res = gst_stm_ts_demux_push_event (demux, event);
      } else {
        res = TRUE;
        /* unref the event */
        gst_event_unref (event);
      }

      gst_stm_ts_hwdemux_flush (demux);
      gst_stm_ts_demux_pause_write_task (demux);
      gst_stm_ts_demux_pause_read_task (demux);
      break;
    case GST_EVENT_FLUSH_STOP:
      gst_adapter_clear (pipe->adapter);
      gst_stm_ts_hwdemux_flush (demux);
      gst_stm_ts_demux_protected_stream_flush (demux);

      /* Forward event */
      demux->srcresult = GST_FLOW_OK;
      demux->flushing = FALSE;
      if ((demux->internal_seek == FALSE) && (demux->upstream_seekable == TRUE)) {
        res = gst_stm_ts_demux_push_event (demux, event);
      } else {
        res = TRUE;
        /* unref the event */
        gst_event_unref (event);
      }
      /* clear eos state in pull mode */
      demux->eos = FALSE;
      /* For internal seek only need to restart chain writing data to adapter */
      demux->sync_lock = FALSE;
      GST_STM_PIPE_MUTEX_LOCK (pipe);
      pipe->srcresult = GST_FLOW_OK;
      /* clear eos state in push mode */
      pipe->eof = FALSE;
      GST_STM_PIPE_MUTEX_UNLOCK (pipe);

      /* Resume Read write task if paused */
      if ((demux->read_task)
          && (gst_task_get_state (demux->read_task) == GST_TASK_PAUSED)) {
        gst_task_start (demux->read_task);
      }

      if (demux->write_task
          && (gst_task_get_state (demux->write_task) == GST_TASK_PAUSED)) {
        gst_stm_ts_demux_resume_write_task (demux);
      }
      break;
    case GST_EVENT_EOS:
      /* Inform the source to stop */
      GST_STM_PIPE_MUTEX_LOCK (pipe);
      pipe->eof = TRUE;
      GST_STM_PIPE_SIGNAL (pipe);

      GST_STM_PIPE_MUTEX_UNLOCK (pipe);
      /* Consume this event for now, task will send eos when finished */
      gst_event_unref (event);
      res = TRUE;
      break;
    case GST_EVENT_CUSTOM_DOWNSTREAM_OOB:
    {
      const GstStructure *structure;

      structure = gst_event_get_structure (event);
      if (gst_structure_has_name (structure, "hwdemux-connection")) {
        gint i;
        gint pid;
        gint codec;
        gint num_audio = 0;
        gint num_ancillary = 0;
        gchar *codec_field = NULL, *pid_field = NULL;
        const GValue *value;
        gboolean register_stream = FALSE;
        gboolean video_present = FALSE;
        gboolean audio_present = FALSE;

        GST_DEBUG_OBJECT (demux, "hwdemux-connection event");
        if (!demux->num_streams) {
          register_stream = TRUE;
        }
        demux->need_unlock = TRUE;
        GST_STM_PIPE_SIGNAL (pipe);
        gst_stm_ts_demux_pause_read_write_task (demux);
        demux->need_unlock = FALSE;
        GST_STM_PIPE_MUTEX_LOCK (pipe);
        /* stop demux on existing pads */
        for (i = 0; i < demux->num_streams; i++) {
          if (!gst_stm_ts_demux_stop_demux (demux, demux->streams[i])) {
            GST_DEBUG_OBJECT (demux, "failed to stop demux");
          }
        }

        if (gst_stm_ts_demux_check_live_mode (demux)) {
          gst_stm_ts_demux_push_event (demux, gst_event_new_flush_start ());
          gst_stm_ts_demux_push_event (demux, gst_event_new_flush_stop (TRUE));
        }

        if ((gst_structure_get_int (structure, "video-codec", &codec) == TRUE)
            && (gst_structure_get_int (structure, "video-pid", &pid) == TRUE)) {
          /* in current channel configure file, for non pid track, set the pid == NONE */
          if (pid != GST_STM_PID_NONE) {
            GST_DEBUG_OBJECT (demux, "pid %d codec %d\n", pid, codec);
            gst_stm_ts_demux_change_stream (demux, STREAM_CONTENT_VIDEO, pid,
                codec);
            video_present = TRUE;
          } else {
            for (i = 0; i < demux->num_streams; i++) {
              if ((demux->streams[i] != NULL)
                  && (demux->streams[i]->content == STREAM_CONTENT_VIDEO)) {
                /* Update pid and codec */
                demux->streams[i]->pid = pid;
                demux->streams[i]->codec = codec;
                /* Delete the video device for audio only stream */
                if (gst_stm_ts_demux_delete_av_device (demux, i) == FALSE) {
                  GST_ERROR_OBJECT (demux, "failed to delete video device %i",
                      i);
                }
              }
            }
          }
        }
        if (gst_structure_get_int (structure, "audio-num", &num_audio) == TRUE) {
          for (i = 0; i < num_audio; i++) {
            codec_field = g_strdup_printf ("audio-codec%d", i);
            pid_field = g_strdup_printf ("audio-pid%d", i);
            if ((gst_structure_get_int (structure, codec_field, &codec) == TRUE)
                && (gst_structure_get_int (structure, pid_field, &pid) == TRUE)) {
              if (pid == GST_STM_PID_NONE) {
                GST_WARNING_OBJECT (demux, "unexpected pid %d codec %d\n", pid,
                    codec);
                g_free (codec_field);
                g_free (pid_field);
                continue;
              }
              GST_DEBUG_OBJECT (demux, "pid %d codec %d\n", pid, codec);
              if (!register_stream) {
                /* in current channel configure file, for non pid track, set the pid == NONE */
                gst_stm_ts_demux_change_stream (demux, STREAM_CONTENT_AUDIO,
                    pid, codec);
                /* Disable avsync for audio only channel */
                if (gst_stm_ts_demux_push_avsync_event (demux,
                        video_present) == FALSE) {
                  GST_WARNING_OBJECT (demux, "failed to push av-sync event");
                }
              } else {
                gst_stm_ts_demux_register_stream (demux, STREAM_CONTENT_AUDIO,
                    pid, codec, FALSE);
              }
              audio_present = TRUE;
            }
            g_free (codec_field);
            g_free (pid_field);
          }
        }
        /* for video only channel lets close audio */
        if (!audio_present) {
          for (i = 0; i < demux->num_streams; i++) {
            if ((demux->streams[i] != NULL)
                && (demux->streams[i]->content == STREAM_CONTENT_AUDIO)) {
              /* Update pid and codec */
              demux->streams[i]->pid = GST_STM_PID_NONE;
              demux->streams[i]->codec = codec;
              /* Delete the audio device for video only stream */
              if (gst_stm_ts_demux_delete_av_device (demux, i) == FALSE) {
                GST_ERROR_OBJECT (demux, "failed to delete audio device %i", i);
              }
            }
          }
        }

        /* We force to add a subtitle track so that subtitleoverlay is always inserted
           in pipeline. Needed for channel zap, as it cannot be inserted on the fly.
         */
        if (gst_structure_get_int (structure, "ancillary-num",
                &num_ancillary) == TRUE) {
          for (i = 0; i < num_ancillary; i++) {
            codec_field = g_strdup_printf ("ancillary-codec%d", i);
            pid_field = g_strdup_printf ("ancillary-pid%d", i);
            if ((gst_structure_get_int (structure, codec_field, &codec) == TRUE)
                && (gst_structure_get_int (structure, pid_field, &pid) == TRUE)) {
              GST_DEBUG_OBJECT (demux, "pid %d codec %d\n", pid, codec);
              if (!register_stream) {
                gst_stm_ts_demux_change_stream (demux, STREAM_CONTENT_SUBTITLE,
                    pid, codec);
              } else {
                gst_stm_ts_demux_register_stream (demux,
                    STREAM_CONTENT_SUBTITLE, pid, codec, FALSE);
              }
            }
            g_free (codec_field);
            g_free (pid_field);
          }
        }

        demux->pids_set_by_event = TRUE;
        demux->pids_set_by_property = FALSE;

        if ((value =
                gst_structure_get_value (structure, "prog-duration")) != NULL) {
          demux->rec_duration = g_value_get_uint64 (value);
        }

        gst_adapter_clear (pipe->adapter);
        demux->sync_lock = FALSE;
        GST_STM_PIPE_MUTEX_UNLOCK (pipe);
        if (demux->pids_set_by_property) {
          /* post a message  to application to notify wrong usage */
          gst_element_post_message (GST_ELEMENT_CAST (demux),
              gst_message_new_element (GST_OBJECT (demux),
                  gst_structure_new_empty ("wrong-rec-play-usage")));
        }

        res = TRUE;

        /* send event to stvideo/staudio to notify if there is channel change */
        gst_stm_ts_demux_push_event (demux, event);

        /* Reset segment params */
        demux->prev_pts = 0;
        demux->strm_pos = 0;
        demux->start_pts = 0;
        demux->start_time = GST_CLOCK_TIME_NONE;

        gst_stm_ts_demux_resume_read_write_task (demux);
      } else if (gst_structure_has_name (structure, "index-support")) {
        GST_DEBUG_OBJECT (demux, "index-support");
        gst_structure_get_boolean (structure, "rap-index",
            &demux->upstream_rap_index_support);
        gst_structure_get_boolean (structure, "time-index",
            &demux->upstream_time_index_support);
        gst_stm_ts_demux_push_event (demux, event);
        res = TRUE;
        if (demux->upstream_rap_index_support
            || demux->upstream_time_index_support) {
          demux->upstream_seekable = TRUE;
          /* start of playback so reset offset */
          GstFormat format = GST_FORMAT_BYTES;
          if (gst_pad_query_position (GST_PAD_PEER (demux->sinkpad), format,
                  (gint64 *) & demux->offset) == FALSE) {
            GST_WARNING_OBJECT (demux,
                "unable to get current file position from upstream");
          }
        }
      } else if (gst_structure_has_name (structure, "pcr-data")) {
        GST_DEBUG_OBJECT (demux, "Received new pcr-data");
        res = gst_stm_ts_demux_push_pcr_event (demux, event);
      } else {
        /* forward event */
        res = gst_pad_event_default (sinkpad, parent, event);
      }
    }
      break;
    case GST_EVENT_PROTECTION:
    {
      const gchar *system_id = NULL;

      gst_event_parse_protection (event, &system_id, NULL, NULL);
      GST_DEBUG_OBJECT (demux, "Received protection event for system ID %s",
          system_id);
      if (system_id != NULL)
        gst_stm_ts_demux_append_protection_system_id (demux, system_id);
      else
        return FALSE;
      /* save the event for later, for source pads that have not been created */
      /* else send it to all pads that already exist */
      if (demux->opened == FALSE)
        g_queue_push_tail (&demux->protection_event_queue,
            gst_event_ref (event));
      else
        gst_stm_ts_demux_push_event (demux, event);
      res = TRUE;
      break;
    }
    default:
      GST_LOG_OBJECT (demux, "event not handle by sink");
      res = gst_pad_event_default (sinkpad, parent, event);
      break;
  }

  return res;
}

/*
  * Function name : gst_stm_ts_demux_append_protection_system_id
  *  Input        :
  *   demux - Gststm_ts_demux context
  *   system_id - Protection system ID
  *
  *  Description  : This function is called to add protection system
  *                 ID information
  * Return        : void
*/
static void
gst_stm_ts_demux_append_protection_system_id (Gststm_ts_demux * tsdemux,
    const gchar * system_id)
{
  gboolean existing_id = FALSE;
  gint i;

  if (!tsdemux->protection_system_ids)
    tsdemux->protection_system_ids =
        g_ptr_array_new_with_free_func ((GDestroyNotify) g_free);
  else {
    /* Check whether we already have an entry for this system ID. */
    for (i = 0; i < tsdemux->protection_system_ids->len; ++i) {
      const gchar *id = g_ptr_array_index (tsdemux->protection_system_ids, i);
      if (g_ascii_strcasecmp (system_id, id) == 0) {
        existing_id = TRUE;
        break;
      }
    }
  }
  if (!existing_id) {
    GST_DEBUG_OBJECT (tsdemux, "Adding cenc protection system ID %s",
        system_id);
    g_ptr_array_add (tsdemux->protection_system_ids, g_ascii_strdown (system_id,
            -1));
  }
}

static gboolean
gst_stm_ts_demux_src_query (GstPad * pad, GstObject * parent, GstQuery * query)
{
  Gststm_ts_demux *demux = GST_STM_TS_DEMUX (parent);
  GstFormat format;
  gint64 timeposition;
  gint64 duration = -1;
  gboolean seekable = TRUE;
  gboolean ret = FALSE;


  GST_DEBUG_OBJECT (demux, "%s - (%s)", __FUNCTION__,
      gst_query_type_get_name (GST_QUERY_TYPE (query)));

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_POSITION:
    {
      gst_query_parse_position (query, &format, NULL);

      timeposition = demux->last_pts;
      if (GST_CLOCK_TIME_IS_VALID (timeposition) == TRUE) {
        ret = TRUE;
        if (format == GST_FORMAT_TIME) {
          timeposition = gst_stm_ts_demux_pts_diff (demux);
          timeposition = gst_stm_ts_demux_time_to_gst (timeposition);
          gst_query_set_position (query, GST_FORMAT_TIME, timeposition);
        } else if (format == GST_FORMAT_DEFAULT) {
          gst_query_set_position (query, GST_FORMAT_DEFAULT, demux->last_pts);
        } else if (format == GST_FORMAT_BYTES) {
          if (GST_PAD_PEER (demux->sinkpad) != NULL) {
            ret = gst_pad_query_default (pad, parent, query);
          }
        } else {
          ret = FALSE;
        }
      }
    }
      break;

    case GST_QUERY_DURATION:
    {
      gst_query_parse_duration (query, &format, NULL);
      if (GST_CLOCK_TIME_IS_VALID (demux->duration) == TRUE) {
        ret = TRUE;
        if (format == GST_FORMAT_TIME) {
          gst_query_set_duration (query, GST_FORMAT_TIME, demux->duration);
        } else if (format == GST_FORMAT_DEFAULT) {
          gst_query_set_duration (query, GST_FORMAT_DEFAULT,
              demux->duration * 90);
        } else if (format == GST_FORMAT_BYTES) {
          if (GST_PAD_PEER (demux->sinkpad) != NULL) {
            ret = gst_pad_query_default (pad, parent, query);
          }
        } else {
          ret = FALSE;
        }
      } else {
        /* send to pads which link to this pad to query */
        ret = gst_pad_query_default (pad, parent, query);
      }
    }
      break;

    case GST_QUERY_SEEKING:
    {
      gst_query_parse_seeking (query, &format, NULL, NULL, NULL);

      if (gst_pad_query_duration (pad, format, &duration) == FALSE) {
        seekable = FALSE;
        duration = -1;
      }
      gst_query_set_seeking (query, format, seekable, 0, duration);
      ret = TRUE;
    }
      break;

    case GST_QUERY_CUSTOM:
    {
      GstStructure *structure;

      structure = gst_query_writable_structure (query);
      if (structure) {
        if (!strcmp ("reverse-support", gst_structure_get_name (structure))) {
          GST_DEBUG_OBJECT (demux, "GST_QUERY_CUSTOM: reverse-support = TRUE");
          gst_structure_set (structure, "reverse", G_TYPE_BOOLEAN, TRUE, NULL);
          ret = TRUE;
        } else {
          ret = gst_pad_query_default (pad, parent, query);
        }
      }
    }
      break;
    case GST_QUERY_SEGMENT:
    {
      if (!demux->streaming) {
        gst_query_set_segment (query, demux->segment.rate,
            GST_FORMAT_TIME, demux->segment.start, demux->segment.stop);
        ret = TRUE;
      } else {
        ret = gst_pad_peer_query (demux->sinkpad, query);
      }
      break;
    }

    default:
      /* send to pads which link to this pad to query */
      ret = gst_pad_query_default (pad, parent, query);
      GST_DEBUG_OBJECT (demux, "Query %s not supported by demux",
          gst_query_type_get_name (GST_QUERY_TYPE (query)));
      break;
  }
  return ret;
}

static gboolean
gst_stm_ts_demux_handle_src_event (GstPad * pad, GstObject * parent,
    GstEvent * event)
{
  Gststm_ts_demux *demux = GST_STM_TS_DEMUX (parent);
  gboolean res = FALSE;
  gint64 seqnum = -1;

  GST_LOG_OBJECT (demux, "gst_stm_ts_demux_handle_src_event");

  GST_DEBUG_OBJECT (demux, "%s - (%s)", __FUNCTION__,
      gst_event_type_get_name (GST_EVENT_TYPE (event)));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_SEEK:
      seqnum = (gint64) gst_event_get_seqnum (event);
      if ((seqnum == demux->prev_seek_seqnum) && (demux->prev_seek_res)) {
        GST_DEBUG_OBJECT (demux, "Same seek event received with seqnum %lld ",
            seqnum);
        gst_event_unref (event);
        return TRUE;
      }
      demux->prev_seek_seqnum = seqnum;

      if (demux->streaming == FALSE) {
        res = gst_stm_ts_handle_pull_mode_seek (demux, pad, event);
        demux->newsegment = res;
      } else {
        res = gst_stm_ts_handle_push_mode_seek (demux, pad, parent, event);
      }
      demux->seek_event = res;
      demux->prev_seek_res = res;
      gst_event_unref (event);
      break;
    case GST_EVENT_LATENCY:
    case GST_EVENT_RECONFIGURE:
    case GST_EVENT_NAVIGATION:
    case GST_EVENT_QOS:
    default:
      GST_LOG_OBJECT (demux, "event not handle by source");
      gst_pad_event_default (pad, parent, event);
      break;
  }

  return res;
}

/*
  * Function name : gst_stm_ts_demux_setup_pcr_device
  *  Input        :
  *   demux - Gststm_ts_demux context
  *
  *  Description  : This function is called to setup pcr device
  *                 pcr device set up is done only for http src
  * Return        : gboolean
*/
static gboolean
gst_stm_ts_demux_setup_pcr_device (Gststm_ts_demux * demux)
{
  GstQuery *query = NULL;
  gboolean status = FALSE;
  /* return TRUE because stream can play even if pcr_pid is not set */
  if (demux->pcr_pid == G_MAXUINT16) {
    GST_WARNING_OBJECT (demux, "pcr_pid is not set");
    return TRUE;
  }

  /* query to check the src URI */
  query = gst_query_new_uri ();
  if (query != NULL) {
    if (gst_pad_peer_query (demux->sinkpad, query) == TRUE) {
      gst_query_parse_uri (query, &demux->src_uri);
      if (demux->src_uri) {
        GST_DEBUG_OBJECT (demux, "source uri %s", demux->src_uri);
        if (!strncasecmp (demux->src_uri, "http://", 7)) {
          GST_DEBUG_OBJECT (demux, "try to set up PCR device");
          status = TRUE;
        }
      }
    }
    gst_query_unref (query);
  }

  /* do not set up pcr device if status is FALSE */
  /* return TRUE because we can still play stream */
  if (status == FALSE) {
    GST_WARNING_OBJECT (demux, "do not set up pcr device for non-http src");
    return TRUE;
  }

  /* create the demux device for PCR filtering */
  if (gst_stm_ts_demux_open_demux (demux, &demux->pcr_fd) != TRUE) {
    GST_ERROR_OBJECT (demux, "failed to create pcr demux device");
    return FALSE;
  }

  /* program the pcr device for PCR filtering */
  if (gst_stm_ts_demux_program_pcr_filter (demux) != TRUE) {
    GST_ERROR_OBJECT (demux, "failed to program pcr demux device");
    return FALSE;
  }
  return TRUE;
}

/*
  * Function name : gst_stm_ts_demux_program_pcr_filter
  *  Input        :
  *   demux - Gststm_ts_demux context
  *
  *  Description  : This function is called to program pcr device
  *
  * Return        :  gboolean
*/
static gboolean
gst_stm_ts_demux_program_pcr_filter (Gststm_ts_demux * demux)
{
  struct dmx_pes_filter_params pes_filter;

  /* Setup PCR filter */
  memset (&pes_filter, 0, sizeof (struct dmx_pes_filter_params));

  pes_filter.pid = demux->pcr_pid;
  pes_filter.pes_type = DMX_PES_PCR0;
  pes_filter.output = DMX_OUT_TAP;
  pes_filter.flags = DMX_IMMEDIATE_START;
  if (ioctl (demux->pcr_fd, DMX_SET_PES_FILTER, &pes_filter) < 0) {
    GST_ERROR_OBJECT (demux,
        "PCR filter setup ioctl failed : %s  \n", strerror (errno));
    return FALSE;
  }

  return TRUE;
}

/* Create all devices */
static gboolean
gst_stm_ts_demux_create_devices (Gststm_ts_demux * demux)
{
  gint i;

  if ((demux->dev_opened == FALSE) && (demux->num_streams > 0)) {
    /* Create the demux and AV devices */
    for (i = 0; i < demux->num_streams; i++) {
      /* Create the demux device */
      if (gst_stm_ts_demux_open_demux (demux, &(demux->streams[i]->fd)) != TRUE) {
        GST_ERROR_OBJECT (demux, "failed to create demux device %i", i);
        goto error;
      }

      /* Create the dvr device */
      if (gst_stm_ts_demux_create_dvr_device (demux) == FALSE) {
        GST_ERROR_OBJECT (demux, "failed to create dvr device");
        goto error;
      }

      if (gst_stm_ts_demux_program_demux (demux, demux->streams[i]) != TRUE) {
        GST_ERROR_OBJECT (demux, "failed to program demux device %i", i);
        goto error;
      }
    }
    /* Create the demux device for PCR filtering */
    if (gst_stm_ts_demux_setup_pcr_device (demux) != TRUE) {
      goto error;
    }
    demux->dev_opened = TRUE;
  }

  return TRUE;

error:
  gst_stm_ts_demux_delete_devices (demux);
  return FALSE;
}

static gboolean
gst_stm_ts_demux_delete_devices (Gststm_ts_demux * demux)
{
  gint i;

  if (demux->dev_opened == TRUE) {
    /* Delete demux devices */
    for (i = 0; i < demux->num_streams; i++) {
      if (gst_stm_ts_demux_close_demux (demux, demux->streams[i]) == TRUE) {
        if (gst_stm_ts_demux_delete_av_device (demux, i) == FALSE) {
          GST_ERROR_OBJECT (demux, "failed to delete audio/video device %i", i);
        }
      } else {
        GST_ERROR_OBJECT (demux, "failed to delete demux device %i", i);
      }
    }

    /* Delete pcr device */
    if (gst_stm_ts_demux_delete_pcr_device (demux) == FALSE) {
      GST_ERROR_OBJECT (demux, "failed to delete pcr device");
      return FALSE;
    }
    /* Delete dvr device */
    if (gst_stm_ts_demux_delete_dvr_device (demux) == FALSE) {
      GST_ERROR_OBJECT (demux, "failed to delete dvr device");
      return FALSE;
    }

    demux->push_index = 0;
    demux->dev_opened = FALSE;
  }

  return TRUE;
}

/* Open dvr device for writing SPTS data */
static gboolean
gst_stm_ts_demux_create_dvr_device (Gststm_ts_demux * demux)
{
  gchar *fn;

  /* Open dvr device corresponding to opened demux */
  if (demux->dvr_fd == -1) {
    /* Open dvr device */
    fn = g_strdup_printf ("/dev/dvb/adapter0/dvr%d", demux->dev_id);
    demux->dvr_fd = open (fn, O_WRONLY);
    if (demux->dvr_fd >= 0) {
      GST_INFO_OBJECT (demux, "dvr device %s opened", fn);
    } else {
      GST_ERROR_OBJECT (demux, "dvr device %s open failed", fn);
      g_free (fn);
      return FALSE;
    }
    g_free (fn);
  }

  return TRUE;
}

/*
  * Function name : gst_stm_ts_demux_delete_pcr_device
  *  Input        :
  *   demux - Gststm_ts_demux context
  *
  *  Description  : This function is called to close pcr device
  *
  * Return        :  gboolean
*/
static gboolean
gst_stm_ts_demux_delete_pcr_device (Gststm_ts_demux * demux)
{
  gint ret = 0;
  if (demux->pcr_fd >= 0) {
    ret = close (demux->pcr_fd);
    demux->pcr_fd = -1;
  }
  return (ret == 0 ? TRUE : FALSE);
}

/* Close dvr device */
static gboolean
gst_stm_ts_demux_delete_dvr_device (Gststm_ts_demux * demux)
{
  gint ret = 0;
  if (demux->dvr_fd >= 0) {
    ret = close (demux->dvr_fd);
    demux->dvr_fd = -1;
  }

  return (ret == 0 ? TRUE : FALSE);
}

/* Delete audio/video devices */
static gboolean
gst_stm_ts_demux_delete_av_device (Gststm_ts_demux * demux, gint index)
{
  Gststm_ts_stream *stream;
  GstCaps *caps;

  /* There is no parsing involve here.
     The stream content should be set by the application for now */
  if (demux->streams[index] == NULL) {
    return FALSE;
  }

  stream = demux->streams[index];

  if (stream->pad == NULL) {
    return TRUE;
  }

  if (stream->content == STREAM_CONTENT_VIDEO) {
    caps = gst_caps_new_empty_simple ("video/fake-pes");
  } else if (stream->content == STREAM_CONTENT_AUDIO) {
    caps = gst_caps_new_empty_simple ("audio/fake-pes");
  } else {
    return TRUE;
  }
  /* We do a flush here to remove all buffers in the queue,
     so that this new buffer can be received asap */
  gst_pad_push_event (stream->pad, gst_event_new_flush_start ());
  gst_pad_push_event (stream->pad, gst_event_new_flush_stop (TRUE));

  gst_pad_push_event (stream->pad, gst_event_new_caps (caps));

  gst_pad_push_event (stream->pad,
      gst_event_new_segment ((const GstSegment *) &demux->segment));
  if (!gst_stm_ts_demux_push_buf (demux, stream, NULL, caps,
          GST_CLOCK_TIME_NONE, FALSE)) {
    GST_ERROR_OBJECT (demux, "failed to push buf");
  }
  gst_caps_unref (caps);

  return TRUE;
}

static gboolean
gst_stm_ts_demux_open_demux (Gststm_ts_demux * demux, gint * fd)
{
  gint i = 0;
  gchar *fn;

  if (*fd == -1) {
    /* First step: look for dev_id to be used, for this we use exclusive mode on open */
    /* In case of HW demux, dev_id should be greater than 4, so we use this to determine
       if it is the first stream we open */
    if (demux->dev_id == -1) {
      for (i = 0; i < GST_STM_TS_DEMUX_MAX_DEVICE_NUM; i++) {
        fn = g_strdup_printf ("/dev/dvb/adapter0/demux%d", i);

        /* if dev_id==-1, it means that we have not yet identified dev_id to be used,
           so first open is done in EXCL mode to check if demux is available */
        *fd = open (fn, O_RDONLY | O_EXCL | O_NONBLOCK);
        if (*fd >= 0) {
          GST_INFO_OBJECT (demux, "demux device %s opened", fn);
          demux->dev_id = i;    /* we have found the dev_id to use, we can exit the loop */
          demux->demux_opened = TRUE;
          g_free (fn);
          break;
        }
        g_free (fn);
      }
      if (i >= GST_STM_TS_DEMUX_MAX_DEVICE_NUM) {
        GST_ERROR_OBJECT (demux, "demux device %s is out of range", fn);
        return FALSE;
      }
    } else {
      if (demux->demux_opened) {
        /* 2nd step: now we can open normally (not exclusive) for other streams, on found dev_id */
        fn = g_strdup_printf ("/dev/dvb/adapter0/demux%d", demux->dev_id);
        *fd = open (fn, O_RDONLY | O_NONBLOCK);
        if (*fd >= 0) {
          GST_INFO_OBJECT (demux, "demux device %s opened", fn);
        } else {
          goto error;
        }
        g_free (fn);
      } else {
        /* static demux device management */
        fn = g_strdup_printf ("/dev/dvb/adapter0/demux%d", demux->dev_id);
        *fd = open (fn, O_RDONLY | O_EXCL | O_NONBLOCK);
        if (*fd >= 0) {
          GST_INFO_OBJECT (demux, "demux device %s opened", fn);
          demux->demux_opened = TRUE;
          g_free (fn);
        } else {
          goto error;
        }
      }
    }
  }
  return TRUE;
error:
  GST_ERROR_OBJECT (demux, "demux device %s open failed", fn);
  g_free (fn);
  return FALSE;
}

static gboolean
gst_stm_ts_demux_close_demux (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream)
{
  if (stream->fd != -1) {
    close (stream->fd);
    stream->fd = -1;
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_stop_demux (Gststm_ts_demux * demux, Gststm_ts_stream * stream)
{
  if (stream == NULL) {
    return FALSE;
  }

  if (stream->fd == -1) {
    return FALSE;
  }

  if (ioctl (stream->fd, DMX_STOP) != 0) {
    GST_ERROR_OBJECT (demux, "demux device ioctl DMX_STOP failed %s\n",
        strerror (errno));
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_program_demux (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream)
{
  struct dmx_pes_filter_params pesfilter;

  /* fake pid used because we need to let stvideo/staudio/subtitleoverlaybin always in pipeline */
  /* so we still generate the pad whose pid is fake but not program this fake pid into demux */
  if (stream->pid & GST_STM_PID_NONE) {
    return TRUE;
  }

  memset (&pesfilter, 0, sizeof (pesfilter));
  pesfilter.input = DMX_IN_DVR;
  pesfilter.pid = stream->pid;
  pesfilter.output = DMX_OUT_TAP;

  /* Set PES type for video so that video is set as pacer */
  switch (stream->content) {
    case STREAM_CONTENT_VIDEO:
      pesfilter.pes_type = DMX_PES_VIDEO;
      break;
    case STREAM_CONTENT_AUDIO:
    case STREAM_CONTENT_SUBTITLE:
    case STREAM_CONTENT_ECM:
    case STREAM_CONTENT_OTHER:
    default:
      pesfilter.pes_type = DMX_PES_OTHER;
      break;
  }

  if (ioctl (stream->fd, DMX_SET_PES_FILTER, &pesfilter) != 0) {
    GST_ERROR_OBJECT (demux,
        "demux device ioctl DMX_SET_PES_FILTER failed: %s  \n",
        strerror (errno));
    return FALSE;
  }

  if (ioctl (stream->fd, DMX_START) < 0) {
    GST_WARNING_OBJECT (demux, "error starting PES filter %s",
        strerror (errno));
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_open (Gststm_ts_demux * demux)
{
  GstClockTime duration;
  gint i;
  guint stream_created = 0;

  GST_LOG_OBJECT (demux, "%s:%d", __FUNCTION__, __LINE__);

  if (demux->streaming == TRUE) {
    /* Specific code for stts demux handling. Stts demux needs to seek to the end of stream then before reading data for demuxing.
       Seek could fail because souphttpsrc has not yet parsed the 'Content-Range'.
       Here we try not to do anything until there is some data available in the adapter.
       This is to make sure that the souphttp has already parse the http header */
    GstPipeContext *pipe;

    pipe = &demux->pipe;
    GST_STM_PIPE_MUTEX_LOCK (pipe);
    /* Wait for the chain to start receiving data */
    while ((gst_adapter_available (pipe->adapter) == 0) && (pipe->eof == FALSE)) {
      GST_STM_PIPE_SIGNAL (pipe);
      GST_STM_PIPE_WAIT (pipe);
    }
    GST_STM_PIPE_MUTEX_UNLOCK (pipe);
  }

  if (gst_stm_ts_demux_parse_stream_info (demux) == FALSE) {
    if (demux->filesize != -1) {
      return FALSE;
    }
  }
  demux->offset = demux->streamoffset;

  if (demux->num_streams == 0) {
    if (gst_stm_ts_demux_parse_table (demux) == FALSE) {
      return FALSE;
    }
  }
  demux->table_id = GST_STM_TS_INVALID_TABLE;

  /* Create a source pad for each stream */
  for (i = 0; i < demux->num_streams; i++) {
    if (gst_stm_ts_demux_create_stream (demux, i) == TRUE) {
      stream_created++;
    }
  }
  gst_element_no_more_pads (GST_ELEMENT (demux));

  if (stream_created == 0) {
    return FALSE;
  }

  if (demux->rec_duration == GST_CLOCK_TIME_NONE
      && demux->duration == GST_CLOCK_TIME_NONE) {
    /* Get duration */
    if (gst_stm_ts_demux_get_duration (demux, &duration)) {
      demux->duration = duration;
    }
  } else if (demux->duration == GST_CLOCK_TIME_NONE) {
    demux->duration = demux->rec_duration;
  }

  demux->segment.duration = demux->duration;

  /* Push new segment event to the pad */
  gst_stm_ts_demux_push_event (demux,
      gst_event_new_segment ((const GstSegment *) &demux->segment));

  if (demux->enable_playback == TRUE) {
    gst_stm_ts_demux_create_devices (demux);
  }

  GST_OBJECT_LOCK (demux);
  demux->sync_lock = FALSE;
  demux->opened = TRUE;
  GST_OBJECT_UNLOCK (demux);

  return TRUE;
}

static void
gst_stm_ts_demux_close (Gststm_ts_demux * demux)
{
  gint i;

  if (demux->opened == FALSE) {
    return;
  }

  /* Remove the source pad and free streams */
  for (i = 0; i < GST_STM_TS_DEMUX_MAX_STREAMS; i++) {
    gst_stm_ts_demux_delete_stream (demux, i);
  }

  /* Delete pcr device */
  if (gst_stm_ts_demux_delete_pcr_device (demux) == FALSE) {
    GST_ERROR_OBJECT (demux, "failed to delete pcr device");
    return;
  }
  /* Delete dvr device */
  if (gst_stm_ts_demux_delete_dvr_device (demux) == FALSE) {
    GST_ERROR_OBJECT (demux, "failed to delete dvr device");
    return;
  }
  /* Delete Protection System ID Array */
  if (demux->protection_system_ids) {
    g_ptr_array_free (demux->protection_system_ids, TRUE);
    demux->protection_system_ids = NULL;
  }
  /* Clear Protection Event Queue */
  g_queue_foreach (&demux->protection_event_queue, (GFunc) gst_event_unref,
      NULL);
  g_queue_clear (&demux->protection_event_queue);

  return;
}

static void
gst_stm_ts_demux_write_loop (GstPad * pad)
{
  GstFlowReturn res = GST_FLOW_OK;
  Gststm_ts_demux *demux = GST_STM_TS_DEMUX (GST_PAD_PARENT (pad));
  GstPipeContext *pipe = &demux->pipe;
  GstBuffer *buf = NULL;
  guint8 *data = NULL;
  guint size;
  guint read_size = 0;
  gint written = 0;
  guint start_offset = 0, end_offset = 0;
  guint packet_size, num_packets, bytes_to_write;
  GstMapInfo info_read;

  if (demux->opened == FALSE) {
    if (gst_stm_ts_demux_open (demux) == FALSE) {
      res = GST_FLOW_ERROR;
      goto pause;
    } else {
      gst_task_start (demux->read_task);
    }
  }

  if (gst_stm_ts_demux_handle_create_delete_av_devices (demux) == FALSE) {
    res = GST_FLOW_ERROR;
    goto pause;
  }

  if (demux->running == FALSE) {
    return;
  }

  /* Device not open, do nothing */
  if (demux->dev_opened == FALSE) {
    /* Clear adapter */
    if (gst_adapter_available (pipe->adapter) > 0) {
      GST_STM_PIPE_MUTEX_LOCK (pipe);
      gst_adapter_clear (pipe->adapter);
      demux->sync_lock = FALSE;
      GST_STM_PIPE_MUTEX_UNLOCK (pipe);
    }
    /* Signal the chain */
    GST_STM_PIPE_SIGNAL (pipe);
    /* For now we sleep for 100ms, we should pause the task  */
    /* and re-start the task again if playback starts        */
    usleep (100 * 1000);
    return;
  }

  if ((demux->segment.rate > 0) || (demux->upstream_rap_index_support == TRUE)) {
    /* Read data */
    if (demux->streaming == FALSE) {
      res = gst_pad_pull_range (pad, demux->offset, BUFFER_SIZE, &buf);
      if (res != GST_FLOW_OK) {
        GST_WARNING_OBJECT (demux,
            "failed to pull data %s from %" G_GUINT64_FORMAT,
            gst_flow_get_name (res), demux->offset);
        GST_OBJECT_LOCK (demux);
        /* Pause appropriately based on if we are flushing or not during seek case */
        if (demux->flushing) {
          res = GST_FLOW_FLUSHING;
        } else {
          GST_DEBUG_OBJECT (demux, "end of stream");
          res = GST_FLOW_EOS;
        }
        GST_OBJECT_UNLOCK (demux);
        goto pause;
      }
    } else {
      read_size = demux->packetsize * 4 + 5;
      if (gst_stm_ts_demux_read (demux, &buf, GST_STM_TS_DEMUX_NO_SEEK,
              read_size, TRUE) == FALSE) {
        GST_WARNING_OBJECT (demux, "push mode read failed");
        GST_OBJECT_LOCK (demux);
        if (demux->flushing) {
          res = GST_FLOW_FLUSHING;
        } else if (!demux->need_unlock) {
          GST_DEBUG_OBJECT (demux, "end of stream");
          res = GST_FLOW_EOS;
        }
        GST_OBJECT_UNLOCK (demux);
        goto pause;
      }
    }
    gst_buffer_map (buf, &info_read, GST_MAP_READ);
    size = info_read.size;
    data = info_read.data;
    GST_LOG_OBJECT (demux, "%u bytes were read", size);

    /* Align the data read with ts packet boundary,
       because streams may have only a part of ts packet at the end.
     */
    if (demux->streaming == FALSE) {
      size -= size % demux->packetsize;
      if (size == 0) {
        GST_WARNING_OBJECT (demux,
            "failed to pull data %s from %" G_GUINT64_FORMAT,
            gst_flow_get_name (res), demux->offset);
        GST_OBJECT_LOCK (demux);
        /* Pause appropriately based on if we are flushing or not during seek case */
        if (demux->flushing) {
          res = GST_FLOW_FLUSHING;
        } else {
          GST_DEBUG_OBJECT (demux, "end of stream");
          res = GST_FLOW_EOS;
        }
        GST_OBJECT_UNLOCK (demux);
        gst_buffer_unmap (buf, &info_read);
        gst_buffer_unref (buf);
        goto pause;
      }
    }

    /* we are in forward motion or DVR (forward/backward in which injection is
       controlled by strecorder) input data can be from push src (dvr, dvb, ip, http)
       or pull src like file in case of push data can be non aligned or corrupted
       like in http streaming through vlc or may a corrupt file in case of pull mode
       TE expects 0x47 byte aligned valid ts packets (for 188 byte packets and for 192 bytes
       packet, 4 bytes are timestamp before Sync byte) so stts_demux will validate data
       for 0x47 byte (in case of 192 byte ts packet this byte will be after 4 bytes (timestamp)
       of start of packet) and filter out unwanted data (like rtp header in case of rtp streaming)
     */

    if (demux->num_streams != 0) {
      /* write available valid ts packets to dvr device */
      end_offset = 0;
      start_offset = 0;

      while (end_offset + demux->packetsize <= size) {
        /* Find consecutive valid packets */
        if (gst_stm_ts_demux_scan_valid_packets (demux, data + end_offset,
                size - end_offset, &packet_size, &start_offset,
                &num_packets, demux->sync_lock) == FALSE) {
          bytes_to_write = 0;
          end_offset += start_offset;
          if (0 == end_offset) {
            end_offset = size;
            GST_WARNING_OBJECT (demux, "No valid Packet Found");
          }
          break;
        } else {
          /* Calculate valid packets */
          bytes_to_write = num_packets * packet_size;
          /* Jump to new valid packet offset */
          end_offset += start_offset;
          demux->packetsize = packet_size;
        }

        /* If we have some packets to inject */
        if (bytes_to_write > 0) {
          /* For trick mode (file playback), we need to get the PTS */
          gst_stm_ts_demux_get_pts (demux, buf, (guint64) end_offset);
          /* Write data to dvr device */
          written = write (demux->dvr_fd, (data + end_offset), bytes_to_write);
          if (written < 0) {
            GST_WARNING_OBJECT (demux, "Write FAILED - %s", strerror (errno));
            gst_buffer_unmap (buf, &info_read);
            gst_buffer_unref (buf);
            res = GST_FLOW_ERROR;
            goto pause;
          } else if (written != bytes_to_write) {
            GST_WARNING_OBJECT (demux,
                "tried to write %d but only managed to write %d",
                bytes_to_write, written);
          }
          end_offset += written;
        }
      }
      if (demux->segment.rate >= 0) {
        demux->offset += end_offset;
      } else if ((demux->offset >= end_offset)
          && (demux->upstream_rap_index_support
              || demux->upstream_time_index_support)) {
        demux->offset -= end_offset;
      }

      if (demux->streaming == TRUE) {
        GST_STM_PIPE_MUTEX_LOCK (pipe);
        if (demux->upstream_rap_index_support
            || demux->upstream_time_index_support) {
          if (gst_adapter_available (pipe->adapter) < end_offset) {
            end_offset = gst_adapter_available (pipe->adapter);
          }
        }
        gst_adapter_flush (pipe->adapter, end_offset);
        GST_STM_PIPE_MUTEX_UNLOCK (pipe);
        if (pipe->eof == TRUE) {
          gst_buffer_unmap (buf, &info_read);
          gst_buffer_unref (buf);
          res = GST_FLOW_EOS;
          goto pause;
        }
      }

    }
    gst_buffer_unmap (buf, &info_read);
    gst_buffer_unref (buf);
  } else {
    if (demux->seeking == TRUE) {
      res = GST_FLOW_FLUSHING;
      goto pause;
    }
    /* Check for completion of previous injection */
    if (demux->disc_read == TRUE) {
      if (gst_stm_ts_demux_reverse (demux) == FALSE) {
        /* Pause appropriately based on if we are flushing or not during seek case */
        if (demux->flushing) {
          res = GST_FLOW_FLUSHING;
        } else {
          GST_DEBUG_OBJECT (demux, "end of stream");
          res = GST_FLOW_EOS;
        }
        GST_OBJECT_UNLOCK (demux);
        goto pause;
      }
    } else {
      /* Sleep for 1ms to reduce cpu load */
      usleep (1000);
    }
  }

  return;

pause:

  GST_DEBUG_OBJECT (demux, "Pausing task, reason %s", gst_flow_get_name (res));
  demux->running = FALSE;

  gst_stm_ts_demux_pause_write_task (demux);
  if (demux->streaming == TRUE) {
    /* Push mode to pause the task */
    GstPipeContext *pipe = &demux->pipe;

    GST_STM_PIPE_MUTEX_LOCK (pipe);
    demux->pipe.srcresult = res;
    GST_STM_PIPE_MUTEX_UNLOCK (pipe);
  }

  /* Fatal errors */
  if (res == GST_FLOW_FLUSHING) {
    GST_DEBUG_OBJECT (demux, "seek flush?");
  } else if (res == GST_FLOW_EOS) {
    GST_DEBUG_OBJECT (demux, "pushing end of streaming event");
    demux->eos = TRUE;
    demux->eos_event_sent = FALSE;
    if ((demux->segment.rate < 0) && (demux->upstream_rap_index_support
            || demux->upstream_time_index_support)) {
      demux->offset = 0;
    }
  } else if ((res == GST_FLOW_NOT_LINKED) || (res < GST_FLOW_EOS)) {
    GST_ERROR ("internal data stream error streaming stopped reason %s",
        gst_flow_get_name (res));
    gst_stm_ts_demux_push_event (demux, gst_event_new_eos ());
    GST_ELEMENT_ERROR (demux, STREAM, FAILED, ("Internal data stream error."),
        ("streaming stopped, reason %s", gst_flow_get_name (res)));
  }

  return;
}

/*
  * Function name : gst_stm_ts_demux_send_pcr_data
  *  Input        :
  *   demux - Gststm_ts_demux context
  *
  *  Description  : This function reads the pcr data from pcr device
  *                 and send a pcr data event downstream
  * Return        : gboolean
*/
static gboolean
gst_stm_ts_demux_send_pcr_data (Gststm_ts_demux * demux)
{
  struct dmx_pcr pcr;
  gboolean result = FALSE;
  gint bytes_read = 0;

  if (demux->pcr_fd < 0) {
    return FALSE;
  }

  bytes_read = read (demux->pcr_fd, &pcr, sizeof (struct dmx_pcr));

  if (bytes_read == sizeof (struct dmx_pcr)) {
    GstEvent *event;
    GstStructure *structure;
    GValue source_time;
    GValue system_time;

    memset (&source_time, 0, sizeof (GValue));
    memset (&system_time, 0, sizeof (GValue));
    g_value_init (&source_time, G_TYPE_UINT64);
    g_value_init (&system_time, G_TYPE_UINT64);

    g_value_set_uint64 (&source_time, pcr.pcr);
    g_value_set_uint64 (&system_time, pcr.system_time);

    structure = gst_structure_new_empty ("pcr-data");

    if (structure) {
      gst_structure_set_value (structure, "source-time", &source_time);
      gst_structure_set_value (structure, "system-time", &system_time);
      event = gst_event_new_custom (GST_EVENT_CUSTOM_DOWNSTREAM_OOB, structure);
      if (event) {
        GST_DEBUG_OBJECT (demux,
            "pushing pcr-data event (pcr=%llu, system_time=%llu", pcr.pcr,
            pcr.system_time);
        result = gst_stm_ts_demux_push_pcr_event (demux, event);
      } else {
        GST_WARNING_OBJECT (demux, "failed to create pcr-data event");
        gst_structure_free (structure);
      }
    } else {
      GST_WARNING_OBJECT (demux, "failed to create pcr-data structure");
    }
  }
  return result;
}

/*
  * Function name : gst_stm_ts_demux_push_pcr_event
  *  Input        :
  *   demux - Gststm_ts_demux context
  *
  *  Description  : This function try to send a pcr data event to
  *                 first available audio / video decoder.
  *                 This event is handled by one of stvideo or staudio
  *                 decoders. The goal is to ensure that Streaming Engine
  *                 receives PCR data asap.
  * Return        : gboolean
*/
static gboolean
gst_stm_ts_demux_push_pcr_event (Gststm_ts_demux * demux, GstEvent * event)
{
  Gststm_ts_stream *stream;
  gboolean result = FALSE;
  gint i;

  for (i = 0; i < demux->num_streams; i++) {
    stream = demux->streams[i];
    if ((stream != NULL) && (stream->pad != NULL)) {
      if ((stream->content == STREAM_CONTENT_VIDEO)
          || (stream->content == STREAM_CONTENT_AUDIO)) {
        GST_DEBUG_OBJECT (demux, "send pcr-data to pad %s", stream->padname);
        gst_event_ref (event);  /* In case we need to send it to another stream */
        if (gst_pad_push_event (stream->pad, event) != TRUE) {
          GST_DEBUG_OBJECT (demux, "%s did not handle pcr-data event",
              stream->padname);
        } else {
          GST_DEBUG_OBJECT (demux, "%s handled pcr-data event, break",
              stream->padname);
          /* Only one element among all downstream elements needs to receive pcr-data */
          result = TRUE;
          break;
        }
      } else {
        GST_DEBUG_OBJECT (demux,
            "Not a valid stream to forward pcr-data event");
        continue;               /* try with next stream */
      }
    } else {
      GST_DEBUG_OBJECT (demux,
          "stream[%d] is not ready yet to push pcr-data event", i);
      continue;                 /* try with next stream */
    }
  }

  if (i == demux->num_streams) {
    /* No element could handle the pcr-data event, we drop it */
    GST_WARNING_OBJECT (demux,
        "Could not find a stream to handle PCR data, DROPPING pcr-data event");
  }
  /* we need to unref the event */
  gst_event_unref (event);

  return result;
}

/*
 Function: gst_stm_ts_demux_read_loop
 Input params:
           pad - sink pad
 description :
 * Read loop task. Reads PES packets from hardware demuxer
 * and injects asap to downstream to avoid downstream
 * packets starvation and injection bottleneck.
 * The task is not voluntarily sleeping except blocking poll
 * with 1ms timeout and when the downstream queue is
 * full of buffers. It will inject PES packets as fast
 * as possible.
 *
 * IMP: For loaded system with equal & lesser thread priorities,
 * the task pre-emption points/wait may be revisited
 *   if scheduler quantum is too high
 *   if downstream comsumer is fast
 Return :
 */
static void
gst_stm_ts_demux_read_loop (GstPad * pad)
{
  Gststm_ts_demux *demux = GST_STM_TS_DEMUX (GST_PAD_PARENT (pad));
  Gststm_ts_stream *stream;
  struct pollfd pfd[demux->num_streams + 1];    // GST_STM_TS_PCR_PKT = 1;
  gint i, nfds = demux->num_streams;

  if ((demux->opened == TRUE) && (demux->dev_opened == TRUE)) {
    /* Setup to poll all demux */
    memset (pfd, 0, sizeof (pfd));
    for (i = 0; i < demux->num_streams; i++) {
      stream = demux->streams[i];
      if ((stream) && (stream->pid != GST_STM_PID_NONE)) {
        pfd[i].fd = stream->fd;
        pfd[i].events = POLLIN;
        pfd[i].revents = 0;
      }
    }

    if ((demux->pcr_pid != G_MAXUINT16)
        && (demux->pcr_fd != -1)) {
      nfds++;
      /* location i = demux->num_streams is for pcr-pid filter */
      i = demux->num_streams;
      pfd[i].fd = demux->pcr_fd;
      pfd[i].events = POLLIN;
      pfd[i].revents = 0;
    }

    if (poll (pfd, nfds, TIMEOUT_MS)) {
      /* Check for event from all filters before reading */
      /* index for polling pcr-pid filter is demux->num_streams */
      if (pfd[demux->num_streams].revents & POLLIN) {
        if (gst_stm_ts_demux_send_pcr_data (demux) == FALSE) {
          GST_WARNING_OBJECT (demux, "failed to send pcr data");
        }
      }

      for (i = (demux->num_streams - 1); i >= 0; i--) {
        stream = demux->streams[i];
        if (pfd[i].revents & POLLIN) {
          if (stream == NULL) {
            GST_WARNING_OBJECT (demux, "invalid params");
            return;
          }
          if (stream->content == STREAM_CONTENT_ECM) {
            if (gst_stm_ts_demux_read_ecm_data (demux, stream) == FALSE) {
              GST_WARNING_OBJECT (demux, "failed to send ecm data");
              return;
            }
          } else {
            if (stream->is_protected == FALSE) {
              if (gst_stm_ts_demux_read_send_pes (demux, stream,
                      ((i == demux->ref_index) ? TRUE : FALSE),
                      FALSE) == TRUE) {
                stream->first_buffer_sent = TRUE;
              }
            } else {
              if (gst_stm_ts_demux_read_parse_pes_protected (demux, stream,
                      ((i == demux->ref_index) ? TRUE : FALSE),
                      FALSE) == TRUE) {
                stream->first_buffer_sent = TRUE;
              }
            }
          }
        } else if ((stream != NULL) && (stream->pad != NULL)) {
          /* if no pes data has been received for a particular Pid,
             send 1 dummy packet to downstream to make the pipeline preroll */
          if (stream->first_buffer_sent == FALSE) {
            /* Push new segment event to the pad */
            if (demux->start_time == GST_CLOCK_TIME_NONE)
              gst_pad_push_event (stream->pad,
                  gst_event_new_segment ((const GstSegment *) &demux->segment));
            if (gst_stm_ts_demux_push_buf (demux, stream, NULL, NULL,
                    GST_CLOCK_TIME_NONE, FALSE) == TRUE)
              stream->first_buffer_sent = TRUE;
          }
        }
      }
    } else if ((demux->eos == TRUE) && (demux->eos_event_sent == FALSE)) {
      demux->no_pes_count++;
      /* Wait for some time before sending eos event,
         this is to avoid very small stream from ending immediately */
      if (demux->no_pes_count > 10) {
        gst_stm_ts_demux_push_event (demux, gst_event_new_eos ());
        demux->eos_event_sent = TRUE;
      }
    } else {
      /* No data Received from TE and not EOS */
      /* push a dummy if not yet done */
      for (i = (demux->num_streams - 1); i >= 0; i--) {
        stream = demux->streams[i];
        if ((stream != NULL) && (stream->pad != NULL)) {
          if (stream->first_buffer_sent == FALSE) {
            if (demux->start_time == GST_CLOCK_TIME_NONE)
              /* Push new segment event to the pad */
              gst_pad_push_event (stream->pad,
                  gst_event_new_segment ((const GstSegment *) &demux->segment));
            if (gst_stm_ts_demux_push_buf (demux, stream, NULL, NULL,
                    GST_CLOCK_TIME_NONE, FALSE) == TRUE)
              stream->first_buffer_sent = TRUE;
          }
        }
      }
    }
  } else {
    /* demuxer and devices not yet opened */
    sched_yield ();
  }
  return;
}

static GstBuffer *
gst_stm_ts_demux_read_get_buffer (Gststm_ts_stream * stream)
{
  GstBuffer *buf = NULL;
  struct dmx_ctrl dmxctrl;
  guint read_size = 0, buf_size = 0;
  GstMapInfo info_write;

  /* get the available size from dmx */
  dmxctrl.id = DMX_CTRL_OUTPUT_BUFFER_STATUS;

  if (stream->content == STREAM_CONTENT_VIDEO) {
    dmxctrl.pes_type = DMX_PES_VIDEO;
  } else {
    dmxctrl.pes_type = DMX_PES_OTHER;
  }

  if (ioctl (stream->fd, DMX_GET_CTRL, &dmxctrl) != 0)
    buf_size = BUFFER_SIZE;
  else {
    buf_size = dmxctrl.value;
  }

  buf_size = MIN (buf_size, BUFFER_SIZE);

  buf = gst_buffer_new_allocate (NULL, buf_size, NULL);
  if (buf != NULL) {
    gst_buffer_map (buf, &info_write, GST_MAP_WRITE);

    read_size = read (stream->fd, info_write.data, buf_size);
    if (read_size <= 0) {
      gst_buffer_unmap (buf, &info_write);
      gst_buffer_unref (buf);
      return NULL;
    }

    gst_buffer_unmap (buf, &info_write);
    gst_buffer_set_size (buf, read_size);
  }
  return buf;
}

static gboolean
gst_stm_ts_demux_read_send_pes (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream, gboolean ref_index, gboolean drop_pes)
{
  GstBuffer *buf;
  gint read_size = 0;
  guint64 pts = GST_CLOCK_TIME_NONE;
  gboolean discontinue;
  GstMapInfo info_write;

  if (stream == NULL) {
    GST_WARNING_OBJECT (demux, "invalid params");
    return FALSE;
  }

  if (stream->pad) {
    demux->no_pes_count = 0;

    /* get the buffer from dmx */
    buf = gst_stm_ts_demux_read_get_buffer (stream);
    if (buf != NULL) {
      discontinue = FALSE;
      gst_buffer_map (buf, &info_write, GST_MAP_WRITE);

      read_size = info_write.size;

      if ((demux->segment.rate < 0)
          || ((demux->segment.rate > 1.0)
              && (demux->upstream_rap_index_support == TRUE))) {
        if (ref_index == TRUE) {
          guint8 *data;
          data = info_write.data;
          if (data[read_size - 8] == 0xde && data[read_size - 7] == 0xad
              && data[read_size - 6] == 0xbe
              && data[read_size - 5] == 0xef
              && data[read_size - 4] == 0xbe
              && data[read_size - 3] == 0xef
              && data[read_size - 2] == 0xde && data[read_size - 1] == 0xad) {
            /* Sometimes the dummy pes can be smaller than 184 bytes */
            gst_buffer_unmap (buf, &info_write);
            if (read_size <
                (GST_STM_TS_DEMUX_TS_PACKET_SIZE -
                    GST_STM_TS_DEMUX_TS_HEADER_SIZE)) {
              gst_buffer_set_size (buf, 0);
            } else {
              gst_buffer_set_size (buf,
                  read_size - (GST_STM_TS_DEMUX_TS_PACKET_SIZE -
                      GST_STM_TS_DEMUX_TS_HEADER_SIZE));
            }
            discontinue = TRUE;
            gst_buffer_map (buf, &info_write, GST_MAP_WRITE);
          }
        }
      }

      if (info_write.size > GST_STM_PES_PTS_SCAN_SIZE) {
        guint8 *pes_header;
        guint i;

        pts = GST_CLOCK_TIME_NONE;
        for (i = 0; i < (info_write.size - GST_STM_PES_PTS_SCAN_SIZE); i++) {
          pes_header = info_write.data + i;
          if (stream->stream_id == 0) {
            stream->stream_id = pes_header[3];
          }
          if (stream->stream_id == pes_header[3]) {
            if (gst_stm_ts_demux_extract_pts (pes_header, &pts) == TRUE) {
              if ((pes_header[6] & 0xF0) != 0x80) {
                pts = GST_CLOCK_TIME_NONE;
              }
            }
          }
          if (GST_CLOCK_TIME_IS_VALID (pts)) {
            pts = MPEGTIME_TO_GSTTIME (pts);
            break;
          }
        }
      }

      gst_buffer_unmap (buf, &info_write);

      if (drop_pes == TRUE) {
        gst_buffer_unref (buf);
      } else {
        if ((discontinue == TRUE) || (stream->discontinue == TRUE)) {
          GST_BUFFER_FLAG_SET (buf, GST_BUFFER_FLAG_DISCONT);
          GST_BUFFER_FLAG_SET (buf, GST_BUFFER_FLAG_DELTA_UNIT);
          if (stream->discontinue == TRUE) {
            gst_buffer_set_size (buf, 0);
          }
          stream->discontinue = FALSE;
        }

        if (gst_stm_ts_demux_push_buf (demux, stream, buf, NULL, pts,
                ref_index) == FALSE) {
          GST_WARNING_OBJECT (demux, "failed to push buffer downstream");
          return FALSE;
        }
        if (discontinue == TRUE) {
          demux->disc_read = TRUE;
        }
      }
    }
  }
  return TRUE;
}

/*
  * Function name : gst_stm_ts_demux_read_ecm_data
  *  Input        :
  *   demux - Gststm_ts_demux context
  *   stream - stream in the context
  *
  *  Description  : This function parses ECM packets
  *                 from stream adapter and forward it
  *                 to extract SENC inofrmation from
  *                 ECM packet.
  * Return        : gboolean
*/
static gboolean
gst_stm_ts_demux_read_ecm_data (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream)
{
  GstBuffer *buf = NULL;
  GstBuffer *send_buf = NULL;

  /* get the buffer from dmx */
  buf = gst_stm_ts_demux_read_get_buffer (stream);
  if (buf != NULL) {
    gst_adapter_push (stream->adapter, buf);

    while (1) {
      if (stream->header_required == TRUE) {
        if ((gst_adapter_available (stream->adapter)) >=
            GST_STM_PES_HEADER_SIZE) {
          memset (stream->pes_header, 0, GST_STM_PES_HEADER_SIZE);
          gst_adapter_copy (stream->adapter, stream->pes_header, 0,
              GST_STM_PES_HEADER_SIZE);
          if ((stream->pes_header[0] == 0x00) && (stream->pes_header[1] == 0x00)
              && (stream->pes_header[2] == 0x01)) {
            stream->pes_packet_size =
                ((stream->pes_header[4] << 8) | (stream->pes_header[5])) +
                GST_STM_PES_HEADER_SIZE;
            stream->header_required = FALSE;
          } else {
            if (gst_adapter_available (stream->adapter) >=
                GST_STM_PES_HEADER_SIZE) {
              gst_adapter_flush (stream->adapter, GST_STM_PES_HEADER_SIZE);
            }
            GST_WARNING_OBJECT (demux, "Wrong PES Header");
            return FALSE;
          }
        } else {
          return TRUE;
        }
      }

      if (gst_adapter_available (stream->adapter) >= stream->pes_packet_size) {
        send_buf =
            gst_adapter_take_buffer (stream->adapter, stream->pes_packet_size);
        if (send_buf == NULL) {
          GST_WARNING_OBJECT (demux, "failed to get buffer");
          return FALSE;
        }
        gst_stm_ts_demux_update_stream_ecm_queue (demux, send_buf);
        stream->header_required = TRUE;
        send_buf = NULL;
      } else {
        return TRUE;
      }
    }
  }
  return TRUE;
}

/*
  * Function name : gst_stm_ts_demux_update_stream_ecm_queue
  *  Input        :
  *   demux - Gststm_ts_demux context
  *   buf - pes ecm buffer
  *
  *  Description  : This function extracts SENC inofrmation
  *                 for the stream from ecm packet and queue
  *                 it in the stream ecm queue.
  * Return        :
*/
static void
gst_stm_ts_demux_update_stream_ecm_queue (Gststm_ts_demux * demux,
    GstBuffer * buf)
{
  guint8 *data = NULL;
  GstMapInfo info_read;
  guint32 i = 0;
  guint32 pes_counter = 0;
  guint32 sample_counter = 0;
  guint32 subsample_counter = 0;
  guint32 scrambled_stream_counter = 0;
  guint32 byte_count = 0;

  gst_buffer_map (buf, &info_read, GST_MAP_READ);
  data = info_read.data;

  if ((data[byte_count] == 0x00) && (data[byte_count + 1] == 0x00)
      && (data[byte_count + 2] == 0x01)) {
    GstStm_ts_ECM_PES_Packet *local_ecm_packet =
        g_malloc0 (sizeof (GstStm_ts_ECM_PES_Packet));

    /* PARSING */
    byte_count += 4;
    local_ecm_packet->pes_packet_length = data[byte_count++];
    local_ecm_packet->pes_packet_length =
        local_ecm_packet->pes_packet_length << 8;
    local_ecm_packet->pes_packet_length =
        local_ecm_packet->pes_packet_length | data[byte_count++];
    local_ecm_packet->pssh_count = data[byte_count++] & 0x0F;
    /* FIXME : Need to Parse PSSH Data */

    local_ecm_packet->scrambled_stream_count = data[byte_count++];
    if (local_ecm_packet->scrambled_stream_count > 0) {
      scrambled_stream_counter = 0;
      for (scrambled_stream_counter = 0;
          scrambled_stream_counter < local_ecm_packet->scrambled_stream_count;
          scrambled_stream_counter++) {
        gboolean stream_ecm_updated = FALSE;
        GstStm_ts_SENC_ECM_Data *temp_senc_data =
            g_malloc0 (sizeof (GstStm_ts_SENC_ECM_Data));
        temp_senc_data->scrambling_control = (data[byte_count] & 0xC0) >> 6;
        temp_senc_data->stream_params_included = (data[byte_count] & 0x20) >> 5;
        temp_senc_data->scrambled_es_pid = (data[byte_count++] & 0x1F) << 8;
        temp_senc_data->scrambled_es_pid =
            temp_senc_data->scrambled_es_pid | data[byte_count++];
        if (temp_senc_data->stream_params_included) {
          temp_senc_data->algorithm_ID =
              (data[byte_count] << 16) | (data[byte_count +
                  1] << 8) | (data[byte_count + 2]);
          byte_count += 3;
          temp_senc_data->iv_size = data[byte_count++];
          memcpy (temp_senc_data->kid, &(data[byte_count]), 16);
          byte_count += 16;
        } else {
          /* Use last SENC_ECM_Data values for stream parameters */
          for (i = 0; i < demux->num_streams; i++) {
            if (demux->streams[i]->pid == temp_senc_data->scrambled_es_pid) {
              temp_senc_data->algorithm_ID =
                  demux->streams[i]->ecm_data->algorithm_ID;
              temp_senc_data->iv_size = demux->streams[i]->ecm_data->iv_size;
              memcpy (temp_senc_data->kid, demux->streams[i]->ecm_data->kid,
                  16);
              break;
            }
          }
        }

        temp_senc_data->pes_count = data[byte_count++];
        if (temp_senc_data->pes_count > 0) {
          temp_senc_data->senc_pes_data =
              g_malloc0 (sizeof (GstStm_ts_SENC_ECM_Pes_Data) *
              temp_senc_data->pes_count);
          for (pes_counter = 0; pes_counter < temp_senc_data->pes_count;
              pes_counter++) {
            /* Parse PTS Timestamp: */
            temp_senc_data->senc_pes_data[pes_counter].pts_timestamp =
                (guint64) ((data[byte_count] << 24) | (data[byte_count +
                        1] << 16) | (data[byte_count +
                        2] << 8) | (data[byte_count + 3]));
            byte_count += 4;
            temp_senc_data->senc_pes_data[pes_counter].pts_timestamp =
                temp_senc_data->senc_pes_data[pes_counter].pts_timestamp << 1;
            temp_senc_data->senc_pes_data[pes_counter].pts_timestamp =
                temp_senc_data->
                senc_pes_data[pes_counter].pts_timestamp | ((data[byte_count]
                    & 0x80) >> 7);

            temp_senc_data->senc_pes_data[pes_counter].sample_count =
                (data[byte_count++] & 0x7F);

            if (temp_senc_data->senc_pes_data[pes_counter].sample_count > 0) {
              temp_senc_data->senc_pes_data[pes_counter].senc_pes_sample_data =
                  g_malloc0 (sizeof (GstStm_ts_SENC_ECM_Pes_Sample_Data) *
                  temp_senc_data->senc_pes_data[pes_counter].sample_count);
              for (sample_counter = 0;
                  sample_counter <
                  temp_senc_data->senc_pes_data[pes_counter].sample_count;
                  sample_counter++) {
                temp_senc_data->senc_pes_data[pes_counter].senc_pes_sample_data
                    [sample_counter].initialization_vector =
                    g_malloc0 (1 * temp_senc_data->iv_size);
                memcpy (temp_senc_data->
                    senc_pes_data[pes_counter].senc_pes_sample_data
                    [sample_counter].initialization_vector, &(data[byte_count]),
                    temp_senc_data->iv_size);
                byte_count += temp_senc_data->iv_size;
                temp_senc_data->senc_pes_data[pes_counter].senc_pes_sample_data
                    [sample_counter].subsamples_count =
                    (data[byte_count] << 8) | (data[byte_count + 1]);
                byte_count += 2;

                if (temp_senc_data->
                    senc_pes_data[pes_counter].senc_pes_sample_data
                    [sample_counter].subsamples_count > 0) {
                  temp_senc_data->
                      senc_pes_data[pes_counter].senc_pes_sample_data
                      [sample_counter].senc_pes_subsample_data =
                      g_malloc0 (sizeof (GstStm_ts_SENC_ECM_Pes_Subsample_Data)
                      *
                      temp_senc_data->
                      senc_pes_data[pes_counter].senc_pes_sample_data
                      [sample_counter].subsamples_count);
                  for (subsample_counter = 0;
                      subsample_counter <
                      temp_senc_data->
                      senc_pes_data[pes_counter].senc_pes_sample_data
                      [sample_counter].subsamples_count; subsample_counter++) {
                    temp_senc_data->
                        senc_pes_data[pes_counter].senc_pes_sample_data
                        [sample_counter].senc_pes_subsample_data
                        [subsample_counter].bytes_clear_data =
                        (data[byte_count] << 8) | (data[byte_count + 1]);
                    byte_count += 2;
                    temp_senc_data->
                        senc_pes_data[pes_counter].senc_pes_sample_data
                        [sample_counter].senc_pes_subsample_data
                        [subsample_counter].bytes_encrypted_data =
                        (data[byte_count] << 24) | (data[byte_count +
                            1] << 16) | (data[byte_count +
                            2] << 8) | (data[byte_count + 3]);
                    byte_count += 4;
                  }
                }
              }
            }
          }
        }
        for (i = 0; i < demux->num_streams; i++) {
          if (demux->streams[i]->pid == temp_senc_data->scrambled_es_pid) {
            demux->streams[i]->is_protected = TRUE;
            g_queue_push_tail (&(demux->streams[i]->ecm_queue), temp_senc_data);
            stream_ecm_updated = TRUE;
            break;
          }
        }
        if (!stream_ecm_updated) {
          gst_stm_ts_demux_ecm_free (temp_senc_data);
        }
      }
    }
    g_free (local_ecm_packet);
  }
  gst_buffer_unmap (buf, &info_read);
  gst_buffer_unref (buf);
  return;
}

/*
  * Function name : gst_stm_ts_demux_update_stream_ecm_data
  *  Input        :
  *   stream - Gststm_ts_stream in context
  *
  *  Description  : This function pops data from the stream
  *                 ecm queue and update the stream current
  *                 ecm parameters
  * Return        : gboolean
*/
static gboolean
gst_stm_ts_demux_update_stream_ecm_data (Gststm_ts_stream * stream)
{
  gboolean ret = TRUE;
  GstStm_ts_SENC_ECM_Data *temp = NULL;
  GstStm_ts_SENC_ECM_Data *temp_ecm_data =
      g_queue_pop_head (&stream->ecm_queue);

  if (temp_ecm_data == NULL)
    return FALSE;

  if (stream->pid == temp_ecm_data->scrambled_es_pid) {
    temp = stream->ecm_data;
    stream->ecm_data = temp_ecm_data;
    stream->pes_counter = 0;
    stream->scrambled_pes_count = stream->ecm_data->pes_count;
  } else {
    temp = temp_ecm_data;
    ret = FALSE;
  }

  gst_stm_ts_demux_ecm_free (temp);

  return ret;
}

/*
  * Function name : gst_stm_ts_demux_get_stream_protection_metadata
  *  Input        :
  *   stream - Gststm_ts_stream in context
  *   pts - pts timestamp
  *
  *  Description  : This function retrieves protection metadata
  *                 (iv, subsamples) from the stream current ecm data
  *                 and wraps in a GstStructure to attached with the
  *                 buffer.
  * Return        : GstStructure*
*/
static GstStructure *
gst_stm_ts_demux_get_stream_protection_metadata (Gststm_ts_stream * stream,
    guint64 pts)
{
  GValue iv_value = { 0 };
  GValue subsample_value = { 0 };
  guint32 subsample_counter = 0;
  guint32 byte_count = 0;
  guint16 clearBE = 0;
  guint32 encryBE = 0;
  guint8 *iv_mem = NULL;
  guint32 pts_diff = 0;

  if (stream->ecm_data->senc_pes_data[stream->pes_counter].pts_timestamp >= pts)
    pts_diff =
        stream->ecm_data->senc_pes_data[stream->pes_counter].pts_timestamp -
        pts;
  else
    pts_diff =
        pts -
        stream->ecm_data->senc_pes_data[stream->pes_counter].pts_timestamp;

  if (pts_diff > GST_STM_PTS_VARIANCE) {
    GST_ERROR_OBJECT (stream, "PTS not matched %lld %lld", pts,
        stream->ecm_data->senc_pes_data[stream->pes_counter].pts_timestamp);
    return NULL;
  }

  iv_mem = g_malloc0 (stream->ecm_data->iv_size);
  if (iv_mem == NULL) {
    return NULL;
  }

  memcpy (iv_mem,
      stream->ecm_data->senc_pes_data[stream->
          pes_counter].senc_pes_sample_data[0].initialization_vector,
      stream->ecm_data->iv_size);

  GstBuffer *iv_buffer =
      gst_buffer_new_wrapped (iv_mem, stream->ecm_data->iv_size);
  g_value_init (&iv_value, GST_TYPE_BUFFER);
  gst_value_set_buffer (&iv_value, iv_buffer);

  guint8 *sub_buffer =
      g_malloc0 (stream->ecm_data->senc_pes_data[stream->
          pes_counter].senc_pes_sample_data[0].subsamples_count * 6);
  for (subsample_counter = 0;
      subsample_counter <
      stream->ecm_data->senc_pes_data[stream->
          pes_counter].senc_pes_sample_data[0].subsamples_count;
      subsample_counter++) {
    /* To add PES header bytes in clear data byte count */
    if (subsample_counter == 0) {
      stream->ecm_data->senc_pes_data[stream->
          pes_counter].senc_pes_sample_data[0].
          senc_pes_subsample_data[subsample_counter].bytes_clear_data =
          stream->ecm_data->senc_pes_data[stream->
          pes_counter].senc_pes_sample_data[0].
          senc_pes_subsample_data[subsample_counter].bytes_clear_data +
          (stream->pes_packet_size -
          (stream->ecm_data->senc_pes_data[stream->
                  pes_counter].senc_pes_sample_data[0].
              senc_pes_subsample_data[subsample_counter].bytes_clear_data +
              stream->ecm_data->senc_pes_data[stream->
                  pes_counter].senc_pes_sample_data[0].
              senc_pes_subsample_data[subsample_counter].bytes_encrypted_data));
    }

    clearBE =
        ((stream->ecm_data->senc_pes_data[stream->
                pes_counter].senc_pes_sample_data[0].
            senc_pes_subsample_data[subsample_counter].bytes_clear_data) << 8) |
        ((stream->ecm_data->senc_pes_data[stream->pes_counter].
            senc_pes_sample_data[0].senc_pes_subsample_data[subsample_counter].
            bytes_clear_data) >> 8);

    memcpy (&(sub_buffer[byte_count]), &(clearBE), 2);
    byte_count += 2;

    encryBE =
        ((stream->ecm_data->senc_pes_data[stream->
                pes_counter].senc_pes_sample_data[0].
            senc_pes_subsample_data[subsample_counter].bytes_encrypted_data >>
            24) & 0xff) | ((stream->ecm_data->senc_pes_data[stream->
                pes_counter].
            senc_pes_sample_data[0].senc_pes_subsample_data
            [subsample_counter].bytes_encrypted_data << 8) & 0xff0000) |
        ((stream->ecm_data->senc_pes_data[stream->pes_counter].
            senc_pes_sample_data[0].senc_pes_subsample_data
            [subsample_counter].bytes_encrypted_data >> 8) & 0xff00) |
        ((stream->ecm_data->senc_pes_data[stream->pes_counter].
            senc_pes_sample_data[0].senc_pes_subsample_data
            [subsample_counter].bytes_encrypted_data << 24) & 0xff000000);

    memcpy (&(sub_buffer[byte_count]), &(encryBE), 4);
    byte_count += 4;
  }

  GstBuffer *subsample_buffer = gst_buffer_new_wrapped (sub_buffer,
      (stream->ecm_data->senc_pes_data[stream->
              pes_counter].senc_pes_sample_data[0].subsamples_count * 6));
  g_value_init (&subsample_value, GST_TYPE_BUFFER);
  gst_value_set_buffer (&subsample_value, subsample_buffer);

  GstStructure *ecm_struct = gst_structure_new ("application/x-cenc",
      "iv_size", G_TYPE_UINT, stream->ecm_data->iv_size,
      "encrypted", G_TYPE_BOOLEAN, TRUE,
      "subsample_count", G_TYPE_UINT,
      stream->ecm_data->senc_pes_data[stream->pes_counter].
      senc_pes_sample_data[0].subsamples_count,
      NULL);

  gst_structure_set_value (ecm_struct, "iv", &iv_value);
  gst_structure_set_value (ecm_struct, "subsamples", &subsample_value);

  return ecm_struct;
}

/*
  * Function name : gst_stm_ts_demux_read_parse_pes_protected
  *  Input        :
  *   demux - Gststm_ts_demux context
  *   stream - Gststm_ts_stream in context
  *   ref_index - reference index
  *   drop_pes - drop pes control
  *
  *  Description  : This function parses the pes packets from
  *                 protected stream.
  * Return        : gboolean
*/
static gboolean
gst_stm_ts_demux_read_parse_pes_protected (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream, gboolean ref_index, gboolean drop_pes)
{
  GstBuffer *buf;
  GstBuffer *send_buf;
  if (stream == NULL) {
    GST_WARNING_OBJECT (demux, "invalid params");
    return FALSE;
  }

  if (stream->pad) {
    demux->no_pes_count = 0;

    /* get the available buffer from dmx */
    buf = gst_stm_ts_demux_read_get_buffer (stream);
    if (buf != NULL) {
      gst_adapter_push (stream->adapter, buf);

      while (1) {
        if (stream->pes_counter >= stream->scrambled_pes_count) {
          if (!gst_stm_ts_demux_update_stream_ecm_data (stream)) {
            return TRUE;
          }
        }
        while (stream->pes_counter < stream->scrambled_pes_count) {
          if (stream->header_required == TRUE) {
            if ((gst_adapter_available (stream->adapter)) >=
                GST_STM_PES_HEADER_SIZE) {
              if (stream->zero_pes == FALSE) {
                memset (stream->pes_header, 0, GST_STM_PES_HEADER_SIZE);
                gst_adapter_copy (stream->adapter, stream->pes_header, 0,
                    GST_STM_PES_HEADER_SIZE);
                if ((stream->pes_header[0] == 0x00)
                    && (stream->pes_header[1] == 0x00)
                    && (stream->pes_header[2] == 0x01)) {
                  stream->pes_packet_size =
                      ((stream->pes_header[4] << 8) | (stream->pes_header[5])) +
                      GST_STM_PES_HEADER_SIZE;
                  if (stream->pes_packet_size == GST_STM_PES_HEADER_SIZE) {
                    gst_adapter_copy (stream->adapter, stream->zero_header, 0,
                        4);
                    stream->zero_pes_size = 4;
                    stream->zero_pes = TRUE;
                  } else {
                    stream->header_required = FALSE;
                  }
                } else {
                  if (gst_adapter_available (stream->adapter) >=
                      GST_STM_PES_HEADER_SIZE) {
                    gst_adapter_flush (stream->adapter,
                        GST_STM_PES_HEADER_SIZE);
                  }
                  GST_WARNING_OBJECT (demux, "Wrong PES Header");
                  return FALSE;
                }
              }
            } else {
              return TRUE;
            }

            if (stream->zero_pes == TRUE) {
              while (1) {
                guint8 temp_value = 0;
                if ((gst_adapter_available (stream->adapter)) <
                    (stream->zero_pes_size + 1)) {
                  return TRUE;
                }
                gst_adapter_copy (stream->adapter, &temp_value,
                    stream->zero_pes_size, 1);
                stream->zero_header[0] = stream->zero_header[1];
                stream->zero_header[1] = stream->zero_header[2];
                stream->zero_header[2] = stream->zero_header[3];
                stream->zero_header[3] = temp_value;
                stream->zero_pes_size++;

                /* Currently checking only for Video as only Video can have zero pes size in header */
                if ((stream->zero_header[0] == 0x00)
                    && (stream->zero_header[1] == 0x00)
                    && (stream->zero_header[2] == 0x01)
                    && (stream->zero_header[3] == 0xe0)) {
                  stream->pes_packet_size = stream->zero_pes_size - 4;
                  stream->zero_pes_size = 0;
                  stream->header_required = FALSE;
                  stream->zero_pes = FALSE;
                  break;
                }
              }
            }
          }

          if (gst_adapter_available (stream->adapter) >=
              stream->pes_packet_size) {
            send_buf =
                gst_adapter_take_buffer (stream->adapter,
                stream->pes_packet_size);
            if (send_buf == NULL) {
              GST_WARNING_OBJECT (demux, "failed to gst buffer");
              return FALSE;
            }

            gst_stm_ts_demux_read_send_pes_protected (demux, stream, ref_index,
                drop_pes, send_buf);
            stream->header_required = TRUE;
            send_buf = NULL;
          } else {
            return TRUE;
          }
          stream->pes_counter++;
        }
      }
    }
  }
  return TRUE;
}

/*
  * Function name : gst_stm_ts_demux_read_send_pes_protected
  *  Input        :
  *   demux - Gststm_ts_demux context
  *   stream - Gststm_ts_stream in context
  *   ref_index - reference index
  *   drop_pes - drop pes control
  *   buf - pes buffer
  *
  *  Description  : This function send the pes packets with metadata
  *                 to downstream.
  * Return        : gboolean
*/
static gboolean
gst_stm_ts_demux_read_send_pes_protected (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream, gboolean ref_index, gboolean drop_pes,
    GstBuffer * buf)
{
  GstMapInfo info_read;
  guint64 pts = GST_CLOCK_TIME_NONE;
  guint8 *reset_scrambled_header;
  guint64 protected_pts = GST_CLOCK_TIME_NONE;

  gst_buffer_map (buf, &info_read, GST_MAP_READ);

  if (info_read.size > GST_STM_PES_PTS_SCAN_SIZE) {
    guint8 *pes_header;
    guint i;

    pts = GST_CLOCK_TIME_NONE;
    for (i = 0; i < (info_read.size - GST_STM_PES_PTS_SCAN_SIZE); i++) {
      pes_header = info_read.data + i;
      if (stream->stream_id == 0) {
        stream->stream_id = pes_header[3];
      }
      if (stream->stream_id == pes_header[3]) {
        if (gst_stm_ts_demux_extract_pts (pes_header, &pts) == TRUE) {
          if ((pes_header[6] & 0xF0) != 0x80) {
            pts = GST_CLOCK_TIME_NONE;
          }
        }
      }
      if (GST_CLOCK_TIME_IS_VALID (pts)) {
        pts = MPEGTIME_TO_GSTTIME (pts);
        break;
      }
    }
  }
  /* Reset Scrambled Bit in PES Header */
  reset_scrambled_header = info_read.data;
  reset_scrambled_header[6] = reset_scrambled_header[6] & 0xCF;

  if (gst_stm_ts_demux_extract_pts (reset_scrambled_header,
          &protected_pts) != TRUE) {
    GST_WARNING_OBJECT (demux, "failed to get pes pts");
  }

  gst_buffer_unmap (buf, &info_read);

  if (drop_pes == TRUE) {
    gst_buffer_unref (buf);
  } else {
    GstStructure *ecm_struct =
        gst_stm_ts_demux_get_stream_protection_metadata (stream, protected_pts);

    if (!gst_buffer_add_protection_meta (buf, ecm_struct)) {
      GST_ERROR_OBJECT (demux, "failed to attach ecm metadata");
      return FALSE;
    }

    if (gst_stm_ts_demux_push_buf (demux, stream, buf, NULL, pts,
            ref_index) == FALSE) {
      GST_WARNING_OBJECT (demux, "failed to push buffer downstream");
      return FALSE;
    }
  }
  return TRUE;
}

/*
  * Function name : gst_stm_ts_demux_read_send_pes_protected
  *  Input        :
  *   demux - Gststm_ts_demux context
  *
  *  Description  : This function frees the ecm data and rest
  *                 the ecm parameters associated with the  stream.
  * Return        : gboolean
*/
static gboolean
gst_stm_ts_demux_protected_stream_flush (Gststm_ts_demux * demux)
{
  guint i = 0;
  guint pes_counter = 0;
  guint sample_counter = 0;

  for (i = 0; i < demux->num_streams; i++) {
    if (demux->streams[i]->is_protected) {
      if (demux->streams[i]->ecm_data != NULL) {
        if (demux->streams[i]->ecm_data->senc_pes_data != NULL) {
          for (pes_counter = 0;
              pes_counter < demux->streams[i]->ecm_data->pes_count;
              pes_counter++) {
            if (demux->streams[i]->ecm_data->
                senc_pes_data[pes_counter].senc_pes_sample_data != NULL) {
              for (sample_counter = 0;
                  sample_counter <
                  demux->streams[i]->ecm_data->
                  senc_pes_data[pes_counter].sample_count; sample_counter++) {
                if (demux->streams[i]->ecm_data->
                    senc_pes_data[pes_counter].senc_pes_sample_data
                    [sample_counter].initialization_vector != NULL) {
                  g_free (demux->streams[i]->ecm_data->
                      senc_pes_data[pes_counter].senc_pes_sample_data
                      [sample_counter].initialization_vector);
                }
                if (demux->streams[i]->ecm_data->
                    senc_pes_data[pes_counter].senc_pes_sample_data
                    [sample_counter].senc_pes_subsample_data != NULL) {
                  g_free (demux->streams[i]->ecm_data->
                      senc_pes_data[pes_counter].senc_pes_sample_data
                      [sample_counter].senc_pes_subsample_data);
                }
              }
            }
            g_free (demux->streams[i]->ecm_data->
                senc_pes_data[pes_counter].senc_pes_sample_data);
          }
          g_free (demux->streams[i]->ecm_data->senc_pes_data);
        }
        g_free (demux->streams[i]->ecm_data);
      }
      demux->streams[i]->ecm_data = NULL;
      gst_adapter_clear (demux->streams[i]->adapter);

      while (!g_queue_is_empty (&(demux->streams[i]->ecm_queue))) {
        GstStm_ts_SENC_ECM_Data *temp_senc_data =
            g_queue_pop_head (&(demux->streams[i]->ecm_queue));
        gst_stm_ts_demux_ecm_free (temp_senc_data);
      }
      g_queue_clear (&(demux->streams[i]->ecm_queue));
      demux->streams[i]->header_required = TRUE;
      demux->streams[i]->pes_packet_size = 0;
      memset (demux->streams[i]->pes_header, 0, GST_STM_PES_HEADER_SIZE);
      demux->streams[i]->pes_counter = 0;
      demux->streams[i]->scrambled_pes_count = 0;
      demux->streams[i]->zero_pes_size = 0;
      demux->streams[i]->zero_pes = FALSE;
    }
  }
  return TRUE;
}

/*
  * Function name : gst_stm_ts_demux_ecm_free
  *  Input        :
  *   senc_ecm_data - ecm data to be freed
  *
  *  Description  : This function frees the ecm data associated
  *                 with the  stream.
  * Return        :
*/
static void
gst_stm_ts_demux_ecm_free (GstStm_ts_SENC_ECM_Data * senc_ecm_data)
{
  guint pes_counter = 0;
  guint sample_counter = 0;

  if (senc_ecm_data != NULL) {
    if (senc_ecm_data->senc_pes_data != NULL) {
      for (pes_counter = 0; pes_counter < senc_ecm_data->pes_count;
          pes_counter++) {
        if (senc_ecm_data->senc_pes_data[pes_counter].senc_pes_sample_data !=
            NULL) {
          for (sample_counter = 0;
              sample_counter <
              senc_ecm_data->senc_pes_data[pes_counter].sample_count;
              sample_counter++) {
            if (senc_ecm_data->
                senc_pes_data[pes_counter].senc_pes_sample_data[sample_counter].
                initialization_vector != NULL) {
              g_free (senc_ecm_data->
                  senc_pes_data[pes_counter].senc_pes_sample_data
                  [sample_counter].initialization_vector);
            }
            if (senc_ecm_data->
                senc_pes_data[pes_counter].senc_pes_sample_data[sample_counter].
                senc_pes_subsample_data != NULL) {
              g_free (senc_ecm_data->
                  senc_pes_data[pes_counter].senc_pes_sample_data
                  [sample_counter].senc_pes_subsample_data);
            }
          }
        }
        g_free (senc_ecm_data->senc_pes_data[pes_counter].senc_pes_sample_data);
      }
      g_free (senc_ecm_data->senc_pes_data);
    }
    g_free (senc_ecm_data);
  }
}

/*
  * Function name : gst_stm_ts_demux_check_ptswrap
  *  Input        :
  *   demux - Gststm_ts_demux context
  *   timestamp - current buffer timestamp
  * Description  : this function detects if there is a PTS wrap
  *  in current buffer timestamp.
  *
  * Return        : gboolean (ptswrap TRUE or FALSE)
*/
static gboolean
gst_stm_ts_demux_check_ptswrap (Gststm_ts_demux * demux, guint64 timestamp)
{
  gboolean ptswrap = FALSE;
  gint64 pts_diff = 0;
  gint64 time_diff = 0;

  if (demux->seek_event == TRUE) {
    if (timestamp < demux->start_time) {
      ptswrap = TRUE;
      demux->start_time = timestamp;
    } else {
      pts_diff = (gint64) timestamp - (gint64) demux->start_time;
      time_diff = (gint64) demux->segment.start - (gint64) timestamp;

      if ((pts_diff < demux->segment.time)
          && (time_diff > GST_STM_TS_DEMUX_SEEKPOS_TIMEDIFF_DONTCARE)) {
        ptswrap = TRUE;
        GST_DEBUG_OBJECT (demux,
            "pts wrap case: pts_diff = %" GST_TIME_FORMAT " time_diff = %"
            GST_TIME_FORMAT, GST_TIME_ARGS (pts_diff),
            GST_TIME_ARGS (time_diff));
      }
    }
  } else {
    if (timestamp < demux->start_time) {
      ptswrap = TRUE;
      demux->start_time = timestamp;
    } else if (timestamp < demux->prev_pts) {
      pts_diff = (gint64) demux->prev_pts - (gint64) timestamp;
      if (pts_diff > GST_STM_TS_DEMUX_PLAYBACK_PTSDIFF_DONTCARE) {
        GST_DEBUG_OBJECT (demux, "pts wrap case, pts_diff = %lld",
            GST_TIME_AS_SECONDS (pts_diff));
        ptswrap = TRUE;
      }
    }
  }

  return ptswrap;
}

/*
  * Function name : gst_stm_ts_demux_stream_position
  *  Input        :
  *   demux - Gststm_ts_demux context
  *   timestamp - current buffer timestamp
  *   ptswrap   - if ptswrap is TRUE or FALSE
  * Description  : this function computes the stream position
  * corresponding to a buffer timestamp.
  * it is called after user seek or ptswrap.
  *
  * Return        : gboolean (ptswrap TRUE or FALSE)
*/
static void
gst_stm_ts_demux_stream_position (Gststm_ts_demux * demux, guint64 timestamp,
    gboolean ptswrap)
{

  if (ptswrap == TRUE) {
    if (demux->seek_event == TRUE) {
      /* ptswrap detected during user seek operation
       * finding stream position is tricky here.
       * user seek jump value is only info we can use (so far). */
      demux->strm_pos = demux->segment.time;
    } else {
      /* we can't do anything here in demux, strm_pos stays intact */
    }
  } else {
    /* we should be here because of user seek only
     * e.g. for segments having no ptswrap */
    demux->strm_pos = (gint64) timestamp - (gint64) demux->start_pts;
  }
  GST_DEBUG_OBJECT (demux, "new stream position = %" GST_TIME_FORMAT,
      GST_TIME_ARGS (demux->strm_pos));
  return;
}

static gboolean
gst_stm_ts_demux_push_buf (Gststm_ts_demux * demux, Gststm_ts_stream * stream,
    GstBuffer * buf, GstCaps * caps, guint64 timestamp, gboolean ref_index)
{
  GstFlowReturn ret = GST_FLOW_OK;

  if (stream == NULL) {
    GST_WARNING_OBJECT (demux, "no stream available, cannot push buffer");
    if (buf) {
      gst_buffer_unref (buf);
    }
    return FALSE;
  } else if (stream->pad == NULL) {
    GST_WARNING_OBJECT (demux, "no pad for this stream, cannot push buffer");
    if (buf) {
      gst_buffer_unref (buf);
    }
    return FALSE;
  }

  if (buf == NULL) {
    buf = gst_buffer_new ();
    if (buf == NULL) {
      GST_ERROR_OBJECT (demux, "failed to allocate new buffer");
      return FALSE;
    }

    if (stream->is_protected == TRUE) {
      GstStructure *ecm_struct1 = gst_structure_new ("application/x-cenc",
          "iv_size", G_TYPE_UINT, 0,
          "encrypted", G_TYPE_BOOLEAN, FALSE, NULL);

      if (!gst_buffer_add_protection_meta (buf, ecm_struct1)) {
        GST_ERROR_OBJECT (demux, "failed to attach cenc metadata 1 to buffer");
      }
    }

  }

  if (GST_CLOCK_TIME_IS_VALID (timestamp)) {
    gboolean ptswrap = FALSE;
    if (!GST_CLOCK_TIME_IS_VALID (demux->start_time)) {
      /* start_time may be modifed during ptswrap detection
       * see function gst_stm_ts_demux_check_ptswrap() */
      demux->start_time = timestamp;

      /* start_pts should not be modified after this,
       * except timeshift playback, cf. ts sink query handler
       * it is used in stream position computation */
      demux->start_pts = timestamp;
      demux->newsegment = TRUE;
      ptswrap = TRUE;
    } else {
      /* detection of ptswrap and update of start_time (if needed) */
      /* no ptswrap detection in reverse trickmode (but we should do it) */
      if ((demux->segment.rate > 0.0) && ((ref_index == TRUE)
              || (demux->seek_event == TRUE))
          && (((!demux->src_uri) || (demux->src_uri
                      && strncasecmp (demux->src_uri, "http://", 7))))) {
        ptswrap = gst_stm_ts_demux_check_ptswrap (demux, timestamp);
      }
    }
    if ((demux->newsegment == TRUE) || (ptswrap == TRUE)) {
      GST_DEBUG_OBJECT (demux, "newsegment = %s, ptswrap = %s",
          ((demux->newsegment == TRUE) ? "TRUE" : "FALSE"),
          ((ptswrap == TRUE) ? "TRUE" : "FALSE"));
      gst_stm_ts_demux_stream_position (demux, timestamp, ptswrap);
      if (demux->segment.rate > 0.0) {
        if (ptswrap == TRUE) {
          demux->segment.start = (gint64) timestamp;
          demux->segment.time = demux->strm_pos;
        }
        demux->segment.stop = GST_CLOCK_TIME_NONE;
      } else {
        demux->segment.start = (gint64) demux->start_pts;
        demux->segment.time = 0;
        demux->segment.stop =
            (gint64) timestamp + (gint64) GST_STM_TS_DEMUX_RTM_INJECTION_TIME;
      }
      if (ptswrap == TRUE) {
        gboolean res = FALSE;
        res = gst_stm_ts_demux_push_ptswrap_event (demux);
        if (res == FALSE) {
          GST_WARNING_OBJECT (demux, "failed to push pts-wrap event\n");
        }
      }
      demux->segment.position = demux->strm_pos;
      demux->newsegment = FALSE;
      GST_DEBUG_OBJECT (demux, "sending new segment: rate %f "
          "start %" GST_TIME_FORMAT " stop %" GST_TIME_FORMAT
          " position %" GST_TIME_FORMAT, demux->segment.rate,
          GST_TIME_ARGS (demux->segment.start),
          GST_TIME_ARGS (demux->segment.stop),
          GST_TIME_ARGS (demux->segment.time));
      /* Push new segment event to the pad */
      gst_stm_ts_demux_push_event (demux,
          gst_event_new_segment ((const GstSegment *) &demux->segment));
    }
    demux->strm_pos =
        demux->segment.time + (gint64) timestamp - demux->segment.start;
    GST_LOG_OBJECT (demux, "stream position = %" GST_TIME_FORMAT,
        GST_TIME_ARGS (demux->strm_pos));
    /* segment.position is also modified during seek operation */
    demux->segment.position = demux->strm_pos;
    if ((ref_index == TRUE) || (demux->seek_event == TRUE)) {
      demux->prev_pts = timestamp;
    }
    demux->seek_event = FALSE;
    /* update stream's prev_pts value */
    stream->prev_pts = timestamp;
  }

  /* update stream specific prev_pts with demux prev_pts if no valid
     timestamp has been received yet for stream */
  if (!GST_CLOCK_TIME_IS_VALID (stream->prev_pts) && demux->prev_pts) {
    stream->prev_pts = demux->prev_pts;
  }

  if (!GST_CLOCK_TIME_IS_VALID (timestamp)) {
    /* if timestamp is invalid, assign prev_pts so that we dont send
       invalid or 0 timestamps
     */
    timestamp = stream->prev_pts;
  }

  GST_BUFFER_TIMESTAMP (buf) = timestamp;
  GST_BUFFER_DURATION (buf) = GST_CLOCK_TIME_NONE;

  GST_LOG_OBJECT (demux, "pushing buffer on pad %s with ts %llu",
      stream->padname, timestamp);
  if ((ret = gst_pad_push (stream->pad, buf)) != GST_FLOW_OK) {
    GST_DEBUG_OBJECT (demux, "failed to push buffer on pad %s - %s",
        stream->padname, gst_flow_get_name (ret));
    /* Do not exit here, first check for combined flow errors */
  }

  /* Check if there was a fatal error */
  demux->srcresult = gst_stm_ts_combine_flows (demux, stream, ret);

  if (demux->srcresult != GST_FLOW_OK) {
    if (!gst_task_pause (demux->read_task)) {
      GST_ERROR_OBJECT (demux, "failed to pause the read task");
    }
    if ((demux->srcresult < GST_FLOW_EOS)
        || (demux->srcresult == GST_FLOW_NOT_LINKED)) {
      GST_ELEMENT_ERROR (demux, STREAM, FAILED,
          ("Internal data stream error."), ("streaming stopped, reason %s",
              gst_flow_get_name (demux->srcresult)));
    } else if (demux->srcresult == GST_FLOW_EOS) {
      gst_stm_ts_demux_push_event (demux, gst_event_new_eos ());
    }
    GST_ERROR_OBJECT (demux, "flow error detected - %s",
        gst_flow_get_name (ret));
    return FALSE;
  }

  return TRUE;
}

static void
gst_stm_ts_demux_reset (Gststm_ts_demux * demux)
{
  GST_LOG_OBJECT (demux, "%s:%d", __FUNCTION__, __LINE__);

  demux->packetsize = BD_TP_PACKET_SIZE;        /* for now default */
  demux->vpads_num = 0;
  demux->apads_num = 0;
  demux->streaming = FALSE;
  demux->opened = FALSE;
  demux->dev_opened = FALSE;
  demux->seeking = FALSE;
  demux->internal_seek = FALSE;
  demux->enable_playback = TRUE;
  demux->dvr_fd = -1;
  demux->demux_opened = FALSE;
  demux->num_streams = 0;
  demux->prog_num = 0;
  demux->push_index = 0;
  demux->ref_index = 0xffffffff;
  demux->offset = 0;
  demux->first_offset = 0;
  demux->last_offset = 0;
  demux->packetoffset = 0;
  demux->filesize = 0;
  demux->first_pts = INVALID_PTS_VALUE;
  demux->last_pts = INVALID_PTS_VALUE;
  demux->flushing = FALSE;
  demux->start_time = GST_CLOCK_TIME_NONE;
  demux->start_pts = GST_CLOCK_TIME_NONE;
  demux->duration = GST_CLOCK_TIME_NONE;
  demux->rec_duration = GST_CLOCK_TIME_NONE;
  demux->eos = FALSE;
  demux->eos_event_sent = FALSE;
  demux->disc_read = TRUE;
  demux->upstream_rap_index_support = FALSE;
  demux->upstream_time_index_support = FALSE;
  demux->upstream_seekable = FALSE;
  demux->prev_seek_res = FALSE;
  demux->prev_seek_seqnum = -1;
  demux->srcresult = GST_FLOW_OK;
  demux->pids_set_by_event = FALSE;
  demux->pids_set_by_property = FALSE;
  demux->live = FALSE;
  demux->sync_lock = FALSE;
  demux->pcr_fd = -1;
  demux->pcr_pid = G_MAXUINT16;
  demux->requested_program_number = -1;
  memset (demux->streams, 0, sizeof (demux->streams));

  demux->pipe.needed = BUFFER_SIZE;

  demux->newsegment = FALSE;
  demux->seek_event = FALSE;
  demux->prev_pts = 0;
  demux->strm_pos = 0;
  demux->table_id = GST_STM_TS_INVALID_TABLE;
  demux->have_group_id = FALSE;
  demux->group_id = G_MAXUINT;
  demux->need_unlock = FALSE;
  demux->protection_system_ids = NULL;
  demux->src_uri = NULL;
  /* Init gst segment */
  gst_segment_init (&demux->segment, GST_FORMAT_TIME);
  return;
}

static gboolean
gst_stm_ts_demux_add_lang_tag (Gststm_ts_stream * stream)
{
  gboolean ret = FALSE;
  gchar *lang_code = NULL;

  if (stream->tag_list) {
    lang_code = g_strdup_printf ("%u", stream->pid);
    gst_tag_list_add (stream->tag_list, GST_TAG_MERGE_REPLACE,
        GST_TAG_LANGUAGE_CODE, lang_code, NULL);
    ret = TRUE;
  }
  return ret;
}

static gboolean
gst_stm_ts_demux_create_taglist (Gststm_ts_stream * stream)
{
  GstTagList *tag_list = NULL;
  gboolean ret = FALSE;

  tag_list = gst_tag_list_new_empty ();
  if (tag_list) {
    stream->tag_list = tag_list;
    gst_stm_ts_demux_add_lang_tag (stream);
    ret = TRUE;
  }
  return ret;
}

static gboolean
gst_stm_ts_demux_create_stream (Gststm_ts_demux * demux, gint index)
{
  GstPad *pad = NULL;
  GstCaps *caps = NULL;
  Gststm_ts_stream *stream = NULL;
  GstEvent *stream_start = NULL;
  gchar *stream_id = NULL;

  GST_LOG_OBJECT (demux, "%s:%d", __FUNCTION__, __LINE__);

  /* There is no parsing involve here.
     The stream content should be set by the application for now */
  if (demux->streams[index] == NULL) {
    return FALSE;
  }


  stream = demux->streams[index];

  switch (stream->content) {
    case STREAM_CONTENT_VIDEO:
#if MULTIPLE_AV_STREAM_WA
      /* Create the video pad */
      if (demux->vpads_num >= 1) {
        return FALSE;
      }
#endif
      stream->padname = g_strdup_printf ("video_%02d", demux->vpads_num);
      pad =
          gst_pad_new_from_static_template (&video_src_pad_template,
          stream->padname);
      demux->vpads_num++;
      /* Always overwrite this if we have a video stream */
      demux->ref_index = index;
      if (stream->is_protected) {
        stream->adapter = gst_adapter_new ();
        g_queue_init (&stream->ecm_queue);
      }
      break;

    case STREAM_CONTENT_AUDIO:
      /* Create the audio pad */
      stream->padname = g_strdup_printf ("audio_%02d", demux->apads_num);
      pad =
          gst_pad_new_from_static_template (&audio_src_pad_template,
          stream->padname);
      demux->apads_num++;
      if (demux->ref_index == 0xffffffff) {
        demux->ref_index = index;
      }
      if (!stream->tag_list) {
        /* create and update language code in taglist */
        if (!gst_stm_ts_demux_create_taglist (stream)) {
          GST_ERROR_OBJECT (demux, "Failed to create tag list for pad name %s",
              stream->padname);
          goto exit_error;
        }
      }
      if (stream->is_protected) {
        stream->adapter = gst_adapter_new ();
        g_queue_init (&stream->ecm_queue);
      }
      break;

    case STREAM_CONTENT_SUBTITLE:
      /* Create the subtitle pad */
      stream->padname = g_strdup_printf ("subtitle_%02d", demux->spads_num);
      pad =
          gst_pad_new_from_static_template (&subtitle_src_pad_template,
          stream->padname);
      demux->spads_num++;
      if (demux->ref_index == 0xffffffff) {
        demux->ref_index = index;
      }
      if (!stream->tag_list) {
        /* create and update language code in taglist */
        if (!gst_stm_ts_demux_create_taglist (stream)) {
          GST_ERROR_OBJECT (demux, "Failed to create tag list for pad name %s",
              stream->padname);
          goto exit_error;
        }
      }
      if (stream->is_protected) {
        stream->adapter = gst_adapter_new ();
        g_queue_init (&stream->ecm_queue);
      }
      break;

    case STREAM_CONTENT_ECM:
      stream->adapter = gst_adapter_new ();
      stream->pad = NULL;
      return TRUE;

    default:
      GST_WARNING_OBJECT (demux, "Unknown pad content %d", stream->content);
      return FALSE;
  }

  GST_INFO_OBJECT (demux, "new pad named %s added for stream[%d]",
      stream->padname, index);

  /* Map caps */
  if (demux->enable_playback == FALSE) {
    if (stream->content == STREAM_CONTENT_VIDEO) {
      caps = gst_caps_new_empty_simple ("video/fake-pes");
    } else if (stream->content == STREAM_CONTENT_AUDIO) {
      caps = gst_caps_new_empty_simple ("audio/fake-pes");
    } else {
      caps = gst_stm_ts_demux_map_caps (demux, index);
    }
  } else {
    caps = gst_stm_ts_demux_map_caps (demux, index);
  }
  if (caps == NULL) {
    GST_WARNING_OBJECT (demux, "Unknown caps for stream %d", index);
    goto exit_error;
  }

  /* Set src pad query and event function */
  gst_pad_set_query_function (pad,
      GST_DEBUG_FUNCPTR (gst_stm_ts_demux_src_query));
  gst_pad_set_event_function (pad,
      GST_DEBUG_FUNCPTR (gst_stm_ts_demux_handle_src_event));

  /* Store pad internally */
  stream->pad = pad;
  /* The stream type is attached to the pad.
     This is for xxx_handle_src_event and xxx_src_query */
  gst_pad_set_element_private (pad, stream);

  /* Activate and add pad */
  if (!gst_pad_set_active (pad, TRUE)) {
    GST_ERROR_OBJECT (demux, "Failed to pad %s as active", stream->padname);
    goto exit_error;
  }

  gst_pad_use_fixed_caps (pad);
  stream_id =
      gst_pad_create_stream_id_printf (pad, GST_ELEMENT_CAST (demux), "%08x",
      index);
  stream_start =
      gst_pad_get_sticky_event (demux->sinkpad, GST_EVENT_STREAM_START, 0);
  if (stream_start) {
    if (gst_event_parse_group_id (stream_start, &demux->group_id)) {
      demux->have_group_id = TRUE;
    } else {
      demux->have_group_id = FALSE;
    }
    gst_event_unref (stream_start);
  } else if (!demux->have_group_id) {
    demux->have_group_id = TRUE;
    demux->group_id = gst_util_group_id_next ();
  }
  stream_start = gst_event_new_stream_start (stream_id);
  g_free (stream_id);
  if (demux->have_group_id) {
    gst_event_set_group_id (stream_start, demux->group_id);
  }
  if (!gst_pad_push_event (pad, stream_start)) {
    GST_ERROR_OBJECT (demux, "Failed to push stream start event on pad %s",
        stream->padname);
    goto exit_error;
  }
  if (!gst_pad_push_event (pad, gst_event_new_caps (caps))) {
    GST_ERROR_OBJECT (demux, "Failed to set caps on pad %s", stream->padname);
    goto exit_error;
  }
  gst_caps_unref (caps);

  if (!gst_element_add_pad (GST_ELEMENT (demux), pad)) {
    GST_ERROR_OBJECT (demux, "Failed to add pad %s to element",
        stream->padname);
    goto exit_error;
  }
  if (stream->tag_list) {
    gst_pad_push_event (stream->pad, gst_event_new_tag (stream->tag_list));
  }

  if (stream->is_protected == TRUE) {
    GList *l;
    /* send pending protection events received from upstream */
    for (l = demux->protection_event_queue.head; l != NULL; l = l->next) {
      gst_pad_push_event (stream->pad, gst_event_ref (l->data));
    }
  }

  return TRUE;

exit_error:
  switch (stream->content) {
    case STREAM_CONTENT_VIDEO:
      demux->vpads_num--;
      break;
    case STREAM_CONTENT_AUDIO:
      demux->apads_num--;
      break;
    case STREAM_CONTENT_SUBTITLE:
      demux->spads_num--;
      break;
    default:
      break;
  }

  if (stream->padname != NULL) {
    g_free (stream->padname);
    stream->padname = NULL;
  }

  demux->ref_index = 0xffffffff;

  if (caps != NULL) {
    gst_caps_unref (caps);
  }

  return FALSE;

}

static gboolean
gst_stm_ts_demux_delete_stream (Gststm_ts_demux * demux, gint index)
{
  Gststm_ts_stream *stream;

  GST_LOG_OBJECT (demux, "%s:%d", __FUNCTION__, __LINE__);

  stream = demux->streams[index];
  if (stream != NULL) {
    if (stream->pad != NULL) {
      gst_element_remove_pad (GST_ELEMENT (demux), stream->pad);
      stream->pad = NULL;
    }
    if (gst_stm_ts_demux_stop_demux (demux, stream) == TRUE) {
      gst_stm_ts_demux_close_demux (demux, stream);
    }
    if (stream->content == STREAM_CONTENT_VIDEO) {
      demux->vpads_num--;
    } else if (stream->content == STREAM_CONTENT_AUDIO) {
      demux->apads_num--;
    } else if (stream->content == STREAM_CONTENT_SUBTITLE) {
      demux->spads_num--;
    }
    if (stream->padname != NULL) {
      g_free (stream->padname);
      stream->padname = NULL;
    }
    if (stream->adapter) {
      gst_adapter_clear (stream->adapter);
      g_object_unref (stream->adapter);
    }
    while (!g_queue_is_empty (&(stream->ecm_queue))) {
      GstStm_ts_SENC_ECM_Data *temp_senc_data =
          g_queue_pop_head (&(stream->ecm_queue));
      gst_stm_ts_demux_ecm_free (temp_senc_data);
    }
    g_queue_clear (&(stream->ecm_queue));
    g_free (stream);
    demux->num_streams--;
    demux->streams[index] = NULL;
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_change_stream (Gststm_ts_demux * demux,
    stream_content_t content, guint pid, guint codec)
{
  GstCaps *caps;
  guint i;

  GST_LOG_OBJECT (demux, "%s:%d", __FUNCTION__, __LINE__);

  for (i = 0; i < demux->num_streams; i++) {
    /* check if received content is already registered */
    if ((demux->streams[i]->content == content)) {
      /* check if stream is registered for same pid */
      if ((demux->streams[i]->pid == pid)
          && (demux->streams[i]->codec == codec)) {
        /* given pid-content is already registered so nothing to do */
        GST_DEBUG_OBJECT (demux,
            "services are already registered. Nothing to do");
        if (gst_stm_ts_demux_program_demux (demux, demux->streams[i]) != TRUE) {
          GST_ERROR_OBJECT (demux, "failed to create demux");
        }
        return TRUE;
      }
      /* content exists but pid is different so update filtering params */
      demux->streams[i]->pid = pid;
      demux->streams[i]->codec = codec;
      demux->streams[i]->discontinue = TRUE;
      demux->streams[i]->first_buffer_sent = FALSE;
      demux->streams[i]->stream_id = 0;

      if (demux->dev_opened == TRUE) {
        /* if pad has already been created then update pad caps */
        if (demux->streams[i]->pad != NULL) {
          /* Remap the pad caps */
          caps = gst_stm_ts_demux_map_caps (demux, i);
          if (caps != NULL) {
            if (gst_pad_set_caps (demux->streams[i]->pad, caps) == FALSE) {
              GST_ERROR_OBJECT (demux, "failed to remap caps on pad %s",
                  demux->streams[i]->padname);
            }
            gst_caps_unref (caps);
          }

          /* push changed tags (audio or/and subttile pid) downstream */
          if (demux->streams[i]->tag_list) {
            if (gst_stm_ts_demux_create_taglist (demux->streams[i])) {
              gst_pad_push_event (demux->streams[i]->pad,
                  gst_event_new_tag (demux->streams[i]->tag_list));
            }
          }
          if (gst_stm_ts_demux_program_demux (demux, demux->streams[i]) != TRUE) {
            GST_ERROR_OBJECT (demux, "failed to create demux");
          }
        } else {
          /* pad is not created yet so create the pad */
          gst_stm_ts_demux_create_stream (demux, i);
        }
      }
      break;
    }
  }

  /* content is not registered yet so register the content */
  if (i == demux->num_streams) {
    gst_stm_ts_demux_register_stream (demux, content, pid, codec, FALSE);
    if (demux->opened == TRUE) {
      /* create new pad for received content */
      gst_stm_ts_demux_create_stream (demux, i);
    }
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_push_event (Gststm_ts_demux * demux, GstEvent * event)
{
  gint i;
  gboolean res = TRUE;

  GST_LOG_OBJECT (demux, "gst_stm_ts_demux_push_event %s",
      gst_event_type_get_name (GST_EVENT_TYPE (event)));

  /* Push event to all demux source pads */
  for (i = 0; i < GST_STM_TS_DEMUX_MAX_STREAMS; i++) {
    Gststm_ts_stream *stream = demux->streams[i];

    if ((stream != NULL) && (stream->pad != NULL)) {
      gst_event_ref (event);
      res |= gst_pad_push_event (stream->pad, event);
    }
  }
  /* Consume this event */
  gst_event_unref (event);

  return res;
}

static gboolean
gst_stm_ts_handle_pull_mode_seek (Gststm_ts_demux * demux, GstPad * pad,
    GstEvent * event)
{
  GstFormat format;
  GstSeekFlags flags;
  GstSeekType cur_type = GST_SEEK_TYPE_NONE, stop_type;
  GstSegment seeksegment;
  gint64 cur, stop;
  gdouble rate;
  gboolean flush;
  gboolean update;
  gboolean upstream_seek = TRUE;
  gboolean res = TRUE;

  GST_LOG_OBJECT (demux, "gst_stm_ts_handle_pull_mode_seek");
  /* If we are not playing live/ip */
  /* Assuming that we can seek all the TS file by parsing the packet header */
  if (event) {
    gst_event_parse_seek (event, &rate, &format, &flags, &cur_type, &cur,
        &stop_type, &stop);
    if (format != GST_FORMAT_TIME) {
      GstFormat fmt = GST_FORMAT_TIME;

      if (cur_type != GST_SEEK_TYPE_NONE) {
        res = gst_pad_query_convert (pad, format, cur, fmt, &cur);
      }

      if (res && stop_type != GST_SEEK_TYPE_NONE) {
        res = gst_pad_query_convert (pad, format, stop, fmt, &stop);
      }

      if (res == FALSE) {
        GST_WARNING_OBJECT (demux, "format not supported, seek aborted.");
        return FALSE;
      }

      format = fmt;
    }
    /* Check if we are doing a seek or speed change */
    if ((cur == 0) && (cur_type == GST_SEEK_TYPE_NONE) && (stop == -1)
        && (stop_type == GST_SEEK_TYPE_NONE)) {
      upstream_seek = FALSE;
    }

    GST_LOG_OBJECT (demux,
        "seek requested: rate %g cur %" GST_TIME_FORMAT " stop %"
        GST_TIME_FORMAT, rate, GST_TIME_ARGS (cur), GST_TIME_ARGS (stop));
  } else {
    GST_DEBUG_OBJECT (demux, "doing seek without event??");
    flags = 0;
    rate = 1.0;
  }

  flush = flags & GST_SEEK_FLAG_FLUSH;

  GST_OBJECT_LOCK (demux);
  demux->seeking = TRUE;
  GST_OBJECT_UNLOCK (demux);
  if (demux->eos) {
    demux->eos = FALSE;
    flush = TRUE;
  }
  /* Send flush start event */
  if (flush) {
    GST_OBJECT_LOCK (demux);
    demux->flushing = TRUE;
    GST_OBJECT_UNLOCK (demux);
    gst_stm_ts_demux_seek_flush_start (demux);
    gst_stm_ts_demux_pause_read_write_task (demux);
  } else {
    /* A non-flushing seek, PAUSE the task so that we can take the STREAM_LOCK */
    GST_LOG_OBJECT (demux, "non flushing seek, pausing task");
    gst_pad_pause_task (demux->sinkpad);
    gst_stm_ts_demux_pause_read_task (demux);
  }

  /* Wait for the sink pad task to stop */
  GST_LOG_OBJECT (demux, "wait for the sink pad task to stop");
  GST_PAD_STREAM_LOCK (demux->sinkpad);

  memcpy (&seeksegment, &demux->segment, sizeof (GstSegment));

  /* Now configure the new seek segment */
  if (event) {
    /* update segment relative to demux->start_time since
       user seeks are relative to start_time.
     */
    gst_stm_ts_demux_update_segment_to_rel (demux, &seeksegment);
    gst_segment_do_seek (&seeksegment, rate, format, flags, cur_type, cur,
        stop_type, stop, &update);
  }

  GST_LOG_OBJECT (demux,
      "segment configured from %" G_GINT64_FORMAT " to %" G_GINT64_FORMAT
      ", position %" G_GINT64_FORMAT, seeksegment.start, seeksegment.stop,
      seeksegment.position);


  /* Perform the seek, segment.position contains new position */
  res =
      gst_stm_ts_demux_do_seek (demux, &seeksegment, upstream_seek, cur_type,
      stop_type);

  /* update segment to get absolute values */
  gst_stm_ts_demux_update_segment_to_abs (demux, &seeksegment);

  /* Prepare to continue streaming */
  if (flush) {
    gst_stm_ts_hwdemux_flush (demux);
    GST_OBJECT_LOCK (demux);
    demux->flushing = FALSE;
    GST_OBJECT_UNLOCK (demux);
    /* Send flush stop command downstream */
    gst_stm_ts_demux_push_event (demux, gst_event_new_flush_stop (TRUE));
  }

  /* If sucessfully seek, update real segment and push out the new segment */
  if (res) {
    /* Update the segment info here */
    memcpy (&demux->segment, &seeksegment, sizeof (GstSegment));

    /* Post the SEGMENT_START message when we do segmented playback */
    if (demux->segment.flags & GST_SEEK_FLAG_SEGMENT) {
      gst_element_post_message (GST_ELEMENT (demux),
          gst_message_new_segment_start (GST_OBJECT (demux),
              demux->segment.format, demux->segment.position));
    }
  }

  GST_OBJECT_LOCK (demux);
  demux->seeking = FALSE;
  demux->running = TRUE;
  GST_OBJECT_UNLOCK (demux);
  /* Restart the task */
  gst_stm_ts_demux_resume_read_write_task (demux);

  /* Release the lock again so we can continue reading */
  GST_PAD_STREAM_UNLOCK (demux->sinkpad);

  return TRUE;
}


gboolean
gst_stm_ts_handle_push_mode_seek (Gststm_ts_demux * demux, GstPad * pad,
    GstObject * parent, GstEvent * event)
{
  GstFormat format = GST_FORMAT_TIME;
  GstSeekFlags flags;
  GstSeekType cur_type = GST_SEEK_TYPE_NONE, stop_type;
  GstSegment seeksegment;
  gint64 cur, stop;
  gdouble rate;
  gboolean flush;
  gboolean update;
  gboolean upstream_seek = TRUE;
  gboolean res = TRUE;
  GstPipeContext *pipe = &demux->pipe;

  GST_LOG_OBJECT (demux, "gst_stm_ts_handle_push_mode_seek");

  /* Check if upstream element supports seek in TIME format */
  if (gst_stm_ts_demux_is_upstream_seekable (demux, format)) {
    gst_event_ref (event);
    res = gst_pad_event_default (pad, parent, event);
    return res;
  }

  /* If we are not playing live/ip */
  /* Assuming that we can seek all the TS file by parsing the packet header */
  if (event) {
    gst_event_parse_seek (event, &rate, &format, &flags, &cur_type, &cur,
        &stop_type, &stop);
    if (format != GST_FORMAT_TIME) {
      GstFormat fmt = GST_FORMAT_TIME;

      if (cur_type != GST_SEEK_TYPE_NONE) {
        res = gst_pad_query_convert (pad, format, cur, fmt, &cur);
      }

      if (res && stop_type != GST_SEEK_TYPE_NONE) {
        res = gst_pad_query_convert (pad, format, stop, fmt, &stop);
      }

      if (res == FALSE) {
        GST_DEBUG_OBJECT (demux, "format not supported, seek aborted");
        return FALSE;
      }

      format = fmt;
    }
    /* Check if we are doing a seek or speed change */
    if ((cur == 0) && (cur_type == GST_SEEK_TYPE_NONE) && (stop == -1)
        && (stop_type == GST_SEEK_TYPE_NONE)) {
      upstream_seek = FALSE;
    }
    if ((demux->upstream_rap_index_support == TRUE)
        || (demux->upstream_time_index_support == TRUE)) {
      upstream_seek = TRUE;
    }
    GST_DEBUG_OBJECT (demux,
        "seek requested: rate %g cur %" GST_TIME_FORMAT " stop %"
        GST_TIME_FORMAT, rate, GST_TIME_ARGS (cur), GST_TIME_ARGS (stop));
  } else {
    GST_DEBUG_OBJECT (demux, "doing seek without event??");
    flags = 0;
    rate = 1.0;
  }

  flush = flags & GST_SEEK_FLAG_FLUSH;

  GST_OBJECT_LOCK (demux);
  demux->seeking = TRUE;
  /*  forward motion after bof or backward motion after eof */
  if (demux->pipe.eof) {
    flush = TRUE;
    demux->pipe.eof = FALSE;
    demux->pipe.srcresult = GST_FLOW_OK;
  }
  GST_OBJECT_UNLOCK (demux);

  if (flush) {
    /* update upstream_seekable flag so that after flush_stop event is
       sent downstream, we can update srcresult of demux and pipe correctly */
    if (gst_stm_ts_demux_is_upstream_seekable (demux, GST_FORMAT_BYTES)) {
      GST_LOG_OBJECT (demux, "upstream_seekable flag updated");
    }

    /* Mark flushing so that the streaming thread can return WRONG_STATE */
    GST_OBJECT_LOCK (demux);
    demux->flushing = TRUE;
    GST_OBJECT_UNLOCK (demux);
    gst_stm_ts_demux_seek_flush_start (demux);
    GST_STM_PIPE_SIGNAL (pipe);
    gst_stm_ts_demux_pause_read_write_task (demux);
    gst_adapter_clear (demux->pipe.adapter);
    demux->sync_lock = FALSE;
  } else {
    gst_stm_ts_demux_pause_write_task (demux);
  }

  memcpy (&seeksegment, &demux->segment, sizeof (GstSegment));

  /* Now configure the seek segment */
  if (event) {
    /* update segment relative to demux->start_time since
       user seeks are relative to start_time.
     */
    gst_stm_ts_demux_update_segment_to_rel (demux, &seeksegment);
    gst_segment_do_seek (&seeksegment, rate, format, flags, cur_type, cur,
        stop_type, stop, &update);
  }

  GST_DEBUG_OBJECT (demux,
      "segment configured from %" G_GINT64_FORMAT " to %" G_GINT64_FORMAT
      ", position %" G_GINT64_FORMAT, seeksegment.start, seeksegment.stop,
      seeksegment.position);

  /* Perform the seek, segment.position contains new position */
  res =
      gst_stm_ts_demux_do_seek (demux, &seeksegment, upstream_seek, cur_type,
      stop_type);

  /* update segment to get absolute values */
  gst_stm_ts_demux_update_segment_to_abs (demux, &seeksegment);

  /* Prepare to continue streaming */
  if (flush) {
    gst_stm_ts_hwdemux_flush (demux);
    GST_OBJECT_LOCK (demux);
    demux->flushing = FALSE;
    GST_OBJECT_UNLOCK (demux);
    /* Send flush stop command downstream */
    gst_stm_ts_demux_push_event (demux, gst_event_new_flush_stop (TRUE));
    if (demux->upstream_seekable == FALSE) {
      pipe->srcresult = GST_FLOW_OK;
      demux->srcresult = GST_FLOW_OK;
    }

  }

  if (res) {
    /* Update the segment info here */
    memcpy (&demux->segment, &seeksegment, sizeof (GstSegment));
  }

  /* we will send it in any case, e.g. required in direct catch live */
  demux->newsegment = TRUE;

  /* If we are not doing a flush, manually resync the file offset with upstream */
  if (((demux->upstream_rap_index_support == TRUE)
          || (demux->upstream_time_index_support == TRUE))
      && (flush == FALSE)) {
    GstFormat format = GST_FORMAT_BYTES;
    if (gst_pad_query_position (GST_PAD_PEER (demux->sinkpad), format,
            (gint64 *) & demux->offset) == FALSE) {
      GST_WARNING_OBJECT (demux,
          "unable to get current file position from upstream");
    }
  }

  GST_OBJECT_LOCK (demux);
  demux->seeking = FALSE;
  demux->running = TRUE;
  GST_OBJECT_UNLOCK (demux);
  /* Restart the task */
  if (flush) {
    /* Read task is paused only for flushing seek */
    gst_task_start (demux->read_task);
  }
  gst_stm_ts_demux_resume_write_task (demux);

  return TRUE;
}

static gboolean
gst_stm_ts_hwdemux_flush (Gststm_ts_demux * demux)
{
  Gststm_ts_stream *streams = NULL;
  gboolean error = FALSE;
  int i = 0;
  for (i = 0; i < GST_STM_TS_DEMUX_MAX_STREAMS; i++) {
    streams = demux->streams[i];
    if (streams != NULL) {
      /* flush the read device so that write call will get unblocked */
      if (ioctl (streams->fd, DMX_FLUSH_CHANNEL, NULL) != 0) {
        GST_ERROR_OBJECT (demux,
            "demux device ioctl DMX_FLUSH_CHANNEL failed %s\n",
            strerror (errno));
        error = TRUE;
      }
    }
  }
  /* Mark discontinuity read after flush */
  demux->disc_read = TRUE;
  return error;
}

static void
gst_stm_ts_demux_seek_flush_start (Gststm_ts_demux * demux)
{
  /* Push event to all pads then it will stop streaming with a WRONG_STATE
     We can thus take the stream lock */
  GST_LOG_OBJECT (demux, "sending flush start ");
  /* Push event downstream */
  gst_stm_ts_demux_push_event (demux, gst_event_new_flush_start ());
  gst_stm_ts_hwdemux_flush (demux);
}

static gboolean
gst_stm_ts_demux_do_seek (Gststm_ts_demux * demux, GstSegment * segment,
    gboolean upstream_seek, GstSeekType cur_type, GstSeekType stop_type)
{
  GstFormat format;
  gint64 target;
  gint64 ts_target;
  guint64 pts_diff;
  guint64 size_diff;
  guint64 jump = 0;

  if (demux->num_streams == 0) {
    return FALSE;
  }

  /* Set new seek position */
  if ((segment->rate > 0) || ((segment->rate < 0)
          && (stop_type != GST_SEEK_TYPE_NONE))) {
    target = ts_target = segment->position;
  } else {
    target = ts_target = segment->start;
  }

  GST_DEBUG_OBJECT (demux, "seek to %" GST_TIME_FORMAT, GST_TIME_ARGS (target));

  if (upstream_seek == TRUE) {
    if ((demux->upstream_rap_index_support == FALSE)
        && (demux->upstream_time_index_support == FALSE)) {
      /* if seek position is at end or beyond, get the size of stream and seek
         to that size.
       */
      if (target >= demux->duration) {
        GstPad *peer;
        GstFormat fmt = GST_FORMAT_BYTES;
        gint64 len = 0;

        /* Get the end position of the stream */
        peer = gst_pad_get_peer (demux->sinkpad);
        if (peer) {
          if (gst_pad_query_duration (peer, fmt, &len)) {
            demux->offset = (len / demux->packetsize) * demux->packetsize;
          }
          gst_object_unref (peer);
        }
      } else {
        /* Calculate the position to jump, now we assume the seek is always SEEK_SET */
        pts_diff = gst_stm_ts_demux_pts_diff (demux);
        size_diff = demux->last_offset - demux->first_offset;
        if (pts_diff > 0) {
          ts_target = gst_stm_time_gst_to_ts_demux (target);
          jump = (size_diff * (90 * ts_target)) / pts_diff;
        }
        demux->offset = demux->first_offset + jump;
        /* Realign to the packet size */
        demux->offset =
            (demux->offset / demux->packetsize) * demux->packetsize +
            demux->streamoffset;
      }
    }

    if (demux->streaming == TRUE) {
      format = GST_FORMAT_BYTES;
      ts_target = demux->offset;
      if (gst_stm_ts_demux_is_upstream_seekable (demux, format)) {
        if (gst_stm_ts_demux_push_mode_seek (demux, segment->rate,
                format, cur_type, ts_target, segment->flags) == FALSE) {
          GST_WARNING_OBJECT (demux, "push mode seek failed");
          return FALSE;
        }
      } else {
        return FALSE;
      }
    }
  }

  return TRUE;
}

/*
 function: gst_stm_ts_demux_update_segment_to_rel
 input params:
           demux - demuxer context
           segment - segment to be updated
 description : will update given segment relative to start_time
 return : void
 */
static void
gst_stm_ts_demux_update_segment_to_rel (Gststm_ts_demux * demux,
    GstSegment * segment)
{
  /* update segment relative to demux->start_time since
     user seeks are relative to start_time.
   */
  if (GST_CLOCK_TIME_IS_VALID (demux->start_time)) {
    segment->position = demux->strm_pos;
    segment->start = segment->time;
    if (GST_CLOCK_TIME_IS_VALID (segment->stop)) {
      segment->stop -= demux->start_time;
    }
  }
  return;
}

/*
 function: gst_stm_ts_demux_update_segment_to_abs
 input params:
           demux - demuxer context
           segment - segment to be updated
 description : will update given segment to absolute
 return : void
 */
static void
gst_stm_ts_demux_update_segment_to_abs (Gststm_ts_demux * demux,
    GstSegment * segment)
{
  /* update segment with demux->start_time to get absolute
     values.
   */
  if (GST_CLOCK_TIME_IS_VALID (demux->start_time)) {
    if (GST_CLOCK_TIME_IS_VALID (segment->start)) {
      segment->start += demux->start_pts;
    }
    if (GST_CLOCK_TIME_IS_VALID (segment->stop)) {
      segment->stop += demux->start_pts;
    }
  }

  return;
}

void
gst_stm_ts_demux_register_stream (Gststm_ts_demux * demux,
    stream_content_t content, guint pid, guint codec, gboolean protected)
{
  Gststm_ts_stream *stream;

  /* No more free slot */
  if (demux->num_streams >= GST_STM_TS_DEMUX_MAX_STREAMS) {
    return;
  }

  /* For TS file, if the audio is PCM, we force it to LPCMB */
  if ((content == STREAM_CONTENT_AUDIO) && (codec == AUDIO_ENCODING_PCM)) {
    codec = AUDIO_ENCODING_LPCMB;
  }

  stream = g_new0 (Gststm_ts_stream, 1);
  stream->dev_id = demux->dev_id;
  stream->fd = -1;
  stream->pad = NULL;
  stream->padname = NULL;
  stream->content = content;
  stream->pid = pid;
  stream->codec = codec;
  stream->discontinue = FALSE;
  stream->stream_id = 0;
  stream->last_flow = GST_FLOW_OK;
  stream->first_buffer_sent = FALSE;
  stream->prev_pts = GST_CLOCK_TIME_NONE;
  stream->is_protected = protected;
  stream->ecm_data = NULL;
  stream->adapter = NULL;
  stream->header_required = TRUE;
  stream->pes_packet_size = 0;
  stream->pes_counter = 0;
  stream->scrambled_pes_count = 0;
  stream->zero_pes_size = 0;
  stream->zero_pes = 0;

  demux->streams[demux->num_streams] = stream;
  demux->num_streams++;
}

gboolean
gst_stm_ts_demux_read (Gststm_ts_demux * demux, GstBuffer ** buf,
    guint64 offset, guint size, gboolean copy)
{
  GstFlowReturn res = GST_FLOW_OK;
  GstSeekFlags flags = GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE;

  GST_TRACE_OBJECT (demux, "reading %d bytes from offset %" G_GINT64_FORMAT,
      size, offset);
  if (demux->streaming == FALSE) {
    if (offset == GST_STM_TS_DEMUX_NO_SEEK) {
      offset = demux->offset;
    }
    res = gst_pad_pull_range (demux->sinkpad, offset, size, buf);
    if (res != GST_FLOW_OK) {
      GST_WARNING_OBJECT (demux, "read failed returned %d", res);
      return FALSE;
    }
  } else {
    if ((offset != GST_STM_TS_DEMUX_NO_SEEK) && (!copy)) {
      demux->internal_seek = TRUE;
      /* in case of live http streaming, if client is not able to seek,
         it should continue processing the incoming data */
      if (gst_stm_ts_demux_is_upstream_seekable (demux, GST_FORMAT_BYTES)) {
        if (gst_stm_ts_demux_push_mode_seek (demux, 1.0, GST_FORMAT_BYTES,
                GST_SEEK_TYPE_SET, offset, flags) == FALSE) {
          GST_WARNING_OBJECT (demux, "push mode seek failed");
        }
      }
      demux->internal_seek = FALSE;
    } else if (offset == GST_STM_TS_DEMUX_NO_SEEK) {
      offset = 0;
    }
    if (gst_stm_ts_demux_push_mode_read (demux, buf, size, copy,
            offset) == FALSE) {
      GST_WARNING_OBJECT (demux, "push mode read failed");
      *buf = NULL;
      return FALSE;
    }
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_push_mode_read (Gststm_ts_demux * demux, GstBuffer ** buf,
    guint size, gboolean copy, guint64 offset)
{
  GstPipeContext *pipe;
  guint available = 0;
  guint read = 0;

  *buf = NULL;

  pipe = &demux->pipe;

  /* Lock the pipe to update some fields */
  GST_STM_PIPE_MUTEX_LOCK (pipe);

  /* Update the required size */
  pipe->needed = size + offset;
  if (pipe->srcresult == GST_FLOW_FLUSHING) {
    GST_STM_PIPE_MUTEX_UNLOCK (pipe);
    return FALSE;
  }

  available = gst_adapter_available (pipe->adapter);
  /* Data in the pipe not enough and file not end yet */
  while ((available < pipe->needed) && (pipe->eof == FALSE)
      && (pipe->closed == FALSE)) {
    GST_TRACE_OBJECT (demux, "available:%d, requested:%d", available, size);
    /* Signal the chain task to read data into the pipe */
    GST_STM_PIPE_SIGNAL (pipe);
    /* Wait for the chain task to signal if demux is not flushing */
    if ((demux->flushing == FALSE) && (!demux->need_unlock)) {
      if (demux->table_id == GST_STM_TS_SDT_TABLE_ID) {
        gint64 timeout = g_get_monotonic_time () + G_TIME_SPAN_SECOND;
        if (!GST_STM_PIPE_WAIT_UNTIL (pipe, timeout)) {
          GST_STM_PIPE_MUTEX_UNLOCK (pipe);
          return FALSE;
        }
      } else {
        GST_STM_PIPE_WAIT (pipe);
      }
    } else {
      available = 0;
      break;
    }
    /* check data from the pipe again */
    available = gst_adapter_available (pipe->adapter);
  }
  available -= offset;

  /* If there is no data available, eof or closed happens */
  if (available == 0) {
    GST_STM_PIPE_MUTEX_UNLOCK (pipe);
    return FALSE;
  }

  /* Data in the buffer >= requested size or end of file reached */
  if ((demux->streaming == TRUE) && ((demux->segment.rate > 0)
          || (demux->upstream_rap_index_support == TRUE))) {
    if (copy) {
      read = MIN (available, BUFFER_SIZE);
    } else {
      read = available / demux->packetsize * demux->packetsize;
      read = MIN (read, BUFFER_SIZE);
    }
  } else {
    read = MIN (available, size);
  }
  if (read > 0) {
    GST_TRACE_OBJECT (demux, "getting %d bytes", read);


    if (!copy) {
      /* Read data from the adapter to dest buffer */
      *buf = gst_adapter_take_buffer (pipe->adapter, read);
    } else {
      GstMapInfo info_read;
      *buf = gst_buffer_new_and_alloc (read);
      gst_buffer_map (*buf, &info_read, GST_MAP_READ);
      gst_adapter_copy (pipe->adapter, (guint8 *) info_read.data, offset,
          info_read.size);
      gst_buffer_unmap (*buf, &info_read);
    }

    if (*buf == NULL) {
      return FALSE;
    }

    GST_TRACE_OBJECT (demux, "%d bytes left in adapter",
        gst_adapter_available (pipe->adapter));
    pipe->offset += read;
  } else {
    /* We may have some small bytes left, less than packetsize */
    GST_STM_PIPE_MUTEX_UNLOCK (pipe);
    return FALSE;
  }

  if (pipe->srcresult == GST_FLOW_FLUSHING) {
    if (*buf != NULL) {
      gst_buffer_unref (*buf);
      *buf = NULL;
    }
    GST_STM_PIPE_MUTEX_UNLOCK (pipe);
    return FALSE;
  }

  /* Unlock the pipe now */
  GST_STM_PIPE_MUTEX_UNLOCK (pipe);

  return TRUE;
}

/*
 function: gst_stm_ts_demux_is_upstream_seekable
 input params:
           demux - demuxer context
           format - GstFormat in which seek is requested
 description : checks if upstream supports seeking in given format
 return : TRUE if successful else FALSE.
 */
gboolean
gst_stm_ts_demux_is_upstream_seekable (Gststm_ts_demux * demux,
    GstFormat format)
{
  GstQuery *query = NULL;

  demux->upstream_seekable = FALSE;
  query = gst_query_new_seeking (format);

  if (query != NULL) {
    if (gst_pad_peer_query (demux->sinkpad, query)) {
      gst_query_parse_seeking (query, &format, &demux->upstream_seekable, NULL,
          NULL);
    }
    gst_query_unref (query);
  }

  GST_DEBUG_OBJECT (demux, "upstream seekable %s",
      demux->upstream_seekable ? "TRUE" : "FALSE");
  return demux->upstream_seekable;
}

gboolean
gst_stm_ts_demux_push_mode_seek (Gststm_ts_demux * demux, gdouble rate,
    GstFormat format, GstSeekType start_type, gint64 start, GstSeekFlags flags)
{
  GstEvent *event = NULL;
  GstPipeContext *pipe = NULL;

  pipe = &demux->pipe;

  /* signal discontinuity - read loop thread paused for application flushing seek */
  demux->disc_read = TRUE;
  GST_STM_PIPE_MUTEX_LOCK (pipe);
  pipe->eof = FALSE;
  GST_STM_PIPE_MUTEX_UNLOCK (pipe);

  if (start_type == GST_SEEK_TYPE_NONE) {
    flags = GST_SEEK_FLAG_NONE;
  }
  /* souphttpsrc expects positive rate with start offset */
  /* no need to update rate for recorder/timeshift usecase */
  if (demux->upstream_rap_index_support == FALSE
      && demux->upstream_time_index_support == FALSE) {
    if (rate < 0) {
      rate = (rate * -1);
    }
  }
  event = gst_event_new_seek (rate, format,
      flags, start_type, start, GST_SEEK_TYPE_NONE, -1);
  if (event == NULL) {
    GST_WARNING_OBJECT (demux, "failed to create seek event");
    return FALSE;
  }

  return gst_pad_push_event (demux->sinkpad, event);
}

static gboolean
gst_stm_ts_demux_parse_stream_info (Gststm_ts_demux * demux)
{
  GstBuffer *buf = NULL;
  GstFormat format = GST_FORMAT_BYTES;
  GstQuery *query = NULL;
  GstStructure *structure;
  guint read_size;
  guint packet_size, packets_start;
  guint64 read_offset;
  GstMapInfo info_read;

  if (demux->filesize != 0) {
    return TRUE;
  }

  /* Get file size  */
  if (gst_pad_is_linked (demux->sinkpad)) {
    if (gst_pad_query_duration (GST_PAD_PEER (demux->sinkpad), format,
            (gint64 *) & demux->filesize) == FALSE) {
      GST_WARNING_OBJECT (demux, "unable to get filesize");
      /* The source may be rtsp */
      demux->filesize = -1;
    }

    /* Get file duration in time, if upstream can provide it */
    if (gst_pad_query_duration (GST_PAD_PEER (demux->sinkpad), GST_FORMAT_TIME,
            (gint64 *) & demux->duration) == FALSE) {
      demux->duration = GST_CLOCK_TIME_NONE;
    }

    /* Check if upstream supports index */
    structure = gst_structure_new ("index-support",
        "index", G_TYPE_BOOLEAN, FALSE, NULL);
    query = gst_query_new_custom (GST_QUERY_CUSTOM, structure);
    if (query != NULL) {
      if (gst_pad_peer_query (demux->sinkpad, query) == TRUE) {
        gst_structure_get_boolean (structure, "rap-index",
            &demux->upstream_rap_index_support);
        gst_structure_get_boolean (structure, "time-index",
            &demux->upstream_time_index_support);
      }
      gst_query_unref (query);
    }
  }

  /* For live and IP source */
  if (demux->live || demux->upstream_rap_index_support) {
    GstPipeContext *pipe;

    pipe = &demux->pipe;
    pipe->needed = BUFFER_SIZE;
    gst_adapter_clear (pipe->adapter);
    demux->sync_lock = FALSE;
    GST_STM_PIPE_SIGNAL (pipe);

    return TRUE;
  }

  /* Try to find the start of valid packet */
  read_size = BUFFER_SIZE;
  for (read_offset = 0; read_offset < demux->filesize;
      read_offset += BUFFER_SIZE) {
    buf = NULL;
    if (gst_stm_ts_demux_read (demux, &buf, read_offset, read_size,
            TRUE) == FALSE) {
      return FALSE;
    }

    gst_buffer_map (buf, &info_read, GST_MAP_READ);
    if (gst_stm_ts_demux_scan_valid_packets (demux, info_read.data,
            info_read.size, &packet_size, &packets_start, NULL,
            FALSE) == TRUE) {
      demux->streamoffset = packets_start;
      demux->packetsize = packet_size;
      if (demux->packetsize == BD_TP_PACKET_SIZE) {
        demux->packetoffset = 4;
      }
      GST_LOG_OBJECT (demux, "packet size %d", demux->packetsize);
      gst_buffer_unmap (buf, &info_read);
      gst_buffer_unref (buf);
      break;
    }
    gst_buffer_unmap (buf, &info_read);
    gst_buffer_unref (buf);
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_get_duration (Gststm_ts_demux * demux, GstClockTime * duration)
{
  GstBuffer *buf = NULL;
  GstClockTime pts_diff;
  guint read_size;
  guint start_offset = 0, packet_size, num_packets;
  guint64 pts;
  guint64 offset = 0;
  guint64 read_offset;
  guint64 cur_offset;
  gint64 aligned_filesize = 0;
  GstSeekFlags flags = GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE;
  GstMapInfo info_read;

  if (demux->filesize == -1) {
    return FALSE;
  }

  /* Store the original offset,
     so that we can go back after getting the duration */
  cur_offset = demux->offset;

  read_size = BUFFER_SIZE < demux->filesize ? BUFFER_SIZE : demux->filesize;
  for (read_offset = demux->streamoffset;
      (read_offset < demux->filesize && demux->running);
      read_offset += BUFFER_SIZE) {
    /* Read file */
    if (gst_stm_ts_demux_read (demux, &buf, read_offset, read_size,
            FALSE) == FALSE) {
      return FALSE;
    }

    /* Search the first pts */
    if (gst_stm_ts_demux_parse_pts (demux, buf, GST_STM_TS_DEMUX_FIRST_PTS,
            &pts, &offset)) {
      GST_LOG_OBJECT (demux, "first pts %" G_GINT64_FORMAT, pts);
      /* got the first pts */
      demux->first_pts = pts;
      demux->first_offset = offset;
      /* Free the buffer */
      gst_buffer_unref (buf);
      buf = NULL;
      break;
    }

    /* Free the buffer */
    gst_buffer_unref (buf);
    buf = NULL;
  }

  /* Read end of file */
  if (demux->filesize > BUFFER_SIZE) {
    read_offset = demux->filesize - BUFFER_SIZE;
    read_size =
        (demux->filesize - BUFFER_SIZE) >
        BUFFER_SIZE ? BUFFER_SIZE : read_offset;
  } else {
    read_offset = demux->streamoffset;
    read_size = demux->filesize;
  }

  /* We may have some padding or unaligned eof, find the last sync byte */
  while (demux->running == TRUE) {
    /* Read file */
    buf = NULL;
    if (gst_stm_ts_demux_read (demux, &buf, read_offset, read_size,
            FALSE) == FALSE) {
      return FALSE;
    }

    gst_buffer_map (buf, &info_read, GST_MAP_READ);
    if (gst_stm_ts_demux_scan_valid_packets (demux, info_read.data,
            info_read.size, &packet_size, &start_offset, &num_packets, FALSE)) {
      aligned_filesize = read_offset + start_offset + num_packets * packet_size;
      gst_buffer_unmap (buf, &info_read);
      gst_buffer_unref (buf);
      break;
    }
    gst_buffer_unmap (buf, &info_read);
    gst_buffer_unref (buf);

    if (read_offset == 0) {
      GST_WARNING_OBJECT (demux, "reached end of file and still no pts");
      break;
    }

    /* Shift the read offset to next block */
    if (read_offset > BUFFER_SIZE) {
      read_offset -= BUFFER_SIZE;
    } else {
      /* Reached the beginning of file */
      if (read_offset == demux->streamoffset) {
        break;
      }
      read_offset = demux->streamoffset;
      read_size = read_offset;
    }
  }

  /* Read end of file */
  if (aligned_filesize > BUFFER_SIZE) {
    read_offset = aligned_filesize - BUFFER_SIZE;
    read_size =
        (aligned_filesize - BUFFER_SIZE) >
        BUFFER_SIZE ? BUFFER_SIZE : read_offset;
  } else {
    read_offset = demux->streamoffset;
    read_size = aligned_filesize;
  }
  while (demux->running == TRUE) {
    /* Read file */
    buf = NULL;
    if (gst_stm_ts_demux_read (demux, &buf, read_offset, read_size,
            FALSE) == FALSE) {
      return FALSE;
    }

    /* Search the last pts */
    offset = 0;
    if (gst_stm_ts_demux_parse_pts (demux, buf, GST_STM_TS_DEMUX_LAST_PTS, &pts,
            &offset)) {
      GST_LOG_OBJECT (demux, "last pts %" G_GINT64_FORMAT, pts);
      /* got the last pts */
      demux->last_pts = pts;
      demux->last_offset = offset;
      /* Free the buffer */
      gst_buffer_unref (buf);
      break;
    }

    /* Free the buffer */
    gst_buffer_unref (buf);

    if (read_offset == 0) {
      GST_WARNING_OBJECT (demux, "reached end of file and still no pts");
      break;
    }

    /* Shift the read offset to next block */
    if (read_offset > BUFFER_SIZE) {
      read_offset -= BUFFER_SIZE;
    } else {
      /* Reached the beginning of file */
      if (read_offset == demux->streamoffset) {
        demux->last_pts = demux->first_pts;
        break;
      }
      read_offset = demux->streamoffset;
      read_size = read_offset;
    }
  }

  if (demux->streaming == TRUE) {
    /* Seek to the beginning of the file */
    demux->internal_seek = TRUE;
    /* in case of live http streaming, if client is not able to seek,
       it should continue processing the incoming data */
    if (gst_stm_ts_demux_is_upstream_seekable (demux, GST_FORMAT_BYTES)) {
      if (gst_stm_ts_demux_push_mode_seek (demux, 1.0, GST_FORMAT_BYTES,
              GST_SEEK_TYPE_SET, cur_offset, flags) == FALSE) {
        GST_WARNING_OBJECT (demux, "push mode seek failed");
        demux->internal_seek = FALSE;
        return FALSE;
      }
    } else if (gst_stm_ts_demux_is_upstream_seekable (demux, GST_FORMAT_TIME)) {
      if (gst_stm_ts_demux_push_mode_seek (demux, 1.0, GST_FORMAT_TIME,
              GST_SEEK_TYPE_SET, 0, flags) == FALSE) {
        GST_WARNING_OBJECT (demux, "push mode seek failed");
        demux->internal_seek = FALSE;
        return FALSE;
      }
    }
    demux->internal_seek = FALSE;
  }

  pts_diff = gst_stm_ts_demux_pts_diff (demux);
  *duration = gst_stm_ts_demux_time_to_gst (pts_diff);

  return TRUE;
}

static void
gst_stm_ts_demux_get_pts (Gststm_ts_demux * demux, GstBuffer * buf,
    guint64 offset)
{
  guint64 pts;
  guint64 pts_offset = offset;

  if (demux->filesize != -1) {
    if (gst_stm_ts_demux_parse_pts (demux, buf,
            GST_STM_TS_DEMUX_FIRST_PTS, &pts, &pts_offset) == TRUE) {
      demux->last_pts = pts;
      demux->last_offset = pts_offset;
    }
  }

  return;
}

static gboolean
gst_stm_ts_demux_parse_pts (Gststm_ts_demux * demux, GstBuffer * buf,
    guint search, guint64 * pts, guint64 * offset)
{
  Gststm_ts_stream *stream;
  guchar *pes_header;
  guint8 *buf_data;
  guint buf_size;
  guint packet_start;
  guint data_offset;
  guint header;
  guint loop = 0;
  guint search_loop;
  guint64 start_offset;
  GstMapInfo info_read;
  gboolean ret = TRUE;

  /* Set the pts value to invalid */
  *pts = INVALID_PTS_VALUE;

  /* Set the start offset value */
  start_offset = *offset;

  /* Set the search loop according to the flag */
  if (search == 0) {
    search_loop = 1;
  } else {
    search_loop = 0xffffffff;
  }

  gst_buffer_map (buf, &info_read, GST_MAP_READ);
  buf_data = info_read.data;
  buf_size = info_read.size;

  /* First get the reference stream */
  stream = demux->streams[demux->ref_index];
  if (stream == NULL) {
    ret = FALSE;
    goto error;
  }

  for (packet_start = demux->packetoffset + start_offset;
      (packet_start < buf_size) && (loop < search_loop);
      packet_start += demux->packetsize) {
    /* Make sure we have the first sync byte */
    for (; packet_start < buf_size; packet_start++) {
      if (buf_data[packet_start] == SYNC_BYTE) {
        break;
      }
    }

    /* Check sync, pid etc */
    header =
        (guint) buf_data[packet_start] | ((guint) buf_data[packet_start +
            1] << 8) | ((guint) buf_data[packet_start +
            2] << 16) | ((guint) buf_data[packet_start + 3] << 24);
    if (!PUSI (header)) {
      continue;
    }

    if (!VALID_PACKET (header)) {
      GST_TRACE_OBJECT (demux, "(%llu) Lost sync %x %x %x %x %x %x\n",
          (demux->offset + packet_start), buf_data[packet_start + 0],
          buf_data[packet_start + 1], buf_data[packet_start + 2],
          buf_data[packet_start + 3], buf_data[packet_start + 4],
          buf_data[packet_start + 5]);
      continue;
    }

    if (PID (header) != stream->pid) {
      continue;
    }

    data_offset = GST_STM_TS_DEMUX_TS_HEADER_SIZE;
    if (ADAPTATION_FIELD (header)) {
      data_offset +=
          1 + buf_data[packet_start + GST_STM_TS_DEMUX_TS_HEADER_SIZE];
    }

    if (!PAYLOAD_PRESENT (header) || (data_offset >= demux->packetsize)) {
      continue;
    }

    /* Here we should have some pts value to parse */
    pes_header = &buf_data[packet_start + data_offset];

    if (gst_stm_ts_demux_extract_pts (pes_header, pts) == TRUE) {
      *offset = demux->offset + packet_start;
      loop++;
      GST_TRACE_OBJECT (demux, "pts = %llu at %llu\n", *pts, *offset);
    }
  }

  if (!IS_PTS_VALUE_VALID (*pts)) {
    GST_TRACE_OBJECT (demux, "no pts available");
    ret = FALSE;
    goto error;
  }

error:
  gst_buffer_unmap (buf, &info_read);
  return ret;
}

static gboolean
gst_stm_ts_demux_extract_pts (guint8 * pes_header, guint64 * pts)
{
  guint64 read_pts;

  if ((pes_header == NULL) || (pts == NULL)) {
    return FALSE;
  }

  if ((pes_header[0] == 0x00) && (pes_header[1] == 0x00)
      && (pes_header[2] == 0x01)) {
    if ((pes_header[7] & 0x80) != 0) {
      /* read PTS value */
      read_pts = (((guint64) (pes_header[9])) & 0x0e) << 29;
      read_pts |= (((guint64) (pes_header[10])) & 0xff) << 22;
      read_pts |= (((guint64) (pes_header[11])) & 0xfe) << 14;
      read_pts |= (((guint64) (pes_header[12])) & 0xff) << 7;
      read_pts |= (((guint64) (pes_header[13])) & 0xfe) >> 1;

      *pts = read_pts;

      return TRUE;
    }
  }

  return FALSE;
}

/* Scan for valid TS packets from the start of data,
   if there is a corrupted packet in the middle,
   the packet end will be the packet before the corruption.
*/
gboolean
gst_stm_ts_demux_scan_valid_packets (Gststm_ts_demux * demux, guint8 * data,
    guint size, guint * packet_size, guint * packets_start, guint * num_packets,
    gboolean lock)
{
  guint i = 0, j = 0;
  guint p_size = 0;
  guint p_start = 0;
  guint p_num = 0;
  gboolean ret = FALSE;

  if (size == 0) {
    goto exit;
  }

  if (lock == FALSE) {
    /* Scan for first valid sync byte */
    for (i = 0; (i < size); i++) {
      /* Will check for 4 consecutive packets .. to be sure */
      if ((data[i] == SYNC_BYTE)
          && ((i + 4 * BD_TP_PACKET_SIZE + 4) < size)) {
        /* Checking for 188 bytes packet */
        if ((data[i + 1 * GST_STM_TS_DEMUX_TS_PACKET_SIZE] == SYNC_BYTE)
            && (data[i + 2 * GST_STM_TS_DEMUX_TS_PACKET_SIZE] == SYNC_BYTE)
            && (data[i + 3 * GST_STM_TS_DEMUX_TS_PACKET_SIZE] == SYNC_BYTE)
            && (data[i + 4 * GST_STM_TS_DEMUX_TS_PACKET_SIZE] == SYNC_BYTE)) {
          p_size = GST_STM_TS_DEMUX_TS_PACKET_SIZE;
          p_start = i;
          break;
        }
        /* Checking for 192 bytes packet */
        if ((data[i + 1 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
            (data[i + 2 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
            (data[i + 3 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
            (data[i + 4 * BD_TP_PACKET_SIZE] == SYNC_BYTE)) {
          /* The fake sync byte can be in the 1st byte of the timecode
             0x47 0xXX 0xXX 0xXX 0x47 */
          if ((data[i + 1 + 1 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
              (data[i + 1 + 2 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
              (data[i + 1 + 3 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
              (data[i + 1 + 4 * BD_TP_PACKET_SIZE] == SYNC_BYTE)) {
            i += 1;
          }
          /* The fake sync byte can be in the 2nd byte of the timecode
             0xXX 0x47 0xXX 0xXX 0x47 */
          else if ((data[i + 2 + 1 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
              (data[i + 2 + 2 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
              (data[i + 2 + 3 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
              (data[i + 2 + 4 * BD_TP_PACKET_SIZE] == SYNC_BYTE)) {
            i += 2;
          }
          /* The fake sync byte can be in the 3rd byte of the timecode
             0xXX 0xXX 0x47 0xXX 0x47 */
          else if ((data[i + 3 + 1 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
              (data[i + 3 + 2 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
              (data[i + 3 + 3 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
              (data[i + 3 + 4 * BD_TP_PACKET_SIZE] == SYNC_BYTE)) {
            i += 3;
          }
          /* The fake sync byte can be in the 4th byte of the timecode
             0xXX 0xXX 0xXX 0x47 0x47 */
          else if ((data[i + 4 + 1 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
              (data[i + 4 + 2 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
              (data[i + 4 + 3 * BD_TP_PACKET_SIZE] == SYNC_BYTE) &&
              (data[i + 4 + 4 * BD_TP_PACKET_SIZE] == SYNC_BYTE)) {
            i += 4;
          }
          p_size = BD_TP_PACKET_SIZE;
          if (i < 4) {
            p_start = i + BD_TP_PACKET_SIZE - 4;
          } else {
            p_start = i - 4;
          }
          break;
        }
      } else if ((i + 4 * BD_TP_PACKET_SIZE + 4) >= size) {
        p_start = i;
        goto exit;
      }
    }
  } else {
    p_size = demux->packetsize;
    if (demux->packetsize == BD_TP_PACKET_SIZE) {
      i = 4;
    }
  }

  demux->sync_lock = TRUE;

  /* Find last valid packet */
  for (j = (i); (j <= (size - GST_STM_TS_DEMUX_TS_PACKET_SIZE)); j += p_size) {
    if (data[j] != SYNC_BYTE) {
      demux->sync_lock = FALSE;
      break;
    }
    p_num++;
  }

  ret = TRUE;

exit:
  if (packet_size) {
    *packet_size = p_size;
  }
  if (packets_start) {
    *packets_start = p_start;
  }
  if (num_packets) {
    *num_packets = p_num;
  }
  return ret;
}

static gboolean
gst_stm_ts_demux_pause_read_write_task (Gststm_ts_demux * demux)
{
  if (gst_stm_ts_demux_pause_write_task (demux) == FALSE) {
    return FALSE;
  }

  if (gst_stm_ts_demux_pause_read_task (demux) == FALSE) {
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_pause_write_task (Gststm_ts_demux * demux)
{

  if ((demux->opened == TRUE) && (demux->write_task)) {
    /* Pause write task */
    if (demux->streaming == TRUE) {
      if (gst_task_pause (demux->write_task) == TRUE) {
        /* Wait for the task to pause to avoid having old data */
        GST_TSDEMUX_MUTEX_LOCK (&demux->write_task_lock);
        GST_TSDEMUX_MUTEX_UNLOCK (&demux->write_task_lock);
        demux->running = FALSE;
      } else {
        GST_WARNING_OBJECT (demux, "failed to pause write task\n\r");
        return FALSE;
      }
    } else {
      if (gst_pad_pause_task (demux->sinkpad) == TRUE) {
        demux->running = FALSE;
      } else {
        GST_WARNING_OBJECT (demux, "failed to pause write task\n\r");
        return FALSE;
      }
    }
  } else {
    GST_WARNING_OBJECT (demux, "write task is not created\n\r");
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_pause_read_task (Gststm_ts_demux * demux)
{
  if (demux->read_task) {
    /* Pause read task */
    if (gst_task_pause (demux->read_task) == TRUE) {
      /* Wait for the task to pause */
      GST_TSDEMUX_MUTEX_LOCK (&demux->read_task_lock);
      GST_TSDEMUX_MUTEX_UNLOCK (&demux->read_task_lock);
    } else {
      GST_WARNING_OBJECT (demux, "failed to pause read task\n\r");
      return FALSE;
    }
  } else {
    GST_WARNING_OBJECT (demux, "read task is not created\n\r");
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_resume_write_task (Gststm_ts_demux * demux)
{
  gboolean ret = FALSE;

  /* Resume write task */
  if ((demux->opened == TRUE) && (demux->write_task)) {
    if (demux->streaming == TRUE) {
      ret = gst_task_start (demux->write_task);
    } else {
      ret =
          gst_pad_start_task (demux->sinkpad,
          (GstTaskFunction) gst_stm_ts_demux_write_loop, demux->sinkpad, NULL);
    }
    if (ret == FALSE) {
      GST_WARNING_OBJECT (demux, "failed to resume write task\n\r");
      return FALSE;
    } else {
      demux->running = TRUE;
    }
  } else {
    GST_WARNING_OBJECT (demux, "write task is not created\n\r");
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_resume_read_write_task (Gststm_ts_demux * demux)
{
  gboolean ret = FALSE;

  /* Resume read task */
  if (demux->read_task) {
    if (gst_task_start (demux->read_task) == FALSE) {
      GST_WARNING_OBJECT (demux, "failed to resume read task\n\r");
      return FALSE;
    }
  } else {
    GST_WARNING_OBJECT (demux, "read task is not created\n\r");
    return FALSE;
  }

  /* Resume write task */
  if ((demux->opened == TRUE) && (demux->write_task)) {
    if (demux->streaming == TRUE) {
      ret = gst_task_start (demux->write_task);
    } else {
      ret =
          gst_pad_start_task (demux->sinkpad,
          (GstTaskFunction) gst_stm_ts_demux_write_loop, demux->sinkpad, NULL);
    }
    if (ret == FALSE) {
      GST_WARNING_OBJECT (demux, "failed to resume write task\n\r");
      return FALSE;
    } else {
      demux->running = TRUE;
    }
  } else {
    GST_WARNING_OBJECT (demux, "write task is not created\n\r");
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_handle_create_delete_av_devices (Gststm_ts_demux * demux)
{
  GstCaps *caps;
  gint i;

  /* Enable playback from set property                               */
  /* Use case: Enable playback dynamically with the pipeline created */
  /* Dvr, demux and AV devices will be closed                         */
  if (demux->enable_playback == TRUE) {
    if (demux->dev_opened == FALSE) {
      /* Remap the pad caps */
      for (i = 0; i < demux->num_streams; i++) {
        /* Set pad caps */
        caps = gst_stm_ts_demux_map_caps (demux, i);
        if (caps != NULL) {
          if (gst_pad_push_event (demux->streams[i]->pad,
                  gst_event_new_caps (caps)) == FALSE) {
            GST_ERROR_OBJECT (demux, "failed to remap caps");
          }
          gst_caps_unref (caps);
        }
      }
      if (gst_stm_ts_demux_create_devices (demux) == FALSE) {
        return FALSE;
      }
    }

    /* Resume the read task */
    if (demux->read_task != NULL) {
      if (gst_task_get_state (demux->read_task) != GST_TASK_STARTED) {
        if (gst_task_start (demux->read_task) == FALSE) {
          GST_WARNING_OBJECT (demux, "failed to resume read task\n\r");
          return FALSE;
        }
      }
    }
  }
  /* Disable playback from set property                               */
  /* Use case: Disable playback dynamically with the pipeline created */
  /* Dvr, demux and AV devices will be closed                         */
  else {
    /* Pause the Read Task */
    if (demux->read_task
        && gst_task_get_state (demux->read_task) == GST_TASK_STARTED) {
      if (!gst_stm_ts_demux_pause_read_task (demux)) {
        GST_WARNING_OBJECT (demux, "Failed to pause read task");
      }
    }
    if (demux->dev_opened == TRUE) {
      if (gst_stm_ts_demux_delete_devices (demux) == FALSE) {
        return FALSE;
      }
    }
  }

  return TRUE;
}

/*
  * Function name : gst_stm_ts_demux_push_avsync_event
  *  Input        :
  *   demux - Gststm_ts_demux context
  *   avsync - controls avsync enable/disable
  *
  *  Description  : This function tries to send a av-sync event to
  *                 audio decoder.
  *                 This event is handled by staudio decoder.
  *                 The goal is to ensure that avsync is disabled before
  *                 playing an audio only channel and enabled before playing
  *                 audio-video channel.
  * Return        : gboolean
*/
static gboolean
gst_stm_ts_demux_push_avsync_event (Gststm_ts_demux * demux, gboolean avsync)
{
  GstStructure *structure = NULL;
  GstEvent *event = NULL;
  Gststm_ts_stream *stream = NULL;
  gint i;
  gboolean ret = TRUE;

  GST_DEBUG_OBJECT (demux, "received av-sync event.");
  structure = gst_structure_new_empty ("av-sync");
  if (structure) {
    gst_structure_set (structure, "sync-disable", G_TYPE_BOOLEAN, avsync, NULL);
    event = gst_event_new_custom (GST_EVENT_CUSTOM_DOWNSTREAM_OOB, structure);
    if (event) {
      for (i = 0; i < demux->num_streams; i++) {
        stream = demux->streams[i];
        if ((stream != NULL) && (stream->pad != NULL)
            && (stream->content == STREAM_CONTENT_AUDIO)) {
          gst_event_ref (event);
          GST_DEBUG_OBJECT (demux, "pushing av-sync event on audio pad");
          ret |= gst_pad_push_event (stream->pad, event);
        }
      }
    } else {
      GST_WARNING_OBJECT (demux, "failed to create av-sync event\n");
      gst_structure_free (structure);
      ret = FALSE;
    }
  } else {
    GST_WARNING_OBJECT (demux, "failed to create av-sync structure");
    ret = FALSE;
  }
  gst_event_unref (event);

  return ret;
}

/*
  * Function name : gst_stm_ts_demux_push_ptswrap_event
  *  Input        :
  *   demux - Gststm_ts_demux context
  *   ptswrap - ptswrap TRUE or FALSE
  *
  *  Description  : This function tries to send a pts wrap event to
  *                 all downstream elements
  *                 This event is handled by stvideo decoder.
  * Return        : gboolean
*/
static gboolean
gst_stm_ts_demux_push_ptswrap_event (Gststm_ts_demux * demux)
{
  GstStructure *structure = NULL;
  GstEvent *event = NULL;
  Gststm_ts_stream *stream = NULL;
  gint i;
  gboolean ret = TRUE;

  structure = gst_structure_new_empty ("pts-wrap");
  if (structure) {
    event = gst_event_new_custom (GST_EVENT_CUSTOM_DOWNSTREAM, structure);
    if (event) {
      for (i = 0; i < demux->num_streams; i++) {
        stream = demux->streams[i];
        if ((stream != NULL) && (stream->pad != NULL)
            && (stream->content == STREAM_CONTENT_VIDEO)) {
          gst_event_ref (event);
          GST_DEBUG_OBJECT (demux, "pushing pts-wrap event to video pad");
          ret |= gst_pad_push_event (stream->pad, event);
        }
      }
    } else {
      GST_WARNING_OBJECT (demux, "failed to create pts-wrap event\n");
      gst_structure_free (structure);
      ret = FALSE;
    }
  } else {
    GST_WARNING_OBJECT (demux, "failed to create pts-wrap structure");
    ret = FALSE;
  }
  gst_event_unref (event);

  return ret;
}

gboolean
sttsdemux_init (GstPlugin * plugin)
{
  GST_DEBUG_CATEGORY_INIT (gst_stm_ts_demux_debug, "stts_demux", 0,
      "ST Demuxer for TS stream");

  return gst_element_register (plugin, "stts_demux", (GST_RANK_PRIMARY + 10),
      GST_TYPE_STM_TS_DEMUX);
}
