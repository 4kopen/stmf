/* Gstreamer ST TS Demuxer Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sys/time.h>
#include "gststm-tsdemux.h"
#include "gststm-demux-utils.h"
#include <linux/dvb/stm_audio.h>

gboolean
gst_stm_ts_demux_configure_protected_caps (Gststm_ts_demux * demux,
    Gststm_ts_stream * stream, GstCaps * caps)
{
  g_return_val_if_fail (demux != NULL, FALSE);
  g_return_val_if_fail (stream != NULL, FALSE);

  if (G_LIKELY (demux->protection_system_ids)) {
    GstStructure *s;
    const gchar **sys_ids;
    const gchar *selected_system;
    gint i;

    sys_ids = g_new0 (const gchar *, demux->protection_system_ids->len + 1);
    for (i = 0; i < demux->protection_system_ids->len; ++i) {
      sys_ids[i] = g_ptr_array_index (demux->protection_system_ids, i);
    }
    selected_system = gst_protection_select_system (sys_ids);
    g_free (sys_ids);
    if (!selected_system) {
      GST_ERROR_OBJECT (demux, "stream is protected, but no "
          "suitable decryptor element has been found");
      return FALSE;
    }

    gst_caps_make_writable (caps);
    s = gst_caps_get_structure (caps, 0);
    gst_structure_set (s,
        "original-media-type", G_TYPE_STRING, gst_structure_get_name (s),
        GST_PROTECTION_SYSTEM_ID_CAPS_FIELD, G_TYPE_STRING, selected_system,
        NULL);
    gst_structure_set_name (s, "application/x-cenc");
  } else {
    GST_ERROR_OBJECT (demux, "stream is protected using cenc, but no "
        "cenc protection system information has been found");
    return FALSE;
  }
  return TRUE;
}

GstCaps *
gst_stm_ts_demux_map_caps (Gststm_ts_demux * demux, gint index)
{
  GstCaps *caps = NULL;
  Gststm_ts_stream *stream;
  stream_content_t content;
  guint codec;

  /* select stream to retrieve audio/video information */
  if (demux->streams[index] == NULL) {
    return NULL;
  }

  stream = demux->streams[index];
  codec = stream->codec;
  content = stream->content;

  switch (gst_stm_demux_get_gststm_codec_type (content, codec)) {
    case GST_STM_VIDEO_MPEG1:
      caps = gst_caps_new_simple ("video/mpeg-pes",
          "mpegversion", G_TYPE_INT, 1,
          "systemstream", G_TYPE_BOOLEAN, FALSE, NULL);
      break;

    case GST_STM_VIDEO_MPEG2:
      caps = gst_caps_new_simple ("video/mpeg-pes",
          "mpegversion", G_TYPE_INT, 2,
          "systemstream", G_TYPE_BOOLEAN, FALSE, NULL);
      break;
    case GST_STM_VIDEO_H264:
      caps = gst_caps_new_empty_simple ("video/x-h264-pes");
      break;

    case GST_STM_VIDEO_HEVC:
      caps = gst_caps_new_empty_simple ("video/x-h265-pes");
      break;

    case GST_STM_VIDEO_AVS:
      caps = gst_caps_new_empty_simple ("video/x-avs-pes");
      break;

    case GST_STM_VIDEO_AVSP:
      caps = gst_caps_new_empty_simple ("video/x-avsp-pes");
      break;

    case GST_STM_VIDEO_VC1:
      caps = gst_caps_new_simple ("video/x-wmv-pes",
          "wmvversion", G_TYPE_INT, 3, "format", G_TYPE_STRING, "WVC1", NULL);
      break;

    case GST_STM_AUDIO_MPEG1:
      caps = gst_caps_new_simple ("audio/mpeg-pes",
          "mpegversion", G_TYPE_INT, 1, "layer", G_TYPE_INT, 1, NULL);
      break;

    case GST_STM_AUDIO_MPEG2:
      caps = gst_caps_new_simple ("audio/mpeg-pes",
          "mpegversion", G_TYPE_INT, 1, "layer", G_TYPE_INT, 2, NULL);
      break;

    case GST_STM_AUDIO_MP3:
      caps = gst_caps_new_simple ("audio/mpeg-pes",
          "mpegversion", G_TYPE_INT, 1, "layer", G_TYPE_INT, 3, NULL);
      break;

    case GST_STM_AUDIO_AC3:
      caps = gst_caps_new_empty_simple ("audio/x-ac3-pes");
      break;

    case GST_STM_AUDIO_DTS:
      caps = gst_caps_new_empty_simple ("audio/x-dts-pes");
      break;

    case GST_STM_AUDIO_AAC:
      caps = gst_caps_new_simple ("audio/mpeg-pes",
          "mpegversion", G_TYPE_INT, 4, NULL);
      break;

    case GST_STM_AUDIO_MLP:
      caps = gst_caps_new_empty_simple ("audio/x-mlp-pes");
      break;

    case GST_STM_AUDIO_DRA:
      caps = gst_caps_new_empty_simple ("audio/x-dra-pes");
      break;

    case GST_STM_AUDIO_LPCMB:
      caps = gst_caps_new_empty_simple ("audio/x-private-ts-lpcm-pes");
      break;

    case GST_STM_AUDIO_LPCM:
      caps = gst_caps_new_empty_simple ("audio/x-private-lpcm-pes");
      break;

    default:
      caps = gst_stm_demux_map_caps ((guint)
          gst_stm_demux_get_gststm_codec_type (content, codec));
      break;
  }

  if (caps == NULL) {
    GST_LOG ("No caps found for codec_id=%d", codec);
    return caps;
  }

  if (stream->is_protected) {
    if (!gst_stm_ts_demux_configure_protected_caps (demux, stream, caps)) {
      GST_ERROR_OBJECT (demux, "Failed to configure protected stream caps.");
      gst_caps_unref (caps);
      caps = NULL;
    }
  }
  return caps;
}

GstClockTime
gst_stm_ts_demux_time_to_gst (guint64 time)
{
  GstClockTime gst_time = GST_CLOCK_TIME_NONE;
  guint64 millisec;

  millisec = time / 90;
  gst_time = gst_util_uint64_scale_int ((guint64) millisec, GST_SECOND, 1000);

  return gst_time;
}

guint64
gst_stm_time_gst_to_ts_demux (GstClockTime gst_time)
{
  guint64 time = 0;

  if (gst_time == GST_CLOCK_TIME_NONE) {
    return time;
  }

  time = gst_util_uint64_scale_int (gst_time, 1000, GST_SECOND);

  return time;
}

guint64
gst_stm_ts_demux_pts_diff (Gststm_ts_demux * demux)
{
  guint64 diff;

  if (demux->last_pts >= demux->first_pts) {
    diff = demux->last_pts - demux->first_pts;
  } else {
    diff = demux->last_pts + (INVALID_PTS_VALUE - demux->first_pts);
  }

  return diff;
}

guint
gst_stm_ts_demux_get_time_in_ms (void)
{
  struct timeval tv;

  gettimeofday (&tv, NULL);

  return ((tv.tv_sec * 1000) + (tv.tv_usec / 1000));
}
