/* Gstreamer ST TS Demuxer Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:element-stm_ts_demux
 *
 * Trickmode operations for TS packets.
 *
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <string.h>
#include <unistd.h>

#include <linux/dvb/stm_ioctls.h>

#include "gststm-tsdemux.h"
#include "gststm-tsdemux-utils.h"
#include "gststm-file.h"

static guint8 scan_buffer[4 * GST_STM_TS_DEMUX_TS_PACKET_SIZE];
static guint8 gst_stm_ts_demux_stuff_packet[] = {
  0x47, 0x00, 0x00, 0x10, 0x00, 0x00, 0x01, 0xE0, 0x00, 0xB2, 0x80, 0x00,
  0x00, 0xAD, 0xBE, 0xEF, 0xBE, 0xEF, 0xDE, 0xAD, 0xDE, 0xAD, 0xBE, 0xEF, 0xBE,
  0xEF, 0xDE, 0xAD,
  0xDE, 0xAD, 0xBE, 0xEF, 0xBE, 0xEF, 0xDE, 0xAD, 0xDE, 0xAD, 0xBE, 0xEF, 0xBE,
  0xEF, 0xDE, 0xAD,
  0xDE, 0xAD, 0xBE, 0xEF, 0xBE, 0xEF, 0xDE, 0xAD, 0xDE, 0xAD, 0xBE, 0xEF, 0xBE,
  0xEF, 0xDE, 0xAD,
  0xDE, 0xAD, 0xBE, 0xEF, 0xBE, 0xEF, 0xDE, 0xAD, 0xDE, 0xAD, 0xBE, 0xEF, 0xBE,
  0xEF, 0xDE, 0xAD,
  0xDE, 0xAD, 0xBE, 0xEF, 0xBE, 0xEF, 0xDE, 0xAD, 0xDE, 0xAD, 0xBE, 0xEF, 0xBE,
  0xEF, 0xDE, 0xAD,
  0xDE, 0xAD, 0xBE, 0xEF, 0xBE, 0xEF, 0xDE, 0xAD, 0xDE, 0xAD, 0xBE, 0xEF, 0xBE,
  0xEF, 0xDE, 0xAD,
  0xDE, 0xAD, 0xBE, 0xEF, 0xBE, 0xEF, 0xDE, 0xAD, 0xDE, 0xAD, 0xBE, 0xEF, 0xBE,
  0xEF, 0xDE, 0xAD,
  0xDE, 0xAD, 0xBE, 0xEF, 0xBE, 0xEF, 0xDE, 0xAD, 0xDE, 0xAD, 0xBE, 0xEF, 0xBE,
  0xEF, 0xDE, 0xAD,
  0xDE, 0xAD, 0xBE, 0xEF, 0xBE, 0xEF, 0xDE, 0xAD, 0xDE, 0xAD, 0xBE, 0xEF, 0xBE,
  0xEF, 0xDE, 0xAD,
  0xDE, 0xAD, 0xBE, 0xEF, 0xBE, 0xEF, 0xDE, 0xAD, 0xDE, 0xAD, 0xBE, 0xEF, 0xBE,
  0xEF, 0xDE, 0xAD,
  0xDE, 0xAD, 0xBE, 0xEF, 0xBE, 0xEF, 0xDE, 0xAD, 0xDE, 0xAD, 0xBE, 0xEF, 0xBE,
  0xEF, 0xDE, 0xAD
};

/* For smooth reverse playback */
static gboolean gst_stm_ts_demux_reverse_generic (Gststm_ts_demux * demux);
static gboolean gst_stm_ts_demux_reverse_write (Gststm_ts_demux * demux,
    guint64 start_offset, guint64 end_offset, guint pid);
static gboolean gst_stm_ts_demux_find_pusi_packet (Gststm_ts_demux * demux,
    GstBuffer * buf, guint pid, guint64 * file_offset);
static gboolean gst_stm_ts_demux_copy_pes_to_scan_buffer (guint offset,
    guint8 * data, guint pid, guint * copy_size);
static gboolean gst_stm_ts_demux_find_keyframe (Gststm_ts_stream * stream,
    guint scan_size);
static gboolean gst_stm_ts_demux_find_keyframe_mpeg (guint8 * data, guint size);
static gboolean gst_stm_ts_demux_find_keyframe_h264 (guint8 * data, guint size);
static gboolean gst_stm_ts_demux_find_keyframe_vc1 (guint8 * data, guint size);
static gboolean gst_stm_ts_demux_get_last_packet (Gststm_ts_demux * demux,
    GstBuffer * buf, guint pid, guint8 * last_packet);
static void gst_stm_ts_demux_inject_discontinuity (Gststm_ts_demux * demux,
    guint8 * last_packet);
static gboolean gst_stm_ts_demux_find_keyframe_hevc (guint8 * data, guint size);

gboolean
gst_stm_ts_demux_reverse (Gststm_ts_demux * demux)
{
  Gststm_ts_stream *stream;
  gboolean ret = FALSE;

  stream = demux->streams[demux->ref_index];

  switch (stream->codec) {
      /* Supported codec */
    case VIDEO_ENCODING_MPEG1:
    case VIDEO_ENCODING_MPEG2:
    case VIDEO_ENCODING_H264:
    case VIDEO_ENCODING_VC1:
    case VIDEO_ENCODING_HEVC:
      ret = gst_stm_ts_demux_reverse_generic (demux);
      break;
    default:
      ret = FALSE;
  }
  return ret;
}

static gboolean
gst_stm_ts_demux_reverse_generic (Gststm_ts_demux * demux)
{
  GstBuffer *buf = NULL;
  Gststm_ts_stream *stream;
  guint i, j;
  guint packet_start;
  guint picture_offset;
  guint scan_size = 0;
  guint read_size;
  guint copy_size;
  guint8 *data = NULL;
  guint64 file_offset;
  guint64 pusioffset;
  gboolean pusi_found = FALSE;
  gboolean keyframe_found = FALSE;
  gint read_offset = 0;
  GstMapInfo info_read;
  guint packet_size = 0;
  guint packets_start = 0;
  guint num_packets = 0;
  guint remain_size = 0;
  guint remain_offset = 0;

  /* First get the reference stream */
  stream = demux->streams[demux->ref_index];

  /* Search for payload unit start indicator */
  file_offset = demux->offset;
  while ((file_offset > demux->streamoffset)
      && (keyframe_found == FALSE)) {
    pusi_found =
        gst_stm_ts_demux_find_pusi_packet (demux, buf, stream->pid,
        &file_offset);
    if (pusi_found == FALSE) {
      break;
    }

    pusioffset = file_offset;
    read_size = demux->packetsize * NUMBER_PACKETS;
    /* Don't read more than last offset, no overlap of scanning */
    if (read_size > (demux->offset - file_offset)) {
      read_size = demux->offset - file_offset;
    }
    if (gst_stm_ts_demux_read (demux, &buf, file_offset, read_size,
            FALSE) == FALSE) {
      GST_WARNING_OBJECT (demux,
          "read failed for keyframe scan in reverse trick mode");
      return FALSE;
    }

    gst_buffer_map (buf, &info_read, GST_MAP_READ);
    data = info_read.data;
    read_size = info_read.size;

    picture_offset = 0;
    /* bugzilla 24974, segmentation fault in reverse trick mode; 
       read_offset taken as signed to avoid wrap around when negative */
    read_offset = read_size - (4 * demux->packetsize);
    /* Scan this buffer for a keyframe */
    while ((keyframe_found == FALSE) && (read_offset > 0)
        && (picture_offset < read_offset)) {
      scan_size = 0;
      copy_size = 0;
      /* Demux 4 packets into a temporary scan buffer */
      for (i = j = 0;
          (i < 4) && (j < ((read_size - picture_offset) / demux->packetsize));
          j++) {
        /* Calculate ts packet offset */
        packet_start =
            picture_offset + (j * demux->packetsize) + demux->packetoffset;
        /* Copy pes to the scan buffer */
        if (gst_stm_ts_demux_copy_pes_to_scan_buffer (scan_size,
                (data + packet_start), stream->pid, &copy_size) == TRUE) {
          i++;
          scan_size += copy_size;
        }
      }

      /* Assume that we need 3 bytes for start code 0x00 0x00 0x01
         and checking the following 3 bytes should be good for all codec.
         For more explanation about why 3 bytes are needed after start code,
         please refer to gst_stm_ts_demux_find_keyframe_hevc()function */
      if (scan_size < 6) {
        /* Not enough data */
        break;
      }
      picture_offset += 3 * demux->packetsize;

      /* Check for key frames in the scan buffer */
      if (gst_stm_ts_demux_find_keyframe (stream, scan_size) == TRUE) {
        keyframe_found = TRUE;
      }
    }
    gst_buffer_unmap (buf, &info_read);
    gst_buffer_unref (buf);
    buf = NULL;
    /* scan and inject valid packets, don't inject corrupted ones */
    if (keyframe_found == TRUE) {
      if (gst_stm_ts_demux_read (demux, &buf, file_offset,
              demux->offset - file_offset, FALSE) == FALSE) {
        GST_WARNING_OBJECT (demux,
            "read failed after keyframe detection for packet scan");
        return FALSE;
      }
      gst_buffer_map (buf, &info_read, GST_MAP_READ);
      data = info_read.data;
      read_size = info_read.size;
      packets_start = 0;
      packet_size = 0;
      num_packets = 0;
      remain_offset = 0;
      remain_size = read_size;
      while (remain_size) {
        if (gst_stm_ts_demux_scan_valid_packets (demux, data + remain_offset,
                remain_size, &packet_size, &packets_start,
                &num_packets, FALSE) == FALSE) {
          remain_size = 0;
          break;
        }
        /* write the complete GOP into the demux from valid packet */
        if (gst_stm_ts_demux_reverse_write (demux, file_offset + packets_start,
                (file_offset + packets_start) + (packet_size * num_packets),
                stream->pid) == FALSE) {
          GST_WARNING_OBJECT (demux, "write failed for reverse trickmode");
          gst_buffer_unmap (buf, &info_read);
          gst_buffer_unref (buf);
          return FALSE;
        }
        remain_size -= (packet_size * num_packets) - packets_start;
        remain_offset += packets_start + (packet_size * num_packets);
        file_offset += packets_start + (packet_size * num_packets);
      }
      demux->offset = pusioffset;
      gst_buffer_unmap (buf, &info_read);
      gst_buffer_unref (buf);
      buf = NULL;
    }
  }

  /* Finished search */
  if (keyframe_found == FALSE) {
    demux->offset = demux->streamoffset;

    /* Resume from here */
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_reverse_write (Gststm_ts_demux * demux, guint64 start_offset,
    guint64 end_offset, guint pid)
{
  GstBuffer *buf = NULL;
  guint8 last_packet[BD_TP_PACKET_SIZE];
  guint transfer, read_size;
  guint64 offset;
  GstMapInfo info_read;

  demux->offset = start_offset;
  read_size = demux->packetsize * NUMBER_PACKETS;

  memset (last_packet, 0, BD_TP_PACKET_SIZE);
  /* Inject till end offset */
  while (demux->offset < end_offset) {

    /* Get the ts bytes to read */
    transfer = end_offset - demux->offset;
    if (transfer > read_size) {
      transfer = read_size;
    }

    if (demux->offset == start_offset) {
      /* Seek to the correct offset for the first time */
      offset = demux->offset;
    } else {
      /* Subsequent read should not seek to save time */
      offset = GST_STM_TS_DEMUX_NO_SEEK;
    }

    /* Read ts packets */
    if (gst_stm_ts_demux_read (demux, &buf, offset, transfer, FALSE) == FALSE) {
      GST_WARNING_OBJECT (demux, "push mode read failed");
      return FALSE;
    }
    gst_buffer_map (buf, &info_read, GST_MAP_READ);
    if (demux->flushing) {
      gst_buffer_unmap (buf, &info_read);
      gst_buffer_unref (buf);
      return FALSE;
    }
    /* If we have some data, write to dvr */
    if (info_read.size) {
      write (demux->dvr_fd, info_read.data, info_read.size);
      /* Get last video packet */
      gst_stm_ts_demux_get_last_packet (demux, buf, pid, last_packet);
      demux->offset += info_read.size;
      gst_buffer_unmap (buf, &info_read);
      gst_buffer_unref (buf);
      buf = NULL;
    } else {
      gst_buffer_unmap (buf, &info_read);
      gst_buffer_unref (buf);
      buf = NULL;
      break;
    }
  }

  /* Shift back to the start of the GOP */
  demux->offset = start_offset;

  gst_stm_ts_demux_inject_discontinuity (demux, last_packet);

  return TRUE;
}

static gboolean
gst_stm_ts_demux_find_pusi_packet (Gststm_ts_demux * demux, GstBuffer * buf,
    guint pid, guint64 * file_offset)
{
  guint8 *data = NULL;
  guint8 *scandata = NULL;
  guint8 *headerdata = NULL;
  guint header = 0;
  guint data_length = 0;
  GstMapInfo info_read;
  guint transfer = 0;
  guint packet_size = 0;
  guint packets_start = 0;
  guint packet_count = 0;
  guint packetpos = 0;
  guint num_packets = 0;
  guint scanlength = 0;
  guint ts_groupcnt = 0;
  guint ts_groupbytes = 0;
  gboolean pusi_found = FALSE;

  if (file_offset == NULL) {
    GST_WARNING_OBJECT (demux, "invalid params");
    return FALSE;
  }
  /* For now it is only supporting reverse search */
  if (demux->segment.rate > 0) {
    GST_WARNING_OBJECT (demux, "only supports reverse search");
    return FALSE;
  }
  while ((*file_offset > demux->streamoffset) && (pusi_found == FALSE)) {
    /* Number of ts packets to jump in backwards */
    /* packet allignment as per packet validation */
    transfer = demux->packetsize * NUMBER_SCAN_PACKETS;
    if (transfer > (*file_offset - demux->streamoffset)) {
      transfer = *file_offset - demux->streamoffset;
    }
    *file_offset -= transfer;

    if (gst_stm_ts_demux_read (demux, &buf, *file_offset, transfer,
            FALSE) == FALSE) {
      return FALSE;
    }
    if (buf == NULL) {
      return FALSE;
    }

    gst_buffer_map (buf, &info_read, GST_MAP_READ);
    data_length = info_read.size;
    data = info_read.data;

    /* Scan packets for PUSI packet, starting from the last packet -> first packet */
    /* search pusi by scanning only valid packets until not found */
    scanlength = SCAN_COUNT * demux->packetsize;
    ts_groupcnt = 1;
    while ((pusi_found == FALSE) && (ts_groupcnt * scanlength) <= data_length) {
      ts_groupbytes = ts_groupcnt * scanlength;
      scandata = data + data_length - ts_groupbytes;
      packet_size = 0;
      packets_start = 0;
      num_packets = 0;
      if (gst_stm_ts_demux_scan_valid_packets (demux, scandata, scanlength,
              &packet_size, &packets_start, &num_packets, FALSE) != FALSE) {
        for (packet_count = num_packets; packet_count > 0; packet_count--) {
          packetpos = (packet_count - 1) * packet_size;
          headerdata = scandata + packets_start + packetpos;
          header = *((guint *) (&headerdata[demux->packetoffset]));
          if (PID (header) != pid) {
            continue;
          }
          pusi_found = PUSI (header);
          if (pusi_found) {
            *file_offset +=
                data_length - ts_groupbytes + packets_start + packetpos;
            break;
          }
        }
      }
      ts_groupcnt += 1;
    }
  }
  gst_buffer_unmap (buf, &info_read);
  gst_buffer_unref (buf);
  if (pusi_found == FALSE) {
    return FALSE;
  } else {
    return TRUE;
  }
}

static gboolean
gst_stm_ts_demux_copy_pes_to_scan_buffer (guint offset, guint8 * data,
    guint pid, guint * copy_size)
{
  guint header;
  guint data_offset = 0;

  if (data == NULL) {
    GST_WARNING ("invalid params");
    return FALSE;
  }

  header = *((guint *) data);

  if (PID (header) != pid) {
    return FALSE;
  }
  data_offset = GST_STM_TS_DEMUX_TS_HEADER_SIZE;
  if (ADAPTATION_FIELD (header)) {
    data_offset += 1 + data[GST_STM_TS_DEMUX_TS_HEADER_SIZE];
  }
  if (PAYLOAD_PRESENT (header)
      && (data_offset < GST_STM_TS_DEMUX_TS_PACKET_SIZE)) {

    /* Not packet size because 188 is still the usable portion of the packet */
    *copy_size = (GST_STM_TS_DEMUX_TS_PACKET_SIZE - data_offset);

    /* Copy the pes data to the scan buffer */
    memcpy (scan_buffer + offset, data + data_offset, *copy_size);
  }

  return TRUE;
}

static gboolean
gst_stm_ts_demux_find_keyframe (Gststm_ts_stream * stream, guint scan_size)
{
  gboolean ret;

  /* Find keyframe in the scan buffer */
  switch (stream->codec) {
    case VIDEO_ENCODING_MPEG1:
    case VIDEO_ENCODING_MPEG2:
      ret = gst_stm_ts_demux_find_keyframe_mpeg (scan_buffer, scan_size);
      break;
    case VIDEO_ENCODING_H264:
      ret = gst_stm_ts_demux_find_keyframe_h264 (scan_buffer, scan_size);
      break;
    case VIDEO_ENCODING_VC1:
      ret = gst_stm_ts_demux_find_keyframe_vc1 (scan_buffer, scan_size);
      break;
    case VIDEO_ENCODING_HEVC:
      ret = gst_stm_ts_demux_find_keyframe_hevc (scan_buffer, scan_size);
      break;
    default:
      ret = FALSE;
      break;
  }

  return ret;
}

static gboolean
gst_stm_ts_demux_find_keyframe_mpeg (guint8 * data, guint size)
{
  guint i;

  /* Look for sequence header 0xB3 */
  for (i = 0; (i < (size - 4)); i++) {
    if ((data[i] == 0) && (data[i + 1] == 0)
        && (data[i + 2] == 1) && (data[i + 3] == SEQUENCE_HEADER)) {
      return TRUE;
    }
  }

  return FALSE;
}

static gboolean
gst_stm_ts_demux_find_keyframe_h264 (guint8 * data, guint size)
{
  guint i;

  for (i = 0; (i < (size - 5)); i++) {
    if ((data[i] == 0) && (data[i + 1] == 0)
        && (data[i + 2] == 1) && SliceOrIDR (data[i + 3])
        && IFrameUnit (data[i + 4])) {
      return TRUE;
    }
  }

  return FALSE;
}

static gboolean
gst_stm_ts_demux_find_keyframe_vc1 (guint8 * data, guint size)
{
  guint i;

  for (i = 0; (i < (size - 4)); i++) {
    if ((data[i] == 0) && (data[i + 1] == 0)
        && (data[i + 2] == 1) && (data[i + 3] == VC1_SEQUENCE_START_CODE)) {
      return TRUE;
    }
  }

  return FALSE;
}

/*
 function: gst_stm_ts_demux_find_keyframe_hevc
 input params:
          data: chunk containing video data
          size: size of chunk
 description:
   T-REC-H.265-201304-I!!PDF-E.pdf
   Table 7-1 NAL unit type codes and NAL unit type classes
   For IRAP pictures, following nal_unit_types are searched for:
   BLA_W_LP     16
   BLA_W_RADL   17
   BLA_N_LP     18
   IDR_W_RADL   19
   IDR_N_LP     20
   CRA_NUT      21

   Table 7.3.1.2 Nal unit header syntax
   forbidden zero bit | nal_unit_type u(6) | nuh_layer_id (u6) | nuh_temporalid_plus1 u(3)
   Currently spec specifies that nuh_layer_id should be 0.
   temporalid should be zero.
   first_slice_segment_in_pic_flag is first bit of slice_segment_header
   and should be 1, signifying that the slice segment is the first slice
   segment of the picture in decoding order.
 return: TRUE if successful else FALSE.
 */
static gboolean
gst_stm_ts_demux_find_keyframe_hevc (guint8 * data, guint size)
{
  guint i;

  for (i = 0; (i < (size - 5)); i++) {
    if ((data[i] == 0) && (data[i + 1] == 0)
        && (data[i + 2] == 1) && IDRorCRAorBLA (data[i + 3])
        && FirstISlice (data[i + 5])) {
      return TRUE;
    }
  }

  return FALSE;
}


static gboolean
gst_stm_ts_demux_get_last_packet (Gststm_ts_demux * demux, GstBuffer * buf,
    guint pid, guint8 * last_packet)
{
  guint8 *data;
  gint i;
  guint data_length;
  guint header;
  GstMapInfo info_read;
  gboolean ret = FALSE;

  if ((demux == NULL) || (buf == NULL) || (last_packet == NULL)) {
    GST_WARNING_OBJECT (demux, "invalid params");
    return FALSE;
  }

  gst_buffer_map (buf, &info_read, GST_MAP_READ);
  data_length = info_read.size;
  data = info_read.data;

  if (data_length > demux->packetsize) {
    i = data_length - demux->packetsize;
  } else {
    i = 0;
  }

  /* Find last specific ts packet, normally video packet */
  for (; i >= 0; i -= demux->packetsize) {
    header =
        data[i + demux->packetoffset] | (data[i + demux->packetoffset +
            1] << 8) | (data[i + demux->packetoffset + 2] << 16) | (data[i +
            demux->packetoffset + 3] << 24);
    if (PID (header) == pid) {
      /* Copy the ts packet */
      memcpy (last_packet, (data + i), demux->packetsize);
      ret = TRUE;
      goto error;
    }
  }

error:
  gst_buffer_unmap (buf, &info_read);
  return ret;
}

static void
gst_stm_ts_demux_inject_discontinuity (Gststm_ts_demux * demux,
    guint8 * last_packet)
{
  guint8 last_cc;
  guint8 dummy[BD_TP_PACKET_SIZE];
  gint i;

  if (last_packet == NULL) {
    GST_WARNING_OBJECT (demux, "invalid params");
    return;
  }

  last_cc = last_packet[demux->packetoffset + 3] & 0x0f;
  last_cc++;
  last_cc = last_cc % 16;

  /* Inject extra packet to detect end of pes when reading from demux */
  memset (dummy, 0, BD_TP_PACKET_SIZE);
  memcpy ((dummy + demux->packetoffset), gst_stm_ts_demux_stuff_packet,
      sizeof (gst_stm_ts_demux_stuff_packet));

  /* Copy PID value */
  dummy[demux->packetoffset + 1] = last_packet[demux->packetoffset + 1] | 0x40;
  dummy[demux->packetoffset + 2] = last_packet[demux->packetoffset + 2];
  dummy[demux->packetoffset + 3] |= last_cc;
  demux->disc_read = FALSE;
  /* We have to inject twice ?? */
  for (i = 0; i < 2; i++) {
    if (write (demux->dvr_fd, dummy, demux->packetsize) < 0) {
      GST_WARNING_OBJECT (demux, "failed to write dummy");
    }
  }
}
