/* Copyright (C) 2012-2013 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <sys/ioctl.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "gststbufferpool.h"
#include "v4l2_utils.h"

#define DEFAULT_NAME_SIZE 128

#if 0
/* A useful debugging helper */
#ifndef ioctl
#define ioctl(fd,ioctlno,args...) \
  ({int ioctl_ret; do { \
    GST_DEBUG_OBJECT (pool, "Calling ioctl "#ioctlno); \
    ioctl_ret = ioctl(fd,ioctlno,##args); \
    GST_DEBUG_OBJECT (pool, "ioctl "#ioctlno" return: %d", ioctl_ret); \
  } while(0); \
  ioctl_ret;})
#endif
#endif

GST_DEBUG_CATEGORY_STATIC (gst_st_buffer_pool_debug);
#define GST_CAT_DEFAULT gst_st_buffer_pool_debug

G_DEFINE_TYPE (GstEncodeBuffer, gst_encode_buffer, GST_TYPE_OBJECT);
G_DEFINE_TYPE (GstEncodeBufferPool, gst_encode_buffer_pool,
    GST_TYPE_BUFFER_POOL);

static void gst_encode_buffer_pool_finalize (GObject * object);
static void gst_encode_buffer_pool_log_free_buffers (GstEncodeBufferPool *
    pool);
GstFlowReturn gst_encode_buffer_accquire (GstBufferPool * pool,
    GstBuffer ** buffer, GstBufferPoolAcquireParams * params);
static void gst_encode_buffer_meta_free (GstEncodeBufferMeta * meta,
    GstBuffer * buffer);
static void gst_encode_buffer_finalize (GObject * object);

#define LOCK_V4LBUFFS(pool) G_STMT_START {                               \
    GST_LOG_OBJECT (pool, "locking objects_lock from thread %p",         \
        g_thread_self());                                                \
    g_mutex_lock (&pool->v4lbuffs_lock);                          \
    GST_LOG_OBJECT (pool, "locked objects_lock from thread %p",          \
        g_thread_self());                                                \
  } G_STMT_END

#define UNLOCK_V4LBUFFS(pool) G_STMT_START {                             \
    GST_LOG_OBJECT (pool, "unlocking objects_lock from thread %p",       \
        g_thread_self());                                                \
    g_mutex_unlock (&pool->v4lbuffs_lock);                        \
  } G_STMT_END

GType
gst_encodebuffer_meta_api_get_type (void)
{
  static volatile GType type;
  static const gchar *tags[] = { "EncodeBufferMeta", NULL };
  if (g_once_init_enter (&type)) {
    GType _type = gst_meta_api_type_register ("GstEncodeBufferMetaAPI", tags);
    g_once_init_leave (&type, _type);
  }
  return type;
}

static void
gst_encode_buffer_meta_free (GstEncodeBufferMeta * meta, GstBuffer * buffer)
{
  if ((meta == NULL) || (meta->bufferinfo == NULL))
    return;

  if (meta->bufferinfo->v4l2_buf.type == V4L2_BUF_TYPE_VIDEO_CAPTURE) {
    /* Release output buffer */
    if (ioctl (meta->fd, VIDIOC_QBUF, &meta->bufferinfo->v4l2_buf) < 0) {
      GST_ERROR ("VIDIOC_QBUF failed %d, %s \n", meta->fd, strerror (errno));
      return;
    }
  }
  meta->bufferinfo->isUsed = FALSE;
  meta->bufferinfo->isQueued = TRUE;
}

const GstMetaInfo *
gst_encodebuffer_meta_get_info (void)
{
  static const GstMetaInfo *encodebuffer_meta_info = NULL;

  if (g_once_init_enter (&encodebuffer_meta_info)) {
    const GstMetaInfo *meta = gst_meta_register (GST_ENCODEBUFFER_META_API_TYPE,
        "EncodeBufferMeta",
        sizeof (GstEncodeBufferMeta), (GstMetaInitFunction) NULL,
        (GstMetaFreeFunction) gst_encode_buffer_meta_free,
        (GstMetaTransformFunction) NULL);
    g_once_init_leave (&encodebuffer_meta_info, meta);
  }
  return encodebuffer_meta_info;
}

/* Functions to handle new buffer type */
static void
gst_encode_buffer_class_init (GstEncodeBufferClass * g_class)
{
  GObjectClass *gobject_class = (GObjectClass *) (g_class);
  gobject_class->finalize = gst_encode_buffer_finalize;
}

static void
gst_encode_buffer_init (GstEncodeBuffer * buf)
{
}

/* This function will be called when the buffer will be unreffed */
static void
gst_encode_buffer_finalize (GObject * object)
{
  GstEncodeBuffer *encode_buffer = (GstEncodeBuffer *) object;
  if (encode_buffer->pool) {
    LOCK_V4LBUFFS (encode_buffer->pool);
  }

  if (encode_buffer->buffer->v4l2_buf.type == V4L2_BUF_TYPE_VIDEO_CAPTURE) {
    /* Release output buffer */
    GST_DEBUG ("finalizing video capture buffer");
    if ((ioctl) (encode_buffer->fd, VIDIOC_QBUF,
            &encode_buffer->buffer->v4l2_buf) < 0) {
      GST_ERROR ("VIDIOC_QBUF failed");
      goto beach;
    }
    encode_buffer->buffer->isUsed = FALSE;
    encode_buffer->buffer->isQueued = TRUE;
  } else if (encode_buffer->buffer->v4l2_buf.type == V4L2_BUF_TYPE_DVB_CAPTURE) {
    GST_DEBUG ("finalizing DVB capture buffer");
    encode_buffer->buffer->v4l2_buf.bytesused = 0;
    if ((ioctl) (encode_buffer->fd, VIDIOC_QBUF,
            &encode_buffer->buffer->v4l2_buf) < 0) {
      GST_ERROR ("VIDIOC_QBUF failed");
      goto beach;
    }
    g_free (encode_buffer->buffer);
  } else {
    GST_DEBUG ("finalizing other buffer");
    encode_buffer->buffer->isUsed = FALSE;
    encode_buffer->buffer->isQueued = FALSE;
  }

  if (encode_buffer->pool)
    gst_encode_buffer_pool_log_free_buffers (encode_buffer->pool);

  if (encode_buffer->pool) {
    UNLOCK_V4LBUFFS (encode_buffer->pool);
  }

  /* Only unref the pool if the buffer was created as part of one */
  if (encode_buffer->pool)
    gst_object_unref (encode_buffer->pool);

  return;

beach:
  if (encode_buffer->pool) {
    UNLOCK_V4LBUFFS (encode_buffer->pool);
  }
}

/* Functions to handle new buffer pool type */
static void
gst_encode_buffer_pool_log_free_buffers (GstEncodeBufferPool * pool)
{
  char msg[1024] = "";
  int i, free_buffers = 0;

  g_snprintf (msg, sizeof (msg), "%s [ ", pool->name);
  for (i = 0; i < pool->num_buffers; i++) {
    if (pool->buffers[i].isUsed == FALSE) {
      g_snprintf (msg, sizeof (msg) - strnlen (msg, 1024), ". ");
      ++free_buffers;
    } else {
      g_snprintf (msg, sizeof (msg) - strnlen (msg, 1024), "X ");
    }
  }
  g_snprintf (msg, sizeof (msg) - strnlen (msg, 1024), "] ");
  g_snprintf (msg + strnlen (msg, 1024), sizeof (msg) - strnlen (msg, 1024),
      "%u free buffers", free_buffers);
  GST_DEBUG_OBJECT (pool, "%s", msg);
}

static void
gst_encode_buffer_pool_class_init (GstEncodeBufferPoolClass * g_class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
  GstBufferPoolClass *gstbufferpool_class = (GstBufferPoolClass *) (g_class);

  GST_DEBUG_CATEGORY_INIT (gst_st_buffer_pool_debug, "stbufferpool",
      GST_DEBUG_FG_MAGENTA, "ST encode buffer pool");

  gobject_class->finalize = gst_encode_buffer_pool_finalize;
  gstbufferpool_class->acquire_buffer = gst_encode_buffer_accquire;
}

static void
gst_encode_buffer_pool_init (GstEncodeBufferPool * pool)
{
  g_mutex_init (&pool->v4lbuffs_lock);
}

GstFlowReturn
gst_encode_buffer_accquire (GstBufferPool * pool, GstBuffer ** buffer,
    GstBufferPoolAcquireParams * params)
{
  GstEncodeBufferPool *buffer_pool = NULL;
  GstFlowReturn ret = GST_FLOW_OK;

  buffer_pool = GST_ENCODE_BUFFER_POOL_CAST (pool);
  *buffer = gst_encode_buffer_pool_get_buffer (buffer_pool, FALSE);
  if (!*buffer) {
    ret = GST_FLOW_ERROR;
  }

  return ret;
}


int
gst_encode_buffer_pool_reqbufs (GstEncodeBufferPool * pool, int nbufs)
{
  struct v4l2_requestbuffers req;

  /* Request input buffers from v4l2 */
  memset (&req, 0, sizeof (struct v4l2_requestbuffers));
  req.count = nbufs;
  req.type = pool->buffer_type;
  req.memory = V4L2_MEMORY_MMAP;

  if ((ioctl) (pool->fd, VIDIOC_REQBUFS, &req) < 0) {
    return -1;
  }

  return req.count;
}

GstEncodeBufferPool *
gst_encode_buffer_pool_create (const char *name, int fd, int nbufs,
    int buffer_type, guint userdata)
{
  GstEncodeBufferPool *pool;
  int i;

  pool = g_object_new (GST_TYPE_ENCODE_BUFFER_POOL, NULL);

  pool->fd = fd;
  pool->nbufs = 0;
  pool->buffer_type = buffer_type;
  pool->userdata = userdata;
  pool->name = g_strdup (name);

  /* Request buffers from v4l2 */
  i = gst_encode_buffer_pool_reqbufs (pool, nbufs);
  if (i < 0) {
    GST_ERROR_OBJECT (pool,
        "%s: request buffers failed - %s", name, strerror (errno));
    g_object_unref (pool);
    return NULL;
  } else {
    if (i != nbufs) {
      GST_WARNING_OBJECT (pool, "%s: request %d buffers got %d",
          name, nbufs, i);
      nbufs = i;
    }
  }

  pool->num_buffers = nbufs;
  pool->buffers = calloc (nbufs, sizeof (*pool->buffers));

  /* Guard against freeing unallocated memory if failing at init time */
  for (i = 0; i < nbufs; i++) {
    pool->buffers[i].start = ((void *) -1);
  }

  /* Query input buffers and map buffers */
  for (i = 0; i < nbufs; i++) {
    struct v4l2_buffer *buf = &(pool->buffers)[i].v4l2_buf;
    memset (buf, 0, sizeof (struct v4l2_buffer));
    buf->type = buffer_type;
    buf->memory = V4L2_MEMORY_MMAP;
    buf->index = i;
    buf->reserved = userdata;

    /* query buffer */
    if (ioctl (fd, VIDIOC_QUERYBUF, buf) < 0) {
      GST_ERROR_OBJECT (pool, "%s: query buffer failed - %s",
          pool->name, strerror (errno));
      return FALSE;
    }

    pool->buffers[i].isUsed = FALSE;
    pool->buffers[i].isQueued = FALSE;
    pool->buffers[i].start =
        (guchar *) mmap (0, pool->buffers[i].v4l2_buf.length,
        PROT_READ | PROT_WRITE, MAP_SHARED, fd,
        pool->buffers[i].v4l2_buf.m.offset);

    if (pool->buffers[i].start == ((void *) -1)) {
      GST_ERROR_OBJECT (pool, "%s: mmap buffer failed", pool->name);
      g_object_unref (pool);
      return FALSE;
    }
  }

  GST_DEBUG_OBJECT (pool, "New %s pool created, %d buffers",
      pool->name, pool->num_buffers);
  gst_encode_buffer_pool_log_free_buffers (pool);
  return pool;
}

gboolean
gst_encode_buffer_pool_free_buffers (GstEncodeBufferPool * pool)
{
  int i;
  gboolean ret = TRUE;

  LOCK_V4LBUFFS (pool);
  GST_DEBUG_OBJECT (pool, "%s: Freeing buffers %p", pool->name, pool->buffers);
  if (pool->buffers) {
    if (!gst_encode_buffer_pool_reqbufs (pool, 0)) {
      UNLOCK_V4LBUFFS (pool);
      return FALSE;
    }
    GST_DEBUG_OBJECT (pool, "%s: Releasing MMAP buffers", pool->name);
    for (i = 0; i < pool->num_buffers; i++) {
      if (pool->buffers[i].start != ((void *) -1)) {
        if (munmap (pool->buffers[i].start,
                pool->buffers[i].v4l2_buf.length) < 0)
          ret = FALSE;
        pool->buffers[i].start = ((void *) -1);
      }
    }
    free (pool->buffers);
    pool->buffers = NULL;
  }
  UNLOCK_V4LBUFFS (pool);
  return ret;
}

int
gst_encode_buffer_pool_find_free_buffer (GstEncodeBufferPool * pool,
    gboolean priority)
{
  gint i;

  LOCK_V4LBUFFS (pool);
  GST_DEBUG_OBJECT (pool, "%s: Finding free buffer, priority %d",
      pool->name, priority);
  gst_encode_buffer_pool_log_free_buffers (pool);
  for (i = priority ? 0 : 1; i < pool->num_buffers; i++) {
    if (!pool->buffers[i].isUsed) {
      GST_DEBUG_OBJECT (pool, "%s: Found free buffer at %d", pool->name, i);
      pool->buffers[i].isUsed = TRUE;
      break;
    }
  }

  gst_encode_buffer_pool_log_free_buffers (pool);
  UNLOCK_V4LBUFFS (pool);
  return i;
}

struct v4l2_buffer *
gst_encode_buffer_pool_get_raw_buffer (GstEncodeBufferPool * pool,
    gboolean priority)
{
  gint i = gst_encode_buffer_pool_find_free_buffer (pool, priority);
  if (i == pool->num_buffers) {
    GST_WARNING_OBJECT (pool, "%s: No free buffer found", pool->name);
    return NULL;
  } else {
    GST_DEBUG_OBJECT (pool, "%s: Returning free raw buffer %d", pool->name, i);
    pool->buffers[i].isQueued = TRUE;
    return &pool->buffers[i].v4l2_buf;
  }
}

GstBuffer *
gst_encode_buffer_pool_get_buffer (GstEncodeBufferPool * pool,
    gboolean priority)
{
  GstEncodeBufferMeta *meta;

  gint i = gst_encode_buffer_pool_find_free_buffer (pool, priority);
  if (i == pool->num_buffers) {
    GST_WARNING_OBJECT (pool, "%s: No free buffer found", pool->name);
    return NULL;
  } else {
    GstBuffer *buf;
    buf =
        gst_buffer_new_wrapped_full (0, pool->buffers[i].start,
        pool->buffers[i].v4l2_buf.length, 0, pool->buffers[i].v4l2_buf.length,
        pool->buffers[i].start, NULL);

    gst_buffer_set_size (buf, pool->buffers[i].v4l2_buf.length);

    meta =
        (GstEncodeBufferMeta *) gst_buffer_add_meta (buf,
        GST_ENCODEBUFFER_META_INFO, NULL);

    meta->fd = pool->fd;
    meta->bufferinfo = &pool->buffers[i];
    memset (meta->name, 0, DEFAULT_NAME_SIZE);
    g_strlcpy (meta->name, "EncodeBuffer", DEFAULT_NAME_SIZE);

    return buf;
  }
}

GstBuffer *
gst_encode_buffer_pool_get_buffer_by_index (GstEncodeBufferPool * pool,
    int index)
{
  GstEncodeBufferMeta *meta;

  LOCK_V4LBUFFS (pool);
  if (index < 0 || index >= pool->num_buffers) {
    GST_WARNING_OBJECT (pool, "%s: Buffer %i is not free", pool->name, index);
    UNLOCK_V4LBUFFS (pool);
    return NULL;
  } else {
    GstBuffer *buf;
    buf =
        gst_buffer_new_wrapped_full (0, pool->buffers[index].start,
        pool->buffers[index].v4l2_buf.length, 0,
        pool->buffers[index].v4l2_buf.length, pool->buffers[index].start, NULL);

    gst_buffer_set_size (buf, pool->buffers[index].v4l2_buf.length);

    meta =
        (GstEncodeBufferMeta *) gst_buffer_add_meta (buf,
        GST_ENCODEBUFFER_META_INFO, NULL);
    meta->fd = pool->fd;
    meta->bufferinfo = &pool->buffers[index];
    memset (meta->name, 0, DEFAULT_NAME_SIZE);
    g_strlcpy (meta->name, "EncodeBuffer", DEFAULT_NAME_SIZE);
    UNLOCK_V4LBUFFS (pool);
    return buf;
  }
}

GstBuffer *
gst_encode_buffer_pool_export (GstEncodeBufferPool * pool,
    struct v4l2_buffer * v4l2_buf)
{
  GstBuffer *buf;
  GstMapInfo info_write;

  GST_DEBUG_OBJECT (pool, "%s: exporting buffer %d", pool->name,
      v4l2_buf->index);
  buf = gst_buffer_new_and_alloc (v4l2_buf->bytesused);
  if (!buf) {
    GST_ERROR_OBJECT (pool, "%s: Failed to allocate %d byte buffer",
        pool->name, v4l2_buf->bytesused);
    return NULL;
  }

  gst_buffer_map (buf, &info_write, GST_MAP_WRITE);
  memcpy (info_write.data, pool->buffers[v4l2_buf->index].start,
      v4l2_buf->bytesused);
  gst_buffer_unmap (buf, &info_write);
  GST_BUFFER_TIMESTAMP (buf) = GST_TIMEVAL_TO_TIME (v4l2_buf->timestamp);
  GST_BUFFER_DURATION (buf) = GST_CLOCK_TIME_NONE;
  LOCK_V4LBUFFS (pool);
  pool->buffers[v4l2_buf->index].isUsed = FALSE;
  gst_encode_buffer_pool_log_free_buffers (pool);
  UNLOCK_V4LBUFFS (pool);
  return buf;
}

void
gst_encode_buffer_pool_free_raw_buffer (GstEncodeBufferPool * pool,
    struct v4l2_buffer *v4l2_buf)
{
  GST_DEBUG_OBJECT (pool, "%s: Freeing Raw buffer %d", pool->name,
      v4l2_buf->index);
  LOCK_V4LBUFFS (pool);
  pool->buffers[v4l2_buf->index].isUsed = FALSE;
  gst_encode_buffer_pool_log_free_buffers (pool);
  UNLOCK_V4LBUFFS (pool);
}

gboolean
gst_encode_buffer_pool_bump_one (GstEncodeBufferPool * pool)
{
  int i;

  LOCK_V4LBUFFS (pool);
  for (i = 0; i < pool->num_buffers; i++) {
    if (pool->buffers[i].isUsed == FALSE) {
      GST_DEBUG_OBJECT (pool, "%s: bumping buffer index %d", pool->name, i);
      pool->buffers[i].isUsed = TRUE;
      gst_encode_buffer_pool_log_free_buffers (pool);
      UNLOCK_V4LBUFFS (pool);
      return TRUE;
    }
  }
  UNLOCK_V4LBUFFS (pool);
  return FALSE;
}

static void
gst_encode_buffer_pool_finalize (GObject * object)
{
  GstEncodeBufferPool *pool;

  pool = GST_ENCODE_BUFFER_POOL (object);
  GST_DEBUG_OBJECT (pool, "%s: Finalizing buffer pool", pool->name);

  gst_encode_buffer_pool_free_buffers (pool);
  g_free (pool->name);
  g_mutex_clear (&pool->v4lbuffs_lock);

  G_OBJECT_CLASS (gst_encode_buffer_pool_parent_class)->finalize (object);
}

void
gst_encode_buffer_pool_flush_buffer (GstEncodeBufferPool * pool)
{
  gint i;

  LOCK_V4LBUFFS (pool);
  for (i = 0; i < pool->num_buffers; i++) {
    if (pool->buffers[i].isQueued) {
      pool->buffers[i].isQueued = FALSE;
      pool->buffers[i].isUsed = FALSE;
      break;
    }
  }
  UNLOCK_V4LBUFFS (pool);
}

void
gst_encode_buffer_pool_mark_dequeue_buffer_by_index (GstEncodeBufferPool * pool,
    int index)
{
  LOCK_V4LBUFFS (pool);
  if (index < 0 || index >= pool->num_buffers) {
    GST_WARNING_OBJECT (pool, "%s: Index %i is wrong", pool->name, index);
  } else {
    pool->buffers[index].isQueued = FALSE;
  }
  UNLOCK_V4LBUFFS (pool);
  return;
}

gboolean
gst_encode_buffer_pool_check_queued_buffer (GstEncodeBufferPool * pool)
{
  int i;

  LOCK_V4LBUFFS (pool);
  for (i = 0; i < pool->num_buffers; i++) {
    if (pool->buffers[i].isQueued && (pool->buffers[i].isUsed == FALSE)) {
      GST_DEBUG_OBJECT (pool, "%s: bumping buffer index %d", pool->name, i);
      pool->buffers[i].isUsed = TRUE;
      gst_encode_buffer_pool_log_free_buffers (pool);
      UNLOCK_V4LBUFFS (pool);
      return TRUE;
    }
  }
  UNLOCK_V4LBUFFS (pool);
  return FALSE;
}

struct v4l2_buffer *
gst_encode_buffer_pool_get_non_queued_buffer (GstEncodeBufferPool * pool,
    gboolean priority)
{
  int i;
  LOCK_V4LBUFFS (pool);
  for (i = 0; i < pool->num_buffers; i++) {
    if (!pool->buffers[i].isQueued) {
      pool->buffers[i].isQueued = TRUE;
      UNLOCK_V4LBUFFS (pool);
      return &pool->buffers[i].v4l2_buf;
    }
  }
  UNLOCK_V4LBUFFS (pool);
  return NULL;
}
