/*
 * GStreamer
 * Copyright (C) 2005 Thomas Vander Stichele <thomas@apestaart.org>
 * Copyright (C) 2005 Ronald S. Bultje <rbultje@ronald.bitfreak.net>
 * Copyright (C) 2011-2013 ST Microelectronics <<user@hostname.org>>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_STBUFFERPOOL_H__
#define __GST_STBUFFERPOOL_H__

#include <gst/gst.h>
#include <linux/videodev2.h>
#include "linux/dvb/dvb_v4l2_export.h"

struct buffer_t
{
  struct v4l2_buffer v4l2_buf;
  void *start;
  gboolean isUsed;
  gboolean isQueued;
};

#define  DUMMY_POOL_SIZE  0xFFFFFFFF

/**
 *
 *
 * Subclass of #GstMeta containing additional information about a encode buffer 
 */
typedef struct _GstEncodeBufferMeta GstEncodeBufferMeta;
struct _GstEncodeBufferMeta
{
  GstMeta meta;
  struct buffer_t *bufferinfo;
  guint fd;
  gchar name[256];
};

/* Declaration of new buffer type */
typedef struct _GstEncodeBuffer GstEncodeBuffer;
typedef struct _GstEncodeBufferClass GstEncodeBufferClass;

GType gst_encodebuffer_meta_api_get_type (void);
#define GST_ENCODEBUFFER_META_API_TYPE  (gst_encodebuffer_meta_api_get_type())
const GstMetaInfo *gst_encodebuffer_meta_get_info (void);
#define GST_ENCODEBUFFER_META_INFO  (gst_encodebuffer_meta_get_info())

#define gst_buffer_get_encodebuffer_meta(b) ((GstEncodeBufferMeta*)gst_buffer_get_meta((b),GST_ENCODEBUFFER_META_API_TYPE))


#define GST_TYPE_ENCODE_BUFFER            (gst_encode_buffer_get_type())

#define GST_IS_ENCODE_BUFFER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_ENCODE_BUFFER))
#define GST_IS_ENCODE_BUFFER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_ENCODE_BUFFER))
#define GST_ENCODE_BUFFER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_ENCODE_BUFFER, GstEncodeBufferClass))
#define GST_ENCODE_BUFFER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_ENCODE_BUFFER, GstEncodeBuffer))
#define GST_ENCODE_BUFFER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_ENCODE_BUFFER, GstEncodeBufferClass))

typedef struct _GstEncodeBufferPool GstEncodeBufferPool;
typedef struct _GstEncodeBufferPoolClass GstEncodeBufferPoolClass;

#define GST_TYPE_ENCODE_BUFFER_POOL            (gst_encode_buffer_pool_get_type())

#define GST_IS_ENCODE_BUFFER_POOL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_ENCODE_BUFFER_POOL))
#define GST_IS_ENCODE_BUFFER_POOL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_ENCODE_BUFFER_POOL))
#define GST_ENCODE_BUFFER_POOL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_ENCODE_BUFFER_POOL, GstEncodeBufferPoolClass))
#define GST_ENCODE_BUFFER_POOL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_ENCODE_BUFFER_POOL, GstEncodeBufferPool))
#define GST_ENCODE_BUFFER_POOL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_ENCODE_BUFFER_POOL, GstEncodeBufferPoolClass))
#define GST_ENCODE_BUFFER_POOL_CAST(obj) ((GstEncodeBufferPool*)(obj))

struct _GstEncodeBuffer
{
  GstBuffer gst_buf;
  guint fd;
  struct buffer_t *buffer;
  GstEncodeBufferPool *pool;
};

struct _GstEncodeBufferClass
{
  GstObjectClass parent_class;
};

GType gst_encode_buffer_get_type (void);

struct _GstEncodeBufferPool
{
  GstBufferPool bufferpool;

  int fd;
  int nbufs;
  int buffer_type;
  guint userdata;
  char *name;

  GMutex v4lbuffs_lock;

  guint num_buffers;
  struct buffer_t *buffers;
};

struct _GstEncodeBufferPoolClass
{
  GstBufferPoolClass parent_class;
};

GstEncodeBufferPool *gst_encode_buffer_pool_create (const char *name, int fd,
    int nbufs, int type, guint userdata);
struct v4l2_buffer *gst_encode_buffer_pool_get_raw_buffer (GstEncodeBufferPool *
    pool, gboolean priority);
GstBuffer *gst_encode_buffer_pool_get_buffer (GstEncodeBufferPool * pool,
    gboolean priority);
GstBuffer *gst_encode_buffer_pool_get_buffer_by_index (GstEncodeBufferPool *
    pool, int index);
GstBuffer *gst_encode_buffer_pool_export (GstEncodeBufferPool * pool,
    struct v4l2_buffer *v4l2_buf);
gboolean gst_encode_buffer_pool_bump_one (GstEncodeBufferPool * pool);
void gst_encode_buffer_pool_free_raw_buffer (GstEncodeBufferPool * pool,
    struct v4l2_buffer *v4l2_buf);

GType gst_encode_buffer_pool_get_type (void);
GstBufferPool *gst_encode_buffer_pool_new (void);

void gst_encode_buffer_pool_flush_buffer (GstEncodeBufferPool * pool);
void gst_encode_buffer_pool_mark_dequeue_buffer_by_index (GstEncodeBufferPool *
    pool, int index);
gboolean gst_encode_buffer_pool_check_queued_buffer (GstEncodeBufferPool *
    pool);
struct v4l2_buffer
    *gst_encode_buffer_pool_get_non_queued_buffer (GstEncodeBufferPool * pool,
    gboolean priority);

#endif /* __GST_STBUFFERPOOL_H__ */
