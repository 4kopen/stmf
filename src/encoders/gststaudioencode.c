/* Gstreamer ST Audio Encoder
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:element-staudioencode
 *
 * FIXME:Describe staudioencode here.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <gst/gst.h>

#include "gststaudioencode.h"
#include "gststbufferpool.h"
#include <sys/mman.h>
#include "v4l2_utils.h"

#if 0
/* A useful debugging helper */
#ifndef ioctl
#define ioctl(fd,ioctlno,args...) \
  ({int ioctl_ret; do { \
    GST_LOG_OBJECT (audioencoder, "Calling ioctl "#ioctlno); \
    ioctl_ret = ioctl(fd,ioctlno,##args); \
    GST_LOG_OBJECT (audioencoder, "ioctl "#ioctlno" return: %d", ioctl_ret); \
  } while(0); \
  ioctl_ret;})
#endif
#endif

#define DEFAULT_NUMBER_OF_INPUT_BUFFER   2
#define DEFAULT_NUMBER_OF_OUTPUT_BUFFER  2

#define DEFAULT_CHANNEL_COUNT               8

#define DEFAULT_CHANNEL_0                   STM_V4L2_AUDIO_CHAN_L
#define DEFAULT_CHANNEL_1                   STM_V4L2_AUDIO_CHAN_R
#define DEFAULT_CHANNEL_2                   STM_V4L2_AUDIO_CHAN_STUFFING
#define DEFAULT_CHANNEL_3                   STM_V4L2_AUDIO_CHAN_STUFFING
#define DEFAULT_CHANNEL_4                   STM_V4L2_AUDIO_CHAN_STUFFING
#define DEFAULT_CHANNEL_5                   STM_V4L2_AUDIO_CHAN_STUFFING
#define DEFAULT_CHANNEL_6                   STM_V4L2_AUDIO_CHAN_STUFFING
#define DEFAULT_CHANNEL_7                   STM_V4L2_AUDIO_CHAN_STUFFING

#define DEFAULT_SAMPLE_RATE                 48000
#define DEFAULT_SAMPLE_RATE_INDEX           12
#define MIN_VBR_QUALITY_FACTOR              0
#define MAX_VBR_QUALITY_FACTOR              100
#define DEFAULT_VBR_QUALITY_FACTOR          100

#define MIN_BITRATE_CAP                     0
#define MAX_BITRATE_CAP                     320000
#define DEFAULT_BITRATE_CAP                 0

#define MIN_BITRATE                         16000
#define MAX_BITRATE                         320000
#define DEFAULT_BITRATE                     192000

#define MIN_SERIAL_CONTROL                  V4L2_MPEG_AUDIO_NO_COPYRIGHT
#define MAX_SERIAL_CONTROL                  V4L2_MPEG_AUDIO_NO_FUTHER_COPY_AUTHORISED
#define DEFAULT_SERIAL_CONTROL              V4L2_MPEG_AUDIO_NO_COPYRIGHT

#define DEFAULT_SAMPLE_FORMAT               V4L2_MPEG_AUDIO_STM_PCM_FMT_32_S32LE
#define DEFAULT_EMPHASIS                    V4L2_MPEG_AUDIO_NO_EMPHASIS

#define MIN_PROG_LEVEL                      -3100
#define MAX_PROG_LEVEL                      0
#define DEFAULT_PROG_LEVEL                  0

#define DEFAULT_ENCODE_BR_MODE              GST_STAUDIOENCODE_BR_MODE_VBR

#define GST_TYPE_AUDIOENCODE_BR_MODE (gst_staudioencode_br_mode_get_type ())

GST_DEBUG_CATEGORY_STATIC (gst_staudioencode_debug);
#define GST_CAT_DEFAULT gst_staudioencode_debug

static GType
gst_staudioencode_br_mode_get_type (void)
{
  static GType staudioencode_br_mode_type = 0;
  static GEnumValue br_mode_types[] = {
    {GST_STAUDIOENCODE_BR_MODE_VBR, "VBR", "vbr"},
    {GST_STAUDIOENCODE_BR_MODE_CBR, "CBR", "cbr"},
    {0, NULL, NULL},
  };

  if (!staudioencode_br_mode_type) {
    staudioencode_br_mode_type =
        g_enum_register_static ("GstStAudioEncodeBrMode", br_mode_types);
  }

  return staudioencode_br_mode_type;
}

enum
{
  PROP_0,
  PROP_ENCODE_BR_MODE,
  PROP_VBR_QUALITY_FACTOR,
  PROP_BITRATE_CAP,
  PROP_BITRATE,
  PROP_SERIAL_CONTROL,
  PROP_PROGRAM_LEVEL,
  PROP_LAST
};

typedef enum
{
  INPUT_PAD,
  OUTPUT_PAD
} encoder_pad;

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-raw, "
        "depth = (int) 32, "
        "width = (int) 32, "
        "channels = (int) 8, "
        "endianness = (int) LITTLE_ENDIAN, "
        "signed = (boolean) true, " "rate = (int) [ 1, MAX ]; ")
    );

static void gst_staudioencode_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_staudioencode_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static gboolean gst_staudioencode_set_caps (Gststaudioencode * audioencoder,
    GstCaps * caps);
static GstBufferPool *gst_staudioencode_poolalloc (GstPad * pad,
    const GstStructure * structure);
static GstFlowReturn gst_staudioencode_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buf);

static gboolean gst_staudioencode_setup_and_allocate (Gststaudioencode *
    audioencoder, encoder_pad pad);
static gboolean gst_staudioencode_controls_init (Gststaudioencode *
    audioencoder);
static void gst_staudioencode_encoder_read (GstPad * pad);
static void gst_staudioencode_reset (Gststencode * encoder, gboolean full);
static gboolean gst_staudioencode_start_pad_task (Gststencode * encoder);
static gboolean gst_staudioencode_set_output_format (Gststaudioencode *
    audioencoder);
static gboolean gst_staudioencode_sink_pad_event (GstPad * pad,
    GstObject * parent, GstEvent * event);
static gboolean gst_staudioencode_sink_query (GstPad * pad, GstObject * parent,
    GstQuery * query);

#define gst_staudioencode_parent_class parent_class
G_DEFINE_TYPE (Gststaudioencode, gst_staudioencode, GST_TYPE_STENCODE);

/* GObject vmethod implementations */
/* initialize the staudioencode's class */
static void
gst_staudioencode_class_init (GststaudioencodeClass * klass)
{
  GststencodeClass *encode_class;
  GststaudioencodeClass *audioencode_class;
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GObjectClass *gobject_class = (GObjectClass *) klass;

  GST_DEBUG_CATEGORY_INIT (gst_staudioencode_debug, "staudioencode", 0,
      "ST audio encoder");

  audioencode_class = (GststaudioencodeClass *) klass;
  encode_class = (GststencodeClass *) klass;

  gobject_class->set_property = gst_staudioencode_set_property;
  gobject_class->get_property = gst_staudioencode_get_property;
  audioencode_class->init_controls = NULL;

  g_object_class_install_property (gobject_class, PROP_ENCODE_BR_MODE,
      g_param_spec_enum ("br-mode", "br-mode",
          "Encode Bitrate mode", GST_TYPE_AUDIOENCODE_BR_MODE,
          DEFAULT_ENCODE_BR_MODE, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_VBR_QUALITY_FACTOR,
      g_param_spec_uint ("vbr-factor", "vbr quality factor for encoder",
          "vbr quality factor for encoder", MIN_VBR_QUALITY_FACTOR,
          MAX_VBR_QUALITY_FACTOR, DEFAULT_VBR_QUALITY_FACTOR,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  g_object_class_install_property (gobject_class, PROP_BITRATE_CAP,
      g_param_spec_uint ("br-cap", "bitrate cap for encoder",
          "bitrate cap for encoder", MIN_BITRATE_CAP, MAX_BITRATE_CAP,
          DEFAULT_BITRATE_CAP,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  g_object_class_install_property (gobject_class, PROP_BITRATE,
      g_param_spec_uint ("bitrate", "bitrate for encoder",
          "bitrate for encoder", MIN_BITRATE, MAX_BITRATE, DEFAULT_BITRATE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  g_object_class_install_property (gobject_class, PROP_SERIAL_CONTROL,
      g_param_spec_uint ("serial-control", "serial control for encoder",
          "serial control for encoder", MIN_SERIAL_CONTROL, MAX_SERIAL_CONTROL,
          DEFAULT_SERIAL_CONTROL,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  g_object_class_install_property (gobject_class, PROP_PROGRAM_LEVEL,
      g_param_spec_int ("prog-level", "program level for encoder",
          "program level for encoder", MIN_PROG_LEVEL, MAX_PROG_LEVEL,
          DEFAULT_PROG_LEVEL,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_factory));

  encode_class->start_task = gst_staudioencode_start_pad_task;
  encode_class->reset = gst_staudioencode_reset;
}

static gboolean
gst_staudioencode_start_pad_task (Gststencode * encoder)
{
  return gst_stencode_start_pad_task (encoder,
      (GstTaskFunction) gst_staudioencode_encoder_read, "staudioencode_read");
}

static void
gst_staudioencode_reset (Gststencode * encoder, gboolean full)
{
  Gststaudioencode *audioencoder = GST_STAUDIOENCODE (encoder);

  if (full) {
    audioencoder->input_info.rate = DEFAULT_SAMPLE_RATE;
    audioencoder->input_info.channels = DEFAULT_CHANNEL_COUNT;
    audioencoder->output_info.rate = DEFAULT_SAMPLE_RATE;
    audioencoder->output_info.channels = 2;

    memset (&audioencoder->src_metadata, 0,
        sizeof (struct v4l2_audenc_src_metadata));
    audioencoder->src_metadata.sample_rate = DEFAULT_SAMPLE_RATE;
    audioencoder->src_metadata.sample_format = DEFAULT_SAMPLE_FORMAT;
    audioencoder->src_metadata.program_level = DEFAULT_PROG_LEVEL;
    audioencoder->src_metadata.emphasis = DEFAULT_EMPHASIS;
    audioencoder->src_metadata.channel_count = DEFAULT_CHANNEL_COUNT;
    audioencoder->src_metadata.channel[0] = DEFAULT_CHANNEL_0;
    audioencoder->src_metadata.channel[1] = DEFAULT_CHANNEL_1;
    audioencoder->src_metadata.channel[2] = DEFAULT_CHANNEL_2;
    audioencoder->src_metadata.channel[3] = DEFAULT_CHANNEL_3;
    audioencoder->src_metadata.channel[4] = DEFAULT_CHANNEL_4;
    audioencoder->src_metadata.channel[5] = DEFAULT_CHANNEL_5;
    audioencoder->src_metadata.channel[6] = DEFAULT_CHANNEL_6;
    audioencoder->src_metadata.channel[7] = DEFAULT_CHANNEL_7;
  }

  gst_stencode_reset (encoder, full);
}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
static void
gst_staudioencode_init (Gststaudioencode * audioencoder)
{
  Gststencode *encoder = GST_STENCODE (audioencoder);

  GST_LOG_OBJECT (audioencoder, "%s:%d\n", __FUNCTION__, __LINE__);

  encoder->desired_num_input_buffers = DEFAULT_NUMBER_OF_INPUT_BUFFER;
  encoder->desired_num_output_buffers = DEFAULT_NUMBER_OF_OUTPUT_BUFFER;
  encoder->input_reserved = (guint) & audioencoder->src_metadata;
  encoder->output_reserved = (guint) & audioencoder->dst_metadata;

  audioencoder->encode_br_mode = V4L2_MPEG_VIDEO_BITRATE_MODE_VBR;
  audioencoder->vbr_quality_factor = DEFAULT_VBR_QUALITY_FACTOR;
  audioencoder->br_cap = DEFAULT_BITRATE_CAP;
  audioencoder->bit_rate = DEFAULT_BITRATE;
  audioencoder->serial_control = DEFAULT_SERIAL_CONTROL;
  audioencoder->program_level = DEFAULT_PROG_LEVEL;

  encoder->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");

  gst_pad_set_chain_function (encoder->sinkpad,
      GST_DEBUG_FUNCPTR (gst_staudioencode_chain));
  gst_pad_set_event_function (encoder->sinkpad,
      GST_DEBUG_FUNCPTR (gst_staudioencode_sink_pad_event));
  gst_pad_set_query_function (encoder->sinkpad,
      GST_DEBUG_FUNCPTR (gst_staudioencode_sink_query));

  gst_element_add_pad (GST_ELEMENT (audioencoder), encoder->sinkpad);

  gst_audio_info_init (&audioencoder->input_info);
  gst_audio_info_init (&audioencoder->output_info);
  gst_staudioencode_reset (encoder, TRUE);
  audioencoder->pool = NULL;
}

static void
gst_staudioencode_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  Gststaudioencode *audioencoder = GST_STAUDIOENCODE (object);
  GstStAudioEncodeBrMode br_mode;

  switch (prop_id) {
    case PROP_ENCODE_BR_MODE:
      br_mode = g_value_get_enum (value);
      if (br_mode == GST_STAUDIOENCODE_BR_MODE_CBR) {
        audioencoder->encode_br_mode = V4L2_MPEG_VIDEO_BITRATE_MODE_CBR;
        break;
      } else if (br_mode == GST_STAUDIOENCODE_BR_MODE_VBR) {
        audioencoder->encode_br_mode = V4L2_MPEG_VIDEO_BITRATE_MODE_VBR;
        break;
      } else {
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
      }
      break;
    case PROP_VBR_QUALITY_FACTOR:
      audioencoder->vbr_quality_factor = g_value_get_uint (value);
      break;
    case PROP_BITRATE_CAP:
      audioencoder->br_cap = g_value_get_uint (value);
      break;
    case PROP_BITRATE:
      audioencoder->bit_rate = g_value_get_uint (value);
      break;
    case PROP_SERIAL_CONTROL:
      audioencoder->serial_control = g_value_get_uint (value);
      break;
    case PROP_PROGRAM_LEVEL:
      audioencoder->program_level = g_value_get_int (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_staudioencode_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  Gststaudioencode *audioencoder = GST_STAUDIOENCODE (object);

  switch (prop_id) {
    case PROP_ENCODE_BR_MODE:
      if (audioencoder->encode_br_mode != V4L2_MPEG_AUDIO_BITRATE_MODE_VBR &&
          audioencoder->encode_br_mode != V4L2_MPEG_AUDIO_BITRATE_MODE_CBR) {
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
      }
      g_value_set_enum (value,
          audioencoder->encode_br_mode ==
          V4L2_MPEG_VIDEO_BITRATE_MODE_VBR ? GST_STAUDIOENCODE_BR_MODE_VBR :
          GST_STAUDIOENCODE_BR_MODE_CBR);
      break;
    case PROP_VBR_QUALITY_FACTOR:
      g_value_set_uint (value, audioencoder->vbr_quality_factor);
      break;
    case PROP_BITRATE_CAP:
      g_value_set_uint (value, audioencoder->br_cap);
      break;
    case PROP_BITRATE:
      g_value_set_uint (value, audioencoder->bit_rate);
      break;
    case PROP_SERIAL_CONTROL:
      g_value_set_uint (value, audioencoder->serial_control);
      break;
    case PROP_PROGRAM_LEVEL:
      g_value_set_int (value, audioencoder->program_level);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

/* GstElement vmethod implementations */
/* this function handles the link with other elements */
static gboolean
gst_staudioencode_set_caps (Gststaudioencode * audioencoder, GstCaps * caps)
{
  Gststencode *encoder;
  const GstStructure *structure;
  const gchar *mimetype;

  encoder = GST_STENCODE (audioencoder);
  structure = gst_caps_get_structure (caps, 0);
  mimetype = gst_structure_get_name (structure);

  GST_DEBUG_OBJECT (audioencoder, "%s %d - received %s as MIME type",
      __FUNCTION__, __LINE__, mimetype);

  if (!gst_caps_is_fixed (caps)) {
    GST_WARNING_OBJECT (audioencoder, "caps not fixed!");
    goto refuse_caps;
  }

  if (!gst_audio_info_from_caps (&audioencoder->input_info, caps)) {
    GST_WARNING_OBJECT (audioencoder, "no info from caps!");
    goto refuse_caps;
  }

  GST_DEBUG_OBJECT (audioencoder, "Input: rate=%d, channels=%d",
      audioencoder->input_info.rate, audioencoder->input_info.channels);

  if (!gst_staudioencode_set_output_format (audioencoder)) {
    GST_ERROR_OBJECT (audioencoder, "cannot set output format");
    return FALSE;
  }

  GST_DEBUG_OBJECT (audioencoder, "Output: rate=%d, channels=%d",
      audioencoder->output_info.rate, audioencoder->output_info.channels);

  audioencoder->src_metadata.sample_rate = audioencoder->input_info.rate;
  audioencoder->src_metadata.channel_count = audioencoder->input_info.channels;

  g_mutex_lock (&encoder->pool_init_mutex);
  if (!encoder->input_buf_allocated) {
    if (!gst_staudioencode_setup_and_allocate (audioencoder, INPUT_PAD)) {
      GST_ERROR_OBJECT (audioencoder,
          "cannot setup encoder output and allocate output buffers");
      g_mutex_unlock (&encoder->pool_init_mutex);
      return FALSE;
    }
  }

  if (!gst_staudioencode_setup_and_allocate (audioencoder, OUTPUT_PAD)) {
    GST_ERROR_OBJECT (audioencoder,
        "cannot setup encoder output and allocate output buffers");
    g_mutex_unlock (&encoder->pool_init_mutex);
    return FALSE;
  }

  /* Sets all the v4l controls */
  if (gst_staudioencode_controls_init (audioencoder) == FALSE) {
    GST_ERROR_OBJECT (audioencoder, "cannot init v4l controls");
    g_mutex_unlock (&encoder->pool_init_mutex);
    return FALSE;
  }
  g_mutex_unlock (&encoder->pool_init_mutex);

  return TRUE;

  /* ERRORS */
refuse_caps:
  {
    GST_WARNING_OBJECT (audioencoder, "rejected caps %" GST_PTR_FORMAT, caps);
    return FALSE;
  }
}

static gboolean
gst_staudioencode_sink_pad_event (GstPad * pad, GstObject * parent,
    GstEvent * event)
{
  Gststaudioencode *audioencoder = (Gststaudioencode *) parent;
  gboolean ret = TRUE;
  GstCaps *caps;

  GST_DEBUG_OBJECT (audioencoder, "%s - (%s)", __FUNCTION__,
      gst_event_type_get_name (GST_EVENT_TYPE (event)));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CAPS:
      gst_event_parse_caps (event, &caps);
      if ((audioencoder->prev_caps != NULL)
          && (gst_caps_is_equal ((const GstCaps *) caps,
                  (const GstCaps *) audioencoder->prev_caps))) {
        GST_DEBUG_OBJECT (audioencoder, "caps are already set");
        return TRUE;
      }
      ret = gst_staudioencode_set_caps (audioencoder, caps);
      if (ret) {
        if (audioencoder->prev_caps) {
          gst_caps_unref (audioencoder->prev_caps);
        }
        audioencoder->prev_caps = gst_caps_ref (caps);
      } else {
        GST_ERROR_OBJECT (audioencoder, "failed to set caps!");
      }
      gst_event_unref (event);
      return ret;
      break;

    default:
      break;
  }

  return gst_stencode_sink_event (pad, event);
}

/*
 function: gst_staudioencode_poolalloc
 input params:
           pad - sinkpad
           structure - structure from query/caps
 description : this function will allocate pool of buffers
 return : pool address or NULL
 */
static GstBufferPool *
gst_staudioencode_poolalloc (GstPad * pad, const GstStructure * structure)
{
  Gststaudioencode *audioencoder = NULL;
  Gststencode *encoder = NULL;


  audioencoder = GST_STAUDIOENCODE (GST_OBJECT_PARENT (pad));
  encoder = GST_STENCODE (GST_OBJECT_PARENT (pad));

  g_mutex_lock (&encoder->pool_init_mutex);
  if (!encoder->input_buf_allocated) {
    if (!gst_staudioencode_setup_and_allocate (audioencoder, INPUT_PAD)) {
      GST_ERROR_OBJECT (audioencoder, "Could not allocate v4l buffers, stopping"
          "read_task");
      gst_pad_stop_task (GST_STENCODE (audioencoder)->srcpad);
      g_mutex_unlock (&encoder->pool_init_mutex);
      return NULL;
    }
  }
  g_mutex_unlock (&encoder->pool_init_mutex);

  return GST_BUFFER_POOL (encoder->input_pool);
}

static gboolean
gst_staudioencode_sink_query (GstPad * pad, GstObject * parent,
    GstQuery * query)
{
  gboolean res = FALSE;
  const GstStructure *structure;
  Gststencode *encoder = NULL;
  GstBufferPool *pool = NULL;

  encoder = GST_STENCODE (GST_OBJECT_PARENT (pad));

  GST_LOG_OBJECT (pad, "%s query", GST_QUERY_TYPE_NAME (query));

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_ALLOCATION:
      structure = gst_query_get_structure (query);
      pool = gst_staudioencode_poolalloc (pad, structure);
      if (pool != NULL) {
        gst_query_add_allocation_pool (query, pool, DUMMY_POOL_SIZE,
            encoder->desired_num_input_buffers,
            encoder->desired_num_input_buffers);
        gst_object_unref (pool);
        res = TRUE;
      }
      break;
    default:
      res = gst_pad_query_default (pad, parent, query);
      break;
  }
  return res;
}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_staudioencode_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
  Gststaudioencode *audioencoder;
  Gststencode *encoder;
  gboolean ret;

  audioencoder = GST_STAUDIOENCODE (GST_OBJECT_PARENT (pad));
  encoder = GST_STENCODE (GST_OBJECT_PARENT (pad));
  GST_LOG_OBJECT (audioencoder, "chain: %" GST_PTR_FORMAT, buf);

  ret = gst_stencode_prechain (pad, buf);
  if (ret != GST_FLOW_OK) {
    gst_buffer_unref (buf);
    return ret;
  }

  /* Clip the buffer */
  buf =
      gst_audio_buffer_clip (buf, &encoder->segment,
      audioencoder->input_info.rate, audioencoder->input_info.bpf);

  if (G_UNLIKELY (!buf)) {
    GST_DEBUG_OBJECT (buf, "no data after clipping to segment");

    return GST_FLOW_OK;
  }

  return gst_stencode_postchain (encoder, buf);
}

/* TO BE CALLED WITH V4LBUFFS LOCK TAKEN */
static gboolean
gst_staudioencode_setup_and_allocate (Gststaudioencode * audioencoder,
    encoder_pad pad)
{
  Gststencode *encoder = GST_STENCODE (audioencoder);
  struct v4l2_format fmt;
  struct v4l2_audenc_format *audenc_fmt;

  GST_LOG_OBJECT (audioencoder, "%s:%d", __FUNCTION__, __LINE__);

  if (pad == INPUT_PAD) {
    struct v4l2_audioout audioout;

    /* Configure encoder input */
    memset (&audioout, 0, sizeof (struct v4l2_audioout));
    audioout.index = encoder->dev_id;
    if (ioctl (encoder->v4l2_fd_input, VIDIOC_S_AUDOUT, &audioout) < 0) {
      GST_ERROR_OBJECT (audioencoder, "cannot set input %d %d - %s",
          encoder->v4l2_fd_input, encoder->dev_id, strerror (errno));
      return FALSE;
    }
    /* Set input format */
    memset (&fmt, 0, sizeof (struct v4l2_format));
    fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    audenc_fmt = (struct v4l2_audenc_format *) fmt.fmt.raw_data;
    audenc_fmt->codec = V4L2_MPEG_AUDIO_STM_ENCODING_PCM;

    if (ioctl (encoder->v4l2_fd_input, VIDIOC_S_FMT, &fmt) < 0) {
      GST_ERROR_OBJECT (audioencoder, "cannot set requested input format - %s",
          strerror (errno));
      return FALSE;
    }
    /* Buffers allocation */
    if (!gst_stencode_allocate_input_buffers (encoder))
      return FALSE;

    encoder->input_buf_allocated = TRUE;
  } else {
    struct v4l2_audio audio;

    /* Configure encoder output */
    memset (&audio, 0, sizeof (struct v4l2_audio));
    audio.index = encoder->dev_id;
    if (ioctl (encoder->v4l2_fd_output, VIDIOC_S_AUDIO, &audio) < 0) {
      GST_ERROR_OBJECT (audioencoder, "cannot set output %d - %s",
          encoder->dev_id, strerror (errno));
      return FALSE;
    }

    memset (&fmt, 0, sizeof (struct v4l2_format));
    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    audenc_fmt = (struct v4l2_audenc_format *) fmt.fmt.raw_data;
    audenc_fmt->codec = audioencoder->codec_type;
    if (ioctl (encoder->v4l2_fd_output, VIDIOC_S_FMT, &fmt) < 0) {
      GST_ERROR_OBJECT (audioencoder, "cannot set requested output format - %s",
          strerror (errno));
      return FALSE;
    }

    /* Buffers allocation */
    if (!gst_stencode_allocate_output_buffers (encoder))
      return FALSE;

  }

  return TRUE;
}

static gboolean
gst_staudioencode_controls_init (Gststaudioencode * audioencoder)
{
  GststaudioencodeClass *klass = GST_STAUDIOENCODE_GET_CLASS (audioencoder);
  Gststencode *encoder = GST_STENCODE (audioencoder);

  GST_LOG_OBJECT (audioencoder, "%s:%d\n", __FUNCTION__, __LINE__);

  if (klass->init_controls) {
    if (!(*klass->init_controls) (audioencoder, encoder->v4l2_fd_output))
      return FALSE;
  }

  return TRUE;
}

static void
gst_staudioencode_encoder_read (GstPad * pad)
{
  Gststaudioencode *audioencoder = NULL;
  Gststencode *encoder = NULL;
  struct v4l2_buffer v4l_output_buf, *v4l2_buf;
  GstBuffer *buf;
  gboolean discont = FALSE;
  GstClockTimeDiff threshold;

  GST_LOG_OBJECT (audioencoder, "%s:%d", __FUNCTION__, __LINE__);
  audioencoder = GST_STAUDIOENCODE (GST_OBJECT_PARENT (pad));
  encoder = GST_STENCODE (GST_OBJECT_PARENT (pad));

  if (encoder->flushing) {
    goto ioctl_error;
  }

  if (encoder->encoded_frame_count == 0) {
    /* Queue the buffer if it is not in use */
    while ((v4l2_buf =
            gst_encode_buffer_pool_get_raw_buffer (encoder->output_pool,
                TRUE))) {
      memset (&v4l_output_buf, 0, sizeof (struct v4l2_buffer));
      v4l_output_buf = *v4l2_buf;
      v4l_output_buf.field = 0;
      v4l_output_buf.reserved = encoder->output_reserved;

      /* Queue output buffer */
      if (ioctl (encoder->v4l2_fd_output, VIDIOC_QBUF, &v4l_output_buf) < 0) {
        GST_ERROR_OBJECT (audioencoder,
            "Err = %d (%s): Can not Queue output buffer for frame encode #%lld",
            errno, strerror (errno), encoder->encoded_frame_count + 1);
        goto ioctl_error;
      }
    }

    /* Have to streamon after the first QBUF */
    if (!encoder->streaming_capture) {
      g_mutex_lock (&encoder->stream_lock);
      /* Start encoder output */
      if (!gst_stencode_set_streaming (encoder, encoder->v4l2_fd_output,
              V4L2_BUF_TYPE_VIDEO_CAPTURE, TRUE)) {
        g_cond_signal (&encoder->stream_cond);
        g_mutex_unlock (&encoder->stream_lock);
        goto ioctl_error;
      }
      g_cond_signal (&encoder->stream_cond);
      g_mutex_unlock (&encoder->stream_lock);
    }

  } else if (encoder->encoded_frame_count >= encoder->output_pool->num_buffers) {
    v4l2_buf =
        gst_encode_buffer_pool_get_raw_buffer (encoder->output_pool, TRUE);
    if (!v4l2_buf) {
      /* CPU load is too high without this */
      /* Pretend EAGAIN, so we'll get paused a bit */
      errno = EAGAIN;
      goto ioctl_error;
    }
    memset (&v4l_output_buf, 0, sizeof (struct v4l2_buffer));
    v4l_output_buf = *v4l2_buf;
    v4l_output_buf.field = 0;
    v4l_output_buf.reserved = encoder->output_reserved;

    if (ioctl (encoder->v4l2_fd_output, VIDIOC_QBUF, &v4l_output_buf) < 0) {
      GST_ERROR_OBJECT (audioencoder,
          "VIDIOC_QBUF failed with - %s", strerror (errno));
      goto ioctl_error;
    }
  }

  /* DeQueue Ouput buffer */
  if (ioctl (encoder->v4l2_fd_output, VIDIOC_DQBUF, &v4l_output_buf) < 0) {
    GST_ERROR_OBJECT (audioencoder,
        "Err = %d (%s): Can not DeQueue output buffer for frame encode #%lld",
        errno, strerror (errno), encoder->encoded_frame_count + 1);
    goto ioctl_error;
  }
  gst_encode_buffer_pool_mark_dequeue_buffer_by_index (encoder->output_pool,
      v4l_output_buf.index);

  if (v4l_output_buf.bytesused == 0) {
    GST_DEBUG_OBJECT (audioencoder, "Received an EOS");
    /* Received an EOS, we shouldn't try anymore to read data otherwise we will get blocked */
    g_mutex_lock (&encoder->eos_lock);
    gst_pad_pause_task (GST_STENCODE (audioencoder)->srcpad);
    if (encoder->eos_received) {
      g_mutex_unlock (&encoder->eos_lock);
      /* Do not reset the eos_received flag here, this will be done in PAUSED_TO_READY state change */
      gst_pad_push_event (GST_STENCODE (audioencoder)->srcpad,
          gst_event_new_eos ());
    } else {
      g_cond_signal (&encoder->eos_cond);
      g_mutex_unlock (&encoder->eos_lock);
    }
    goto ioctl_error;
  }

  encoder->encoded_frame_count++;

  /* Set the Discontinuity */
  if (v4l_output_buf.flags & V4L2_BUF_FLAG_STM_ENCODE_TIME_DISCONTINUITY) {
    discont = TRUE;
    GST_DEBUG_OBJECT (audioencoder, "Discontinuity at frame %lld",
        encoder->encoded_frame_count);
  }

  GST_LOG_OBJECT (audioencoder, "Frame successfully encoded with size %d",
      v4l_output_buf.bytesused);

  if (encoder->do_initial_frame_hack) {
    if (encoder->initial_frame == TRUE) {
      if (v4l_output_buf.timestamp.tv_sec > GST_FIRST_BUFFER_ENCODE_VAL) {
        /* Drop frames with high timestamp */
        gst_encode_buffer_pool_free_raw_buffer (encoder->output_pool,
            &v4l_output_buf);
        return;
      } else {
        encoder->initial_frame = FALSE;
      }
    }
  }

  buf = gst_encode_buffer_pool_export (encoder->output_pool, &v4l_output_buf);
  if (buf) {
    if (discont) {
      GST_BUFFER_FLAG_SET (buf, GST_BUFFER_FLAG_DISCONT);
    }

    /* To be removed ASAP */
    /* BAD Workaround for first frame with timestamp 0 */
    if (encoder->do_first_frame_hack) {
      if (encoder->first_frame == TRUE) {
        GST_BUFFER_TIMESTAMP (buf) = 0;
        encoder->first_frame = FALSE;
      }
    }

    threshold =
        GST_FRAMES_TO_CLOCK_TIME (1, audioencoder->output_info.rate) / 10;
    if (gst_stencode_push_buffer (encoder, buf, threshold) == GST_FLOW_ERROR) {
      GST_ERROR_OBJECT (audioencoder, "FLOW_ERROR, pausing the read task");
      /* We pause the task, so we will not be able to receive any EOS frame
         that the PAUSED->READY code might be waiting. So we signal the eos
         condition variable now, to unblock it, since we're done (till we
         are restarted, if we end up being). */
      g_mutex_lock (&encoder->eos_lock);
      gst_pad_pause_task (encoder->srcpad);
      g_cond_signal (&encoder->eos_cond);
      g_mutex_unlock (&encoder->eos_lock);
    }
  } else {
    GST_ERROR_OBJECT (audioencoder, "Could not allocate buffer of %d bytes",
        v4l_output_buf.bytesused);
  }

  return;

ioctl_error:
  if (errno == EAGAIN) {
    usleep (1000);
  } else {
    g_mutex_lock (&encoder->eos_lock);
    gst_pad_pause_task (encoder->srcpad);
    g_cond_signal (&encoder->eos_cond);
    g_mutex_unlock (&encoder->eos_lock);
  }
}

static gboolean
gst_staudioencode_set_output_format (Gststaudioencode * audioencoder)
{
  Gststencode *encoder;
  GstStructure *srcstructure = NULL;
  GstCaps *allowed_caps = NULL;
  GstCaps *srccaps = NULL;
  const char *media_type;

  encoder = GST_STENCODE (audioencoder);

  srccaps =
      gst_pad_template_get_caps (gst_pad_get_pad_template (encoder->srcpad));
  media_type = gst_structure_get_name (gst_caps_get_structure (srccaps, 0));

  if (gst_pad_get_current_caps (encoder->srcpad) == NULL) {
    allowed_caps = gst_pad_get_allowed_caps (encoder->srcpad);
    if (!allowed_caps)
      return TRUE;
    if (gst_caps_is_empty (allowed_caps)) {
      gst_caps_unref (allowed_caps);
      return TRUE;
    }

    srccaps = gst_caps_copy_nth (allowed_caps, 0);
    gst_caps_unref (allowed_caps);

    GST_DEBUG_OBJECT (audioencoder, "source caps are %" GST_PTR_FORMAT,
        srccaps);

    srcstructure = gst_caps_get_structure (srccaps, 0);
    if (!strcmp (gst_structure_get_name (srcstructure), media_type)) {
      if (!audioencoder->fixed_rate) {
        if (!gst_structure_get_int (srcstructure, "rate",
                &audioencoder->output_info.rate)) {
          audioencoder->output_info.rate = audioencoder->input_info.rate;
        }
      }

      if (!gst_structure_get_int (srcstructure, "channels",
              &audioencoder->output_info.channels)) {
        audioencoder->output_info.channels = 2;
      }
    } else {
      GST_ERROR_OBJECT (audioencoder,
          "Unexpected media type: expected %s, found %s",
          media_type, gst_structure_get_name (srcstructure));
      gst_caps_unref (srccaps);
      return FALSE;
    }
    GST_DEBUG_OBJECT (audioencoder, "Output: rate=%d, channels=%d",
        audioencoder->output_info.rate, audioencoder->output_info.channels);

    gst_structure_set (srcstructure, "rate", G_TYPE_INT,
        audioencoder->output_info.rate, "channels", G_TYPE_INT,
        audioencoder->output_info.channels, NULL);

    /* Try and set caps on the source with what we negotiated */
    if (!gst_pad_set_caps (encoder->srcpad, srccaps)) {
      GST_ERROR_OBJECT (audioencoder,
          "Failed to set source caps: %" GST_PTR_FORMAT, srccaps);
      gst_caps_unref (srccaps);
      return FALSE;
    }

    gst_caps_unref (srccaps);
  }

  return TRUE;
}
