/* Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include "gststencode.h"
#include "gststbufferpool.h"
#include "v4l2_utils.h"

#if 0
/* A useful debugging helper */
#ifndef ioctl
#define ioctl(fd,ioctlno,args...) \
  ({int ioctl_ret; do { \
    GST_DEBUG_OBJECT (encoder, "Calling ioctl "#ioctlno); \
    ioctl_ret = ioctl(fd,ioctlno,##args); \
    GST_DEBUG_OBJECT (encoder, "ioctl "#ioctlno" return: %d", ioctl_ret); \
  } while(0); \
  ioctl_ret;})
#endif
#endif

GST_DEBUG_CATEGORY_STATIC (gst_encode_debug);
#define GST_CAT_DEFAULT gst_encode_debug

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_DEV_ID,
  PROP_LAST
};

#define MIN_DEV_ID          0
#define MAX_DEV_ID          7
#define DEFAULT_DEV_ID      0

static void gst_stencode_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_stencode_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static GstStateChangeReturn gst_stencode_change_state (GstElement * element,
    GstStateChange transition);
static void gst_stencode_finalize (GObject * object);

#if 0
static void
_do_init (GType object_type)
{
  const GInterfaceInfo preset_interface_info = {
    NULL,                       /* interface_init */
    NULL,                       /* interface_finalize */
    NULL                        /* interface_data */
  };

  g_type_add_interface_static (object_type, GST_TYPE_PRESET,
      &preset_interface_info);
}
#endif
#define gst_stencode_parent_class parent_class
G_DEFINE_TYPE (Gststencode, gst_stencode, GST_TYPE_ELEMENT);

static QueuedEvent *
make_queued_event (GstEvent * event, GstClockTime ts)
{
  QueuedEvent *qe = g_slice_new (QueuedEvent);
  qe->ts = ts;
  qe->event = gst_event_ref (event);
  return qe;
}

static void
free_queued_event (QueuedEvent * qe)
{
  gst_event_unref (qe->event);
  g_slice_free (QueuedEvent, qe);
}

void
gst_stencode_queue_event (Gststencode * encoder, GstEvent * event,
    GstClockTime ts)
{
  g_mutex_lock (&encoder->event_lock);
  GST_DEBUG_OBJECT (encoder,
      "Queueing %s event, last buffer was %" GST_TIME_FORMAT,
      GST_EVENT_TYPE_NAME (event), GST_TIME_ARGS (ts));
  encoder->queued_events = g_list_append (encoder->queued_events,
      make_queued_event (event, ts));
  g_mutex_unlock (&encoder->event_lock);
}

void
gst_stencode_drop_queued_events (Gststencode * encoder)
{
  g_mutex_lock (&encoder->event_lock);
  g_list_free_full (encoder->queued_events, (GDestroyNotify) free_queued_event);
  encoder->queued_events = NULL;
  g_mutex_unlock (&encoder->event_lock);
}

/* Sends all queued events that were received before timestamp ts.
   Stops on first error, with the event that failed to send removed
   from the list. Not sure if it would be better to not remove it,
   and let the caller remove if needed. Returns FALSE if an error
   occured. */
gboolean
gst_stencode_send_queued_events_before (Gststencode * encoder, GstClockTime ts)
{
  GstPad *pad = encoder->srcpad;
  gboolean ret = TRUE;

  g_mutex_lock (&encoder->event_lock);

  while (encoder->queued_events) {
    QueuedEvent *ev = encoder->queued_events->data;
    if (ev->ts != GST_CLOCK_TIME_NONE && ev->ts >= ts)
      break;
    GST_DEBUG_OBJECT (encoder,
        "We have a %s event sent before %" GST_TIME_FORMAT ", sending",
        GST_EVENT_TYPE_NAME (ev->event), GST_TIME_ARGS (ts));
    ret = gst_pad_push_event (pad, gst_event_ref (ev->event));
    free_queued_event (encoder->queued_events->data);
    encoder->queued_events = g_list_delete_link (encoder->queued_events,
        encoder->queued_events);
    if (!ret)
      break;
  }

  g_mutex_unlock (&encoder->event_lock);

  return ret;
}

static void
gst_stencode_class_init (GststencodeClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *element_class;

  GST_DEBUG_CATEGORY_INIT (gst_encode_debug, "stencode",
      GST_DEBUG_FG_MAGENTA, "ST encoder");

  gobject_class = (GObjectClass *) klass;
  element_class = (GstElementClass *) klass;

  gobject_class->set_property = gst_stencode_set_property;
  gobject_class->get_property = gst_stencode_get_property;
  gobject_class->finalize = gst_stencode_finalize;

  element_class->change_state = gst_stencode_change_state;
  klass->reset = gst_stencode_reset;

  klass->deallocate = gst_stencode_deallocate;
  klass->term = gst_stencode_term;

  g_object_class_install_property (gobject_class, PROP_DEV_ID,
      g_param_spec_int ("dev-id", "device id for encoder",
          "device id for encoder", MIN_DEV_ID, MAX_DEV_ID, DEFAULT_DEV_ID,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));
}

static void
gst_stencode_init (Gststencode * encoder)
{
  encoder->v4l2_fd_input = -1;
  encoder->v4l2_fd_output = -1;
  encoder->input_buf_allocated = FALSE;
  encoder->input_reserved = 0;
  encoder->output_reserved = 0;
  encoder->dev_id = 0;
  encoder->queued_events = NULL;
  encoder->last_received_ts = GST_CLOCK_TIME_NONE;
  encoder->srcresult = GST_FLOW_OK;
  gst_segment_init (&encoder->segment, GST_FORMAT_TIME);
  encoder->timestamp = 0;
  encoder->raw_frame_count = 0;
  encoder->encoded_frame_count = 0;
  encoder->skipped_frame_count = 0;
  encoder->streaming_capture = FALSE;
  encoder->streaming_output = FALSE;

  encoder->eos_received = FALSE;

  /* To be removed ASAP */
  /* BAD Workaround for first frame with timestamp 0 */
  encoder->do_first_frame_hack = FALSE;
  encoder->do_initial_frame_hack = FALSE;

  /* Init mutex and cond */
  g_mutex_init (&encoder->stream_lock);
  g_mutex_init (&encoder->eos_lock);
  g_mutex_init (&encoder->pool_init_mutex);
  g_mutex_init (&encoder->event_lock);

  g_cond_init (&encoder->stream_cond);
  g_cond_init (&encoder->eos_cond);

}

gboolean
gst_stencode_deallocate (Gststencode * encoder)
{
  gboolean ret = TRUE;

  GST_LOG_OBJECT (encoder, "Terminating encoder");

  /* Stop streaming I/O */
  gst_stencode_set_streaming (encoder, encoder->v4l2_fd_output,
      V4L2_BUF_TYPE_VIDEO_CAPTURE, FALSE);
  gst_stencode_set_streaming (encoder, encoder->v4l2_fd_input,
      V4L2_BUF_TYPE_VIDEO_OUTPUT, FALSE);

  if (gst_pad_stop_task (encoder->srcpad) != TRUE) {
    ret = FALSE;
    GST_ERROR_OBJECT (encoder, "Can not stop the encoder read task");
  }

  g_list_free_full (encoder->queued_events, (GDestroyNotify) free_queued_event);
  encoder->queued_events = NULL;

  /* Deallocation */
  g_mutex_lock (&encoder->pool_init_mutex);
  if (!gst_stencode_free_output_buffers (encoder))
    ret = FALSE;
  if (!gst_stencode_free_input_buffers (encoder))
    ret = FALSE;
  g_mutex_unlock (&encoder->pool_init_mutex);

  return ret;
}

gboolean
gst_stencode_term (Gststencode * encoder)
{
  /*Close encoder devices */
  if (encoder->v4l2_fd_input != -1) {
    close (encoder->v4l2_fd_input);
    encoder->v4l2_fd_input = -1;
  }

  if (encoder->v4l2_fd_output != -1) {
    close (encoder->v4l2_fd_output);
    encoder->v4l2_fd_output = -1;
  }

  return TRUE;
}

void
gst_stencode_finalize (GObject * object)
{
  Gststencode *encoder = GST_STENCODE (object);

  /* clear mutex and cond */
  g_mutex_clear (&encoder->stream_lock);
  g_mutex_clear (&encoder->eos_lock);
  g_mutex_clear (&encoder->pool_init_mutex);
  g_mutex_clear (&encoder->event_lock);

  g_cond_clear (&encoder->stream_cond);
  g_cond_clear (&encoder->eos_cond);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_stencode_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  Gststencode *encoder = GST_STENCODE (object);

  switch (prop_id) {
    case PROP_DEV_ID:
      encoder->dev_id = g_value_get_int (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_stencode_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  Gststencode *encoder = GST_STENCODE (object);

  switch (prop_id) {
    case PROP_DEV_ID:
      g_value_set_int (value, encoder->dev_id);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

gboolean
gst_stencode_set_streaming (Gststencode * encoder,
    int fd, int buffer_type, gboolean on)
{
  struct v4l2_requestbuffers req;
  int ioctlno = on ? VIDIOC_STREAMON : VIDIOC_STREAMOFF;
  const char *command = on ? "start" : "stop";
  const char *type = fd == encoder->v4l2_fd_input ? "encode" : "output";

  memset (&req, 0, sizeof (struct v4l2_requestbuffers));
  req.type = buffer_type;
  GST_DEBUG_OBJECT (encoder, "%sing streaming on %s device", command, type);
  if ((ioctl) (fd, ioctlno, &req.type) < 0) {
    GST_ERROR_OBJECT (encoder,
        "Can not %s streaming on %s device - %s", command, type,
        strerror (errno));
    return FALSE;
  }
  if (buffer_type == V4L2_BUF_TYPE_VIDEO_CAPTURE) {
    encoder->streaming_capture = on;
  }
  if (buffer_type == V4L2_BUF_TYPE_VIDEO_OUTPUT) {
    encoder->streaming_output = on;
  }
  GST_INFO_OBJECT (encoder, "%s streaming success on %s device", command, type);
  return TRUE;
}

/* TO BE CALLED WITH POOL INIT LOCK TAKEN */
gboolean
gst_stencode_free_output_buffers (Gststencode * encoder)
{
  if (encoder->output_pool)
    gst_object_unref (GST_OBJECT (encoder->output_pool));
  encoder->output_pool = NULL;
  return TRUE;
}

/* TO BE CALLED WITH POOL INIT LOCK TAKEN */
gboolean
gst_stencode_free_input_buffers (Gststencode * encoder)
{
  if (encoder->input_pool)
    gst_object_unref (GST_OBJECT (encoder->input_pool));
  encoder->input_pool = NULL;
  encoder->input_buf_allocated = FALSE;
  return TRUE;
}

gboolean
gst_stencode_start_pad_task (Gststencode * encoder, GstTaskFunction f,
    const char *name)
{
  g_return_val_if_fail (encoder->srcpad != NULL, FALSE);

  if (!gst_pad_start_task (encoder->srcpad, f, encoder->srcpad, NULL)) {
    GST_WARNING_OBJECT (encoder->srcpad, "Failed to start pad task");
    return FALSE;
  }
  gst_object_set_name (GST_OBJECT (GST_PAD_TASK (encoder->srcpad)), name);
  return TRUE;
}

static GstStateChangeReturn
gst_stencode_change_state (GstElement * element, GstStateChange transition)
{
  Gststencode *encoder = (Gststencode *) element;
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
  GstTask *task;

  /* upwards transitions */
  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      GST_DEBUG_OBJECT (element, "GST_STATE_CHANGE_NULL_TO_READY");

      /* Open encoder devices */
      if (encoder->v4l2_fd_input < 0) {
        encoder->v4l2_fd_input =
            v4l2_open_by_name (V4L2_ENCODE_DRIVER_NAME, V4L2_ENCODE_CARD_NAME,
            O_RDWR);
        if (encoder->v4l2_fd_input < 0) {
          GST_ERROR_OBJECT (encoder, "Unable to open %s",
              V4L2_ENCODE_DRIVER_NAME);
          return GST_STATE_CHANGE_FAILURE;
        }
      }
      if (encoder->v4l2_fd_output < 0) {
        encoder->v4l2_fd_output =
            v4l2_open_by_name (V4L2_ENCODE_DRIVER_NAME, V4L2_ENCODE_CARD_NAME,
            O_RDWR);
        if (encoder->v4l2_fd_output < 0) {
          GST_ERROR_OBJECT (encoder, "Unable to open %s",
              V4L2_ENCODE_DRIVER_NAME);
          return GST_STATE_CHANGE_FAILURE;
        }
      }

      break;

    case GST_STATE_CHANGE_READY_TO_PAUSED:
      GST_DEBUG_OBJECT (element, "GST_STATE_CHANGE_READY_TO_PAUSED");
      gst_segment_init (&encoder->segment, GST_FORMAT_TIME);
      break;

    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      GST_DEBUG_OBJECT (element, "GST_STATE_CHANGE_PAUSED_TO_PLAYING");
      break;

    case GST_STATE_CHANGE_PAUSED_TO_READY:
      GST_DEBUG_OBJECT (element, "GST_STATE_CHANGE_PAUSED_TO_READY");
      /* Push a EOS to be sure it will unblock the read task */
      g_mutex_lock (&encoder->eos_lock);
      task = GST_PAD_TASK (encoder->srcpad);
      if (task && gst_task_get_state (task) == GST_TASK_STARTED) {
        if (encoder->eos_received == FALSE) {
          if (gst_stencode_send_eos (encoder)) {
            g_cond_wait (&encoder->eos_cond, &encoder->eos_lock);
          } else {
            gst_pad_stop_task (encoder->srcpad);
          }
        } else {
          encoder->eos_received = FALSE;
        }
      }
      g_mutex_unlock (&encoder->eos_lock);
      if (!GST_STENCODE_GET_CLASS (encoder)->deallocate (encoder))
        return GST_STATE_CHANGE_FAILURE;
      break;

    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
  if (ret == GST_STATE_CHANGE_FAILURE)
    return ret;

  /* downwards transitions */
  switch (transition) {

    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      GST_DEBUG_OBJECT (element, "GST_STATE_CHANGE_PLAYING_TO_PAUSED");
      break;

    case GST_STATE_CHANGE_READY_TO_NULL:
      GST_DEBUG_OBJECT (element, "GST_STATE_CHANGE_READY_TO_NULL");
      GST_DEBUG_OBJECT (encoder, "Frames encoded = %lld - skipped = %lld",
          encoder->encoded_frame_count, encoder->skipped_frame_count);
      /* Termination of encoder */
      if (!GST_STENCODE_GET_CLASS (encoder)->term (encoder))
        return GST_STATE_CHANGE_FAILURE;
      break;

    default:
      break;
  }

  return ret;
}

gboolean
gst_stencode_send_eos (Gststencode * encoder)
{
  struct v4l2_buffer *v4l_input_buf;
  GstEncodeBufferMeta *meta;

  if (encoder->input_pool) {
    /* Get a free Buffer */
    GstBuffer *buf =
        gst_encode_buffer_pool_get_buffer (encoder->input_pool, TRUE);
    if (buf == NULL) {
      GST_ERROR_OBJECT (encoder,
          "Err: Can not find any available buffer to push EOS event");
      return FALSE;
    }

    meta =
        (GstEncodeBufferMeta *) gst_buffer_get_meta (buf,
        GST_ENCODEBUFFER_META_API_TYPE);

    v4l_input_buf = &meta->bufferinfo->v4l2_buf;
    v4l_input_buf->bytesused = 0;
    v4l_input_buf->reserved = encoder->input_reserved;

    if (ioctl (encoder->v4l2_fd_input, VIDIOC_QBUF, v4l_input_buf) < 0) {
      GST_ERROR_OBJECT (encoder,
          "Err = %d (%s): Can not queue input buffer for EOS",
          errno, strerror (errno));
      return FALSE;
    }
    if (ioctl (encoder->v4l2_fd_input, VIDIOC_DQBUF, v4l_input_buf) < 0) {
      GST_ERROR_OBJECT (encoder,
          "Err = %d (%s): Can not dequeue input buffer for EOS",
          errno, strerror (errno));
      return FALSE;
    }
    /* isUsed being set to TRUE in gst_encode_buffer_pool_find_free_buffer() fn.,
     * so unreffing the buffer
     */
    gst_buffer_unref (buf);
    return TRUE;
  } else {
    return FALSE;
  }
}

void
gst_stencode_reset (Gststencode * encoder, gboolean full)
{
  if (encoder->encoded_frame_count > 0 || encoder->skipped_frame_count > 0) {
    GST_DEBUG_OBJECT (encoder, "Frames encoded = %lld - skipped = %lld",
        encoder->encoded_frame_count, encoder->skipped_frame_count);
  }

  gst_stencode_drop_queued_events (encoder);
  encoder->last_received_ts = GST_CLOCK_TIME_NONE;
  encoder->srcresult = GST_FLOW_OK;
  encoder->eos_received = FALSE;
  encoder->flushing = FALSE;

  encoder->raw_frame_count = 0;
  encoder->encoded_frame_count = 0;
  if (full) {
    encoder->first_frame = FALSE;
    encoder->initial_frame = FALSE;
  }

  gst_segment_init (&encoder->segment, GST_FORMAT_TIME);
}

gboolean
gst_stencode_encoder_write (Gststencode * encoder, GstBuffer * buf)
{
  struct v4l2_buffer *v4l_input_buf = NULL;
  GstBuffer *input_buf = 0;
  GstMapInfo info_read;
  GstMapInfo info_write;
  GstEncodeBufferMeta *meta;

  if (encoder->flushing) {
    GST_DEBUG_OBJECT (encoder, "Flushing, discarding buf and return");
    return FALSE;
  }

  meta = (GstEncodeBufferMeta *) gst_buffer_get_meta (buf,
      GST_ENCODEBUFFER_META_API_TYPE);

  GST_LOG_OBJECT (encoder, "Writing buffer %" GST_PTR_FORMAT, buf);

  gst_buffer_map (buf, &info_read, GST_MAP_READ);

  if (meta && !strcmp (meta->name, "EncodeBuffer")) {
    GST_DEBUG_OBJECT (encoder, "We have a buffer we allocated");
    v4l_input_buf = &meta->bufferinfo->v4l2_buf;        //&GST_ENCODE_BUFFER (buf)->buffer->v4l2_buf;
  } else {
    GST_DEBUG_OBJECT (encoder,
        "We have a buffer we didn't allocate, a memcpy is needed");

    /* Get a free Buffer */
    input_buf = gst_encode_buffer_pool_get_buffer (encoder->input_pool, FALSE);
    if (input_buf == NULL) {
      GST_ERROR_OBJECT (encoder, "cannot find free input buffers");
      goto failed;
    } else {
      meta =
          (GstEncodeBufferMeta *) gst_buffer_get_meta (buf,
          GST_ENCODEBUFFER_META_API_TYPE);
      if (meta && !strcmp (meta->name, "EncodeBuffer")) {
        gst_buffer_map (input_buf, &info_write, GST_MAP_WRITE);
        memcpy (info_write.data, info_read.data, info_read.size);
        v4l_input_buf =
            (struct v4l2_buffer *) &GST_ENCODE_BUFFER (input_buf)->
            buffer->v4l2_buf;
        gst_buffer_unmap (input_buf, &info_write);
      } else {
        goto failed;
      }
    }
  }

  /* Init input buffer */
  v4l_input_buf->bytesused = info_read.size;
  v4l_input_buf->field = 0;
  GST_TIME_TO_TIMEVAL (encoder->timestamp, v4l_input_buf->timestamp);
  v4l_input_buf->reserved = encoder->input_reserved;

  if (GST_STENCODE_GET_CLASS (encoder)->decorate_input_buffer)
    GST_STENCODE_GET_CLASS (encoder)->decorate_input_buffer (encoder,
        buf, v4l_input_buf);

  /* Queue input buffer */
  if (ioctl (encoder->v4l2_fd_input, VIDIOC_QBUF, v4l_input_buf) < 0) {
    GST_ERROR_OBJECT (encoder,
        "Err = %d (%s): Can not queue input buffer for frame raw #%lld", errno,
        strerror (errno), encoder->raw_frame_count + 1);
    goto failed_unlocked;
  }

  if (encoder->raw_frame_count == 0) {
    if (encoder->do_first_frame_hack) {
      if (encoder->timestamp == 0) {
        /* To be removed ASAP */
        /* BAD Workaround for first frame with timestamp 0 */
        encoder->first_frame = TRUE;
      }
    }

    if (encoder->do_initial_frame_hack) {
      /* To be removed ASAP */
      /* BAD Workaround for high timestamp of encoded buffer */
      encoder->initial_frame = TRUE;
    }

    /* Start read task */
    if (!GST_STENCODE_GET_CLASS (encoder)->start_task (encoder))
      goto failed_unlocked;

    /* Start encoder input */
    if (!encoder->streaming_output) {
      /* to serialize CAPTURE and OUTPUT streamon sequence
       * (CAPTURE streamon is done in read task started above)
       * if streamon done on CAPTURE device, do not wait*/
      g_mutex_lock (&encoder->stream_lock);
      if (!encoder->streaming_capture) {
        g_cond_wait (&encoder->stream_cond, &encoder->stream_lock);
      }
      g_mutex_unlock (&encoder->stream_lock);
      if (!gst_stencode_set_streaming (encoder, encoder->v4l2_fd_input,
              V4L2_BUF_TYPE_VIDEO_OUTPUT, TRUE)) {
        goto failed;
      }
    }
  }

  encoder->raw_frame_count++;

  /* DeQueue input buffer */
  if (ioctl (encoder->v4l2_fd_input, VIDIOC_DQBUF, v4l_input_buf) < 0) {
    GST_ERROR_OBJECT (encoder,
        "Err = %d (%s):  Can not DeQueue input buffer for frame raw #%lld",
        errno, strerror (errno), encoder->raw_frame_count);
    goto failed_unlocked;
  }

  if (!(meta && !strcmp (meta->name, "EncodeBuffer"))) {
    gst_buffer_unref (input_buf);
  }

  gst_buffer_unmap (buf, &info_read);
  return TRUE;

failed:
failed_unlocked:
  gst_buffer_unmap (buf, &info_read);
  return FALSE;
}

GstFlowReturn
gst_stencode_prechain (GstPad * pad, GstBuffer * buf)
{
  Gststencode *encoder = GST_STENCODE (GST_PAD_PARENT (pad));

  GST_LOG_OBJECT (encoder, "received buffer %" GST_PTR_FORMAT, buf);

  if (GST_BUFFER_TIMESTAMP_IS_VALID (buf))
    encoder->last_received_ts = GST_BUFFER_TIMESTAMP (buf);

  if (!gst_pad_get_current_caps (pad)) {
    GST_WARNING_OBJECT (encoder, "Buffer received before caps negotiation");
    return GST_FLOW_NOT_NEGOTIATED;
  }

  if (encoder->segment.format == GST_FORMAT_UNDEFINED) {
    GST_WARNING_OBJECT (encoder, "Buffer received without TIME segment");
    return GST_FLOW_ERROR;
  }

  if (encoder->srcresult != GST_FLOW_OK) {
    GST_ERROR_OBJECT (encoder, "error in data flow %s ",
        gst_flow_get_name (encoder->srcresult));
    return encoder->srcresult;
  }

  return GST_FLOW_OK;
}

GstFlowReturn
gst_stencode_postchain (Gststencode * encoder, GstBuffer * buf)
{
  if (GST_BUFFER_TIMESTAMP_IS_VALID (buf))
    encoder->timestamp = GST_BUFFER_TIMESTAMP (buf);

  if (gst_stencode_encoder_write (encoder, buf) == FALSE) {
    gst_buffer_unref (buf);
    if (encoder->srcresult == GST_FLOW_FLUSHING)
      return GST_FLOW_FLUSHING;
    else
      return GST_FLOW_ERROR;
  }

  /* And we restart the read_task if necessary */
  if (!GST_STENCODE_GET_CLASS (encoder)->start_task (encoder)) {
    gst_buffer_unref (buf);
    return GST_FLOW_ERROR;
  }

  gst_buffer_unref (buf);
  return GST_FLOW_OK;
}

gboolean
gst_stencode_allocate_input_buffers (Gststencode * encoder)
{
  encoder->input_pool = gst_encode_buffer_pool_create ("input encoder pool",
      encoder->v4l2_fd_input,
      encoder->desired_num_input_buffers, V4L2_BUF_TYPE_VIDEO_OUTPUT,
      encoder->input_reserved);
  return encoder->input_pool != NULL;
}

gboolean
gst_stencode_allocate_output_buffers (Gststencode * encoder)
{
  encoder->output_pool = gst_encode_buffer_pool_create ("output encoder pool",
      encoder->v4l2_fd_output,
      encoder->desired_num_output_buffers, V4L2_BUF_TYPE_VIDEO_CAPTURE,
      encoder->output_reserved);
  return encoder->output_pool != NULL;
}

static gboolean
gst_stencode_flush_start (Gststencode * encoder)
{
  if (encoder->streaming_output) {
    gst_stencode_set_streaming (encoder, encoder->v4l2_fd_input,
        V4L2_BUF_TYPE_VIDEO_OUTPUT, FALSE);
  }
  if (encoder->streaming_capture) {
    gst_stencode_set_streaming (encoder, encoder->v4l2_fd_output,
        V4L2_BUF_TYPE_VIDEO_CAPTURE, FALSE);
  }

  gst_encode_buffer_pool_flush_buffer (encoder->output_pool);

  return TRUE;
}

gboolean
gst_stencode_sink_event (GstPad * pad, GstEvent * event)
{
  gboolean ret = TRUE;
  Gststencode *encoder = NULL;

  encoder = GST_STENCODE (gst_pad_get_parent (pad));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_FLUSH_START:
      GST_DEBUG_OBJECT (encoder, "Got flush-start");
      encoder->srcresult = GST_FLOW_FLUSHING;
      encoder->flushing = TRUE;
      ret = gst_pad_push_event (encoder->srcpad, event);
      gst_stencode_flush_start (encoder);
      goto done;
      break;
    case GST_EVENT_FLUSH_STOP:
      GST_DEBUG_OBJECT (encoder, "Got flush-stop");
      encoder->srcresult = GST_FLOW_OK;
      GST_STENCODE_GET_CLASS (encoder)->reset (encoder, FALSE);
      ret = gst_pad_push_event (encoder->srcpad, event);
      goto done;
      break;
    case GST_EVENT_EOS:
      GST_DEBUG_OBJECT (encoder, "EOS event received");
      g_mutex_lock (&encoder->eos_lock);
      encoder->eos_received = TRUE;
      if (!gst_stencode_send_eos (encoder)) {
        gst_pad_stop_task (encoder->srcpad);
        ret = FALSE;
      }
      g_mutex_unlock (&encoder->eos_lock);
      gst_event_unref (event);
      goto done;
      break;
    case GST_EVENT_SEGMENT:
    {
      const GstSegment *segment;

      GST_OBJECT_LOCK (encoder);
      gst_event_parse_segment (event, &segment);

      if (segment->format == GST_FORMAT_TIME) {
        GST_DEBUG_OBJECT (encoder, "New segment update rate %f "
            "start %" GST_TIME_FORMAT " stop %" GST_TIME_FORMAT
            " position %" GST_TIME_FORMAT, segment->rate,
            GST_TIME_ARGS (segment->start), GST_TIME_ARGS (segment->stop),
            GST_TIME_ARGS (segment->position));

        /* FIXME: We should theorically drain all buffers here */
        gst_segment_copy_into (segment, &encoder->segment);
      } else {
#if 0
        GST_DEBUG_OBJECT (encoder, "received NEW_SEGMENT %" G_GINT64_FORMAT
            " -- %" G_GINT64_FORMAT ", time %" G_GINT64_FORMAT
            ", rate %g, applied_rate %g", start, stop, time, rate, arate);
#endif
        GST_DEBUG_OBJECT (encoder, "unsupported format; ignoring");
      }
      GST_OBJECT_UNLOCK (encoder);
      break;
    }
    case GST_EVENT_STREAM_START:
      ret = gst_pad_push_event (encoder->srcpad, event);
      goto done;
      break;
    case GST_EVENT_CAPS:
      goto done;
      break;
    default:
      break;
  }

  /* If this event is serialized, don't forward it now */
  if (GST_EVENT_IS_SERIALIZED (event)) {
    gst_stencode_queue_event (encoder, event, encoder->last_received_ts);
    gst_event_unref (event);
    ret = TRUE;
  } else {
    ret = gst_pad_push_event (encoder->srcpad, event);
  }

done:
  gst_object_unref (encoder);
  return ret;
}

GstFlowReturn
gst_stencode_push_buffer (Gststencode * encoder, GstBuffer * buf,
    GstClockTimeDiff threshold)
{
  GstClockTime ev_ts;

  /* Here, we are ready to sent a frame. Before we do, we check if we
     have any queued event that's to be sent before that timestamp,
     and send them if so. Since the timestamps we get from V4L2 can
     be slightly off due to the arithmetic done on them, we subtract
     some small offset (a tenth of a frame) to ensure we do not miss
     an event due to rounding error accumulation. */
  if (GST_BUFFER_TIMESTAMP (buf) > threshold) {
    ev_ts = GST_BUFFER_TIMESTAMP (buf) - threshold;
  } else {
    ev_ts = 0;
  }

  if (!gst_stencode_send_queued_events_before (encoder, ev_ts)) {
    GST_WARNING_OBJECT (encoder, "Failed to send queued event");
    /* TODO: return error, or ignore ? */
  }

  /* Push the encoded frame */
  encoder->srcresult = gst_pad_push (encoder->srcpad, buf);
  if (encoder->srcresult != GST_FLOW_OK) {
    GST_ERROR_OBJECT (encoder, "Failed to push buffer downstream - %s",
        gst_flow_get_name (encoder->srcresult));
    return encoder->srcresult;
  }
  return GST_FLOW_OK;
}
