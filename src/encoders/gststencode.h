/*
 * GStreamer
 * Copyright (C) 2005 Thomas Vander Stichele <thomas@apestaart.org>
 * Copyright (C) 2005 Ronald S. Bultje <rbultje@ronald.bitfreak.net>
 * Copyright (C) 2011 ST Microelectronics <<user@hostname.org>>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_STENCODE_H__
#define __GST_STENCODE_H__

#include <gst/gst.h>
#include <linux/videodev2.h>
#include "linux/dvb/dvb_v4l2_export.h"
#include "gststbufferpool.h"

#define GST_FIRST_BUFFER_ENCODE_VAL        95440        /* Value in seconds */

/* #defines don't like whitespacey bits */
#define GST_TYPE_STENCODE (gst_stencode_get_type())
#define GST_STENCODE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_STENCODE,Gststencode))
#define GST_STENCODE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_STENCODE,GststencodeClass))
#define GST_IS_STENCODE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_STENCODE))
#define GST_IS_STENCODE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_STENCODE))
#define GST_STENCODE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_STENCODE, GststencodeClass))

typedef struct _Gststencode Gststencode;
typedef struct _GststencodeClass GststencodeClass;

struct _Gststencode
{
  GstElement element;

  gint v4l2_fd_input;           /* Encoder file descriptor : /dev/videox (input = uncompressed data) */
  gint v4l2_fd_output;          /* Encoder file descriptor : /dev/videox (output = compressed data)) */
  GstEncodeBufferPool *input_pool;
  GstEncodeBufferPool *output_pool;

  guint dev_id;                 /* Encoder device id */

  guint desired_num_input_buffers;
  gboolean input_buf_allocated;
  guint desired_num_output_buffers;

  guint input_reserved;
  guint output_reserved;

  GstPad *sinkpad, *srcpad;

  GMutex eos_lock;              /* Semaphore used for EOS event */
  GCond eos_cond;
  gboolean eos_received;

  GMutex stream_lock;           /* Mutex to synchronize streamon */
  GCond stream_cond;            /* Cond to synchronize streamon */

  GMutex pool_init_mutex;

  gint64 timestamp;
  gint64 raw_frame_count;       /* Number of frames that were input  */
  gint64 encoded_frame_count;   /* Number of frames that were encoded  */
  gint64 skipped_frame_count;   /* Number of frames that were skipped  */

  GstFlowReturn srcresult;
  GstSegment segment;           /* The currently configured segment */

  /* A list of serialized events, to ensure we sent those events
     in the correct order along with buffers */
  GList *queued_events;
  GstClockTime last_received_ts;

  gboolean flushing;
  gboolean streaming_capture;   /* capture device streaming status */
  gboolean streaming_output;    /* output device streaming status */

  /* To be removed ASAP */
  /* BAD Workaround for first frame with timestamp 0 */
  gboolean do_first_frame_hack;
  gboolean first_frame;
  /* FIXME Bug 42028 - BAD Workaround for high timestamp of encoded buffer for few initial frames */
  gboolean do_initial_frame_hack;
  gboolean initial_frame;

  GMutex event_lock;            /* lock for queue/dequeue events */
};

struct _GststencodeClass
{
  GstElementClass parent_class;

    gboolean (*deallocate) (Gststencode *);
    gboolean (*term) (Gststencode *);
    gboolean (*start_task) (Gststencode *);
  void (*reset) (Gststencode *, gboolean);
  void (*decorate_input_buffer) (Gststencode * encoder, GstBuffer * buf,
      struct v4l2_buffer * v4l_input_buf);
};

/* An event, and the timestamp of the last buffer received
   before it. It should be sent in order. */
typedef struct
{
  GstClockTime ts;
  GstEvent *event;
} QueuedEvent;

void gst_stencode_queue_event (Gststencode * encoder, GstEvent * event,
    GstClockTime ts);
void gst_stencode_drop_queued_events (Gststencode * encoder);
gboolean gst_stencode_send_queued_events_before (Gststencode * encoder,
    GstClockTime ts);
gboolean gst_stencode_deallocate (Gststencode * encoder);
gboolean gst_stencode_term (Gststencode * encoder);
int gst_stencode_reqbufs (int fd, int nbufs, int type);
gboolean gst_stencode_free_output_buffers (Gststencode * encoder);
gboolean gst_stencode_free_input_buffers (Gststencode * encoder);
gboolean gst_stencode_set_streaming (Gststencode * encoder, int fd,
    int buffer_type, gboolean on);
gboolean gst_stencode_stop_streaming (Gststencode * encoder, int fd,
    int buffer_type);
gboolean gst_stencode_start_pad_task (Gststencode * encoder, GstTaskFunction f,
    const char *name);
gboolean gst_stencode_send_eos (Gststencode * encoder);
gint gst_stencode_find_free_input_buffer (Gststencode * encoder, gboolean eos);
GstBuffer *gst_stencode_get_free_input_buffer (Gststencode * encoder);
void gst_stencode_reset (Gststencode * encoder, gboolean full);
gboolean gst_stencode_encoder_write (Gststencode * encoder, GstBuffer * buf);
GstFlowReturn gst_stencode_prechain (GstPad * pad, GstBuffer * buf);
GstFlowReturn gst_stencode_postchain (Gststencode * encoder, GstBuffer * buf);
gboolean gst_stencode_allocate_input_buffers (Gststencode * encoder);
gboolean gst_stencode_allocate_output_buffers (Gststencode * encoder);
gboolean gst_stencode_sink_event (GstPad * pad, GstEvent * event);
GstFlowReturn gst_stencode_push_buffer (Gststencode * encoder, GstBuffer * buf,
    GstClockTimeDiff threshold);

GType gst_stencode_get_type (void);

#endif /* __GST_STENCODE_H__ */
