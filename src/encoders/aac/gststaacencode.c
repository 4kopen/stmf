/* Gstreamer ST AAC Encoder Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:element-staacencode
 *
 * FIXME:Describe staacencode here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! staacencode ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/ioctl.h>
#include <string.h>
#include <gst/gst.h>

#include "gststaacencode.h"
#include "v4l2_utils.h"

/* Array matching the sampling frequencies to the values expected by the
 * v4l2 API.
 *
 * The index of a value in the array corresponds to the value of
 * the stm_v4l2_audenc_sampling_freq_table enum */
static guint const sampling_freq[] = {
  4000, 5000, 6000, 8000, 11025, 12000, 16000, 22050, 24000, 32000,
  44100, 48000, 64000, 88200, 96000, 128000, 176400, 192000, 256000,
  352800, 384000, 512000, 705600, 768000,
};

#define DEFAULT_SAMPLE_RATE_INDEX           11

GST_DEBUG_CATEGORY_STATIC (gst_staacencode_debug);
#define GST_CAT_DEFAULT gst_staacencode_debug


/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

/* the capabilities of the outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/mpeg, "
        "mpegversion = (int) 4, "
        "stream-format= (string) adts,"
        "channels = (int) [1, 8], " "rate = (int) [ 8000, 48000]; ")
    );

#define gst_staacencode_parent_class parent_class
G_DEFINE_TYPE (Gststaacencode, gst_staacencode, GST_TYPE_STAUDIOENCODE);

static gboolean gst_staacencode_controls_init (Gststaudioencode * audioencoder,
    int fd);

/* GObject vmethod implementations */
/* initialize the staacencode's class */
static void
gst_staacencode_class_init (GststaacencodeClass * klass)
{
  GststaudioencodeClass *audioencode_class;

  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  gst_element_class_set_details_simple (element_class,
      "ST AAC Encoder",
      "Codec/Encoder/Audio",
      "GStreamer AAC Encoder Element for ST", "http://www.st.com");

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_factory));

  audioencode_class = GST_STAUDIOENCODE_CLASS (klass);
  audioencode_class->init_controls = gst_staacencode_controls_init;
}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
static void
gst_staacencode_init (Gststaacencode * aacencoder)
{
  Gststaudioencode *audioencoder = GST_STAUDIOENCODE (aacencoder);
  Gststencode *encoder = GST_STENCODE (aacencoder);

  GST_LOG_OBJECT (aacencoder, "%s:%d\n", __FUNCTION__, __LINE__);

  audioencoder->codec_type = V4L2_MPEG_AUDIO_STM_ENCODING_AAC;
  audioencoder->fixed_rate = FALSE;
  encoder->do_initial_frame_hack = TRUE;

  encoder->srcpad = gst_pad_new_from_static_template (&src_factory, "src");
  gst_element_add_pad (GST_ELEMENT (aacencoder), encoder->srcpad);
}

static gboolean
gst_staacencode_controls_init (Gststaudioencode * audioencoder, int fd)
{
  struct v4l2_ext_control ext_ctrl[20];
  struct v4l2_ext_controls ext_ctrls;
  gint i;
  gboolean found = FALSE;

  GST_LOG_OBJECT (audioencoder, "Getting params");

  /* all the controls to set are extended controls from the MPEG class */

  ext_ctrl[0].id = V4L2_CID_MPEG_STM_AUDIO_BITRATE_MODE;
  ext_ctrl[0].value = audioencoder->encode_br_mode;

  ext_ctrl[1].id = V4L2_CID_MPEG_STM_AUDIO_VBR_QUALITY_FACTOR;
  ext_ctrl[1].value = audioencoder->vbr_quality_factor;

  ext_ctrl[2].id = V4L2_CID_MPEG_STM_AUDIO_BITRATE_CAP;
  ext_ctrl[2].value = audioencoder->br_cap;

  ext_ctrl[3].id = V4L2_CID_MPEG_AUDIO_AAC_BITRATE;
  ext_ctrl[3].value = audioencoder->bit_rate;

  ext_ctrl[4].id = V4L2_CID_MPEG_STM_AUDIO_BITRATE_CONTROL;
  ext_ctrl[4].value = 1;

  ext_ctrl[5].id = V4L2_CID_MPEG_STM_AUDIO_CHANNEL_COUNT;
  ext_ctrl[5].value = audioencoder->output_info.channels;

  ext_ctrl[6].id = V4L2_CID_MPEG_AUDIO_SAMPLING_FREQ;
  for (i = 0; i < G_N_ELEMENTS (sampling_freq); i++) {
    if (sampling_freq[i] == audioencoder->output_info.rate) {
      GST_DEBUG_OBJECT (audioencoder, "Setting sample rate to %u", i);
      ext_ctrl[6].value = i;
      found = TRUE;
      break;
    }
  }
  if (!found)
    ext_ctrl[6].value = DEFAULT_SAMPLE_RATE_INDEX;

  for (i = 0; i < audioencoder->output_info.channels; i++) {
    ext_ctrl[i + 7].id = V4L2_CID_MPEG_STM_AUDIO_CHANNEL_MAP;
    ext_ctrl[i + 7].value = 0;
    ext_ctrl[i + 7].value |= i;
    ext_ctrl[i + 7].value <<= 8;
    ext_ctrl[i + 7].value |= audioencoder->src_metadata.channel[i];
  }

  ext_ctrl[i + 7].id = V4L2_CID_MPEG_STM_AUDIO_CORE_FORMAT;
  ext_ctrl[i + 7].value = 1;

  ext_ctrl[i + 8].id = V4L2_CID_MPEG_STM_AUDIO_SERIAL_CONTROL;
  ext_ctrl[i + 8].value = audioencoder->serial_control;

  ext_ctrl[i + 9].id = V4L2_CID_MPEG_STM_AUDIO_PROGRAM_LEVEL;
  ext_ctrl[i + 9].value = audioencoder->program_level;

  ext_ctrls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
  ext_ctrls.controls = ext_ctrl;
  ext_ctrls.count = i + 10;

  if (ioctl (fd, VIDIOC_S_EXT_CTRLS, &ext_ctrls) < 0) {
    GST_ERROR_OBJECT (audioencoder, "cannot set the extended controls - %s",
        strerror (errno));
    return FALSE;
  }

  return TRUE;
}

/* entry point to initialize the element */
gboolean
staacencode_init (GstPlugin * staacencode)
{
  GST_DEBUG_CATEGORY_INIT (gst_staacencode_debug, "staacencode", 0,
      "ST AAC encoder");

  return gst_element_register (staacencode, "staacencode", GST_RANK_PRIMARY,
      GST_TYPE_STAACENCODE);
}
