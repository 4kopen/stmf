/*
 * GStreamer
 * Copyright (C) 2005 Thomas Vander Stichele <thomas@apestaart.org>
 * Copyright (C) 2005 Ronald S. Bultje <rbultje@ronald.bitfreak.net>
 * Copyright (C) 2011 ST Microelectronics <<user@hostname.org>>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_STAUDIOENCODE_H__
#define __GST_STAUDIOENCODE_H__

#include <gst/gst.h>
#include <gst/audio/audio.h>
#include "gststencode.h"

G_BEGIN_DECLS
/* #defines don't like whitespacey bits */
#define GST_TYPE_STAUDIOENCODE (gst_staudioencode_get_type())
#define GST_STAUDIOENCODE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_STAUDIOENCODE,Gststaudioencode))
#define GST_STAUDIOENCODE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_STAUDIOENCODE,GststaudioencodeClass))
#define GST_IS_STAUDIOENCODE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_STAUDIOENCODE))
#define GST_IS_STAUDIOENCODE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_STAUDIOENCODE))
#define GST_STAUDIOENCODE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_STAUDIOENCODE, GststaudioencodeClass))
typedef struct _Gststaudioencode Gststaudioencode;
typedef struct _GststaudioencodeClass GststaudioencodeClass;

typedef enum
{
  GST_STAUDIOENCODE_BR_MODE_VBR,
  GST_STAUDIOENCODE_BR_MODE_CBR
} GstStAudioEncodeBrMode;


struct _Gststaudioencode
{
  Gststencode parent;

  /* Properties */
  guint encode_br_mode;         /* 0 for vbr mode and 1 for cbr mode */
  guint vbr_quality_factor;     /* a quality factor for VBR modes. for MP3 and AAC the quality factor is expressed from 0 (lowest quality) to 100 (highest quality) */
  guint br_cap;                 /* for VBR and ABR mode only: if set then this field indicates the instant bitrate in bps that should not be exceeded.
                                   if set to 0 if there is no cap (pure VBR or ABR) */
  guint bit_rate;               /* The target bitrate in bps */
  guint serial_control;         /* Describes the serial copy managment system */
  gint program_level;           /* Program level AKA dialog level at which an audio stream should be decoded/rendered */

  gboolean fixed_rate;

  struct v4l2_audenc_src_metadata src_metadata;
  struct v4l2_audenc_dst_metadata dst_metadata;

  enum stm_v4l2_mpeg_audio_encoding codec_type;

  GstAudioInfo input_info;
  GstAudioInfo output_info;
  GstCaps *prev_caps;
  GstBufferPool *pool;
};

struct _GststaudioencodeClass
{
  GststencodeClass parent_class;

    gboolean (*init_controls) (Gststaudioencode * audioencode, int fd);
};

GType gst_staudioencode_get_type (void);

gboolean staudioencode_init (GstPlugin * staudioencode);

G_END_DECLS
#endif /* __GST_STAUDIOENCODE_H__ */
