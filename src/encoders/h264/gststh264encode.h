/*
 * GStreamer
 * Copyright (C) 2005 Thomas Vander Stichele <thomas@apestaart.org>
 * Copyright (C) 2005 Ronald S. Bultje <rbultje@ronald.bitfreak.net>
 * Copyright (C) 2011 ST Microelectronics <<user@hostname.org>>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_STH264ENCODE_H__
#define __GST_STH264ENCODE_H__

#include <gst/gst.h>
#include "gststencode.h"

G_BEGIN_DECLS
/* #defines don't like whitespacey bits */
#define GST_TYPE_STH264ENCODE (gst_sth264encode_get_type())
#define GST_STH264ENCODE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_STH264ENCODE,Gststh264encode))
#define GST_STH264ENCODE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_STH264ENCODE,Gststh264encodeClass))
#define GST_IS_STH264ENCODE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_STH264ENCODE))
#define GST_IS_STH264ENCODE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_STH264ENCODE))
typedef struct _Gststh264encode Gststh264encode;
typedef struct _Gststh264encodeClass Gststh264encodeClass;


typedef enum
{
  GST_ST_BR_MODE_VBR,
  GST_ST_BR_MODE_CBR
} GstStH26EncodeBrMode;


struct _Gststh264encode
{
  Gststencode parent;

  gint input_width;             /* width of the frame to be encoded */
  gint input_height;
  gint input_fps_num;
  gint input_fps_den;

  gint output_width;            /*width and height of encoded frame */
  gint output_height;
  gint output_fps_num;
  gint output_fps_den;

  gint output_ar_num;
  gint output_ar_den;
  gint output_aspect_ratio;     /* Output Aspect Ratio */

  guint bitrateBps;             /* bitrate in bits per second */
  guint encode_gop_size;        /* gop size in frame */
  guint encode_br_mode;         /* Bitrate mode, either CBR or VBR (enum from V4L2 api) */
  guint max_end_to_end_delay;   /* Max end to end delay, will impact the CPB Buffer */

  gint par_num, par_den;

  GList *key_unit_events;
  GList *pending_key_unit_events;
  GstCaps *prev_caps;
  GstCaps *srccaps;
};

struct _Gststh264encodeClass
{
  GststencodeClass parent_class;
};

GType gst_sth264encode_get_type (void);

gboolean sth264encode_init (GstPlugin * sth264encode);

G_END_DECLS
#endif /* __GST_STH264ENCODE_H__ */
