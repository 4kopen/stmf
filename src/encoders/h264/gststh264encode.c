/* Gstreamer ST H264 Encoder Plugin
 *
 * Copyright (C) 2012 STMicroelectronics - All Rights Reserved
 *
 * Author(s): STMicroelectronics.
 *
 * License type: LGPLv2.1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:element-sth264encode
 *
 * FIXME:Describe sth264encode here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! sth264encode ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <gst/gst.h>
#include <gst/video/video.h>

#include "gststh264encode.h"
#include "gststbufferpool.h"
#include <sys/mman.h>
#include "v4l2_utils.h"

#if 0
/* A useful debugging helper */
#ifndef ioctl
#define ioctl(fd,ioctlno,args...) \
  ({int ioctl_ret; do { \
    GST_DEBUG_OBJECT (h264encoder, "Calling ioctl "#ioctlno); \
    ioctl_ret = ioctl(fd,ioctlno,##args); \
    GST_DEBUG_OBJECT (h264encoder, "ioctl "#ioctlno" return: %d", ioctl_ret); \
  } while(0); \
  ioctl_ret;})
#endif
#endif

#ifndef min
#define min(a, b) (a < b ? a : b)
#endif

#define DEFAULT_WIDTH        1280
#define DEFAULT_HEIGHT       720
#define DEFAULT_FPS_NUM      25
#define DEFAULT_FPS_DEN      1

#define DEFAULT_AR_NUM          1
#define DEFAULT_AR_DEN          1
#define DEFAULT_ASPECT_RATIO    V4L2_STM_ENCODER_ASPECT_RATIO_IGNORE

#define MIN_BITRATE          1
#define MAX_BITRATE          G_MAXUINT
#define DEFAULT_BITRATE      4000000

#define MIN_ENCODE_GOP_SIZE      2
#define MAX_ENCODE_GOP_SIZE      50
#define DEFAULT_ENCODE_GOP_SIZE  15

#define DEFAULT_ENCODE_BR_MODE                GST_ST_BR_MODE_VBR

#define MIN_MAX_END_TO_END_DELAY_IN_MS      50
#define MAX_MAX_END_TO_END_DELAY_IN_MS      8000
#define DEFAULT_MAX_END_TO_END_DELAY_IN_MS  2000

#define DEFAULT_NUMBER_OF_INPUT_BUFFER    4
#define DEFAULT_NUMBER_OF_OUTPUT_BUFFER   4

#define MIN_WIDTH 1
#define MIN_HEIGHT 1
#define MAX_WIDTH 1920
#define MAX_HEIGHT 1088

#define GST_TYPE_H264ENCODE_BR_MODE (gst_sth264encode_br_mode_get_type ())
static GType
gst_sth264encode_br_mode_get_type (void)
{
  static GType sth264encode_br_mode_type = 0;
  static GEnumValue br_mode_types[] = {
    {GST_ST_BR_MODE_VBR, "VBR", "vbr"},
    {GST_ST_BR_MODE_CBR, "CBR", "cbr"},
    {0, NULL, NULL},
  };

  if (!sth264encode_br_mode_type) {
    sth264encode_br_mode_type =
        g_enum_register_static ("GstStH26EncodeBrMode", br_mode_types);
  }

  return sth264encode_br_mode_type;
}

GST_DEBUG_CATEGORY_STATIC (gst_sth264encode_debug);
#define GST_CAT_DEFAULT gst_sth264encode_debug


/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_DFB,
  PROP_BITRATE_BPS,
  PROP_ENCODE_GOP_SIZE,
  PROP_ENCODE_BR_MODE,
  PROP_MAX_END_TO_END_DELAY,
  PROP_LAST
};

typedef enum
{
  INPUT_PAD,
  OUTPUT_PAD
} encoder_pad;

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-raw, "
        "format = (string) UYVY, "
        "width = (int) [ 1, 1920 ], "
        "height = (int) [ 1, 1088 ], " "framerate = (fraction) [ 0, MAX ];")
    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-h264,"
        "width = (int) [ 0, 1920 ],"
        "height = (int) [ 0, 1088 ]," "framerate = (fraction) [ 0, MAX ], "
        "stream-format = (string) byte-stream, alignment = (string)au;")
    );

#define gst_sth264encode_parent_class parent_class
G_DEFINE_TYPE (Gststh264encode, gst_sth264encode, GST_TYPE_STENCODE);

static void gst_sth264encode_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_sth264encode_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static gboolean gst_sth264encode_set_caps (Gststh264encode * h264encoder,
    GstCaps * caps);
static gboolean gst_sth264encode_sink_event (GstPad * pad, GstObject * parent,
    GstEvent * event);
static gboolean gst_sth264encode_sink_query (GstPad * pad, GstObject * parent,
    GstQuery * query);
static gboolean gst_sth264encode_src_event (GstPad * pad, GstObject * parent,
    GstEvent * event);
static GstFlowReturn gst_sth264encode_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buf);
static GstStateChangeReturn gst_sth264encode_change_state (GstElement * element,
    GstStateChange transition);

static gboolean gst_sth264encode_setup_and_allocate (Gststh264encode *
    h264encoder, encoder_pad pad);
static gboolean gst_sth264encode_controls_init (Gststh264encode * h264encoder);
static void gst_sth264encode_encoder_read (GstPad * pad);
static gboolean gst_sth264encode_start_pad_task (Gststencode * encoder);
static void gst_h264encoder_decorate_input_buffer (Gststencode * encoder,
    GstBuffer * buf, struct v4l2_buffer *v4l_input_buf);
static GstBufferPool *gst_sth264encode_poolalloc (GstPad * pad,
    const GstStructure * structure);
#if 0
static void gst_h264encoder_element_init_interfaces (GType object_type);

static void
gst_h264encoder_element_init_interfaces (GType object_type)
{
  const GInterfaceInfo preset_interface_info = {
    NULL,                       /* interface_init */
    NULL,                       /* interface_finalize */
    NULL                        /* interface_data */
  };

  g_type_add_interface_static (object_type, GST_TYPE_PRESET,
      &preset_interface_info);
}

G_DEFINE_TYPE_EXTENDED (Gststh264encode, gst_sth264encode, GST_TYPE_ELEMENT, 0,
    G_IMPLEMENT_INTERFACE (GST_TYPE_PRESET_HANDLER,
        gst_h264encoder_element_init_interfaces));
#endif
/* initialize the sth264encode's class */
static void
gst_sth264encode_class_init (Gststh264encodeClass * klass)
{
  GObjectClass *gobject_class;
  GststencodeClass *encoder_class;
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  gobject_class = (GObjectClass *) klass;
  encoder_class = (GststencodeClass *) klass;


  gst_element_class_set_static_metadata (element_class,
      "ST H264 Encoder",
      "Codec/Encoder/Video",
      "GStreamer H264 Encoder Element for ST", "http://www.st.com");

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_factory));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_factory));

  element_class->change_state = gst_sth264encode_change_state;

  gobject_class->set_property = gst_sth264encode_set_property;
  gobject_class->get_property = gst_sth264encode_get_property;

  encoder_class->start_task = gst_sth264encode_start_pad_task;
  encoder_class->decorate_input_buffer = gst_h264encoder_decorate_input_buffer;

  g_object_class_install_property (gobject_class, PROP_BITRATE_BPS,
      g_param_spec_uint ("bitrate", "bitrate in bit per second",
          "bitrate in bit per second", MIN_BITRATE, MAX_BITRATE,
          DEFAULT_BITRATE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));

  g_object_class_install_property (gobject_class, PROP_ENCODE_GOP_SIZE,
      g_param_spec_uint ("gop-size", "Encode GOP Size", "Encoding GOP Size",
          MIN_ENCODE_GOP_SIZE, MAX_ENCODE_GOP_SIZE, DEFAULT_ENCODE_GOP_SIZE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));
  g_object_class_install_property (gobject_class, PROP_ENCODE_BR_MODE,
      g_param_spec_enum ("br-mode",
          "br-mode",
          "Encode Bitrate mode",
          GST_TYPE_H264ENCODE_BR_MODE, DEFAULT_ENCODE_BR_MODE,
          G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_MAX_END_TO_END_DELAY,
      g_param_spec_uint ("max-end-to-end-delay",
          "For CBR, max end to end delay before first frame",
          "For CBR, max end to end delay before first frame",
          MIN_MAX_END_TO_END_DELAY_IN_MS, MAX_MAX_END_TO_END_DELAY_IN_MS,
          DEFAULT_MAX_END_TO_END_DELAY_IN_MS,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | GST_PARAM_CONTROLLABLE));
}

static gboolean
gst_sth264encode_start_pad_task (Gststencode * encoder)
{
  return gst_stencode_start_pad_task (encoder,
      (GstTaskFunction) gst_sth264encode_encoder_read, "sth264encode_read");
}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
static void
gst_sth264encode_init (Gststh264encode * h264encoder)
{
  Gststencode *encoder = GST_STENCODE (h264encoder);

  GST_LOG_OBJECT (h264encoder, "initializing encoder");

  encoder->desired_num_input_buffers = DEFAULT_NUMBER_OF_INPUT_BUFFER;
  encoder->desired_num_output_buffers = DEFAULT_NUMBER_OF_OUTPUT_BUFFER;
  h264encoder->input_width = DEFAULT_WIDTH;
  h264encoder->output_width = DEFAULT_WIDTH;
  h264encoder->input_height = DEFAULT_HEIGHT;
  h264encoder->output_height = DEFAULT_HEIGHT;
  h264encoder->input_fps_num = DEFAULT_FPS_NUM;
  h264encoder->output_fps_num = DEFAULT_FPS_NUM;
  h264encoder->input_fps_den = DEFAULT_FPS_DEN;
  h264encoder->output_fps_den = DEFAULT_FPS_DEN;
  h264encoder->output_ar_num = DEFAULT_AR_NUM;
  h264encoder->output_ar_den = DEFAULT_AR_DEN;
  h264encoder->output_aspect_ratio = DEFAULT_ASPECT_RATIO;
  h264encoder->bitrateBps = DEFAULT_BITRATE;
  h264encoder->encode_gop_size = DEFAULT_ENCODE_GOP_SIZE;
  h264encoder->encode_br_mode = V4L2_MPEG_VIDEO_BITRATE_MODE_VBR;
  h264encoder->max_end_to_end_delay = DEFAULT_MAX_END_TO_END_DELAY_IN_MS;
  h264encoder->par_num = 1;
  h264encoder->par_den = 1;

  encoder->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
  gst_pad_set_event_function (encoder->sinkpad,
      GST_DEBUG_FUNCPTR (gst_sth264encode_sink_event));
  gst_pad_set_query_function (encoder->sinkpad,
      GST_DEBUG_FUNCPTR (gst_sth264encode_sink_query));

  encoder->srcpad = gst_pad_new_from_static_template (&src_factory, "src");
  gst_pad_set_event_function (encoder->srcpad,
      GST_DEBUG_FUNCPTR (gst_sth264encode_src_event));

  gst_pad_set_chain_function (encoder->sinkpad,
      GST_DEBUG_FUNCPTR (gst_sth264encode_chain));

  gst_element_add_pad (GST_ELEMENT (encoder), encoder->sinkpad);
  gst_pad_set_active (encoder->srcpad, TRUE);
  gst_element_add_pad (GST_ELEMENT (encoder), encoder->srcpad);
  gst_pad_use_fixed_caps (encoder->srcpad);

  gst_stencode_reset (encoder, TRUE);
}

static void
gst_sth264encode_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  Gststh264encode *h264encoder = GST_STH264ENCODE (object);
  GstStH26EncodeBrMode br_mode;

  switch (prop_id) {
    case PROP_BITRATE_BPS:
      h264encoder->bitrateBps = g_value_get_uint (value);
      break;
    case PROP_ENCODE_GOP_SIZE:
      h264encoder->encode_gop_size = g_value_get_uint (value);
      break;
    case PROP_ENCODE_BR_MODE:
      br_mode = g_value_get_enum (value);
      if (br_mode == GST_ST_BR_MODE_CBR) {
        h264encoder->encode_br_mode = V4L2_MPEG_VIDEO_BITRATE_MODE_CBR;
        break;
      } else if (br_mode == GST_ST_BR_MODE_VBR) {
        h264encoder->encode_br_mode = V4L2_MPEG_VIDEO_BITRATE_MODE_VBR;
        break;
      } else {
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
      }
    case PROP_MAX_END_TO_END_DELAY:
      h264encoder->max_end_to_end_delay = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_sth264encode_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  Gststh264encode *h264encoder = GST_STH264ENCODE (object);

  switch (prop_id) {
    case PROP_BITRATE_BPS:
      g_value_set_uint (value, h264encoder->bitrateBps);
      break;
    case PROP_ENCODE_GOP_SIZE:
      g_value_set_uint (value, h264encoder->encode_gop_size);
      break;
    case PROP_MAX_END_TO_END_DELAY:
      g_value_set_uint (value, h264encoder->max_end_to_end_delay);
      break;
    case PROP_ENCODE_BR_MODE:
      if (h264encoder->encode_br_mode != V4L2_MPEG_VIDEO_BITRATE_MODE_VBR &&
          h264encoder->encode_br_mode != V4L2_MPEG_VIDEO_BITRATE_MODE_CBR) {
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
      }
      g_value_set_enum (value,
          h264encoder->encode_br_mode ==
          V4L2_MPEG_VIDEO_BITRATE_MODE_VBR ? GST_ST_BR_MODE_VBR :
          GST_ST_BR_MODE_CBR);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static gboolean
gst_sth264encode_sink_query (GstPad * pad, GstObject * parent, GstQuery * query)
{
  gboolean res = FALSE;
  const GstStructure *structure;
  Gststencode *encoder = NULL;
  GstBufferPool *pool = NULL;

  encoder = GST_STENCODE (GST_OBJECT_PARENT (pad));

  GST_LOG_OBJECT (pad, "%s query", GST_QUERY_TYPE_NAME (query));

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_ALLOCATION:
      structure = gst_query_get_structure (query);
      pool = gst_sth264encode_poolalloc (pad, structure);
      if (pool != NULL) {
        gst_query_add_allocation_pool (query, pool, DUMMY_POOL_SIZE,
            encoder->desired_num_input_buffers,
            encoder->desired_num_input_buffers);
        gst_object_unref (pool);
        res = TRUE;
      }
      break;
    case GST_QUERY_CUSTOM:
      structure = gst_query_writable_structure (query);
      if (structure) {
        if (!strcmp ("video-entity", gst_structure_get_name (structure))) {
          res = FALSE;
        }
      } else {
        res = gst_pad_query_default (pad, parent, query);
      }
      break;

    default:
      res = gst_pad_query_default (pad, parent, query);
      break;
  }
  return res;
}

/* GstElement vmethod implementations */
static GstStateChangeReturn
gst_sth264encode_change_state (GstElement * element, GstStateChange transition)
{
  Gststh264encode *h264encoder = (Gststh264encode *) element;

  GST_DEBUG_OBJECT (h264encoder, "%d -> %d",
      GST_STATE_TRANSITION_CURRENT (transition),
      GST_STATE_TRANSITION_NEXT (transition));

  /* upwards transitions */
  switch (transition) {
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      g_list_free_full (h264encoder->key_unit_events,
          (GDestroyNotify) gst_event_unref);
      g_list_free_full (h264encoder->pending_key_unit_events,
          (GDestroyNotify) gst_event_unref);
      h264encoder->key_unit_events = NULL;
      h264encoder->pending_key_unit_events = NULL;
      break;

    default:
      break;
  }

  return GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
}

/* Return TRUE if we can use those caps, FALSE otherwise. Fields are
   extracted to the appropriate parameters. */
static gboolean
gst_sth264encode_check_caps (const GstCaps * caps, int *width, int *height,
    int *fps_num, int *fps_den, int *par_num, int *par_den)
{
  const GstStructure *structure;

  if (!caps)
    return FALSE;
  structure = gst_caps_get_structure (caps, 0);
  /* We're assuming video/x-raw-yuv,format=UYVY as per template caps */
  if (!gst_structure_get (structure,
          "width", G_TYPE_INT, width, "height", G_TYPE_INT, height,
          "framerate", GST_TYPE_FRACTION, fps_num, fps_den, NULL))
    return FALSE;
  if (!gst_structure_get (structure,
          "pixel-aspect-ratio", GST_TYPE_FRACTION, par_num, par_den, NULL)) {
    GST_WARNING ("pixel-aspect-ratio not found in caps, assuming 1/1");
    *par_num = *par_den = 1;
  }
  GST_DEBUG ("Caps %" GST_PTR_FORMAT ": %dx%d, %d/%d fps, %d/%d par",
      caps, *width, *height, *fps_num, *fps_den, *par_num, *par_den);
  return TRUE;
}

/* this function handles the link with other elements */
static gboolean
gst_sth264encode_set_caps (Gststh264encode * h264encoder, GstCaps * caps)
{
  Gststencode *encoder = NULL;
  const GstStructure *structure = NULL;
  const gchar *mimetype = NULL;
  GstStructure *srcstructure = NULL;
  GstCaps *allowed_caps = NULL;
  GstCaps *srccaps = NULL;
  int width, height, fps_num, fps_den, par_num, par_den;

  encoder = GST_STENCODE (h264encoder);
  structure = gst_caps_get_structure (caps, 0);
  mimetype = gst_structure_get_name (structure);

  GST_DEBUG_OBJECT (h264encoder, "received %s as MIME type", mimetype);

  /* First check caps are to our liking and get fields */
  if (!gst_sth264encode_check_caps (caps, &width, &height, &fps_num, &fps_den,
          &par_num, &par_den)) {
    GST_ERROR_OBJECT (h264encoder, "Unsupported caps: %" GST_PTR_FORMAT, caps);
    return FALSE;
  }

  if (!strcmp (mimetype, "video/x-raw")) {
    h264encoder->input_width = width;
    h264encoder->input_height = height;
    if ((fps_num != 0) && (fps_den != 0)) {
      h264encoder->input_fps_num = fps_num;
      h264encoder->input_fps_den = fps_den;
    }
    h264encoder->par_num = par_num;
    h264encoder->par_den = par_den;
    GST_DEBUG_OBJECT (h264encoder, "Size %dx%d, Framerate=%d/%d, par=%d/%d",
        width, height, fps_num, fps_den, par_num, par_den);
  } else {
    GST_ERROR_OBJECT (h264encoder, "Unsupported caps: %" GST_PTR_FORMAT, caps);
    return FALSE;
  }

  if (gst_pad_get_current_caps (encoder->srcpad) == NULL) {
    allowed_caps = gst_pad_get_allowed_caps (encoder->srcpad);
    if (allowed_caps && !gst_caps_is_empty (allowed_caps)) {

      srccaps = gst_caps_copy_nth (allowed_caps, 0);
      gst_caps_unref (allowed_caps);

      srcstructure = gst_caps_get_structure (srccaps, 0);
      if (!strcmp (gst_structure_get_name (srcstructure), "video/x-h264")) {
        if (!gst_structure_get_fraction (srcstructure, "framerate",
                &h264encoder->output_fps_num, &h264encoder->output_fps_den)) {
          if (h264encoder->input_fps_den != 0) {
            h264encoder->output_fps_num =
                (h264encoder->input_fps_num + (h264encoder->input_fps_den -
                    1)) / h264encoder->input_fps_den;
            h264encoder->output_fps_den = 1;
          } else {
            h264encoder->output_fps_num = DEFAULT_FPS_NUM;
            h264encoder->output_fps_den = DEFAULT_FPS_DEN;
          }
        } else {
          if (h264encoder->output_fps_den != 0) {
            h264encoder->output_fps_num =
                (h264encoder->output_fps_num + (h264encoder->output_fps_den -
                    1)) / h264encoder->output_fps_den;
            h264encoder->output_fps_den = 1;
          } else {
            h264encoder->output_fps_num = DEFAULT_FPS_NUM;
            h264encoder->output_fps_den = DEFAULT_FPS_DEN;
          }
        }
        if (!gst_structure_get_int (srcstructure, "width",
                &h264encoder->output_width)) {
          h264encoder->output_width = h264encoder->input_width;
        }
        if (!gst_structure_get_int (srcstructure, "height",
                &h264encoder->output_height)) {
          h264encoder->output_height = h264encoder->input_height;
        }
        if (gst_structure_get_fraction (srcstructure, "aspect-ratio",
                &h264encoder->output_ar_num, &h264encoder->output_ar_den)) {
          if (h264encoder->output_ar_num == 1
              && h264encoder->output_ar_den == 1) {
            h264encoder->output_aspect_ratio =
                V4L2_STM_ENCODER_ASPECT_RATIO_IGNORE;
          } else if (h264encoder->output_ar_num == 4
              && h264encoder->output_ar_den == 3) {
            h264encoder->output_aspect_ratio =
                V4L2_STM_ENCODER_ASPECT_RATIO_4_3;
          } else if (h264encoder->output_ar_num == 16
              && h264encoder->output_ar_den == 9) {
            h264encoder->output_aspect_ratio =
                V4L2_STM_ENCODER_ASPECT_RATIO_16_9;
          }
        }
      }
      GST_DEBUG_OBJECT (h264encoder, "output_width=%d",
          h264encoder->output_width);
      GST_DEBUG_OBJECT (h264encoder, "output_height=%d",
          h264encoder->output_height);
      GST_INFO_OBJECT (h264encoder, "output_framerate num=%d den=%d",
          h264encoder->output_fps_num, h264encoder->output_fps_den);
      GST_INFO_OBJECT (h264encoder, "output_aspect_ratio num=%d den=%d",
          h264encoder->output_ar_num, h264encoder->output_ar_den);
      gst_structure_set (srcstructure, "width", G_TYPE_INT,
          h264encoder->output_width, "height", G_TYPE_INT,
          h264encoder->output_height, "framerate", GST_TYPE_FRACTION,
          h264encoder->output_fps_num, h264encoder->output_fps_den,
          "aspect-ratio", GST_TYPE_FRACTION, h264encoder->output_ar_num,
          h264encoder->output_ar_den, NULL);
      if (gst_structure_has_field (srcstructure, "pixel-aspect-ratio")) {
        gst_structure_fixate_field_nearest_fraction (srcstructure,
            "pixel-aspect-ratio", h264encoder->par_num, h264encoder->par_den);
        if (!gst_structure_get_fraction (srcstructure, "pixel-aspect-ratio",
                &h264encoder->par_num, &h264encoder->par_den)) {
          GST_WARNING_OBJECT (h264encoder, "Failed to get pixel-aspect-ratio");
          h264encoder->par_num = 1;
          h264encoder->par_den = 1;
        }
      } else {
        gst_structure_set (srcstructure, "pixel-aspect-ratio",
            GST_TYPE_FRACTION, h264encoder->par_num, h264encoder->par_den,
            NULL);
      }
      GST_DEBUG_OBJECT (h264encoder, "Negotiated caps: %" GST_PTR_FORMAT,
          srccaps);
    }
    /* Try and set caps on the source with what we negotiated */
    if (!gst_pad_set_caps (encoder->srcpad, srccaps)) {
      GST_ERROR_OBJECT (h264encoder,
          "Failed to set source caps: %" GST_PTR_FORMAT, srccaps);
      gst_caps_unref (srccaps);
      return FALSE;
    }
    gst_caps_unref (srccaps);
  }

  g_mutex_lock (&encoder->pool_init_mutex);
  if (!encoder->input_buf_allocated) {
    if (!gst_sth264encode_setup_and_allocate (h264encoder, INPUT_PAD)) {
      GST_ERROR_OBJECT (h264encoder,
          "cannot setup encoder input and allocate input buffers");
      g_mutex_unlock (&encoder->pool_init_mutex);
      return FALSE;
    }
  }
  if (!gst_sth264encode_setup_and_allocate (h264encoder, OUTPUT_PAD)) {
    GST_ERROR_OBJECT (h264encoder,
        "cannot setup encoder output and allocate output buffers");
    g_mutex_unlock (&encoder->pool_init_mutex);
    return FALSE;
  }

  /* Sets all the v4l controls */
  if (gst_sth264encode_controls_init (h264encoder) == FALSE) {
    GST_ERROR_OBJECT (h264encoder, "cannot init v4l controls");
    g_mutex_unlock (&encoder->pool_init_mutex);
    return FALSE;
  }
  g_mutex_unlock (&encoder->pool_init_mutex);
  return TRUE;
}

/*
 function: gst_sth264encode_poolalloc
 input params:
           pad - sinkpad
           structure - structure from query/caps
 description : this function will allocate pool of buffers
 return : pool address or NULL
 */
static GstBufferPool *
gst_sth264encode_poolalloc (GstPad * pad, const GstStructure * structure)
{
  Gststh264encode *h264encoder = NULL;
  Gststencode *encoder = NULL;


  h264encoder = GST_STH264ENCODE (GST_OBJECT_PARENT (pad));
  encoder = GST_STENCODE (GST_OBJECT_PARENT (pad));

  g_mutex_lock (&encoder->pool_init_mutex);
  if (!encoder->input_buf_allocated) {

    /* Caps changed (or were just set), so call setup function for input
       buffers (after freeing any we already had) */
    if (encoder->input_buf_allocated)
      gst_stencode_free_input_buffers (encoder);
    if (!gst_sth264encode_setup_and_allocate (h264encoder, INPUT_PAD)) {
      g_mutex_unlock (&encoder->pool_init_mutex);
      return NULL;
    }
  }
  g_mutex_unlock (&encoder->pool_init_mutex);

  return GST_BUFFER_POOL (encoder->input_pool);
}

static gboolean
gst_sth264encode_sink_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
  gboolean ret = TRUE;
  Gststh264encode *h264encoder = NULL;
  GstCaps *caps;
  struct v4l2_buffer *v4l2_buf;
  Gststencode *encoder;

  h264encoder = GST_STH264ENCODE (parent);
  encoder = GST_STENCODE (h264encoder);

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CUSTOM_DOWNSTREAM:{
      guint count;
      gboolean all_headers;
      GstClockTime pending_key_unit_ts;

      if (!gst_video_event_is_force_key_unit (event))
        break;

      GST_OBJECT_LOCK (h264encoder);

      gst_video_event_parse_downstream_force_key_unit (event, NULL, NULL,
          &pending_key_unit_ts, &all_headers, &count);

      GST_DEBUG_OBJECT (h264encoder,
          "received downstream force-key-unit event, "
          "seqnum %d running_time %" GST_TIME_FORMAT
          " all_headers %d count %d", gst_event_get_seqnum (event),
          GST_TIME_ARGS (pending_key_unit_ts), all_headers, count);

      h264encoder->key_unit_events =
          g_list_append (h264encoder->key_unit_events, event);

      GST_OBJECT_UNLOCK (h264encoder);
      break;
    }
    case GST_EVENT_CAPS:
      gst_event_parse_caps (event, &caps);
      if ((h264encoder->prev_caps != NULL)
          && (gst_caps_is_equal ((const GstCaps *) h264encoder->prev_caps,
                  (const GstCaps *) caps))) {
        GST_DEBUG_OBJECT (h264encoder, "caps are already set");
        return TRUE;
      }
      ret = gst_sth264encode_set_caps (h264encoder, caps);
      if (ret) {
        if (h264encoder->prev_caps) {
          gst_caps_unref (h264encoder->prev_caps);
        }
        h264encoder->prev_caps = gst_caps_ref (caps);
      } else {
        GST_ERROR_OBJECT (h264encoder, "failed to set caps!");
      }
      gst_event_unref (event);
      return ret;
      break;

    case GST_EVENT_FLUSH_STOP:
    {
      /* need to queue buffers for read (CAPTURE) */
      while ((v4l2_buf =
              gst_encode_buffer_pool_get_non_queued_buffer
              (encoder->output_pool, TRUE))) {
        /* Queue output buffers */
        v4l2_buf->field = 0;
        v4l2_buf->reserved = encoder->output_reserved;
        if (ioctl (encoder->v4l2_fd_output, VIDIOC_QBUF, v4l2_buf) < 0) {
          GST_ERROR_OBJECT (encoder, "VIDIOC_QBUF failed - %s",
              strerror (errno));
          return FALSE;
        }
      }

      if (!encoder->streaming_capture) {
        gst_stencode_set_streaming (encoder, encoder->v4l2_fd_output,
            V4L2_BUF_TYPE_VIDEO_CAPTURE, TRUE);
      }
      if (!encoder->streaming_output) {
        gst_stencode_set_streaming (encoder, encoder->v4l2_fd_input,
            V4L2_BUF_TYPE_VIDEO_OUTPUT, TRUE);
      }
    }
      break;

    default:
      break;
  }

  return gst_stencode_sink_event (pad, event);
}

static gboolean
gst_sth264encode_src_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
  gboolean ret = TRUE;
  gboolean forward = TRUE;
  Gststh264encode *h264encoder = NULL;

  h264encoder = GST_STH264ENCODE (parent);

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CUSTOM_UPSTREAM:{
      guint count;
      gboolean all_headers;
      GstClockTime pending_key_unit_ts;

      if (!gst_video_event_is_force_key_unit (event))
        break;

      GST_OBJECT_LOCK (h264encoder);

      gst_video_event_parse_upstream_force_key_unit (event,
          &pending_key_unit_ts, &all_headers, &count);

      GST_DEBUG_OBJECT (h264encoder, "received upstream force-key-unit event, "
          "seqnum %d running_time %" GST_TIME_FORMAT
          " all_headers %d count %d\n", gst_event_get_seqnum (event),
          GST_TIME_ARGS (pending_key_unit_ts), all_headers, count);

      h264encoder->key_unit_events =
          g_list_append (h264encoder->key_unit_events, event);
      GST_OBJECT_UNLOCK (h264encoder);
      forward = FALSE;
      break;
    }
    default:
      break;
  }

  if (forward)
    ret = gst_pad_push_event (GST_STENCODE (h264encoder)->sinkpad, event);

  return ret;
}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_sth264encode_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
  Gststh264encode *h264encoder;
  Gststencode *encoder;
  guint64 start, stop;
  GstFlowReturn ret;

  h264encoder = GST_STH264ENCODE (parent);
  encoder = GST_STENCODE (GST_OBJECT_PARENT (pad));

  ret = gst_stencode_prechain (pad, buf);
  if (ret != GST_FLOW_OK) {
    gst_buffer_unref (buf);
    return ret;
  }

  /* Check the frame is within our segment */
  start = GST_BUFFER_TIMESTAMP (buf);
  if (GST_CLOCK_TIME_IS_VALID (start)) {
    if (GST_BUFFER_DURATION_IS_VALID (buf)) {
      stop = start + GST_BUFFER_DURATION (buf);
    } else {
      stop = GST_CLOCK_TIME_NONE;
    }
    if (!gst_segment_clip (&encoder->segment,
            GST_FORMAT_TIME, start, stop, &start, &stop)) {
      GST_DEBUG_OBJECT (h264encoder, "Dropping buffer outside segment");
      gst_buffer_unref (buf);
      return encoder->srcresult;
    }
  }

  return gst_stencode_postchain (encoder, buf);
}

static gboolean
gst_sth264encode_setup_and_allocate (Gststh264encode * h264encoder,
    encoder_pad pad)
{
  Gststencode *encoder = GST_STENCODE (h264encoder);
  struct v4l2_format format;
  struct v4l2_streamparm param;
  struct v4l2_buffer *v4l2_buf;

  GST_LOG_OBJECT (h264encoder, "Setting up %s pad",
      pad == INPUT_PAD ? "input" : "output");

  if (pad == INPUT_PAD) {
    /* Configure encoder input */
    if (ioctl (encoder->v4l2_fd_input, VIDIOC_S_OUTPUT, &encoder->dev_id) < 0) {
      GST_ERROR_OBJECT (h264encoder, "cannot set input %d %d - %s",
          encoder->v4l2_fd_input, encoder->dev_id, strerror (errno));
      return FALSE;
    }
    /* Set input format */
    GST_DEBUG_OBJECT (h264encoder, "input_width=%d", h264encoder->input_width);
    GST_DEBUG_OBJECT (h264encoder, "input_height=%d",
        h264encoder->input_height);

    memset (&format, 0, sizeof (struct v4l2_format));
    format.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    format.fmt.pix.width = h264encoder->input_width;
    format.fmt.pix.height = h264encoder->input_height;
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_UYVY;
    format.fmt.pix.field = V4L2_FIELD_NONE;

    if (ioctl (encoder->v4l2_fd_input, VIDIOC_S_FMT, &format) < 0) {
      GST_ERROR_OBJECT (h264encoder, "cannot set requested input format - %s",
          strerror (errno));
      return FALSE;
    }

    /* Set source framerate */
    memset (&param, 0, sizeof (param));
    param.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    param.parm.output.timeperframe.numerator = h264encoder->input_fps_den;
    param.parm.output.timeperframe.denominator = h264encoder->input_fps_num;

    if (ioctl (encoder->v4l2_fd_input, VIDIOC_S_PARM, &param) < 0) {
      GST_ERROR_OBJECT (h264encoder,
          "cannot set requested input parameters - %s", strerror (errno));
      return FALSE;
    }

    /* Buffers allocation */
    if (!gst_stencode_allocate_input_buffers (encoder))
      return FALSE;

    encoder->input_buf_allocated = TRUE;
  } else {
    /* Configure encoder output */
    if (ioctl (encoder->v4l2_fd_output, VIDIOC_S_INPUT, &encoder->dev_id) < 0) {
      GST_ERROR_OBJECT (h264encoder, "cannot set input %d - %s",
          encoder->dev_id, strerror (errno));
      return FALSE;
    }

    GST_DEBUG_OBJECT (h264encoder, "output_width=%d",
        h264encoder->output_width);
    GST_DEBUG_OBJECT (h264encoder, "output_height=%d",
        h264encoder->output_height);

    memset (&format, 0, sizeof (struct v4l2_format));
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    format.fmt.pix.width = h264encoder->output_width;
    format.fmt.pix.height = h264encoder->output_height;
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_H264;

    if (ioctl (encoder->v4l2_fd_output, VIDIOC_S_FMT, &format) < 0) {
      GST_ERROR_OBJECT (h264encoder, "cannot set requested input format - %s",
          strerror (errno));
      return FALSE;
    }

    memset (&param, 0, sizeof (param));
    param.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    param.parm.capture.timeperframe.numerator = h264encoder->output_fps_den;
    param.parm.capture.timeperframe.denominator = h264encoder->output_fps_num;

    if (ioctl (encoder->v4l2_fd_output, VIDIOC_S_PARM, &param) < 0) {
      GST_ERROR_OBJECT (h264encoder,
          "cannot set requested input parameters - %s", strerror (errno));
      return FALSE;
    }

    /* Buffers allocation */
    if (!gst_stencode_allocate_output_buffers (encoder))
      return FALSE;

    while ((v4l2_buf =
            gst_encode_buffer_pool_get_raw_buffer (encoder->output_pool,
                TRUE))) {
      /* Queue output buffers */
      if (ioctl (encoder->v4l2_fd_output, VIDIOC_QBUF, v4l2_buf) < 0) {
        GST_ERROR_OBJECT (h264encoder, "VIDIOC_QBUF failed - %s",
            strerror (errno));
        return FALSE;
      }
    }
  }

  return TRUE;
}

static gboolean
gst_sth264encode_controls_init (Gststh264encode * h264encoder)
{
  Gststencode *encoder = GST_STENCODE (h264encoder);
  gint cpbBufferSize = 0;
  guint64 tmp_size = 0;
  struct v4l2_ext_control ext_ctrl[9];
  struct v4l2_ext_controls ext_ctrls;
  struct v4l2_control ctrl_p;

  GST_LOG_OBJECT (h264encoder, "Initializing encoder settings");

  /*
   * The cpb buffer size has 2 contraints :
   *    it brings end to end delay : Delay = CpbBufferSize / bitrate :  for now we use 8 sec as max delay.
   *    it has a max value for each level number : 140000 for level 3.1 (see h264 specs, ANNEXE A)
   * The rational behind MAX_END_TO_END_DELAY_IN_SECONDS = 8 seconds is that in transcode use case, we should not worry about delay
   * +>  For exemple HLS already bring few seconds latency due to chunks size.
   * This may be different for video conf use cases.
   * On the other hand, if the Cpb buffer is too small, the number of frame skip will increase.
   * Btw, this can be a way to test frame skip behaviour
   * */
  tmp_size = (guint64) h264encoder->bitrateBps *
      (guint64) h264encoder->max_end_to_end_delay;
  cpbBufferSize = min ((gint) (tmp_size / 1000), 14000000);
  GST_INFO_OBJECT (h264encoder, "Bitrate = %u CpbBufferSize = %d",
      h264encoder->bitrateBps, cpbBufferSize);

  /* all the controls to set are extended controls from the MPEG class */
  ext_ctrl[0].id = V4L2_CID_MPEG_VIDEO_BITRATE_MODE;
  ext_ctrl[0].size = 0;
  ext_ctrl[0].reserved2[0] = 0;
  ext_ctrl[0].value = h264encoder->encode_br_mode;

  ext_ctrl[1].id = V4L2_CID_MPEG_VIDEO_GOP_SIZE;
  ext_ctrl[1].size = 0;
  ext_ctrl[1].reserved2[0] = 0;
  ext_ctrl[1].value = h264encoder->encode_gop_size;

  ext_ctrl[2].id = V4L2_CID_MPEG_VIDEO_H264_PROFILE;
  ext_ctrl[2].size = 0;
  ext_ctrl[2].reserved2[0] = 0;
  ext_ctrl[2].value = V4L2_MPEG_VIDEO_H264_PROFILE_HIGH;

  ext_ctrl[3].id = V4L2_CID_MPEG_VIDEO_H264_LEVEL;
  ext_ctrl[3].size = 0;
  ext_ctrl[3].reserved2[0] = 0;
  ext_ctrl[3].value = V4L2_MPEG_VIDEO_H264_LEVEL_4_2;

  ext_ctrl[4].id = V4L2_CID_MPEG_VIDEO_BITRATE;
  ext_ctrl[4].size = 0;
  ext_ctrl[4].reserved2[0] = 0;
  ext_ctrl[4].value = h264encoder->bitrateBps;

  ext_ctrl[5].id = V4L2_CID_MPEG_VIDEO_H264_CPB_SIZE;
  ext_ctrl[5].size = 0;
  ext_ctrl[5].reserved2[0] = 0;
  ext_ctrl[5].value = cpbBufferSize;

  ext_ctrls.ctrl_class = V4L2_CTRL_CLASS_MPEG;
  ext_ctrls.count = 6;
  ext_ctrls.reserved[0] = 0;
  ext_ctrls.reserved[1] = 0;
  ext_ctrls.controls = ext_ctrl;

#if 0                           //not yet supported
  ext_ctrl[6].id = V4L2_CID_MPEG_VIDEO_MULTI_SLICE_MAX_BYTES;
  ext_ctrl[6].size = 0;
  ext_ctrl[6].reserved2[0] = 0;
  ext_ctrl[6].value = 0;

  ext_ctrl[7].id = V4L2_CID_MPEG_VIDEO_MULTI_SLICE_MAX_MB;
  ext_ctrl[7].size = 0;
  ext_ctrl[7].reserved2[0] = 0;
  ext_ctrl[7].value = 0;

  ext_ctrl[8].id = V4L2_CID_MPEG_VIDEO_MULTI_SLICE_MODE;
  ext_ctrl[8].size = 0;
  ext_ctrl[8].reserved2[0] = 0;
  ext_ctrl[8].value = 0;

  ext_ctrls.count = 9;
#endif

  if (ioctl (encoder->v4l2_fd_output, VIDIOC_S_EXT_CTRLS, &ext_ctrls) < 0) {
    GST_ERROR_OBJECT (h264encoder, "cannot set the extended controls - %s",
        strerror (errno));
    return FALSE;
  }

  /* Preproc controls settings */
  ctrl_p.id = V4L2_CID_MPEG_STM_VIDEO_H264_DEI_SET_MODE;
  ctrl_p.value = 1;
  if (ioctl (encoder->v4l2_fd_output, VIDIOC_S_CTRL, &ctrl_p) < 0) {
    GST_ERROR_OBJECT (h264encoder,
        "cannot set the preproc DEI mode control - %s", strerror (errno));
    return FALSE;
  }

  ctrl_p.id = V4L2_CID_MPEG_STM_VIDEO_H264_NOISE_FILTERING;
  ctrl_p.value = 0;
  if (ioctl (encoder->v4l2_fd_output, VIDIOC_S_CTRL, &ctrl_p) < 0) {
    GST_ERROR_OBJECT (h264encoder,
        "cannot set the preproc noise filtering control - %s",
        strerror (errno));
    return FALSE;
  }

  ctrl_p.id = V4L2_CID_MPEG_STM_VIDEO_H264_ASPECT_RATIO;
  ctrl_p.value = h264encoder->output_aspect_ratio;
  if (ioctl (encoder->v4l2_fd_output, VIDIOC_S_CTRL, &ctrl_p) < 0) {
    GST_ERROR_OBJECT (encoder,
        "cannot set the preproc aspect ratio control - %s", strerror (errno));
    return FALSE;
  }
  return TRUE;
}

static void
parse_key_unit_event (GstEvent * event, GstClockTime * running_time,
    gboolean * all_headers, guint * count)
{
  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CUSTOM_UPSTREAM:
      gst_video_event_parse_upstream_force_key_unit (event,
          running_time, all_headers, count);
      break;
    case GST_EVENT_CUSTOM_DOWNSTREAM:
      gst_video_event_parse_downstream_force_key_unit (event, NULL, NULL,
          running_time, all_headers, count);
      break;
    default:
      GST_DEBUG ("should not reach in this case");
      break;
  }
}

static void
gst_sth264encode_encoder_read (GstPad * pad)
{
  Gststh264encode *h264encoder = NULL;
  Gststencode *encoder;
  struct v4l2_buffer v4l_buffer_output;
  GstBuffer *gst_buffer;
  GstClockTimeDiff threshold;
  GstMapInfo info_write;
  GstEncodeBufferMeta *meta;

  h264encoder = GST_STH264ENCODE (GST_OBJECT_PARENT (pad));
  encoder = GST_STENCODE (h264encoder);
  GST_LOG_OBJECT (h264encoder, "read loop");

  if (encoder->flushing) {
    goto pause_task;
  }

  /* Init Output buffer */
  memset (&v4l_buffer_output, 0, sizeof (struct v4l2_buffer));
  v4l_buffer_output.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  v4l_buffer_output.memory = V4L2_MEMORY_MMAP;
  v4l_buffer_output.field = 0;
  v4l_buffer_output.length =
      h264encoder->output_width * h264encoder->output_height * 16 / 8;

  /* In case it fails */
  encoder->skipped_frame_count++;

  /* Have to streamon after the first QBUF */
  if (!encoder->streaming_capture) {
    g_mutex_lock (&encoder->stream_lock);
    /* Start streaming output */
    if (!gst_stencode_set_streaming (encoder, encoder->v4l2_fd_output,
            V4L2_BUF_TYPE_VIDEO_CAPTURE, TRUE)) {
      g_cond_signal (&encoder->stream_cond);
      g_mutex_unlock (&encoder->stream_lock);
      goto pause_task;
    }
    g_cond_signal (&encoder->stream_cond);
    g_mutex_unlock (&encoder->stream_lock);
  } else if (encoder->encoded_frame_count >= encoder->output_pool->num_buffers) {
    /* Look for buffer before dequeue if not it will hang */
    if (!gst_encode_buffer_pool_check_queued_buffer (encoder->output_pool)) {
      /* CPU load is too high without this */
      goto done;
    }
  }

  /* DeQueue Ouput buffer */
  if (ioctl (encoder->v4l2_fd_output, VIDIOC_DQBUF, &v4l_buffer_output) < 0) {
    GST_ELEMENT_ERROR (h264encoder, RESOURCE, READ,
        ("Err = %d (%s): Can not DeQueue output buffer for frame encode #%lld",
            errno, strerror (errno), encoder->encoded_frame_count + 1), NULL);
    goto pause_task;
  }
  gst_encode_buffer_pool_mark_dequeue_buffer_by_index (encoder->output_pool,
      v4l_buffer_output.index);

  encoder->encoded_frame_count++;

  if (v4l_buffer_output.bytesused == 0) {
    GST_DEBUG_OBJECT (h264encoder, "Received an EOS");
    /* Received an EOS, we shouldn't try anymore to read data otherwise we will get blocked */
    g_mutex_lock (&encoder->eos_lock);
    GST_DEBUG_OBJECT (h264encoder, "Pausing task on EOS");
    gst_pad_pause_task (encoder->srcpad);
    if (encoder->eos_received) {
      GST_DEBUG_OBJECT (h264encoder, "eos_received TRUE, pushing EOS event");
      g_mutex_unlock (&encoder->eos_lock);
      /* Do not reset the eos_received flag here, this will be done in PAUSED_TO_READY state change */
      if (!gst_pad_push_event (encoder->srcpad, gst_event_new_eos ())) {
        GST_DEBUG_OBJECT (h264encoder, "Failed to push EOS event");
        goto pause_task;
      }
      GST_DEBUG_OBJECT (h264encoder, "Pushed EOS event");
    } else {
      GST_DEBUG_OBJECT (h264encoder, "eos_received FALSE, signalling eos cond");
      g_cond_signal (&encoder->eos_cond);
      g_mutex_unlock (&encoder->eos_lock);
    }
    GST_DEBUG_OBJECT (h264encoder, "Done with EOS handling");
    goto done;
  }

  /*In fact it succeeded */
  encoder->skipped_frame_count--;
  GST_LOG_OBJECT (h264encoder, "Frame successfully encoded with size %d",
      v4l_buffer_output.bytesused);

  /* Create the gstreamer buffer */
  gst_buffer =
      gst_encode_buffer_pool_get_buffer_by_index (encoder->output_pool,
      v4l_buffer_output.index);
  gst_buffer_map (gst_buffer, &info_write, GST_MAP_WRITE);
  meta =
      (GstEncodeBufferMeta *) gst_buffer_get_meta (gst_buffer,
      GST_ENCODEBUFFER_META_API_TYPE);
  memcpy (info_write.data, meta->bufferinfo->start,
      v4l_buffer_output.bytesused);
  gst_buffer_unmap (gst_buffer, &info_write);
  gst_buffer_set_size (gst_buffer, v4l_buffer_output.bytesused);
  GST_BUFFER_TIMESTAMP (gst_buffer) =
      GST_TIMEVAL_TO_TIME (v4l_buffer_output.timestamp);
  GST_BUFFER_DURATION (gst_buffer) = GST_CLOCK_TIME_NONE;
  /* Set the Discontinuity */
  if (v4l_buffer_output.flags & V4L2_BUF_FLAG_STM_ENCODE_TIME_DISCONTINUITY) {
    GST_DEBUG_OBJECT (h264encoder, "Discontinuity at frame %lld",
        encoder->encoded_frame_count);
    GST_BUFFER_FLAG_SET (gst_buffer, GST_BUFFER_FLAG_DISCONT);
  }
  if (v4l_buffer_output.flags & V4L2_BUF_FLAG_STM_ENCODE_CLOSED_GOP) {
    GST_LOG_OBJECT (h264encoder, "Encoded frame is in Closed GOP");
  }
  if (v4l_buffer_output.flags & V4L2_BUF_FLAG_KEYFRAME)
    GST_BUFFER_FLAG_UNSET (gst_buffer, GST_BUFFER_FLAG_DELTA_UNIT);
  else
    GST_BUFFER_FLAG_SET (gst_buffer, GST_BUFFER_FLAG_DELTA_UNIT);

  if (v4l_buffer_output.flags & V4L2_BUF_FLAG_STM_ENCODE_GOP_START &&
      h264encoder->pending_key_unit_events) {
    GList *item;
    GstClockTime running_time;

    GST_DEBUG_OBJECT (h264encoder, "Start of GOP at frame %lld after request",
        encoder->encoded_frame_count);

    running_time = gst_segment_to_running_time (&encoder->segment,
        GST_FORMAT_TIME, GST_BUFFER_TIMESTAMP (gst_buffer));

    for (item = h264encoder->pending_key_unit_events; item; item = item->next) {
      GstEvent *event = item->data;
      GstClockTime ev_running_time;
      GstClockTime stream_time;
      gboolean all_headers;
      guint count;

      parse_key_unit_event (event, &ev_running_time, &all_headers, &count);

      if (ev_running_time > running_time)
        continue;

      h264encoder->pending_key_unit_events =
          g_list_delete_link (h264encoder->pending_key_unit_events, item);
      gst_event_unref (event);

      stream_time = gst_segment_to_stream_time (&encoder->segment,
          GST_FORMAT_TIME, GST_BUFFER_TIMESTAMP (gst_buffer));

      GST_DEBUG_OBJECT (h264encoder, "Pushing downstream force key unit event"
          " ts: %" GST_TIME_FORMAT " stream time %" GST_TIME_FORMAT
          " running time: %" GST_TIME_FORMAT " all_headers %d count %u",
          GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (gst_buffer)),
          GST_TIME_ARGS (stream_time), GST_TIME_ARGS (running_time),
          all_headers, count);

      gst_pad_push_event (encoder->srcpad,
          gst_video_event_new_downstream_force_key_unit (GST_BUFFER_TIMESTAMP
              (gst_buffer), stream_time, running_time, all_headers, count));
      break;
    }
  } else if (v4l_buffer_output.flags & V4L2_BUF_FLAG_STM_ENCODE_GOP_START) {
    GST_DEBUG_OBJECT (h264encoder, "Start of GOP at frame %lld",
        encoder->encoded_frame_count);
  }
  if (h264encoder->input_fps_den > 0) {
    threshold =
        gst_util_uint64_scale_int (GST_SECOND, h264encoder->input_fps_den,
        h264encoder->input_fps_num) / 10;
  } else {
    threshold = 0;
  }

  if (gst_stencode_push_buffer (encoder, gst_buffer,
          threshold) == GST_FLOW_ERROR) {
    GST_ERROR_OBJECT (encoder, "FLOW_ERROR, pausing the read task");
    goto pause_task;
  }

done:
  return;

pause_task:
  /* We pause the task, so we will not be able to receive any EOS frame
     that the PAUSED->READY code might be waiting. So we signal the eos
     condition variable now, to unblock it, since we're done (till we
     are restarted, if we end up being). */
  g_mutex_lock (&encoder->eos_lock);
  gst_pad_pause_task (encoder->srcpad);
  g_cond_signal (&encoder->eos_cond);
  g_mutex_unlock (&encoder->eos_lock);
}

static void
gst_h264encoder_decorate_input_buffer (Gststencode * encoder,
    GstBuffer * buf, struct v4l2_buffer *v4l_input_buf)
{
  Gststh264encode *h264encoder = GST_STH264ENCODE (encoder);

  if (h264encoder->key_unit_events) {
    GstClockTime running_time;
    GList *item = NULL;

    running_time = gst_segment_to_running_time (&encoder->segment,
        GST_FORMAT_TIME, GST_BUFFER_TIMESTAMP (buf));

    for (item = h264encoder->key_unit_events; item; item = item->next) {
      GstEvent *event = item->data;
      GstClockTime ev_running_time;

      parse_key_unit_event (event, &ev_running_time, NULL, NULL);

      /* Simple case, keyunit ASAP */
      if (ev_running_time == GST_CLOCK_TIME_NONE)
        break;

      /* Event for before this frame */
      if (ev_running_time <= running_time)
        break;
    }

    if (item) {
      GstEvent *event = item->data;

      h264encoder->key_unit_events =
          g_list_delete_link (h264encoder->key_unit_events, item);
      h264encoder->pending_key_unit_events =
          g_list_append (h264encoder->pending_key_unit_events, event);

      GST_LOG_OBJECT (h264encoder,
          "Forcing a key unit at running time %" GST_TIME_FORMAT,
          GST_TIME_ARGS (running_time));

      v4l_input_buf->flags |= V4L2_BUF_FLAG_STM_ENCODE_CLOSED_GOP_REQUEST;
    }
  }

}

/* entry point to initialize the element */
gboolean
sth264encode_init (GstPlugin * sth264encode)
{
  GST_DEBUG_CATEGORY_INIT (gst_sth264encode_debug, "sth264encode", 0,
      "ST H264 encoder");

  return gst_element_register (sth264encode, "sth264encode", GST_RANK_PRIMARY,
      GST_TYPE_STH264ENCODE);
}
