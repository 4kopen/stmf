#!/bin/sh

GSTREAMER_BASE=$1

if [ -z "$GSTREAMER_BASE" ]
then
  echo "Need to set GSTEAMER_BASE (ex: /home/foo/gstreamer) to find gst-indent tool"
  echo "Usage: STMF_indent.sh <gstreamer path>"
else
  for ((i=0; i<2; i++));
  do
    # Apply gst-indent to all .c & .h file.
    find src -name '*' | grep -e '\.h' -e '\.c' | \
    xargs $GSTREAMER_BASE/tools/gst-indent

    # Clean-up backup files created by gst-indent (if any)
    find src -name '*' | grep -e '\.h~' -e '\.c~' > /dev/null
    if [ $? == 0 ]; then find src -name '*' | grep -e '\.h~' -e '\.c~' | xargs rm; fi
  done
fi

