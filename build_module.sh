#!/bin/bash

#build_module.sh

# this is standalone build file for APPS_STMF_ext. README.arm
# has been renamed to build_module.sh to configure env and
# build stmf. this file is supposed to be called by build_stmf.sh
# this file should only be used to compile stmf standalone.
# @author      : Pavan Goyal
# @Last Upadte : 5th Dec 2014

export CC=armv7-linux-gcc;
export CXX=armv7-linux-g++;
export AS=armv7-linux-as;
export LD=armv7-linux-ld;
export AR=armv7-linux-ar;
export RANLIB=armv7-linux-ranlib;
export NM=armv7-linux-nm;

export PATH=/opt/STM/STLinux-2.4/host/bin:/opt/STM/STLinux-2.4/devkit/armv7/bin:$PATH
export PKG_CONFIG=/opt/STM/STLinux-2.4/host/bin/pkg-config
export PKG_CONFIG_PATH=/opt/STM/STLinux-2.4/devkit/armv7/target/usr/lib/pkgconfig
export PKG_CONFIG_LIBDIR=/opt/STM/STLinux-2.4/devkit/armv7/target/usr/lib/pkgconfig
export PKG_PREFIX_BASE=/opt/STM/STLinux-2.4/devkit/armv7/target/
export PKG_CONFIG_SYSROOT_DIR=/opt/STM/STLinux-2.4/devkit/armv7/target/
export LIBTOOL_PREFIX_BASE=/opt/STM/STLinux-2.4/devkit/armv7/target/
export TARGET_DIR=/opt/STM/STLinux-2.4/devkit/armv7/target/

autoreconf --verbose --force --install
./configure --host=arm-cortex-linux-gnueabi --build=`./config.guess` --prefix=/usr --with-sysroot=${TARGET_DIR}
make
LIBTOOL_PREFIX_BASE=/opt/STM/STLinux-2.4/devkit/armv7/target/ PATH=/opt/STM/STLinux-2.4/host/bin:/opt/STM/STLinux-2.4/devkit/armv7/bin:$PATH make DESTDIR="/opt/STM/STLinux-2.4/devkit/armv7/target/" install
